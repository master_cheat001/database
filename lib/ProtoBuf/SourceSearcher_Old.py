import re,os
reProtoContract = re.compile(r'\s+\[ProtoContract\(Name = "(.+?)"\)\]')
reProtoMember = re.compile(r'\s+\[ProtoMember\((\d+?), (IsRequired = true, )?Name = "(.+?)", DataFormat = DataFormat\.(.+?)\)]')
reProtoMemberTyp = re.compile(r'\s+public (.+?) (.+)')
reProtoEnum = re.compile(r'\s+\[ProtoEnum\(Name = "(.+?)", Value = (\d+)\)\]')

lpath = os.path.join(
	os.path.realpath(__file__),
	*[os.pardir,os.pardir,os.pardir,'res','Source','Assets','Source','BlackJack','ConfigData']
)

def CollectProtoData(mypath=lpath):
	# path to files
	files = os.listdir(mypath)

	# enum
	configs= {}
	enums = {}

	for f in files:
		#print(f)
		try:
			with open(os.path.join(mypath,f), "rt", encoding='utf8') as f:
				#find contract start and decide if enum or config
				for line in f:
					match = reProtoContract.match(line)
					if match:
						name=match[1]
						if 'enum' in f.readline():
							enums[name]={}
							for line in f:
								match = reProtoEnum.match(line)
								if match:
									enums[name][match[2]] = match[1]
						else:
							configs[name]={}
							for line in f:
								match = reProtoMember.match(line)
								if match:
									#/
									#0-all, 1-number, 2-Required, 3-name, 4-DataFormat, 5 type
									#2
									#	1-typ, 2-name
									match2 = reProtoMemberTyp.match(f.readline())

									configs[name][match[1]]={
										'num':	match[1],
										'name':	match[3],
										'required':	bool(match[2]),
										'dataformat':	match[4],
										'type':	match2[1]
									}

		except PermissionError:
			print('PermissionError')

	return (configs, enums)

if __name__ == '__main__':
	t,e=CollectProtoData(lpath)
	print(t)
	print(e)