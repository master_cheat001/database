import os,json

from lib import CollectProtoData,CollectProtoData_Old,generateProtos

PATH = os.path.dirname(os.path.realpath(__file__))
set__path = os.path.join(PATH,*['res','FoundSettings'])
buffer_path  = os.path.join(PATH,*['res','ProtoBuffer'])
protopy_path = os.path.join(PATH,*['res','ProtoPy'])

def SaveJson(path,obj):
	os.makedirs(os.path.dirname(path), exist_ok=True)
	open(path,'wb').write(json.dumps(obj, ensure_ascii=False, indent='\t').encode('utf8'))

#	1.	Create Old Protos
version='Old'
source			=	os.path.join(PATH,*['res','Assembly','Source','Assets','Source','BlackJack','ConfigData'])
configs, enums = CollectProtoData_Old(source)

SaveJson(os.path.join(set__path,'Config_old.json'),configs)
SaveJson(os.path.join(set__path,'Enums_old.json'),enums)

os.makedirs(os.path.join(buffer_path,version),exist_ok=True)
os.makedirs(os.path.join(protopy_path,version),exist_ok=True)
generateProtos(configs, enums, os.path.join(buffer_path,version), os.path.join(protopy_path,version))

#	2.	Create GL/CN Protos
for version in ['Global','China']:
	source			=	os.path.join(PATH,*['res','Assembly',version,'BlackJack','ConfigData'])
	configs, enums = CollectProtoData(source)

	SaveJson(os.path.join(set__path,'Config_%s.json'%version),configs)
	SaveJson(os.path.join(set__path,'Enums_%s.json'%version),enums)

	os.makedirs(os.path.join(buffer_path,version),exist_ok=True)
	os.makedirs(os.path.join(protopy_path,version),exist_ok=True)
	generateProtos(configs, enums, os.path.join(buffer_path,version), os.path.join(protopy_path,version))