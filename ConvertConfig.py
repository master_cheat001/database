import os,json
from lib.ProtoBuf import proto_spy, ConvertConfigData

PATH = os.path.dirname(os.path.realpath(__file__))

SPY=False
PROTO=True

configuable_const={
	version.title():{
		int(i):val[18:]
		for i,val in json.loads(open(os.path.join(PATH, *['res','FoundSettings','Enums_%s.json'%version]),'rb').read())['ConfigableConstId'].items()
	}
	for version in ['Global','China','old']
}

def ConvertConfigs(p_raw, cpath):
	#	1.	convert via spy
	if SPY:
		lpath=os.path.join(cpath,'Spy')
		os.makedirs(lpath,exist_ok=True)
		for fp in os.listdir(p_raw):
			name=fp.rsplit('.',1)[0]
			def ConfigSpyToJson(path_raw,path_dest):
				data=open(path_raw,'rb')
				obj = proto_spy.read_proto_message(data)
				open(path_dest,'wb').write(json.dumps(obj,ensure_ascii=False, indent='\t').encode('utf8'))
			ConfigSpyToJson(os.path.join(p_raw,fp),os.path.join(lpath,name+'.json'))

	#	2.	via Protos
	if PROTO:
		protopy_path=os.path.join(PATH,*['res','ProtoPy'])
		for v in ['Global','China','Old']:
			print(v)
			lpath			=	os.path.join(cpath,v)
			lprotopy_path	=	os.path.join(protopy_path,v)
			os.makedirs(lpath,exist_ok=True)
			for fp in os.listdir(p_raw):
				name=fp.split('.',1)[0]
				ConvertConfigData(os.path.join(p_raw,fp),os.path.join(lpath,name+'.json'),lprotopy_path)


	#	fix ConfigDataConfigableConst
	for v in ['Global','China','Old']:
		lpath = os.path.join(cpath,v)
		data = json.loads(open(os.path.join(lpath,'ConfigDataConfigableConst.json'),'rb').read())
		for entry in data:
			if entry['ID'] in configuable_const[v]:
				entry['iname'] = configuable_const[v][entry['ID']]
		open(os.path.join(lpath,*['ConfigDataConfigableConst.json']),'wb').write(json.dumps(data,ensure_ascii=False,indent='\t').encode('utf8'))

if __name__ == '__main__':
	print('global')
	p_raw = config_path = os.path.join(PATH,*['assets','global','ExportAssetBundle','configdata','configdata','en'])
	cpath = os.path.dirname(os.path.dirname(p_raw))
	ConvertConfigs(p_raw,cpath)
	
	print('china')
	p_raw = os.path.join(PATH,*['assets','china','ExportAssetBundle','configdata','configdata'])
	cpath = os.path.dirname(p_raw)
	ConvertConfigs(p_raw,cpath)
