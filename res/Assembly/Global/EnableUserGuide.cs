﻿// Decompiled with JetBrains decompiler
// Type: EnableUserGuide
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.UI;

public class EnableUserGuide : IDebugCmd
{
  public void Execute(string strParams)
  {
    UserGuideUITask.Enable = bool.Parse(strParams);
  }

  public string GetHelpDesc()
  {
    return "eug true : 启用新手引导；eug false : 关闭新手引导；";
  }

  public string GetName()
  {
    return "eug";
  }
}
