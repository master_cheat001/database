﻿// Decompiled with JetBrains decompiler
// Type: KeyUpEventArgs
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using System;
using UnityEngine;

public class KeyUpEventArgs : EventArgs
{
  private readonly KeyCode _keyCode;

  public KeyUpEventArgs(KeyCode keyCode)
  {
    this._keyCode = keyCode;
  }

  public KeyCode KeyCode
  {
    get
    {
      return this._keyCode;
    }
  }
}
