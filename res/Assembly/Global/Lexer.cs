﻿// Decompiled with JetBrains decompiler
// Type: Lexer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using System;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;

internal class Lexer
{
  private static int[] fsm_return_table;
  private static Lexer.StateHandler[] fsm_handler_table;
  private bool allow_comments;
  private bool allow_single_quoted_strings;
  private bool end_of_input;
  private FsmContext fsm_context;
  private int input_buffer;
  private int input_char;
  private TextReader reader;
  private int state;
  private StringBuilder string_buffer;
  private string string_value;
  private int token;
  private int unichar;

  static Lexer()
  {
    Lexer.PopulateFsmTables();
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public Lexer(TextReader reader)
  {
    this.allow_comments = true;
    this.allow_single_quoted_strings = true;
    this.input_buffer = 0;
    this.string_buffer = new StringBuilder(128);
    this.state = 1;
    this.end_of_input = false;
    this.reader = reader;
    this.fsm_context = new FsmContext();
    this.fsm_context.L = this;
  }

  public bool AllowComments
  {
    get
    {
      return this.allow_comments;
    }
    set
    {
      this.allow_comments = value;
    }
  }

  public bool AllowSingleQuotedStrings
  {
    get
    {
      return this.allow_single_quoted_strings;
    }
    set
    {
      this.allow_single_quoted_strings = value;
    }
  }

  public bool EndOfInput
  {
    get
    {
      return this.end_of_input;
    }
  }

  public int Token
  {
    get
    {
      return this.token;
    }
  }

  public string StringValue
  {
    get
    {
      return this.string_value;
    }
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static int HexValue(int digit)
  {
    switch (digit)
    {
      case 65:
      case 97:
        return 10;
      case 66:
      case 98:
        return 11;
      case 67:
      case 99:
        return 12;
      case 68:
      case 100:
        return 13;
      case 69:
      case 101:
        return 14;
      case 70:
      case 102:
        return 15;
      default:
        return digit - 48;
    }
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static void PopulateFsmTables()
  {
    Lexer.StateHandler[] stateHandlerArray = new Lexer.StateHandler[28];
    // ISSUE: reference to a compiler-generated field
    if (Lexer.\u003C\u003Ef__mg\u0024cache0 == null)
    {
      // ISSUE: reference to a compiler-generated field
      Lexer.\u003C\u003Ef__mg\u0024cache0 = new Lexer.StateHandler(Lexer.State1);
    }
    // ISSUE: reference to a compiler-generated field
    stateHandlerArray[0] = Lexer.\u003C\u003Ef__mg\u0024cache0;
    // ISSUE: reference to a compiler-generated field
    if (Lexer.\u003C\u003Ef__mg\u0024cache1 == null)
    {
      // ISSUE: reference to a compiler-generated field
      Lexer.\u003C\u003Ef__mg\u0024cache1 = new Lexer.StateHandler(Lexer.State2);
    }
    // ISSUE: reference to a compiler-generated field
    stateHandlerArray[1] = Lexer.\u003C\u003Ef__mg\u0024cache1;
    // ISSUE: reference to a compiler-generated field
    if (Lexer.\u003C\u003Ef__mg\u0024cache2 == null)
    {
      // ISSUE: reference to a compiler-generated field
      Lexer.\u003C\u003Ef__mg\u0024cache2 = new Lexer.StateHandler(Lexer.State3);
    }
    // ISSUE: reference to a compiler-generated field
    stateHandlerArray[2] = Lexer.\u003C\u003Ef__mg\u0024cache2;
    // ISSUE: reference to a compiler-generated field
    if (Lexer.\u003C\u003Ef__mg\u0024cache3 == null)
    {
      // ISSUE: reference to a compiler-generated field
      Lexer.\u003C\u003Ef__mg\u0024cache3 = new Lexer.StateHandler(Lexer.State4);
    }
    // ISSUE: reference to a compiler-generated field
    stateHandlerArray[3] = Lexer.\u003C\u003Ef__mg\u0024cache3;
    // ISSUE: reference to a compiler-generated field
    if (Lexer.\u003C\u003Ef__mg\u0024cache4 == null)
    {
      // ISSUE: reference to a compiler-generated field
      Lexer.\u003C\u003Ef__mg\u0024cache4 = new Lexer.StateHandler(Lexer.State5);
    }
    // ISSUE: reference to a compiler-generated field
    stateHandlerArray[4] = Lexer.\u003C\u003Ef__mg\u0024cache4;
    // ISSUE: reference to a compiler-generated field
    if (Lexer.\u003C\u003Ef__mg\u0024cache5 == null)
    {
      // ISSUE: reference to a compiler-generated field
      Lexer.\u003C\u003Ef__mg\u0024cache5 = new Lexer.StateHandler(Lexer.State6);
    }
    // ISSUE: reference to a compiler-generated field
    stateHandlerArray[5] = Lexer.\u003C\u003Ef__mg\u0024cache5;
    // ISSUE: reference to a compiler-generated field
    if (Lexer.\u003C\u003Ef__mg\u0024cache6 == null)
    {
      // ISSUE: reference to a compiler-generated field
      Lexer.\u003C\u003Ef__mg\u0024cache6 = new Lexer.StateHandler(Lexer.State7);
    }
    // ISSUE: reference to a compiler-generated field
    stateHandlerArray[6] = Lexer.\u003C\u003Ef__mg\u0024cache6;
    // ISSUE: reference to a compiler-generated field
    if (Lexer.\u003C\u003Ef__mg\u0024cache7 == null)
    {
      // ISSUE: reference to a compiler-generated field
      Lexer.\u003C\u003Ef__mg\u0024cache7 = new Lexer.StateHandler(Lexer.State8);
    }
    // ISSUE: reference to a compiler-generated field
    stateHandlerArray[7] = Lexer.\u003C\u003Ef__mg\u0024cache7;
    // ISSUE: reference to a compiler-generated field
    if (Lexer.\u003C\u003Ef__mg\u0024cache8 == null)
    {
      // ISSUE: reference to a compiler-generated field
      Lexer.\u003C\u003Ef__mg\u0024cache8 = new Lexer.StateHandler(Lexer.State9);
    }
    // ISSUE: reference to a compiler-generated field
    stateHandlerArray[8] = Lexer.\u003C\u003Ef__mg\u0024cache8;
    // ISSUE: reference to a compiler-generated field
    if (Lexer.\u003C\u003Ef__mg\u0024cache9 == null)
    {
      // ISSUE: reference to a compiler-generated field
      Lexer.\u003C\u003Ef__mg\u0024cache9 = new Lexer.StateHandler(Lexer.State10);
    }
    // ISSUE: reference to a compiler-generated field
    stateHandlerArray[9] = Lexer.\u003C\u003Ef__mg\u0024cache9;
    // ISSUE: reference to a compiler-generated field
    if (Lexer.\u003C\u003Ef__mg\u0024cacheA == null)
    {
      // ISSUE: reference to a compiler-generated field
      Lexer.\u003C\u003Ef__mg\u0024cacheA = new Lexer.StateHandler(Lexer.State11);
    }
    // ISSUE: reference to a compiler-generated field
    stateHandlerArray[10] = Lexer.\u003C\u003Ef__mg\u0024cacheA;
    // ISSUE: reference to a compiler-generated field
    if (Lexer.\u003C\u003Ef__mg\u0024cacheB == null)
    {
      // ISSUE: reference to a compiler-generated field
      Lexer.\u003C\u003Ef__mg\u0024cacheB = new Lexer.StateHandler(Lexer.State12);
    }
    // ISSUE: reference to a compiler-generated field
    stateHandlerArray[11] = Lexer.\u003C\u003Ef__mg\u0024cacheB;
    // ISSUE: reference to a compiler-generated field
    if (Lexer.\u003C\u003Ef__mg\u0024cacheC == null)
    {
      // ISSUE: reference to a compiler-generated field
      Lexer.\u003C\u003Ef__mg\u0024cacheC = new Lexer.StateHandler(Lexer.State13);
    }
    // ISSUE: reference to a compiler-generated field
    stateHandlerArray[12] = Lexer.\u003C\u003Ef__mg\u0024cacheC;
    // ISSUE: reference to a compiler-generated field
    if (Lexer.\u003C\u003Ef__mg\u0024cacheD == null)
    {
      // ISSUE: reference to a compiler-generated field
      Lexer.\u003C\u003Ef__mg\u0024cacheD = new Lexer.StateHandler(Lexer.State14);
    }
    // ISSUE: reference to a compiler-generated field
    stateHandlerArray[13] = Lexer.\u003C\u003Ef__mg\u0024cacheD;
    // ISSUE: reference to a compiler-generated field
    if (Lexer.\u003C\u003Ef__mg\u0024cacheE == null)
    {
      // ISSUE: reference to a compiler-generated field
      Lexer.\u003C\u003Ef__mg\u0024cacheE = new Lexer.StateHandler(Lexer.State15);
    }
    // ISSUE: reference to a compiler-generated field
    stateHandlerArray[14] = Lexer.\u003C\u003Ef__mg\u0024cacheE;
    // ISSUE: reference to a compiler-generated field
    if (Lexer.\u003C\u003Ef__mg\u0024cacheF == null)
    {
      // ISSUE: reference to a compiler-generated field
      Lexer.\u003C\u003Ef__mg\u0024cacheF = new Lexer.StateHandler(Lexer.State16);
    }
    // ISSUE: reference to a compiler-generated field
    stateHandlerArray[15] = Lexer.\u003C\u003Ef__mg\u0024cacheF;
    // ISSUE: reference to a compiler-generated field
    if (Lexer.\u003C\u003Ef__mg\u0024cache10 == null)
    {
      // ISSUE: reference to a compiler-generated field
      Lexer.\u003C\u003Ef__mg\u0024cache10 = new Lexer.StateHandler(Lexer.State17);
    }
    // ISSUE: reference to a compiler-generated field
    stateHandlerArray[16] = Lexer.\u003C\u003Ef__mg\u0024cache10;
    // ISSUE: reference to a compiler-generated field
    if (Lexer.\u003C\u003Ef__mg\u0024cache11 == null)
    {
      // ISSUE: reference to a compiler-generated field
      Lexer.\u003C\u003Ef__mg\u0024cache11 = new Lexer.StateHandler(Lexer.State18);
    }
    // ISSUE: reference to a compiler-generated field
    stateHandlerArray[17] = Lexer.\u003C\u003Ef__mg\u0024cache11;
    // ISSUE: reference to a compiler-generated field
    if (Lexer.\u003C\u003Ef__mg\u0024cache12 == null)
    {
      // ISSUE: reference to a compiler-generated field
      Lexer.\u003C\u003Ef__mg\u0024cache12 = new Lexer.StateHandler(Lexer.State19);
    }
    // ISSUE: reference to a compiler-generated field
    stateHandlerArray[18] = Lexer.\u003C\u003Ef__mg\u0024cache12;
    // ISSUE: reference to a compiler-generated field
    if (Lexer.\u003C\u003Ef__mg\u0024cache13 == null)
    {
      // ISSUE: reference to a compiler-generated field
      Lexer.\u003C\u003Ef__mg\u0024cache13 = new Lexer.StateHandler(Lexer.State20);
    }
    // ISSUE: reference to a compiler-generated field
    stateHandlerArray[19] = Lexer.\u003C\u003Ef__mg\u0024cache13;
    // ISSUE: reference to a compiler-generated field
    if (Lexer.\u003C\u003Ef__mg\u0024cache14 == null)
    {
      // ISSUE: reference to a compiler-generated field
      Lexer.\u003C\u003Ef__mg\u0024cache14 = new Lexer.StateHandler(Lexer.State21);
    }
    // ISSUE: reference to a compiler-generated field
    stateHandlerArray[20] = Lexer.\u003C\u003Ef__mg\u0024cache14;
    // ISSUE: reference to a compiler-generated field
    if (Lexer.\u003C\u003Ef__mg\u0024cache15 == null)
    {
      // ISSUE: reference to a compiler-generated field
      Lexer.\u003C\u003Ef__mg\u0024cache15 = new Lexer.StateHandler(Lexer.State22);
    }
    // ISSUE: reference to a compiler-generated field
    stateHandlerArray[21] = Lexer.\u003C\u003Ef__mg\u0024cache15;
    // ISSUE: reference to a compiler-generated field
    if (Lexer.\u003C\u003Ef__mg\u0024cache16 == null)
    {
      // ISSUE: reference to a compiler-generated field
      Lexer.\u003C\u003Ef__mg\u0024cache16 = new Lexer.StateHandler(Lexer.State23);
    }
    // ISSUE: reference to a compiler-generated field
    stateHandlerArray[22] = Lexer.\u003C\u003Ef__mg\u0024cache16;
    // ISSUE: reference to a compiler-generated field
    if (Lexer.\u003C\u003Ef__mg\u0024cache17 == null)
    {
      // ISSUE: reference to a compiler-generated field
      Lexer.\u003C\u003Ef__mg\u0024cache17 = new Lexer.StateHandler(Lexer.State24);
    }
    // ISSUE: reference to a compiler-generated field
    stateHandlerArray[23] = Lexer.\u003C\u003Ef__mg\u0024cache17;
    // ISSUE: reference to a compiler-generated field
    if (Lexer.\u003C\u003Ef__mg\u0024cache18 == null)
    {
      // ISSUE: reference to a compiler-generated field
      Lexer.\u003C\u003Ef__mg\u0024cache18 = new Lexer.StateHandler(Lexer.State25);
    }
    // ISSUE: reference to a compiler-generated field
    stateHandlerArray[24] = Lexer.\u003C\u003Ef__mg\u0024cache18;
    // ISSUE: reference to a compiler-generated field
    if (Lexer.\u003C\u003Ef__mg\u0024cache19 == null)
    {
      // ISSUE: reference to a compiler-generated field
      Lexer.\u003C\u003Ef__mg\u0024cache19 = new Lexer.StateHandler(Lexer.State26);
    }
    // ISSUE: reference to a compiler-generated field
    stateHandlerArray[25] = Lexer.\u003C\u003Ef__mg\u0024cache19;
    // ISSUE: reference to a compiler-generated field
    if (Lexer.\u003C\u003Ef__mg\u0024cache1A == null)
    {
      // ISSUE: reference to a compiler-generated field
      Lexer.\u003C\u003Ef__mg\u0024cache1A = new Lexer.StateHandler(Lexer.State27);
    }
    // ISSUE: reference to a compiler-generated field
    stateHandlerArray[26] = Lexer.\u003C\u003Ef__mg\u0024cache1A;
    // ISSUE: reference to a compiler-generated field
    if (Lexer.\u003C\u003Ef__mg\u0024cache1B == null)
    {
      // ISSUE: reference to a compiler-generated field
      Lexer.\u003C\u003Ef__mg\u0024cache1B = new Lexer.StateHandler(Lexer.State28);
    }
    // ISSUE: reference to a compiler-generated field
    stateHandlerArray[27] = Lexer.\u003C\u003Ef__mg\u0024cache1B;
    Lexer.fsm_handler_table = stateHandlerArray;
    Lexer.fsm_return_table = new int[28]
    {
      65542,
      0,
      65537,
      65537,
      0,
      65537,
      0,
      65537,
      0,
      0,
      65538,
      0,
      0,
      0,
      65539,
      0,
      0,
      65540,
      65541,
      65542,
      0,
      0,
      65541,
      65542,
      0,
      0,
      0,
      0
    };
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static char ProcessEscChar(int esc_char)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static bool State1(FsmContext ctx)
  {
    while (ctx.L.GetChar())
    {
      if (ctx.L.input_char != 32 && (ctx.L.input_char < 9 || ctx.L.input_char > 13))
      {
        if (ctx.L.input_char >= 49 && ctx.L.input_char <= 57)
        {
          ctx.L.string_buffer.Append((char) ctx.L.input_char);
          ctx.NextState = 3;
          return true;
        }
        int inputChar = ctx.L.input_char;
        switch (inputChar)
        {
          case 44:
label_14:
            ctx.NextState = 1;
            ctx.Return = true;
            return true;
          case 45:
            ctx.L.string_buffer.Append((char) ctx.L.input_char);
            ctx.NextState = 2;
            return true;
          case 47:
            if (!ctx.L.allow_comments)
              return false;
            ctx.NextState = 25;
            return true;
          case 48:
            ctx.L.string_buffer.Append((char) ctx.L.input_char);
            ctx.NextState = 4;
            return true;
          default:
            switch (inputChar - 91)
            {
              case 0:
              case 2:
                goto label_14;
              default:
                switch (inputChar - 123)
                {
                  case 0:
                  case 2:
                    goto label_14;
                  default:
                    if (inputChar != 34)
                    {
                      if (inputChar != 39)
                      {
                        if (inputChar != 58)
                        {
                          if (inputChar != 102)
                          {
                            if (inputChar != 110)
                            {
                              if (inputChar != 116)
                                return false;
                              ctx.NextState = 9;
                              return true;
                            }
                            ctx.NextState = 16;
                            return true;
                          }
                          ctx.NextState = 12;
                          return true;
                        }
                        goto label_14;
                      }
                      else
                      {
                        if (!ctx.L.allow_single_quoted_strings)
                          return false;
                        ctx.L.input_char = 34;
                        ctx.NextState = 23;
                        ctx.Return = true;
                        return true;
                      }
                    }
                    else
                    {
                      ctx.NextState = 19;
                      ctx.Return = true;
                      return true;
                    }
                }
            }
        }
      }
    }
    return true;
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static bool State2(FsmContext ctx)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static bool State3(FsmContext ctx)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static bool State4(FsmContext ctx)
  {
    ctx.L.GetChar();
    if (ctx.L.input_char == 32 || ctx.L.input_char >= 9 && ctx.L.input_char <= 13)
    {
      ctx.Return = true;
      ctx.NextState = 1;
      return true;
    }
    int inputChar = ctx.L.input_char;
    switch (inputChar)
    {
      case 44:
        ctx.L.UngetChar();
        ctx.Return = true;
        ctx.NextState = 1;
        return true;
      case 46:
        ctx.L.string_buffer.Append((char) ctx.L.input_char);
        ctx.NextState = 5;
        return true;
      default:
        if (inputChar != 69)
        {
          if (inputChar != 93)
          {
            if (inputChar != 101)
            {
              if (inputChar != 125)
                return false;
              goto case 44;
            }
          }
          else
            goto case 44;
        }
        ctx.L.string_buffer.Append((char) ctx.L.input_char);
        ctx.NextState = 7;
        return true;
    }
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static bool State5(FsmContext ctx)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static bool State6(FsmContext ctx)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static bool State7(FsmContext ctx)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static bool State8(FsmContext ctx)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static bool State9(FsmContext ctx)
  {
    ctx.L.GetChar();
    if (ctx.L.input_char != 114)
      return false;
    ctx.NextState = 10;
    return true;
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static bool State10(FsmContext ctx)
  {
    ctx.L.GetChar();
    if (ctx.L.input_char != 117)
      return false;
    ctx.NextState = 11;
    return true;
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static bool State11(FsmContext ctx)
  {
    ctx.L.GetChar();
    if (ctx.L.input_char != 101)
      return false;
    ctx.Return = true;
    ctx.NextState = 1;
    return true;
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static bool State12(FsmContext ctx)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static bool State13(FsmContext ctx)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static bool State14(FsmContext ctx)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static bool State15(FsmContext ctx)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static bool State16(FsmContext ctx)
  {
    ctx.L.GetChar();
    if (ctx.L.input_char != 117)
      return false;
    ctx.NextState = 17;
    return true;
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static bool State17(FsmContext ctx)
  {
    ctx.L.GetChar();
    if (ctx.L.input_char != 108)
      return false;
    ctx.NextState = 18;
    return true;
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static bool State18(FsmContext ctx)
  {
    ctx.L.GetChar();
    if (ctx.L.input_char != 108)
      return false;
    ctx.Return = true;
    ctx.NextState = 1;
    return true;
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static bool State19(FsmContext ctx)
  {
    while (ctx.L.GetChar())
    {
      switch (ctx.L.input_char)
      {
        case 34:
          ctx.L.UngetChar();
          ctx.Return = true;
          ctx.NextState = 20;
          return true;
        case 92:
          ctx.StateStack = 19;
          ctx.NextState = 21;
          return true;
        default:
          ctx.L.string_buffer.Append((char) ctx.L.input_char);
          continue;
      }
    }
    return true;
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static bool State20(FsmContext ctx)
  {
    ctx.L.GetChar();
    if (ctx.L.input_char != 34)
      return false;
    ctx.Return = true;
    ctx.NextState = 1;
    return true;
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static bool State21(FsmContext ctx)
  {
    ctx.L.GetChar();
    int inputChar = ctx.L.input_char;
    switch (inputChar)
    {
      case 114:
      case 116:
        ctx.L.string_buffer.Append(Lexer.ProcessEscChar(ctx.L.input_char));
        ctx.NextState = ctx.StateStack;
        return true;
      case 117:
        ctx.NextState = 22;
        return true;
      default:
        if (inputChar != 34 && inputChar != 39 && (inputChar != 47 && inputChar != 92) && (inputChar != 98 && inputChar != 102 && inputChar != 110))
          return false;
        goto case 114;
    }
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static bool State22(FsmContext ctx)
  {
    int num1 = 0;
    int num2 = 4096;
    ctx.L.unichar = 0;
    while (ctx.L.GetChar())
    {
      if ((ctx.L.input_char < 48 || ctx.L.input_char > 57) && (ctx.L.input_char < 65 || ctx.L.input_char > 70) && (ctx.L.input_char < 97 || ctx.L.input_char > 102))
        return false;
      ctx.L.unichar += Lexer.HexValue(ctx.L.input_char) * num2;
      ++num1;
      num2 /= 16;
      if (num1 == 4)
      {
        ctx.L.string_buffer.Append(Convert.ToChar(ctx.L.unichar));
        ctx.NextState = ctx.StateStack;
        return true;
      }
    }
    return true;
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static bool State23(FsmContext ctx)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static bool State24(FsmContext ctx)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static bool State25(FsmContext ctx)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static bool State26(FsmContext ctx)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static bool State27(FsmContext ctx)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static bool State28(FsmContext ctx)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private bool GetChar()
  {
    if ((this.input_char = this.NextChar()) != -1)
      return true;
    this.end_of_input = true;
    return false;
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private int NextChar()
  {
    if (this.input_buffer == 0)
      return this.reader.Read();
    int inputBuffer = this.input_buffer;
    this.input_buffer = 0;
    return inputBuffer;
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public bool NextToken()
  {
    this.fsm_context.Return = false;
    for (; Lexer.fsm_handler_table[this.state - 1](this.fsm_context); this.state = this.fsm_context.NextState)
    {
      if (this.end_of_input)
        return false;
      if (this.fsm_context.Return)
      {
        this.string_value = this.string_buffer.ToString();
        this.string_buffer.Remove(0, this.string_buffer.Length);
        this.token = Lexer.fsm_return_table[this.state - 1];
        if (this.token == 65542)
          this.token = this.input_char;
        this.state = this.fsm_context.NextState;
        return true;
      }
    }
    throw new JsonException(this.input_char);
  }

  private void UngetChar()
  {
    this.input_buffer = this.input_char;
  }

  private delegate bool StateHandler(FsmContext ctx);
}
