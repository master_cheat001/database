﻿// Decompiled with JetBrains decompiler
// Type: BuglyAgent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using UnityEngine;

public sealed class BuglyAgent
{
  private static LogSeverity _autoReportLogLevel = LogSeverity.LogError;
  private static int _crashReporterType = 1;
  private static readonly int EXCEPTION_TYPE_UNCAUGHT = 1;
  private static readonly int EXCEPTION_TYPE_CAUGHT = 2;
  private static readonly string _pluginVersion = "1.5.1";
  private static bool _isInitialized;
  private static bool _debugMode;
  private static bool _autoQuitApplicationAfterReport;
  private static Func<Dictionary<string, string>> _LogCallbackExtrasHandler;
  private static bool _uncaughtAutoReportOnce;

  public static void ConfigCrashReporter(int type, int logLevel)
  {
    BuglyAgent._SetCrashReporterType(type);
    BuglyAgent._SetCrashReporterLogLevel(logLevel);
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void InitWithAppId(string appId)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void EnableExceptionHandler()
  {
    if (BuglyAgent.IsInitialized)
    {
      BuglyAgent.DebugLog((string) null, "BuglyAgent has already been initialized.");
    }
    else
    {
      BuglyAgent.DebugLog((string) null, "Only enable the exception handler, please make sure you has initialized the sdk in the native code in associated Android or iOS project.");
      BuglyAgent._RegisterExceptionHandler();
    }
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void RegisterLogCallback(BuglyAgent.LogCallbackDelegate handler)
  {
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void SetLogCallbackExtrasHandler(Func<Dictionary<string, string>> handler)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void ReportException(Exception e, string message)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void ReportException(string name, string message, string stackTrace)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void UnregisterLogCallback(BuglyAgent.LogCallbackDelegate handler)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void SetUserId(string userId)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void SetScene(int sceneId)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void AddSceneData(string key, string value)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void ConfigDebugMode(bool enable)
  {
  }

  public static void ConfigAutoQuitApplication(bool autoQuit)
  {
    BuglyAgent._autoQuitApplicationAfterReport = autoQuit;
  }

  public static void ConfigAutoReportLogLevel(LogSeverity level)
  {
    BuglyAgent._autoReportLogLevel = level;
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void ConfigDefault(string channel, string version, string user, long delay)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void DebugLog(string tag, string format, params object[] args)
  {
    if (!BuglyAgent._debugMode || string.IsNullOrEmpty(format))
      return;
    Console.WriteLine("[BuglyAgent] <Debug> - {0} : {1}", (object) tag, (object) string.Format(format, args));
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void PrintLog(LogSeverity level, string format, params object[] args)
  {
    // ISSUE: unable to decompile the method.
  }

  private static void InitBuglyAgent(string appId)
  {
  }

  private static void ConfigDefaultBeforeInit(
    string channel,
    string version,
    string user,
    long delay)
  {
  }

  private static void EnableDebugMode(bool enable)
  {
  }

  private static void SetUserInfo(string userInfo)
  {
  }

  private static void ReportException(
    int type,
    string name,
    string message,
    string stackTrace,
    bool quitProgram)
  {
  }

  private static void SetCurrentScene(int sceneId)
  {
  }

  private static void AddKeyAndValueInScene(string key, string value)
  {
  }

  private static void AddExtraDataWithException(string key, string value)
  {
  }

  private static void LogRecord(LogSeverity level, string message)
  {
  }

  private static void SetUnityVersion()
  {
  }

  private static event BuglyAgent.LogCallbackDelegate _LogCallbackEventHandler
  {
    [MethodImpl((MethodImplOptions) 32768)] add
    {
      // ISSUE: unable to decompile the method.
    }
    [MethodImpl((MethodImplOptions) 32768)] remove
    {
      // ISSUE: unable to decompile the method.
    }
  }

  public static string PluginVersion
  {
    get
    {
      return BuglyAgent._pluginVersion;
    }
  }

  public static bool IsInitialized
  {
    get
    {
      return BuglyAgent._isInitialized;
    }
  }

  public static bool AutoQuitApplicationAfterReport
  {
    get
    {
      return BuglyAgent._autoQuitApplicationAfterReport;
    }
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static void _SetCrashReporterType(int type)
  {
  }

  private static void _SetCrashReporterLogLevel(int logLevel)
  {
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static void _RegisterExceptionHandler()
  {
    try
    {
      // ISSUE: reference to a compiler-generated field
      if (BuglyAgent.\u003C\u003Ef__mg\u0024cache0 == null)
      {
        // ISSUE: reference to a compiler-generated field
        BuglyAgent.\u003C\u003Ef__mg\u0024cache0 = new Application.LogCallback(BuglyAgent._OnLogCallbackHandler);
      }
      // ISSUE: reference to a compiler-generated field
      Application.RegisterLogCallback(BuglyAgent.\u003C\u003Ef__mg\u0024cache0);
      AppDomain currentDomain = AppDomain.CurrentDomain;
      // ISSUE: reference to a compiler-generated field
      if (BuglyAgent.\u003C\u003Ef__mg\u0024cache1 == null)
      {
        // ISSUE: reference to a compiler-generated field
        BuglyAgent.\u003C\u003Ef__mg\u0024cache1 = new UnhandledExceptionEventHandler(BuglyAgent._OnUncaughtExceptionHandler);
      }
      // ISSUE: reference to a compiler-generated field
      UnhandledExceptionEventHandler fMgCache1 = BuglyAgent.\u003C\u003Ef__mg\u0024cache1;
      currentDomain.UnhandledException += fMgCache1;
      BuglyAgent._isInitialized = true;
      BuglyAgent.DebugLog((string) null, "Register the log callback in Unity {0}", (object) Application.unityVersion);
    }
    catch
    {
    }
    BuglyAgent.SetUnityVersion();
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static void _UnregisterExceptionHandler()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static void _OnLogCallbackHandler(string condition, string stackTrace, LogType type)
  {
    if (BuglyAgent._LogCallbackEventHandler != null)
      BuglyAgent._LogCallbackEventHandler(condition, stackTrace, type);
    if (!BuglyAgent.IsInitialized || !string.IsNullOrEmpty(condition) && condition.Contains("[BuglyAgent] <Log>") || BuglyAgent._uncaughtAutoReportOnce)
      return;
    LogSeverity logLevel = LogSeverity.Log;
    switch (type)
    {
      case LogType.Error:
        logLevel = LogSeverity.LogError;
        break;
      case LogType.Assert:
        logLevel = LogSeverity.LogAssert;
        break;
      case LogType.Warning:
        logLevel = LogSeverity.LogWarning;
        break;
      case LogType.Log:
        logLevel = LogSeverity.LogDebug;
        break;
      case LogType.Exception:
        logLevel = LogSeverity.LogException;
        break;
    }
    if (logLevel == LogSeverity.Log)
      return;
    BuglyAgent._HandleException(logLevel, (string) null, condition, stackTrace, true);
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static void _OnUncaughtExceptionHandler(object sender, UnhandledExceptionEventArgs args)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static void _HandleException(Exception e, string message, bool uncaught)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static void _reportException(
    bool uncaught,
    string name,
    string reason,
    string stackTrace)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static void _HandleException(
    LogSeverity logLevel,
    string name,
    string message,
    string stackTrace,
    bool uncaught)
  {
    if (!BuglyAgent.IsInitialized)
    {
      BuglyAgent.DebugLog((string) null, "It has not been initialized.");
    }
    else
    {
      if (logLevel == LogSeverity.Log)
        return;
      if (uncaught && logLevel < BuglyAgent._autoReportLogLevel)
      {
        BuglyAgent.DebugLog((string) null, "Not report exception for level {0}", (object) logLevel.ToString());
      }
      else
      {
        string name1 = (string) null;
        string reason = (string) null;
        if (!string.IsNullOrEmpty(message))
        {
          try
          {
            if (logLevel == LogSeverity.LogException && message.Contains("Exception"))
            {
              Match match = new Regex("^(?<errorType>\\S+):\\s*(?<errorMessage>.*)", RegexOptions.Singleline).Match(message);
              if (match.Success)
              {
                name1 = match.Groups["errorType"].Value.Trim();
                reason = match.Groups["errorMessage"].Value.Trim();
              }
            }
            else if (logLevel == LogSeverity.LogError)
            {
              if (message.StartsWith("Unhandled Exception:"))
              {
                Match match = new Regex("^Unhandled\\s+Exception:\\s*(?<exceptionName>\\S+):\\s*(?<exceptionDetail>.*)", RegexOptions.Singleline).Match(message);
                if (match.Success)
                {
                  string str1 = match.Groups["exceptionName"].Value.Trim();
                  string str2 = match.Groups["exceptionDetail"].Value.Trim();
                  int num = str1.LastIndexOf(".");
                  name1 = num <= 0 || num == str1.Length ? str1 : str1.Substring(num + 1);
                  int length1 = str2.IndexOf(" at ");
                  if (length1 > 0)
                  {
                    reason = str2.Substring(0, length1);
                    string str3 = str2.Substring(length1 + 3).Replace(" at ", "\n").Replace("in <filename unknown>:0", string.Empty).Replace("[0x00000]", string.Empty);
                    stackTrace = string.Format("{0}\n{1}", (object) stackTrace, (object) str3.Trim());
                  }
                  else
                    reason = str2;
                  if (name1.Equals("LuaScriptException"))
                  {
                    if (str2.Contains(".lua"))
                    {
                      if (str2.Contains("stack traceback:"))
                      {
                        int length2 = str2.IndexOf("stack traceback:");
                        if (length2 > 0)
                        {
                          reason = str2.Substring(0, length2);
                          string str3 = str2.Substring(length2 + 16).Replace(" [", " \n[");
                          stackTrace = string.Format("{0}\n{1}", (object) stackTrace, (object) str3.Trim());
                        }
                      }
                    }
                  }
                }
              }
            }
          }
          catch
          {
          }
          if (string.IsNullOrEmpty(reason))
            reason = message;
        }
        if (string.IsNullOrEmpty(name))
        {
          if (string.IsNullOrEmpty(name1))
            name1 = string.Format("Unity{0}", (object) logLevel.ToString());
        }
        else
          name1 = name;
        BuglyAgent._reportException(uncaught, name1, reason, stackTrace);
      }
    }
  }

  [MethodImpl((MethodImplOptions) 32768)]
  static BuglyAgent()
  {
  }

  public delegate void LogCallbackDelegate(string condition, string stackTrace, LogType type);
}
