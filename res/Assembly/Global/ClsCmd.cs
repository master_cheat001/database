﻿// Decompiled with JetBrains decompiler
// Type: ClsCmd
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

public class ClsCmd : IDebugCmd
{
  private const string _NAME = "cls";
  private const string _DESC = "cls: clear console screen.";

  public void Execute(string strParams)
  {
    DebugConsoleMode.instance.ClearLog();
  }

  public string GetHelpDesc()
  {
    return "cls: clear console screen.";
  }

  public string GetName()
  {
    return "cls";
  }
}
