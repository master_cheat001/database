﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ServerFramework.Protocol.LoginByAuthTokenAck
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ServerFramework.Protocol
{
  [ProtoContract(Name = "LoginByAuthTokenAck")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class LoginByAuthTokenAck : IExtensible
  {
    private int _Result;
    private bool _NeedRedirect;
    private string _SessionToken;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public LoginByAuthTokenAck()
    {
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Result")]
    public int Result
    {
      get
      {
        return this._Result;
      }
      set
      {
        this._Result = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = true, Name = "NeedRedirect")]
    public bool NeedRedirect
    {
      get
      {
        return this._NeedRedirect;
      }
      set
      {
        this._NeedRedirect = value;
      }
    }

    [DefaultValue("")]
    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = false, Name = "SessionToken")]
    public string SessionToken
    {
      get
      {
        return this._SessionToken;
      }
      set
      {
        this._SessionToken = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
