﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.GuildMassiveCombatInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class GuildMassiveCombatInfo
  {
    public List<GuildMassiveCombatStronghold> Strongholds;
    public List<GuildMassiveCombatMemberInfo> Members;
    public DateTime CreateTime;
    public DateTime FinishTime;
    public DateTime RewardSendTime;

    [MethodImpl((MethodImplOptions) 32768)]
    public GuildMassiveCombatInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    public ulong InstanceId { get; set; }

    public int Difficulty { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsRewardSent(DateTime now)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GuildMassiveCombatStronghold GetStronghold(int LevelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsConquered(DateTime now)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
