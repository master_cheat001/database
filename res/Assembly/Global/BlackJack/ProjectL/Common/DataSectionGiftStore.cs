﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionGiftStore
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class DataSectionGiftStore : DataSection
  {
    private List<GiftStoreItem> m_localBoughtItems;
    private Dictionary<int, string> m_firstboughtItemsRecords;
    protected List<GiftStoreOperationalGoods> m_operationalGoodsList;
    private Dictionary<int, DateTime> m_banBuyingGoodsList;
    private Dictionary<string, OrderReward> m_orderRerads;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionGiftStore()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void ClearInitedData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GiftStoreItem FindLocalBoughtItemById(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddBoughtItem(GiftStoreItem item)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddFirstBuyGoodsRecord(int goodsId, string goodsRegisterId)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool HasBought(int goodsId)
    {
      return this.m_firstboughtItemsRecords.ContainsKey(goodsId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetFirstBoughtItemRegisterId(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitBoughtItem(GiftStoreItem storeItem)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBoughtItems(List<GiftStoreItem> storeItems)
    {
    }

    public void InitFirstBuyGoodsRecord(int goodsId, string registerId)
    {
      this.m_firstboughtItemsRecords.Add(goodsId, registerId);
    }

    public List<GiftStoreItem> GetLocalBoughtItems()
    {
      return this.m_localBoughtItems;
    }

    public Dictionary<int, string> GetFirstBoughtRecords()
    {
      return this.m_firstboughtItemsRecords;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsGoodsOnBanBuyingPeriod(int goodsId, DateTime currentTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBanBuyingGoodsTime(int goodsId, DateTime expiredTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitmBanBuyingGoodsList(Dictionary<int, DateTime> goodsList)
    {
      // ISSUE: unable to decompile the method.
    }

    public Dictionary<int, DateTime> BanBuyingGoodsList
    {
      get
      {
        return this.m_banBuyingGoodsList;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GiftStoreOperationalGoods FindGiftStoreOperationalGoods(
      int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    public void ClearGiftStoreOperationalGoods()
    {
      this.m_operationalGoodsList.Clear();
    }

    public void InitOperationalGoodsList(GiftStoreOperationalGoods goods)
    {
      this.m_operationalGoodsList.Add(goods);
    }

    public List<GiftStoreOperationalGoods> GetOperationalGoodsList()
    {
      return this.m_operationalGoodsList;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitOrderReward(OrderReward orderReward)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveOrderReward(string orderId)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddOrderReward(string orderId, OrderReward reward)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public OrderReward FindOrderReward(string orderId)
    {
      // ISSUE: unable to decompile the method.
    }

    public Dictionary<string, OrderReward> GetAllOrderRewards()
    {
      return this.m_orderRerads;
    }
  }
}
