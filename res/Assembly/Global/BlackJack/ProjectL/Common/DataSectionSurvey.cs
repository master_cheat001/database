﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionSurvey
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class DataSectionSurvey : DataSection
  {
    private Survey m_currentSurvey;

    [MethodImpl((MethodImplOptions) 32768)]
    public SurveyStatus GetCurrentSurveyStatus()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSurveyStatus(SurveyStatus status)
    {
    }

    public void SetCurrentSurvey(Survey survey)
    {
      this.m_currentSurvey = survey;
      this.SetDirty(true);
    }

    public void InitCurrentSurvey(Survey survey)
    {
      this.m_currentSurvey = survey;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    public Survey GetCurrentSurvey()
    {
      return this.m_currentSurvey;
    }
  }
}
