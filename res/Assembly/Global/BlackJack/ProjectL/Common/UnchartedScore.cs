﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.UnchartedScore
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Protocol;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class UnchartedScore
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public UnchartedScore(int id)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<ProUnchartedScore> ProUnchartedScores2PbProUnchartedScores(
      List<UnchartedScore> unchartedScores)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static ProUnchartedScore UnchartedScore2PbUnchartedScore(
      UnchartedScore unchartedScore)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<UnchartedScore> PbUnchartedScores(
      List<ProUnchartedScore> pbUnchartedScores)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static UnchartedScore PbUnchartedScoreToUnchartedScore(
      ProUnchartedScore pbUnchartedScore)
    {
      // ISSUE: unable to decompile the method.
    }

    public ulong ActivityInstanceId { get; set; }

    public int Id { get; set; }

    public int BonusCount { get; set; }

    public int Score { get; set; }

    public HashSet<int> RewardGains { get; set; }

    public HashSet<int> SocreLevelCompleteIds { get; set; }

    public HashSet<int> ChallengeLevelCompleteIds { get; set; }

    public ConfigDataUnchartedScoreInfo Config { get; set; }
  }
}
