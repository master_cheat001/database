﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.RandomStoreComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using SLua;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class RandomStoreComponentCommon : IComponentBase
  {
    protected IConfigDataLoader m_configDataLoader;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected BagComponentCommon m_bag;
    protected DataSectionRandomStore m_randomStoreDS;

    [MethodImpl((MethodImplOptions) 32768)]
    public RandomStoreComponentCommon()
    {
    }

    public string GetName()
    {
      return "RandomStore";
    }

    public virtual void Init()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    public virtual void DeInit()
    {
    }

    public virtual void Tick(uint deltaMillisecond)
    {
    }

    public virtual bool Serialize<T>(T dest)
    {
      return false;
    }

    public virtual void DeSerialize<T>(T source)
    {
    }

    public virtual void PostDeSerialize()
    {
    }

    public IComponentOwner Owner { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnFlushManualFlushNums()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanAutoFlushStore(RandomStore store)
    {
      // ISSUE: unable to decompile the method.
    }

    protected bool IsBoughtStoreItem(RandomStoreItem storeItem)
    {
      return storeItem.Bought;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected int CanManualFlushStore(RandomStore store, ConfigDataRandomStoreInfo storeInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int CanBuyRandomStoreItem(int storeId, int index, int selectedIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnBuyStoreItem(int storeId, int goodsId)
    {
    }

    public event Action<int, int> BuyStoreItemEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }
  }
}
