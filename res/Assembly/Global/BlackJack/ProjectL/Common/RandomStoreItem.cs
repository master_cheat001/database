﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.RandomStoreItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class RandomStoreItem
  {
    public int Id { set; get; }

    public bool Bought { set; get; }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProRandomStoreItem StoreItemToPBStoreItem(
      RandomStoreItem storeItem)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static RandomStoreItem PBStoreItemToStoreItem(
      ProRandomStoreItem pbStoreItem)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
