﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.TreasureMapComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Common
{
  [HotFix]
  public class TreasureMapComponentCommon : IComponentBase
  {
    protected IConfigDataLoader m_configDataLoader;
    protected DataSectionTreasureMap m_treasureMapDS;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected BattleComponentCommon m_battle;
    protected HeroComponentCommon m_hero;
    protected BagComponentCommon m_bag;
    [DoNotToLua]
    private TreasureMapComponentCommon.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_GetName_hotfix;
    private LuaFunction m_Init_hotfix;
    private LuaFunction m_PostInit_hotfix;
    private LuaFunction m_DeInit_hotfix;
    private LuaFunction m_TickUInt32_hotfix;
    private LuaFunction m_SerializeT_hotfix;
    private LuaFunction m_DeSerializeT_hotfix;
    private LuaFunction m_PostDeSerialize_hotfix;
    private LuaFunction m_get_Owner_hotfix;
    private LuaFunction m_set_OwnerIComponentOwner_hotfix;
    private LuaFunction m_IsLevelFinishedInt32_hotfix;
    private LuaFunction m_AttackTreasureLevelInt32_hotfix;
    private LuaFunction m_IsGameFunctionOpened_hotfix;
    private LuaFunction m_CanAttackTreasureLevelInt32_hotfix;
    private LuaFunction m_CanAttackLevelByEnergyAndSoOnConfigDataTreasureLevelInfo_hotfix;
    private LuaFunction m_SetSuccessTreasureMapLevelConfigDataTreasureLevelInfoList`1_hotfix;
    private LuaFunction m_OnFinishedLevelInt32_hotfix;
    private LuaFunction m_add_CompleteTreasureMapMissionEventAction`3_hotfix;
    private LuaFunction m_remove_CompleteTreasureMapMissionEventAction`3_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public TreasureMapComponentCommon()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Tick(uint deltaMillisecond)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool Serialize<T>(T dest)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeSerialize<T>(T source)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostDeSerialize()
    {
      // ISSUE: unable to decompile the method.
    }

    public IComponentOwner Owner
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsLevelFinished(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AttackTreasureLevel(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsGameFunctionOpened()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int CanAttackTreasureLevel(int leveId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int CanAttackLevelByEnergyAndSoOn(ConfigDataTreasureLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void SetSuccessTreasureMapLevel(
      ConfigDataTreasureLevelInfo levelInfo,
      List<int> battleTreasures)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnFinishedLevel(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<BattleType, int, List<int>> CompleteTreasureMapMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public TreasureMapComponentCommon.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_CompleteTreasureMapMissionEvent(
      BattleType arg1,
      int arg2,
      List<int> arg3)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_CompleteTreasureMapMissionEvent(
      BattleType arg1,
      int arg2,
      List<int> arg3)
    {
      this.CompleteTreasureMapMissionEvent = (Action<BattleType, int, List<int>>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private TreasureMapComponentCommon m_owner;

      public LuaExportHelper(TreasureMapComponentCommon owner)
      {
        this.m_owner = owner;
      }

      public void __callDele_CompleteTreasureMapMissionEvent(
        BattleType arg1,
        int arg2,
        List<int> arg3)
      {
        this.m_owner.__callDele_CompleteTreasureMapMissionEvent(arg1, arg2, arg3);
      }

      public void __clearDele_CompleteTreasureMapMissionEvent(
        BattleType arg1,
        int arg2,
        List<int> arg3)
      {
        this.m_owner.__clearDele_CompleteTreasureMapMissionEvent(arg1, arg2, arg3);
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public DataSectionTreasureMap m_treasureMapDS
      {
        get
        {
          return this.m_owner.m_treasureMapDS;
        }
        set
        {
          this.m_owner.m_treasureMapDS = value;
        }
      }

      public PlayerBasicInfoComponentCommon m_basicInfo
      {
        get
        {
          return this.m_owner.m_basicInfo;
        }
        set
        {
          this.m_owner.m_basicInfo = value;
        }
      }

      public BattleComponentCommon m_battle
      {
        get
        {
          return this.m_owner.m_battle;
        }
        set
        {
          this.m_owner.m_battle = value;
        }
      }

      public HeroComponentCommon m_hero
      {
        get
        {
          return this.m_owner.m_hero;
        }
        set
        {
          this.m_owner.m_hero = value;
        }
      }

      public BagComponentCommon m_bag
      {
        get
        {
          return this.m_owner.m_bag;
        }
        set
        {
          this.m_owner.m_bag = value;
        }
      }

      public bool IsGameFunctionOpened()
      {
        return this.m_owner.IsGameFunctionOpened();
      }

      public int CanAttackLevelByEnergyAndSoOn(ConfigDataTreasureLevelInfo levelInfo)
      {
        return this.m_owner.CanAttackLevelByEnergyAndSoOn(levelInfo);
      }

      public void SetSuccessTreasureMapLevel(
        ConfigDataTreasureLevelInfo levelInfo,
        List<int> battleTreasures)
      {
        this.m_owner.SetSuccessTreasureMapLevel(levelInfo, battleTreasures);
      }

      public void OnFinishedLevel(int levelId)
      {
        this.m_owner.OnFinishedLevel(levelId);
      }
    }
  }
}
