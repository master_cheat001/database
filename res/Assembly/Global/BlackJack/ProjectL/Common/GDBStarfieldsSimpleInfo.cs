﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.GDBStarfieldsSimpleInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Common
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "GDBStarfieldsSimpleInfo")]
  [Serializable]
  public class GDBStarfieldsSimpleInfo : IExtensible
  {
    private int _Id;
    private string _name;
    private float _gLocationX;
    private float _gLocationZ;
    private bool _isNeedLocalization;
    private string _localizationKey;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public GDBStarfieldsSimpleInfo()
    {
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Id")]
    public int Id
    {
      get
      {
        return this._Id;
      }
      set
      {
        this._Id = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = true, Name = "name")]
    public string Name
    {
      get
      {
        return this._name;
      }
      set
      {
        this._name = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.FixedSize, IsRequired = true, Name = "gLocationX")]
    public float GLocationX
    {
      get
      {
        return this._gLocationX;
      }
      set
      {
        this._gLocationX = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.FixedSize, IsRequired = true, Name = "gLocationZ")]
    public float GLocationZ
    {
      get
      {
        return this._gLocationZ;
      }
      set
      {
        this._gLocationZ = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "isNeedLocalization")]
    public bool IsNeedLocalization
    {
      get
      {
        return this._isNeedLocalization;
      }
      set
      {
        this._isNeedLocalization = value;
      }
    }

    [DefaultValue("")]
    [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = false, Name = "localizationKey")]
    public string LocalizationKey
    {
      get
      {
        return this._localizationKey;
      }
      set
      {
        this._localizationKey = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
