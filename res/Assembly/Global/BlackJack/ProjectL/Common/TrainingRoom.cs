﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.TrainingRoom
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class TrainingRoom
  {
    public List<TrainingCourse> Courses;
    private IConfigDataLoader _ConfigDataLoader;

    [MethodImpl((MethodImplOptions) 32768)]
    public TrainingRoom()
    {
    }

    public int ConfigId { get; set; }

    public int AnikiGymLevelRequired
    {
      get
      {
        return this.Config.LevelToUnlock;
      }
    }

    private List<int> TotalLevelupExpSteps
    {
      get
      {
        return this.Config.m_totalLevelupExpSteps;
      }
    }

    public TrainingGround WhichTrainingGround { get; set; }

    public ConfigDataTrainingRoomInfo Config
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public IConfigDataLoader ConfigDataLoader
    {
      get
      {
        return this._ConfigDataLoader;
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Level
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int TotalExp
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Exp
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool Locked
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ReloadConfigData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateInstantiatedData(List<TrainingTech> AvailableTechs)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
