﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.RealTimePVPComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Common
{
  [HotFix]
  public class RealTimePVPComponentCommon : IComponentBase
  {
    private IConfigDataLoader _configDataLoader;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected BagComponentCommon m_bag;
    protected BattleComponentCommon m_battle;
    public DataSectionRealTimePVP m_realTimePVP;
    [DoNotToLua]
    private RealTimePVPComponentCommon.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_GetName_hotfix;
    private LuaFunction m_Init_hotfix;
    private LuaFunction m_PostInit_hotfix;
    private LuaFunction m_DeInit_hotfix;
    private LuaFunction m_TickUInt32_hotfix;
    private LuaFunction m_SerializeT_hotfix;
    private LuaFunction m_DeSerializeT_hotfix;
    private LuaFunction m_PostDeSerialize_hotfix;
    private LuaFunction m_get_Owner_hotfix;
    private LuaFunction m_set_OwnerIComponentOwner_hotfix;
    private LuaFunction m_RealTimeArenaDanUpdateInt32_hotfix;
    private LuaFunction m_RealTimeArenaBattleFinishBattleModeBooleanList`1_hotfix;
    private LuaFunction m_RealTimeArenaBattleStartBattleMode_hotfix;
    private LuaFunction m_IsRealTimePVPUnlocked_hotfix;
    private LuaFunction m_IsRealTimePVPArenaAvailableBattleMode_hotfix;
    private LuaFunction m_OnFlushRealTimePVP_hotfix;
    private LuaFunction m_StartNewSeason_hotfix;
    private LuaFunction m_IsPromotion_hotfix;
    private LuaFunction m_GetPromotionBattleReports_hotfix;
    private LuaFunction m_GetBattleReports_hotfix;
    private LuaFunction m_StartPromotion_hotfix;
    private LuaFunction m_EndPromotion_hotfix;
    private LuaFunction m_SavePromotionReportRealTimePVPBattleReport_hotfix;
    private LuaFunction m_SaveReportRealTimePVPBattleReport_hotfix;
    private LuaFunction m_CheckAcquireWinsBonusInt32_hotfix;
    private LuaFunction m_AcquireWinsBonusInt32_hotfix;
    private LuaFunction m_GetConsecutiveWinsBattleMode_hotfix;
    private LuaFunction m_GetConsecutiveLossesBattleMode_hotfix;
    private LuaFunction m_GetLadderCareerMatchStats_hotfix;
    private LuaFunction m_TryGetBotParamsForNoviceBattleModeInt32Int32_Int32_Int32_Int32__hotfix;
    private LuaFunction m_TryGetBotParamsForLoserBattleModeInt32Int32Int32_Int32_Int32_Int32__hotfix;
    private LuaFunction m_get_m_configDataLoader_hotfix;
    private LuaFunction m_set_m_configDataLoaderIConfigDataLoader_hotfix;
    private LuaFunction m_add_RealTimeArenaBattleStartMissionEventAction`1_hotfix;
    private LuaFunction m_remove_RealTimeArenaBattleStartMissionEventAction`1_hotfix;
    private LuaFunction m_add_RealTimeArenaBattleFinishMissionEventAction`2_hotfix;
    private LuaFunction m_remove_RealTimeArenaBattleFinishMissionEventAction`2_hotfix;
    private LuaFunction m_add_RealTimeArenaDanUpdateMissionEventAction`1_hotfix;
    private LuaFunction m_remove_RealTimeArenaDanUpdateMissionEventAction`1_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public RealTimePVPComponentCommon()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Tick(uint deltaMillisecond)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool Serialize<T>(T dest)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeSerialize<T>(T source)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostDeSerialize()
    {
      // ISSUE: unable to decompile the method.
    }

    public IComponentOwner Owner
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RealTimeArenaDanUpdate(int dan)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RealTimeArenaBattleFinish(BattleMode mode, bool win, List<int> heroes)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RealTimeArenaBattleStart(BattleMode mode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int IsRealTimePVPUnlocked()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int IsRealTimePVPArenaAvailable(BattleMode Mode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnFlushRealTimePVP()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void StartNewSeason()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPromotion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public LinkedList<RealTimePVPBattleReport> GetPromotionBattleReports()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public LinkedList<RealTimePVPBattleReport> GetBattleReports()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartPromotion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void EndPromotion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SavePromotionReport(RealTimePVPBattleReport Report)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SaveReport(RealTimePVPBattleReport Report)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CheckAcquireWinsBonus(int BonusId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int AcquireWinsBonus(int BonusId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetConsecutiveWins(BattleMode Mode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetConsecutiveLosses(BattleMode Mode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RealTimePVPMatchStats GetLadderCareerMatchStats()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool TryGetBotParamsForNovice(
      BattleMode Mode,
      int MyScore,
      out int LevelMin,
      out int LevelMax,
      out int ScoreMin,
      out int ScoreMax)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool TryGetBotParamsForLoser(
      BattleMode Mode,
      int MyScore,
      int Dan,
      out int LevelMin,
      out int LevelMax,
      out int ScoreMin,
      out int ScoreMax)
    {
      // ISSUE: unable to decompile the method.
    }

    protected IConfigDataLoader m_configDataLoader
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<BattleMode> RealTimeArenaBattleStartMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<BattleMode, bool> RealTimeArenaBattleFinishMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> RealTimeArenaDanUpdateMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public RealTimePVPComponentCommon.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_RealTimeArenaBattleStartMissionEvent(BattleMode obj)
    {
    }

    private void __clearDele_RealTimeArenaBattleStartMissionEvent(BattleMode obj)
    {
      this.RealTimeArenaBattleStartMissionEvent = (Action<BattleMode>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_RealTimeArenaBattleFinishMissionEvent(BattleMode arg1, bool arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_RealTimeArenaBattleFinishMissionEvent(BattleMode arg1, bool arg2)
    {
      this.RealTimeArenaBattleFinishMissionEvent = (Action<BattleMode, bool>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_RealTimeArenaDanUpdateMissionEvent(int obj)
    {
    }

    private void __clearDele_RealTimeArenaDanUpdateMissionEvent(int obj)
    {
      this.RealTimeArenaDanUpdateMissionEvent = (Action<int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private RealTimePVPComponentCommon m_owner;

      public LuaExportHelper(RealTimePVPComponentCommon owner)
      {
        this.m_owner = owner;
      }

      public void __callDele_RealTimeArenaBattleStartMissionEvent(BattleMode obj)
      {
        this.m_owner.__callDele_RealTimeArenaBattleStartMissionEvent(obj);
      }

      public void __clearDele_RealTimeArenaBattleStartMissionEvent(BattleMode obj)
      {
        this.m_owner.__clearDele_RealTimeArenaBattleStartMissionEvent(obj);
      }

      public void __callDele_RealTimeArenaBattleFinishMissionEvent(BattleMode arg1, bool arg2)
      {
        this.m_owner.__callDele_RealTimeArenaBattleFinishMissionEvent(arg1, arg2);
      }

      public void __clearDele_RealTimeArenaBattleFinishMissionEvent(BattleMode arg1, bool arg2)
      {
        this.m_owner.__clearDele_RealTimeArenaBattleFinishMissionEvent(arg1, arg2);
      }

      public void __callDele_RealTimeArenaDanUpdateMissionEvent(int obj)
      {
        this.m_owner.__callDele_RealTimeArenaDanUpdateMissionEvent(obj);
      }

      public void __clearDele_RealTimeArenaDanUpdateMissionEvent(int obj)
      {
        this.m_owner.__clearDele_RealTimeArenaDanUpdateMissionEvent(obj);
      }

      public IConfigDataLoader _configDataLoader
      {
        get
        {
          return this.m_owner._configDataLoader;
        }
        set
        {
          this.m_owner._configDataLoader = value;
        }
      }

      public PlayerBasicInfoComponentCommon m_basicInfo
      {
        get
        {
          return this.m_owner.m_basicInfo;
        }
        set
        {
          this.m_owner.m_basicInfo = value;
        }
      }

      public BagComponentCommon m_bag
      {
        get
        {
          return this.m_owner.m_bag;
        }
        set
        {
          this.m_owner.m_bag = value;
        }
      }

      public BattleComponentCommon m_battle
      {
        get
        {
          return this.m_owner.m_battle;
        }
        set
        {
          this.m_owner.m_battle = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public void OnFlushRealTimePVP()
      {
        this.m_owner.OnFlushRealTimePVP();
      }

      public void StartNewSeason()
      {
        this.m_owner.StartNewSeason();
      }
    }
  }
}
