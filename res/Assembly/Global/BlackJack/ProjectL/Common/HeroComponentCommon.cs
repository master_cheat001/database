﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.HeroComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Common
{
  [HotFix]
  public class HeroComponentCommon : IComponentBase
  {
    protected IConfigDataLoader m_configDataLoader;
    protected BagComponentCommon m_bag;
    protected RiftComponentCommon m_rift;
    protected MissionComponentCommon m_mission;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected BattleComponentCommon m_battle;
    protected ResourceComponentCommon m_resource;
    protected GuildComponentCommon m_guild;
    protected TrainingGroundCompomentCommon m_trainingGround;
    protected DataSectionHero m_heroDS;
    protected Dictionary<ulong, int> m_wearedEquipmentHeroDict;
    protected HashSet<int> m_soliders;
    protected HashSet<int> m_skills;
    protected Dictionary<int, DateTime> m_atuoEquipmentTouch;
    public Action<Hero> HeroFavorabilityLevelupMissionEvent;
    public Action<Hero, int> HeroFetterLevelupMissionEvent;
    [DoNotToLua]
    private HeroComponentCommon.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_GetName_hotfix;
    private LuaFunction m_Init_hotfix;
    private LuaFunction m_PostInit_hotfix;
    private LuaFunction m_DeInit_hotfix;
    private LuaFunction m_TickUInt32_hotfix;
    private LuaFunction m_SerializeT_hotfix;
    private LuaFunction m_DeSerializeT_hotfix;
    private LuaFunction m_PostDeSerialize_hotfix;
    private LuaFunction m_get_Owner_hotfix;
    private LuaFunction m_set_OwnerIComponentOwner_hotfix;
    private LuaFunction m_GetProtagonistID_hotfix;
    private LuaFunction m_IsProtagonistHeroInt32_hotfix;
    private LuaFunction m_IsProtagonistExist_hotfix;
    private LuaFunction m_SetProtagonistInt32_hotfix;
    private LuaFunction m_GmStrengthenAllHeroes_hotfix;
    private LuaFunction m_GetAllStarLvlMaxHeroes_hotfix;
    private LuaFunction m_GetAllStarLvlMaxHeroFragments_hotfix;
    private LuaFunction m_InitHeroDataByCaculateHero_hotfix;
    private LuaFunction m_InitHeroJobsHero_hotfix;
    private LuaFunction m_AddHeroDefaultInfosHeroConfigDataHeroInfo_hotfix;
    private LuaFunction m_CanHeroJobEquipSkillInt32Int32_hotfix;
    private LuaFunction m_CanSelectSkillHeroHeroList`1_hotfix;
    private LuaFunction m_SelectSkillsInt32List`1_hotfix;
    private LuaFunction m_SelectSoldierInt32Int32_hotfix;
    private LuaFunction m_CanHeroSelectSoliderHeroInt32_hotfix;
    private LuaFunction m_RemoveHeroInt32_hotfix;
    private LuaFunction m_RemoveAllHeroList`1_hotfix;
    private LuaFunction m_RemoveAllHeros_hotfix;
    private LuaFunction m_GetAllHeros_hotfix;
    private LuaFunction m_FindHeroJobHeroInt32_hotfix;
    private LuaFunction m_IsUnlockedHeroJobHeroInt32_hotfix;
    private LuaFunction m_CanGetHeroJobByRankHeroInt32_hotfix;
    private LuaFunction m_FindHeroJobInt32Int32_hotfix;
    private LuaFunction m_FindHeroInt32_hotfix;
    private LuaFunction m_AddHeroFightNumsInt32Int32_hotfix;
    private LuaFunction m_AddHeroAllNeedJobsAchievementsInt32List`1_hotfix;
    private LuaFunction m_AddHeroInt32_hotfix;
    private LuaFunction m_AddHeroInfosConfigDataHeroInfo_hotfix;
    private LuaFunction m_IsWastefulAddExpHeroInt32_hotfix;
    private LuaFunction m_IsCurrentLevelMaxHeroLevelInt32_hotfix;
    private LuaFunction m_IsFullCurrentHeroExpHero_hotfix;
    private LuaFunction m_AddHeroesExpList`1Int32_hotfix;
    private LuaFunction m_CanAddHeroExpInt32_hotfix;
    private LuaFunction m_AddHeroExpInt32Int32_hotfix;
    private LuaFunction m_OutPutHeroLevelUpOperateLogInt32Int32_hotfix;
    private LuaFunction m_AddHeroExpByUseableBagItemInt32GoodsTypeInt32Int32_hotfix;
    private LuaFunction m_ComposeHeroInt32_hotfix;
    private LuaFunction m_IsLvlMaxHeroStarHero_hotfix;
    private LuaFunction m_LevelUpHeroStarLevelInt32_hotfix;
    private LuaFunction m_OutPutHeroUpgradeOperateLogInt32Int32List`1_hotfix;
    private LuaFunction m_GetGainHeroIds_hotfix;
    private LuaFunction m_HasGainHeroInt32_hotfix;
    private LuaFunction m_IsExistSkillIdInt32_hotfix;
    private LuaFunction m_IsExistSoliderIdInt32_hotfix;
    private LuaFunction m_FindTopLevelHeroesInt32_hotfix;
    private LuaFunction m_GetActiveHeroJobRelatedIdByHeroIdInt32_hotfix;
    private LuaFunction m_GmResetHeroJobInt32_hotfix;
    private LuaFunction m_OnHeroLevelUpInt32Hero_hotfix;
    private LuaFunction m_GetAdditiveHeroAddExpInt32_hotfix;
    private LuaFunction m_GetAdditiveHeroFavourabilityAddExpInt32_hotfix;
    private LuaFunction m_AddHeroJobAchievementsAfterBattleLevelEndList`1List`1_hotfix;
    private LuaFunction m_AddHeroJobHeroConfigDataJobConnectionInfo_hotfix;
    private LuaFunction m_IsJobLevelMaxConfigDataJobConnectionInfoInt32_hotfix;
    private LuaFunction m_IsJobLevelMaxHeroJob_hotfix;
    private LuaFunction m_CanLevelUpHeroJobLevelInt32Int32_hotfix;
    private LuaFunction m_CanTransferHeroJobRankInt32Int32_hotfix;
    private LuaFunction m_CanTransferHeroJobInt32Int32_hotfix;
    private LuaFunction m_TransferHeroJobInt32Int32_hotfix;
    private LuaFunction m_OutPutHeroJobChangeOperateLogInt32Int32Int32List`1_hotfix;
    private LuaFunction m_OnTransferHeroJobHeroConfigDataJobConnectionInfoInt32_hotfix;
    private LuaFunction m_TryEquipNewSkillHeroInt32_hotfix;
    private LuaFunction m_IsNeedUnlockConfigDataJobConnectionInfo_hotfix;
    private LuaFunction m_IsNeedAddAchievementConfigDataJobConnectionInfo_hotfix;
    private LuaFunction m_UnlockedHeroJobInt32Int32_hotfix;
    private LuaFunction m_OutPutHeroJobUnlockOperateLogInt32Int32Int32List`1_hotfix;
    private LuaFunction m_CanUnlockedHeroJobHeroConfigDataJobConnectionInfo_hotfix;
    private LuaFunction m_LevelUpHeroJobLevelInt32Int32_hotfix;
    private LuaFunction m_OutPutHeroJobLevelUpOperateLogInt32Int32Int32List`1_hotfix;
    private LuaFunction m_AddSkillHeroInt32_hotfix;
    private LuaFunction m_AddSoldierHeroInt32Boolean_hotfix;
    private LuaFunction m_AutoTakeOffEquipmentsInt32_hotfix;
    private LuaFunction m_InitWearedEquipments_hotfix;
    private LuaFunction m_CanWearEquipmentInt32UInt64_hotfix;
    private LuaFunction m_CanWearEquipmentByJobAndArmyHeroBagItemBase_hotfix;
    private LuaFunction m_IsEquipmentWearedUInt64_hotfix;
    private LuaFunction m_GetWearedEquipmentHeroUInt64_hotfix;
    private LuaFunction m_TakeOffEquipmentsOrNotWhenTransferHeroJobHero_hotfix;
    private LuaFunction m_WearEquipmentInt32UInt64_hotfix;
    private LuaFunction m_WearEquipmentBySlotHeroInt32UInt64_hotfix;
    private LuaFunction m_TakeOffEquipmentBySlotHeroInt32_hotfix;
    private LuaFunction m_CanTakeOffEquipmentInt32Int32_hotfix;
    private LuaFunction m_TakeOffEquipmentInt32Int32_hotfix;
    private LuaFunction m_HasBetterEquipmentByHeroInt32_hotfix;
    private LuaFunction m_GetBestEquipmentsHero_hotfix;
    private LuaFunction m_GetTopLevelEquipmentsHeroInt32_hotfix;
    private LuaFunction m_CanAutoEquipmentByClientInt32_hotfix;
    private LuaFunction m_CanAutoEquipmentInt32_hotfix;
    private LuaFunction m_CanWearCharSkinInt32Int32_hotfix;
    private LuaFunction m_WearCharSkinInt32Int32_hotfix;
    private LuaFunction m_CanTakeOffCharSkinInt32_hotfix;
    private LuaFunction m_TakeOffCharSkinInt32_hotfix;
    private LuaFunction m_CanWearModelSkinInt32Int32Int32_hotfix;
    private LuaFunction m_WearModelSkinInt32Int32Int32_hotfix;
    private LuaFunction m_CanTakeOffModelSkinInt32Int32_hotfix;
    private LuaFunction m_TakeOffModelSkinInt32Int32_hotfix;
    private LuaFunction m_CanWearSoldierSkinInt32Int32Int32_hotfix;
    private LuaFunction m_WearSoldierSkinInt32Int32Int32Boolean_hotfix;
    private LuaFunction m_CanTakeOffSoldierSkinInt32Int32_hotfix;
    private LuaFunction m_TakeOffSoldierSkinInt32Int32Boolean_hotfix;
    private LuaFunction m_TakeOffSoldierSkinApplyToAllInt32Int32_hotfix;
    private LuaFunction m_AddHeroFavorabilityExpByUseableBagItemInt32GoodsTypeInt32Int32_hotfix;
    private LuaFunction m_AddHeroFavorabilityLevelInt32Int32_hotfix;
    private LuaFunction m_OutPutHeroFavourabilityOperateLogInt32Int32Int32Int32Int32List`1_hotfix;
    private LuaFunction m_AddHeroFavorabilityLevelHero_hotfix;
    private LuaFunction m_GenerateFavorabilityLevelUpRewardInt32Int32_hotfix;
    private LuaFunction m_IsFullFavorabilityExpHeroConfigDataHeroInformationInfo_hotfix;
    private LuaFunction m_UpdateProtagonistHeroFavorabilityLevel_hotfix;
    private LuaFunction m_CanUnlockHeroFetterInt32Int32_hotfix;
    private LuaFunction m_CanReachFetterUnlockConditionHeroFetterCompletionCondition_hotfix;
    private LuaFunction m_GmLevelUpHeroFetter2SpecificLevelInt32Int32Int32_hotfix;
    private LuaFunction m_CanLevelUpHeroFetterInt32Int32_hotfix;
    private LuaFunction m_LevelUpHeroFetterInt32Int32_hotfix;
    private LuaFunction m_LevelUpHeroFetterCallBackHeroInt32_hotfix;
    private LuaFunction m_OutPutHeroFetterOpereateLogInt32Int32Int32List`1List`1_hotfix;
    private LuaFunction m_UnlockHeroFetterInt32Int32Boolean_hotfix;
    private LuaFunction m_GenerateFetterUnlockRewardsInt32Int32_hotfix;
    private LuaFunction m_ConfessHeroInt32Boolean_hotfix;
    private LuaFunction m_GenerateConfessionRewardsInt32ConfigDataHeroConfessionInfo_hotfix;
    private LuaFunction m_CanConfessHeroInt32_hotfix;
    private LuaFunction m_GmSetHeroHeartFetterInt32Int32_hotfix;
    private LuaFunction m_CanUnlockHeroHeartFetterInt32_hotfix;
    private LuaFunction m_CanReachHeroHeartFetterUnlockConditionHeroHeroHeartFetterUnlockCondition_hotfix;
    private LuaFunction m_UnlockHeroHeartFetterInt32_hotfix;
    private LuaFunction m_OnHeroHeartFettterUnlockHeroDateTime_hotfix;
    private LuaFunction m_GenerateHeroHeartFetterUnlockRewardConfigDataHeroInfo_hotfix;
    private LuaFunction m_CanLevelUpHeroHeartFetterInt32_hotfix;
    private LuaFunction m_LevelUpHeroHeartFetterInt32_hotfix;
    private LuaFunction m_OnHeroHeartFetterLevelUpHeroConfigDataHeroHeartFetterInfo_hotfix;
    private LuaFunction m_OnHeroHeartFetterLevelMaxHeroDateTime_hotfix;
    private LuaFunction m_FlushHeroInteractNums_hotfix;
    private LuaFunction m_InitHeroInteractNums_hotfix;
    private LuaFunction m_CanFlushHeroInteractNums_hotfix;
    private LuaFunction m_IsFullHeroInteractNums_hotfix;
    private LuaFunction m_AddHeroInteractNumsInt32Boolean_hotfix;
    private LuaFunction m_IsHeroInteractNumsEqualToMax_hotfix;
    private LuaFunction m_CanInteractHeroInt32_hotfix;
    private LuaFunction m_GetHeroFavorabilityUpLevelHeroConfigDataHeroInformationInfoInt32_hotfix;
    private LuaFunction m_GetHeroInteractIdHeroConfigDataHeroInformationInfo_hotfix;
    private LuaFunction m_GetAllHeroBattlePower_hotfix;
    private LuaFunction m_GetLastAllHeroRank_hotfix;
    private LuaFunction m_GetTopHeroBattlePower_hotfix;
    private LuaFunction m_GetLastTopHeroRank_hotfix;
    private LuaFunction m_GetChampionHeroBattlePower_hotfix;
    private LuaFunction m_GetChampionHeroId_hotfix;
    private LuaFunction m_GetLastChampionHeroRank_hotfix;
    private LuaFunction m_ComputeBattlePowerHero_hotfix;
    private LuaFunction m_OnHeroBattlePowerChangeHero_hotfix;
    private LuaFunction m_CanComposeHeroInt32_hotfix;
    private LuaFunction m_CanLevelUpHeroStarLevelInt32_hotfix;
    private LuaFunction m_GetEquipedEquipments_hotfix;
    private LuaFunction m_GetUnlcokJobAchievementsConfigDataJobConnectionInfo_hotfix;
    private LuaFunction m_GetSkillEnergyFromConfigInt32_hotfix;
    private LuaFunction m_GetHeroStarLevelUpDataFromConfigInt32Int32_hotfix;
    private LuaFunction m_GetSkillCostFromConfigInt32_hotfix;
    private LuaFunction m_add_AddHeroEventAction`1_hotfix;
    private LuaFunction m_remove_AddHeroEventAction`1_hotfix;
    private LuaFunction m_add_SpecificHeroFightMissionEventAction`2_hotfix;
    private LuaFunction m_remove_SpecificHeroFightMissionEventAction`2_hotfix;
    private LuaFunction m_add_HeroNewJobTransferMissionEventAction`2_hotfix;
    private LuaFunction m_remove_HeroNewJobTransferMissionEventAction`2_hotfix;
    private LuaFunction m_add_HeroJobLevelUpMissionEventAction_hotfix;
    private LuaFunction m_remove_HeroJobLevelUpMissionEventAction_hotfix;
    private LuaFunction m_add_HeroJobMasterMissionEventAction`2_hotfix;
    private LuaFunction m_remove_HeroJobMasterMissionEventAction`2_hotfix;
    private LuaFunction m_add_AddHeroJobMissionEventAction_hotfix;
    private LuaFunction m_remove_AddHeroJobMissionEventAction_hotfix;
    private LuaFunction m_add_HeroLevelUpMissionEventAction`1_hotfix;
    private LuaFunction m_remove_HeroLevelUpMissionEventAction`1_hotfix;
    private LuaFunction m_add_AddHeroJobSkillMissionEventAction`1_hotfix;
    private LuaFunction m_remove_AddHeroJobSkillMissionEventAction`1_hotfix;
    private LuaFunction m_add_AddHeroJobSoliderMissionEventAction`1_hotfix;
    private LuaFunction m_remove_AddHeroJobSoliderMissionEventAction`1_hotfix;
    private LuaFunction m_add_AddHeroMissionEventAction`1_hotfix;
    private LuaFunction m_remove_AddHeroMissionEventAction`1_hotfix;
    private LuaFunction m_add_LevelUpHeroStarLevelMissionEventAction`1_hotfix;
    private LuaFunction m_remove_LevelUpHeroStarLevelMissionEventAction`1_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroComponentCommon()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Tick(uint deltaMillisecond)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool Serialize<T>(T dest)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeSerialize<T>(T source)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostDeSerialize()
    {
      // ISSUE: unable to decompile the method.
    }

    public IComponentOwner Owner
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetProtagonistID()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsProtagonistHero(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsProtagonistExist()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int SetProtagonist(int protagonistId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GmStrengthenAllHeroes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Hero> GetAllStarLvlMaxHeroes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<HeroFragmentBagItem> GetAllStarLvlMaxHeroFragments()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void InitHeroDataByCaculate(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitHeroJobs(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddHeroDefaultInfos(Hero hero, ConfigDataHeroInfo heroInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanHeroJobEquipSkill(int jobRelatedId, int skillId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanSelectSkillHero(Hero hero, List<int> skills)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int SelectSkills(int heroId, List<int> skills)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int SelectSoldier(int heroId, int soldierId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanHeroSelectSolider(Hero hero, int soliderId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int RemoveHero(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveAllHero(List<int> changedHeroIdList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveAllHeros()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Hero> GetAllHeros()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroJob FindHeroJob(Hero hero, int jobRelatedId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsUnlockedHeroJob(Hero hero, int jobRelatedId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CanGetHeroJobByRank(Hero hero, int rank)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroJob FindHeroJob(int heroId, int jobRelatedId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Hero FindHero(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddHeroFightNums(int heroId, int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddHeroAllNeedJobsAchievements(int heroId, List<int> achievements)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddHero(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Hero AddHeroInfos(ConfigDataHeroInfo heroInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsWastefulAddExp(Hero hero, int addExp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCurrentLevelMaxHeroLevel(int heroLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsFullCurrentHeroExp(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddHeroesExp(List<int> heroIds, int exp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAddHeroExp(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddHeroExp(int heroId, int exp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OutPutHeroLevelUpOperateLog(int heroId, int currentLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddHeroExpByUseableBagItem(
      int heroId,
      GoodsType goodsTypeId,
      int contentId,
      int consumeNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ComposeHero(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsLvlMaxHeroStar(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int LevelUpHeroStarLevel(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OutPutHeroUpgradeOperateLog(
      int heroId,
      int currentStar,
      List<Goods> costItems)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HashSet<int> GetGainHeroIds()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasGainHero(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsExistSkillId(int skillId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsExistSoliderId(int soliderId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public LinkedList<Hero> FindTopLevelHeroes(int topNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetActiveHeroJobRelatedIdByHeroId(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GmResetHeroJob(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Hero CreateDefaultConfigHero(ConfigDataHeroInfo heroInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnHeroLevelUp(int oldLevel, Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetAdditiveHeroAddExp(int addExp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetAdditiveHeroFavourabilityAddExp(int addExp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddHeroJobAchievementsAfterBattleLevelEnd(
      List<int> relatedAchievements,
      List<int> fightHeroes)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddHeroJob(Hero hero, ConfigDataJobConnectionInfo jobConnectionInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsJobLevelMax(ConfigDataJobConnectionInfo jobConnectionInfo, int jobLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsJobLevelMax(HeroJob job)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanLevelUpHeroJobLevel(int heroId, int jobRelatedId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CanTransferHeroJobRank(int jobRank, int CanTransferMaxRank)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanTransferHeroJob(int heroId, int jobRelatedId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int TransferHeroJob(int heroId, int jobRelatedId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OutPutHeroJobChangeOperateLog(
      int heroId,
      int preJobConnectionId,
      int postJobConnectionId,
      List<Goods> costItems)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTransferHeroJob(
      Hero hero,
      ConfigDataJobConnectionInfo jobConnectionInfo,
      int jobLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TryEquipNewSkill(Hero hero, int newGotSkillId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsNeedUnlock(ConfigDataJobConnectionInfo jobConnectionInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsNeedAddAchievement(ConfigDataJobConnectionInfo jobConnectionInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int UnlockedHeroJob(int heroId, int jobRelatedId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OutPutHeroJobUnlockOperateLog(
      int heroId,
      int activeJobId,
      int unlockJobId,
      List<Goods> costItems)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int CanUnlockedHeroJob(Hero hero, ConfigDataJobConnectionInfo jobConnectionInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int LevelUpHeroJobLevel(int heroId, int jobRelatedId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OutPutHeroJobLevelUpOperateLog(
      int heroId,
      int activeJobId,
      int currentLevel,
      List<Goods> costItems)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddSkill(Hero hero, int skillId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddSoldier(Hero hero, int soldierId, bool needExchange = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AutoTakeOffEquipments(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void InitWearedEquipments()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanWearEquipment(int heroId, ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanWearEquipmentByJobAndArmy(Hero hero, BagItemBase equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsEquipmentWeared(ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Hero GetWearedEquipmentHero(ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TakeOffEquipmentsOrNotWhenTransferHeroJob(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int WearEquipment(int heroId, ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void WearEquipmentBySlot(Hero hero, int slot, ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TakeOffEquipmentBySlot(Hero hero, int slot)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanTakeOffEquipment(int heroId, int slot)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int TakeOffEquipment(int heroId, int slot)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasBetterEquipmentByHero(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ulong[] GetBestEquipments(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected Dictionary<int, List<HeroComponentCommon.EquipmentBattlePowerInfo>> GetTopLevelEquipments(
      Hero hero,
      int topNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAutoEquipmentByClient(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAutoEquipment(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanWearCharSkin(int heroId, int charSkinId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int WearCharSkin(int heroId, int charSkinId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanTakeOffCharSkin(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int TakeOffCharSkin(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanWearModelSkin(int heroId, int jobRelatedId, int modelSkinId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int WearModelSkin(int heroId, int jobRelatedId, int modelSkinId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanTakeOffModelSkin(int heroId, int jobRelatedId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int TakeOffModelSkin(int heroId, int jobRelatedId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanWearSoldierSkin(int heroId, int soldierId, int soldierSkinId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int WearSoldierSkin(int heroId, int soldierId, int soldierSkinId, bool applyToAll)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanTakeOffSoldierSkin(int heroId, int soldierId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int TakeOffSoldierSkin(int heroId, int soldierId, bool applyToAll)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void TakeOffSoldierSkinApplyToAll(int soldierId, int skinId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddHeroFavorabilityExpByUseableBagItem(
      int heroId,
      GoodsType goodsTypeId,
      int contentId,
      int consumeNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddHeroFavorabilityLevel(int heroId, int addExp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OutPutHeroFavourabilityOperateLog(
      int heroId,
      int oldLvl,
      int oldExp,
      int newLvl,
      int newExp,
      List<Goods> rewards = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddHeroFavorabilityLevel(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void GenerateFavorabilityLevelUpReward(int heroId, int dropId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsFullFavorabilityExp(Hero hero, ConfigDataHeroInformationInfo heroInformationInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void UpdateProtagonistHeroFavorabilityLevel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanUnlockHeroFetter(int heroId, int fetterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanReachFetterUnlockCondition(HeroFetterCompletionCondition condition)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GmLevelUpHeroFetter2SpecificLevel(int heroId, int fetterId, int reachLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanLevelUpHeroFetter(int heroId, int fetterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int LevelUpHeroFetter(int heroId, int fetterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void LevelUpHeroFetterCallBack(Hero hero, int fetterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OutPutHeroFetterOpereateLog(
      int heroId,
      int fetterId,
      int currentLvl,
      List<Goods> itemCost = null,
      List<Goods> rewards = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int UnlockHeroFetter(int heroId, int fetterId, bool check = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void GenerateFetterUnlockRewards(int heroId, int fetterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ConfessHero(int heroId, bool check = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void GenerateConfessionRewards(
      int herId,
      ConfigDataHeroConfessionInfo confessionInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanConfessHero(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GmSetHeroHeartFetter(int heroId, int level)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanUnlockHeroHeartFetter(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanReachHeroHeartFetterUnlockCondition(
      Hero hero,
      HeroHeartFetterUnlockCondition condition)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int UnlockHeroHeartFetter(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnHeroHeartFettterUnlock(Hero hero, DateTime time)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void GenerateHeroHeartFetterUnlockReward(ConfigDataHeroInfo heroInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanLevelUpHeroHeartFetter(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int LevelUpHeroHeartFetter(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroHeartFetterLevelUp(
      Hero hero,
      ConfigDataHeroHeartFetterInfo heroHeartFetterInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnHeroHeartFetterLevelMax(Hero hero, DateTime time)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FlushHeroInteractNums()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void InitHeroInteractNums()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CanFlushHeroInteractNums()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsFullHeroInteractNums()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void AddHeroInteractNums(int addNums, bool recoveryByTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsHeroInteractNumsEqualToMax()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanInteractHero(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroFavorabilityUpLevel(
      Hero hero,
      ConfigDataHeroInformationInfo heroInformationInfo,
      int addExp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected int GetHeroInteractId(Hero hero, ConfigDataHeroInformationInfo heroInformationInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetAllHeroBattlePower()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetLastAllHeroRank()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetTopHeroBattlePower()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetLastTopHeroRank()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetChampionHeroBattlePower()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetChampionHeroId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetLastChampionHeroRank()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool ComputeBattlePower(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnHeroBattlePowerChange(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanComposeHero(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanLevelUpHeroStarLevel(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ulong> GetEquipedEquipments()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<int> GetUnlcokJobAchievements(ConfigDataJobConnectionInfo jobConnectionInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetSkillEnergyFromConfig(int heroLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<Goods> GetHeroStarLevelUpDataFromConfig(int fragmentId, int newStarLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetSkillCostFromConfig(int skillId)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> AddHeroEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int> SpecificHeroFightMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int> HeroNewJobTransferMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action HeroJobLevelUpMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int> HeroJobMasterMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action AddHeroJobMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> HeroLevelUpMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> AddHeroJobSkillMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> AddHeroJobSoliderMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> AddHeroMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> LevelUpHeroStarLevelMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public HeroComponentCommon.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_AddHeroEvent(int obj)
    {
    }

    private void __clearDele_AddHeroEvent(int obj)
    {
      this.AddHeroEvent = (Action<int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_SpecificHeroFightMissionEvent(int arg1, int arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_SpecificHeroFightMissionEvent(int arg1, int arg2)
    {
      this.SpecificHeroFightMissionEvent = (Action<int, int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_HeroNewJobTransferMissionEvent(int arg1, int arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_HeroNewJobTransferMissionEvent(int arg1, int arg2)
    {
      this.HeroNewJobTransferMissionEvent = (Action<int, int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_HeroJobLevelUpMissionEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_HeroJobLevelUpMissionEvent()
    {
      this.HeroJobLevelUpMissionEvent = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_HeroJobMasterMissionEvent(int arg1, int arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_HeroJobMasterMissionEvent(int arg1, int arg2)
    {
      this.HeroJobMasterMissionEvent = (Action<int, int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_AddHeroJobMissionEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_AddHeroJobMissionEvent()
    {
      this.AddHeroJobMissionEvent = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_HeroLevelUpMissionEvent(int obj)
    {
    }

    private void __clearDele_HeroLevelUpMissionEvent(int obj)
    {
      this.HeroLevelUpMissionEvent = (Action<int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_AddHeroJobSkillMissionEvent(int obj)
    {
    }

    private void __clearDele_AddHeroJobSkillMissionEvent(int obj)
    {
      this.AddHeroJobSkillMissionEvent = (Action<int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_AddHeroJobSoliderMissionEvent(int obj)
    {
    }

    private void __clearDele_AddHeroJobSoliderMissionEvent(int obj)
    {
      this.AddHeroJobSoliderMissionEvent = (Action<int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_AddHeroMissionEvent(int obj)
    {
    }

    private void __clearDele_AddHeroMissionEvent(int obj)
    {
      this.AddHeroMissionEvent = (Action<int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_LevelUpHeroStarLevelMissionEvent(int obj)
    {
    }

    private void __clearDele_LevelUpHeroStarLevelMissionEvent(int obj)
    {
      this.LevelUpHeroStarLevelMissionEvent = (Action<int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class EquipmentBattlePowerInfo
    {
      public ulong InstanceId { get; set; }

      public int Level { get; set; }
    }

    public class LuaExportHelper
    {
      private HeroComponentCommon m_owner;

      public LuaExportHelper(HeroComponentCommon owner)
      {
        this.m_owner = owner;
      }

      public void __callDele_AddHeroEvent(int obj)
      {
        this.m_owner.__callDele_AddHeroEvent(obj);
      }

      public void __clearDele_AddHeroEvent(int obj)
      {
        this.m_owner.__clearDele_AddHeroEvent(obj);
      }

      public void __callDele_SpecificHeroFightMissionEvent(int arg1, int arg2)
      {
        this.m_owner.__callDele_SpecificHeroFightMissionEvent(arg1, arg2);
      }

      public void __clearDele_SpecificHeroFightMissionEvent(int arg1, int arg2)
      {
        this.m_owner.__clearDele_SpecificHeroFightMissionEvent(arg1, arg2);
      }

      public void __callDele_HeroNewJobTransferMissionEvent(int arg1, int arg2)
      {
        this.m_owner.__callDele_HeroNewJobTransferMissionEvent(arg1, arg2);
      }

      public void __clearDele_HeroNewJobTransferMissionEvent(int arg1, int arg2)
      {
        this.m_owner.__clearDele_HeroNewJobTransferMissionEvent(arg1, arg2);
      }

      public void __callDele_HeroJobLevelUpMissionEvent()
      {
        this.m_owner.__callDele_HeroJobLevelUpMissionEvent();
      }

      public void __clearDele_HeroJobLevelUpMissionEvent()
      {
        this.m_owner.__clearDele_HeroJobLevelUpMissionEvent();
      }

      public void __callDele_HeroJobMasterMissionEvent(int arg1, int arg2)
      {
        this.m_owner.__callDele_HeroJobMasterMissionEvent(arg1, arg2);
      }

      public void __clearDele_HeroJobMasterMissionEvent(int arg1, int arg2)
      {
        this.m_owner.__clearDele_HeroJobMasterMissionEvent(arg1, arg2);
      }

      public void __callDele_AddHeroJobMissionEvent()
      {
        this.m_owner.__callDele_AddHeroJobMissionEvent();
      }

      public void __clearDele_AddHeroJobMissionEvent()
      {
        this.m_owner.__clearDele_AddHeroJobMissionEvent();
      }

      public void __callDele_HeroLevelUpMissionEvent(int obj)
      {
        this.m_owner.__callDele_HeroLevelUpMissionEvent(obj);
      }

      public void __clearDele_HeroLevelUpMissionEvent(int obj)
      {
        this.m_owner.__clearDele_HeroLevelUpMissionEvent(obj);
      }

      public void __callDele_AddHeroJobSkillMissionEvent(int obj)
      {
        this.m_owner.__callDele_AddHeroJobSkillMissionEvent(obj);
      }

      public void __clearDele_AddHeroJobSkillMissionEvent(int obj)
      {
        this.m_owner.__clearDele_AddHeroJobSkillMissionEvent(obj);
      }

      public void __callDele_AddHeroJobSoliderMissionEvent(int obj)
      {
        this.m_owner.__callDele_AddHeroJobSoliderMissionEvent(obj);
      }

      public void __clearDele_AddHeroJobSoliderMissionEvent(int obj)
      {
        this.m_owner.__clearDele_AddHeroJobSoliderMissionEvent(obj);
      }

      public void __callDele_AddHeroMissionEvent(int obj)
      {
        this.m_owner.__callDele_AddHeroMissionEvent(obj);
      }

      public void __clearDele_AddHeroMissionEvent(int obj)
      {
        this.m_owner.__clearDele_AddHeroMissionEvent(obj);
      }

      public void __callDele_LevelUpHeroStarLevelMissionEvent(int obj)
      {
        this.m_owner.__callDele_LevelUpHeroStarLevelMissionEvent(obj);
      }

      public void __clearDele_LevelUpHeroStarLevelMissionEvent(int obj)
      {
        this.m_owner.__clearDele_LevelUpHeroStarLevelMissionEvent(obj);
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public BagComponentCommon m_bag
      {
        get
        {
          return this.m_owner.m_bag;
        }
        set
        {
          this.m_owner.m_bag = value;
        }
      }

      public RiftComponentCommon m_rift
      {
        get
        {
          return this.m_owner.m_rift;
        }
        set
        {
          this.m_owner.m_rift = value;
        }
      }

      public MissionComponentCommon m_mission
      {
        get
        {
          return this.m_owner.m_mission;
        }
        set
        {
          this.m_owner.m_mission = value;
        }
      }

      public PlayerBasicInfoComponentCommon m_basicInfo
      {
        get
        {
          return this.m_owner.m_basicInfo;
        }
        set
        {
          this.m_owner.m_basicInfo = value;
        }
      }

      public BattleComponentCommon m_battle
      {
        get
        {
          return this.m_owner.m_battle;
        }
        set
        {
          this.m_owner.m_battle = value;
        }
      }

      public ResourceComponentCommon m_resource
      {
        get
        {
          return this.m_owner.m_resource;
        }
        set
        {
          this.m_owner.m_resource = value;
        }
      }

      public GuildComponentCommon m_guild
      {
        get
        {
          return this.m_owner.m_guild;
        }
        set
        {
          this.m_owner.m_guild = value;
        }
      }

      public TrainingGroundCompomentCommon m_trainingGround
      {
        get
        {
          return this.m_owner.m_trainingGround;
        }
        set
        {
          this.m_owner.m_trainingGround = value;
        }
      }

      public DataSectionHero m_heroDS
      {
        get
        {
          return this.m_owner.m_heroDS;
        }
        set
        {
          this.m_owner.m_heroDS = value;
        }
      }

      public Dictionary<ulong, int> m_wearedEquipmentHeroDict
      {
        get
        {
          return this.m_owner.m_wearedEquipmentHeroDict;
        }
        set
        {
          this.m_owner.m_wearedEquipmentHeroDict = value;
        }
      }

      public HashSet<int> m_soliders
      {
        get
        {
          return this.m_owner.m_soliders;
        }
        set
        {
          this.m_owner.m_soliders = value;
        }
      }

      public HashSet<int> m_skills
      {
        get
        {
          return this.m_owner.m_skills;
        }
        set
        {
          this.m_owner.m_skills = value;
        }
      }

      public Dictionary<int, DateTime> m_atuoEquipmentTouch
      {
        get
        {
          return this.m_owner.m_atuoEquipmentTouch;
        }
        set
        {
          this.m_owner.m_atuoEquipmentTouch = value;
        }
      }

      public void InitHeroDataByCaculate(Hero hero)
      {
        this.m_owner.InitHeroDataByCaculate(hero);
      }

      public void InitHeroJobs(Hero hero)
      {
        this.m_owner.InitHeroJobs(hero);
      }

      public void AddHeroDefaultInfos(Hero hero, ConfigDataHeroInfo heroInfo)
      {
        this.m_owner.AddHeroDefaultInfos(hero, heroInfo);
      }

      public bool CanGetHeroJobByRank(Hero hero, int rank)
      {
        return this.m_owner.CanGetHeroJobByRank(hero, rank);
      }

      public Hero AddHeroInfos(ConfigDataHeroInfo heroInfo)
      {
        return this.m_owner.AddHeroInfos(heroInfo);
      }

      public void OutPutHeroLevelUpOperateLog(int heroId, int currentLevel)
      {
        this.m_owner.OutPutHeroLevelUpOperateLog(heroId, currentLevel);
      }

      public void OutPutHeroUpgradeOperateLog(int heroId, int currentStar, List<Goods> costItems)
      {
        this.m_owner.OutPutHeroUpgradeOperateLog(heroId, currentStar, costItems);
      }

      public void OnHeroLevelUp(int oldLevel, Hero hero)
      {
        this.m_owner.OnHeroLevelUp(oldLevel, hero);
      }

      public void AddHeroJob(Hero hero, ConfigDataJobConnectionInfo jobConnectionInfo)
      {
        this.m_owner.AddHeroJob(hero, jobConnectionInfo);
      }

      public bool CanTransferHeroJobRank(int jobRank, int CanTransferMaxRank)
      {
        return this.m_owner.CanTransferHeroJobRank(jobRank, CanTransferMaxRank);
      }

      public void OutPutHeroJobChangeOperateLog(
        int heroId,
        int preJobConnectionId,
        int postJobConnectionId,
        List<Goods> costItems)
      {
        this.m_owner.OutPutHeroJobChangeOperateLog(heroId, preJobConnectionId, postJobConnectionId, costItems);
      }

      public void OnTransferHeroJob(
        Hero hero,
        ConfigDataJobConnectionInfo jobConnectionInfo,
        int jobLevel)
      {
        this.m_owner.OnTransferHeroJob(hero, jobConnectionInfo, jobLevel);
      }

      public void TryEquipNewSkill(Hero hero, int newGotSkillId)
      {
        this.m_owner.TryEquipNewSkill(hero, newGotSkillId);
      }

      public bool IsNeedAddAchievement(ConfigDataJobConnectionInfo jobConnectionInfo)
      {
        return this.m_owner.IsNeedAddAchievement(jobConnectionInfo);
      }

      public void OutPutHeroJobUnlockOperateLog(
        int heroId,
        int activeJobId,
        int unlockJobId,
        List<Goods> costItems)
      {
        this.m_owner.OutPutHeroJobUnlockOperateLog(heroId, activeJobId, unlockJobId, costItems);
      }

      public int CanUnlockedHeroJob(Hero hero, ConfigDataJobConnectionInfo jobConnectionInfo)
      {
        return this.m_owner.CanUnlockedHeroJob(hero, jobConnectionInfo);
      }

      public void OutPutHeroJobLevelUpOperateLog(
        int heroId,
        int activeJobId,
        int currentLevel,
        List<Goods> costItems)
      {
        this.m_owner.OutPutHeroJobLevelUpOperateLog(heroId, activeJobId, currentLevel, costItems);
      }

      public void AddSkill(Hero hero, int skillId)
      {
        this.m_owner.AddSkill(hero, skillId);
      }

      public void InitWearedEquipments()
      {
        this.m_owner.InitWearedEquipments();
      }

      public void TakeOffEquipmentsOrNotWhenTransferHeroJob(Hero hero)
      {
        this.m_owner.TakeOffEquipmentsOrNotWhenTransferHeroJob(hero);
      }

      public void WearEquipmentBySlot(Hero hero, int slot, ulong instanceId)
      {
        this.m_owner.WearEquipmentBySlot(hero, slot, instanceId);
      }

      public void TakeOffEquipmentBySlot(Hero hero, int slot)
      {
        this.m_owner.TakeOffEquipmentBySlot(hero, slot);
      }

      public Dictionary<int, List<HeroComponentCommon.EquipmentBattlePowerInfo>> GetTopLevelEquipments(
        Hero hero,
        int topNums)
      {
        return this.m_owner.GetTopLevelEquipments(hero, topNums);
      }

      public void OutPutHeroFavourabilityOperateLog(
        int heroId,
        int oldLvl,
        int oldExp,
        int newLvl,
        int newExp,
        List<Goods> rewards)
      {
        this.m_owner.OutPutHeroFavourabilityOperateLog(heroId, oldLvl, oldExp, newLvl, newExp, rewards);
      }

      public void AddHeroFavorabilityLevel(Hero hero)
      {
        this.m_owner.AddHeroFavorabilityLevel(hero);
      }

      public void GenerateFavorabilityLevelUpReward(int heroId, int dropId)
      {
        this.m_owner.GenerateFavorabilityLevelUpReward(heroId, dropId);
      }

      public void UpdateProtagonistHeroFavorabilityLevel()
      {
        this.m_owner.UpdateProtagonistHeroFavorabilityLevel();
      }

      public void LevelUpHeroFetterCallBack(Hero hero, int fetterId)
      {
        this.m_owner.LevelUpHeroFetterCallBack(hero, fetterId);
      }

      public void OutPutHeroFetterOpereateLog(
        int heroId,
        int fetterId,
        int currentLvl,
        List<Goods> itemCost,
        List<Goods> rewards)
      {
        this.m_owner.OutPutHeroFetterOpereateLog(heroId, fetterId, currentLvl, itemCost, rewards);
      }

      public void GenerateFetterUnlockRewards(int heroId, int fetterId)
      {
        this.m_owner.GenerateFetterUnlockRewards(heroId, fetterId);
      }

      public void GenerateConfessionRewards(int herId, ConfigDataHeroConfessionInfo confessionInfo)
      {
        this.m_owner.GenerateConfessionRewards(herId, confessionInfo);
      }

      public void OnHeroHeartFettterUnlock(Hero hero, DateTime time)
      {
        this.m_owner.OnHeroHeartFettterUnlock(hero, time);
      }

      public void GenerateHeroHeartFetterUnlockReward(ConfigDataHeroInfo heroInfo)
      {
        this.m_owner.GenerateHeroHeartFetterUnlockReward(heroInfo);
      }

      public void OnHeroHeartFetterLevelUp(
        Hero hero,
        ConfigDataHeroHeartFetterInfo heroHeartFetterInfo)
      {
        this.m_owner.OnHeroHeartFetterLevelUp(hero, heroHeartFetterInfo);
      }

      public void OnHeroHeartFetterLevelMax(Hero hero, DateTime time)
      {
        this.m_owner.OnHeroHeartFetterLevelMax(hero, time);
      }

      public void FlushHeroInteractNums()
      {
        this.m_owner.FlushHeroInteractNums();
      }

      public void InitHeroInteractNums()
      {
        this.m_owner.InitHeroInteractNums();
      }

      public bool CanFlushHeroInteractNums()
      {
        return this.m_owner.CanFlushHeroInteractNums();
      }

      public bool IsFullHeroInteractNums()
      {
        return this.m_owner.IsFullHeroInteractNums();
      }

      public void AddHeroInteractNums(int addNums, bool recoveryByTime)
      {
        this.m_owner.AddHeroInteractNums(addNums, recoveryByTime);
      }

      public bool IsHeroInteractNumsEqualToMax()
      {
        return this.m_owner.IsHeroInteractNumsEqualToMax();
      }

      public int GetHeroInteractId(Hero hero, ConfigDataHeroInformationInfo heroInformationInfo)
      {
        return this.m_owner.GetHeroInteractId(hero, heroInformationInfo);
      }

      public List<int> GetUnlcokJobAchievements(ConfigDataJobConnectionInfo jobConnectionInfo)
      {
        return this.m_owner.GetUnlcokJobAchievements(jobConnectionInfo);
      }

      public List<Goods> GetHeroStarLevelUpDataFromConfig(int fragmentId, int newStarLevel)
      {
        return this.m_owner.GetHeroStarLevelUpDataFromConfig(fragmentId, newStarLevel);
      }

      public int GetSkillCostFromConfig(int skillId)
      {
        return this.m_owner.GetSkillCostFromConfig(skillId);
      }
    }
  }
}
