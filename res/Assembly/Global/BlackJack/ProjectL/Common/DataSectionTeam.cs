﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionTeam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class DataSectionTeam : DataSection
  {
    private Dictionary<string, TeamRoomInviteInfo> m_inviteInfos;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionTeam()
    {
      // ISSUE: unable to decompile the method.
    }

    public override void ClearInitedData()
    {
      this.ClearInviteInfos();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearTeamRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    public void ClearInviteInfos()
    {
      this.m_inviteInfos.Clear();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearAInviteInfo(ulong sessionId, int roomId)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsInvited(ulong sessionId, int roomId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<TeamRoomInviteInfo> GetInviteInfos()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string BuildInviteInfoKey(ulong sessionId, int roomId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SetTeamRoomInviteInfo(TeamRoomInviteInfo info)
    {
      // ISSUE: unable to decompile the method.
    }

    public int RoomId { get; set; }

    public GameFunctionType GameFunctionTypeId { get; set; }

    public int LocationId { get; set; }

    public GameFunctionType WaitingFunctionTypeId { get; set; }

    public int WaitingLocationId { get; set; }

    public DateTime LastInviteSendTime { get; set; }
  }
}
