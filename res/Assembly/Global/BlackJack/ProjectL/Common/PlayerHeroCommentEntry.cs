﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.PlayerHeroCommentEntry
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class PlayerHeroCommentEntry
  {
    public List<ulong> CommentedEntryInstanceIds;
    public HashSet<ulong> PraisedEntryInstanceIds;

    [MethodImpl((MethodImplOptions) 32768)]
    public PlayerHeroCommentEntry()
    {
      // ISSUE: unable to decompile the method.
    }

    public int HeroId { get; set; }

    public int CommentedNums { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProPlayerHeroCommentEntry PlayerHeroCommentEntryToPBPlayerHeroCommentEntry(
      PlayerHeroCommentEntry entry)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static PlayerHeroCommentEntry PBPlayerHeroCommentEntryToPlayerHeroCommentEntry(
      ProPlayerHeroCommentEntry pbEntry)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
