﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.HeroJob
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Protocol;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class HeroJob
  {
    private int m_jobRelatedId;
    public HashSet<int> Achievements;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroJob()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroJob(HeroJob other)
    {
      // ISSUE: unable to decompile the method.
    }

    public int JobRelatedId
    {
      set
      {
        this.m_jobRelatedId = value;
        this.UpdateJobConnectionInfo();
      }
      get
      {
        return this.m_jobRelatedId;
      }
    }

    public int JobLevel { get; set; }

    public int ModelSkinId { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProHeroJob HeroJobToPBHeroJob(HeroJob heroJob)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static HeroJob PbHeroJobToHeroJob(ProHeroJob pbHeroJob)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateJobConnectionInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    public ConfigDataJobConnectionInfo JobConnectionInfo { private set; get; }

    public bool IsLevelMax()
    {
      return this.IsLevelMax(this.JobLevel);
    }

    public bool IsLevelMax(int jobLevel)
    {
      return this.JobConnectionInfo.IsJobLevelMax(jobLevel);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataModelSkinResourceInfo GetModelSkinResourceInfo()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
