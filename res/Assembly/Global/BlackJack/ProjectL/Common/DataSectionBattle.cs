﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionBattle
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class DataSectionBattle : DataSection
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void ClearInitedData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    public void InitProcessingBattle(ProcessingBattle info)
    {
      this.ProcessingBattleInfo = info;
    }

    public void InitTeams(Dictionary<int, List<int>> teams)
    {
      this.Teams = new Dictionary<int, List<int>>((IDictionary<int, List<int>>) teams);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTeam(int teamTypeId, List<int> heroes)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetTeam(int teamTypeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetProcessingBattleInfo(BattleType type, int typeId)
    {
    }

    public void SetRandomSeed(int randomSeed)
    {
      this.ProcessingBattleInfo.RandomSeed = randomSeed;
    }

    public void SetArmyRandomSeed(int ArmyRandomSeed)
    {
      this.ProcessingBattleInfo.ArmyRandomSeed = ArmyRandomSeed;
    }

    public int GetArmyRandomSeed()
    {
      return this.ProcessingBattleInfo.ArmyRandomSeed;
    }

    public bool IsGotBattleTreasureId(int id)
    {
      return this.GotBattleTreasureIds.Contains(id);
    }

    public void AddBattleTreasureId(int id)
    {
      this.GotBattleTreasureIds.Add(id);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetArmyRandomSeedByBattleId(int battleId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveEveryTimeArmyRandomSeed(int battleId)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddEveryTimeArmyRandomSeed(int battleId, int randomSeed)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddDailyTimeArmyRandomSeed(int battleId, int randomSeed)
    {
    }

    public void InitDailyTimeArmyRandomSeed(int bettleId, int armyRandomSeed)
    {
      this.DailyArmyRandomSeedDict.Add(bettleId, armyRandomSeed);
    }

    public void InitEveryTimeArmyRandomSeed(int bettleId, int armyRandomSeed)
    {
      this.EveryTimeArmyRandomSeedDict.Add(bettleId, armyRandomSeed);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearDailyArmyRandomSeeds()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearProcesingBattleInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    public ProcessingBattle ProcessingBattleInfo { get; private set; }

    public Dictionary<int, List<int>> Teams { get; set; }

    public List<int> GotBattleTreasureIds { get; set; }

    public Dictionary<int, int> DailyArmyRandomSeedDict { get; set; }

    public Dictionary<int, int> EveryTimeArmyRandomSeedDict { get; set; }

    public ArenaBattleStatus ArenaBattleStatus { get; set; }

    public int ArenaBattleId { get; set; }

    public int ArenaBattleRandomSeed { get; set; }

    public ulong BattleRoomId { get; set; }
  }
}
