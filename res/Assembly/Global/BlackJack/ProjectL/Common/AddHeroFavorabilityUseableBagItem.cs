﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.AddHeroFavorabilityUseableBagItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class AddHeroFavorabilityUseableBagItem : UseableBagItem
  {
    public const int SpecificHeroAddFavorabilityExpBasicValue = 10000;
    public List<int> SpecificHeroes;

    [MethodImpl((MethodImplOptions) 32768)]
    public AddHeroFavorabilityUseableBagItem(
      GoodsType goodsTypeId,
      int contentId,
      int nums,
      ulong instanceId)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override int HaveEffect(IComponentOwner owner, params object[] param)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CalculateAddFavorabilityExp(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    private int HeroId { get; set; }

    public int NormalAddExp { get; set; }

    public int SpecificHeroAddFavorabilityExpMultipleValue { get; set; }
  }
}
