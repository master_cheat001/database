﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionHeroDungeon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class DataSectionHeroDungeon : DataSection
  {
    private int m_dailyChallengeNums;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionHeroDungeon()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void ClearInitedData()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroDungeonChapter FindChapter(int chapterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroDungeonLevel FindLevel(int chapterId, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ResetLevelChallengeNums()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitLevel(HeroDungeonChapter chapter, int levelId, int stars, int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    public void FinishedLevel(int levelId)
    {
      this.FinishedLevels.Add(levelId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddLevel(HeroDungeonChapter chapter, int levelId, int stars, int nums)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroDungeonChapter AddChapter(int chapterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitChapteStarRewardIndexes(int chapterId, List<int> starRewardIndexes)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddChapterStarRewardIndex(HeroDungeonChapter chapter, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsFinishedLevel(int levelId)
    {
      return this.FinishedLevels.Contains(levelId);
    }

    public bool HasGotAchievementRelationId(int achievementId)
    {
      return this.AchievementRelationIds.Contains(achievementId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddAchievementRelationId(int achievementRelationId)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitAchievementRelationIds(List<int> achievementIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddHeroDungeonLevelChallengeNums(HeroDungeonLevel level, int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    public void InitDailyChallengeNums(int nums)
    {
      this.m_dailyChallengeNums = nums;
    }

    public void SetDailyChallengeNums(int nums)
    {
      this.m_dailyChallengeNums = nums;
      this.SetDirty(true);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddDailyChallengeNums(int nums)
    {
    }

    public int DailyChallengeNums
    {
      get
      {
        return this.m_dailyChallengeNums;
      }
    }

    public HashSet<int> FinishedLevels { get; set; }

    public HashSet<int> AchievementRelationIds { get; set; }

    public Dictionary<int, HeroDungeonChapter> Chapters { get; set; }
  }
}
