﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSecionRandomEvent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class DataSecionRandomEvent : DataSection
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public DataSecionRandomEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void ClearInitedData()
    {
      // ISSUE: unable to decompile the method.
    }

    public void InitDefiniteLevelZone(RandomEventLevelZone zone)
    {
      this.DefiniteLevelZone = zone;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitRandomLevelZone(List<RandomEventLevelZone> zones)
    {
      // ISSUE: unable to decompile the method.
    }

    public void InitGenerateEventTime(List<int> timeList)
    {
      this.RandomEventTimeList = new List<int>((IEnumerable<int>) timeList);
    }

    public void InitGenerateRandomEventTotalCount(int count)
    {
      this.GenerateRandomEventTotalCount = count;
    }

    public void InitRandomEventTotalCount(int count)
    {
      this.RandomEventTotalCount = count;
    }

    public void SetDefiniteEventLevelZone(RandomEventLevelZone zone)
    {
      this.DefiniteLevelZone = zone;
      this.SetDirty(true);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DefiniteGroupGenerateRandomEvent(int groupId)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RandomGroupGenerateRandomEvent(int levelZoneId, int groupId, int maxCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddRandomEventLevelZone(int index, RandomEventLevelZone zone)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetRandomEventTotalCount(int count)
    {
      this.RandomEventTotalCount = count;
      this.SetDirty(true);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRandomEventTimes(List<int> timeList)
    {
    }

    public void SetNextRandomEventFlushTime(DateTime setTime)
    {
      this.NextRandomEventFlushTime = setTime;
      this.SetDirty(true);
    }

    public void SetDefiniteEventMaxCount(int maxCount)
    {
      this.DefiniteEventMaxCount = maxCount;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddRandomEventCount(int count = 1)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Dictionary<int, RandomEventGroup> GetDefiniteGroupRandomEventGroups()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddDefiniteGroupRandomEventGroup(RandomEventGroup newGroup)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Dictionary<int, RandomEventGroup> GetRandomGroupLevelZoneEventGroups(
      int levelZoneId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearRandomInfo()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveRandomEventTime(int time)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCompletedRandomEventGenerate()
    {
      // ISSUE: unable to decompile the method.
    }

    public int RandomEventTotalCount { get; set; }

    public int GenerateRandomEventTotalCount { get; set; }

    public int DefiniteEventMaxCount { get; set; }

    public DateTime NextRandomEventFlushTime { get; set; }

    public RandomEventLevelZone DefiniteLevelZone { get; set; }

    public Dictionary<int, RandomEventLevelZone> RandomLevelZone { get; set; }

    public List<int> RandomEventTimeList { get; set; }
  }
}
