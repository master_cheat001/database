﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionArena
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class DataSectionArena : DataSection
  {
    public DataSectionArena()
    {
      this.ArenaPlayerInfo = (ArenaPlayerInfo) null;
    }

    public void InitArenaPlayerInfo(ArenaPlayerInfo info)
    {
      this.ArenaPlayerInfo = info;
    }

    public override void ClearInitedData()
    {
      this.ArenaPlayerInfo = (ArenaPlayerInfo) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsEmptyArenaPlayerInfo()
    {
      return this.ArenaPlayerInfo == null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDefensiveTeam(ArenaPlayerDefensiveTeam team)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetWeekLastFlushTime(DateTime weekLastFlushTime)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetThisWeekBattleIds(List<int> battleIds)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDefensiveBattleId(byte battleId)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDefensiveRuleId(byte ruleId)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddVictoryPoints(int points)
    {
      // ISSUE: unable to decompile the method.
    }

    public int GetCurrentVictoryPoints()
    {
      return this.ArenaPlayerInfo.VictoryPoints;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ResetVictoryPoints()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ResetReceivedVictoryPointsRewardedIndexs()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasReceivedVictoryPointsRewardedIndex(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddReceivedVictoryPointsRewardIndex(int victoryPointsIndex)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaOpponent FindOpponent(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAttackedOpponent(bool autoBattle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetOpponents(List<ArenaOpponent> opponents)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetArenaDefensiveBattleInfo(ArenaOpponentDefensiveBattleInfo info)
    {
      this.ArenaPlayerInfo.OpponentDefensiveBattleInfo = info;
    }

    public ArenaOpponentDefensiveBattleInfo GetArenaDefensiveBattleInfo()
    {
      return this.ArenaPlayerInfo.OpponentDefensiveBattleInfo;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetOpponentUserId()
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetArenaLevelId(int arenaLevelId)
    {
      this.ArenaPlayerInfo.ArenaLevelId = arenaLevelId;
    }

    public void SetArenaPoints(ushort ArenaPoints)
    {
      this.ArenaPlayerInfo.ArenaPoints = ArenaPoints;
    }

    public void SetArenaPlayerInfo(ArenaPlayerInfo info)
    {
      this.ArenaPlayerInfo = info;
      this.SetDirty(true);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsFirstFindOpponents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ResetConsecutiveVictoryNums()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddConsecutiveVictoryNums()
    {
      // ISSUE: unable to decompile the method.
    }

    public ArenaPlayerInfo ArenaPlayerInfo { get; private set; }
  }
}
