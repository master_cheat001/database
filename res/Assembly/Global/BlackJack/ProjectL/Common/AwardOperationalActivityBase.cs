﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.AwardOperationalActivityBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class AwardOperationalActivityBase : OperationalActivityBase
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public AwardOperationalActivityBase()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public AwardOperationalActivityBase(
      ulong instanceId,
      int operationalActivityId,
      OperationalActivityType operationalActivityType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int CanGainRewardByIndex(int rewardIndex, DateTime currentTime)
    {
      // ISSUE: unable to decompile the method.
    }

    public void GainReward(int rewardIndex)
    {
      this.GainedRewardIndexs.Add(rewardIndex);
    }

    public List<int> GainedRewardIndexs { get; set; }
  }
}
