﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.ChatMessage
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  [Serializable]
  public class ChatMessage
  {
    public ChatChannel ChannelId { get; set; }

    public ChatSrcType ChatSrcType { get; set; }

    public string SrcName { get; set; }

    public int AvatarId { get; set; }

    public ChatContentType ChatContentType { get; set; }

    public int SrcPlayerLevel { get; set; }

    public string SrcGameUserID { get; set; }

    public DateTime SendTime { get; set; }

    public string DestGameUserId { get; set; }

    public string DestChatGroupId { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public ChatMessageNtf ToPbChatMessage()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
