﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.GiftStoreItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Protocol;
using SLua;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class GiftStoreItem
  {
    public int GoodsId { get; set; }

    public bool IsFirstBuy { get; set; }

    public int BoughtNums { get; set; }

    public DateTime NextFlushTime { get; set; }

    public DateTime SaleStartTime { get; set; }

    public DateTime SaleEndTime { get; set; }

    public ConfigDataGiftStoreItemInfo Config { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProGiftStoreItem ToPB(GiftStoreItem item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static GiftStoreItem FromPB(ProGiftStoreItem pbItem)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
