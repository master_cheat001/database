﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.TeamRoom
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class TeamRoom
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public TeamRoom()
    {
    }

    public int RoomId { get; set; }

    public TeamRoomPlayer Leader { get; set; }

    public List<TeamRoomPlayer> Players { get; set; }

    public DateTime LeaderKickOutTime { get; set; }

    public TeamRoomSetting Setting { get; set; }

    public bool IsLeader(TeamRoomPlayer player)
    {
      return player == this.Leader;
    }

    public bool IsLeaderBySessionId(ulong sessionId)
    {
      return (long) this.Leader.SessionId == (long) sessionId;
    }

    public void JoinTeamRoom(TeamRoomPlayer player)
    {
      this.Players.Add(player);
    }

    public void QuitTeamRoom(TeamRoomPlayer player)
    {
      this.Players.Remove(player);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TeamRoomPlayer FindTeamRoomPlayer(ulong sessionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TeamRoomPlayer FindTeamRoomPlayerAtPosition(int position)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProTeamRoom TeamRoomToPbTeamRoom(TeamRoom room)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static TeamRoom PbTeamRoomToTeamRoom(ProTeamRoom pbRoom)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
