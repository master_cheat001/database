﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.LimitedTimeExchangeOperationActivity
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Protocol;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class LimitedTimeExchangeOperationActivity : OperationalActivityBase
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public LimitedTimeExchangeOperationActivity()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public LimitedTimeExchangeOperationActivity(
      ulong instanceId,
      int operationalActivityId,
      OperationalActivityType operationalActivityType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeserializeFromPB(
      ProLimitedTimeExchangeOperationActivity pbOperationalActivity,
      ConfigDataOperationalActivityInfo config)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void ToPBNtf(DSOperationalActivityNtf ntf)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProLimitedTimeExchangeOperationActivity SerializeToPB()
    {
      // ISSUE: unable to decompile the method.
    }

    public Dictionary<int, int> ExchangedItemGroups { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanExchangeOperationalActivityItem(int itemGroupIndex, int exchangeTimes = 1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddOperationalActivityItemExchangeTimes(int itemGroupIndex, int exchangeTimes = 1)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
