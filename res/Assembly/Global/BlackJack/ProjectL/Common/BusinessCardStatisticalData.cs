﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.BusinessCardStatisticalData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class BusinessCardStatisticalData
  {
    public int MostSkilledHeroId { get; set; }

    public int HeroTotalPower { get; set; }

    public int AchievementMissionNums { get; set; }

    public int MasterJobNums { get; set; }

    public int RiftAchievementNums { get; set; }

    public int ChooseLevelAchievementNums { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProBusinessCardStatisticalData ToProtocol(
      BusinessCardStatisticalData data)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static BusinessCardStatisticalData FromProtocol(
      ProBusinessCardStatisticalData pbData)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
