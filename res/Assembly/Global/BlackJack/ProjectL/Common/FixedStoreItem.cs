﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.FixedStoreItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class FixedStoreItem
  {
    public int Id { set; get; }

    public int BoughtCount { set; get; }

    public bool IsFirstBuy { get; set; }

    public long NextFlushTime { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProFixedStoreItem StoreItemToPBStoreItem(FixedStoreItem storeItem)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<ProFixedStoreItem> StoreItemsToPBStoreItems(
      List<FixedStoreItem> storeItems)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static FixedStoreItem PBStoreItemToStoreItem(ProFixedStoreItem pbStoreItem)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<FixedStoreItem> PBStoreItemsToStoreItems(
      List<ProFixedStoreItem> pbStoreItems)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
