﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.CollectionActivity
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class CollectionActivity
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivity()
    {
      // ISSUE: unable to decompile the method.
    }

    public ulong ActivityInstanceId { get; set; }

    public List<int> FinishedChallengeLevelIds { get; set; }

    public int LastFinishedScenarioId { get; set; }

    public List<int> FinishedLootLevelIds { get; set; }

    public int CurrentWayPointId { get; set; }

    public List<CollectionActivityPlayerExchangeInfo> ExchangeInfoList { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProCollectionActivity ToPb(CollectionActivity activity)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static CollectionActivity FromPb(ProCollectionActivity activity)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
