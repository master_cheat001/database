﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.WayPointStatus
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

namespace BlackJack.ProjectL.Common
{
  public enum WayPointStatus
  {
    None = 0,
    Close = 1,
    Open = 2,
    Public = 4,
    Arrived = 8,
    NormalEvent = 16, // 0x00000010
    RandomEvent = 32, // 0x00000020
  }
}
