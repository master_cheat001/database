﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.GameFunctionStatus
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

namespace BlackJack.ProjectL.Common
{
  public enum GameFunctionStatus
  {
    Start = 1,
    End = 2,
    Cancel = 3,
    FightOutTime = 4,
    Error = 5,
    Inteam = 6,
    OutTeam = 7,
    MatchmakingStart = 8,
    MatchmakingCancel = 9,
    ProtectHero = 10, // 0x0000000A
    BanHero = 11, // 0x0000000B
    RealTimePVPStart = 12, // 0x0000000C
  }
}
