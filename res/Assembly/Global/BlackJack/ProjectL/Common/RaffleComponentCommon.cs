﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.RaffleComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class RaffleComponentCommon : IComponentBase
  {
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected IConfigDataLoader m_configDataLoader;
    protected BagComponentCommon m_bag;
    protected OperationalActivityCompomentCommon m_operationalActivity;
    protected DataSectionRaffle m_RaffleDS;

    [MethodImpl((MethodImplOptions) 32768)]
    public RaffleComponentCommon()
    {
    }

    public string GetName()
    {
      return "Raffle";
    }

    public virtual void Init()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    public virtual void DeInit()
    {
    }

    public virtual void Tick(uint deltaMillisecond)
    {
    }

    public virtual bool Serialize<T>(T dest)
    {
      return false;
    }

    public virtual void DeSerialize<T>(T source)
    {
    }

    public virtual void PostDeSerialize()
    {
    }

    public IComponentOwner Owner { get; set; }

    public void RemoveRafflePool(int poolId)
    {
      this.m_RaffleDS.RemoveRafflePool(poolId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void InitRafflePoolConfigs(List<RafflePool> rafflePools)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int IsRafflePoolOnActivityTime(int rafflePoolId)
    {
      // ISSUE: unable to decompile the method.
    }

    public OperationalActivityBase FindOperationalActivityByRafflePoolId(
      int activityCardPoolId)
    {
      return this.m_operationalActivity.FindOperationalActivityByRafflePoolId(activityCardPoolId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetAllOpenActivityRafflePool()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RafflePool GetRafflePool(int poolId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsRaffleItemsAllDrawed(RafflePool pool)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsDrawItemsEnough(GoodsType goodsType, int drawItemId, int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetDrawItemCost(RafflePool pool)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanFreeDraw(RafflePool pool)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanDraw(int poolId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void DrawCostItem(RafflePool rafflePool, int raffleId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RaffleItem GetRaffleItem(RafflePool pool, int raffleId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetFreeDrawedCount(RafflePool pool)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetDrawedCount(RafflePool pool)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetAllDrawedCount(RafflePool pool)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
