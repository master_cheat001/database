﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.TeamRoomSetting
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Protocol;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class TeamRoomSetting
  {
    public GameFunctionType GameFunctionTypeId { get; set; }

    public int LocationId { get; set; }

    public int JoinMinLevel { get; set; }

    public int JoinMaxLevel { get; set; }

    public TeamRoomAuthority Authority { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProTeamRoomSetting TeamRoomSettingToPbTeamRoomSetting(
      TeamRoomSetting setting)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static TeamRoomSetting PbTeamRoomSettingToTeamRoomSetting(
      ProTeamRoomSetting pbSetting)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
