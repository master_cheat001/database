﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.RankingPlayerInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class RankingPlayerInfo
  {
    public string UserId { get; set; }

    public int HeadIcon { get; set; }

    public int Level { get; set; }

    public string Name { get; set; }

    public int ChampionHeroId { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProRankingPlayerInfo RankingPlayerToPBRankingPlayer(
      RankingPlayerInfo playerInfo,
      int score)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static RankingPlayerInfo PBRankingPlayerToRankingPlayer(
      ProRankingPlayerInfo proPlayerInfo)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
