﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionTrainingGround
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class DataSectionTrainingGround : DataSection
  {
    public TrainingGround TrainingGround;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionTrainingGround()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void ClearInitedData()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateInstantiatedData(DSTrainingGroundNtf DS)
    {
      // ISSUE: unable to decompile the method.
    }

    public TrainingRoom GetRoom(int RoomId)
    {
      return this.TrainingGround.GetRoom(RoomId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TrainingTech GetTech(int TechId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateTechLevel(int TechId, int Level)
    {
    }
  }
}
