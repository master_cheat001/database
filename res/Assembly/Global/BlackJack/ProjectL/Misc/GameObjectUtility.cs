﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Misc.GameObjectUtility
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using UnityEngine;

namespace BlackJack.ProjectL.Misc
{
  [CustomLuaClass]
  public class GameObjectUtility
  {
    private static GameObject m_sceneRoot;

    public static GameObject SceneRoot
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static GameObject FindChildObject(GameObject parentObj, string[] path)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void FindChildObject(
      GameObject parentObj,
      string[] path,
      Action<GameObject> action)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static GameObject FindChildGameObject_R(GameObject go, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetDefaultName(string assetName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static T AddControllerToGameObject<T>(GameObject go) where T : class
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static PrefabControllerBase AddControllerToGameObject(
      System.Type type,
      GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static GameObject FindGameObjectByName(Transform parent, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static T FindComponentByName<T>(Transform parent, string name) where T : MonoBehaviour
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static GameObject CloneGameObject(GameObject cloneObj, Transform parent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void DestroyChildren(GameObject obj)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void DestroyChildrenImmediate(GameObject obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void DestroyComponentList<T>(List<T> list) where T : MonoBehaviour
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void InactiveComponentList<T>(List<T> list) where T : MonoBehaviour
    {
      // ISSUE: unable to decompile the method.
    }

    public static bool HasChinese(string str)
    {
      return Regex.IsMatch(str, "[\\u4e00-\\u9fa5]");
    }
  }
}
