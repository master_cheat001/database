﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Misc.GameObjectPool2`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.Misc
{
  public class GameObjectPool2<T> where T : MonoBehaviour
  {
    private List<T> m_freeList;
    private GameObject m_prefab;
    private Transform m_parent;

    [MethodImpl((MethodImplOptions) 32768)]
    public GameObjectPool2()
    {
    }

    public void Setup(GameObject prefab, Transform parent)
    {
      this.m_prefab = prefab;
      this.m_parent = parent;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public T Allocate()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public T Allocate(out bool isNew)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Free(T ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Destroy()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
