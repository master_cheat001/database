﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Misc.LocalAccountConfigData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System;
using System.Collections.Generic;

namespace BlackJack.ProjectL.Misc
{
  [CustomLuaClass]
  public class LocalAccountConfigData
  {
    public bool IsRealtimePVPShowNotice = true;
    public string[] HaveReadAnnounceActivities;
    public Dictionary<ulong, string> LastReadActivityTimeList;
    public Dictionary<int, string> LastReadRecommendGiftBoxTimeDic;
    public int[] HaveReadHeroBiographyIds;
    public int[] HaveReadHeroPerformanceIds;
    public int[] UnlockHeroBiographyIds;
    public int[] UnlockHeroPerformanceIds;
    public int[] UnlockHeroDungeonLevelIds;
    public int[] UnlockHeroFetterIds;
    public int[] ArenaAttackerHeroIds;
    public int TeamPlayerLevelMin;
    public int TeamPlayerLevelMax;
    public bool HaveDoneMemoryExtraction;
    public DateTime LastCheckMsgLanguageTime;
    public string[] ChatMsgLanguageArray;
    public string ChatMsgLanguage;
  }
}
