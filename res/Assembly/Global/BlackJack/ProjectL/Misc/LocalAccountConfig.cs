﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Misc.LocalAccountConfig
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Misc
{
  [CustomLuaClass]
  public class LocalAccountConfig
  {
    private static LocalAccountConfig s_instance;
    private static string m_fileName;
    private LocalAccountConfigData m_data;

    [MethodImpl((MethodImplOptions) 32768)]
    public LocalAccountConfig()
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetFileName(string name)
    {
      LocalAccountConfig.m_fileName = name;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Save()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Load()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ResetLocalAccountConfigData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddHaveReadHeroBiography(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddHaveReadHeroPerformance(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddUnlockHeroBiography(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddUnlockHeroPerformance(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddUnlockHeroDungeonLevelId(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddUnlockHeroFetterId(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetArenaAttackerHeroIds(List<int> heroIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHaveDoneMemoryExtraction(bool haveDoneMemoryExtraction)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveExpireActivityLastReadTime(List<OperationalActivityBase> existActivityList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetActivityLastReadTime(ulong activityInstanceID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetActivityLastReadTime(ulong activityInstanceId, DateTime dateTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsNeedDetectLanguage()
    {
      // ISSUE: unable to decompile the method.
    }

    public string GetChatMsgLanguage()
    {
      return this.Data.ChatMsgLanguage;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SaveChatMsgLanguageToArray(string lang, DateTime t)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckAndSaveLanguageToChatMsgLanguage()
    {
      // ISSUE: unable to decompile the method.
    }

    private void SaveLastCheckMsgLanguageTime(DateTime t)
    {
      this.Data.LastCheckMsgLanguageTime = t;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveExpireRecommendGiftBoxReadTime(
      List<ConfigDataGiftStoreItemInfo> giftStoreItemList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetRecommendGiftBoxReadTime(int ID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRecommendGiftBoxReadTime(int ID, DateTime dateTime)
    {
      // ISSUE: unable to decompile the method.
    }

    public LocalAccountConfigData Data
    {
      get
      {
        return this.m_data;
      }
    }

    public static LocalAccountConfig Instance
    {
      set
      {
        LocalAccountConfig.s_instance = value;
      }
      get
      {
        return LocalAccountConfig.s_instance;
      }
    }
  }
}
