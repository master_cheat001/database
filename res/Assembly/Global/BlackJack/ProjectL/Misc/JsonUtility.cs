﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Misc.JsonUtility
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.Common;
using SimpleJson;
using SLua;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Misc
{
  [CustomLuaClass]
  public class JsonUtility
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public static string Serialize(object j)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static object Deserialize(string txt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static T Deserialize<T>(string txt) where T : class
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string SerializeObject(JsonObject j)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string SerializeArray(JsonArray j)
    {
      // ISSUE: unable to decompile the method.
    }

    public static JsonObject DeserializeObject(string txt)
    {
      return JsonUtility.Deserialize(txt) as JsonObject;
    }

    public static JsonArray DeserializeArray(string txt)
    {
      return JsonUtility.Deserialize(txt) as JsonArray;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static JsonArray AddArray(JsonObject j, string key)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static JsonObject AddObject(JsonObject j, string key)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static JsonObject AddObject(JsonArray a)
    {
      // ISSUE: unable to decompile the method.
    }

    public static JsonObject GetObject(JsonObject j, string key)
    {
      return JsonUtility.GetObj(j, key) as JsonObject;
    }

    public static JsonArray GetArray(JsonObject j, string key)
    {
      return JsonUtility.GetObj(j, key) as JsonArray;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static object GetObj(JsonObject j, string key)
    {
      // ISSUE: unable to decompile the method.
    }

    public static void Set(JsonObject j, string key, long value)
    {
      j[key] = (object) value;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Get(JsonObject j, string key, ref long value)
    {
      // ISSUE: unable to decompile the method.
    }

    public static void Set(JsonObject j, string key, int value)
    {
      j[key] = (object) value;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Get(JsonObject j, string key, ref int value)
    {
      // ISSUE: unable to decompile the method.
    }

    public static void Set(JsonObject j, string key, uint value)
    {
      j[key] = (object) value;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Get(JsonObject j, string key, ref uint value)
    {
      // ISSUE: unable to decompile the method.
    }

    public static void Set(JsonObject j, string key, short value)
    {
      j[key] = (object) value;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Get(JsonObject j, string key, ref short value)
    {
      // ISSUE: unable to decompile the method.
    }

    public static void Set(JsonObject j, string key, ushort value)
    {
      j[key] = (object) value;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Get(JsonObject j, string key, ref ushort value)
    {
      // ISSUE: unable to decompile the method.
    }

    public static void Set(JsonObject j, string key, sbyte value)
    {
      j[key] = (object) value;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Get(JsonObject j, string key, ref sbyte value)
    {
      // ISSUE: unable to decompile the method.
    }

    public static void Set(JsonObject j, string key, byte value)
    {
      j[key] = (object) value;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Get(JsonObject j, string key, ref byte value)
    {
      // ISSUE: unable to decompile the method.
    }

    public static void Set(JsonObject j, string key, double value)
    {
      j[key] = (object) value;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Get(JsonObject j, string key, ref double value)
    {
      // ISSUE: unable to decompile the method.
    }

    public static void Set(JsonObject j, string key, float value)
    {
      j[key] = (object) value;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Get(JsonObject j, string key, ref float value)
    {
      // ISSUE: unable to decompile the method.
    }

    public static void Set(JsonObject j, string key, bool value)
    {
      j[key] = (object) value;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Get(JsonObject j, string key, ref bool value)
    {
      // ISSUE: unable to decompile the method.
    }

    public static void Set(JsonObject j, string key, string value)
    {
      j[key] = (object) value;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Get(JsonObject j, string key, ref string value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Set(JsonObject j, string key, DateTime value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Get(JsonObject j, string key, ref DateTime value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Get(JsonObject j, string key, ref PLong value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Get(JsonObject j, string key, ref PInt value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Get(JsonObject j, string key, ref PUInt value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Get(JsonObject j, string key, ref PShort value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Get(JsonObject j, string key, ref PUShort value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Get(JsonObject j, string key, ref PSByte value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Get(JsonObject j, string key, ref PByte value)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
