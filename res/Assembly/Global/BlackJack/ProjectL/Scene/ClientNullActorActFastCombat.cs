﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.ClientNullActorActFastCombat
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using SLua;

namespace BlackJack.ProjectL.Scene
{
  [CustomLuaClass]
  public class ClientNullActorActFastCombat : ClientActorAct
  {
    public FastCombatActorInfo m_fastCombatActorInfoA;
    public FastCombatActorInfo m_fastCombatActorInfoB;
    public ConfigDataSkillInfo m_attackerSkillInfo;
  }
}
