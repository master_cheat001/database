﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.FastCombatActorInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.Battle;
using SLua;

namespace BlackJack.ProjectL.Scene
{
  [CustomLuaClass]
  public class FastCombatActorInfo
  {
    public BattleActor m_battleActor;
    public int m_heroHpMax;
    public int m_soldierHpMax;
    public int m_beforeHeroHp;
    public int m_beforeSoldierHp;
    public int m_afterHeroHp;
    public int m_afterSoldierHp;
    public int m_heroDamage;
    public int m_soldierDamage;
    public bool m_isReceiveCriticalAttack;
  }
}
