﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.CommonReflection.ComponenetManager`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.CommonReflection
{
  public class ComponenetManager<ComponentType> where ComponentType : class, IComponentBase
  {
    private Dictionary<string, ComponentType> m_components;
    private Dictionary<Type, string> m_type2Name;
    private Dictionary<Type, ComponenetManager<ComponentType>.TickableComponentWrapper> m_tickableComponents;
    private IComponentOwner m_owner;

    [MethodImpl((MethodImplOptions) 32768)]
    public ComponenetManager(IComponentOwner owner)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public T Add<T>() where T : class, IComponentBase, new()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Remove<T>() where T : class, IComponentBase
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public T GetComponent<T>() where T : class, IComponentBase
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ComponentType GetComponent(string componentName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ComponentType> GetAllcomponents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SerializeComponents<T>(T dest)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeSerializeComponents<T>(T source)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PostDeSerializeComponents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Tick(uint deltaMillisecond)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PostInitComponents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRemove(IComponentBase component)
    {
      // ISSUE: unable to decompile the method.
    }

    public class TickableComponentWrapper
    {
      public ComponentType Component { get; set; }

      public MethodInfo TickMethodInfo { get; set; }
    }
  }
}
