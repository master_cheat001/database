﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.AR.ARPlaneTrace
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;

namespace BlackJack.ProjectL.AR
{
  [HotFix]
  public class ARPlaneTrace
  {
    public Vector3 centerPos;
    public Quaternion centerRotation;
    public Camera camera;
    private float maxRayDistance;
    private LayerMask collisionLayerMask;
    private float findingSquareDist;
    private ARPlaneTrace.FocusState squareState;
    private bool trackingInitialized;
    [DoNotToLua]
    private ARPlaneTrace.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_get_SquareState_hotfix;
    private LuaFunction m_set_SquareStateFocusState_hotfix;
    private LuaFunction m_InitCamera_hotfix;
    private LuaFunction m_Update_hotfix;
    private LuaFunction m_CalculateLookVectorVector3Vector3_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ARPlaneTrace()
    {
      // ISSUE: unable to decompile the method.
    }

    public ARPlaneTrace.FocusState SquareState
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init(Camera camera)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Quaternion CalculateLookVector(Vector3 srcPos, Vector3 destPos)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public ARPlaneTrace.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public enum FocusState
    {
      Initializing,
      Finding,
      Found,
    }

    public class LuaExportHelper
    {
      private ARPlaneTrace m_owner;

      public LuaExportHelper(ARPlaneTrace owner)
      {
        this.m_owner = owner;
      }

      public float maxRayDistance
      {
        get
        {
          return this.m_owner.maxRayDistance;
        }
        set
        {
          this.m_owner.maxRayDistance = value;
        }
      }

      public LayerMask collisionLayerMask
      {
        get
        {
          return this.m_owner.collisionLayerMask;
        }
        set
        {
          this.m_owner.collisionLayerMask = value;
        }
      }

      public float findingSquareDist
      {
        get
        {
          return this.m_owner.findingSquareDist;
        }
        set
        {
          this.m_owner.findingSquareDist = value;
        }
      }

      public ARPlaneTrace.FocusState squareState
      {
        get
        {
          return this.m_owner.squareState;
        }
        set
        {
          this.m_owner.squareState = value;
        }
      }

      public bool trackingInitialized
      {
        get
        {
          return this.m_owner.trackingInitialized;
        }
        set
        {
          this.m_owner.trackingInitialized = value;
        }
      }

      public Quaternion CalculateLookVector(Vector3 srcPos, Vector3 destPos)
      {
        return this.m_owner.CalculateLookVector(srcPos, destPos);
      }
    }
  }
}
