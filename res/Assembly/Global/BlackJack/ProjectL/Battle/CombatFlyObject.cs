﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.CombatFlyObject
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using FixMath.NET;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Battle
{
  [HotFix]
  public class CombatFlyObject : Entity
  {
    private CombatTeam m_team;
    private Vector2i m_startPosition;
    private Vector2i m_endPosition;
    private int m_startZ;
    private int m_endZ;
    private Fix64 m_vz;
    private int m_life;
    private int m_lifeMax;
    private int m_frameCount;
    private ushort m_hitId;
    private List<DelayHit> m_delayHits;
    private CombatActor m_targetActor;
    private int m_targetTeamNumber;
    private CombatActor m_sourceActor;
    private CombatSkillState m_sourceSkillState;
    private ConfigDataFlyObjectInfo m_flyObjectInfo;
    private IBattleGraphic m_fx;
    private Fix64 m_fxLife;
    private bool m_isGraphicSkillFade;
    [DoNotToLua]
    private CombatFlyObject.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_Dispose_hotfix;
    private LuaFunction m_InitializeCombatTeam_hotfix;
    private LuaFunction m_SetupConfigDataFlyObjectInfoCombatSkillStateCombatActorCombatActor_hotfix;
    private LuaFunction m_IsHero_hotfix;
    private LuaFunction m_SetGraphicSkillFadeBoolean_hotfix;
    private LuaFunction m_SearchHitTargetFix64Fix64UInt16_hotfix;
    private LuaFunction m_Tick_hotfix;
    private LuaFunction m_AddSkillDelayHitsCombatSkillStateFix64Fix64_hotfix;
    private LuaFunction m_AttackCombatActor_hotfix;
    private LuaFunction m_AddDelayHitCombatActorInt32_hotfix;
    private LuaFunction m_GetDelayHitFrameMax_hotfix;
    private LuaFunction m_HasDelayHit_hotfix;
    private LuaFunction m_TickDelayHits_hotfix;
    private LuaFunction m_TickGraphicSingle_hotfix;
    private LuaFunction m_Draw_hotfix;
    private LuaFunction m_DoPauseBoolean_hotfix;
    private LuaFunction m_ComputeParabolicPositionFix64Vector2i_Fix64__hotfix;
    private LuaFunction m_CreateFxStringSingle_hotfix;
    private LuaFunction m_LogCanNotStopCombat_hotfix;
    private LuaFunction m_get_Combat_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public CombatFlyObject()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Dispose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Initialize(CombatTeam team)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Setup(
      ConfigDataFlyObjectInfo flyObjInfo,
      CombatSkillState ss,
      CombatActor sourceActor,
      CombatActor targetActor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsHero()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGraphicSkillFade(bool fade)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int ComputeTargetScore(Fix64 x, CombatActor target)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private CombatActor SearchHitTarget(Fix64 x0, Fix64 x1, ushort hitId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Tick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddSkillDelayHits(CombatSkillState ss, Fix64 x0, Fix64 x1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool Attack(CombatActor target)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddDelayHit(CombatActor target, int delay)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetDelayHitFrameMax()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool HasDelayHit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TickDelayHits()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void TickGraphic(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Draw()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void DoPause(bool pause)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ComputeParabolicPosition(Fix64 life, out Vector2i pos, out Fix64 z)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateFx(string assetName, float scale)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void LogCanNotStopCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    public Combat Combat
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public CombatFlyObject.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Dispose()
    {
      base.Dispose();
    }

    private void __callBase_Tick()
    {
      base.Tick();
    }

    private void __callBase_TickGraphic(float dt)
    {
      base.TickGraphic(dt);
    }

    private void __callBase_Draw()
    {
      base.Draw();
    }

    private void __callBase_Pause(bool pause)
    {
      this.Pause(pause);
    }

    private void __callBase_DoPause(bool pause)
    {
      base.DoPause(pause);
    }

    private void __callBase_DeleteMe()
    {
      this.DeleteMe();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private CombatFlyObject m_owner;

      public LuaExportHelper(CombatFlyObject owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Dispose()
      {
        this.m_owner.__callBase_Dispose();
      }

      public void __callBase_Tick()
      {
        this.m_owner.__callBase_Tick();
      }

      public void __callBase_TickGraphic(float dt)
      {
        this.m_owner.__callBase_TickGraphic(dt);
      }

      public void __callBase_Draw()
      {
        this.m_owner.__callBase_Draw();
      }

      public void __callBase_Pause(bool pause)
      {
        this.m_owner.__callBase_Pause(pause);
      }

      public void __callBase_DoPause(bool pause)
      {
        this.m_owner.__callBase_DoPause(pause);
      }

      public void __callBase_DeleteMe()
      {
        this.m_owner.__callBase_DeleteMe();
      }

      public CombatTeam m_team
      {
        get
        {
          return this.m_owner.m_team;
        }
        set
        {
          this.m_owner.m_team = value;
        }
      }

      public Vector2i m_startPosition
      {
        get
        {
          return this.m_owner.m_startPosition;
        }
        set
        {
          this.m_owner.m_startPosition = value;
        }
      }

      public Vector2i m_endPosition
      {
        get
        {
          return this.m_owner.m_endPosition;
        }
        set
        {
          this.m_owner.m_endPosition = value;
        }
      }

      public int m_startZ
      {
        get
        {
          return this.m_owner.m_startZ;
        }
        set
        {
          this.m_owner.m_startZ = value;
        }
      }

      public int m_endZ
      {
        get
        {
          return this.m_owner.m_endZ;
        }
        set
        {
          this.m_owner.m_endZ = value;
        }
      }

      public Fix64 m_vz
      {
        get
        {
          return this.m_owner.m_vz;
        }
        set
        {
          this.m_owner.m_vz = value;
        }
      }

      public int m_life
      {
        get
        {
          return this.m_owner.m_life;
        }
        set
        {
          this.m_owner.m_life = value;
        }
      }

      public int m_lifeMax
      {
        get
        {
          return this.m_owner.m_lifeMax;
        }
        set
        {
          this.m_owner.m_lifeMax = value;
        }
      }

      public int m_frameCount
      {
        get
        {
          return this.m_owner.m_frameCount;
        }
        set
        {
          this.m_owner.m_frameCount = value;
        }
      }

      public ushort m_hitId
      {
        get
        {
          return this.m_owner.m_hitId;
        }
        set
        {
          this.m_owner.m_hitId = value;
        }
      }

      public List<DelayHit> m_delayHits
      {
        get
        {
          return this.m_owner.m_delayHits;
        }
        set
        {
          this.m_owner.m_delayHits = value;
        }
      }

      public CombatActor m_targetActor
      {
        get
        {
          return this.m_owner.m_targetActor;
        }
        set
        {
          this.m_owner.m_targetActor = value;
        }
      }

      public int m_targetTeamNumber
      {
        get
        {
          return this.m_owner.m_targetTeamNumber;
        }
        set
        {
          this.m_owner.m_targetTeamNumber = value;
        }
      }

      public CombatActor m_sourceActor
      {
        get
        {
          return this.m_owner.m_sourceActor;
        }
        set
        {
          this.m_owner.m_sourceActor = value;
        }
      }

      public CombatSkillState m_sourceSkillState
      {
        get
        {
          return this.m_owner.m_sourceSkillState;
        }
        set
        {
          this.m_owner.m_sourceSkillState = value;
        }
      }

      public ConfigDataFlyObjectInfo m_flyObjectInfo
      {
        get
        {
          return this.m_owner.m_flyObjectInfo;
        }
        set
        {
          this.m_owner.m_flyObjectInfo = value;
        }
      }

      public IBattleGraphic m_fx
      {
        get
        {
          return this.m_owner.m_fx;
        }
        set
        {
          this.m_owner.m_fx = value;
        }
      }

      public Fix64 m_fxLife
      {
        get
        {
          return this.m_owner.m_fxLife;
        }
        set
        {
          this.m_owner.m_fxLife = value;
        }
      }

      public bool m_isGraphicSkillFade
      {
        get
        {
          return this.m_owner.m_isGraphicSkillFade;
        }
        set
        {
          this.m_owner.m_isGraphicSkillFade = value;
        }
      }

      public static int ComputeTargetScore(Fix64 x, CombatActor target)
      {
        return CombatFlyObject.ComputeTargetScore(x, target);
      }

      public CombatActor SearchHitTarget(Fix64 x0, Fix64 x1, ushort hitId)
      {
        return this.m_owner.SearchHitTarget(x0, x1, hitId);
      }

      public void AddSkillDelayHits(CombatSkillState ss, Fix64 x0, Fix64 x1)
      {
        this.m_owner.AddSkillDelayHits(ss, x0, x1);
      }

      public bool Attack(CombatActor target)
      {
        return this.m_owner.Attack(target);
      }

      public void AddDelayHit(CombatActor target, int delay)
      {
        this.m_owner.AddDelayHit(target, delay);
      }

      public int GetDelayHitFrameMax()
      {
        return this.m_owner.GetDelayHitFrameMax();
      }

      public bool HasDelayHit()
      {
        return this.m_owner.HasDelayHit();
      }

      public void TickDelayHits()
      {
        this.m_owner.TickDelayHits();
      }

      public void ComputeParabolicPosition(Fix64 life, out Vector2i pos, out Fix64 z)
      {
        this.m_owner.ComputeParabolicPosition(life, out pos, out z);
      }

      public void CreateFx(string assetName, float scale)
      {
        this.m_owner.CreateFx(assetName, scale);
      }
    }
  }
}
