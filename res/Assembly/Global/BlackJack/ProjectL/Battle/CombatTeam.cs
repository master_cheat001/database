﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.CombatTeam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Battle
{
  [HotFix]
  public class CombatTeam
  {
    private Combat m_combat;
    private int m_teamNumber;
    private BattleActor m_battleActor;
    private List<CombatActor> m_actors;
    private List<CombatFlyObject> m_flyObjects;
    private static Position2i[] s_formationPositions;
    private ConfigDataSkillInfo m_heroSkillInfo;
    private bool m_shouldHeroToHeroCriticalAttack;
    private bool m_shouldHeroToSoldierCriticalAttack;
    private bool m_shouldSoldierToHeroCriticalAttack;
    private bool m_shouldSoldierToSoldierCriticalAttack;
    private bool m_isCastAnyDamageSkill;
    private bool m_isBeCriticalAttack;
    private ConfigDataSkillInfo m_lastDamageBySkillInfo;
    private int m_heroReceiveTotalDamage;
    private int m_soldierReceiveTotalDamage;
    private int m_heroApplyTotalDamage;
    private int m_soldierApplyTotalDamage;
    private int m_reboundPercent;
    private bool m_isTryApplyBuff;
    private ConfigDataSkillInfo m_attachBuffSourceSkillInfo;
    private BuffState m_doubleAttackBuffState;
    [DoNotToLua]
    private CombatTeam.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_InitializeCombatInt32BattleActorConfigDataSkillInfo_hotfix;
    private LuaFunction m_ComputeHeroCriticalAttack_hotfix;
    private LuaFunction m_ComputeSoldierCriticalAttack_hotfix;
    private LuaFunction m_IsAttackHeroOnly_hotfix;
    private LuaFunction m_Tick_hotfix;
    private LuaFunction m_RemoveDeleted_hotfix;
    private LuaFunction m_TickGraphicSingle_hotfix;
    private LuaFunction m_Draw_hotfix;
    private LuaFunction m_PauseBoolean_hotfix;
    private LuaFunction m_GetColor_hotfix;
    private LuaFunction m_CreateActorBoolean_hotfix;
    private LuaFunction m_CreateFlyObject_hotfix;
    private LuaFunction m_RemoveAll_hotfix;
    private LuaFunction m_OnMyActorCastSkillCombatActorConfigDataSkillInfo_hotfix;
    private LuaFunction m_OnMyActorAttackByCombatActorCombatActorConfigDataSkillInfoInt32BooleanArmyRelationData_hotfix;
    private LuaFunction m_OnMyActorReboundDamageCombatActorCombatActorConfigDataSkillInfoInt32Int32_hotfix;
    private LuaFunction m_OnMyActorDieCombatActor_hotfix;
    private LuaFunction m_OnMyActorFightAgainCombatActor_hotfix;
    private LuaFunction m_GetFormationPositionInt32_hotfix;
    private LuaFunction m_GetFormationLineInt32_hotfix;
    private LuaFunction m_GetHero_hotfix;
    private LuaFunction m_GetActors_hotfix;
    private LuaFunction m_EnterCombat_hotfix;
    private LuaFunction m_StartCombat_hotfix;
    private LuaFunction m_StopCombat_hotfix;
    private LuaFunction m_CanStopCombat_hotfix;
    private LuaFunction m_LogCanNotStopCombat_hotfix;
    private LuaFunction m_CanReturn_hotfix;
    private LuaFunction m_HasAliveActor_hotfix;
    private LuaFunction m_GetAliveActorCount_hotfix;
    private LuaFunction m_SetGraphicSkillFadeBoolean_hotfix;
    private LuaFunction m_IsAllAliveActorStateCombatActorStateCombatActorStateCombatActorState_hotfix;
    private LuaFunction m_GetTargetTeam_hotfix;
    private LuaFunction m_ComputeSoldierTotalHealthPoint_hotfix;
    private LuaFunction m_GetLastDamageBySkill_hotfix;
    private LuaFunction m_IsReceiveAnyDamage_hotfix;
    private LuaFunction m_GetAttachBuffSourceSkillInfo_hotfix;
    private LuaFunction m_GetDoubleAttackBuff_hotfix;
    private LuaFunction m_get_Combat_hotfix;
    private LuaFunction m_get_TeamNumber_hotfix;
    private LuaFunction m_get_BattleActor_hotfix;
    private LuaFunction m_get_HeroInfo_hotfix;
    private LuaFunction m_get_HeroArmyInfo_hotfix;
    private LuaFunction m_get_JobConnectionInfo_hotfix;
    private LuaFunction m_get_JobInfo_hotfix;
    private LuaFunction m_get_SoldierInfo_hotfix;
    private LuaFunction m_get_SoldierArmyInfo_hotfix;
    private LuaFunction m_get_HeroSkillInfo_hotfix;
    private LuaFunction m_get_HeroLevel_hotfix;
    private LuaFunction m_get_HeroStar_hotfix;
    private LuaFunction m_get_JobLevel_hotfix;
    private LuaFunction m_get_ShouldHeroToHeroCriticalAttack_hotfix;
    private LuaFunction m_get_ShouldHeroToSoldierCriticalAttack_hotfix;
    private LuaFunction m_get_ShouldSoldierToHeroCriticalAttack_hotfix;
    private LuaFunction m_get_ShouldSoldierToSoldierCriticalAttack_hotfix;
    private LuaFunction m_get_IsCastAnyDamageSkill_hotfix;
    private LuaFunction m_get_IsBeCriticalAttack_hotfix;
    private LuaFunction m_get_HeroReceiveTotalDamage_hotfix;
    private LuaFunction m_get_SoldierReceiveTotalDamage_hotfix;
    private LuaFunction m_get_HeroApplyTotalDamage_hotfix;
    private LuaFunction m_get_SoldierApplyTotalDamage_hotfix;
    private LuaFunction m_get_ReboundPercent_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public CombatTeam()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    static CombatTeam()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Initialize(
      Combat combat,
      int team,
      BattleActor battleActor,
      ConfigDataSkillInfo heroSkillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ComputeHeroCriticalAttack()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ComputeSoldierCriticalAttack()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsAttackHeroOnly()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Tick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveDeleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void TickGraphic(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Draw()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Pause(bool pause)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Colori GetColor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CombatActor CreateActor(bool isHero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CombatFlyObject CreateFlyObject()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveAll()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnMyActorCastSkill(CombatActor a, ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnMyActorAttackBy(
      CombatActor a,
      CombatActor attacker,
      ConfigDataSkillInfo skillInfo,
      int hpModify,
      bool isCritical,
      ArmyRelationData armyRelation)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnMyActorReboundDamage(
      CombatActor a,
      CombatActor attacker,
      ConfigDataSkillInfo skillInfo,
      int hpModify,
      int hpReboundPercent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnMyActorDie(CombatActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnMyActorFightAgain(CombatActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Position2i GetFormationPosition(int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetFormationLine(int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CombatActor GetHero()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<CombatActor> GetActors()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void EnterCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StopCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanStopCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void LogCanNotStopCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool HasAliveActor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetAliveActorCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGraphicSkillFade(bool fade)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsAllAliveActorState(
      CombatActorState state1,
      CombatActorState state2,
      CombatActorState state3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CombatTeam GetTargetTeam()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ComputeSoldierTotalHealthPoint()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSkillInfo GetLastDamageBySkill()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsReceiveAnyDamage()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSkillInfo GetAttachBuffSourceSkillInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BuffState GetDoubleAttackBuff()
    {
      // ISSUE: unable to decompile the method.
    }

    public Combat Combat
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int TeamNumber
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public BattleActor BattleActor
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataHeroInfo HeroInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataArmyInfo HeroArmyInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataJobConnectionInfo JobConnectionInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataJobInfo JobInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataSoldierInfo SoldierInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataArmyInfo SoldierArmyInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataSkillInfo HeroSkillInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int HeroLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int HeroStar
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int JobLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool ShouldHeroToHeroCriticalAttack
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool ShouldHeroToSoldierCriticalAttack
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool ShouldSoldierToHeroCriticalAttack
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool ShouldSoldierToSoldierCriticalAttack
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsCastAnyDamageSkill
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsBeCriticalAttack
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int HeroReceiveTotalDamage
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int SoldierReceiveTotalDamage
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int HeroApplyTotalDamage
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int SoldierApplyTotalDamage
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ReboundPercent
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public CombatTeam.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private CombatTeam m_owner;

      public LuaExportHelper(CombatTeam owner)
      {
        this.m_owner = owner;
      }

      public Combat m_combat
      {
        get
        {
          return this.m_owner.m_combat;
        }
        set
        {
          this.m_owner.m_combat = value;
        }
      }

      public int m_teamNumber
      {
        get
        {
          return this.m_owner.m_teamNumber;
        }
        set
        {
          this.m_owner.m_teamNumber = value;
        }
      }

      public BattleActor m_battleActor
      {
        get
        {
          return this.m_owner.m_battleActor;
        }
        set
        {
          this.m_owner.m_battleActor = value;
        }
      }

      public List<CombatActor> m_actors
      {
        get
        {
          return this.m_owner.m_actors;
        }
        set
        {
          this.m_owner.m_actors = value;
        }
      }

      public List<CombatFlyObject> m_flyObjects
      {
        get
        {
          return this.m_owner.m_flyObjects;
        }
        set
        {
          this.m_owner.m_flyObjects = value;
        }
      }

      public static Position2i[] s_formationPositions
      {
        get
        {
          return CombatTeam.s_formationPositions;
        }
        set
        {
          CombatTeam.s_formationPositions = value;
        }
      }

      public ConfigDataSkillInfo m_heroSkillInfo
      {
        get
        {
          return this.m_owner.m_heroSkillInfo;
        }
        set
        {
          this.m_owner.m_heroSkillInfo = value;
        }
      }

      public bool m_shouldHeroToHeroCriticalAttack
      {
        get
        {
          return this.m_owner.m_shouldHeroToHeroCriticalAttack;
        }
        set
        {
          this.m_owner.m_shouldHeroToHeroCriticalAttack = value;
        }
      }

      public bool m_shouldHeroToSoldierCriticalAttack
      {
        get
        {
          return this.m_owner.m_shouldHeroToSoldierCriticalAttack;
        }
        set
        {
          this.m_owner.m_shouldHeroToSoldierCriticalAttack = value;
        }
      }

      public bool m_shouldSoldierToHeroCriticalAttack
      {
        get
        {
          return this.m_owner.m_shouldSoldierToHeroCriticalAttack;
        }
        set
        {
          this.m_owner.m_shouldSoldierToHeroCriticalAttack = value;
        }
      }

      public bool m_shouldSoldierToSoldierCriticalAttack
      {
        get
        {
          return this.m_owner.m_shouldSoldierToSoldierCriticalAttack;
        }
        set
        {
          this.m_owner.m_shouldSoldierToSoldierCriticalAttack = value;
        }
      }

      public bool m_isCastAnyDamageSkill
      {
        get
        {
          return this.m_owner.m_isCastAnyDamageSkill;
        }
        set
        {
          this.m_owner.m_isCastAnyDamageSkill = value;
        }
      }

      public bool m_isBeCriticalAttack
      {
        get
        {
          return this.m_owner.m_isBeCriticalAttack;
        }
        set
        {
          this.m_owner.m_isBeCriticalAttack = value;
        }
      }

      public ConfigDataSkillInfo m_lastDamageBySkillInfo
      {
        get
        {
          return this.m_owner.m_lastDamageBySkillInfo;
        }
        set
        {
          this.m_owner.m_lastDamageBySkillInfo = value;
        }
      }

      public int m_heroReceiveTotalDamage
      {
        get
        {
          return this.m_owner.m_heroReceiveTotalDamage;
        }
        set
        {
          this.m_owner.m_heroReceiveTotalDamage = value;
        }
      }

      public int m_soldierReceiveTotalDamage
      {
        get
        {
          return this.m_owner.m_soldierReceiveTotalDamage;
        }
        set
        {
          this.m_owner.m_soldierReceiveTotalDamage = value;
        }
      }

      public int m_heroApplyTotalDamage
      {
        get
        {
          return this.m_owner.m_heroApplyTotalDamage;
        }
        set
        {
          this.m_owner.m_heroApplyTotalDamage = value;
        }
      }

      public int m_soldierApplyTotalDamage
      {
        get
        {
          return this.m_owner.m_soldierApplyTotalDamage;
        }
        set
        {
          this.m_owner.m_soldierApplyTotalDamage = value;
        }
      }

      public int m_reboundPercent
      {
        get
        {
          return this.m_owner.m_reboundPercent;
        }
        set
        {
          this.m_owner.m_reboundPercent = value;
        }
      }

      public bool m_isTryApplyBuff
      {
        get
        {
          return this.m_owner.m_isTryApplyBuff;
        }
        set
        {
          this.m_owner.m_isTryApplyBuff = value;
        }
      }

      public ConfigDataSkillInfo m_attachBuffSourceSkillInfo
      {
        get
        {
          return this.m_owner.m_attachBuffSourceSkillInfo;
        }
        set
        {
          this.m_owner.m_attachBuffSourceSkillInfo = value;
        }
      }

      public BuffState m_doubleAttackBuffState
      {
        get
        {
          return this.m_owner.m_doubleAttackBuffState;
        }
        set
        {
          this.m_owner.m_doubleAttackBuffState = value;
        }
      }

      public void ComputeHeroCriticalAttack()
      {
        this.m_owner.ComputeHeroCriticalAttack();
      }

      public void ComputeSoldierCriticalAttack()
      {
        this.m_owner.ComputeSoldierCriticalAttack();
      }

      public bool HasAliveActor()
      {
        return this.m_owner.HasAliveActor();
      }

      public int GetAliveActorCount()
      {
        return this.m_owner.GetAliveActorCount();
      }

      public bool IsAllAliveActorState(
        CombatActorState state1,
        CombatActorState state2,
        CombatActorState state3)
      {
        return this.m_owner.IsAllAliveActorState(state1, state2, state3);
      }
    }
  }
}
