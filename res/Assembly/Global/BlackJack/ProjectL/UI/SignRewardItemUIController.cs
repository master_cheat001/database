﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.SignRewardItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using SLua;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class SignRewardItemUIController : UIControllerBase, IPointerClickHandler, IEventSystemHandler
  {
    [AutoBind("./ItemGroup/RewardGoodsDummy", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_signItemGoodsDummy;
    [AutoBind("./FrameImage", AutoBindAttribute.InitState.NotInit, false)]
    public Image m_itemFrameImage;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_signItemUIStateCtrl;
    [AutoBind("./DayText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_dayText;
    private SignRewardItemUIController.SignState m_signState;
    private GoodsType m_goodsType;
    private int m_goodsId;
    private int m_goodsCount;
    private ConfigDataItemInfo m_itemInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPointerClick(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetReward(Goods goods)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDay(int day)
    {
      // ISSUE: unable to decompile the method.
    }

    public void PlaySignAnimation(Action onEnd)
    {
      this.StartCoroutine(this.Co_ChangeStateToSigning(onEnd));
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_ChangeStateToSigning(Action onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void TodayAutoSign()
    {
      // ISSUE: unable to decompile the method.
    }

    public GoodsType GetGoodsType()
    {
      return this.m_goodsType;
    }

    public int GetGoodsId()
    {
      return this.m_goodsId;
    }

    public int GetGoodsCount()
    {
      return this.m_goodsCount;
    }

    public event Action EventOnSignTodayItemClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    public event Action<GoodsType, int, int> EventOnSignTodayBoxOpenClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    public event Action<List<Goods>, SignRewardItemUIController> EventOnShowBoxRewards
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    public enum SignState
    {
      Signed,
      NeedSign,
      NotSign,
    }
  }
}
