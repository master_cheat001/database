﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BattleResultUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class BattleResultUIController : UIControllerBase
  {
    [AutoBind("./BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_backgroundButton;
    [AutoBind("./AccountingPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_accountingPanelUIStateController;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/Stars", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_starsGameObject;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/Stars/Star1", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_star1UIStateController;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/Stars/Star2", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_star2UIStateController;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/Stars/Star2/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_star2Text;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/Stars/Star3", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_star3UIStateController;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/Stars/Star3/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_star3Text;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/ArenaResult", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_arenaResultGameObject;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/ArenaResult/ArenaPoint/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_arenaPointText;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/ArenaResult/VictoryPoint/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_arenaVictoryPointText;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/ArenaResult/ArenaPoint/AutoFightText", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_arenaAutoFightGameObject;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/RealtimePVPResult", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_realtimePVPResultGameObject;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/RealtimePVPResult/Score/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_realtimePVPScoreText;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/RealtimePVPResult/Win/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_realtimePVPWinText;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/RealtimePVPResult/Score/WinBonusText", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_realtimePVPWinBonusGameObject;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/RealtimePVPResult/Score/WinBonusText/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_realtimePVPWinBonusText;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/Gold", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_goldGameObject;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/Exp", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_expGameObject;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/Gold/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_goldText;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/Exp/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerExpText;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/Exp/EXP/LvNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerGroupLvText;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/Exp/EXP/ExpProgressBar", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_playerExpImage;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/Exp/LevelUpEffect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_playerLevelUpGameObject;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/HeroExp/HeroGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroGroupGameObject;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/HeroExp", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroExpGameObject;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/EnemyBoomToGold/Gold/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enemyBoomToGoldText;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/EnemyBoomToGold", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_enemyBoomToGoldGameObject;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/EnemyBoomToGold/TreasureMapRewardScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_treasureMapRewardGroupGameObject;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/EnemyBoomToGold/TreasureMapRewardScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_treasureMapRewardGroupScrollRect;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/EnemyBoomToGold/TreasureMapRewardScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GridLayoutGroup m_treasureMapRewardGroupGridLayputGroup;
    [AutoBind("./AchievementPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_achievementPanelUIStateController;
    [AutoBind("./AchievementPanel/AchievementInfo", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_achievementGameObject;
    [AutoBind("./ClickScreenContinue", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_clickScreenContinueGameObject;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefabs/HeroItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroItemPrefab;
    [AutoBind("./Prefabs/EnemyItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_enemyItemPrefab;
    private List<RewardHeroUIController> m_rewardHeros;
    private BattleAchievementItemUIController m_achievementItemUIController;
    private bool m_isClick;
    private PlayerLevelUpUITask m_playerLevelUpUITask;
    private float m_playerExpTotalWidth;
    private float m_singleAddExp;
    private float m_beforePlayerExp;
    private int m_finalPlayerExp;
    private float m_singleAddGold;
    private float m_beforePlayerGold;
    private int m_finalPlayerGold;
    [DoNotToLua]
    private BattleResultUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_ShowBattleResultBattleTypeList`1Int32Int32Int32Int32BattleRewardList`1BattleLevelAchievementbe_hotfix;
    private LuaFunction m_Co_ShowBattleResultBattleTypeList`1Int32Int32Int32Int32BattleRewardList`1BattleLevelAchievementbe_hotfix;
    private LuaFunction m_AddPlayerExpBarWidth_hotfix;
    private LuaFunction m_UpdateTextValueSingle_hotfix;
    private LuaFunction m_Co_ShowStarsBattleTypeInt32Int32Int32Int32_hotfix;
    private LuaFunction m_Co_ShowHerosBattleRewardList`1_hotfix;
    private LuaFunction m_Co_ShowEnemyBoomToGold_hotfix;
    private LuaFunction m_AutoMoveItemWhenOutOfScrollRectEnemyBoomToGoldUIController_hotfix;
    private LuaFunction m_Co_ShowPlayerLevelUpInt32Int32_hotfix;
    private LuaFunction m_Co_ShowAchievementsList`1BattleLevelAchievementbe_hotfix;
    private LuaFunction m_Co_SetGoldValueChangedSingle_hotfix;
    private LuaFunction m_Co_SetAndWaitUIStateCommonUIStateControllerString_hotfix;
    private LuaFunction m_Co_WaitClick_hotfix;
    private LuaFunction m_PlayerLevelUpUITask_OnClose_hotfix;
    private LuaFunction m_ClearHeroItems_hotfix;
    private LuaFunction m_OnBackgroundButtonClick_hotfix;
    private LuaFunction m_add_EventOnCloseAction_hotfix;
    private LuaFunction m_remove_EventOnCloseAction_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    private BattleResultUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBattleResult(
      BattleType battleType,
      List<Hero> heros,
      int stars,
      int starTurnMax,
      int starDeadMax,
      int turn,
      BattleReward reward,
      List<int> gotAchievements,
      BattleLevelAchievement[] achievements)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_ShowBattleResult(
      BattleType battleType,
      List<Hero> heros,
      int stars,
      int starTurnMax,
      int starDeadMax,
      int turn,
      BattleReward reward,
      List<int> gotAchievements,
      BattleLevelAchievement[] achievements)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator AddPlayerExpBarWidth()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateTextValue(float scale)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_ShowStars(
      BattleType battleType,
      int stars,
      int starTurnMax,
      int starDeadMax,
      int turn)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_ShowHeros(BattleReward reward, List<Hero> heros)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_ShowEnemyBoomToGold()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AutoMoveItemWhenOutOfScrollRect(EnemyBoomToGoldUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_ShowPlayerLevelUp(int oldLevel, int newLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_ShowAchievements(
      List<int> gotAchievements,
      BattleLevelAchievement[] achievements)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_SetGoldValueChanged(float newValue)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_SetAndWaitUIState(CommonUIStateController ctrl, string state)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_WaitClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerLevelUpUITask_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearHeroItems()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public BattleResultUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnClose()
    {
      this.EventOnClose = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private BattleResultUIController m_owner;

      public LuaExportHelper(BattleResultUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnClose()
      {
        this.m_owner.__callDele_EventOnClose();
      }

      public void __clearDele_EventOnClose()
      {
        this.m_owner.__clearDele_EventOnClose();
      }

      public Button m_backgroundButton
      {
        get
        {
          return this.m_owner.m_backgroundButton;
        }
        set
        {
          this.m_owner.m_backgroundButton = value;
        }
      }

      public CommonUIStateController m_accountingPanelUIStateController
      {
        get
        {
          return this.m_owner.m_accountingPanelUIStateController;
        }
        set
        {
          this.m_owner.m_accountingPanelUIStateController = value;
        }
      }

      public GameObject m_starsGameObject
      {
        get
        {
          return this.m_owner.m_starsGameObject;
        }
        set
        {
          this.m_owner.m_starsGameObject = value;
        }
      }

      public CommonUIStateController m_star1UIStateController
      {
        get
        {
          return this.m_owner.m_star1UIStateController;
        }
        set
        {
          this.m_owner.m_star1UIStateController = value;
        }
      }

      public CommonUIStateController m_star2UIStateController
      {
        get
        {
          return this.m_owner.m_star2UIStateController;
        }
        set
        {
          this.m_owner.m_star2UIStateController = value;
        }
      }

      public Text m_star2Text
      {
        get
        {
          return this.m_owner.m_star2Text;
        }
        set
        {
          this.m_owner.m_star2Text = value;
        }
      }

      public CommonUIStateController m_star3UIStateController
      {
        get
        {
          return this.m_owner.m_star3UIStateController;
        }
        set
        {
          this.m_owner.m_star3UIStateController = value;
        }
      }

      public Text m_star3Text
      {
        get
        {
          return this.m_owner.m_star3Text;
        }
        set
        {
          this.m_owner.m_star3Text = value;
        }
      }

      public GameObject m_arenaResultGameObject
      {
        get
        {
          return this.m_owner.m_arenaResultGameObject;
        }
        set
        {
          this.m_owner.m_arenaResultGameObject = value;
        }
      }

      public Text m_arenaPointText
      {
        get
        {
          return this.m_owner.m_arenaPointText;
        }
        set
        {
          this.m_owner.m_arenaPointText = value;
        }
      }

      public Text m_arenaVictoryPointText
      {
        get
        {
          return this.m_owner.m_arenaVictoryPointText;
        }
        set
        {
          this.m_owner.m_arenaVictoryPointText = value;
        }
      }

      public GameObject m_arenaAutoFightGameObject
      {
        get
        {
          return this.m_owner.m_arenaAutoFightGameObject;
        }
        set
        {
          this.m_owner.m_arenaAutoFightGameObject = value;
        }
      }

      public GameObject m_realtimePVPResultGameObject
      {
        get
        {
          return this.m_owner.m_realtimePVPResultGameObject;
        }
        set
        {
          this.m_owner.m_realtimePVPResultGameObject = value;
        }
      }

      public Text m_realtimePVPScoreText
      {
        get
        {
          return this.m_owner.m_realtimePVPScoreText;
        }
        set
        {
          this.m_owner.m_realtimePVPScoreText = value;
        }
      }

      public Text m_realtimePVPWinText
      {
        get
        {
          return this.m_owner.m_realtimePVPWinText;
        }
        set
        {
          this.m_owner.m_realtimePVPWinText = value;
        }
      }

      public GameObject m_realtimePVPWinBonusGameObject
      {
        get
        {
          return this.m_owner.m_realtimePVPWinBonusGameObject;
        }
        set
        {
          this.m_owner.m_realtimePVPWinBonusGameObject = value;
        }
      }

      public Text m_realtimePVPWinBonusText
      {
        get
        {
          return this.m_owner.m_realtimePVPWinBonusText;
        }
        set
        {
          this.m_owner.m_realtimePVPWinBonusText = value;
        }
      }

      public GameObject m_goldGameObject
      {
        get
        {
          return this.m_owner.m_goldGameObject;
        }
        set
        {
          this.m_owner.m_goldGameObject = value;
        }
      }

      public GameObject m_expGameObject
      {
        get
        {
          return this.m_owner.m_expGameObject;
        }
        set
        {
          this.m_owner.m_expGameObject = value;
        }
      }

      public Text m_goldText
      {
        get
        {
          return this.m_owner.m_goldText;
        }
        set
        {
          this.m_owner.m_goldText = value;
        }
      }

      public Text m_playerExpText
      {
        get
        {
          return this.m_owner.m_playerExpText;
        }
        set
        {
          this.m_owner.m_playerExpText = value;
        }
      }

      public Text m_playerGroupLvText
      {
        get
        {
          return this.m_owner.m_playerGroupLvText;
        }
        set
        {
          this.m_owner.m_playerGroupLvText = value;
        }
      }

      public Image m_playerExpImage
      {
        get
        {
          return this.m_owner.m_playerExpImage;
        }
        set
        {
          this.m_owner.m_playerExpImage = value;
        }
      }

      public GameObject m_playerLevelUpGameObject
      {
        get
        {
          return this.m_owner.m_playerLevelUpGameObject;
        }
        set
        {
          this.m_owner.m_playerLevelUpGameObject = value;
        }
      }

      public GameObject m_heroGroupGameObject
      {
        get
        {
          return this.m_owner.m_heroGroupGameObject;
        }
        set
        {
          this.m_owner.m_heroGroupGameObject = value;
        }
      }

      public GameObject m_heroExpGameObject
      {
        get
        {
          return this.m_owner.m_heroExpGameObject;
        }
        set
        {
          this.m_owner.m_heroExpGameObject = value;
        }
      }

      public Text m_enemyBoomToGoldText
      {
        get
        {
          return this.m_owner.m_enemyBoomToGoldText;
        }
        set
        {
          this.m_owner.m_enemyBoomToGoldText = value;
        }
      }

      public GameObject m_enemyBoomToGoldGameObject
      {
        get
        {
          return this.m_owner.m_enemyBoomToGoldGameObject;
        }
        set
        {
          this.m_owner.m_enemyBoomToGoldGameObject = value;
        }
      }

      public GameObject m_treasureMapRewardGroupGameObject
      {
        get
        {
          return this.m_owner.m_treasureMapRewardGroupGameObject;
        }
        set
        {
          this.m_owner.m_treasureMapRewardGroupGameObject = value;
        }
      }

      public ScrollRect m_treasureMapRewardGroupScrollRect
      {
        get
        {
          return this.m_owner.m_treasureMapRewardGroupScrollRect;
        }
        set
        {
          this.m_owner.m_treasureMapRewardGroupScrollRect = value;
        }
      }

      public GridLayoutGroup m_treasureMapRewardGroupGridLayputGroup
      {
        get
        {
          return this.m_owner.m_treasureMapRewardGroupGridLayputGroup;
        }
        set
        {
          this.m_owner.m_treasureMapRewardGroupGridLayputGroup = value;
        }
      }

      public CommonUIStateController m_achievementPanelUIStateController
      {
        get
        {
          return this.m_owner.m_achievementPanelUIStateController;
        }
        set
        {
          this.m_owner.m_achievementPanelUIStateController = value;
        }
      }

      public GameObject m_achievementGameObject
      {
        get
        {
          return this.m_owner.m_achievementGameObject;
        }
        set
        {
          this.m_owner.m_achievementGameObject = value;
        }
      }

      public GameObject m_clickScreenContinueGameObject
      {
        get
        {
          return this.m_owner.m_clickScreenContinueGameObject;
        }
        set
        {
          this.m_owner.m_clickScreenContinueGameObject = value;
        }
      }

      public GameObject m_prefabsGameObject
      {
        get
        {
          return this.m_owner.m_prefabsGameObject;
        }
        set
        {
          this.m_owner.m_prefabsGameObject = value;
        }
      }

      public GameObject m_heroItemPrefab
      {
        get
        {
          return this.m_owner.m_heroItemPrefab;
        }
        set
        {
          this.m_owner.m_heroItemPrefab = value;
        }
      }

      public GameObject m_enemyItemPrefab
      {
        get
        {
          return this.m_owner.m_enemyItemPrefab;
        }
        set
        {
          this.m_owner.m_enemyItemPrefab = value;
        }
      }

      public List<RewardHeroUIController> m_rewardHeros
      {
        get
        {
          return this.m_owner.m_rewardHeros;
        }
        set
        {
          this.m_owner.m_rewardHeros = value;
        }
      }

      public BattleAchievementItemUIController m_achievementItemUIController
      {
        get
        {
          return this.m_owner.m_achievementItemUIController;
        }
        set
        {
          this.m_owner.m_achievementItemUIController = value;
        }
      }

      public bool m_isClick
      {
        get
        {
          return this.m_owner.m_isClick;
        }
        set
        {
          this.m_owner.m_isClick = value;
        }
      }

      public PlayerLevelUpUITask m_playerLevelUpUITask
      {
        get
        {
          return this.m_owner.m_playerLevelUpUITask;
        }
        set
        {
          this.m_owner.m_playerLevelUpUITask = value;
        }
      }

      public float m_playerExpTotalWidth
      {
        get
        {
          return this.m_owner.m_playerExpTotalWidth;
        }
        set
        {
          this.m_owner.m_playerExpTotalWidth = value;
        }
      }

      public float m_singleAddExp
      {
        get
        {
          return this.m_owner.m_singleAddExp;
        }
        set
        {
          this.m_owner.m_singleAddExp = value;
        }
      }

      public float m_beforePlayerExp
      {
        get
        {
          return this.m_owner.m_beforePlayerExp;
        }
        set
        {
          this.m_owner.m_beforePlayerExp = value;
        }
      }

      public int m_finalPlayerExp
      {
        get
        {
          return this.m_owner.m_finalPlayerExp;
        }
        set
        {
          this.m_owner.m_finalPlayerExp = value;
        }
      }

      public float m_singleAddGold
      {
        get
        {
          return this.m_owner.m_singleAddGold;
        }
        set
        {
          this.m_owner.m_singleAddGold = value;
        }
      }

      public float m_beforePlayerGold
      {
        get
        {
          return this.m_owner.m_beforePlayerGold;
        }
        set
        {
          this.m_owner.m_beforePlayerGold = value;
        }
      }

      public int m_finalPlayerGold
      {
        get
        {
          return this.m_owner.m_finalPlayerGold;
        }
        set
        {
          this.m_owner.m_finalPlayerGold = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public IEnumerator Co_ShowBattleResult(
        BattleType battleType,
        List<Hero> heros,
        int stars,
        int starTurnMax,
        int starDeadMax,
        int turn,
        BattleReward reward,
        List<int> gotAchievements,
        BattleLevelAchievement[] achievements)
      {
        // ISSUE: unable to decompile the method.
      }

      public IEnumerator AddPlayerExpBarWidth()
      {
        return this.m_owner.AddPlayerExpBarWidth();
      }

      public void UpdateTextValue(float scale)
      {
        this.m_owner.UpdateTextValue(scale);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public IEnumerator Co_ShowStars(
        BattleType battleType,
        int stars,
        int starTurnMax,
        int starDeadMax,
        int turn)
      {
        // ISSUE: unable to decompile the method.
      }

      public IEnumerator Co_ShowHeros(BattleReward reward, List<Hero> heros)
      {
        return this.m_owner.Co_ShowHeros(reward, heros);
      }

      public IEnumerator Co_ShowEnemyBoomToGold()
      {
        return this.m_owner.Co_ShowEnemyBoomToGold();
      }

      public void AutoMoveItemWhenOutOfScrollRect(EnemyBoomToGoldUIController ctrl)
      {
        this.m_owner.AutoMoveItemWhenOutOfScrollRect(ctrl);
      }

      public IEnumerator Co_ShowPlayerLevelUp(int oldLevel, int newLevel)
      {
        return this.m_owner.Co_ShowPlayerLevelUp(oldLevel, newLevel);
      }

      public IEnumerator Co_ShowAchievements(
        List<int> gotAchievements,
        BattleLevelAchievement[] achievements)
      {
        return this.m_owner.Co_ShowAchievements(gotAchievements, achievements);
      }

      public IEnumerator Co_SetGoldValueChanged(float newValue)
      {
        return this.m_owner.Co_SetGoldValueChanged(newValue);
      }

      public IEnumerator Co_SetAndWaitUIState(CommonUIStateController ctrl, string state)
      {
        return this.m_owner.Co_SetAndWaitUIState(ctrl, state);
      }

      public IEnumerator Co_WaitClick()
      {
        return this.m_owner.Co_WaitClick();
      }

      public void PlayerLevelUpUITask_OnClose()
      {
        this.m_owner.PlayerLevelUpUITask_OnClose();
      }

      public void ClearHeroItems()
      {
        this.m_owner.ClearHeroItems();
      }

      public void OnBackgroundButtonClick()
      {
        this.m_owner.OnBackgroundButtonClick();
      }
    }
  }
}
