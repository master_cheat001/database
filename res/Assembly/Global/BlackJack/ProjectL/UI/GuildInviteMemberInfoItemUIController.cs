﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.GuildInviteMemberInfoItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class GuildInviteMemberInfoItemUIController : UIControllerBase
  {
    [AutoBind("./Char", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_headIconStateCtrl;
    [AutoBind("./Char/HeadIcon/HeadIconGrey", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_headIconGrey;
    [AutoBind("./Char/HeadIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_headIcon;
    [AutoBind("./Char/HeadIcon/HeadFrameDummy", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_HeadFrameDummy;
    [AutoBind("./Char/HeadIcon/LevelText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_levelText;
    [AutoBind("./Char/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameText;
    [AutoBind("./PeopleValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_powerText;
    [AutoBind("./AddButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_addButton;
    [AutoBind("./AddButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_addButtonStateCtrl;
    [AutoBind("./StateGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_onlineStateCtrl;
    [AutoBind("./StateGroup/OffLine/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_OffLineText;
    private bool m_isAdd;
    [DoNotToLua]
    private GuildInviteMemberInfoItemUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_InitPlayerInfoUserSummaryBoolean_hotfix;
    private LuaFunction m_OnAddClick_hotfix;
    private LuaFunction m_add_EventOnClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnClickAction`1_hotfix;
    private LuaFunction m_get_InvitePlayer_hotfix;
    private LuaFunction m_set_InvitePlayerUserSummary_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitPlayerInfo(UserSummary player, bool isInvited)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<GuildInviteMemberInfoItemUIController> EventOnClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public UserSummary InvitePlayer
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public GuildInviteMemberInfoItemUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnClick(GuildInviteMemberInfoItemUIController obj)
    {
    }

    private void __clearDele_EventOnClick(GuildInviteMemberInfoItemUIController obj)
    {
      this.EventOnClick = (Action<GuildInviteMemberInfoItemUIController>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private GuildInviteMemberInfoItemUIController m_owner;

      public LuaExportHelper(GuildInviteMemberInfoItemUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnClick(GuildInviteMemberInfoItemUIController obj)
      {
        this.m_owner.__callDele_EventOnClick(obj);
      }

      public void __clearDele_EventOnClick(GuildInviteMemberInfoItemUIController obj)
      {
        this.m_owner.__clearDele_EventOnClick(obj);
      }

      public CommonUIStateController m_headIconStateCtrl
      {
        get
        {
          return this.m_owner.m_headIconStateCtrl;
        }
        set
        {
          this.m_owner.m_headIconStateCtrl = value;
        }
      }

      public Image m_headIconGrey
      {
        get
        {
          return this.m_owner.m_headIconGrey;
        }
        set
        {
          this.m_owner.m_headIconGrey = value;
        }
      }

      public Image m_headIcon
      {
        get
        {
          return this.m_owner.m_headIcon;
        }
        set
        {
          this.m_owner.m_headIcon = value;
        }
      }

      public Transform m_HeadFrameDummy
      {
        get
        {
          return this.m_owner.m_HeadFrameDummy;
        }
        set
        {
          this.m_owner.m_HeadFrameDummy = value;
        }
      }

      public Text m_levelText
      {
        get
        {
          return this.m_owner.m_levelText;
        }
        set
        {
          this.m_owner.m_levelText = value;
        }
      }

      public Text m_nameText
      {
        get
        {
          return this.m_owner.m_nameText;
        }
        set
        {
          this.m_owner.m_nameText = value;
        }
      }

      public Text m_powerText
      {
        get
        {
          return this.m_owner.m_powerText;
        }
        set
        {
          this.m_owner.m_powerText = value;
        }
      }

      public Button m_addButton
      {
        get
        {
          return this.m_owner.m_addButton;
        }
        set
        {
          this.m_owner.m_addButton = value;
        }
      }

      public CommonUIStateController m_addButtonStateCtrl
      {
        get
        {
          return this.m_owner.m_addButtonStateCtrl;
        }
        set
        {
          this.m_owner.m_addButtonStateCtrl = value;
        }
      }

      public CommonUIStateController m_onlineStateCtrl
      {
        get
        {
          return this.m_owner.m_onlineStateCtrl;
        }
        set
        {
          this.m_owner.m_onlineStateCtrl = value;
        }
      }

      public Text m_OffLineText
      {
        get
        {
          return this.m_owner.m_OffLineText;
        }
        set
        {
          this.m_owner.m_OffLineText = value;
        }
      }

      public bool m_isAdd
      {
        get
        {
          return this.m_owner.m_isAdd;
        }
        set
        {
          this.m_owner.m_isAdd = value;
        }
      }

      public UserSummary InvitePlayer
      {
        set
        {
          this.m_owner.InvitePlayer = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void OnAddClick()
      {
        this.m_owner.OnAddClick();
      }
    }
  }
}
