﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PointDescComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.UI;
using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.EventSystems;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class PointDescComponent : UIControllerBase, IPointerClickHandler, IPointerDownHandler, IPointerUpHandler, IBeginDragHandler, IEndDragHandler, IDragHandler, IEventSystemHandler
  {
    private GameObject m_checkBoundaryGo;
    private GameObject m_draggingGo;
    private bool m_isDragging;
    private GameObject emptyImageGo;
    private GameObject m_downGo;
    private GameObject m_enterGo;

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGameObject(
      GameObject addComponentRoot,
      bool isNeedExcuteEvent,
      GameObject addReturnImageRoot = null,
      GameObject checkBoundaryGo = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowReturnBgImage(bool isShow)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPointerClick(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPointerDown(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPointerUp(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
      this.PassEvent<IBeginDragHandler>(eventData, ExecuteEvents.beginDragHandler);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
      this.PassEvent<IEndDragHandler>(eventData, ExecuteEvents.endDragHandler);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnDrag(PointerEventData eventData)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CheckPositionBoundary(PointerEventData data)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PassEvent<T>(PointerEventData data, ExecuteEvents.EventFunction<T> function) where T : IEventSystemHandler
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    public bool IsNeedExcuteEvent { private set; get; }
  }
}
