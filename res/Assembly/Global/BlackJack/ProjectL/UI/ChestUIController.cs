﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ChestUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class ChestUIController : UIControllerBase
  {
    [AutoBind("./BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_backgroundButton;
    [AutoBind("./ChestObject", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_chestGameObject;
    [AutoBind("./ChestObject/Chest", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_chestUIStateController;
    [AutoBind("./FirstWin", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_firstWinGameObject;
    [AutoBind("./RewardGoodsGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_rewardGoodsGroupGameObject;
    [AutoBind("./TeamRewardInfo", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_teamRewardUIStateController;
    [AutoBind("./TeamRewardInfo/Detail/RewardGroup/Friendship", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_teamRewardFriendshipGameObject;
    [AutoBind("./TeamRewardInfo/Detail/RewardGroup/Friendship/ValuGroup/Value", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_teamRewardFriendshipValueText;
    [AutoBind("./TeamRewardInfo/Detail/RewardGroup/Team", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_teamRewardTeamGameObject;
    [AutoBind("./TeamRewardInfo/Detail/RewardGroup/Team/ValuGroup/Value", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_teamRewardTeamValueText;
    [AutoBind("./TeamRewardInfo/Detail/RewardGroup/Friend", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_teamRewardFriendGameObject;
    [AutoBind("./TeamRewardInfo/Detail/RewardGroup/Friend/ValuGroup/Value", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_teamRewardFriendValueText;
    [AutoBind("./DailyRewardGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_dailyRewardUIStateController;
    [AutoBind("./DailyRewardGroup/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_dailyRewardText;
    [AutoBind("./ClickScreenContinue", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_clickScreenContinueGameObject;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefabs/GoodsTweenDummy", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_goodsTweenDummyPrefab;
    private List<RewardGoodsUIController> m_rewardGoods;
    private bool m_isClick;
    [DoNotToLua]
    private ChestUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_ShowRewardBattleRewardBooleanBoolean_hotfix;
    private LuaFunction m_Co_ShowRewardBattleRewardBooleanBoolean_hotfix;
    private LuaFunction m_Co_SetAndWaitUIStateCommonUIStateControllerString_hotfix;
    private LuaFunction m_Co_PlayAndWaitTweenGameObject_hotfix;
    private LuaFunction m_Co_WaitClick_hotfix;
    private LuaFunction m_CreateGoodsTweenDummyList`1_hotfix;
    private LuaFunction m_ClearGoodsDummyGroup_hotfix;
    private LuaFunction m_ClearRewardItems_hotfix;
    private LuaFunction m_OnBackgroundButtonClick_hotfix;
    private LuaFunction m_add_EventOnCloseAction_hotfix;
    private LuaFunction m_remove_EventOnCloseAction_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    private ChestUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowReward(BattleReward reward, bool isFirstWin, bool isAutoOpen)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_ShowReward(
      BattleReward reward,
      bool isFirstWin,
      bool isAutoOpen)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_SetAndWaitUIState(CommonUIStateController ctrl, string state)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_PlayAndWaitTween(GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_WaitClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateGoodsTweenDummy(List<Goods> goods)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearGoodsDummyGroup()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearRewardItems()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public ChestUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnClose()
    {
      this.EventOnClose = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private ChestUIController m_owner;

      public LuaExportHelper(ChestUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnClose()
      {
        this.m_owner.__callDele_EventOnClose();
      }

      public void __clearDele_EventOnClose()
      {
        this.m_owner.__clearDele_EventOnClose();
      }

      public Button m_backgroundButton
      {
        get
        {
          return this.m_owner.m_backgroundButton;
        }
        set
        {
          this.m_owner.m_backgroundButton = value;
        }
      }

      public GameObject m_chestGameObject
      {
        get
        {
          return this.m_owner.m_chestGameObject;
        }
        set
        {
          this.m_owner.m_chestGameObject = value;
        }
      }

      public CommonUIStateController m_chestUIStateController
      {
        get
        {
          return this.m_owner.m_chestUIStateController;
        }
        set
        {
          this.m_owner.m_chestUIStateController = value;
        }
      }

      public GameObject m_firstWinGameObject
      {
        get
        {
          return this.m_owner.m_firstWinGameObject;
        }
        set
        {
          this.m_owner.m_firstWinGameObject = value;
        }
      }

      public GameObject m_rewardGoodsGroupGameObject
      {
        get
        {
          return this.m_owner.m_rewardGoodsGroupGameObject;
        }
        set
        {
          this.m_owner.m_rewardGoodsGroupGameObject = value;
        }
      }

      public CommonUIStateController m_teamRewardUIStateController
      {
        get
        {
          return this.m_owner.m_teamRewardUIStateController;
        }
        set
        {
          this.m_owner.m_teamRewardUIStateController = value;
        }
      }

      public GameObject m_teamRewardFriendshipGameObject
      {
        get
        {
          return this.m_owner.m_teamRewardFriendshipGameObject;
        }
        set
        {
          this.m_owner.m_teamRewardFriendshipGameObject = value;
        }
      }

      public Text m_teamRewardFriendshipValueText
      {
        get
        {
          return this.m_owner.m_teamRewardFriendshipValueText;
        }
        set
        {
          this.m_owner.m_teamRewardFriendshipValueText = value;
        }
      }

      public GameObject m_teamRewardTeamGameObject
      {
        get
        {
          return this.m_owner.m_teamRewardTeamGameObject;
        }
        set
        {
          this.m_owner.m_teamRewardTeamGameObject = value;
        }
      }

      public Text m_teamRewardTeamValueText
      {
        get
        {
          return this.m_owner.m_teamRewardTeamValueText;
        }
        set
        {
          this.m_owner.m_teamRewardTeamValueText = value;
        }
      }

      public GameObject m_teamRewardFriendGameObject
      {
        get
        {
          return this.m_owner.m_teamRewardFriendGameObject;
        }
        set
        {
          this.m_owner.m_teamRewardFriendGameObject = value;
        }
      }

      public Text m_teamRewardFriendValueText
      {
        get
        {
          return this.m_owner.m_teamRewardFriendValueText;
        }
        set
        {
          this.m_owner.m_teamRewardFriendValueText = value;
        }
      }

      public CommonUIStateController m_dailyRewardUIStateController
      {
        get
        {
          return this.m_owner.m_dailyRewardUIStateController;
        }
        set
        {
          this.m_owner.m_dailyRewardUIStateController = value;
        }
      }

      public Text m_dailyRewardText
      {
        get
        {
          return this.m_owner.m_dailyRewardText;
        }
        set
        {
          this.m_owner.m_dailyRewardText = value;
        }
      }

      public GameObject m_clickScreenContinueGameObject
      {
        get
        {
          return this.m_owner.m_clickScreenContinueGameObject;
        }
        set
        {
          this.m_owner.m_clickScreenContinueGameObject = value;
        }
      }

      public GameObject m_prefabsGameObject
      {
        get
        {
          return this.m_owner.m_prefabsGameObject;
        }
        set
        {
          this.m_owner.m_prefabsGameObject = value;
        }
      }

      public GameObject m_goodsTweenDummyPrefab
      {
        get
        {
          return this.m_owner.m_goodsTweenDummyPrefab;
        }
        set
        {
          this.m_owner.m_goodsTweenDummyPrefab = value;
        }
      }

      public List<RewardGoodsUIController> m_rewardGoods
      {
        get
        {
          return this.m_owner.m_rewardGoods;
        }
        set
        {
          this.m_owner.m_rewardGoods = value;
        }
      }

      public bool m_isClick
      {
        get
        {
          return this.m_owner.m_isClick;
        }
        set
        {
          this.m_owner.m_isClick = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public IEnumerator Co_ShowReward(
        BattleReward reward,
        bool isFirstWin,
        bool isAutoOpen)
      {
        return this.m_owner.Co_ShowReward(reward, isFirstWin, isAutoOpen);
      }

      public IEnumerator Co_SetAndWaitUIState(CommonUIStateController ctrl, string state)
      {
        return this.m_owner.Co_SetAndWaitUIState(ctrl, state);
      }

      public IEnumerator Co_PlayAndWaitTween(GameObject go)
      {
        return this.m_owner.Co_PlayAndWaitTween(go);
      }

      public IEnumerator Co_WaitClick()
      {
        return this.m_owner.Co_WaitClick();
      }

      public void CreateGoodsTweenDummy(List<Goods> goods)
      {
        this.m_owner.CreateGoodsTweenDummy(goods);
      }

      public void ClearGoodsDummyGroup()
      {
        this.m_owner.ClearGoodsDummyGroup();
      }

      public void ClearRewardItems()
      {
        this.m_owner.ClearRewardItems();
      }

      public void OnBackgroundButtonClick()
      {
        this.m_owner.OnBackgroundButtonClick();
      }
    }
  }
}
