﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.FriendSmallItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class FriendSmallItemUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_itemButton;
    [AutoBind("./ChatButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_chatButton;
    [AutoBind("./ButtonGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_buttonGroupUIStateController;
    [AutoBind("./StateGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_friendOnlineUIStateController;
    [AutoBind("./StateGroup/OnLineText2", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_friendOnlineLongText;
    [AutoBind("./PlayerIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_friendIconImage;
    [AutoBind("./PlayerIconImageGrey", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_friendIconGreyImage;
    [AutoBind("./HeadFrameDummy", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_friendHeadFrameTransform;
    [AutoBind("./ButtonGroup/AgreeButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_agreeButton;
    [AutoBind("./ButtonGroup/DisagreeButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_disagreeButton;
    [AutoBind("./ButtonGroup/AddButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_addButton;
    [AutoBind("./ButtonGroup/SelectToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_selectToggle;
    [AutoBind("./LevelText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_friendLevelText;
    [AutoBind("./PlayerNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_friendNameText;
    [AutoBind("./ServerText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_serverText;
    [AutoBind("./InGroup", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_inGroupButton;
    private string m_userID;
    private UserSummary m_userSummy;
    private IConfigDataLoader m_configDataLoader;
    private ProjectLPlayerContext m_playerContext;
    [DoNotToLua]
    private FriendSmallItemUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_SetFriendInfoUserSummaryFriendInfoType_hotfix;
    private LuaFunction m_GetUserID_hotfix;
    private LuaFunction m_GetUserSummy_hotfix;
    private LuaFunction m_SetUserSelectBoolean_hotfix;
    private LuaFunction m_SetUserInGroupBoolean_hotfix;
    private LuaFunction m_GetSimpleInfoPostionType_hotfix;
    private LuaFunction m_GetPlayerSimpleInfoPos_hotfix;
    private LuaFunction m_OnItemButtonClick_hotfix;
    private LuaFunction m_OnChatButtonClick_hotfix;
    private LuaFunction m_OnAgreeButtonClick_hotfix;
    private LuaFunction m_OnDisagreeButtonClick_hotfix;
    private LuaFunction m_OnInGroupButtonClick_hotfix;
    private LuaFunction m_OnAddButtonClick_hotfix;
    private LuaFunction m_OnSelectToggleValueChangedBoolean_hotfix;
    private LuaFunction m_add_EventOnShowPlayerSimpleInfoAction`1_hotfix;
    private LuaFunction m_remove_EventOnShowPlayerSimpleInfoAction`1_hotfix;
    private LuaFunction m_add_EventOnAgreeAction`1_hotfix;
    private LuaFunction m_remove_EventOnAgreeAction`1_hotfix;
    private LuaFunction m_add_EventOnDisagreeAction`1_hotfix;
    private LuaFunction m_remove_EventOnDisagreeAction`1_hotfix;
    private LuaFunction m_add_EventOnAddAction`1_hotfix;
    private LuaFunction m_remove_EventOnAddAction`1_hotfix;
    private LuaFunction m_add_EventOnSelectToggleValueChangedAction`2_hotfix;
    private LuaFunction m_remove_EventOnSelectToggleValueChangedAction`2_hotfix;
    private LuaFunction m_add_EventOnChatAction`1_hotfix;
    private LuaFunction m_remove_EventOnChatAction`1_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public FriendSmallItemUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetFriendInfo(UserSummary userSummy, FriendInfoType friendInfoType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetUserID()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public UserSummary GetUserSummy()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetUserSelect(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetUserInGroup(bool isIn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PlayerSimpleInfoUITask.PostionType GetSimpleInfoPostionType()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Vector3 GetPlayerSimpleInfoPos()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnItemButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChatButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAgreeButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDisagreeButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInGroupButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSelectToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<FriendSmallItemUIController> EventOnShowPlayerSimpleInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<FriendSmallItemUIController> EventOnAgree
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<FriendSmallItemUIController> EventOnDisagree
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<FriendSmallItemUIController> EventOnAdd
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<bool, FriendSmallItemUIController> EventOnSelectToggleValueChanged
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<FriendSmallItemUIController> EventOnChat
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public FriendSmallItemUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowPlayerSimpleInfo(FriendSmallItemUIController obj)
    {
    }

    private void __clearDele_EventOnShowPlayerSimpleInfo(FriendSmallItemUIController obj)
    {
      this.EventOnShowPlayerSimpleInfo = (Action<FriendSmallItemUIController>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnAgree(FriendSmallItemUIController obj)
    {
    }

    private void __clearDele_EventOnAgree(FriendSmallItemUIController obj)
    {
      this.EventOnAgree = (Action<FriendSmallItemUIController>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnDisagree(FriendSmallItemUIController obj)
    {
    }

    private void __clearDele_EventOnDisagree(FriendSmallItemUIController obj)
    {
      this.EventOnDisagree = (Action<FriendSmallItemUIController>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnAdd(FriendSmallItemUIController obj)
    {
    }

    private void __clearDele_EventOnAdd(FriendSmallItemUIController obj)
    {
      this.EventOnAdd = (Action<FriendSmallItemUIController>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnSelectToggleValueChanged(
      bool arg1,
      FriendSmallItemUIController arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnSelectToggleValueChanged(
      bool arg1,
      FriendSmallItemUIController arg2)
    {
      this.EventOnSelectToggleValueChanged = (Action<bool, FriendSmallItemUIController>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnChat(FriendSmallItemUIController obj)
    {
    }

    private void __clearDele_EventOnChat(FriendSmallItemUIController obj)
    {
      this.EventOnChat = (Action<FriendSmallItemUIController>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private FriendSmallItemUIController m_owner;

      public LuaExportHelper(FriendSmallItemUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnShowPlayerSimpleInfo(FriendSmallItemUIController obj)
      {
        this.m_owner.__callDele_EventOnShowPlayerSimpleInfo(obj);
      }

      public void __clearDele_EventOnShowPlayerSimpleInfo(FriendSmallItemUIController obj)
      {
        this.m_owner.__clearDele_EventOnShowPlayerSimpleInfo(obj);
      }

      public void __callDele_EventOnAgree(FriendSmallItemUIController obj)
      {
        this.m_owner.__callDele_EventOnAgree(obj);
      }

      public void __clearDele_EventOnAgree(FriendSmallItemUIController obj)
      {
        this.m_owner.__clearDele_EventOnAgree(obj);
      }

      public void __callDele_EventOnDisagree(FriendSmallItemUIController obj)
      {
        this.m_owner.__callDele_EventOnDisagree(obj);
      }

      public void __clearDele_EventOnDisagree(FriendSmallItemUIController obj)
      {
        this.m_owner.__clearDele_EventOnDisagree(obj);
      }

      public void __callDele_EventOnAdd(FriendSmallItemUIController obj)
      {
        this.m_owner.__callDele_EventOnAdd(obj);
      }

      public void __clearDele_EventOnAdd(FriendSmallItemUIController obj)
      {
        this.m_owner.__clearDele_EventOnAdd(obj);
      }

      public void __callDele_EventOnSelectToggleValueChanged(
        bool arg1,
        FriendSmallItemUIController arg2)
      {
        this.m_owner.__callDele_EventOnSelectToggleValueChanged(arg1, arg2);
      }

      public void __clearDele_EventOnSelectToggleValueChanged(
        bool arg1,
        FriendSmallItemUIController arg2)
      {
        this.m_owner.__clearDele_EventOnSelectToggleValueChanged(arg1, arg2);
      }

      public void __callDele_EventOnChat(FriendSmallItemUIController obj)
      {
        this.m_owner.__callDele_EventOnChat(obj);
      }

      public void __clearDele_EventOnChat(FriendSmallItemUIController obj)
      {
        this.m_owner.__clearDele_EventOnChat(obj);
      }

      public CommonUIStateController m_uiStateController
      {
        get
        {
          return this.m_owner.m_uiStateController;
        }
        set
        {
          this.m_owner.m_uiStateController = value;
        }
      }

      public Button m_itemButton
      {
        get
        {
          return this.m_owner.m_itemButton;
        }
        set
        {
          this.m_owner.m_itemButton = value;
        }
      }

      public Button m_chatButton
      {
        get
        {
          return this.m_owner.m_chatButton;
        }
        set
        {
          this.m_owner.m_chatButton = value;
        }
      }

      public CommonUIStateController m_buttonGroupUIStateController
      {
        get
        {
          return this.m_owner.m_buttonGroupUIStateController;
        }
        set
        {
          this.m_owner.m_buttonGroupUIStateController = value;
        }
      }

      public CommonUIStateController m_friendOnlineUIStateController
      {
        get
        {
          return this.m_owner.m_friendOnlineUIStateController;
        }
        set
        {
          this.m_owner.m_friendOnlineUIStateController = value;
        }
      }

      public Text m_friendOnlineLongText
      {
        get
        {
          return this.m_owner.m_friendOnlineLongText;
        }
        set
        {
          this.m_owner.m_friendOnlineLongText = value;
        }
      }

      public Image m_friendIconImage
      {
        get
        {
          return this.m_owner.m_friendIconImage;
        }
        set
        {
          this.m_owner.m_friendIconImage = value;
        }
      }

      public Image m_friendIconGreyImage
      {
        get
        {
          return this.m_owner.m_friendIconGreyImage;
        }
        set
        {
          this.m_owner.m_friendIconGreyImage = value;
        }
      }

      public Transform m_friendHeadFrameTransform
      {
        get
        {
          return this.m_owner.m_friendHeadFrameTransform;
        }
        set
        {
          this.m_owner.m_friendHeadFrameTransform = value;
        }
      }

      public Button m_agreeButton
      {
        get
        {
          return this.m_owner.m_agreeButton;
        }
        set
        {
          this.m_owner.m_agreeButton = value;
        }
      }

      public Button m_disagreeButton
      {
        get
        {
          return this.m_owner.m_disagreeButton;
        }
        set
        {
          this.m_owner.m_disagreeButton = value;
        }
      }

      public Button m_addButton
      {
        get
        {
          return this.m_owner.m_addButton;
        }
        set
        {
          this.m_owner.m_addButton = value;
        }
      }

      public Toggle m_selectToggle
      {
        get
        {
          return this.m_owner.m_selectToggle;
        }
        set
        {
          this.m_owner.m_selectToggle = value;
        }
      }

      public Text m_friendLevelText
      {
        get
        {
          return this.m_owner.m_friendLevelText;
        }
        set
        {
          this.m_owner.m_friendLevelText = value;
        }
      }

      public Text m_friendNameText
      {
        get
        {
          return this.m_owner.m_friendNameText;
        }
        set
        {
          this.m_owner.m_friendNameText = value;
        }
      }

      public Text m_serverText
      {
        get
        {
          return this.m_owner.m_serverText;
        }
        set
        {
          this.m_owner.m_serverText = value;
        }
      }

      public Button m_inGroupButton
      {
        get
        {
          return this.m_owner.m_inGroupButton;
        }
        set
        {
          this.m_owner.m_inGroupButton = value;
        }
      }

      public string m_userID
      {
        get
        {
          return this.m_owner.m_userID;
        }
        set
        {
          this.m_owner.m_userID = value;
        }
      }

      public UserSummary m_userSummy
      {
        get
        {
          return this.m_owner.m_userSummy;
        }
        set
        {
          this.m_owner.m_userSummy = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void OnItemButtonClick()
      {
        this.m_owner.OnItemButtonClick();
      }

      public void OnChatButtonClick()
      {
        this.m_owner.OnChatButtonClick();
      }

      public void OnAgreeButtonClick()
      {
        this.m_owner.OnAgreeButtonClick();
      }

      public void OnDisagreeButtonClick()
      {
        this.m_owner.OnDisagreeButtonClick();
      }

      public void OnInGroupButtonClick()
      {
        this.m_owner.OnInGroupButtonClick();
      }

      public void OnAddButtonClick()
      {
        this.m_owner.OnAddButtonClick();
      }

      public void OnSelectToggleValueChanged(bool isOn)
      {
        this.m_owner.OnSelectToggleValueChanged(isOn);
      }
    }
  }
}
