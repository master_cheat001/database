﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BattleSkillButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class BattleSkillButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerClickHandler, IEventSystemHandler
  {
    private bool m_isPointerLongDown;
    private bool m_ignoreClick;
    private float m_pointerLongDownCountdown;
    private Text m_cooldownText;
    private Image m_iconImage;
    private Image m_frameImage;
    private GameObject m_cooldownGameObject;
    private GameObject m_selectedGameObject;
    private GameObject m_descGameObject;
    private GameObject m_costGameObject;
    private GameObject m_passiveGameObject;
    private GameObject m_banGameObject;
    private ConfigDataSkillInfo m_skillInfo;
    private int m_index;
    private bool m_isSelected;
    private bool m_isBanned;
    private bool m_isHandleLongDown;
    private CommonUIStateController m_descStateCtrl;
    [DoNotToLua]
    private BattleSkillButton.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_Awake_hotfix;
    private LuaFunction m_SetIndexInt32_hotfix;
    private LuaFunction m_GetIndex_hotfix;
    private LuaFunction m_SetSkillInfoConfigDataSkillInfo_hotfix;
    private LuaFunction m_GetSkillInfo_hotfix;
    private LuaFunction m_SetCooldownInt32_hotfix;
    private LuaFunction m_SetSelectedBoolean_hotfix;
    private LuaFunction m_IsSelected_hotfix;
    private LuaFunction m_ShowCostBoolean_hotfix;
    private LuaFunction m_ShowFrameBoolean_hotfix;
    private LuaFunction m_SetBannedBoolean_hotfix;
    private LuaFunction m_IsBanned_hotfix;
    private LuaFunction m_SetHandleLongDownBoolean_hotfix;
    private LuaFunction m_SetDescGameObjectGameObject_hotfix;
    private LuaFunction m_ShowDesc_hotfix;
    private LuaFunction m_HideDesc_hotfix;
    private LuaFunction m_OnPointerDownPointerEventData_hotfix;
    private LuaFunction m_OnPointerUpPointerEventData_hotfix;
    private LuaFunction m_OnPointerClickPointerEventData_hotfix;
    private LuaFunction m_InvokeClickEvent_hotfix;
    private LuaFunction m_Update_hotfix;
    private LuaFunction m_add_EventOnClickAction_hotfix;
    private LuaFunction m_remove_EventOnClickAction_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    private void Awake()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetIndex(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetIndex()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSkillInfo(ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSkillInfo GetSkillInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCooldown(int cd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSelected(bool selected)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsSelected()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowCost(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowFrame(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBanned(bool banned)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBanned()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHandleLongDown(bool h)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDescGameObject(GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowDesc()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HideDesc()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPointerDown(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPointerUp(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPointerClick(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool InvokeClickEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public BattleSkillButton.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnClick()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnClick()
    {
      this.EventOnClick = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private BattleSkillButton m_owner;

      public LuaExportHelper(BattleSkillButton owner)
      {
        this.m_owner = owner;
      }

      public void __callDele_EventOnClick()
      {
        this.m_owner.__callDele_EventOnClick();
      }

      public void __clearDele_EventOnClick()
      {
        this.m_owner.__clearDele_EventOnClick();
      }

      public bool m_isPointerLongDown
      {
        get
        {
          return this.m_owner.m_isPointerLongDown;
        }
        set
        {
          this.m_owner.m_isPointerLongDown = value;
        }
      }

      public bool m_ignoreClick
      {
        get
        {
          return this.m_owner.m_ignoreClick;
        }
        set
        {
          this.m_owner.m_ignoreClick = value;
        }
      }

      public float m_pointerLongDownCountdown
      {
        get
        {
          return this.m_owner.m_pointerLongDownCountdown;
        }
        set
        {
          this.m_owner.m_pointerLongDownCountdown = value;
        }
      }

      public Text m_cooldownText
      {
        get
        {
          return this.m_owner.m_cooldownText;
        }
        set
        {
          this.m_owner.m_cooldownText = value;
        }
      }

      public Image m_iconImage
      {
        get
        {
          return this.m_owner.m_iconImage;
        }
        set
        {
          this.m_owner.m_iconImage = value;
        }
      }

      public Image m_frameImage
      {
        get
        {
          return this.m_owner.m_frameImage;
        }
        set
        {
          this.m_owner.m_frameImage = value;
        }
      }

      public GameObject m_cooldownGameObject
      {
        get
        {
          return this.m_owner.m_cooldownGameObject;
        }
        set
        {
          this.m_owner.m_cooldownGameObject = value;
        }
      }

      public GameObject m_selectedGameObject
      {
        get
        {
          return this.m_owner.m_selectedGameObject;
        }
        set
        {
          this.m_owner.m_selectedGameObject = value;
        }
      }

      public GameObject m_descGameObject
      {
        get
        {
          return this.m_owner.m_descGameObject;
        }
        set
        {
          this.m_owner.m_descGameObject = value;
        }
      }

      public GameObject m_costGameObject
      {
        get
        {
          return this.m_owner.m_costGameObject;
        }
        set
        {
          this.m_owner.m_costGameObject = value;
        }
      }

      public GameObject m_passiveGameObject
      {
        get
        {
          return this.m_owner.m_passiveGameObject;
        }
        set
        {
          this.m_owner.m_passiveGameObject = value;
        }
      }

      public GameObject m_banGameObject
      {
        get
        {
          return this.m_owner.m_banGameObject;
        }
        set
        {
          this.m_owner.m_banGameObject = value;
        }
      }

      public ConfigDataSkillInfo m_skillInfo
      {
        get
        {
          return this.m_owner.m_skillInfo;
        }
        set
        {
          this.m_owner.m_skillInfo = value;
        }
      }

      public int m_index
      {
        get
        {
          return this.m_owner.m_index;
        }
        set
        {
          this.m_owner.m_index = value;
        }
      }

      public bool m_isSelected
      {
        get
        {
          return this.m_owner.m_isSelected;
        }
        set
        {
          this.m_owner.m_isSelected = value;
        }
      }

      public bool m_isBanned
      {
        get
        {
          return this.m_owner.m_isBanned;
        }
        set
        {
          this.m_owner.m_isBanned = value;
        }
      }

      public bool m_isHandleLongDown
      {
        get
        {
          return this.m_owner.m_isHandleLongDown;
        }
        set
        {
          this.m_owner.m_isHandleLongDown = value;
        }
      }

      public CommonUIStateController m_descStateCtrl
      {
        get
        {
          return this.m_owner.m_descStateCtrl;
        }
        set
        {
          this.m_owner.m_descStateCtrl = value;
        }
      }

      public void Awake()
      {
        this.m_owner.Awake();
      }

      public void ShowDesc()
      {
        this.m_owner.ShowDesc();
      }

      public void HideDesc()
      {
        this.m_owner.HideDesc();
      }

      public void Update()
      {
        this.m_owner.Update();
      }
    }
  }
}
