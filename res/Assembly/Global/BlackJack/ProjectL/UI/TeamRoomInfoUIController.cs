﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.TeamRoomInfoUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectLBasic;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class TeamRoomInfoUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./GuildOrTeamState", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_guildOrTeamUIStateController;
    [AutoBind("./WaitStartBattle", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_waitStartBattleGameObject;
    [AutoBind("./Panel/StartBattleButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_startButton;
    [AutoBind("./Panel/StartBattleButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_startButtonUIStateController;
    [AutoBind("./Panel/TeamLeaveButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_leaveButton;
    [AutoBind("./Panel/EditTeamButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_showChangePlayerPositionButton;
    [AutoBind("./Panel/ChatButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_chatButton;
    [AutoBind("./Panel/Texts/TargetText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_gameFunctionTypeNameText;
    [AutoBind("./Panel/Texts/TargetFloorText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_locationNameText;
    [AutoBind("./Panel/Texts/LVText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerLevelText;
    [AutoBind("./Panel/Texts/CountDownText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_quitCountdownText;
    [AutoBind("./Panel/Texts/ConsumeNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_energyText;
    [AutoBind("./Panel/Texts/GuildLvValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_guildMassiveCombatNameText;
    [AutoBind("./Panel/AuthorityDropdown", AutoBindAttribute.InitState.NotInit, false)]
    private Dropdown m_authorityDropdown;
    [AutoBind("./Panel/AuthorityDropdown/LockButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_authorityLockButton;
    [AutoBind("./Panel/PlayerInfo0", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_playerInfo0GameObject;
    [AutoBind("./Panel/PlayerInfo1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_playerInfo1GameObject;
    [AutoBind("./Panel/PlayerInfo2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_playerInfo2GameObject;
    [AutoBind("./Panel/EditTeamPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_changePlayerPositionPanelGameObject;
    [AutoBind("./Panel/EditTeamPanel/CompleteButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_changePlayerPositionCompleteButton;
    [AutoBind("./Panel/EditTeamPanel/PlayerInfo0", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_changePlayerPositionInfo0GameObject;
    [AutoBind("./Panel/EditTeamPanel/PlayerInfo1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_changePlayerPositionInfo1GameObject;
    [AutoBind("./Panel/EditTeamPanel/PlayerInfo2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_changePlayerPositionInfo2GameObject;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefabs/PlayerInfo", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_playerInfoPrefab;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.NotInit, false)]
    private SmallExpressionParseDesc m_expressionParseDesc;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.NotInit, false)]
    private PrefabResourceContainer m_expressionResContainer;
    private TeamRoomPlayerInfoUIController[] m_playerInfoUIControllers;
    private TeamRoomPlayerInfoUIController[] m_editPlayerInfoUIControllers;
    private TeamRoomPlayerInfoUIController m_draggingPlayerInfoUIController;
    private bool m_isIgnoreToggleEvent;
    private GameFunctionType m_gameFunctionType;
    private bool m_isOpened;
    [DoNotToLua]
    private TeamRoomInfoUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_GetPlayerInfoParentInt32Boolean_hotfix;
    private LuaFunction m_OnDisable_hotfix;
    private LuaFunction m_OnApplicationPauseBoolean_hotfix;
    private LuaFunction m_OnApplicationFocusBoolean_hotfix;
    private LuaFunction m_Open_hotfix;
    private LuaFunction m_CloseAction_hotfix;
    private LuaFunction m_SetTeamRoomSettingTeamRoomSetting_hotfix;
    private LuaFunction m_IsGuildMassiveCombat_hotfix;
    private LuaFunction m_SetTeamRoomPlayersList`1Boolean_hotfix;
    private LuaFunction m_SetBattleNameGameFunctionTypeInt32_hotfix;
    private LuaFunction m_SetPlayerLevelRangeInt32Int32_hotfix;
    private LuaFunction m_SetAuthorityTeamRoomAuthority_hotfix;
    private LuaFunction m_SetQuitCountdownTimeSpan_hotfix;
    private LuaFunction m_ShowWaitStartBattleBoolean_hotfix;
    private LuaFunction m_ShowChangePlayerPositionTeamRoom_hotfix;
    private LuaFunction m_HideChangePlayerPosition_hotfix;
    private LuaFunction m_GetPlayerChangedPositionTeamRoomPlayer_hotfix;
    private LuaFunction m_ShowPlayerChatInt32String_hotfix;
    private LuaFunction m_ShowPlayerBigExpressionInt32Int32_hotfix;
    private LuaFunction m_CreateDraggingPlayerInfoUIControllerTeamRoomPlayerInfoUIController_hotfix;
    private LuaFunction m_DestroyDragginPlayerInfoUIController_hotfix;
    private LuaFunction m_MoveDraggingPlayerInfoUIControllerVector2_hotfix;
    private LuaFunction m_DropDraggingPlayerInfoUIControllerVector3_hotfix;
    private LuaFunction m_OnLeaveButtonClick_hotfix;
    private LuaFunction m_OnStartButtonClick_hotfix;
    private LuaFunction m_OnShowChangePlayerPositionButtonClick_hotfix;
    private LuaFunction m_OnChangePlayerPositionCompleteButtonClick_hotfix;
    private LuaFunction m_OnChatButtonClick_hotfix;
    private LuaFunction m_OnAuthorityDropdownValueChangedInt32_hotfix;
    private LuaFunction m_OnAuthorityLockButtonClick_hotfix;
    private LuaFunction m_TeamRoomPlayerInfoUIController_OnInviteButtonClickTeamRoomPlayerInfoUIController_hotfix;
    private LuaFunction m_TeamRoomPlayerInfoUIController_OnPlayerButtonClickTeamRoomPlayerInfoUIController_hotfix;
    private LuaFunction m_TeamRoomPlayerInfoUIController_OnBlessingButtonClickTeamRoomPlayerInfoUIController_hotfix;
    private LuaFunction m_TeamRoomPlayerInfoUIController_OnBeginDragTeamRoomPlayerInfoUIControllerPointerEventData_hotfix;
    private LuaFunction m_TeamRoomPlayerInfoUIController_OnEndDragTeamRoomPlayerInfoUIControllerPointerEventData_hotfix;
    private LuaFunction m_TeamRoomPlayerInfoUIController_OnDragPointerEventData_hotfix;
    private LuaFunction m_TeamRoomPlayerInfoUIController_OnDropPointerEventData_hotfix;
    private LuaFunction m_add_EventOnLeaveAction_hotfix;
    private LuaFunction m_remove_EventOnLeaveAction_hotfix;
    private LuaFunction m_add_EventOnStartAction_hotfix;
    private LuaFunction m_remove_EventOnStartAction_hotfix;
    private LuaFunction m_add_EventOnShowChangePlayerPositionAction_hotfix;
    private LuaFunction m_remove_EventOnShowChangePlayerPositionAction_hotfix;
    private LuaFunction m_add_EventOnChangePlayerPositionCompleteAction_hotfix;
    private LuaFunction m_remove_EventOnChangePlayerPositionCompleteAction_hotfix;
    private LuaFunction m_add_EventOnShowChatAction_hotfix;
    private LuaFunction m_remove_EventOnShowChatAction_hotfix;
    private LuaFunction m_add_EventOnChangeAuthorityAction`1_hotfix;
    private LuaFunction m_remove_EventOnChangeAuthorityAction`1_hotfix;
    private LuaFunction m_add_EventOnShowInviteAction_hotfix;
    private LuaFunction m_remove_EventOnShowInviteAction_hotfix;
    private LuaFunction m_add_EventOnShowPlayerInfoAction`2_hotfix;
    private LuaFunction m_remove_EventOnShowPlayerInfoAction`2_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    private TeamRoomInfoUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Transform GetPlayerInfoParent(int idx, bool isEdit)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDisable()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnApplicationPause(bool isPause)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnApplicationFocus(bool focus)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Close(Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTeamRoomSetting(TeamRoomSetting setting)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsGuildMassiveCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTeamRoomPlayers(List<TeamRoomPlayer> players, bool isLeader)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetBattleName(GameFunctionType gameFunctionType, int locationId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPlayerLevelRange(int levelMin, int levelMax)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAuthority(TeamRoomAuthority authority)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetQuitCountdown(TimeSpan ts)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowWaitStartBattle(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowChangePlayerPosition(TeamRoom teamRoom)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideChangePlayerPosition()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPlayerChangedPosition(TeamRoomPlayer player)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowPlayerChat(int playerIndex, string text)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowPlayerBigExpression(int playerIndex, int expressionID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateDraggingPlayerInfoUIController(TeamRoomPlayerInfoUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DestroyDragginPlayerInfoUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void MoveDraggingPlayerInfoUIController(Vector2 pos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DropDraggingPlayerInfoUIController(Vector3 pos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnLeaveButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnStartButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnShowChangePlayerPositionButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChangePlayerPositionCompleteButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChatButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAuthorityDropdownValueChanged(int value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAuthorityLockButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamRoomPlayerInfoUIController_OnInviteButtonClick(
      TeamRoomPlayerInfoUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamRoomPlayerInfoUIController_OnPlayerButtonClick(
      TeamRoomPlayerInfoUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamRoomPlayerInfoUIController_OnBlessingButtonClick(
      TeamRoomPlayerInfoUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamRoomPlayerInfoUIController_OnBeginDrag(
      TeamRoomPlayerInfoUIController ctrl,
      PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamRoomPlayerInfoUIController_OnEndDrag(
      TeamRoomPlayerInfoUIController ctrl,
      PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamRoomPlayerInfoUIController_OnDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void TeamRoomPlayerInfoUIController_OnDrop(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnLeave
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnStart
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowChangePlayerPosition
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnChangePlayerPositionComplete
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowChat
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<TeamRoomAuthority> EventOnChangeAuthority
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowInvite
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, RectTransform> EventOnShowPlayerInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public TeamRoomInfoUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnLeave()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnLeave()
    {
      this.EventOnLeave = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnStart()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnStart()
    {
      this.EventOnStart = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowChangePlayerPosition()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowChangePlayerPosition()
    {
      this.EventOnShowChangePlayerPosition = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnChangePlayerPositionComplete()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnChangePlayerPositionComplete()
    {
      this.EventOnChangePlayerPositionComplete = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowChat()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowChat()
    {
      this.EventOnShowChat = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnChangeAuthority(TeamRoomAuthority obj)
    {
    }

    private void __clearDele_EventOnChangeAuthority(TeamRoomAuthority obj)
    {
      this.EventOnChangeAuthority = (Action<TeamRoomAuthority>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowInvite()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowInvite()
    {
      this.EventOnShowInvite = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowPlayerInfo(int arg1, RectTransform arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowPlayerInfo(int arg1, RectTransform arg2)
    {
      this.EventOnShowPlayerInfo = (Action<int, RectTransform>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private TeamRoomInfoUIController m_owner;

      public LuaExportHelper(TeamRoomInfoUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnLeave()
      {
        this.m_owner.__callDele_EventOnLeave();
      }

      public void __clearDele_EventOnLeave()
      {
        this.m_owner.__clearDele_EventOnLeave();
      }

      public void __callDele_EventOnStart()
      {
        this.m_owner.__callDele_EventOnStart();
      }

      public void __clearDele_EventOnStart()
      {
        this.m_owner.__clearDele_EventOnStart();
      }

      public void __callDele_EventOnShowChangePlayerPosition()
      {
        this.m_owner.__callDele_EventOnShowChangePlayerPosition();
      }

      public void __clearDele_EventOnShowChangePlayerPosition()
      {
        this.m_owner.__clearDele_EventOnShowChangePlayerPosition();
      }

      public void __callDele_EventOnChangePlayerPositionComplete()
      {
        this.m_owner.__callDele_EventOnChangePlayerPositionComplete();
      }

      public void __clearDele_EventOnChangePlayerPositionComplete()
      {
        this.m_owner.__clearDele_EventOnChangePlayerPositionComplete();
      }

      public void __callDele_EventOnShowChat()
      {
        this.m_owner.__callDele_EventOnShowChat();
      }

      public void __clearDele_EventOnShowChat()
      {
        this.m_owner.__clearDele_EventOnShowChat();
      }

      public void __callDele_EventOnChangeAuthority(TeamRoomAuthority obj)
      {
        this.m_owner.__callDele_EventOnChangeAuthority(obj);
      }

      public void __clearDele_EventOnChangeAuthority(TeamRoomAuthority obj)
      {
        this.m_owner.__clearDele_EventOnChangeAuthority(obj);
      }

      public void __callDele_EventOnShowInvite()
      {
        this.m_owner.__callDele_EventOnShowInvite();
      }

      public void __clearDele_EventOnShowInvite()
      {
        this.m_owner.__clearDele_EventOnShowInvite();
      }

      public void __callDele_EventOnShowPlayerInfo(int arg1, RectTransform arg2)
      {
        this.m_owner.__callDele_EventOnShowPlayerInfo(arg1, arg2);
      }

      public void __clearDele_EventOnShowPlayerInfo(int arg1, RectTransform arg2)
      {
        this.m_owner.__clearDele_EventOnShowPlayerInfo(arg1, arg2);
      }

      public CommonUIStateController m_uiStateController
      {
        get
        {
          return this.m_owner.m_uiStateController;
        }
        set
        {
          this.m_owner.m_uiStateController = value;
        }
      }

      public CommonUIStateController m_guildOrTeamUIStateController
      {
        get
        {
          return this.m_owner.m_guildOrTeamUIStateController;
        }
        set
        {
          this.m_owner.m_guildOrTeamUIStateController = value;
        }
      }

      public GameObject m_waitStartBattleGameObject
      {
        get
        {
          return this.m_owner.m_waitStartBattleGameObject;
        }
        set
        {
          this.m_owner.m_waitStartBattleGameObject = value;
        }
      }

      public Button m_startButton
      {
        get
        {
          return this.m_owner.m_startButton;
        }
        set
        {
          this.m_owner.m_startButton = value;
        }
      }

      public CommonUIStateController m_startButtonUIStateController
      {
        get
        {
          return this.m_owner.m_startButtonUIStateController;
        }
        set
        {
          this.m_owner.m_startButtonUIStateController = value;
        }
      }

      public Button m_leaveButton
      {
        get
        {
          return this.m_owner.m_leaveButton;
        }
        set
        {
          this.m_owner.m_leaveButton = value;
        }
      }

      public Button m_showChangePlayerPositionButton
      {
        get
        {
          return this.m_owner.m_showChangePlayerPositionButton;
        }
        set
        {
          this.m_owner.m_showChangePlayerPositionButton = value;
        }
      }

      public Button m_chatButton
      {
        get
        {
          return this.m_owner.m_chatButton;
        }
        set
        {
          this.m_owner.m_chatButton = value;
        }
      }

      public Text m_gameFunctionTypeNameText
      {
        get
        {
          return this.m_owner.m_gameFunctionTypeNameText;
        }
        set
        {
          this.m_owner.m_gameFunctionTypeNameText = value;
        }
      }

      public Text m_locationNameText
      {
        get
        {
          return this.m_owner.m_locationNameText;
        }
        set
        {
          this.m_owner.m_locationNameText = value;
        }
      }

      public Text m_playerLevelText
      {
        get
        {
          return this.m_owner.m_playerLevelText;
        }
        set
        {
          this.m_owner.m_playerLevelText = value;
        }
      }

      public Text m_quitCountdownText
      {
        get
        {
          return this.m_owner.m_quitCountdownText;
        }
        set
        {
          this.m_owner.m_quitCountdownText = value;
        }
      }

      public Text m_energyText
      {
        get
        {
          return this.m_owner.m_energyText;
        }
        set
        {
          this.m_owner.m_energyText = value;
        }
      }

      public Text m_guildMassiveCombatNameText
      {
        get
        {
          return this.m_owner.m_guildMassiveCombatNameText;
        }
        set
        {
          this.m_owner.m_guildMassiveCombatNameText = value;
        }
      }

      public Dropdown m_authorityDropdown
      {
        get
        {
          return this.m_owner.m_authorityDropdown;
        }
        set
        {
          this.m_owner.m_authorityDropdown = value;
        }
      }

      public Button m_authorityLockButton
      {
        get
        {
          return this.m_owner.m_authorityLockButton;
        }
        set
        {
          this.m_owner.m_authorityLockButton = value;
        }
      }

      public GameObject m_playerInfo0GameObject
      {
        get
        {
          return this.m_owner.m_playerInfo0GameObject;
        }
        set
        {
          this.m_owner.m_playerInfo0GameObject = value;
        }
      }

      public GameObject m_playerInfo1GameObject
      {
        get
        {
          return this.m_owner.m_playerInfo1GameObject;
        }
        set
        {
          this.m_owner.m_playerInfo1GameObject = value;
        }
      }

      public GameObject m_playerInfo2GameObject
      {
        get
        {
          return this.m_owner.m_playerInfo2GameObject;
        }
        set
        {
          this.m_owner.m_playerInfo2GameObject = value;
        }
      }

      public GameObject m_changePlayerPositionPanelGameObject
      {
        get
        {
          return this.m_owner.m_changePlayerPositionPanelGameObject;
        }
        set
        {
          this.m_owner.m_changePlayerPositionPanelGameObject = value;
        }
      }

      public Button m_changePlayerPositionCompleteButton
      {
        get
        {
          return this.m_owner.m_changePlayerPositionCompleteButton;
        }
        set
        {
          this.m_owner.m_changePlayerPositionCompleteButton = value;
        }
      }

      public GameObject m_changePlayerPositionInfo0GameObject
      {
        get
        {
          return this.m_owner.m_changePlayerPositionInfo0GameObject;
        }
        set
        {
          this.m_owner.m_changePlayerPositionInfo0GameObject = value;
        }
      }

      public GameObject m_changePlayerPositionInfo1GameObject
      {
        get
        {
          return this.m_owner.m_changePlayerPositionInfo1GameObject;
        }
        set
        {
          this.m_owner.m_changePlayerPositionInfo1GameObject = value;
        }
      }

      public GameObject m_changePlayerPositionInfo2GameObject
      {
        get
        {
          return this.m_owner.m_changePlayerPositionInfo2GameObject;
        }
        set
        {
          this.m_owner.m_changePlayerPositionInfo2GameObject = value;
        }
      }

      public GameObject m_prefabsGameObject
      {
        get
        {
          return this.m_owner.m_prefabsGameObject;
        }
        set
        {
          this.m_owner.m_prefabsGameObject = value;
        }
      }

      public GameObject m_playerInfoPrefab
      {
        get
        {
          return this.m_owner.m_playerInfoPrefab;
        }
        set
        {
          this.m_owner.m_playerInfoPrefab = value;
        }
      }

      public SmallExpressionParseDesc m_expressionParseDesc
      {
        get
        {
          return this.m_owner.m_expressionParseDesc;
        }
        set
        {
          this.m_owner.m_expressionParseDesc = value;
        }
      }

      public PrefabResourceContainer m_expressionResContainer
      {
        get
        {
          return this.m_owner.m_expressionResContainer;
        }
        set
        {
          this.m_owner.m_expressionResContainer = value;
        }
      }

      public TeamRoomPlayerInfoUIController[] m_playerInfoUIControllers
      {
        get
        {
          return this.m_owner.m_playerInfoUIControllers;
        }
        set
        {
          this.m_owner.m_playerInfoUIControllers = value;
        }
      }

      public TeamRoomPlayerInfoUIController[] m_editPlayerInfoUIControllers
      {
        get
        {
          return this.m_owner.m_editPlayerInfoUIControllers;
        }
        set
        {
          this.m_owner.m_editPlayerInfoUIControllers = value;
        }
      }

      public TeamRoomPlayerInfoUIController m_draggingPlayerInfoUIController
      {
        get
        {
          return this.m_owner.m_draggingPlayerInfoUIController;
        }
        set
        {
          this.m_owner.m_draggingPlayerInfoUIController = value;
        }
      }

      public bool m_isIgnoreToggleEvent
      {
        get
        {
          return this.m_owner.m_isIgnoreToggleEvent;
        }
        set
        {
          this.m_owner.m_isIgnoreToggleEvent = value;
        }
      }

      public GameFunctionType m_gameFunctionType
      {
        get
        {
          return this.m_owner.m_gameFunctionType;
        }
        set
        {
          this.m_owner.m_gameFunctionType = value;
        }
      }

      public bool m_isOpened
      {
        get
        {
          return this.m_owner.m_isOpened;
        }
        set
        {
          this.m_owner.m_isOpened = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public Transform GetPlayerInfoParent(int idx, bool isEdit)
      {
        return this.m_owner.GetPlayerInfoParent(idx, isEdit);
      }

      public void OnDisable()
      {
        this.m_owner.OnDisable();
      }

      public void OnApplicationPause(bool isPause)
      {
        this.m_owner.OnApplicationPause(isPause);
      }

      public void OnApplicationFocus(bool focus)
      {
        this.m_owner.OnApplicationFocus(focus);
      }

      public bool IsGuildMassiveCombat()
      {
        return this.m_owner.IsGuildMassiveCombat();
      }

      public void SetBattleName(GameFunctionType gameFunctionType, int locationId)
      {
        this.m_owner.SetBattleName(gameFunctionType, locationId);
      }

      public void SetPlayerLevelRange(int levelMin, int levelMax)
      {
        this.m_owner.SetPlayerLevelRange(levelMin, levelMax);
      }

      public void CreateDraggingPlayerInfoUIController(TeamRoomPlayerInfoUIController ctrl)
      {
        this.m_owner.CreateDraggingPlayerInfoUIController(ctrl);
      }

      public void DestroyDragginPlayerInfoUIController()
      {
        this.m_owner.DestroyDragginPlayerInfoUIController();
      }

      public void MoveDraggingPlayerInfoUIController(Vector2 pos)
      {
        this.m_owner.MoveDraggingPlayerInfoUIController(pos);
      }

      public void DropDraggingPlayerInfoUIController(Vector3 pos)
      {
        this.m_owner.DropDraggingPlayerInfoUIController(pos);
      }

      public void OnLeaveButtonClick()
      {
        this.m_owner.OnLeaveButtonClick();
      }

      public void OnStartButtonClick()
      {
        this.m_owner.OnStartButtonClick();
      }

      public void OnShowChangePlayerPositionButtonClick()
      {
        this.m_owner.OnShowChangePlayerPositionButtonClick();
      }

      public void OnChangePlayerPositionCompleteButtonClick()
      {
        this.m_owner.OnChangePlayerPositionCompleteButtonClick();
      }

      public void OnChatButtonClick()
      {
        this.m_owner.OnChatButtonClick();
      }

      public void OnAuthorityDropdownValueChanged(int value)
      {
        this.m_owner.OnAuthorityDropdownValueChanged(value);
      }

      public void OnAuthorityLockButtonClick()
      {
        this.m_owner.OnAuthorityLockButtonClick();
      }

      public void TeamRoomPlayerInfoUIController_OnInviteButtonClick(
        TeamRoomPlayerInfoUIController ctrl)
      {
        this.m_owner.TeamRoomPlayerInfoUIController_OnInviteButtonClick(ctrl);
      }

      public void TeamRoomPlayerInfoUIController_OnPlayerButtonClick(
        TeamRoomPlayerInfoUIController ctrl)
      {
        this.m_owner.TeamRoomPlayerInfoUIController_OnPlayerButtonClick(ctrl);
      }

      public void TeamRoomPlayerInfoUIController_OnBlessingButtonClick(
        TeamRoomPlayerInfoUIController ctrl)
      {
        this.m_owner.TeamRoomPlayerInfoUIController_OnBlessingButtonClick(ctrl);
      }

      public void TeamRoomPlayerInfoUIController_OnBeginDrag(
        TeamRoomPlayerInfoUIController ctrl,
        PointerEventData eventData)
      {
        this.m_owner.TeamRoomPlayerInfoUIController_OnBeginDrag(ctrl, eventData);
      }

      public void TeamRoomPlayerInfoUIController_OnEndDrag(
        TeamRoomPlayerInfoUIController ctrl,
        PointerEventData eventData)
      {
        this.m_owner.TeamRoomPlayerInfoUIController_OnEndDrag(ctrl, eventData);
      }

      public void TeamRoomPlayerInfoUIController_OnDrag(PointerEventData eventData)
      {
        this.m_owner.TeamRoomPlayerInfoUIController_OnDrag(eventData);
      }
    }
  }
}
