﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ArenaDefendMapUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Misc;
using SLua;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class ArenaDefendMapUIController : UIControllerBase
  {
    private GameObjectPool m_stagePosition0Pool;
    private GameObjectPool m_stagePosition1Pool;
    [AutoBind("./Grid", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_gridGameObject;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    private ArenaDefendBattle m_arenaDefendBattle;

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaDefendMapUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void AddChildPrefab(GameObject go, string assetName)
    {
      // ISSUE: unable to decompile the method.
    }

    public void Initialize(ArenaDefendBattle battle)
    {
      this.m_arenaDefendBattle = battle;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowStagePosition(GridPosition pos, bool isEmpty)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowStageAttackerPosition(GridPosition pos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideStagePositions()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayOnStageFx(GridPosition pos)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Vector3 StagePositionToWorldPosition(GridPosition pos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Vector3 GridPositionToWorldPosition(GridPosition p, float z)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
