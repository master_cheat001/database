﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.NormalItemBuyUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class NormalItemBuyUIController : UIControllerBase
  {
    private int m_selfChooseIndex = -1;
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_itemBuyPanelUIStateController;
    [AutoBind("./LayoutRoot/ItemGoodsDesc/Item/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_itemIconImage;
    [AutoBind("./LayoutRoot/ItemGoodsDesc/Item/CountText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_itemCountText;
    [AutoBind("./LayoutRoot/ItemGoodsDesc/Item/Image", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_itemCountBgGo;
    [AutoBind("./LayoutRoot/ItemGoodsDesc/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_itemNameText;
    [AutoBind("./LayoutRoot/ItemGoodsDesc/GoodCount", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_itemGoodCountObj;
    [AutoBind("./LayoutRoot/ItemGoodsDesc/GoodCount/HaveCountText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_itemHaveCountText;
    [AutoBind("./LayoutRoot/ItemGoodsDesc/DescPanel/DescText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_itemDescText;
    [AutoBind("./LayoutRoot/BuyPanel/Group/PriceIcon/PriceIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_itemPriceIcon;
    [AutoBind("./LayoutRoot/BuyPanel/Group/PriceIcon/PriceIcon/U_crystal", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_itemPriceIconEffectGameObject;
    [AutoBind("./LayoutRoot/BuyPanel/Group/PriceText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_itemPriceText;
    [AutoBind("./BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_itemBuyPanelCloseButton;
    [AutoBind("./LayoutRoot/BuyPanel", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_itemBuyButton;
    [AutoBind("./ResonanceInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_resonanceInfoPanel;
    [AutoBind("./ResonanceInfoPanel/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_resonanceInfoPanelNameText;
    [AutoBind("./ResonanceInfoPanel/SuitInfo/2SuitInfo/2SuitInfoText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_resonanceInfoPanel2SuitInfoText;
    [AutoBind("./ResonanceInfoPanel/SuitInfo/4SuitInfo/4SuitInfoText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_resonanceInfoPanel4SuitInfoText;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private NormalItemBuyUITask m_normalItemBuyUITask;
    private StoreId m_storeId;
    private int m_fixedStoreItemId;
    [DoNotToLua]
    private NormalItemBuyUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_InitNormalItemBuyUITask_hotfix;
    private LuaFunction m_ClosePanelAction_hotfix;
    private LuaFunction m_ShowItemBuyPanelStoreIdInt32_hotfix;
    private LuaFunction m_ShowSelfChooseITemBuyPlaneStoreIdInt32Int32_hotfix;
    private LuaFunction m_SetFiexdStoreItemBuyPanelStoreIdInt32_hotfix;
    private LuaFunction m_SetFiexdStoreSelfChooseItemBuyPanelStoreIdInt32Int32_hotfix;
    private LuaFunction m_SetFiexdStoreBuyPanelGoodsGoodsTypeInt32_hotfix;
    private LuaFunction m_SetEnchantStoneResonanceInfoPanelGoodsTypeInt32_hotfix;
    private LuaFunction m_HandleBoxOpenNetTaskGoodsTypeInt32Int32Action`1Action_hotfix;
    private LuaFunction m_GetStoreGoodsInt32Int32_hotfix;
    private LuaFunction m_GetSelfChooseGoodsInt32Int32_hotfix;
    private LuaFunction m_StoreUIController_CrystalNotEnough_hotfix;
    private LuaFunction m_OnBGButtonClick_hotfix;
    private LuaFunction m_OnBuyItemClick_hotfix;
    private LuaFunction m_add_EventOnBuySuccessAction_hotfix;
    private LuaFunction m_remove_EventOnBuySuccessAction_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init(NormalItemBuyUITask normalItemBuyUITask)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClosePanel(Action OnCompleteClose)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowItemBuyPanel(StoreId storeId, int fixedStoreItemId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowSelfChooseITemBuyPlane(
      StoreId storeId,
      int fixedStoreItemId,
      int selfChooseIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetFiexdStoreItemBuyPanel(StoreId storeId, int fixedStoreItemId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetFiexdStoreSelfChooseItemBuyPanel(
      StoreId storeId,
      int fixedStoreItemId,
      int selfChooseIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetFiexdStoreBuyPanel(Goods goods, GoodsType currencyType, int price)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetEnchantStoneResonanceInfoPanel(GoodsType goodsType, int goodsID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HandleBoxOpenNetTask(
      GoodsType type,
      int id,
      int count,
      Action<List<Goods>> successedCallback,
      Action failedCallback)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<Goods> GetStoreGoods(int fixedStoreItemId, int selfChooseIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Goods GetSelfChooseGoods(int fixedStoreItemId, int selfChooseIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StoreUIController_CrystalNotEnough()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBGButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBuyItemClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnBuySuccess
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public NormalItemBuyUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnBuySuccess()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnBuySuccess()
    {
      this.EventOnBuySuccess = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private NormalItemBuyUIController m_owner;

      public LuaExportHelper(NormalItemBuyUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnBuySuccess()
      {
        this.m_owner.__callDele_EventOnBuySuccess();
      }

      public void __clearDele_EventOnBuySuccess()
      {
        this.m_owner.__clearDele_EventOnBuySuccess();
      }

      public CommonUIStateController m_itemBuyPanelUIStateController
      {
        get
        {
          return this.m_owner.m_itemBuyPanelUIStateController;
        }
        set
        {
          this.m_owner.m_itemBuyPanelUIStateController = value;
        }
      }

      public Image m_itemIconImage
      {
        get
        {
          return this.m_owner.m_itemIconImage;
        }
        set
        {
          this.m_owner.m_itemIconImage = value;
        }
      }

      public Text m_itemCountText
      {
        get
        {
          return this.m_owner.m_itemCountText;
        }
        set
        {
          this.m_owner.m_itemCountText = value;
        }
      }

      public GameObject m_itemCountBgGo
      {
        get
        {
          return this.m_owner.m_itemCountBgGo;
        }
        set
        {
          this.m_owner.m_itemCountBgGo = value;
        }
      }

      public Text m_itemNameText
      {
        get
        {
          return this.m_owner.m_itemNameText;
        }
        set
        {
          this.m_owner.m_itemNameText = value;
        }
      }

      public GameObject m_itemGoodCountObj
      {
        get
        {
          return this.m_owner.m_itemGoodCountObj;
        }
        set
        {
          this.m_owner.m_itemGoodCountObj = value;
        }
      }

      public Text m_itemHaveCountText
      {
        get
        {
          return this.m_owner.m_itemHaveCountText;
        }
        set
        {
          this.m_owner.m_itemHaveCountText = value;
        }
      }

      public Text m_itemDescText
      {
        get
        {
          return this.m_owner.m_itemDescText;
        }
        set
        {
          this.m_owner.m_itemDescText = value;
        }
      }

      public Image m_itemPriceIcon
      {
        get
        {
          return this.m_owner.m_itemPriceIcon;
        }
        set
        {
          this.m_owner.m_itemPriceIcon = value;
        }
      }

      public GameObject m_itemPriceIconEffectGameObject
      {
        get
        {
          return this.m_owner.m_itemPriceIconEffectGameObject;
        }
        set
        {
          this.m_owner.m_itemPriceIconEffectGameObject = value;
        }
      }

      public Text m_itemPriceText
      {
        get
        {
          return this.m_owner.m_itemPriceText;
        }
        set
        {
          this.m_owner.m_itemPriceText = value;
        }
      }

      public Button m_itemBuyPanelCloseButton
      {
        get
        {
          return this.m_owner.m_itemBuyPanelCloseButton;
        }
        set
        {
          this.m_owner.m_itemBuyPanelCloseButton = value;
        }
      }

      public Button m_itemBuyButton
      {
        get
        {
          return this.m_owner.m_itemBuyButton;
        }
        set
        {
          this.m_owner.m_itemBuyButton = value;
        }
      }

      public GameObject m_resonanceInfoPanel
      {
        get
        {
          return this.m_owner.m_resonanceInfoPanel;
        }
        set
        {
          this.m_owner.m_resonanceInfoPanel = value;
        }
      }

      public Text m_resonanceInfoPanelNameText
      {
        get
        {
          return this.m_owner.m_resonanceInfoPanelNameText;
        }
        set
        {
          this.m_owner.m_resonanceInfoPanelNameText = value;
        }
      }

      public Text m_resonanceInfoPanel2SuitInfoText
      {
        get
        {
          return this.m_owner.m_resonanceInfoPanel2SuitInfoText;
        }
        set
        {
          this.m_owner.m_resonanceInfoPanel2SuitInfoText = value;
        }
      }

      public Text m_resonanceInfoPanel4SuitInfoText
      {
        get
        {
          return this.m_owner.m_resonanceInfoPanel4SuitInfoText;
        }
        set
        {
          this.m_owner.m_resonanceInfoPanel4SuitInfoText = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public NormalItemBuyUITask m_normalItemBuyUITask
      {
        get
        {
          return this.m_owner.m_normalItemBuyUITask;
        }
        set
        {
          this.m_owner.m_normalItemBuyUITask = value;
        }
      }

      public StoreId m_storeId
      {
        get
        {
          return this.m_owner.m_storeId;
        }
        set
        {
          this.m_owner.m_storeId = value;
        }
      }

      public int m_fixedStoreItemId
      {
        get
        {
          return this.m_owner.m_fixedStoreItemId;
        }
        set
        {
          this.m_owner.m_fixedStoreItemId = value;
        }
      }

      public int m_selfChooseIndex
      {
        get
        {
          return this.m_owner.m_selfChooseIndex;
        }
        set
        {
          this.m_owner.m_selfChooseIndex = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void SetFiexdStoreItemBuyPanel(StoreId storeId, int fixedStoreItemId)
      {
        this.m_owner.SetFiexdStoreItemBuyPanel(storeId, fixedStoreItemId);
      }

      public void SetFiexdStoreBuyPanel(Goods goods, GoodsType currencyType, int price)
      {
        this.m_owner.SetFiexdStoreBuyPanel(goods, currencyType, price);
      }

      public void SetEnchantStoneResonanceInfoPanel(GoodsType goodsType, int goodsID)
      {
        this.m_owner.SetEnchantStoneResonanceInfoPanel(goodsType, goodsID);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void HandleBoxOpenNetTask(
        GoodsType type,
        int id,
        int count,
        Action<List<Goods>> successedCallback,
        Action failedCallback)
      {
        // ISSUE: unable to decompile the method.
      }

      public List<Goods> GetStoreGoods(int fixedStoreItemId, int selfChooseIndex)
      {
        return this.m_owner.GetStoreGoods(fixedStoreItemId, selfChooseIndex);
      }

      public Goods GetSelfChooseGoods(int fixedStoreItemId, int selfChooseIndex)
      {
        return this.m_owner.GetSelfChooseGoods(fixedStoreItemId, selfChooseIndex);
      }

      public void StoreUIController_CrystalNotEnough()
      {
        this.m_owner.StoreUIController_CrystalNotEnough();
      }
    }
  }
}
