﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.EquipmentEnchantNetTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.Common;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class EquipmentEnchantNetTask : UINetTask
  {
    private ulong m_instanceId;
    private ulong m_enchantStoneInstanceId;
    public int NewResonanceId;
    public List<CommonBattleProperty> Properties;

    [MethodImpl((MethodImplOptions) 32768)]
    public EquipmentEnchantNetTask(ulong equipmentInstanceId, ulong enchantStoneInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterNetworkEvent()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnEquipmentEnchantAck(
      int result,
      int newResonanceId,
      List<CommonBattleProperty> properties)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool StartNetWorking()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
