﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ScreenFade
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class ScreenFade
  {
    private float m_curTime;
    private float m_duration;
    private int m_delayFrame;
    private bool m_isFadeIn;
    private bool m_isEnd;
    private Color m_color;
    private Action m_onEnd;
    private Image m_image;

    [MethodImpl((MethodImplOptions) 32768)]
    public ScreenFade()
    {
    }

    public void Setup(Image image)
    {
      this.m_image = image;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FadeIn(float time, Color color, Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FadeOut(float time, Color color, Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsFading()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Update(float dt)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
