﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BattlePrepareStageActor
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Scene;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class BattlePrepareStageActor
  {
    private GridPosition m_position;
    private int m_direction;
    private int m_team;
    private int m_playerIndex;
    private BattleHero m_hero;
    private int m_behaviorId;
    private int m_groupId;
    private StageActorTagType m_tagType;
    private StagePositionType m_stagePositionType;
    private bool m_isGray;
    private int m_heroHealthPoint;
    private int m_soldierHealthPoint;
    private ConfigDataSkillInfo m_extraTalentSkillInfo;
    private ClientBattle m_clientBattle;
    private GenericGraphic[] m_graphics;
    private int m_soldierCount;
    private Colori m_tweenFromColor;
    private Colori m_tweenToColor;
    private float m_tweenColorTime;
    private BattleActorUIController m_uiController;
    [DoNotToLua]
    private BattlePrepareStageActor.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctorClientBattle_hotfix;
    private LuaFunction m_Destroy_hotfix;
    private LuaFunction m_DestroyGraphic_hotfix;
    private LuaFunction m_DestroyUI_hotfix;
    private LuaFunction m_SetupBattleHeroInt32StagePositionTypeStageActorTagTypeInt32Int32Int32Boolean_hotfix;
    private LuaFunction m_GetHero_hotfix;
    private LuaFunction m_GetHeroId_hotfix;
    private LuaFunction m_GetSoldierId_hotfix;
    private LuaFunction m_GetSkillList_hotfix;
    private LuaFunction m_SetHpInt32Int32Int32Int32_hotfix;
    private LuaFunction m_GetHeroHp_hotfix;
    private LuaFunction m_GetSoldierHp_hotfix;
    private LuaFunction m_SetExtraTalentSkillInfoConfigDataSkillInfo_hotfix;
    private LuaFunction m_GetExtraTalentSkillInfo_hotfix;
    private LuaFunction m_PlayAnimationStringBoolean_hotfix;
    private LuaFunction m_SetPositionGridPositionInt32_hotfix;
    private LuaFunction m_SetGrayBoolean_hotfix;
    private LuaFunction m_SetChooseEffectBoolean_hotfix;
    private LuaFunction m_IsGray_hotfix;
    private LuaFunction m_ForceUpdateSpine_hotfix;
    private LuaFunction m_GetPosition_hotfix;
    private LuaFunction m_GetDirection_hotfix;
    private LuaFunction m_GetTeam_hotfix;
    private LuaFunction m_GetPlayerIndex_hotfix;
    private LuaFunction m_GetTagType_hotfix;
    private LuaFunction m_GetPositionType_hotfix;
    private LuaFunction m_GetBehaviorId_hotfix;
    private LuaFunction m_GetGroupId_hotfix;
    private LuaFunction m_IsNpc_hotfix;
    private LuaFunction m_IsAINpc_hotfix;
    private LuaFunction m_IsTeammate_hotfix;
    private LuaFunction m_IsEnforce_hotfix;
    private LuaFunction m_TweenColorColori_hotfix;
    private LuaFunction m_GetUIController_hotfix;
    private LuaFunction m_TickGraphicSingle_hotfix;
    private LuaFunction m_PauseBoolean_hotfix;
    private LuaFunction m_ComputeGraphicPositionVector2_hotfix;
    private LuaFunction m_ComputeGraphicOffsetInt32Int32_hotfix;
    private LuaFunction m_ComputeSoldierGraphicCountInt32Int32_hotfix;
    private LuaFunction m_UpdateGraphicVisible_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattlePrepareStageActor(ClientBattle clientBattle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Destroy()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DestroyGraphic()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DestroyUI()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Setup(
      BattleHero hero,
      int team,
      StagePositionType posType,
      StageActorTagType tagType,
      int behaviorId,
      int groupId,
      int playerIndex,
      bool isSkipGraphic)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleHero GetHero()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetSoldierId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetSkillList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHp(int heroHp, int heroHpMax, int soldierHp, int soldierHpMax)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroHp()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetSoldierHp()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetExtraTalentSkillInfo(ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSkillInfo GetExtraTalentSkillInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayAnimation(string name, bool loop)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPosition(GridPosition p, int dir)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGray(bool isGray)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetChooseEffect(bool canChoose)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsGray()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ForceUpdateSpine()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GridPosition GetPosition()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetDirection()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetTeam()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPlayerIndex()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public StageActorTagType GetTagType()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public StagePositionType GetPositionType()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetBehaviorId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetGroupId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsNpc()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsAINpc()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsTeammate()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsEnforce()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void TweenColor(Colori c)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleActorUIController GetUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void TickGraphic(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Pause(bool pause)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Vector3 ComputeGraphicPosition(Vector2 p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Vector2 ComputeGraphicOffset(int idx, int dir)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int ComputeSoldierGraphicCount(int soldierHp, int soldierHpMax)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateGraphicVisible()
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public BattlePrepareStageActor.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private BattlePrepareStageActor m_owner;

      public LuaExportHelper(BattlePrepareStageActor owner)
      {
        this.m_owner = owner;
      }

      public GridPosition m_position
      {
        get
        {
          return this.m_owner.m_position;
        }
        set
        {
          this.m_owner.m_position = value;
        }
      }

      public int m_direction
      {
        get
        {
          return this.m_owner.m_direction;
        }
        set
        {
          this.m_owner.m_direction = value;
        }
      }

      public int m_team
      {
        get
        {
          return this.m_owner.m_team;
        }
        set
        {
          this.m_owner.m_team = value;
        }
      }

      public int m_playerIndex
      {
        get
        {
          return this.m_owner.m_playerIndex;
        }
        set
        {
          this.m_owner.m_playerIndex = value;
        }
      }

      public BattleHero m_hero
      {
        get
        {
          return this.m_owner.m_hero;
        }
        set
        {
          this.m_owner.m_hero = value;
        }
      }

      public int m_behaviorId
      {
        get
        {
          return this.m_owner.m_behaviorId;
        }
        set
        {
          this.m_owner.m_behaviorId = value;
        }
      }

      public int m_groupId
      {
        get
        {
          return this.m_owner.m_groupId;
        }
        set
        {
          this.m_owner.m_groupId = value;
        }
      }

      public StageActorTagType m_tagType
      {
        get
        {
          return this.m_owner.m_tagType;
        }
        set
        {
          this.m_owner.m_tagType = value;
        }
      }

      public StagePositionType m_stagePositionType
      {
        get
        {
          return this.m_owner.m_stagePositionType;
        }
        set
        {
          this.m_owner.m_stagePositionType = value;
        }
      }

      public bool m_isGray
      {
        get
        {
          return this.m_owner.m_isGray;
        }
        set
        {
          this.m_owner.m_isGray = value;
        }
      }

      public int m_heroHealthPoint
      {
        get
        {
          return this.m_owner.m_heroHealthPoint;
        }
        set
        {
          this.m_owner.m_heroHealthPoint = value;
        }
      }

      public int m_soldierHealthPoint
      {
        get
        {
          return this.m_owner.m_soldierHealthPoint;
        }
        set
        {
          this.m_owner.m_soldierHealthPoint = value;
        }
      }

      public ConfigDataSkillInfo m_extraTalentSkillInfo
      {
        get
        {
          return this.m_owner.m_extraTalentSkillInfo;
        }
        set
        {
          this.m_owner.m_extraTalentSkillInfo = value;
        }
      }

      public ClientBattle m_clientBattle
      {
        get
        {
          return this.m_owner.m_clientBattle;
        }
        set
        {
          this.m_owner.m_clientBattle = value;
        }
      }

      public GenericGraphic[] m_graphics
      {
        get
        {
          return this.m_owner.m_graphics;
        }
        set
        {
          this.m_owner.m_graphics = value;
        }
      }

      public int m_soldierCount
      {
        get
        {
          return this.m_owner.m_soldierCount;
        }
        set
        {
          this.m_owner.m_soldierCount = value;
        }
      }

      public Colori m_tweenFromColor
      {
        get
        {
          return this.m_owner.m_tweenFromColor;
        }
        set
        {
          this.m_owner.m_tweenFromColor = value;
        }
      }

      public Colori m_tweenToColor
      {
        get
        {
          return this.m_owner.m_tweenToColor;
        }
        set
        {
          this.m_owner.m_tweenToColor = value;
        }
      }

      public float m_tweenColorTime
      {
        get
        {
          return this.m_owner.m_tweenColorTime;
        }
        set
        {
          this.m_owner.m_tweenColorTime = value;
        }
      }

      public BattleActorUIController m_uiController
      {
        get
        {
          return this.m_owner.m_uiController;
        }
        set
        {
          this.m_owner.m_uiController = value;
        }
      }

      public void DestroyGraphic()
      {
        this.m_owner.DestroyGraphic();
      }

      public void DestroyUI()
      {
        this.m_owner.DestroyUI();
      }

      public void PlayAnimation(string name, bool loop)
      {
        this.m_owner.PlayAnimation(name, loop);
      }

      public Vector3 ComputeGraphicPosition(Vector2 p)
      {
        return this.m_owner.ComputeGraphicPosition(p);
      }

      public Vector2 ComputeGraphicOffset(int idx, int dir)
      {
        return this.m_owner.ComputeGraphicOffset(idx, dir);
      }

      public int ComputeSoldierGraphicCount(int soldierHp, int soldierHpMax)
      {
        return this.m_owner.ComputeSoldierGraphicCount(soldierHp, soldierHpMax);
      }

      public void UpdateGraphicVisible()
      {
        this.m_owner.UpdateGraphicVisible();
      }
    }
  }
}
