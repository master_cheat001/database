﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.GiftStoreCancelBuyNetTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class GiftStoreCancelBuyNetTask : UINetTask
  {
    private int m_giftStoreItemId;

    [MethodImpl((MethodImplOptions) 32768)]
    public GiftStoreCancelBuyNetTask(int giftStoreItemid)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool StartNetWorking()
    {
      // ISSUE: unable to decompile the method.
    }

    protected void OnGiftStoreCancelBuyStoreItemAck(int result, int goodsId)
    {
      this.Result = result;
      this.OnTransactionComplete();
    }
  }
}
