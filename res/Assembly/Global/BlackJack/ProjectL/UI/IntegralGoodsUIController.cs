﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.IntegralGoodsUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class IntegralGoodsUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_stateCtrl;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_button;
    [AutoBind("./BGImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_bgImage;
    [AutoBind("./IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_iconImage;
    [AutoBind("./IconImage/U_crystal", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_crystalEffect;
    [AutoBind("./SSREffect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_ssrEffect;
    [AutoBind("./CountValue", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_countValueText;
    [AutoBind("./IntegralGroup/IntegralText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_integralText;
    private IntegralGoodsUIController.IntegralGoodsState m_integralGoodsState;
    [DoNotToLua]
    private IntegralGoodsUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_InitIntegralGoodsInfoConfigDataNoviceRewardInfoInt32_hotfix;
    private LuaFunction m_SetIntegralGoodsState_hotfix;
    private LuaFunction m_SetIntegralGoodsInfo_hotfix;
    private LuaFunction m_OnItemClick_hotfix;
    private LuaFunction m_PlayGotChangeEffectAction_hotfix;
    private LuaFunction m_add_EventOnIntegralGoodsClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnIntegralGoodsClickAction`1_hotfix;
    private LuaFunction m_set_SlotInt32_hotfix;
    private LuaFunction m_get_Slot_hotfix;
    private LuaFunction m_set_NoviceRewardConfigDataNoviceRewardInfo_hotfix;
    private LuaFunction m_get_NoviceReward_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitIntegralGoodsInfo(ConfigDataNoviceRewardInfo reward, int slot)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetIntegralGoodsState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetIntegralGoodsInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnItemClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayGotChangeEffect(Action OnStateFinish)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<IntegralGoodsUIController> EventOnIntegralGoodsClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Slot
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataNoviceRewardInfo NoviceReward
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public IntegralGoodsUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnIntegralGoodsClick(IntegralGoodsUIController obj)
    {
    }

    private void __clearDele_EventOnIntegralGoodsClick(IntegralGoodsUIController obj)
    {
      this.EventOnIntegralGoodsClick = (Action<IntegralGoodsUIController>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public enum IntegralGoodsState
    {
      Normal,
      CanGet,
      Got,
    }

    public class LuaExportHelper
    {
      private IntegralGoodsUIController m_owner;

      public LuaExportHelper(IntegralGoodsUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        return this.m_owner.__callBase_BindFieldImpl(fieldType, path, initState, fieldName, ctrlName, optional);
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnIntegralGoodsClick(IntegralGoodsUIController obj)
      {
        this.m_owner.__callDele_EventOnIntegralGoodsClick(obj);
      }

      public void __clearDele_EventOnIntegralGoodsClick(IntegralGoodsUIController obj)
      {
        this.m_owner.__clearDele_EventOnIntegralGoodsClick(obj);
      }

      public CommonUIStateController m_stateCtrl
      {
        get
        {
          return this.m_owner.m_stateCtrl;
        }
        set
        {
          this.m_owner.m_stateCtrl = value;
        }
      }

      public Button m_button
      {
        get
        {
          return this.m_owner.m_button;
        }
        set
        {
          this.m_owner.m_button = value;
        }
      }

      public Image m_bgImage
      {
        get
        {
          return this.m_owner.m_bgImage;
        }
        set
        {
          this.m_owner.m_bgImage = value;
        }
      }

      public Image m_iconImage
      {
        get
        {
          return this.m_owner.m_iconImage;
        }
        set
        {
          this.m_owner.m_iconImage = value;
        }
      }

      public GameObject m_crystalEffect
      {
        get
        {
          return this.m_owner.m_crystalEffect;
        }
        set
        {
          this.m_owner.m_crystalEffect = value;
        }
      }

      public GameObject m_ssrEffect
      {
        get
        {
          return this.m_owner.m_ssrEffect;
        }
        set
        {
          this.m_owner.m_ssrEffect = value;
        }
      }

      public Text m_countValueText
      {
        get
        {
          return this.m_owner.m_countValueText;
        }
        set
        {
          this.m_owner.m_countValueText = value;
        }
      }

      public Text m_integralText
      {
        get
        {
          return this.m_owner.m_integralText;
        }
        set
        {
          this.m_owner.m_integralText = value;
        }
      }

      public IntegralGoodsUIController.IntegralGoodsState m_integralGoodsState
      {
        get
        {
          return this.m_owner.m_integralGoodsState;
        }
        set
        {
          this.m_owner.m_integralGoodsState = value;
        }
      }

      public int Slot
      {
        set
        {
          this.m_owner.Slot = value;
        }
      }

      public ConfigDataNoviceRewardInfo NoviceReward
      {
        set
        {
          this.m_owner.NoviceReward = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void SetIntegralGoodsState()
      {
        this.m_owner.SetIntegralGoodsState();
      }

      public void SetIntegralGoodsInfo()
      {
        this.m_owner.SetIntegralGoodsInfo();
      }

      public void OnItemClick()
      {
        this.m_owner.OnItemClick();
      }
    }
  }
}
