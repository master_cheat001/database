﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.MissionUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using MarchingBytes;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class MissionUIController : UIControllerBase
  {
    [AutoBind("./Margin", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("./CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_closeButton;
    [AutoBind("./Margin/FilterToggles/DayMission", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_dayMissionToggle;
    [AutoBind("./Margin/FilterToggles/DayMission/Click/RedTag", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_dayMissionToggleClickTag;
    [AutoBind("./Margin/FilterToggles/DayMission/Click/RedTag/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_dayMissionToggleClickTagNum;
    [AutoBind("./Margin/FilterToggles/DayMission/UnClick/RedTag", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_dayMissionToggleUnClickTag;
    [AutoBind("./Margin/FilterToggles/DayMission/UnClick/RedTag/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_dayMissionToggleUnClickTagNum;
    [AutoBind("./Margin/FilterToggles/ChallengeMission", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_challengeMissionToggle;
    [AutoBind("./Margin/FilterToggles/ChallengeMission/Click/RedTag", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_challengeMissionToggleClickTag;
    [AutoBind("./Margin/FilterToggles/ChallengeMission/Click/RedTag/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_challengeMissionToggleClickTagNum;
    [AutoBind("./Margin/FilterToggles/ChallengeMission/UnClick/RedTag", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_challengeMissionToggleUnClickTag;
    [AutoBind("./Margin/FilterToggles/ChallengeMission/UnClick/RedTag/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_challengeMissionToggleUnClickTagNum;
    [AutoBind("./Margin/FilterToggles/Achievement", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_achievementToggle;
    [AutoBind("./Margin/FilterToggles/Achievement/Click/RedTag", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_achievementToggleClickTag;
    [AutoBind("./Margin/FilterToggles/Achievement/Click/RedTag/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_achievementToggleClickTagNum;
    [AutoBind("./Margin/FilterToggles/Achievement/UnClick/RedTag", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_achievementToggleUnClickTag;
    [AutoBind("./Margin/FilterToggles/Achievement/UnClick/RedTag/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_achievementToggleUnClickTagNum;
    [AutoBind("./MissionList", AutoBindAttribute.InitState.NotInit, false)]
    private LoopVerticalScrollRect m_missionListScrollView;
    [AutoBind("./MissionList", AutoBindAttribute.InitState.NotInit, false)]
    private EasyObjectPool m_missionListItemPool;
    [AutoBind("./Tips", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_tipsUIStateController;
    [AutoBind("./Tips/Panel/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_tipsCancelButton;
    [AutoBind("./Tips/Panel/OkButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_tipsConfirmButton;
    [AutoBind("./Prefab/MissionItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_missionItemPrefab;
    private List<Mission> m_cachedMissionList;
    private int m_cachedMissionProcessingStartIndex;
    private int m_cachedMissionFinishedStartIndex;
    private int m_dayCompleteNum;
    private int m_challengeCompleteNum;
    private int m_achievementCompleteNum;
    private MissionColumnType m_curMissionColumnType;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private GetRewardGoodsUITask m_getRewardGoodsUITask;
    private int m_oldPlayerLevel;
    private int m_missionID;
    private const string c_missionListItemPrefabName = "MissionListItemUIPrefab";
    [DoNotToLua]
    private MissionUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_InitItemListPool_hotfix;
    private LuaFunction m_OnPoolObjCreatedStringGameObject_hotfix;
    private LuaFunction m_OnListItemFillUIControllerBase_hotfix;
    private LuaFunction m_ShowMissionListByMissionColumnType_hotfix;
    private LuaFunction m_ShowToggleTagNumTextBooleanBooleanBoolean_hotfix;
    private LuaFunction m_CompareMissionBySortIdMissionMission_hotfix;
    private LuaFunction m_AddMissionColumnRedTagNumMission_hotfix;
    private LuaFunction m_ResetRedTagNum_hotfix;
    private LuaFunction m_OnGetRewardButtonClickInt32_hotfix;
    private LuaFunction m_ShowRewardsList`1_hotfix;
    private LuaFunction m_GetRewardGoodsUITask_OnClose_hotfix;
    private LuaFunction m_OnGotoLayerButtonClickGetPathData_hotfix;
    private LuaFunction m_OnCloseButtonClick_hotfix;
    private LuaFunction m_OnTipsBackgroundButtonClick_hotfix;
    private LuaFunction m_OnTipsConfirmButtonClick_hotfix;
    private LuaFunction m_ResetScrollViewToTop_hotfix;
    private LuaFunction m_OnDayMissionToggleValueChangedBoolean_hotfix;
    private LuaFunction m_OnChallengeToggleValueChangedBoolean_hotfix;
    private LuaFunction m_OnAchievementToggleValueChangedBoolean_hotfix;
    private LuaFunction m_add_EventOnGetRewardButtonClickAction`2_hotfix;
    private LuaFunction m_remove_EventOnGetRewardButtonClickAction`2_hotfix;
    private LuaFunction m_add_EventOnGotoLayerButtonClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnGotoLayerButtonClickAction`1_hotfix;
    private LuaFunction m_add_EventOnCloseAction_hotfix;
    private LuaFunction m_remove_EventOnCloseAction_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public MissionUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitItemListPool()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPoolObjCreated(string poolName, GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnListItemFill(UIControllerBase itemCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowMissionListByMissionColumnType()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowToggleTagNumText(bool day, bool challenge, bool achievement)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int CompareMissionBySortId(Mission m1, Mission m2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddMissionColumnRedTagNum(Mission mission)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ResetRedTagNum()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGetRewardButtonClick(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowRewards(List<Goods> rewards)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GetRewardGoodsUITask_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGotoLayerButtonClick(GetPathData pathInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTipsBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTipsConfirmButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ResetScrollViewToTop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDayMissionToggleValueChanged(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChallengeToggleValueChanged(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAchievementToggleValueChanged(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int, Action<List<Goods>>> EventOnGetRewardButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<GetPathData> EventOnGotoLayerButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public MissionUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnGetRewardButtonClick(int arg1, Action<List<Goods>> arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnGetRewardButtonClick(int arg1, Action<List<Goods>> arg2)
    {
      this.EventOnGetRewardButtonClick = (Action<int, Action<List<Goods>>>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnGotoLayerButtonClick(GetPathData obj)
    {
    }

    private void __clearDele_EventOnGotoLayerButtonClick(GetPathData obj)
    {
      this.EventOnGotoLayerButtonClick = (Action<GetPathData>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnClose()
    {
      this.EventOnClose = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private MissionUIController m_owner;

      public LuaExportHelper(MissionUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnGetRewardButtonClick(int arg1, Action<List<Goods>> arg2)
      {
        this.m_owner.__callDele_EventOnGetRewardButtonClick(arg1, arg2);
      }

      public void __clearDele_EventOnGetRewardButtonClick(int arg1, Action<List<Goods>> arg2)
      {
        this.m_owner.__clearDele_EventOnGetRewardButtonClick(arg1, arg2);
      }

      public void __callDele_EventOnGotoLayerButtonClick(GetPathData obj)
      {
        this.m_owner.__callDele_EventOnGotoLayerButtonClick(obj);
      }

      public void __clearDele_EventOnGotoLayerButtonClick(GetPathData obj)
      {
        this.m_owner.__clearDele_EventOnGotoLayerButtonClick(obj);
      }

      public void __callDele_EventOnClose()
      {
        this.m_owner.__callDele_EventOnClose();
      }

      public void __clearDele_EventOnClose()
      {
        this.m_owner.__clearDele_EventOnClose();
      }

      public RectTransform m_marginTransform
      {
        get
        {
          return this.m_owner.m_marginTransform;
        }
        set
        {
          this.m_owner.m_marginTransform = value;
        }
      }

      public Button m_closeButton
      {
        get
        {
          return this.m_owner.m_closeButton;
        }
        set
        {
          this.m_owner.m_closeButton = value;
        }
      }

      public Toggle m_dayMissionToggle
      {
        get
        {
          return this.m_owner.m_dayMissionToggle;
        }
        set
        {
          this.m_owner.m_dayMissionToggle = value;
        }
      }

      public GameObject m_dayMissionToggleClickTag
      {
        get
        {
          return this.m_owner.m_dayMissionToggleClickTag;
        }
        set
        {
          this.m_owner.m_dayMissionToggleClickTag = value;
        }
      }

      public Text m_dayMissionToggleClickTagNum
      {
        get
        {
          return this.m_owner.m_dayMissionToggleClickTagNum;
        }
        set
        {
          this.m_owner.m_dayMissionToggleClickTagNum = value;
        }
      }

      public GameObject m_dayMissionToggleUnClickTag
      {
        get
        {
          return this.m_owner.m_dayMissionToggleUnClickTag;
        }
        set
        {
          this.m_owner.m_dayMissionToggleUnClickTag = value;
        }
      }

      public Text m_dayMissionToggleUnClickTagNum
      {
        get
        {
          return this.m_owner.m_dayMissionToggleUnClickTagNum;
        }
        set
        {
          this.m_owner.m_dayMissionToggleUnClickTagNum = value;
        }
      }

      public Toggle m_challengeMissionToggle
      {
        get
        {
          return this.m_owner.m_challengeMissionToggle;
        }
        set
        {
          this.m_owner.m_challengeMissionToggle = value;
        }
      }

      public GameObject m_challengeMissionToggleClickTag
      {
        get
        {
          return this.m_owner.m_challengeMissionToggleClickTag;
        }
        set
        {
          this.m_owner.m_challengeMissionToggleClickTag = value;
        }
      }

      public Text m_challengeMissionToggleClickTagNum
      {
        get
        {
          return this.m_owner.m_challengeMissionToggleClickTagNum;
        }
        set
        {
          this.m_owner.m_challengeMissionToggleClickTagNum = value;
        }
      }

      public GameObject m_challengeMissionToggleUnClickTag
      {
        get
        {
          return this.m_owner.m_challengeMissionToggleUnClickTag;
        }
        set
        {
          this.m_owner.m_challengeMissionToggleUnClickTag = value;
        }
      }

      public Text m_challengeMissionToggleUnClickTagNum
      {
        get
        {
          return this.m_owner.m_challengeMissionToggleUnClickTagNum;
        }
        set
        {
          this.m_owner.m_challengeMissionToggleUnClickTagNum = value;
        }
      }

      public Toggle m_achievementToggle
      {
        get
        {
          return this.m_owner.m_achievementToggle;
        }
        set
        {
          this.m_owner.m_achievementToggle = value;
        }
      }

      public GameObject m_achievementToggleClickTag
      {
        get
        {
          return this.m_owner.m_achievementToggleClickTag;
        }
        set
        {
          this.m_owner.m_achievementToggleClickTag = value;
        }
      }

      public Text m_achievementToggleClickTagNum
      {
        get
        {
          return this.m_owner.m_achievementToggleClickTagNum;
        }
        set
        {
          this.m_owner.m_achievementToggleClickTagNum = value;
        }
      }

      public GameObject m_achievementToggleUnClickTag
      {
        get
        {
          return this.m_owner.m_achievementToggleUnClickTag;
        }
        set
        {
          this.m_owner.m_achievementToggleUnClickTag = value;
        }
      }

      public Text m_achievementToggleUnClickTagNum
      {
        get
        {
          return this.m_owner.m_achievementToggleUnClickTagNum;
        }
        set
        {
          this.m_owner.m_achievementToggleUnClickTagNum = value;
        }
      }

      public LoopVerticalScrollRect m_missionListScrollView
      {
        get
        {
          return this.m_owner.m_missionListScrollView;
        }
        set
        {
          this.m_owner.m_missionListScrollView = value;
        }
      }

      public EasyObjectPool m_missionListItemPool
      {
        get
        {
          return this.m_owner.m_missionListItemPool;
        }
        set
        {
          this.m_owner.m_missionListItemPool = value;
        }
      }

      public CommonUIStateController m_tipsUIStateController
      {
        get
        {
          return this.m_owner.m_tipsUIStateController;
        }
        set
        {
          this.m_owner.m_tipsUIStateController = value;
        }
      }

      public Button m_tipsCancelButton
      {
        get
        {
          return this.m_owner.m_tipsCancelButton;
        }
        set
        {
          this.m_owner.m_tipsCancelButton = value;
        }
      }

      public Button m_tipsConfirmButton
      {
        get
        {
          return this.m_owner.m_tipsConfirmButton;
        }
        set
        {
          this.m_owner.m_tipsConfirmButton = value;
        }
      }

      public GameObject m_missionItemPrefab
      {
        get
        {
          return this.m_owner.m_missionItemPrefab;
        }
        set
        {
          this.m_owner.m_missionItemPrefab = value;
        }
      }

      public List<Mission> m_cachedMissionList
      {
        get
        {
          return this.m_owner.m_cachedMissionList;
        }
        set
        {
          this.m_owner.m_cachedMissionList = value;
        }
      }

      public int m_cachedMissionProcessingStartIndex
      {
        get
        {
          return this.m_owner.m_cachedMissionProcessingStartIndex;
        }
        set
        {
          this.m_owner.m_cachedMissionProcessingStartIndex = value;
        }
      }

      public int m_cachedMissionFinishedStartIndex
      {
        get
        {
          return this.m_owner.m_cachedMissionFinishedStartIndex;
        }
        set
        {
          this.m_owner.m_cachedMissionFinishedStartIndex = value;
        }
      }

      public int m_dayCompleteNum
      {
        get
        {
          return this.m_owner.m_dayCompleteNum;
        }
        set
        {
          this.m_owner.m_dayCompleteNum = value;
        }
      }

      public int m_challengeCompleteNum
      {
        get
        {
          return this.m_owner.m_challengeCompleteNum;
        }
        set
        {
          this.m_owner.m_challengeCompleteNum = value;
        }
      }

      public int m_achievementCompleteNum
      {
        get
        {
          return this.m_owner.m_achievementCompleteNum;
        }
        set
        {
          this.m_owner.m_achievementCompleteNum = value;
        }
      }

      public MissionColumnType m_curMissionColumnType
      {
        get
        {
          return this.m_owner.m_curMissionColumnType;
        }
        set
        {
          this.m_owner.m_curMissionColumnType = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public GetRewardGoodsUITask m_getRewardGoodsUITask
      {
        get
        {
          return this.m_owner.m_getRewardGoodsUITask;
        }
        set
        {
          this.m_owner.m_getRewardGoodsUITask = value;
        }
      }

      public int m_oldPlayerLevel
      {
        get
        {
          return this.m_owner.m_oldPlayerLevel;
        }
        set
        {
          this.m_owner.m_oldPlayerLevel = value;
        }
      }

      public int m_missionID
      {
        get
        {
          return this.m_owner.m_missionID;
        }
        set
        {
          this.m_owner.m_missionID = value;
        }
      }

      public static string c_missionListItemPrefabName
      {
        get
        {
          return "MissionListItemUIPrefab";
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void OnPoolObjCreated(string poolName, GameObject go)
      {
        this.m_owner.OnPoolObjCreated(poolName, go);
      }

      public void OnListItemFill(UIControllerBase itemCtrl)
      {
        this.m_owner.OnListItemFill(itemCtrl);
      }

      public void ShowToggleTagNumText(bool day, bool challenge, bool achievement)
      {
        this.m_owner.ShowToggleTagNumText(day, challenge, achievement);
      }

      public int CompareMissionBySortId(Mission m1, Mission m2)
      {
        return this.m_owner.CompareMissionBySortId(m1, m2);
      }

      public void AddMissionColumnRedTagNum(Mission mission)
      {
        this.m_owner.AddMissionColumnRedTagNum(mission);
      }

      public void ResetRedTagNum()
      {
        this.m_owner.ResetRedTagNum();
      }

      public void OnGetRewardButtonClick(int id)
      {
        this.m_owner.OnGetRewardButtonClick(id);
      }

      public void ShowRewards(List<Goods> rewards)
      {
        this.m_owner.ShowRewards(rewards);
      }

      public void GetRewardGoodsUITask_OnClose()
      {
        this.m_owner.GetRewardGoodsUITask_OnClose();
      }

      public void OnGotoLayerButtonClick(GetPathData pathInfo)
      {
        this.m_owner.OnGotoLayerButtonClick(pathInfo);
      }

      public void OnCloseButtonClick()
      {
        this.m_owner.OnCloseButtonClick();
      }

      public void OnTipsBackgroundButtonClick()
      {
        this.m_owner.OnTipsBackgroundButtonClick();
      }

      public void OnTipsConfirmButtonClick()
      {
        this.m_owner.OnTipsConfirmButtonClick();
      }

      public void ResetScrollViewToTop()
      {
        this.m_owner.ResetScrollViewToTop();
      }

      public void OnDayMissionToggleValueChanged(bool on)
      {
        this.m_owner.OnDayMissionToggleValueChanged(on);
      }

      public void OnChallengeToggleValueChanged(bool on)
      {
        this.m_owner.OnChallengeToggleValueChanged(on);
      }

      public void OnAchievementToggleValueChanged(bool on)
      {
        this.m_owner.OnAchievementToggleValueChanged(on);
      }
    }
  }
}
