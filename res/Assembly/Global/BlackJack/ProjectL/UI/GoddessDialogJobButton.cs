﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.GoddessDialogJobButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Scene;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class GoddessDialogJobButton : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_buton;
    [AutoBind("./ArmyIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_armyIconImage;
    [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameTex;
    [AutoBind("./DescText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descTex;
    [AutoBind("./Graphic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_graphicGameObject;
    private ConfigDataJobConnectionInfo m_jobConnectionInfo;
    private UISpineGraphic m_graphic;

    private void OnDisable()
    {
      this.DestroySpineGraphic();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetJobConnectionInfo(ConfigDataJobConnectionInfo jobConnectionInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    public ConfigDataJobConnectionInfo GetJobConnectionInfo()
    {
      return this.m_jobConnectionInfo;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnClick()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateSpineGraphic(
      string assetName,
      float scale,
      Vector2 offset,
      GameObject parent,
      List<ReplaceAnim> replaceAnims)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DestroySpineGraphic()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<GoddessDialogJobButton> EventOnClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }
  }
}
