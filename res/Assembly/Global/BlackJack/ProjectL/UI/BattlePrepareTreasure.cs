﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BattlePrepareTreasure
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Scene;
using SLua;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class BattlePrepareTreasure
  {
    private ClientBattle m_clientBattle;
    private ConfigDataBattleTreasureInfo m_battleTreasureInfo;
    private GenericGraphic m_graphic;
    private GridPosition m_position;
    private bool m_isOpened;
    [DoNotToLua]
    private BattlePrepareTreasure.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_InitializeClientBattleConfigDataBattleTreasureInfo_hotfix;
    private LuaFunction m_GetBattleTreasureInfo_hotfix;
    private LuaFunction m_Destroy_hotfix;
    private LuaFunction m_ComputeGraphicPositionVector2Single_hotfix;
    private LuaFunction m_PauseBoolean_hotfix;
    private LuaFunction m_SetOpenedBoolean_hotfix;
    private LuaFunction m_IsOpened_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattlePrepareTreasure()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Initialize(ClientBattle clientBattle, ConfigDataBattleTreasureInfo treasureInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBattleTreasureInfo GetBattleTreasureInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Destroy()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Vector3 ComputeGraphicPosition(Vector2 p, float zoffset)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Pause(bool pause)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetOpened(bool isOpened)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsOpened()
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public BattlePrepareTreasure.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private BattlePrepareTreasure m_owner;

      public LuaExportHelper(BattlePrepareTreasure owner)
      {
        this.m_owner = owner;
      }

      public ClientBattle m_clientBattle
      {
        get
        {
          return this.m_owner.m_clientBattle;
        }
        set
        {
          this.m_owner.m_clientBattle = value;
        }
      }

      public ConfigDataBattleTreasureInfo m_battleTreasureInfo
      {
        get
        {
          return this.m_owner.m_battleTreasureInfo;
        }
        set
        {
          this.m_owner.m_battleTreasureInfo = value;
        }
      }

      public GenericGraphic m_graphic
      {
        get
        {
          return this.m_owner.m_graphic;
        }
        set
        {
          this.m_owner.m_graphic = value;
        }
      }

      public GridPosition m_position
      {
        get
        {
          return this.m_owner.m_position;
        }
        set
        {
          this.m_owner.m_position = value;
        }
      }

      public bool m_isOpened
      {
        get
        {
          return this.m_owner.m_isOpened;
        }
        set
        {
          this.m_owner.m_isOpened = value;
        }
      }

      public Vector3 ComputeGraphicPosition(Vector2 p, float zoffset)
      {
        return this.m_owner.ComputeGraphicPosition(p, zoffset);
      }
    }
  }
}
