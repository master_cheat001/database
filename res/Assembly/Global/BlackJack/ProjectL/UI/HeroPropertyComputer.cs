﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroPropertyComputer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Battle;
using BlackJack.ProjectL.Common;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class HeroPropertyComputer
  {
    private IConfigDataLoader m_configDataLoader;
    private BattleProperty m_property0;
    private BattleProperty m_property1;
    private BattleProperty m_property2;
    private BattlePropertyModifier m_propertyModifier;
    [DoNotToLua]
    private HeroPropertyComputer.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_InitIConfigDataLoader_hotfix;
    private LuaFunction m_ComputeHeroPropertiesHeroInt32Int32Int32Int32Int32Int32Int32_hotfix;
    private LuaFunction m_ComputeHeroJobLevelUpPropertiesHeroInt32_hotfix;
    private LuaFunction m_ComputeHeroEquipmentPropertiesHero_hotfix;
    private LuaFunction m_ComputeSoldierCommandPropertiesHeroConfigDataSoldierInfo_hotfix;
    private LuaFunction m_ComputeSoldierCommandPropertiesBattleHeroConfigDataSoldierInfoList`1ConfigDataModelSkinResourceInfo_hotfix;
    private LuaFunction m_ComputeHeroBuffPropertiesBattleActor_hotfix;
    private LuaFunction m_ComputeSoldierBuffCommandPropertiesBattleActor_hotfix;
    private LuaFunction m_CollectJobMasterPropertyModifierHeroInt32Int32_hotfix;
    private LuaFunction m_CollectFetterPropertyModifierDictionary`2_hotfix;
    private LuaFunction m_CollectHeartFetterPropertyModifierInt32Int32_hotfix;
    private LuaFunction m_CollectEquipmentPropertyModifierHero_hotfix;
    private LuaFunction m_CollectSoldierPassiveSkillPropertyModifierConfigDataSoldierInfoList`1_hotfix;
    private LuaFunction m_GetEquipmentResonanceSkillInfosUInt64be_hotfix;
    private LuaFunction m_CollectSkinPropertyModifierConfigDataModelSkinResourceInfo_hotfix;
    private LuaFunction m_CollectBuffPropertyModifierBattleActorBoolean_hotfix;
    private LuaFunction m_CollectBuffPropertyExchangeBattleActorBattleProperty_hotfix;
    private LuaFunction m_CollectJobMasterPropertyModifierBattleActor_hotfix;
    private LuaFunction m_CollectEquipmentPropertyModifierBattleActor_hotfix;
    private LuaFunction m_ComputeHeroPropertiesBattleHero_hotfix;
    private LuaFunction m_ComputeHeroEquipmentPropertiesBattleHero_hotfix;
    private LuaFunction m_CollectJobMasterPropertyModifierBattleHero_hotfix;
    private LuaFunction m_CollectEquipmentPropertyModifierBattleHero_hotfix;
    private LuaFunction m_CollectJobPropertyModifierConfigDataJobInfo_hotfix;
    private LuaFunction m_CollectPassiveSkillStaticPropertyModifierConfigDataSkillInfo_hotfix;
    private LuaFunction m_CollectStaticPropertyModifierPropertyModifyTypeInt32_hotfix;
    private LuaFunction m_get_Property0_hotfix;
    private LuaFunction m_get_Property1_hotfix;
    private LuaFunction m_get_Property2_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroPropertyComputer()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init(IConfigDataLoader configDataLoader)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ComputeHeroProperties(
      Hero hero,
      int jobConnectionId,
      int heroLevel0 = -1,
      int heroLevel1 = -1,
      int starLevel0 = -1,
      int starLevel1 = -1,
      int jobLevel0 = -1,
      int jobLevel1 = -1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ComputeHeroJobLevelUpProperties(Hero hero, int jobConnectionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ComputeHeroEquipmentProperties(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ComputeSoldierCommandProperties(Hero hero, ConfigDataSoldierInfo soldierInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ComputeSoldierCommandProperties(
      BattleHero hero,
      ConfigDataSoldierInfo soldierInfo,
      List<TrainingTech> techs,
      ConfigDataModelSkinResourceInfo soldierSkinResInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ComputeHeroBuffProperties(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ComputeSoldierBuffCommandProperties(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectJobMasterPropertyModifier(Hero hero, int jobConnectionId, int jobLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectFetterPropertyModifier(Dictionary<int, int> fetters)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectHeartFetterPropertyModifier(int heroId, int heartFetterLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectEquipmentPropertyModifier(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectSoldierPassiveSkillPropertyModifier(
      ConfigDataSoldierInfo soldierInfo,
      List<TrainingTech> techs)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ConfigDataSkillInfo> GetEquipmentResonanceSkillInfos(
      ulong[] equipmentIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectSkinPropertyModifier(ConfigDataModelSkinResourceInfo skinResInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectBuffPropertyModifier(BattleActor a, bool isDynamic)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectBuffPropertyExchange(BattleActor a, BattleProperty property)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectJobMasterPropertyModifier(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectEquipmentPropertyModifier(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ComputeHeroProperties(BattleHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ComputeHeroEquipmentProperties(BattleHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectJobMasterPropertyModifier(BattleHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectEquipmentPropertyModifier(BattleHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectJobPropertyModifier(ConfigDataJobInfo jobInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectPassiveSkillStaticPropertyModifier(ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectStaticPropertyModifier(PropertyModifyType modifyType, int value)
    {
      // ISSUE: unable to decompile the method.
    }

    public BattleProperty Property0
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public BattleProperty Property1
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public BattleProperty Property2
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public HeroPropertyComputer.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private HeroPropertyComputer m_owner;

      public LuaExportHelper(HeroPropertyComputer owner)
      {
        this.m_owner = owner;
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public BattleProperty m_property0
      {
        get
        {
          return this.m_owner.m_property0;
        }
        set
        {
          this.m_owner.m_property0 = value;
        }
      }

      public BattleProperty m_property1
      {
        get
        {
          return this.m_owner.m_property1;
        }
        set
        {
          this.m_owner.m_property1 = value;
        }
      }

      public BattleProperty m_property2
      {
        get
        {
          return this.m_owner.m_property2;
        }
        set
        {
          this.m_owner.m_property2 = value;
        }
      }

      public BattlePropertyModifier m_propertyModifier
      {
        get
        {
          return this.m_owner.m_propertyModifier;
        }
        set
        {
          this.m_owner.m_propertyModifier = value;
        }
      }

      public void CollectJobMasterPropertyModifier(Hero hero, int jobConnectionId, int jobLevel)
      {
        this.m_owner.CollectJobMasterPropertyModifier(hero, jobConnectionId, jobLevel);
      }

      public void CollectFetterPropertyModifier(Dictionary<int, int> fetters)
      {
        this.m_owner.CollectFetterPropertyModifier(fetters);
      }

      public void CollectHeartFetterPropertyModifier(int heroId, int heartFetterLevel)
      {
        this.m_owner.CollectHeartFetterPropertyModifier(heroId, heartFetterLevel);
      }

      public void CollectEquipmentPropertyModifier(Hero hero)
      {
        this.m_owner.CollectEquipmentPropertyModifier(hero);
      }

      public void CollectSoldierPassiveSkillPropertyModifier(
        ConfigDataSoldierInfo soldierInfo,
        List<TrainingTech> techs)
      {
        this.m_owner.CollectSoldierPassiveSkillPropertyModifier(soldierInfo, techs);
      }

      public void CollectSkinPropertyModifier(ConfigDataModelSkinResourceInfo skinResInfo)
      {
        this.m_owner.CollectSkinPropertyModifier(skinResInfo);
      }

      public void CollectBuffPropertyModifier(BattleActor a, bool isDynamic)
      {
        this.m_owner.CollectBuffPropertyModifier(a, isDynamic);
      }

      public void CollectBuffPropertyExchange(BattleActor a, BattleProperty property)
      {
        this.m_owner.CollectBuffPropertyExchange(a, property);
      }

      public void CollectJobMasterPropertyModifier(BattleActor a)
      {
        this.m_owner.CollectJobMasterPropertyModifier(a);
      }

      public void CollectEquipmentPropertyModifier(BattleActor a)
      {
        this.m_owner.CollectEquipmentPropertyModifier(a);
      }

      public void CollectJobMasterPropertyModifier(BattleHero hero)
      {
        this.m_owner.CollectJobMasterPropertyModifier(hero);
      }

      public void CollectEquipmentPropertyModifier(BattleHero hero)
      {
        this.m_owner.CollectEquipmentPropertyModifier(hero);
      }

      public void CollectJobPropertyModifier(ConfigDataJobInfo jobInfo)
      {
        this.m_owner.CollectJobPropertyModifier(jobInfo);
      }

      public void CollectPassiveSkillStaticPropertyModifier(ConfigDataSkillInfo skillInfo)
      {
        this.m_owner.CollectPassiveSkillStaticPropertyModifier(skillInfo);
      }

      public void CollectStaticPropertyModifier(PropertyModifyType modifyType, int value)
      {
        this.m_owner.CollectStaticPropertyModifier(modifyType, value);
      }
    }
  }
}
