﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ScreenRecorderUI
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using SLua;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class ScreenRecorderUI : PrefabControllerBase, IBeginDragHandler, IDragHandler, IEndDragHandler, IEventSystemHandler
  {
    private Toggle m_screenRecordToggle;
    private GameObject m_recordIndicator;
    private Text m_recordTimerText;
    private float m_recordTime;
    private bool m_isPreviewing;

    private void OnDisable()
    {
      this.ToggleScreenRecord(false);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Awake()
    {
      // ISSUE: unable to decompile the method.
    }

    public void OnScreenRecordToggleValueChanged(bool isOn)
    {
      this.ToggleScreenRecord(isOn);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ToggleScreenRecord(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string FormatRecordTimeString(float time)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
      this.SetDraggedPosition(eventData);
    }

    public void OnDrag(PointerEventData eventData)
    {
      this.SetDraggedPosition(eventData);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
      this.SetDraggedPosition(eventData);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetDraggedPosition(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    private void StartRecord()
    {
    }

    private void StopRecord()
    {
    }

    private void UpdateScreenRecord()
    {
    }
  }
}
