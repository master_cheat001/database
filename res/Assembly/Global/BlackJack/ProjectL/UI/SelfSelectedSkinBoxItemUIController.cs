﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.SelfSelectedSkinBoxItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class SelfSelectedSkinBoxItemUIController : UIControllerBase
  {
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_itemAnimation;
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_itemToggle;
    [AutoBind("./SelectButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_selectButton;
    [AutoBind("./CancelButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_cancelButton;
    [AutoBind("./IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_iconImage;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    public ConfigDataHeroSkinInfo m_heroSkinInfo;
    private bool m_isSelect;
    private bool m_hasOwnHeroSkin;
    private SelfSelectedSkinBoxUITask m_selfSelectedSkinBoxUITask;
    [DoNotToLua]
    private SelfSelectedSkinBoxItemUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_InitSelfSelectedSkinBoxUITaskInt32_hotfix;
    private LuaFunction m_SetViewMode_hotfix;
    private LuaFunction m_ToggleSelect_hotfix;
    private LuaFunction m_HasSkinOwn_hotfix;
    private LuaFunction m_HasSkinSelect_hotfix;
    private LuaFunction m_SetItemNoneState_hotfix;
    private LuaFunction m_SetItemSelectState_hotfix;
    private LuaFunction m_OnSelectClick_hotfix;
    private LuaFunction m_OnCancelClick_hotfix;
    private LuaFunction m_OnItemToggleSelect_hotfix;
    private LuaFunction m_add_EventOnSkinItemStateChangeAction_hotfix;
    private LuaFunction m_remove_EventOnSkinItemStateChangeAction_hotfix;
    private LuaFunction m_add_EventOnItemToggleSelectAction`1_hotfix;
    private LuaFunction m_remove_EventOnItemToggleSelectAction`1_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init(
      SelfSelectedSkinBoxUITask selfSelectedSkinBoxUITask,
      int heroSkinInfoID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetViewMode()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ToggleSelect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasSkinOwn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasSkinSelect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetItemNoneState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetItemSelectState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSelectClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCancelClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnItemToggleSelect()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnSkinItemStateChange
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnItemToggleSelect
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public SelfSelectedSkinBoxItemUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnSkinItemStateChange()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnSkinItemStateChange()
    {
      this.EventOnSkinItemStateChange = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnItemToggleSelect(int obj)
    {
    }

    private void __clearDele_EventOnItemToggleSelect(int obj)
    {
      this.EventOnItemToggleSelect = (Action<int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private SelfSelectedSkinBoxItemUIController m_owner;

      public LuaExportHelper(SelfSelectedSkinBoxItemUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnSkinItemStateChange()
      {
        this.m_owner.__callDele_EventOnSkinItemStateChange();
      }

      public void __clearDele_EventOnSkinItemStateChange()
      {
        this.m_owner.__clearDele_EventOnSkinItemStateChange();
      }

      public void __callDele_EventOnItemToggleSelect(int obj)
      {
        this.m_owner.__callDele_EventOnItemToggleSelect(obj);
      }

      public void __clearDele_EventOnItemToggleSelect(int obj)
      {
        this.m_owner.__clearDele_EventOnItemToggleSelect(obj);
      }

      public CommonUIStateController m_itemAnimation
      {
        get
        {
          return this.m_owner.m_itemAnimation;
        }
        set
        {
          this.m_owner.m_itemAnimation = value;
        }
      }

      public Toggle m_itemToggle
      {
        get
        {
          return this.m_owner.m_itemToggle;
        }
        set
        {
          this.m_owner.m_itemToggle = value;
        }
      }

      public Button m_selectButton
      {
        get
        {
          return this.m_owner.m_selectButton;
        }
        set
        {
          this.m_owner.m_selectButton = value;
        }
      }

      public Button m_cancelButton
      {
        get
        {
          return this.m_owner.m_cancelButton;
        }
        set
        {
          this.m_owner.m_cancelButton = value;
        }
      }

      public Image m_iconImage
      {
        get
        {
          return this.m_owner.m_iconImage;
        }
        set
        {
          this.m_owner.m_iconImage = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public bool m_isSelect
      {
        get
        {
          return this.m_owner.m_isSelect;
        }
        set
        {
          this.m_owner.m_isSelect = value;
        }
      }

      public bool m_hasOwnHeroSkin
      {
        get
        {
          return this.m_owner.m_hasOwnHeroSkin;
        }
        set
        {
          this.m_owner.m_hasOwnHeroSkin = value;
        }
      }

      public SelfSelectedSkinBoxUITask m_selfSelectedSkinBoxUITask
      {
        get
        {
          return this.m_owner.m_selfSelectedSkinBoxUITask;
        }
        set
        {
          this.m_owner.m_selfSelectedSkinBoxUITask = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void OnSelectClick()
      {
        this.m_owner.OnSelectClick();
      }

      public void OnCancelClick()
      {
        this.m_owner.OnCancelClick();
      }

      public void OnItemToggleSelect()
      {
        this.m_owner.OnItemToggleSelect();
      }
    }
  }
}
