﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ActivityNoticeUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Misc;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class ActivityNoticeUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./LayoutRoot/LampGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_lampGroupObj;
    [AutoBind("./LayoutRoot/TopImageGroup/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_bigActiveScrollViewContentObject;
    [AutoBind("./LayoutRoot/List/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_smallActiveScrollViewContentObject;
    [AutoBind("./Prefabs/Lamp", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_lampPrefabObj;
    [AutoBind("./Prefabs/BigActive", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_bigActivePrefabObj;
    [AutoBind("./Prefabs/SmallActive", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_smallActivePrefabObj;
    [AutoBind("./LayoutRoot/Closebutton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_closeButton;
    [AutoBind("./LayoutRoot/TopImageGroup", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_bigItemScrollRect;
    [AutoBind("./BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_bgCloseButton;
    private GameObjectPool<ActivityNoticeBigItemUIController> m_bigItemPool;
    private GameObjectPool<ActivityNoticeSmallItemUIController> m_smallItemPool;
    private GameObjectPool m_lampItemPool;
    private List<CommonUIStateController> m_lampUIStateCtrlList;
    private ScrollSnapCenter m_bigItemScrollSnapCenter;
    private int m_curBigItemIndex;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SignOpenTween()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateActivityList(List<ActivityNoticeInfo> activityList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RefreshLampActive()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LateUpdate()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnActivityItemClick(int activityId)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseButtonClick()
    {
    }

    public event Action<int> EventOnActivityClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    public event Action EventOnCloseButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }
  }
}
