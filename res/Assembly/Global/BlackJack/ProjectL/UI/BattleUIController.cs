﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BattleUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Battle;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Misc;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.Scene;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class BattleUIController : UIControllerBase
  {
    [AutoBind("./Margin", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("./Margin/AutoOffButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_autoOffButton;
    [AutoBind("./Margin/ArenaAutoButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_arenaAutoButton;
    [AutoBind("./TopLeft", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_topLeftGameObject;
    [AutoBind("./TopLeft/SimpleInfoButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_actorSimpleInfoButton;
    [AutoBind("./TopLeft/SimpleInfoButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_actorSimpleInfoUIStateController;
    [AutoBind("./TopLeft/SimpleInfoButton/Head", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_actorSimpleInfoHeadImage;
    [AutoBind("./TopLeft/SimpleInfoButton/Name/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_actorSimpleInfoNameText;
    [AutoBind("./TopLeft/SimpleInfoButton/Level/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_actorSimpleInfoLevelText;
    [AutoBind("./TopLeft/SimpleInfoButton/Army/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_actorSimpleInfoArmyImage;
    [AutoBind("./TopLeft/SimpleInfoButton/HeroHP/Bar", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_actorSimpleInfoHeroHPImage;
    [AutoBind("./TopLeft/SimpleInfoButton/HeroHP/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_actorSimpleInfoHeroHPText;
    [AutoBind("./TopLeft/SimpleInfoButton/SoldierHP/Bar", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_actorSimpleInfoSoldierHPImage;
    [AutoBind("./TopLeft/SimpleInfoButton/SoldierHP/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_actorSimpleInfoSoldierHPText;
    [AutoBind("./Margin/TopLeft", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_topLeftGameObject2;
    [AutoBind("./Margin/TopLeft/RegretButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_regratButton;
    [AutoBind("./Margin/TopLeft/RegretButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_regretButtonUIStateController;
    [AutoBind("./Margin/TopLeft/RegretButton/Count/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_regretCountText;
    [AutoBind("./Margin/TopRight", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_topRightGameObject2;
    [AutoBind("./Margin/TopRight/PauseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_pauseButton;
    [AutoBind("./Margin/TopRight/AutoButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_autoButton;
    [AutoBind("./Margin/TopRight/AutoButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_autoButtonUIStateController;
    [AutoBind("./Margin/TopRight/FastButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_fastButton;
    [AutoBind("./Margin/TopRight/FastButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_fastButtonUIStateController;
    [AutoBind("./Margin/TopRight/SkipButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_skipCombatButton;
    [AutoBind("./Margin/TopRight/SkipButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_skipCombatButtonUIStateController;
    [AutoBind("./Margin/TopRight/TerrainInfo", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_terrainInfoGameObject;
    [AutoBind("./Margin/TopRight/TerrainInfo", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_terrainInfoButton;
    [AutoBind("./Margin/TopRight/TerrainInfo/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_terrainInfoText;
    [AutoBind("./Margin/TopRight/TerrainInfo/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_terrainInfoImage;
    [AutoBind("./Margin/TopRight/TerrainInfo/Def/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_terrainInfoDefText;
    [AutoBind("./Margin/Bottom", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_bottomGameObject;
    [AutoBind("./Margin/Bottom/DangerOnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_dangerOnButton;
    [AutoBind("./Margin/Bottom/DangerOffButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_dangerOffButton;
    [AutoBind("./Margin/Bottom/EndAllActionButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_endAllActionButton;
    [AutoBind("./Margin/Bottom/UseSkillButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_useSkillButton;
    [AutoBind("./Margin/Bottom/CancelSkillButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_cancelSkillButton;
    [AutoBind("./Margin/Bottom/ChatButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_chatButton;
    [AutoBind("./Margin/Bottom/ChatButton/CountPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_chatRedPoint;
    [AutoBind("./Margin/Bottom/Skills", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_skillsUIStateController;
    [AutoBind("./Margin/Bottom/Skills/EndActionButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_endActionButton;
    [AutoBind("./Margin/Bottom/Skills/List", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillListGameObject;
    [AutoBind("./Margin/Bottom/Skills/List", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_skillListUIStateController;
    [AutoBind("./Margin/Bottom/SkillDesc", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillDescGameObject;
    [AutoBind("./Margin/Bottom/SkillHint", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_skillHintUIStateController;
    [AutoBind("./Margin/Bottom/SkillHint/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_skillHintText;
    [AutoBind("./Margin/Bottom/Status", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_statusUIStateController;
    [AutoBind("./Margin/Bottom/Status/TutnValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_statusTurnText;
    [AutoBind("./Margin/Bottom/Status/ConditionGroup/Condition1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_statusWinDescGameObject1;
    [AutoBind("./Margin/Bottom/Status/ConditionGroup/Condition1/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_statusWinDescText1;
    [AutoBind("./Margin/Bottom/Status/ConditionGroup/Condition2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_statusWinDescGameObject2;
    [AutoBind("./Margin/Bottom/Status/ConditionGroup/Condition2/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_statusWinDescText2;
    [AutoBind("./Margin/Bottom/Danmaku", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_danmakuStateCtrl;
    [AutoBind("./Margin/Bottom/Danmaku", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_danmakuInputBackButton;
    [AutoBind("./Margin/Bottom/DanmakuToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_danmakuToggle;
    [AutoBind("./Margin/Bottom/DanmakuToggle/InputWordButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_danmakuInputWordButton;
    [AutoBind("./Margin/Bottom/Danmaku/Input/InputField", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_danmakuInputField;
    [AutoBind("./Margin/Bottom/Danmaku/Input/SendButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_danmakuSendButton;
    [AutoBind("./RegretInOrOut", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_regretUIStateController;
    [AutoBind("./Margin/RegretPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_regretPanelGameObject;
    [AutoBind("./Margin/RegretPanel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_regratConfirmButton;
    [AutoBind("./Margin/RegretPanel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_regretConfirmUIStateController;
    [AutoBind("./Margin/RegretPanel/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_regretCancelButton;
    [AutoBind("./Margin/RegretPanel/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_regretCancelUIStateController;
    [AutoBind("./Margin/RegretPanel/BackwardButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_regretBackwardButton;
    [AutoBind("./Margin/RegretPanel/BackwardButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_regretBackwardUIStateController;
    [AutoBind("./Margin/RegretPanel/ForwardButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_regretForwardButton;
    [AutoBind("./Margin/RegretPanel/ForwardButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_regretForwardUIStateController;
    [AutoBind("./Margin/RegretPanel/RoundDetail/PreTurnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_regretPrevTurnButton;
    [AutoBind("./Margin/RegretPanel/RoundDetail/PreTurnButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_regretPrevTurnUIStateController;
    [AutoBind("./Margin/RegretPanel/RoundDetail/NextTurnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_regretNextTurnButton;
    [AutoBind("./Margin/RegretPanel/RoundDetail/NextTurnButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_regretNextTurnUIStateController;
    [AutoBind("./Margin/RegretPanel/RoundDetail/Info/RoundText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_regretTurnText;
    [AutoBind("./Margin/RegretPanel/RoundDetail/Info/ActionText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_regretActionText;
    [AutoBind("./Margin/RegretPanel/RoundDetail/Info/ActionCountGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_regretTurnUIStateController;
    [AutoBind("./Margin/RegretPanel/RoundDetail/Info/ActionCountGroup/MyCountText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_regretMyCountText;
    [AutoBind("./Margin/RegretPanel/RoundDetail/Info/ActionCountGroup/EnemyCountText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_regretEnemyCountText;
    [AutoBind("./Margin/RegretPanel/EffectImage", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_regretEffectUIStateController;
    [AutoBind("./Margin/FXRegret", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_regretFxUIStateController;
    [AutoBind("./CutsceneSkill", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_cutsceneSkillUIStateController;
    [AutoBind("./CutsceneSkill/Icon/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_cutsceneSkillIconImage;
    [AutoBind("./CutsceneSkill/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_cutsceneSkillNameText;
    [AutoBind("./Objective", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_objectiveGameObject;
    [AutoBind("./Objective/BGImage/WinDescGroup/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_objectiveWinDescText;
    [AutoBind("./Objective/BGImage/LoseDescText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_objectiveLoseDescText;
    [AutoBind("./Win", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_winGameObject;
    [AutoBind("./Lose", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_loseGameObject;
    [AutoBind("./TurnStart", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_turnStartGameObject;
    [AutoBind("./TurnStart/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_turnStartText;
    [AutoBind("./PlayerTurn", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_playerTurnGameObject;
    [AutoBind("./EnemyTurn", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_enemyTurnGameObject;
    [AutoBind("./MyAction", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_myActionUIStateController;
    [AutoBind("./MyAction/DetailPanel/PlayerIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_myActionHeadImage;
    [AutoBind("./MyAction/DetailPanel/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_myActionNameText;
    [AutoBind("./MyAction/DetailPanel/PlayerTag", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_myActionPlayerTagImage;
    [AutoBind("./TeammateAction", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_teammateActionUIStateController;
    [AutoBind("./TeammateAction/DetailPanel/PlayerIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_teammateActionHeadImage;
    [AutoBind("./TeammateAction/DetailPanel/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_teammateActionNameText;
    [AutoBind("./TeammateAction/DetailPanel/PlayerTag", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_teammateActionPlayerTagImage;
    [AutoBind("./EnemyAction", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_enemyActionUIStateController;
    [AutoBind("./EnemyAction/DetailPanel/PlayerIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_enemyActionHeadImage;
    [AutoBind("./EnemyAction/DetailPanel/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enemyActionNameText;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefabs/BattleSkillButton", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_battleSkillButtonPrefab;
    private CommonUIStateController m_armyRelationButtonUIStateController;
    private CommonUIStateController m_armyRelationDescUIStateController;
    private Vector2 m_pointerDownPosition;
    private GameObjectPool<BattleSkillButton> m_battleSkillButtonPool;
    private float m_chatRedPointLastTime;
    private float m_hideSkillHintTime;
    private bool m_isShowSkillHint;
    private bool m_isShowRegretPanel;
    private int m_developerClickCount;
    private int m_turn;
    private IConfigDataLoader m_configDataLoader;
    private ProjectLPlayerContext m_playerContext;
    [DoNotToLua]
    private BattleUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_Update_hotfix;
    private LuaFunction m_StartBattle_hotfix;
    private LuaFunction m_StopBattleBoolean_hotfix;
    private LuaFunction m_WinOrLoseTweenFinished_hotfix;
    private LuaFunction m_SetWinLoseConditionsStringString_hotfix;
    private LuaFunction m_ShowTurnStatusBoolean_hotfix;
    private LuaFunction m_ShowObjecive_hotfix;
    private LuaFunction m_ShowTurnStartInt32Int32_hotfix;
    private LuaFunction m_ShowPlayerTurn_hotfix;
    private LuaFunction m_ShowEnemyTurn_hotfix;
    private LuaFunction m_ShowMyActionBattleRoomPlayerInt32Action_hotfix;
    private LuaFunction m_ShowTeammateActionBattleRoomPlayerInt32Action_hotfix;
    private LuaFunction m_ShowEnemyActionBattleRoomPlayer_hotfix;
    private LuaFunction m_HidePauseButton_hotfix;
    private LuaFunction m_ShowChatButtonBoolean_hotfix;
    private LuaFunction m_SetAutoBattleBoolean_hotfix;
    private LuaFunction m_HideAutoBattleButton_hotfix;
    private LuaFunction m_SetArenaAutoBattleBoolean_hotfix;
    private LuaFunction m_SetFastBattleBoolean_hotfix;
    private LuaFunction m_SetSkipCombatModeSkipCombatMode_hotfix;
    private LuaFunction m_SetActorSimpleInfoBattleActor_hotfix;
    private LuaFunction m_ShowActorSimpleInfoBoolean_hotfix;
    private LuaFunction m_IsShowActorSimpleInfo_hotfix;
    private LuaFunction m_SetShowDangerBoolean_hotfix;
    private LuaFunction m_ShowEndActionBoolean_hotfix;
    private LuaFunction m_ShowEndAllActionBoolean_hotfix;
    private LuaFunction m_SetActionOrderTypeActionOrderType_hotfix;
    private LuaFunction m_RefreshChatRedState_hotfix;
    private LuaFunction m_ShowTopUIBoolean_hotfix;
    private LuaFunction m_IsShowTopUI_hotfix;
    private LuaFunction m_ShowBottomUIBoolean_hotfix;
    private LuaFunction m_IsShowBottomUI_hotfix;
    private LuaFunction m_ShowSkillsList`1BooleanBoolean_hotfix;
    private LuaFunction m_GetSkillButtonRectTransformInt32_hotfix;
    private LuaFunction m_HideSkills_hotfix;
    private LuaFunction m_ShowUseOrCancelSkillBoolean_hotfix;
    private LuaFunction m_ShowUseSkillBoolean_hotfix;
    private LuaFunction m_SetCurrentSkillBattleSkillState_hotfix;
    private LuaFunction m_ShowSkillHintStringTableIdSingle_hotfix;
    private LuaFunction m_HideSkillHint_hotfix;
    private LuaFunction m_HighlightSkillHint_hotfix;
    private LuaFunction m_ShowTerrainInfoConfigDataTerrainInfo_hotfix;
    private LuaFunction m_HideTerrainInfo_hotfix;
    private LuaFunction m_ShowArmyRelationButton_hotfix;
    private LuaFunction m_HideArmyRelation_hotfix;
    private LuaFunction m_ShowArmyRelationDesc_hotfix;
    private LuaFunction m_HideArmyRelationDesc_hotfix;
    private LuaFunction m_IsArmyRelationDescVisible_hotfix;
    private LuaFunction m_ShowCutsceneSkillConfigDataSkillInfo_hotfix;
    private LuaFunction m_HideCutsceneSkill_hotfix;
    private LuaFunction m_OnPauseButtonClick_hotfix;
    private LuaFunction m_OnArmyRelationButtonClick_hotfix;
    private LuaFunction m_OnAutoButtonClick_hotfix;
    private LuaFunction m_OnAutoOffButtonClick_hotfix;
    private LuaFunction m_OnArenaAutoButtonClick_hotfix;
    private LuaFunction m_OnFastButtonClick_hotfix;
    private LuaFunction m_OnSkipCombatButtonClick_hotfix;
    private LuaFunction m_OnDangerOnButtonClick_hotfix;
    private LuaFunction m_OnDangerOffButtonClick_hotfix;
    private LuaFunction m_OnEndActionButtonClick_hotfix;
    private LuaFunction m_OnEndAllActionButtonClick_hotfix;
    private LuaFunction m_OnActorSimpleInfoButtonClick_hotfix;
    private LuaFunction m_OnUseSkillButtonClick_hotfix;
    private LuaFunction m_OnCancelSkillButtonClick_hotfix;
    private LuaFunction m_BattleSkillButton_OnClickBattleSkillButton_hotfix;
    private LuaFunction m_OnChatButtonClick_hotfix;
    private LuaFunction m_OnHelpButtonClick_hotfix;
    private LuaFunction m_OnTerrainInfoButtonClick_hotfix;
    private LuaFunction m_DeveloperModeClick_hotfix;
    private LuaFunction m_ShowDanmakuToggleBoolean_hotfix;
    private LuaFunction m_ShowCurTurnDanmakuInt32_hotfix;
    private LuaFunction m_OnDanmakuToggleValueChangedBoolean_hotfix;
    private LuaFunction m_OnDanmakuInputWordButtonClick_hotfix;
    private LuaFunction m_OnDanmakuInputBackButtonClick_hotfix;
    private LuaFunction m_OnDanmakuSendButtonClick_hotfix;
    private LuaFunction m_SetRegretCountInt32Int32_hotfix;
    private LuaFunction m_SetRegretTurnStatusInt32Int32Int32Int32_hotfix;
    private LuaFunction m_SetRegretButtonStatusBooleanBooleanBooleanBooleanBooleanBoolean_hotfix;
    private LuaFunction m_ShowRegretButtonBoolean_hotfix;
    private LuaFunction m_ShowRegretPanelBoolean_hotfix;
    private LuaFunction m_ShowRegretConfirmFxAction_hotfix;
    private LuaFunction m_OnRegretButtonClick_hotfix;
    private LuaFunction m_OnRegretConfirmButtonClick_hotfix;
    private LuaFunction m_OnRegretCancelButtonClick_hotfix;
    private LuaFunction m_OnRegretBackwardButtonClick_hotfix;
    private LuaFunction m_OnRegretForwardButtonClick_hotfix;
    private LuaFunction m_OnRegretPrevTurnButtonClick_hotfix;
    private LuaFunction m_OnRegretNextTurnButtonClick_hotfix;
    private LuaFunction m_OnScenePointerDownPointerEventData_hotfix;
    private LuaFunction m_OnScenePointerUpPointerEventData_hotfix;
    private LuaFunction m_OnScenePointerClickPointerEventData_hotfix;
    private LuaFunction m_OnSceneBeginDragPointerEventData_hotfix;
    private LuaFunction m_OnSceneEndDragPointerEventData_hotfix;
    private LuaFunction m_OnSceneDragPointerEventData_hotfix;
    private LuaFunction m_OnScene3DTouchVector2_hotfix;
    private LuaFunction m_add_EventOnPauseBattleAction_hotfix;
    private LuaFunction m_remove_EventOnPauseBattleAction_hotfix;
    private LuaFunction m_add_EventOnShowArmyRelationAction_hotfix;
    private LuaFunction m_remove_EventOnShowArmyRelationAction_hotfix;
    private LuaFunction m_add_EventOnAutoBattleAction`1_hotfix;
    private LuaFunction m_remove_EventOnAutoBattleAction`1_hotfix;
    private LuaFunction m_add_EventOnFastBattleAction`1_hotfix;
    private LuaFunction m_remove_EventOnFastBattleAction`1_hotfix;
    private LuaFunction m_add_EventOnSkipCombatAction`1_hotfix;
    private LuaFunction m_remove_EventOnSkipCombatAction`1_hotfix;
    private LuaFunction m_add_EventOnShowDangerAction`1_hotfix;
    private LuaFunction m_remove_EventOnShowDangerAction`1_hotfix;
    private LuaFunction m_add_EventOnEndActionAction_hotfix;
    private LuaFunction m_remove_EventOnEndActionAction_hotfix;
    private LuaFunction m_add_EventOnShowActorInfoAction_hotfix;
    private LuaFunction m_remove_EventOnShowActorInfoAction_hotfix;
    private LuaFunction m_add_EventOnEndAllActionAction_hotfix;
    private LuaFunction m_remove_EventOnEndAllActionAction_hotfix;
    private LuaFunction m_add_EventOnUseSkillAction_hotfix;
    private LuaFunction m_remove_EventOnUseSkillAction_hotfix;
    private LuaFunction m_add_EventOnCancelSkillAction_hotfix;
    private LuaFunction m_remove_EventOnCancelSkillAction_hotfix;
    private LuaFunction m_add_EventOnWinOrLoseEndAction_hotfix;
    private LuaFunction m_remove_EventOnWinOrLoseEndAction_hotfix;
    private LuaFunction m_add_EventOnShowChatAction_hotfix;
    private LuaFunction m_remove_EventOnShowChatAction_hotfix;
    private LuaFunction m_add_EventOnShowHelpAction_hotfix;
    private LuaFunction m_remove_EventOnShowHelpAction_hotfix;
    private LuaFunction m_add_EventOnSelectSkillAction`1_hotfix;
    private LuaFunction m_remove_EventOnSelectSkillAction`1_hotfix;
    private LuaFunction m_add_EventOnPointerDownAction`2_hotfix;
    private LuaFunction m_remove_EventOnPointerDownAction`2_hotfix;
    private LuaFunction m_add_EventOnPointerUpAction`2_hotfix;
    private LuaFunction m_remove_EventOnPointerUpAction`2_hotfix;
    private LuaFunction m_add_EventOnPointerClickAction`2_hotfix;
    private LuaFunction m_remove_EventOnPointerClickAction`2_hotfix;
    private LuaFunction m_add_EventOn3DTouchAction`1_hotfix;
    private LuaFunction m_remove_EventOn3DTouchAction`1_hotfix;
    private LuaFunction m_add_EventOnShowCurTurnDanmakuAction`1_hotfix;
    private LuaFunction m_remove_EventOnShowCurTurnDanmakuAction`1_hotfix;
    private LuaFunction m_add_EventOnCloseCurTurnDanmakuAction_hotfix;
    private LuaFunction m_remove_EventOnCloseCurTurnDanmakuAction_hotfix;
    private LuaFunction m_add_EventOnShowOneDanmakuAction`1_hotfix;
    private LuaFunction m_remove_EventOnShowOneDanmakuAction`1_hotfix;
    private LuaFunction m_add_EventOnRegretActiveAction_hotfix;
    private LuaFunction m_remove_EventOnRegretActiveAction_hotfix;
    private LuaFunction m_add_EventOnRegretConfirmAction_hotfix;
    private LuaFunction m_remove_EventOnRegretConfirmAction_hotfix;
    private LuaFunction m_add_EventOnRegretCancelAction_hotfix;
    private LuaFunction m_remove_EventOnRegretCancelAction_hotfix;
    private LuaFunction m_add_EventOnRegretBackwardAction_hotfix;
    private LuaFunction m_remove_EventOnRegretBackwardAction_hotfix;
    private LuaFunction m_add_EventOnRegretForwardAction_hotfix;
    private LuaFunction m_remove_EventOnRegretForwardAction_hotfix;
    private LuaFunction m_add_EventOnRegretPrevTurnAction_hotfix;
    private LuaFunction m_remove_EventOnRegretPrevTurnAction_hotfix;
    private LuaFunction m_add_EventOnRegretNextTurnAction_hotfix;
    private LuaFunction m_remove_EventOnRegretNextTurnAction_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    private BattleUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StopBattle(bool win)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WinOrLoseTweenFinished()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetWinLoseConditions(string winDesc, string loseDesc)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowTurnStatus(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowObjecive()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowTurnStart(int turn, int turnMax)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowPlayerTurn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowEnemyTurn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowMyAction(BattleRoomPlayer player, int playerTagIndex, Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowTeammateAction(BattleRoomPlayer player, int playerIndex, Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowEnemyAction(BattleRoomPlayer player)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HidePauseButton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowChatButton(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAutoBattle(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideAutoBattleButton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetArenaAutoBattle(bool auto)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetFastBattle(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSkipCombatMode(SkipCombatMode mode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetActorSimpleInfo(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowActorSimpleInfo(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsShowActorSimpleInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetShowDanger(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowEndAction(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowEndAllAction(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetActionOrderType(ActionOrderType actionOrderType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RefreshChatRedState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void ComputeActionIconPositionScale(int index, out Vector2 pos, out float scale)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowTopUI(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsShowTopUI()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBottomUI(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsShowBottomUI()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowSkills(
      List<BattleSkillState> skills,
      bool isActiveSkillBanned,
      bool isPassiveSkillBanned)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RectTransform GetSkillButtonRectTransform(int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideSkills()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowUseOrCancelSkill(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowUseSkill(bool enable)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCurrentSkill(BattleSkillState ss)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowSkillHint(StringTableId id, float hideTime = 0.0f)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideSkillHint()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HighlightSkillHint()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowTerrainInfo(ConfigDataTerrainInfo terrain)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideTerrainInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowArmyRelationButton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HideArmyRelation()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowArmyRelationDesc()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideArmyRelationDesc()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsArmyRelationDescVisible()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowCutsceneSkill(ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HideCutsceneSkill()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPauseButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnArmyRelationButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAutoButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAutoOffButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnArenaAutoButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFastButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSkipCombatButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDangerOnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDangerOffButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEndActionButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEndAllActionButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnActorSimpleInfoButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnUseSkillButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCancelSkillButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleSkillButton_OnClick(BattleSkillButton sb)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChatButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHelpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTerrainInfoButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DeveloperModeClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowDanmakuToggle(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowCurTurnDanmaku(int turn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDanmakuToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDanmakuInputWordButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDanmakuInputBackButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDanmakuSendButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRegretCount(int remainCount, int maxCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRegretTurnStatus(int turn, int actionTeam, int count, int countMax)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRegretButtonStatus(
      bool canBackward,
      bool canForward,
      bool canPrevTurn,
      bool canNextTurn,
      bool canConfirm,
      bool canCancel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowRegretButton(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowRegretPanel(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowRegretConfirmFx(Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRegretButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRegretConfirmButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRegretCancelButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRegretBackwardButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRegretForwardButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRegretPrevTurnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRegretNextTurnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnScenePointerDown(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnScenePointerUp(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnScenePointerClick(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnSceneBeginDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnSceneEndDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnSceneDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnScene3DTouch(Vector2 p)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnPauseBattle
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowArmyRelation
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<bool> EventOnAutoBattle
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<bool> EventOnFastBattle
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<SkipCombatMode> EventOnSkipCombat
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<bool> EventOnShowDanger
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnEndAction
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowActorInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnEndAllAction
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnUseSkill
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnCancelSkill
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnWinOrLoseEnd
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowChat
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowHelp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnSelectSkill
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<PointerEventData.InputButton, Vector2> EventOnPointerDown
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<PointerEventData.InputButton, Vector2> EventOnPointerUp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<PointerEventData.InputButton, Vector2> EventOnPointerClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<Vector2> EventOn3DTouch
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnShowCurTurnDanmaku
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnCloseCurTurnDanmaku
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<DanmakuEntry> EventOnShowOneDanmaku
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnRegretActive
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnRegretConfirm
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnRegretCancel
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnRegretBackward
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnRegretForward
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnRegretPrevTurn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnRegretNextTurn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public BattleUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnPauseBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnPauseBattle()
    {
      this.EventOnPauseBattle = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowArmyRelation()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowArmyRelation()
    {
      this.EventOnShowArmyRelation = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnAutoBattle(bool obj)
    {
    }

    private void __clearDele_EventOnAutoBattle(bool obj)
    {
      this.EventOnAutoBattle = (Action<bool>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnFastBattle(bool obj)
    {
    }

    private void __clearDele_EventOnFastBattle(bool obj)
    {
      this.EventOnFastBattle = (Action<bool>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnSkipCombat(SkipCombatMode obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnSkipCombat(SkipCombatMode obj)
    {
      this.EventOnSkipCombat = (Action<SkipCombatMode>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowDanger(bool obj)
    {
    }

    private void __clearDele_EventOnShowDanger(bool obj)
    {
      this.EventOnShowDanger = (Action<bool>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnEndAction()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnEndAction()
    {
      this.EventOnEndAction = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowActorInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowActorInfo()
    {
      this.EventOnShowActorInfo = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnEndAllAction()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnEndAllAction()
    {
      this.EventOnEndAllAction = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnUseSkill()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnUseSkill()
    {
      this.EventOnUseSkill = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnCancelSkill()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnCancelSkill()
    {
      this.EventOnCancelSkill = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnWinOrLoseEnd()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnWinOrLoseEnd()
    {
      this.EventOnWinOrLoseEnd = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowChat()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowChat()
    {
      this.EventOnShowChat = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowHelp()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowHelp()
    {
      this.EventOnShowHelp = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnSelectSkill(int obj)
    {
    }

    private void __clearDele_EventOnSelectSkill(int obj)
    {
      this.EventOnSelectSkill = (Action<int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnPointerDown(PointerEventData.InputButton arg1, Vector2 arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnPointerDown(PointerEventData.InputButton arg1, Vector2 arg2)
    {
      this.EventOnPointerDown = (Action<PointerEventData.InputButton, Vector2>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnPointerUp(PointerEventData.InputButton arg1, Vector2 arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnPointerUp(PointerEventData.InputButton arg1, Vector2 arg2)
    {
      this.EventOnPointerUp = (Action<PointerEventData.InputButton, Vector2>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnPointerClick(PointerEventData.InputButton arg1, Vector2 arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnPointerClick(PointerEventData.InputButton arg1, Vector2 arg2)
    {
      this.EventOnPointerClick = (Action<PointerEventData.InputButton, Vector2>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOn3DTouch(Vector2 obj)
    {
    }

    private void __clearDele_EventOn3DTouch(Vector2 obj)
    {
      this.EventOn3DTouch = (Action<Vector2>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowCurTurnDanmaku(int obj)
    {
    }

    private void __clearDele_EventOnShowCurTurnDanmaku(int obj)
    {
      this.EventOnShowCurTurnDanmaku = (Action<int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnCloseCurTurnDanmaku()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnCloseCurTurnDanmaku()
    {
      this.EventOnCloseCurTurnDanmaku = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowOneDanmaku(DanmakuEntry obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowOneDanmaku(DanmakuEntry obj)
    {
      this.EventOnShowOneDanmaku = (Action<DanmakuEntry>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnRegretActive()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnRegretActive()
    {
      this.EventOnRegretActive = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnRegretConfirm()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnRegretConfirm()
    {
      this.EventOnRegretConfirm = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnRegretCancel()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnRegretCancel()
    {
      this.EventOnRegretCancel = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnRegretBackward()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnRegretBackward()
    {
      this.EventOnRegretBackward = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnRegretForward()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnRegretForward()
    {
      this.EventOnRegretForward = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnRegretPrevTurn()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnRegretPrevTurn()
    {
      this.EventOnRegretPrevTurn = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnRegretNextTurn()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnRegretNextTurn()
    {
      this.EventOnRegretNextTurn = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private BattleUIController m_owner;

      public LuaExportHelper(BattleUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnPauseBattle()
      {
        this.m_owner.__callDele_EventOnPauseBattle();
      }

      public void __clearDele_EventOnPauseBattle()
      {
        this.m_owner.__clearDele_EventOnPauseBattle();
      }

      public void __callDele_EventOnShowArmyRelation()
      {
        this.m_owner.__callDele_EventOnShowArmyRelation();
      }

      public void __clearDele_EventOnShowArmyRelation()
      {
        this.m_owner.__clearDele_EventOnShowArmyRelation();
      }

      public void __callDele_EventOnAutoBattle(bool obj)
      {
        this.m_owner.__callDele_EventOnAutoBattle(obj);
      }

      public void __clearDele_EventOnAutoBattle(bool obj)
      {
        this.m_owner.__clearDele_EventOnAutoBattle(obj);
      }

      public void __callDele_EventOnFastBattle(bool obj)
      {
        this.m_owner.__callDele_EventOnFastBattle(obj);
      }

      public void __clearDele_EventOnFastBattle(bool obj)
      {
        this.m_owner.__clearDele_EventOnFastBattle(obj);
      }

      public void __callDele_EventOnSkipCombat(SkipCombatMode obj)
      {
        this.m_owner.__callDele_EventOnSkipCombat(obj);
      }

      public void __clearDele_EventOnSkipCombat(SkipCombatMode obj)
      {
        this.m_owner.__clearDele_EventOnSkipCombat(obj);
      }

      public void __callDele_EventOnShowDanger(bool obj)
      {
        this.m_owner.__callDele_EventOnShowDanger(obj);
      }

      public void __clearDele_EventOnShowDanger(bool obj)
      {
        this.m_owner.__clearDele_EventOnShowDanger(obj);
      }

      public void __callDele_EventOnEndAction()
      {
        this.m_owner.__callDele_EventOnEndAction();
      }

      public void __clearDele_EventOnEndAction()
      {
        this.m_owner.__clearDele_EventOnEndAction();
      }

      public void __callDele_EventOnShowActorInfo()
      {
        this.m_owner.__callDele_EventOnShowActorInfo();
      }

      public void __clearDele_EventOnShowActorInfo()
      {
        this.m_owner.__clearDele_EventOnShowActorInfo();
      }

      public void __callDele_EventOnEndAllAction()
      {
        this.m_owner.__callDele_EventOnEndAllAction();
      }

      public void __clearDele_EventOnEndAllAction()
      {
        this.m_owner.__clearDele_EventOnEndAllAction();
      }

      public void __callDele_EventOnUseSkill()
      {
        this.m_owner.__callDele_EventOnUseSkill();
      }

      public void __clearDele_EventOnUseSkill()
      {
        this.m_owner.__clearDele_EventOnUseSkill();
      }

      public void __callDele_EventOnCancelSkill()
      {
        this.m_owner.__callDele_EventOnCancelSkill();
      }

      public void __clearDele_EventOnCancelSkill()
      {
        this.m_owner.__clearDele_EventOnCancelSkill();
      }

      public void __callDele_EventOnWinOrLoseEnd()
      {
        this.m_owner.__callDele_EventOnWinOrLoseEnd();
      }

      public void __clearDele_EventOnWinOrLoseEnd()
      {
        this.m_owner.__clearDele_EventOnWinOrLoseEnd();
      }

      public void __callDele_EventOnShowChat()
      {
        this.m_owner.__callDele_EventOnShowChat();
      }

      public void __clearDele_EventOnShowChat()
      {
        this.m_owner.__clearDele_EventOnShowChat();
      }

      public void __callDele_EventOnShowHelp()
      {
        this.m_owner.__callDele_EventOnShowHelp();
      }

      public void __clearDele_EventOnShowHelp()
      {
        this.m_owner.__clearDele_EventOnShowHelp();
      }

      public void __callDele_EventOnSelectSkill(int obj)
      {
        this.m_owner.__callDele_EventOnSelectSkill(obj);
      }

      public void __clearDele_EventOnSelectSkill(int obj)
      {
        this.m_owner.__clearDele_EventOnSelectSkill(obj);
      }

      public void __callDele_EventOnPointerDown(PointerEventData.InputButton arg1, Vector2 arg2)
      {
        this.m_owner.__callDele_EventOnPointerDown(arg1, arg2);
      }

      public void __clearDele_EventOnPointerDown(PointerEventData.InputButton arg1, Vector2 arg2)
      {
        this.m_owner.__clearDele_EventOnPointerDown(arg1, arg2);
      }

      public void __callDele_EventOnPointerUp(PointerEventData.InputButton arg1, Vector2 arg2)
      {
        this.m_owner.__callDele_EventOnPointerUp(arg1, arg2);
      }

      public void __clearDele_EventOnPointerUp(PointerEventData.InputButton arg1, Vector2 arg2)
      {
        this.m_owner.__clearDele_EventOnPointerUp(arg1, arg2);
      }

      public void __callDele_EventOnPointerClick(PointerEventData.InputButton arg1, Vector2 arg2)
      {
        this.m_owner.__callDele_EventOnPointerClick(arg1, arg2);
      }

      public void __clearDele_EventOnPointerClick(PointerEventData.InputButton arg1, Vector2 arg2)
      {
        this.m_owner.__clearDele_EventOnPointerClick(arg1, arg2);
      }

      public void __callDele_EventOn3DTouch(Vector2 obj)
      {
        this.m_owner.__callDele_EventOn3DTouch(obj);
      }

      public void __clearDele_EventOn3DTouch(Vector2 obj)
      {
        this.m_owner.__clearDele_EventOn3DTouch(obj);
      }

      public void __callDele_EventOnShowCurTurnDanmaku(int obj)
      {
        this.m_owner.__callDele_EventOnShowCurTurnDanmaku(obj);
      }

      public void __clearDele_EventOnShowCurTurnDanmaku(int obj)
      {
        this.m_owner.__clearDele_EventOnShowCurTurnDanmaku(obj);
      }

      public void __callDele_EventOnCloseCurTurnDanmaku()
      {
        this.m_owner.__callDele_EventOnCloseCurTurnDanmaku();
      }

      public void __clearDele_EventOnCloseCurTurnDanmaku()
      {
        this.m_owner.__clearDele_EventOnCloseCurTurnDanmaku();
      }

      public void __callDele_EventOnShowOneDanmaku(DanmakuEntry obj)
      {
        this.m_owner.__callDele_EventOnShowOneDanmaku(obj);
      }

      public void __clearDele_EventOnShowOneDanmaku(DanmakuEntry obj)
      {
        this.m_owner.__clearDele_EventOnShowOneDanmaku(obj);
      }

      public void __callDele_EventOnRegretActive()
      {
        this.m_owner.__callDele_EventOnRegretActive();
      }

      public void __clearDele_EventOnRegretActive()
      {
        this.m_owner.__clearDele_EventOnRegretActive();
      }

      public void __callDele_EventOnRegretConfirm()
      {
        this.m_owner.__callDele_EventOnRegretConfirm();
      }

      public void __clearDele_EventOnRegretConfirm()
      {
        this.m_owner.__clearDele_EventOnRegretConfirm();
      }

      public void __callDele_EventOnRegretCancel()
      {
        this.m_owner.__callDele_EventOnRegretCancel();
      }

      public void __clearDele_EventOnRegretCancel()
      {
        this.m_owner.__clearDele_EventOnRegretCancel();
      }

      public void __callDele_EventOnRegretBackward()
      {
        this.m_owner.__callDele_EventOnRegretBackward();
      }

      public void __clearDele_EventOnRegretBackward()
      {
        this.m_owner.__clearDele_EventOnRegretBackward();
      }

      public void __callDele_EventOnRegretForward()
      {
        this.m_owner.__callDele_EventOnRegretForward();
      }

      public void __clearDele_EventOnRegretForward()
      {
        this.m_owner.__clearDele_EventOnRegretForward();
      }

      public void __callDele_EventOnRegretPrevTurn()
      {
        this.m_owner.__callDele_EventOnRegretPrevTurn();
      }

      public void __clearDele_EventOnRegretPrevTurn()
      {
        this.m_owner.__clearDele_EventOnRegretPrevTurn();
      }

      public void __callDele_EventOnRegretNextTurn()
      {
        this.m_owner.__callDele_EventOnRegretNextTurn();
      }

      public void __clearDele_EventOnRegretNextTurn()
      {
        this.m_owner.__clearDele_EventOnRegretNextTurn();
      }

      public RectTransform m_marginTransform
      {
        get
        {
          return this.m_owner.m_marginTransform;
        }
        set
        {
          this.m_owner.m_marginTransform = value;
        }
      }

      public Button m_autoOffButton
      {
        get
        {
          return this.m_owner.m_autoOffButton;
        }
        set
        {
          this.m_owner.m_autoOffButton = value;
        }
      }

      public Button m_arenaAutoButton
      {
        get
        {
          return this.m_owner.m_arenaAutoButton;
        }
        set
        {
          this.m_owner.m_arenaAutoButton = value;
        }
      }

      public GameObject m_topLeftGameObject
      {
        get
        {
          return this.m_owner.m_topLeftGameObject;
        }
        set
        {
          this.m_owner.m_topLeftGameObject = value;
        }
      }

      public Button m_actorSimpleInfoButton
      {
        get
        {
          return this.m_owner.m_actorSimpleInfoButton;
        }
        set
        {
          this.m_owner.m_actorSimpleInfoButton = value;
        }
      }

      public CommonUIStateController m_actorSimpleInfoUIStateController
      {
        get
        {
          return this.m_owner.m_actorSimpleInfoUIStateController;
        }
        set
        {
          this.m_owner.m_actorSimpleInfoUIStateController = value;
        }
      }

      public Image m_actorSimpleInfoHeadImage
      {
        get
        {
          return this.m_owner.m_actorSimpleInfoHeadImage;
        }
        set
        {
          this.m_owner.m_actorSimpleInfoHeadImage = value;
        }
      }

      public Text m_actorSimpleInfoNameText
      {
        get
        {
          return this.m_owner.m_actorSimpleInfoNameText;
        }
        set
        {
          this.m_owner.m_actorSimpleInfoNameText = value;
        }
      }

      public Text m_actorSimpleInfoLevelText
      {
        get
        {
          return this.m_owner.m_actorSimpleInfoLevelText;
        }
        set
        {
          this.m_owner.m_actorSimpleInfoLevelText = value;
        }
      }

      public Image m_actorSimpleInfoArmyImage
      {
        get
        {
          return this.m_owner.m_actorSimpleInfoArmyImage;
        }
        set
        {
          this.m_owner.m_actorSimpleInfoArmyImage = value;
        }
      }

      public Image m_actorSimpleInfoHeroHPImage
      {
        get
        {
          return this.m_owner.m_actorSimpleInfoHeroHPImage;
        }
        set
        {
          this.m_owner.m_actorSimpleInfoHeroHPImage = value;
        }
      }

      public Text m_actorSimpleInfoHeroHPText
      {
        get
        {
          return this.m_owner.m_actorSimpleInfoHeroHPText;
        }
        set
        {
          this.m_owner.m_actorSimpleInfoHeroHPText = value;
        }
      }

      public Image m_actorSimpleInfoSoldierHPImage
      {
        get
        {
          return this.m_owner.m_actorSimpleInfoSoldierHPImage;
        }
        set
        {
          this.m_owner.m_actorSimpleInfoSoldierHPImage = value;
        }
      }

      public Text m_actorSimpleInfoSoldierHPText
      {
        get
        {
          return this.m_owner.m_actorSimpleInfoSoldierHPText;
        }
        set
        {
          this.m_owner.m_actorSimpleInfoSoldierHPText = value;
        }
      }

      public GameObject m_topLeftGameObject2
      {
        get
        {
          return this.m_owner.m_topLeftGameObject2;
        }
        set
        {
          this.m_owner.m_topLeftGameObject2 = value;
        }
      }

      public Button m_regratButton
      {
        get
        {
          return this.m_owner.m_regratButton;
        }
        set
        {
          this.m_owner.m_regratButton = value;
        }
      }

      public CommonUIStateController m_regretButtonUIStateController
      {
        get
        {
          return this.m_owner.m_regretButtonUIStateController;
        }
        set
        {
          this.m_owner.m_regretButtonUIStateController = value;
        }
      }

      public Text m_regretCountText
      {
        get
        {
          return this.m_owner.m_regretCountText;
        }
        set
        {
          this.m_owner.m_regretCountText = value;
        }
      }

      public GameObject m_topRightGameObject2
      {
        get
        {
          return this.m_owner.m_topRightGameObject2;
        }
        set
        {
          this.m_owner.m_topRightGameObject2 = value;
        }
      }

      public Button m_pauseButton
      {
        get
        {
          return this.m_owner.m_pauseButton;
        }
        set
        {
          this.m_owner.m_pauseButton = value;
        }
      }

      public Button m_autoButton
      {
        get
        {
          return this.m_owner.m_autoButton;
        }
        set
        {
          this.m_owner.m_autoButton = value;
        }
      }

      public CommonUIStateController m_autoButtonUIStateController
      {
        get
        {
          return this.m_owner.m_autoButtonUIStateController;
        }
        set
        {
          this.m_owner.m_autoButtonUIStateController = value;
        }
      }

      public Button m_fastButton
      {
        get
        {
          return this.m_owner.m_fastButton;
        }
        set
        {
          this.m_owner.m_fastButton = value;
        }
      }

      public CommonUIStateController m_fastButtonUIStateController
      {
        get
        {
          return this.m_owner.m_fastButtonUIStateController;
        }
        set
        {
          this.m_owner.m_fastButtonUIStateController = value;
        }
      }

      public Button m_skipCombatButton
      {
        get
        {
          return this.m_owner.m_skipCombatButton;
        }
        set
        {
          this.m_owner.m_skipCombatButton = value;
        }
      }

      public CommonUIStateController m_skipCombatButtonUIStateController
      {
        get
        {
          return this.m_owner.m_skipCombatButtonUIStateController;
        }
        set
        {
          this.m_owner.m_skipCombatButtonUIStateController = value;
        }
      }

      public GameObject m_terrainInfoGameObject
      {
        get
        {
          return this.m_owner.m_terrainInfoGameObject;
        }
        set
        {
          this.m_owner.m_terrainInfoGameObject = value;
        }
      }

      public Button m_terrainInfoButton
      {
        get
        {
          return this.m_owner.m_terrainInfoButton;
        }
        set
        {
          this.m_owner.m_terrainInfoButton = value;
        }
      }

      public Text m_terrainInfoText
      {
        get
        {
          return this.m_owner.m_terrainInfoText;
        }
        set
        {
          this.m_owner.m_terrainInfoText = value;
        }
      }

      public Image m_terrainInfoImage
      {
        get
        {
          return this.m_owner.m_terrainInfoImage;
        }
        set
        {
          this.m_owner.m_terrainInfoImage = value;
        }
      }

      public Text m_terrainInfoDefText
      {
        get
        {
          return this.m_owner.m_terrainInfoDefText;
        }
        set
        {
          this.m_owner.m_terrainInfoDefText = value;
        }
      }

      public GameObject m_bottomGameObject
      {
        get
        {
          return this.m_owner.m_bottomGameObject;
        }
        set
        {
          this.m_owner.m_bottomGameObject = value;
        }
      }

      public Button m_dangerOnButton
      {
        get
        {
          return this.m_owner.m_dangerOnButton;
        }
        set
        {
          this.m_owner.m_dangerOnButton = value;
        }
      }

      public Button m_dangerOffButton
      {
        get
        {
          return this.m_owner.m_dangerOffButton;
        }
        set
        {
          this.m_owner.m_dangerOffButton = value;
        }
      }

      public Button m_endAllActionButton
      {
        get
        {
          return this.m_owner.m_endAllActionButton;
        }
        set
        {
          this.m_owner.m_endAllActionButton = value;
        }
      }

      public Button m_useSkillButton
      {
        get
        {
          return this.m_owner.m_useSkillButton;
        }
        set
        {
          this.m_owner.m_useSkillButton = value;
        }
      }

      public Button m_cancelSkillButton
      {
        get
        {
          return this.m_owner.m_cancelSkillButton;
        }
        set
        {
          this.m_owner.m_cancelSkillButton = value;
        }
      }

      public Button m_chatButton
      {
        get
        {
          return this.m_owner.m_chatButton;
        }
        set
        {
          this.m_owner.m_chatButton = value;
        }
      }

      public GameObject m_chatRedPoint
      {
        get
        {
          return this.m_owner.m_chatRedPoint;
        }
        set
        {
          this.m_owner.m_chatRedPoint = value;
        }
      }

      public CommonUIStateController m_skillsUIStateController
      {
        get
        {
          return this.m_owner.m_skillsUIStateController;
        }
        set
        {
          this.m_owner.m_skillsUIStateController = value;
        }
      }

      public Button m_endActionButton
      {
        get
        {
          return this.m_owner.m_endActionButton;
        }
        set
        {
          this.m_owner.m_endActionButton = value;
        }
      }

      public GameObject m_skillListGameObject
      {
        get
        {
          return this.m_owner.m_skillListGameObject;
        }
        set
        {
          this.m_owner.m_skillListGameObject = value;
        }
      }

      public CommonUIStateController m_skillListUIStateController
      {
        get
        {
          return this.m_owner.m_skillListUIStateController;
        }
        set
        {
          this.m_owner.m_skillListUIStateController = value;
        }
      }

      public GameObject m_skillDescGameObject
      {
        get
        {
          return this.m_owner.m_skillDescGameObject;
        }
        set
        {
          this.m_owner.m_skillDescGameObject = value;
        }
      }

      public CommonUIStateController m_skillHintUIStateController
      {
        get
        {
          return this.m_owner.m_skillHintUIStateController;
        }
        set
        {
          this.m_owner.m_skillHintUIStateController = value;
        }
      }

      public Text m_skillHintText
      {
        get
        {
          return this.m_owner.m_skillHintText;
        }
        set
        {
          this.m_owner.m_skillHintText = value;
        }
      }

      public CommonUIStateController m_statusUIStateController
      {
        get
        {
          return this.m_owner.m_statusUIStateController;
        }
        set
        {
          this.m_owner.m_statusUIStateController = value;
        }
      }

      public Text m_statusTurnText
      {
        get
        {
          return this.m_owner.m_statusTurnText;
        }
        set
        {
          this.m_owner.m_statusTurnText = value;
        }
      }

      public GameObject m_statusWinDescGameObject1
      {
        get
        {
          return this.m_owner.m_statusWinDescGameObject1;
        }
        set
        {
          this.m_owner.m_statusWinDescGameObject1 = value;
        }
      }

      public Text m_statusWinDescText1
      {
        get
        {
          return this.m_owner.m_statusWinDescText1;
        }
        set
        {
          this.m_owner.m_statusWinDescText1 = value;
        }
      }

      public GameObject m_statusWinDescGameObject2
      {
        get
        {
          return this.m_owner.m_statusWinDescGameObject2;
        }
        set
        {
          this.m_owner.m_statusWinDescGameObject2 = value;
        }
      }

      public Text m_statusWinDescText2
      {
        get
        {
          return this.m_owner.m_statusWinDescText2;
        }
        set
        {
          this.m_owner.m_statusWinDescText2 = value;
        }
      }

      public CommonUIStateController m_danmakuStateCtrl
      {
        get
        {
          return this.m_owner.m_danmakuStateCtrl;
        }
        set
        {
          this.m_owner.m_danmakuStateCtrl = value;
        }
      }

      public Button m_danmakuInputBackButton
      {
        get
        {
          return this.m_owner.m_danmakuInputBackButton;
        }
        set
        {
          this.m_owner.m_danmakuInputBackButton = value;
        }
      }

      public Toggle m_danmakuToggle
      {
        get
        {
          return this.m_owner.m_danmakuToggle;
        }
        set
        {
          this.m_owner.m_danmakuToggle = value;
        }
      }

      public Button m_danmakuInputWordButton
      {
        get
        {
          return this.m_owner.m_danmakuInputWordButton;
        }
        set
        {
          this.m_owner.m_danmakuInputWordButton = value;
        }
      }

      public InputField m_danmakuInputField
      {
        get
        {
          return this.m_owner.m_danmakuInputField;
        }
        set
        {
          this.m_owner.m_danmakuInputField = value;
        }
      }

      public Button m_danmakuSendButton
      {
        get
        {
          return this.m_owner.m_danmakuSendButton;
        }
        set
        {
          this.m_owner.m_danmakuSendButton = value;
        }
      }

      public CommonUIStateController m_regretUIStateController
      {
        get
        {
          return this.m_owner.m_regretUIStateController;
        }
        set
        {
          this.m_owner.m_regretUIStateController = value;
        }
      }

      public GameObject m_regretPanelGameObject
      {
        get
        {
          return this.m_owner.m_regretPanelGameObject;
        }
        set
        {
          this.m_owner.m_regretPanelGameObject = value;
        }
      }

      public Button m_regratConfirmButton
      {
        get
        {
          return this.m_owner.m_regratConfirmButton;
        }
        set
        {
          this.m_owner.m_regratConfirmButton = value;
        }
      }

      public CommonUIStateController m_regretConfirmUIStateController
      {
        get
        {
          return this.m_owner.m_regretConfirmUIStateController;
        }
        set
        {
          this.m_owner.m_regretConfirmUIStateController = value;
        }
      }

      public Button m_regretCancelButton
      {
        get
        {
          return this.m_owner.m_regretCancelButton;
        }
        set
        {
          this.m_owner.m_regretCancelButton = value;
        }
      }

      public CommonUIStateController m_regretCancelUIStateController
      {
        get
        {
          return this.m_owner.m_regretCancelUIStateController;
        }
        set
        {
          this.m_owner.m_regretCancelUIStateController = value;
        }
      }

      public Button m_regretBackwardButton
      {
        get
        {
          return this.m_owner.m_regretBackwardButton;
        }
        set
        {
          this.m_owner.m_regretBackwardButton = value;
        }
      }

      public CommonUIStateController m_regretBackwardUIStateController
      {
        get
        {
          return this.m_owner.m_regretBackwardUIStateController;
        }
        set
        {
          this.m_owner.m_regretBackwardUIStateController = value;
        }
      }

      public Button m_regretForwardButton
      {
        get
        {
          return this.m_owner.m_regretForwardButton;
        }
        set
        {
          this.m_owner.m_regretForwardButton = value;
        }
      }

      public CommonUIStateController m_regretForwardUIStateController
      {
        get
        {
          return this.m_owner.m_regretForwardUIStateController;
        }
        set
        {
          this.m_owner.m_regretForwardUIStateController = value;
        }
      }

      public Button m_regretPrevTurnButton
      {
        get
        {
          return this.m_owner.m_regretPrevTurnButton;
        }
        set
        {
          this.m_owner.m_regretPrevTurnButton = value;
        }
      }

      public CommonUIStateController m_regretPrevTurnUIStateController
      {
        get
        {
          return this.m_owner.m_regretPrevTurnUIStateController;
        }
        set
        {
          this.m_owner.m_regretPrevTurnUIStateController = value;
        }
      }

      public Button m_regretNextTurnButton
      {
        get
        {
          return this.m_owner.m_regretNextTurnButton;
        }
        set
        {
          this.m_owner.m_regretNextTurnButton = value;
        }
      }

      public CommonUIStateController m_regretNextTurnUIStateController
      {
        get
        {
          return this.m_owner.m_regretNextTurnUIStateController;
        }
        set
        {
          this.m_owner.m_regretNextTurnUIStateController = value;
        }
      }

      public Text m_regretTurnText
      {
        get
        {
          return this.m_owner.m_regretTurnText;
        }
        set
        {
          this.m_owner.m_regretTurnText = value;
        }
      }

      public Text m_regretActionText
      {
        get
        {
          return this.m_owner.m_regretActionText;
        }
        set
        {
          this.m_owner.m_regretActionText = value;
        }
      }

      public CommonUIStateController m_regretTurnUIStateController
      {
        get
        {
          return this.m_owner.m_regretTurnUIStateController;
        }
        set
        {
          this.m_owner.m_regretTurnUIStateController = value;
        }
      }

      public Text m_regretMyCountText
      {
        get
        {
          return this.m_owner.m_regretMyCountText;
        }
        set
        {
          this.m_owner.m_regretMyCountText = value;
        }
      }

      public Text m_regretEnemyCountText
      {
        get
        {
          return this.m_owner.m_regretEnemyCountText;
        }
        set
        {
          this.m_owner.m_regretEnemyCountText = value;
        }
      }

      public CommonUIStateController m_regretEffectUIStateController
      {
        get
        {
          return this.m_owner.m_regretEffectUIStateController;
        }
        set
        {
          this.m_owner.m_regretEffectUIStateController = value;
        }
      }

      public CommonUIStateController m_regretFxUIStateController
      {
        get
        {
          return this.m_owner.m_regretFxUIStateController;
        }
        set
        {
          this.m_owner.m_regretFxUIStateController = value;
        }
      }

      public CommonUIStateController m_cutsceneSkillUIStateController
      {
        get
        {
          return this.m_owner.m_cutsceneSkillUIStateController;
        }
        set
        {
          this.m_owner.m_cutsceneSkillUIStateController = value;
        }
      }

      public Image m_cutsceneSkillIconImage
      {
        get
        {
          return this.m_owner.m_cutsceneSkillIconImage;
        }
        set
        {
          this.m_owner.m_cutsceneSkillIconImage = value;
        }
      }

      public Text m_cutsceneSkillNameText
      {
        get
        {
          return this.m_owner.m_cutsceneSkillNameText;
        }
        set
        {
          this.m_owner.m_cutsceneSkillNameText = value;
        }
      }

      public GameObject m_objectiveGameObject
      {
        get
        {
          return this.m_owner.m_objectiveGameObject;
        }
        set
        {
          this.m_owner.m_objectiveGameObject = value;
        }
      }

      public Text m_objectiveWinDescText
      {
        get
        {
          return this.m_owner.m_objectiveWinDescText;
        }
        set
        {
          this.m_owner.m_objectiveWinDescText = value;
        }
      }

      public Text m_objectiveLoseDescText
      {
        get
        {
          return this.m_owner.m_objectiveLoseDescText;
        }
        set
        {
          this.m_owner.m_objectiveLoseDescText = value;
        }
      }

      public GameObject m_winGameObject
      {
        get
        {
          return this.m_owner.m_winGameObject;
        }
        set
        {
          this.m_owner.m_winGameObject = value;
        }
      }

      public GameObject m_loseGameObject
      {
        get
        {
          return this.m_owner.m_loseGameObject;
        }
        set
        {
          this.m_owner.m_loseGameObject = value;
        }
      }

      public GameObject m_turnStartGameObject
      {
        get
        {
          return this.m_owner.m_turnStartGameObject;
        }
        set
        {
          this.m_owner.m_turnStartGameObject = value;
        }
      }

      public Text m_turnStartText
      {
        get
        {
          return this.m_owner.m_turnStartText;
        }
        set
        {
          this.m_owner.m_turnStartText = value;
        }
      }

      public GameObject m_playerTurnGameObject
      {
        get
        {
          return this.m_owner.m_playerTurnGameObject;
        }
        set
        {
          this.m_owner.m_playerTurnGameObject = value;
        }
      }

      public GameObject m_enemyTurnGameObject
      {
        get
        {
          return this.m_owner.m_enemyTurnGameObject;
        }
        set
        {
          this.m_owner.m_enemyTurnGameObject = value;
        }
      }

      public CommonUIStateController m_myActionUIStateController
      {
        get
        {
          return this.m_owner.m_myActionUIStateController;
        }
        set
        {
          this.m_owner.m_myActionUIStateController = value;
        }
      }

      public Image m_myActionHeadImage
      {
        get
        {
          return this.m_owner.m_myActionHeadImage;
        }
        set
        {
          this.m_owner.m_myActionHeadImage = value;
        }
      }

      public Text m_myActionNameText
      {
        get
        {
          return this.m_owner.m_myActionNameText;
        }
        set
        {
          this.m_owner.m_myActionNameText = value;
        }
      }

      public Image m_myActionPlayerTagImage
      {
        get
        {
          return this.m_owner.m_myActionPlayerTagImage;
        }
        set
        {
          this.m_owner.m_myActionPlayerTagImage = value;
        }
      }

      public CommonUIStateController m_teammateActionUIStateController
      {
        get
        {
          return this.m_owner.m_teammateActionUIStateController;
        }
        set
        {
          this.m_owner.m_teammateActionUIStateController = value;
        }
      }

      public Image m_teammateActionHeadImage
      {
        get
        {
          return this.m_owner.m_teammateActionHeadImage;
        }
        set
        {
          this.m_owner.m_teammateActionHeadImage = value;
        }
      }

      public Text m_teammateActionNameText
      {
        get
        {
          return this.m_owner.m_teammateActionNameText;
        }
        set
        {
          this.m_owner.m_teammateActionNameText = value;
        }
      }

      public Image m_teammateActionPlayerTagImage
      {
        get
        {
          return this.m_owner.m_teammateActionPlayerTagImage;
        }
        set
        {
          this.m_owner.m_teammateActionPlayerTagImage = value;
        }
      }

      public CommonUIStateController m_enemyActionUIStateController
      {
        get
        {
          return this.m_owner.m_enemyActionUIStateController;
        }
        set
        {
          this.m_owner.m_enemyActionUIStateController = value;
        }
      }

      public Image m_enemyActionHeadImage
      {
        get
        {
          return this.m_owner.m_enemyActionHeadImage;
        }
        set
        {
          this.m_owner.m_enemyActionHeadImage = value;
        }
      }

      public Text m_enemyActionNameText
      {
        get
        {
          return this.m_owner.m_enemyActionNameText;
        }
        set
        {
          this.m_owner.m_enemyActionNameText = value;
        }
      }

      public GameObject m_prefabsGameObject
      {
        get
        {
          return this.m_owner.m_prefabsGameObject;
        }
        set
        {
          this.m_owner.m_prefabsGameObject = value;
        }
      }

      public GameObject m_battleSkillButtonPrefab
      {
        get
        {
          return this.m_owner.m_battleSkillButtonPrefab;
        }
        set
        {
          this.m_owner.m_battleSkillButtonPrefab = value;
        }
      }

      public CommonUIStateController m_armyRelationButtonUIStateController
      {
        get
        {
          return this.m_owner.m_armyRelationButtonUIStateController;
        }
        set
        {
          this.m_owner.m_armyRelationButtonUIStateController = value;
        }
      }

      public CommonUIStateController m_armyRelationDescUIStateController
      {
        get
        {
          return this.m_owner.m_armyRelationDescUIStateController;
        }
        set
        {
          this.m_owner.m_armyRelationDescUIStateController = value;
        }
      }

      public Vector2 m_pointerDownPosition
      {
        get
        {
          return this.m_owner.m_pointerDownPosition;
        }
        set
        {
          this.m_owner.m_pointerDownPosition = value;
        }
      }

      public GameObjectPool<BattleSkillButton> m_battleSkillButtonPool
      {
        get
        {
          return this.m_owner.m_battleSkillButtonPool;
        }
        set
        {
          this.m_owner.m_battleSkillButtonPool = value;
        }
      }

      public float m_chatRedPointLastTime
      {
        get
        {
          return this.m_owner.m_chatRedPointLastTime;
        }
        set
        {
          this.m_owner.m_chatRedPointLastTime = value;
        }
      }

      public float m_hideSkillHintTime
      {
        get
        {
          return this.m_owner.m_hideSkillHintTime;
        }
        set
        {
          this.m_owner.m_hideSkillHintTime = value;
        }
      }

      public bool m_isShowSkillHint
      {
        get
        {
          return this.m_owner.m_isShowSkillHint;
        }
        set
        {
          this.m_owner.m_isShowSkillHint = value;
        }
      }

      public bool m_isShowRegretPanel
      {
        get
        {
          return this.m_owner.m_isShowRegretPanel;
        }
        set
        {
          this.m_owner.m_isShowRegretPanel = value;
        }
      }

      public int m_developerClickCount
      {
        get
        {
          return this.m_owner.m_developerClickCount;
        }
        set
        {
          this.m_owner.m_developerClickCount = value;
        }
      }

      public int m_turn
      {
        get
        {
          return this.m_owner.m_turn;
        }
        set
        {
          this.m_owner.m_turn = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void Update()
      {
        this.m_owner.Update();
      }

      public void WinOrLoseTweenFinished()
      {
        this.m_owner.WinOrLoseTweenFinished();
      }

      public static void ComputeActionIconPositionScale(
        int index,
        out Vector2 pos,
        out float scale)
      {
        BattleUIController.ComputeActionIconPositionScale(index, out pos, out scale);
      }

      public void HideArmyRelation()
      {
        this.m_owner.HideArmyRelation();
      }

      public void HideCutsceneSkill()
      {
        this.m_owner.HideCutsceneSkill();
      }

      public void OnPauseButtonClick()
      {
        this.m_owner.OnPauseButtonClick();
      }

      public void OnArmyRelationButtonClick()
      {
        this.m_owner.OnArmyRelationButtonClick();
      }

      public void OnAutoButtonClick()
      {
        this.m_owner.OnAutoButtonClick();
      }

      public void OnAutoOffButtonClick()
      {
        this.m_owner.OnAutoOffButtonClick();
      }

      public void OnArenaAutoButtonClick()
      {
        this.m_owner.OnArenaAutoButtonClick();
      }

      public void OnFastButtonClick()
      {
        this.m_owner.OnFastButtonClick();
      }

      public void OnSkipCombatButtonClick()
      {
        this.m_owner.OnSkipCombatButtonClick();
      }

      public void OnDangerOnButtonClick()
      {
        this.m_owner.OnDangerOnButtonClick();
      }

      public void OnDangerOffButtonClick()
      {
        this.m_owner.OnDangerOffButtonClick();
      }

      public void OnEndActionButtonClick()
      {
        this.m_owner.OnEndActionButtonClick();
      }

      public void OnEndAllActionButtonClick()
      {
        this.m_owner.OnEndAllActionButtonClick();
      }

      public void OnActorSimpleInfoButtonClick()
      {
        this.m_owner.OnActorSimpleInfoButtonClick();
      }

      public void OnUseSkillButtonClick()
      {
        this.m_owner.OnUseSkillButtonClick();
      }

      public void OnCancelSkillButtonClick()
      {
        this.m_owner.OnCancelSkillButtonClick();
      }

      public void BattleSkillButton_OnClick(BattleSkillButton sb)
      {
        this.m_owner.BattleSkillButton_OnClick(sb);
      }

      public void OnChatButtonClick()
      {
        this.m_owner.OnChatButtonClick();
      }

      public void OnHelpButtonClick()
      {
        this.m_owner.OnHelpButtonClick();
      }

      public void OnTerrainInfoButtonClick()
      {
        this.m_owner.OnTerrainInfoButtonClick();
      }

      public void DeveloperModeClick()
      {
        this.m_owner.DeveloperModeClick();
      }

      public void OnDanmakuToggleValueChanged(bool isOn)
      {
        this.m_owner.OnDanmakuToggleValueChanged(isOn);
      }

      public void OnDanmakuInputWordButtonClick()
      {
        this.m_owner.OnDanmakuInputWordButtonClick();
      }

      public void OnDanmakuInputBackButtonClick()
      {
        this.m_owner.OnDanmakuInputBackButtonClick();
      }

      public void OnDanmakuSendButtonClick()
      {
        this.m_owner.OnDanmakuSendButtonClick();
      }

      public void OnRegretButtonClick()
      {
        this.m_owner.OnRegretButtonClick();
      }

      public void OnRegretConfirmButtonClick()
      {
        this.m_owner.OnRegretConfirmButtonClick();
      }

      public void OnRegretCancelButtonClick()
      {
        this.m_owner.OnRegretCancelButtonClick();
      }

      public void OnRegretBackwardButtonClick()
      {
        this.m_owner.OnRegretBackwardButtonClick();
      }

      public void OnRegretForwardButtonClick()
      {
        this.m_owner.OnRegretForwardButtonClick();
      }

      public void OnRegretPrevTurnButtonClick()
      {
        this.m_owner.OnRegretPrevTurnButtonClick();
      }

      public void OnRegretNextTurnButtonClick()
      {
        this.m_owner.OnRegretNextTurnButtonClick();
      }
    }
  }
}
