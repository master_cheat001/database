﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ArenaDefendUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.EventSystems;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class ArenaDefendUITask : UITask
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private ArenaDefendUIController m_arenaDefendUIController;
    private ArenaDefendMapUIController m_arenaDefendMapUIController;
    private ArenaDefendSceneUIController m_arenaDefendSceneUIController;
    private BattlePrepareActorInfoUIController m_battlePrepareActorInfoUIController;
    private ArenaDefendBattle m_arenaDefendBattle;
    private List<GridPosition> m_defendPositions;
    private List<GridPosition> m_attackPositions;
    private List<ConfigDataArenaBattleInfo> m_arenaBattleInfos;
    private List<ConfigDataArenaDefendRuleInfo> m_defendRuleInfos;
    private List<BattleHero> m_playerBattleHeros;
    private List<BattleHero> m_defendStageHeros;
    private List<TrainingTech> m_trainingTechs;
    private int m_curBattleIndex;
    private int m_curDefendRuleIndex;
    private IConfigDataLoader m_configDataLoader;
    [DoNotToLua]
    private ArenaDefendUITask.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctorString_hotfix;
    private LuaFunction m_IsNeedLoadDynamicRes_hotfix;
    private LuaFunction m_CollectBattlefieldAssetsConfigDataBattlefieldInfo_hotfix;
    private LuaFunction m_CollectHeroAndSoldierModelAssetsBattleHero_hotfix;
    private LuaFunction m_CollectHeadImageAssetsConfigDataCharImageInfo_hotfix;
    private LuaFunction m_InitLayerStateOnLoadAllResCompleted_hotfix;
    private LuaFunction m_InitAllUIControllers_hotfix;
    private LuaFunction m_ClearAllContextAndRes_hotfix;
    private LuaFunction m_InitArenaDefendUIController_hotfix;
    private LuaFunction m_UninitArenaDefendUIController_hotfix;
    private LuaFunction m_OnMemoryWarning_hotfix;
    private LuaFunction m_UpdateView_hotfix;
    private LuaFunction m_OnTick_hotfix;
    private LuaFunction m_SetupStageActors_hotfix;
    private LuaFunction m_ShowStagePositions_hotfix;
    private LuaFunction m_ClearMapAndActors_hotfix;
    private LuaFunction m_ArenaDefendUIController_OnReturn_hotfix;
    private LuaFunction m_ArenaDefendUIController_OnSave_hotfix;
    private LuaFunction m_ArenaDefendUIController_OnShowActionOrderPanel_hotfix;
    private LuaFunction m_UpdateStageHeroActionValues_hotfix;
    private LuaFunction m_ArenaDefendUIController_OnShowMapPanel_hotfix;
    private LuaFunction m_ArenaDefendUIController_OnShowDefendRulePanel_hotfix;
    private LuaFunction m_ArenaDefendUIController_OnConfirmActionOrder_hotfix;
    private LuaFunction m_ArenaDefendUIController_OnConfirmMapInt32_hotfix;
    private LuaFunction m_ArenaDefendUIController_OnConfirmDefendRuleInt32_hotfix;
    private LuaFunction m_ArenaDefendUIController_OnShowMyActorInfoBattleHero_hotfix;
    private LuaFunction m_ArenaDefendUIController_OnHideActorInfo_hotfix;
    private LuaFunction m_ArenaDefendUIController_OnStageActorChange_hotfix;
    private LuaFunction m_UpdateBattlePower_hotfix;
    private LuaFunction m_ArenaDefendUIController_OnHeroOnStageBattleHeroGridPosition_hotfix;
    private LuaFunction m_ArenaDefendUIController_OnActorOffStageArenaDefendActor_hotfix;
    private LuaFunction m_ArenaDefendUIController_OnStageActorMoveArenaDefendActorGridPosition_hotfix;
    private LuaFunction m_ArenaDefendUIController_OnStageActorSwapArenaDefendActorArenaDefendActor_hotfix;
    private LuaFunction m_ArenaDefendUIController_OnPointerDownInputButtonVector2_hotfix;
    private LuaFunction m_ArenaDefendUIController_OnPointerUpInputButtonVector2_hotfix;
    private LuaFunction m_ArenaDefendUIController_OnPointerClickInputButtonVector2_hotfix;
    private LuaFunction m_BattlePrepareActorInfoUIController_OnShowSelectSoldierPanelBattleHero_hotfix;
    private LuaFunction m_BattlePrepareActorInfoUIController_OnChangeSkillBattleHeroList`1_hotfix;
    private LuaFunction m_BattlePrepareActorInfoUIController_OnChangeSoldierBattleHeroInt32_hotfix;
    private LuaFunction m_ArenaDefendSceneUIController_OnPointerDownPointerEventData_hotfix;
    private LuaFunction m_ArenaDefendSceneUIController_OnPointerUpPointerEventData_hotfix;
    private LuaFunction m_ArenaDefendSceneUIController_OnPointerClickPointerEventData_hotfix;
    private LuaFunction m_ArenaDefendSceneUIController_OnBeginDragPointerEventData_hotfix;
    private LuaFunction m_ArenaDefendSceneUIController_OnEndDragPointerEventData_hotfix;
    private LuaFunction m_ArenaDefendSceneUIController_OnDragPointerEventData_hotfix;
    private LuaFunction m_get_LayerDescArray_hotfix;
    private LuaFunction m_get_UICtrlDescArray_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaDefendUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectBattlefieldAssets(ConfigDataBattlefieldInfo battlefieldInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectHeroAndSoldierModelAssets(BattleHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectHeadImageAssets(ConfigDataCharImageInfo charImageInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitLayerStateOnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitArenaDefendUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UninitArenaDefendUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnMemoryWarning()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnTick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetupStageActors()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowStagePositions()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearMapAndActors()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendUIController_OnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendUIController_OnSave()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendUIController_OnShowActionOrderPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateStageHeroActionValues()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int CompareHeroActionValue(BattleHero hero0, BattleHero hero1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendUIController_OnShowMapPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendUIController_OnShowDefendRulePanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendUIController_OnConfirmActionOrder()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendUIController_OnConfirmMap(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendUIController_OnConfirmDefendRule(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendUIController_OnShowMyActorInfo(BattleHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendUIController_OnHideActorInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendUIController_OnStageActorChange()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateBattlePower()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendUIController_OnHeroOnStage(BattleHero hero, GridPosition pos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendUIController_OnActorOffStage(ArenaDefendActor sa)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendUIController_OnStageActorMove(ArenaDefendActor sa, GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendUIController_OnStageActorSwap(
      ArenaDefendActor sa1,
      ArenaDefendActor sa2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendUIController_OnPointerDown(
      PointerEventData.InputButton button,
      Vector2 position)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendUIController_OnPointerUp(
      PointerEventData.InputButton button,
      Vector2 position)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendUIController_OnPointerClick(
      PointerEventData.InputButton button,
      Vector2 position)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePrepareActorInfoUIController_OnShowSelectSoldierPanel(BattleHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePrepareActorInfoUIController_OnChangeSkill(
      BattleHero hero,
      List<int> skillIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePrepareActorInfoUIController_OnChangeSoldier(BattleHero hero, int soldierId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendSceneUIController_OnPointerDown(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendSceneUIController_OnPointerUp(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendSceneUIController_OnPointerClick(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendSceneUIController_OnBeginDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendSceneUIController_OnEndDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendSceneUIController_OnDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public ArenaDefendUITask.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private bool __callBase_OnStart(UIIntent intent)
    {
      return this.OnStart(intent);
    }

    private void __callBase_OnStop()
    {
      this.OnStop();
    }

    private void __callBase_OnPause()
    {
      this.OnPause();
    }

    private bool __callBase_OnResume(UIIntent intent)
    {
      return this.OnResume(intent);
    }

    private bool __callBase_OnNewIntent(UIIntent intent)
    {
      return this.OnNewIntent(intent);
    }

    private void __callBase_EnableUIInput(bool isEnable, bool? isGlobalEnable)
    {
      this.EnableUIInput(isEnable, isGlobalEnable);
    }

    private bool __callBase_IsNeedLoadDynamicRes()
    {
      return base.IsNeedLoadDynamicRes();
    }

    private List<string> __callBase_CollectAllDynamicResForLoad()
    {
      return this.CollectAllDynamicResForLoad();
    }

    private void __callBase_RegisterPlayerContextEvents()
    {
      this.RegisterPlayerContextEvents();
    }

    private void __callBase_UnregisterPlayerContextEvents()
    {
      this.UnregisterPlayerContextEvents();
    }

    private void __callBase_PostUpdateView()
    {
      this.PostUpdateView();
    }

    private void __callBase_CollectPreloadResourceList()
    {
      this.CollectPreloadResourceList();
    }

    private void __callBase_ClearAssetList()
    {
      this.ClearAssetList();
    }

    private void __callBase_CollectAsset(string assetName)
    {
      this.CollectAsset(assetName);
    }

    private void __callBase_CollectSpriteAsset(string assetName)
    {
      this.CollectSpriteAsset(assetName);
    }

    private void __callBase_CollectFxAsset(string assetName)
    {
      this.CollectFxAsset(assetName);
    }

    private void __callBase_StartLoadCustomAssets(Action onLoadCompleted)
    {
      this.StartLoadCustomAssets(onLoadCompleted);
    }

    private bool __callBase_IsLoadingCustomAssets()
    {
      return this.IsLoadingCustomAssets();
    }

    private bool __callBase_IsPipeLineRunning()
    {
      return this.IsPipeLineRunning();
    }

    private bool __callBase_IsOpeningUI()
    {
      return this.IsOpeningUI();
    }

    private UITaskBase __callBase_ReturnPrevUITask()
    {
      return this.ReturnPrevUITask();
    }

    private void __callBase_ClearUnusedDynamicResourceCache()
    {
      this.ClearUnusedDynamicResourceCache();
    }

    private void __callBase_ClearDynamicResourceCache(List<string> assets)
    {
      this.ClearDynamicResourceCache(assets);
    }

    private void __callBase_OnMemoryWarning()
    {
      base.OnMemoryWarning();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private ArenaDefendUITask m_owner;

      public LuaExportHelper(ArenaDefendUITask owner)
      {
        this.m_owner = owner;
      }

      public bool __callBase_OnStart(UIIntent intent)
      {
        return this.m_owner.__callBase_OnStart(intent);
      }

      public void __callBase_OnStop()
      {
        this.m_owner.__callBase_OnStop();
      }

      public void __callBase_OnPause()
      {
        this.m_owner.__callBase_OnPause();
      }

      public bool __callBase_OnResume(UIIntent intent)
      {
        return this.m_owner.__callBase_OnResume(intent);
      }

      public bool __callBase_OnNewIntent(UIIntent intent)
      {
        return this.m_owner.__callBase_OnNewIntent(intent);
      }

      public void __callBase_EnableUIInput(bool isEnable, bool? isGlobalEnable)
      {
        this.m_owner.__callBase_EnableUIInput(isEnable, isGlobalEnable);
      }

      public bool __callBase_IsNeedLoadDynamicRes()
      {
        return this.m_owner.__callBase_IsNeedLoadDynamicRes();
      }

      public List<string> __callBase_CollectAllDynamicResForLoad()
      {
        return this.m_owner.__callBase_CollectAllDynamicResForLoad();
      }

      public void __callBase_RegisterPlayerContextEvents()
      {
        this.m_owner.__callBase_RegisterPlayerContextEvents();
      }

      public void __callBase_UnregisterPlayerContextEvents()
      {
        this.m_owner.__callBase_UnregisterPlayerContextEvents();
      }

      public void __callBase_PostUpdateView()
      {
        this.m_owner.__callBase_PostUpdateView();
      }

      public void __callBase_CollectPreloadResourceList()
      {
        this.m_owner.__callBase_CollectPreloadResourceList();
      }

      public void __callBase_ClearAssetList()
      {
        this.m_owner.__callBase_ClearAssetList();
      }

      public void __callBase_CollectAsset(string assetName)
      {
        this.m_owner.__callBase_CollectAsset(assetName);
      }

      public void __callBase_CollectSpriteAsset(string assetName)
      {
        this.m_owner.__callBase_CollectSpriteAsset(assetName);
      }

      public void __callBase_CollectFxAsset(string assetName)
      {
        this.m_owner.__callBase_CollectFxAsset(assetName);
      }

      public void __callBase_StartLoadCustomAssets(Action onLoadCompleted)
      {
        this.m_owner.__callBase_StartLoadCustomAssets(onLoadCompleted);
      }

      public bool __callBase_IsLoadingCustomAssets()
      {
        return this.m_owner.__callBase_IsLoadingCustomAssets();
      }

      public bool __callBase_IsPipeLineRunning()
      {
        return this.m_owner.__callBase_IsPipeLineRunning();
      }

      public bool __callBase_IsOpeningUI()
      {
        return this.m_owner.__callBase_IsOpeningUI();
      }

      public UITaskBase __callBase_ReturnPrevUITask()
      {
        return this.m_owner.__callBase_ReturnPrevUITask();
      }

      public void __callBase_ClearUnusedDynamicResourceCache()
      {
        this.m_owner.__callBase_ClearUnusedDynamicResourceCache();
      }

      public void __callBase_ClearDynamicResourceCache(List<string> assets)
      {
        this.m_owner.__callBase_ClearDynamicResourceCache(assets);
      }

      public void __callBase_OnMemoryWarning()
      {
        this.m_owner.__callBase_OnMemoryWarning();
      }

      public UITaskBase.LayerDesc[] m_layerDescArray
      {
        get
        {
          return this.m_owner.m_layerDescArray;
        }
        set
        {
          this.m_owner.m_layerDescArray = value;
        }
      }

      public UITaskBase.UIControllerDesc[] m_uiCtrlDescArray
      {
        get
        {
          return this.m_owner.m_uiCtrlDescArray;
        }
        set
        {
          this.m_owner.m_uiCtrlDescArray = value;
        }
      }

      public ArenaDefendUIController m_arenaDefendUIController
      {
        get
        {
          return this.m_owner.m_arenaDefendUIController;
        }
        set
        {
          this.m_owner.m_arenaDefendUIController = value;
        }
      }

      public ArenaDefendMapUIController m_arenaDefendMapUIController
      {
        get
        {
          return this.m_owner.m_arenaDefendMapUIController;
        }
        set
        {
          this.m_owner.m_arenaDefendMapUIController = value;
        }
      }

      public ArenaDefendSceneUIController m_arenaDefendSceneUIController
      {
        get
        {
          return this.m_owner.m_arenaDefendSceneUIController;
        }
        set
        {
          this.m_owner.m_arenaDefendSceneUIController = value;
        }
      }

      public BattlePrepareActorInfoUIController m_battlePrepareActorInfoUIController
      {
        get
        {
          return this.m_owner.m_battlePrepareActorInfoUIController;
        }
        set
        {
          this.m_owner.m_battlePrepareActorInfoUIController = value;
        }
      }

      public ArenaDefendBattle m_arenaDefendBattle
      {
        get
        {
          return this.m_owner.m_arenaDefendBattle;
        }
        set
        {
          this.m_owner.m_arenaDefendBattle = value;
        }
      }

      public List<GridPosition> m_defendPositions
      {
        get
        {
          return this.m_owner.m_defendPositions;
        }
        set
        {
          this.m_owner.m_defendPositions = value;
        }
      }

      public List<GridPosition> m_attackPositions
      {
        get
        {
          return this.m_owner.m_attackPositions;
        }
        set
        {
          this.m_owner.m_attackPositions = value;
        }
      }

      public List<ConfigDataArenaBattleInfo> m_arenaBattleInfos
      {
        get
        {
          return this.m_owner.m_arenaBattleInfos;
        }
        set
        {
          this.m_owner.m_arenaBattleInfos = value;
        }
      }

      public List<ConfigDataArenaDefendRuleInfo> m_defendRuleInfos
      {
        get
        {
          return this.m_owner.m_defendRuleInfos;
        }
        set
        {
          this.m_owner.m_defendRuleInfos = value;
        }
      }

      public List<BattleHero> m_playerBattleHeros
      {
        get
        {
          return this.m_owner.m_playerBattleHeros;
        }
        set
        {
          this.m_owner.m_playerBattleHeros = value;
        }
      }

      public List<BattleHero> m_defendStageHeros
      {
        get
        {
          return this.m_owner.m_defendStageHeros;
        }
        set
        {
          this.m_owner.m_defendStageHeros = value;
        }
      }

      public List<TrainingTech> m_trainingTechs
      {
        get
        {
          return this.m_owner.m_trainingTechs;
        }
        set
        {
          this.m_owner.m_trainingTechs = value;
        }
      }

      public int m_curBattleIndex
      {
        get
        {
          return this.m_owner.m_curBattleIndex;
        }
        set
        {
          this.m_owner.m_curBattleIndex = value;
        }
      }

      public int m_curDefendRuleIndex
      {
        get
        {
          return this.m_owner.m_curDefendRuleIndex;
        }
        set
        {
          this.m_owner.m_curDefendRuleIndex = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public UITaskBase.LayerDesc[] LayerDescArray
      {
        get
        {
          return this.m_owner.LayerDescArray;
        }
      }

      public UITaskBase.UIControllerDesc[] UICtrlDescArray
      {
        get
        {
          return this.m_owner.UICtrlDescArray;
        }
      }

      public bool IsNeedLoadDynamicRes()
      {
        return this.m_owner.IsNeedLoadDynamicRes();
      }

      public void CollectBattlefieldAssets(ConfigDataBattlefieldInfo battlefieldInfo)
      {
        this.m_owner.CollectBattlefieldAssets(battlefieldInfo);
      }

      public void CollectHeroAndSoldierModelAssets(BattleHero hero)
      {
        this.m_owner.CollectHeroAndSoldierModelAssets(hero);
      }

      public void CollectHeadImageAssets(ConfigDataCharImageInfo charImageInfo)
      {
        this.m_owner.CollectHeadImageAssets(charImageInfo);
      }

      public void InitLayerStateOnLoadAllResCompleted()
      {
        this.m_owner.InitLayerStateOnLoadAllResCompleted();
      }

      public void InitAllUIControllers()
      {
        this.m_owner.InitAllUIControllers();
      }

      public void ClearAllContextAndRes()
      {
        this.m_owner.ClearAllContextAndRes();
      }

      public void InitArenaDefendUIController()
      {
        this.m_owner.InitArenaDefendUIController();
      }

      public void UninitArenaDefendUIController()
      {
        this.m_owner.UninitArenaDefendUIController();
      }

      public void OnMemoryWarning()
      {
        this.m_owner.OnMemoryWarning();
      }

      public void UpdateView()
      {
        this.m_owner.UpdateView();
      }

      public void OnTick()
      {
        this.m_owner.OnTick();
      }

      public void SetupStageActors()
      {
        this.m_owner.SetupStageActors();
      }

      public void ShowStagePositions()
      {
        this.m_owner.ShowStagePositions();
      }

      public void ClearMapAndActors()
      {
        this.m_owner.ClearMapAndActors();
      }

      public void ArenaDefendUIController_OnReturn()
      {
        this.m_owner.ArenaDefendUIController_OnReturn();
      }

      public void ArenaDefendUIController_OnSave()
      {
        this.m_owner.ArenaDefendUIController_OnSave();
      }

      public void ArenaDefendUIController_OnShowActionOrderPanel()
      {
        this.m_owner.ArenaDefendUIController_OnShowActionOrderPanel();
      }

      public void UpdateStageHeroActionValues()
      {
        this.m_owner.UpdateStageHeroActionValues();
      }

      public static int CompareHeroActionValue(BattleHero hero0, BattleHero hero1)
      {
        return ArenaDefendUITask.CompareHeroActionValue(hero0, hero1);
      }

      public void ArenaDefendUIController_OnShowMapPanel()
      {
        this.m_owner.ArenaDefendUIController_OnShowMapPanel();
      }

      public void ArenaDefendUIController_OnShowDefendRulePanel()
      {
        this.m_owner.ArenaDefendUIController_OnShowDefendRulePanel();
      }

      public void ArenaDefendUIController_OnConfirmActionOrder()
      {
        this.m_owner.ArenaDefendUIController_OnConfirmActionOrder();
      }

      public void ArenaDefendUIController_OnConfirmMap(int index)
      {
        this.m_owner.ArenaDefendUIController_OnConfirmMap(index);
      }

      public void ArenaDefendUIController_OnConfirmDefendRule(int index)
      {
        this.m_owner.ArenaDefendUIController_OnConfirmDefendRule(index);
      }

      public void ArenaDefendUIController_OnShowMyActorInfo(BattleHero hero)
      {
        this.m_owner.ArenaDefendUIController_OnShowMyActorInfo(hero);
      }

      public void ArenaDefendUIController_OnHideActorInfo()
      {
        this.m_owner.ArenaDefendUIController_OnHideActorInfo();
      }

      public void ArenaDefendUIController_OnStageActorChange()
      {
        this.m_owner.ArenaDefendUIController_OnStageActorChange();
      }

      public void UpdateBattlePower()
      {
        this.m_owner.UpdateBattlePower();
      }

      public void ArenaDefendUIController_OnHeroOnStage(BattleHero hero, GridPosition pos)
      {
        this.m_owner.ArenaDefendUIController_OnHeroOnStage(hero, pos);
      }

      public void ArenaDefendUIController_OnActorOffStage(ArenaDefendActor sa)
      {
        this.m_owner.ArenaDefendUIController_OnActorOffStage(sa);
      }

      public void ArenaDefendUIController_OnStageActorMove(ArenaDefendActor sa, GridPosition p)
      {
        this.m_owner.ArenaDefendUIController_OnStageActorMove(sa, p);
      }

      public void ArenaDefendUIController_OnStageActorSwap(
        ArenaDefendActor sa1,
        ArenaDefendActor sa2)
      {
        this.m_owner.ArenaDefendUIController_OnStageActorSwap(sa1, sa2);
      }

      public void ArenaDefendUIController_OnPointerDown(
        PointerEventData.InputButton button,
        Vector2 position)
      {
        this.m_owner.ArenaDefendUIController_OnPointerDown(button, position);
      }

      public void ArenaDefendUIController_OnPointerUp(
        PointerEventData.InputButton button,
        Vector2 position)
      {
        this.m_owner.ArenaDefendUIController_OnPointerUp(button, position);
      }

      public void ArenaDefendUIController_OnPointerClick(
        PointerEventData.InputButton button,
        Vector2 position)
      {
        this.m_owner.ArenaDefendUIController_OnPointerClick(button, position);
      }

      public void BattlePrepareActorInfoUIController_OnShowSelectSoldierPanel(BattleHero hero)
      {
        this.m_owner.BattlePrepareActorInfoUIController_OnShowSelectSoldierPanel(hero);
      }

      public void BattlePrepareActorInfoUIController_OnChangeSkill(
        BattleHero hero,
        List<int> skillIds)
      {
        this.m_owner.BattlePrepareActorInfoUIController_OnChangeSkill(hero, skillIds);
      }

      public void BattlePrepareActorInfoUIController_OnChangeSoldier(BattleHero hero, int soldierId)
      {
        this.m_owner.BattlePrepareActorInfoUIController_OnChangeSoldier(hero, soldierId);
      }

      public void ArenaDefendSceneUIController_OnPointerDown(PointerEventData eventData)
      {
        this.m_owner.ArenaDefendSceneUIController_OnPointerDown(eventData);
      }

      public void ArenaDefendSceneUIController_OnPointerUp(PointerEventData eventData)
      {
        this.m_owner.ArenaDefendSceneUIController_OnPointerUp(eventData);
      }

      public void ArenaDefendSceneUIController_OnPointerClick(PointerEventData eventData)
      {
        this.m_owner.ArenaDefendSceneUIController_OnPointerClick(eventData);
      }

      public void ArenaDefendSceneUIController_OnBeginDrag(PointerEventData eventData)
      {
        this.m_owner.ArenaDefendSceneUIController_OnBeginDrag(eventData);
      }

      public void ArenaDefendSceneUIController_OnEndDrag(PointerEventData eventData)
      {
        this.m_owner.ArenaDefendSceneUIController_OnEndDrag(eventData);
      }

      public void ArenaDefendSceneUIController_OnDrag(PointerEventData eventData)
      {
        this.m_owner.ArenaDefendSceneUIController_OnDrag(eventData);
      }
    }
  }
}
