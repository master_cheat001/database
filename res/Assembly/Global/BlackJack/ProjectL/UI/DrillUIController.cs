﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.DrillUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class DrillUIController : UIControllerBase
  {
    [AutoBind("./Margin", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./HelpButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_helpButton;
    [AutoBind("./FastMaxButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_fastMaxButton;
    [AutoBind("./TrainingPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_trainingStateCtrl;
    [AutoBind("./TrainingPanel/LeftToggleGroup/Toggles", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_trainingPanelLeftToggleGroup;
    [AutoBind("./TrainingPanel/DetailPanel/TitleInfoGroup/LvValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_trainingPanelTitleInfoLvValueText;
    [AutoBind("./TrainingPanel/DetailPanel/TitleInfoGroup/ExpValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_trainingPanelTitleInfoExpValueText;
    [AutoBind("./TrainingPanel/DetailPanel/TitleInfoGroup/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_trainingPanelTitleInfoProgressBar;
    [AutoBind("./TrainingPanel/DetailPanel/TrainingEventScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_trainingPanelTrainingEventScrollViewContent;
    [AutoBind("./TrainingPanel/DetailPanel/TrainingButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_trainingPanelTrainingButton;
    [AutoBind("./TrainingPanel/DetailPanel/PropretyGroup/Detail/HPAdd", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_trainingPanelPropretyGroupHPAddStateCtrl;
    [AutoBind("./TrainingPanel/DetailPanel/PropretyGroup/Detail/HPAdd/Value", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_trainingPanelPropretyGroupHPAddValue;
    [AutoBind("./TrainingPanel/DetailPanel/PropretyGroup/Detail/AttackAdd", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_trainingPanelPropretyGroupAttackAddStateCtrl;
    [AutoBind("./TrainingPanel/DetailPanel/PropretyGroup/Detail/AttackAdd/Value", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_trainingPanelPropretyGroupAttackAddValue;
    [AutoBind("./TrainingPanel/DetailPanel/PropretyGroup/Detail/DefenseAdd", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_trainingPanelPropretyGroupDefenseAddStateCtrl;
    [AutoBind("./TrainingPanel/DetailPanel/PropretyGroup/Detail/DefenseAdd/Value", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_trainingPanelPropretyGroupDefenseAddValue;
    [AutoBind("./TrainingPanel/DetailPanel/PropretyGroup/Detail/MagicDFAdd", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_trainingPanelPropretyGroupMagicDFAddStateCtrl;
    [AutoBind("./TrainingPanel/DetailPanel/PropretyGroup/Detail/MagicDFAdd/Value", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_trainingPanelPropretyGroupMagicDFAddValue;
    [AutoBind("./TrainingPanel/DetailPanel/PropretyGroup/Detail/HPPer", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_trainingPanelPropretyGroupHPPerStateCtrl;
    [AutoBind("./TrainingPanel/DetailPanel/PropretyGroup/Detail/HPPer/Value", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_trainingPanelPropretyGroupHPPerValue;
    [AutoBind("./TrainingPanel/DetailPanel/PropretyGroup/Detail/AttackPer", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_trainingPanelPropretyGroupAttackPerStateCtrl;
    [AutoBind("./TrainingPanel/DetailPanel/PropretyGroup/Detail/AttackPer/Value", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_trainingPanelPropretyGroupAttackPerValue;
    [AutoBind("./TrainingPanel/DetailPanel/PropretyGroup/Detail/DefensePer", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_trainingPanelPropretyGroupDefensePerStateCtrl;
    [AutoBind("./TrainingPanel/DetailPanel/PropretyGroup/Detail/DefensePer/Value", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_trainingPanelPropretyGroupDefensePerValue;
    [AutoBind("./TrainingPanel/DetailPanel/PropretyGroup/Detail/MagicDFPer", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_trainingPanelPropretyGroupMagicDFPerStateCtrl;
    [AutoBind("./TrainingPanel/DetailPanel/PropretyGroup/Detail/MagicDFPer/Value", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_trainingPanelPropretyGroupMagicDFPerValue;
    [AutoBind("./Teaching", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_teachingStateCtrl;
    [AutoBind("./Teaching", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_teachingChar;
    [AutoBind("./Teaching/Detail/Team1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_teachingDetailTeam1;
    [AutoBind("./Teaching/Detail/Team2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_teachingDetailTeam2;
    [AutoBind("./Teaching/Detail/Team3", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_teachingDetailTeam3;
    [AutoBind("./Teaching/TodayReward/IconGroup/ArmyIcon1", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_teachingTodayRewardArmyIcon1;
    [AutoBind("./Teaching/TodayReward/IconGroup/ArmyIcon2", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_teachingTodayRewardArmyIcon2;
    [AutoBind("./Margin/RightToggle/TrainingToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_trainingToggle;
    [AutoBind("./Margin/RightToggle/TeachingToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_teachingToggle;
    [AutoBind("./Margin/RightToggle/TeachingToggle/Click/RedMark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_teachingToggleClickRedMark;
    [AutoBind("./Margin/RightToggle/TeachingToggle/UnClick/RedMark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_teachingToggleUnClickRedMark;
    [AutoBind("./SoldierInfoButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_soldierInfoButton;
    [AutoBind("./StopTeachingPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_stopTeachingPanelStateCtrl;
    [AutoBind("./StopTeachingPanel/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_stopTeachingPanelBackButton;
    [AutoBind("./StopTeachingPanel/Panel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_stopTeachingPanelConfirmButton;
    [AutoBind("./StopTeachingPanel/Panel/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_stopTeachingPanelCancelButton;
    private DrillUIController.DrillState m_curDrillState;
    private TrainingRoom m_curTrainingRoom;
    private List<AssistanceTeamUIController> m_assistanceTeamUICtrlList;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private int[] m_buffPropArr;
    private HeroCharUIController m_heroCharUIController;
    private AssistanceTeamUIController m_stopHeroAssistantsTaskCtrl;
    private List<CommonUIStateController> propStateCtrlList;
    private List<Text> propGoList;
    [DoNotToLua]
    private DrillUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_Open_hotfix;
    private LuaFunction m_UpdateViewInDrillInt32_hotfix;
    private LuaFunction m_SetTrainingDetailPanelTrainingRoom_hotfix;
    private LuaFunction m_OnCourseItemClickCourseItemUIController_hotfix;
    private LuaFunction m_OnRoomToggleClickDrillRoomToggleUIController_hotfix;
    private LuaFunction m_SetPropertyAddition_hotfix;
    private LuaFunction m_ResetAllPropertyState_hotfix;
    private LuaFunction m_CalcSoldierPropertyModifityAddtionPropertyModifyTypeInt32_hotfix;
    private LuaFunction m_SetTeachingDetailPanel_hotfix;
    private LuaFunction m_SetTeamTimeInt32TimeSpan_hotfix;
    private LuaFunction m_OnAssistanceTeamTrainingButtonClickInt32_hotfix;
    private LuaFunction m_OnAssistanceTeamStopButtonClickAssistanceTeamUIController_hotfix;
    private LuaFunction m_OpenStopTeachingPanel_hotfix;
    private LuaFunction m_CloseStopTeachingPanel_hotfix;
    private LuaFunction m_OnStopTeachingPanelConfirmButtonClick_hotfix;
    private LuaFunction m_OnAssistanceTeamGetRewardButtonClickAssistanceTeamUIController_hotfix;
    private LuaFunction m_OnTrainingButtonClick_hotfix;
    private LuaFunction m_OnSoldierInfoButtonClick_hotfix;
    private LuaFunction m_OnTrainingToggleValueChangedBoolean_hotfix;
    private LuaFunction m_OnTeachingToggleValueChangedBoolean_hotfix;
    private LuaFunction m_OnFastMaxButtonClick_hotfix;
    private LuaFunction m_ClearDataCache_hotfix;
    private LuaFunction m_OnReturnButtonClick_hotfix;
    private LuaFunction m_OnHelpButtonClick_hotfix;
    private LuaFunction m_add_EventOnReturnAction_hotfix;
    private LuaFunction m_remove_EventOnReturnAction_hotfix;
    private LuaFunction m_add_EventOnShowHelpAction_hotfix;
    private LuaFunction m_remove_EventOnShowHelpAction_hotfix;
    private LuaFunction m_add_EventOnManualButtonClickAction_hotfix;
    private LuaFunction m_remove_EventOnManualButtonClickAction_hotfix;
    private LuaFunction m_add_EventOnTrainingButtonClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnTrainingButtonClickAction`1_hotfix;
    private LuaFunction m_add_EventOnAssistanceTrainingButtonClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnAssistanceTrainingButtonClickAction`1_hotfix;
    private LuaFunction m_add_EventOnAssistanceStopButtonClickAction`3_hotfix;
    private LuaFunction m_remove_EventOnAssistanceStopButtonClickAction`3_hotfix;
    private LuaFunction m_add_EventOnAssistanceGetRewardButtonClickAction`2_hotfix;
    private LuaFunction m_remove_EventOnAssistanceGetRewardButtonClickAction`2_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public DrillUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateViewInDrill(int drillState = -1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetTrainingDetailPanel(TrainingRoom room)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCourseItemClick(CourseItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRoomToggleClick(DrillRoomToggleUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPropertyAddition()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ResetAllPropertyState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CalcSoldierPropertyModifityAddtion(PropertyModifyType type, int value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetTeachingDetailPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTeamTime(int slot, TimeSpan ts)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAssistanceTeamTrainingButtonClick(int slot)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAssistanceTeamStopButtonClick(AssistanceTeamUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OpenStopTeachingPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseStopTeachingPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnStopTeachingPanelConfirmButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAssistanceTeamGetRewardButtonClick(AssistanceTeamUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTrainingButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSoldierInfoButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTrainingToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTeachingToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFastMaxButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearDataCache()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHelpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowHelp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnManualButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnTrainingButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnAssistanceTrainingButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int, Action> EventOnAssistanceStopButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int> EventOnAssistanceGetRewardButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public DrillUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnReturn()
    {
      this.EventOnReturn = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowHelp()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowHelp()
    {
      this.EventOnShowHelp = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnManualButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnManualButtonClick()
    {
      this.EventOnManualButtonClick = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnTrainingButtonClick(int obj)
    {
    }

    private void __clearDele_EventOnTrainingButtonClick(int obj)
    {
      this.EventOnTrainingButtonClick = (Action<int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnAssistanceTrainingButtonClick(int obj)
    {
    }

    private void __clearDele_EventOnAssistanceTrainingButtonClick(int obj)
    {
      this.EventOnAssistanceTrainingButtonClick = (Action<int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnAssistanceStopButtonClick(int arg1, int arg2, Action arg3)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnAssistanceStopButtonClick(int arg1, int arg2, Action arg3)
    {
      this.EventOnAssistanceStopButtonClick = (Action<int, int, Action>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnAssistanceGetRewardButtonClick(int arg1, int arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnAssistanceGetRewardButtonClick(int arg1, int arg2)
    {
      this.EventOnAssistanceGetRewardButtonClick = (Action<int, int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public enum DrillState
    {
      Training,
      Teaching,
    }

    public class LuaExportHelper
    {
      private DrillUIController m_owner;

      public LuaExportHelper(DrillUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        return this.m_owner.__callBase_BindFieldImpl(fieldType, path, initState, fieldName, ctrlName, optional);
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnReturn()
      {
        this.m_owner.__callDele_EventOnReturn();
      }

      public void __clearDele_EventOnReturn()
      {
        this.m_owner.__clearDele_EventOnReturn();
      }

      public void __callDele_EventOnShowHelp()
      {
        this.m_owner.__callDele_EventOnShowHelp();
      }

      public void __clearDele_EventOnShowHelp()
      {
        this.m_owner.__clearDele_EventOnShowHelp();
      }

      public void __callDele_EventOnManualButtonClick()
      {
        this.m_owner.__callDele_EventOnManualButtonClick();
      }

      public void __clearDele_EventOnManualButtonClick()
      {
        this.m_owner.__clearDele_EventOnManualButtonClick();
      }

      public void __callDele_EventOnTrainingButtonClick(int obj)
      {
        this.m_owner.__callDele_EventOnTrainingButtonClick(obj);
      }

      public void __clearDele_EventOnTrainingButtonClick(int obj)
      {
        this.m_owner.__clearDele_EventOnTrainingButtonClick(obj);
      }

      public void __callDele_EventOnAssistanceTrainingButtonClick(int obj)
      {
        this.m_owner.__callDele_EventOnAssistanceTrainingButtonClick(obj);
      }

      public void __clearDele_EventOnAssistanceTrainingButtonClick(int obj)
      {
        this.m_owner.__clearDele_EventOnAssistanceTrainingButtonClick(obj);
      }

      public void __callDele_EventOnAssistanceStopButtonClick(int arg1, int arg2, Action arg3)
      {
        this.m_owner.__callDele_EventOnAssistanceStopButtonClick(arg1, arg2, arg3);
      }

      public void __clearDele_EventOnAssistanceStopButtonClick(int arg1, int arg2, Action arg3)
      {
        this.m_owner.__clearDele_EventOnAssistanceStopButtonClick(arg1, arg2, arg3);
      }

      public void __callDele_EventOnAssistanceGetRewardButtonClick(int arg1, int arg2)
      {
        this.m_owner.__callDele_EventOnAssistanceGetRewardButtonClick(arg1, arg2);
      }

      public void __clearDele_EventOnAssistanceGetRewardButtonClick(int arg1, int arg2)
      {
        this.m_owner.__clearDele_EventOnAssistanceGetRewardButtonClick(arg1, arg2);
      }

      public RectTransform m_marginTransform
      {
        get
        {
          return this.m_owner.m_marginTransform;
        }
        set
        {
          this.m_owner.m_marginTransform = value;
        }
      }

      public Button m_returnButton
      {
        get
        {
          return this.m_owner.m_returnButton;
        }
        set
        {
          this.m_owner.m_returnButton = value;
        }
      }

      public Button m_helpButton
      {
        get
        {
          return this.m_owner.m_helpButton;
        }
        set
        {
          this.m_owner.m_helpButton = value;
        }
      }

      public Button m_fastMaxButton
      {
        get
        {
          return this.m_owner.m_fastMaxButton;
        }
        set
        {
          this.m_owner.m_fastMaxButton = value;
        }
      }

      public CommonUIStateController m_trainingStateCtrl
      {
        get
        {
          return this.m_owner.m_trainingStateCtrl;
        }
        set
        {
          this.m_owner.m_trainingStateCtrl = value;
        }
      }

      public GameObject m_trainingPanelLeftToggleGroup
      {
        get
        {
          return this.m_owner.m_trainingPanelLeftToggleGroup;
        }
        set
        {
          this.m_owner.m_trainingPanelLeftToggleGroup = value;
        }
      }

      public Text m_trainingPanelTitleInfoLvValueText
      {
        get
        {
          return this.m_owner.m_trainingPanelTitleInfoLvValueText;
        }
        set
        {
          this.m_owner.m_trainingPanelTitleInfoLvValueText = value;
        }
      }

      public Text m_trainingPanelTitleInfoExpValueText
      {
        get
        {
          return this.m_owner.m_trainingPanelTitleInfoExpValueText;
        }
        set
        {
          this.m_owner.m_trainingPanelTitleInfoExpValueText = value;
        }
      }

      public Image m_trainingPanelTitleInfoProgressBar
      {
        get
        {
          return this.m_owner.m_trainingPanelTitleInfoProgressBar;
        }
        set
        {
          this.m_owner.m_trainingPanelTitleInfoProgressBar = value;
        }
      }

      public GameObject m_trainingPanelTrainingEventScrollViewContent
      {
        get
        {
          return this.m_owner.m_trainingPanelTrainingEventScrollViewContent;
        }
        set
        {
          this.m_owner.m_trainingPanelTrainingEventScrollViewContent = value;
        }
      }

      public Button m_trainingPanelTrainingButton
      {
        get
        {
          return this.m_owner.m_trainingPanelTrainingButton;
        }
        set
        {
          this.m_owner.m_trainingPanelTrainingButton = value;
        }
      }

      public CommonUIStateController m_trainingPanelPropretyGroupHPAddStateCtrl
      {
        get
        {
          return this.m_owner.m_trainingPanelPropretyGroupHPAddStateCtrl;
        }
        set
        {
          this.m_owner.m_trainingPanelPropretyGroupHPAddStateCtrl = value;
        }
      }

      public Text m_trainingPanelPropretyGroupHPAddValue
      {
        get
        {
          return this.m_owner.m_trainingPanelPropretyGroupHPAddValue;
        }
        set
        {
          this.m_owner.m_trainingPanelPropretyGroupHPAddValue = value;
        }
      }

      public CommonUIStateController m_trainingPanelPropretyGroupAttackAddStateCtrl
      {
        get
        {
          return this.m_owner.m_trainingPanelPropretyGroupAttackAddStateCtrl;
        }
        set
        {
          this.m_owner.m_trainingPanelPropretyGroupAttackAddStateCtrl = value;
        }
      }

      public Text m_trainingPanelPropretyGroupAttackAddValue
      {
        get
        {
          return this.m_owner.m_trainingPanelPropretyGroupAttackAddValue;
        }
        set
        {
          this.m_owner.m_trainingPanelPropretyGroupAttackAddValue = value;
        }
      }

      public CommonUIStateController m_trainingPanelPropretyGroupDefenseAddStateCtrl
      {
        get
        {
          return this.m_owner.m_trainingPanelPropretyGroupDefenseAddStateCtrl;
        }
        set
        {
          this.m_owner.m_trainingPanelPropretyGroupDefenseAddStateCtrl = value;
        }
      }

      public Text m_trainingPanelPropretyGroupDefenseAddValue
      {
        get
        {
          return this.m_owner.m_trainingPanelPropretyGroupDefenseAddValue;
        }
        set
        {
          this.m_owner.m_trainingPanelPropretyGroupDefenseAddValue = value;
        }
      }

      public CommonUIStateController m_trainingPanelPropretyGroupMagicDFAddStateCtrl
      {
        get
        {
          return this.m_owner.m_trainingPanelPropretyGroupMagicDFAddStateCtrl;
        }
        set
        {
          this.m_owner.m_trainingPanelPropretyGroupMagicDFAddStateCtrl = value;
        }
      }

      public Text m_trainingPanelPropretyGroupMagicDFAddValue
      {
        get
        {
          return this.m_owner.m_trainingPanelPropretyGroupMagicDFAddValue;
        }
        set
        {
          this.m_owner.m_trainingPanelPropretyGroupMagicDFAddValue = value;
        }
      }

      public CommonUIStateController m_trainingPanelPropretyGroupHPPerStateCtrl
      {
        get
        {
          return this.m_owner.m_trainingPanelPropretyGroupHPPerStateCtrl;
        }
        set
        {
          this.m_owner.m_trainingPanelPropretyGroupHPPerStateCtrl = value;
        }
      }

      public Text m_trainingPanelPropretyGroupHPPerValue
      {
        get
        {
          return this.m_owner.m_trainingPanelPropretyGroupHPPerValue;
        }
        set
        {
          this.m_owner.m_trainingPanelPropretyGroupHPPerValue = value;
        }
      }

      public CommonUIStateController m_trainingPanelPropretyGroupAttackPerStateCtrl
      {
        get
        {
          return this.m_owner.m_trainingPanelPropretyGroupAttackPerStateCtrl;
        }
        set
        {
          this.m_owner.m_trainingPanelPropretyGroupAttackPerStateCtrl = value;
        }
      }

      public Text m_trainingPanelPropretyGroupAttackPerValue
      {
        get
        {
          return this.m_owner.m_trainingPanelPropretyGroupAttackPerValue;
        }
        set
        {
          this.m_owner.m_trainingPanelPropretyGroupAttackPerValue = value;
        }
      }

      public CommonUIStateController m_trainingPanelPropretyGroupDefensePerStateCtrl
      {
        get
        {
          return this.m_owner.m_trainingPanelPropretyGroupDefensePerStateCtrl;
        }
        set
        {
          this.m_owner.m_trainingPanelPropretyGroupDefensePerStateCtrl = value;
        }
      }

      public Text m_trainingPanelPropretyGroupDefensePerValue
      {
        get
        {
          return this.m_owner.m_trainingPanelPropretyGroupDefensePerValue;
        }
        set
        {
          this.m_owner.m_trainingPanelPropretyGroupDefensePerValue = value;
        }
      }

      public CommonUIStateController m_trainingPanelPropretyGroupMagicDFPerStateCtrl
      {
        get
        {
          return this.m_owner.m_trainingPanelPropretyGroupMagicDFPerStateCtrl;
        }
        set
        {
          this.m_owner.m_trainingPanelPropretyGroupMagicDFPerStateCtrl = value;
        }
      }

      public Text m_trainingPanelPropretyGroupMagicDFPerValue
      {
        get
        {
          return this.m_owner.m_trainingPanelPropretyGroupMagicDFPerValue;
        }
        set
        {
          this.m_owner.m_trainingPanelPropretyGroupMagicDFPerValue = value;
        }
      }

      public CommonUIStateController m_teachingStateCtrl
      {
        get
        {
          return this.m_owner.m_teachingStateCtrl;
        }
        set
        {
          this.m_owner.m_teachingStateCtrl = value;
        }
      }

      public GameObject m_teachingChar
      {
        get
        {
          return this.m_owner.m_teachingChar;
        }
        set
        {
          this.m_owner.m_teachingChar = value;
        }
      }

      public GameObject m_teachingDetailTeam1
      {
        get
        {
          return this.m_owner.m_teachingDetailTeam1;
        }
        set
        {
          this.m_owner.m_teachingDetailTeam1 = value;
        }
      }

      public GameObject m_teachingDetailTeam2
      {
        get
        {
          return this.m_owner.m_teachingDetailTeam2;
        }
        set
        {
          this.m_owner.m_teachingDetailTeam2 = value;
        }
      }

      public GameObject m_teachingDetailTeam3
      {
        get
        {
          return this.m_owner.m_teachingDetailTeam3;
        }
        set
        {
          this.m_owner.m_teachingDetailTeam3 = value;
        }
      }

      public Image m_teachingTodayRewardArmyIcon1
      {
        get
        {
          return this.m_owner.m_teachingTodayRewardArmyIcon1;
        }
        set
        {
          this.m_owner.m_teachingTodayRewardArmyIcon1 = value;
        }
      }

      public Image m_teachingTodayRewardArmyIcon2
      {
        get
        {
          return this.m_owner.m_teachingTodayRewardArmyIcon2;
        }
        set
        {
          this.m_owner.m_teachingTodayRewardArmyIcon2 = value;
        }
      }

      public Toggle m_trainingToggle
      {
        get
        {
          return this.m_owner.m_trainingToggle;
        }
        set
        {
          this.m_owner.m_trainingToggle = value;
        }
      }

      public Toggle m_teachingToggle
      {
        get
        {
          return this.m_owner.m_teachingToggle;
        }
        set
        {
          this.m_owner.m_teachingToggle = value;
        }
      }

      public GameObject m_teachingToggleClickRedMark
      {
        get
        {
          return this.m_owner.m_teachingToggleClickRedMark;
        }
        set
        {
          this.m_owner.m_teachingToggleClickRedMark = value;
        }
      }

      public GameObject m_teachingToggleUnClickRedMark
      {
        get
        {
          return this.m_owner.m_teachingToggleUnClickRedMark;
        }
        set
        {
          this.m_owner.m_teachingToggleUnClickRedMark = value;
        }
      }

      public Button m_soldierInfoButton
      {
        get
        {
          return this.m_owner.m_soldierInfoButton;
        }
        set
        {
          this.m_owner.m_soldierInfoButton = value;
        }
      }

      public CommonUIStateController m_stopTeachingPanelStateCtrl
      {
        get
        {
          return this.m_owner.m_stopTeachingPanelStateCtrl;
        }
        set
        {
          this.m_owner.m_stopTeachingPanelStateCtrl = value;
        }
      }

      public Button m_stopTeachingPanelBackButton
      {
        get
        {
          return this.m_owner.m_stopTeachingPanelBackButton;
        }
        set
        {
          this.m_owner.m_stopTeachingPanelBackButton = value;
        }
      }

      public Button m_stopTeachingPanelConfirmButton
      {
        get
        {
          return this.m_owner.m_stopTeachingPanelConfirmButton;
        }
        set
        {
          this.m_owner.m_stopTeachingPanelConfirmButton = value;
        }
      }

      public Button m_stopTeachingPanelCancelButton
      {
        get
        {
          return this.m_owner.m_stopTeachingPanelCancelButton;
        }
        set
        {
          this.m_owner.m_stopTeachingPanelCancelButton = value;
        }
      }

      public DrillUIController.DrillState m_curDrillState
      {
        get
        {
          return this.m_owner.m_curDrillState;
        }
        set
        {
          this.m_owner.m_curDrillState = value;
        }
      }

      public TrainingRoom m_curTrainingRoom
      {
        get
        {
          return this.m_owner.m_curTrainingRoom;
        }
        set
        {
          this.m_owner.m_curTrainingRoom = value;
        }
      }

      public List<AssistanceTeamUIController> m_assistanceTeamUICtrlList
      {
        get
        {
          return this.m_owner.m_assistanceTeamUICtrlList;
        }
        set
        {
          this.m_owner.m_assistanceTeamUICtrlList = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public int[] m_buffPropArr
      {
        get
        {
          return this.m_owner.m_buffPropArr;
        }
        set
        {
          this.m_owner.m_buffPropArr = value;
        }
      }

      public HeroCharUIController m_heroCharUIController
      {
        get
        {
          return this.m_owner.m_heroCharUIController;
        }
        set
        {
          this.m_owner.m_heroCharUIController = value;
        }
      }

      public AssistanceTeamUIController m_stopHeroAssistantsTaskCtrl
      {
        get
        {
          return this.m_owner.m_stopHeroAssistantsTaskCtrl;
        }
        set
        {
          this.m_owner.m_stopHeroAssistantsTaskCtrl = value;
        }
      }

      public List<CommonUIStateController> propStateCtrlList
      {
        get
        {
          return this.m_owner.propStateCtrlList;
        }
        set
        {
          this.m_owner.propStateCtrlList = value;
        }
      }

      public List<Text> propGoList
      {
        get
        {
          return this.m_owner.propGoList;
        }
        set
        {
          this.m_owner.propGoList = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void SetTrainingDetailPanel(TrainingRoom room)
      {
        this.m_owner.SetTrainingDetailPanel(room);
      }

      public void OnCourseItemClick(CourseItemUIController ctrl)
      {
        this.m_owner.OnCourseItemClick(ctrl);
      }

      public void OnRoomToggleClick(DrillRoomToggleUIController ctrl)
      {
        this.m_owner.OnRoomToggleClick(ctrl);
      }

      public void SetPropertyAddition()
      {
        this.m_owner.SetPropertyAddition();
      }

      public void ResetAllPropertyState()
      {
        this.m_owner.ResetAllPropertyState();
      }

      public void CalcSoldierPropertyModifityAddtion(PropertyModifyType type, int value)
      {
        this.m_owner.CalcSoldierPropertyModifityAddtion(type, value);
      }

      public void SetTeachingDetailPanel()
      {
        this.m_owner.SetTeachingDetailPanel();
      }

      public void OnAssistanceTeamTrainingButtonClick(int slot)
      {
        this.m_owner.OnAssistanceTeamTrainingButtonClick(slot);
      }

      public void OnAssistanceTeamStopButtonClick(AssistanceTeamUIController ctrl)
      {
        this.m_owner.OnAssistanceTeamStopButtonClick(ctrl);
      }

      public void OpenStopTeachingPanel()
      {
        this.m_owner.OpenStopTeachingPanel();
      }

      public void CloseStopTeachingPanel()
      {
        this.m_owner.CloseStopTeachingPanel();
      }

      public void OnStopTeachingPanelConfirmButtonClick()
      {
        this.m_owner.OnStopTeachingPanelConfirmButtonClick();
      }

      public void OnAssistanceTeamGetRewardButtonClick(AssistanceTeamUIController ctrl)
      {
        this.m_owner.OnAssistanceTeamGetRewardButtonClick(ctrl);
      }

      public void OnTrainingButtonClick()
      {
        this.m_owner.OnTrainingButtonClick();
      }

      public void OnSoldierInfoButtonClick()
      {
        this.m_owner.OnSoldierInfoButtonClick();
      }

      public void OnTrainingToggleValueChanged(bool isOn)
      {
        this.m_owner.OnTrainingToggleValueChanged(isOn);
      }

      public void OnTeachingToggleValueChanged(bool isOn)
      {
        this.m_owner.OnTeachingToggleValueChanged(isOn);
      }

      public void OnFastMaxButtonClick()
      {
        this.m_owner.OnFastMaxButtonClick();
      }

      public void ClearDataCache()
      {
        this.m_owner.ClearDataCache();
      }

      public void OnReturnButtonClick()
      {
        this.m_owner.OnReturnButtonClick();
      }

      public void OnHelpButtonClick()
      {
        this.m_owner.OnHelpButtonClick();
      }
    }
  }
}
