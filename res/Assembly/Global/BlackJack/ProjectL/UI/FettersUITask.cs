﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.FettersUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class FettersUITask : UITask
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private FettersUIController m_fettersUIController;
    private FettersFavorabilityUIController m_fettersFavorabilityUIController;
    private FettersInformationUIController m_fettersInformationUIController;
    private FettersConfessionUIController m_fettersConfessionUIController;
    public const string MatthewMode = "Matthew";
    public const string FavorabilityMode = "Favorability";
    public const string InformationMode = "Information";
    public const string GiftMode = "Gift";
    public const string ConfessionMode = "Confession";
    private string m_curMode;
    private string m_lastMode;
    private int m_curLayerDescIndex;
    private Hero m_hero;
    private int m_lastHeroId;
    private IConfigDataLoader m_configDataLoader;
    private ProjectLPlayerContext m_playerContext;
    private int m_nowSeconds;
    private bool m_isNeedShowFadeIn;
    [DoNotToLua]
    private FettersUITask.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctorString_hotfix;
    private LuaFunction m_OnStartUIIntent_hotfix;
    private LuaFunction m_OnResumeUIIntent_hotfix;
    private LuaFunction m_OnStop_hotfix;
    private LuaFunction m_SaveUIStateToIntent_hotfix;
    private LuaFunction m_GetUIStateFromIntentUIIntent_hotfix;
    private LuaFunction m_IsNeedLoadDynamicRes_hotfix;
    private LuaFunction m_InitLayerStateOnLoadAllResCompleted_hotfix;
    private LuaFunction m_IsNeedLoadStaticRes_hotfix;
    private LuaFunction m_CollectAllStaticResDescForLoad_hotfix;
    private LuaFunction m_InitAllUIControllers_hotfix;
    private LuaFunction m_ClearAllContextAndRes_hotfix;
    private LuaFunction m_OnMemoryWarning_hotfix;
    private LuaFunction m_UpdateView_hotfix;
    private LuaFunction m_PushAndPopLayerByState_hotfix;
    private LuaFunction m_FettersUIController_OnReturn_hotfix;
    private LuaFunction m_FettersUIController_OnShowHelp_hotfix;
    private LuaFunction m_FettersUIController_OnListItemClickHero_hotfix;
    private LuaFunction m_FettersUIController_OnInformationButtonClickHero_hotfix;
    private LuaFunction m_FettersUIController_OnFettersButtonClickHero_hotfix;
    private LuaFunction m_FettersFavorabilityUIController_OnReturn_hotfix;
    private LuaFunction m_FettersFavorabilityUIController_OnShowHelp_hotfix;
    private LuaFunction m_FettersFavorabilityUIController_OnInformationButtonClickHero_hotfix;
    private LuaFunction m_FettersFavorabilityUIController_OnGiftButtonClickHero_hotfix;
    private LuaFunction m_FettersFavorabilityUIController_OnUseGiftGoodsTypeInt32Int32Action`1_hotfix;
    private LuaFunction m_FettersFavorabilityUIController_GotoBagFullUITask_hotfix;
    private LuaFunction m_FettersFavorabilityUIController_OnHeroInteract_hotfix;
    private LuaFunction m_FettersFavorabilityUIController_OnMemoryButtonClickHero_hotfix;
    private LuaFunction m_FettersFavorabilityUIController_OnFettersButtonClickHero_hotfix;
    private LuaFunction m_FettersInformationUIController_OnReturn_hotfix;
    private LuaFunction m_FettersInformationUIController_OnVoiceItemClickInt32_hotfix;
    private LuaFunction m_FettersGiftUIController_OnReturn_hotfix;
    private LuaFunction m_FettersConfessionUIController_OnReturn_hotfix;
    private LuaFunction m_FettersConfessionUIControlle_OnShowHelp_hotfix;
    private LuaFunction m_FettersConfessionUIController_OnAddGold_hotfix;
    private LuaFunction m_FettersConfessionUIController_OnAddCrystal_hotfix;
    private LuaFunction m_FettersConfessionUIController_OnSkillUnlockButtonClickInt32Int32Action`1_hotfix;
    private LuaFunction m_FettersConfessionUIController_OnEvolutionButtonClickInt32Int32Action_hotfix;
    private LuaFunction m_FettersConfessionUIController_OnHeroFetterConfessInt32_hotfix;
    private LuaFunction m_FettersFavorabilityUIController_OnConfessionButtonClickHero_hotfix;
    private LuaFunction m_FettersConfessionUIController_OnUnlockCenterHeartFetterInt32Action_hotfix;
    private LuaFunction m_FettersConfessionUIController_OnEvolutionHeartFetterInt32Action_hotfix;
    private LuaFunction m_FettersConfessionUIController_OnEvolutionMaterialClickGoodsTypeInt32Int32_hotfix;
    private LuaFunction m_FettersConfessionUIController_OnGotoGetPathGetPathDataNeedGoods_hotfix;
    private LuaFunction m_OnTick_hotfix;
    private LuaFunction m_UpdateTouchTimeText_hotfix;
    private LuaFunction m_get_LayerDescArray_hotfix;
    private LuaFunction m_get_UICtrlDescArray_hotfix;
    private LuaFunction m_add_EventOnStartHeroDungeonAction`2_hotfix;
    private LuaFunction m_remove_EventOnStartHeroDungeonAction`2_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public FettersUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnResume(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SaveUIStateToIntent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GetUIStateFromIntent(UIIntent uiIntent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitLayerStateOnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadStaticRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override List<UITaskBase.LayerDesc> CollectAllStaticResDescForLoad()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnMemoryWarning()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PushAndPopLayerByState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FettersUIController_OnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FettersUIController_OnShowHelp()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FettersUIController_OnListItemClick(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FettersUIController_OnInformationButtonClick(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FettersUIController_OnFettersButtonClick(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FettersFavorabilityUIController_OnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FettersFavorabilityUIController_OnShowHelp()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FettersFavorabilityUIController_OnInformationButtonClick(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FettersFavorabilityUIController_OnGiftButtonClick(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FettersFavorabilityUIController_OnUseGift(
      GoodsType goodsType,
      int itemId,
      int count,
      Action<List<Goods>> Onsucceed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FettersFavorabilityUIController_GotoBagFullUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FettersFavorabilityUIController_OnHeroInteract()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FettersFavorabilityUIController_OnMemoryButtonClick(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FettersFavorabilityUIController_OnFettersButtonClick(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FettersInformationUIController_OnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FettersInformationUIController_OnVoiceItemClick(int heroPerformanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FettersGiftUIController_OnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FettersConfessionUIController_OnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FettersConfessionUIControlle_OnShowHelp()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FettersConfessionUIController_OnAddGold()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FettersConfessionUIController_OnAddCrystal()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FettersConfessionUIController_OnSkillUnlockButtonClick(
      int heroId,
      int fetterId,
      Action<List<Goods>> OnSucceed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FettersConfessionUIController_OnEvolutionButtonClick(
      int heroId,
      int fetterId,
      Action OnSucceed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FettersConfessionUIController_OnHeroFetterConfess(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FettersFavorabilityUIController_OnConfessionButtonClick(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FettersConfessionUIController_OnUnlockCenterHeartFetter(
      int heroId,
      Action OnSucceed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FettersConfessionUIController_OnEvolutionHeartFetter(int heroId, Action OnSucceed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FettersConfessionUIController_OnEvolutionMaterialClick(
      GoodsType goodsType,
      int goodsId,
      int needCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FettersConfessionUIController_OnGotoGetPath(
      GetPathData getPath,
      NeedGoods needGoods = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnTick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateTouchTimeText()
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<Hero, UIIntent> EventOnStartHeroDungeon
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public FettersUITask.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private bool __callBase_OnStart(UIIntent intent)
    {
      return base.OnStart(intent);
    }

    private void __callBase_OnStop()
    {
      base.OnStop();
    }

    private void __callBase_OnPause()
    {
      this.OnPause();
    }

    private bool __callBase_OnResume(UIIntent intent)
    {
      return base.OnResume(intent);
    }

    private bool __callBase_OnNewIntent(UIIntent intent)
    {
      return this.OnNewIntent(intent);
    }

    private void __callBase_EnableUIInput(bool isEnable, bool? isGlobalEnable)
    {
      this.EnableUIInput(isEnable, isGlobalEnable);
    }

    private bool __callBase_IsNeedLoadDynamicRes()
    {
      return base.IsNeedLoadDynamicRes();
    }

    private List<string> __callBase_CollectAllDynamicResForLoad()
    {
      return this.CollectAllDynamicResForLoad();
    }

    private void __callBase_RegisterPlayerContextEvents()
    {
      this.RegisterPlayerContextEvents();
    }

    private void __callBase_UnregisterPlayerContextEvents()
    {
      this.UnregisterPlayerContextEvents();
    }

    private void __callBase_PostUpdateView()
    {
      this.PostUpdateView();
    }

    private void __callBase_CollectPreloadResourceList()
    {
      this.CollectPreloadResourceList();
    }

    private void __callBase_ClearAssetList()
    {
      this.ClearAssetList();
    }

    private void __callBase_CollectAsset(string assetName)
    {
      this.CollectAsset(assetName);
    }

    private void __callBase_CollectSpriteAsset(string assetName)
    {
      this.CollectSpriteAsset(assetName);
    }

    private void __callBase_CollectFxAsset(string assetName)
    {
      this.CollectFxAsset(assetName);
    }

    private void __callBase_StartLoadCustomAssets(Action onLoadCompleted)
    {
      this.StartLoadCustomAssets(onLoadCompleted);
    }

    private bool __callBase_IsLoadingCustomAssets()
    {
      return this.IsLoadingCustomAssets();
    }

    private bool __callBase_IsPipeLineRunning()
    {
      return this.IsPipeLineRunning();
    }

    private bool __callBase_IsOpeningUI()
    {
      return this.IsOpeningUI();
    }

    private UITaskBase __callBase_ReturnPrevUITask()
    {
      return this.ReturnPrevUITask();
    }

    private void __callBase_ClearUnusedDynamicResourceCache()
    {
      this.ClearUnusedDynamicResourceCache();
    }

    private void __callBase_ClearDynamicResourceCache(List<string> assets)
    {
      this.ClearDynamicResourceCache(assets);
    }

    private void __callBase_OnMemoryWarning()
    {
      base.OnMemoryWarning();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnStartHeroDungeon(Hero arg1, UIIntent arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnStartHeroDungeon(Hero arg1, UIIntent arg2)
    {
      this.EventOnStartHeroDungeon = (Action<Hero, UIIntent>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private FettersUITask m_owner;

      public LuaExportHelper(FettersUITask owner)
      {
        this.m_owner = owner;
      }

      public bool __callBase_OnStart(UIIntent intent)
      {
        return this.m_owner.__callBase_OnStart(intent);
      }

      public void __callBase_OnStop()
      {
        this.m_owner.__callBase_OnStop();
      }

      public void __callBase_OnPause()
      {
        this.m_owner.__callBase_OnPause();
      }

      public bool __callBase_OnResume(UIIntent intent)
      {
        return this.m_owner.__callBase_OnResume(intent);
      }

      public bool __callBase_OnNewIntent(UIIntent intent)
      {
        return this.m_owner.__callBase_OnNewIntent(intent);
      }

      public void __callBase_EnableUIInput(bool isEnable, bool? isGlobalEnable)
      {
        this.m_owner.__callBase_EnableUIInput(isEnable, isGlobalEnable);
      }

      public bool __callBase_IsNeedLoadDynamicRes()
      {
        return this.m_owner.__callBase_IsNeedLoadDynamicRes();
      }

      public List<string> __callBase_CollectAllDynamicResForLoad()
      {
        return this.m_owner.__callBase_CollectAllDynamicResForLoad();
      }

      public void __callBase_RegisterPlayerContextEvents()
      {
        this.m_owner.__callBase_RegisterPlayerContextEvents();
      }

      public void __callBase_UnregisterPlayerContextEvents()
      {
        this.m_owner.__callBase_UnregisterPlayerContextEvents();
      }

      public void __callBase_PostUpdateView()
      {
        this.m_owner.__callBase_PostUpdateView();
      }

      public void __callBase_CollectPreloadResourceList()
      {
        this.m_owner.__callBase_CollectPreloadResourceList();
      }

      public void __callBase_ClearAssetList()
      {
        this.m_owner.__callBase_ClearAssetList();
      }

      public void __callBase_CollectAsset(string assetName)
      {
        this.m_owner.__callBase_CollectAsset(assetName);
      }

      public void __callBase_CollectSpriteAsset(string assetName)
      {
        this.m_owner.__callBase_CollectSpriteAsset(assetName);
      }

      public void __callBase_CollectFxAsset(string assetName)
      {
        this.m_owner.__callBase_CollectFxAsset(assetName);
      }

      public void __callBase_StartLoadCustomAssets(Action onLoadCompleted)
      {
        this.m_owner.__callBase_StartLoadCustomAssets(onLoadCompleted);
      }

      public bool __callBase_IsLoadingCustomAssets()
      {
        return this.m_owner.__callBase_IsLoadingCustomAssets();
      }

      public bool __callBase_IsPipeLineRunning()
      {
        return this.m_owner.__callBase_IsPipeLineRunning();
      }

      public bool __callBase_IsOpeningUI()
      {
        return this.m_owner.__callBase_IsOpeningUI();
      }

      public UITaskBase __callBase_ReturnPrevUITask()
      {
        return this.m_owner.__callBase_ReturnPrevUITask();
      }

      public void __callBase_ClearUnusedDynamicResourceCache()
      {
        this.m_owner.__callBase_ClearUnusedDynamicResourceCache();
      }

      public void __callBase_ClearDynamicResourceCache(List<string> assets)
      {
        this.m_owner.__callBase_ClearDynamicResourceCache(assets);
      }

      public void __callBase_OnMemoryWarning()
      {
        this.m_owner.__callBase_OnMemoryWarning();
      }

      public void __callDele_EventOnStartHeroDungeon(Hero arg1, UIIntent arg2)
      {
        this.m_owner.__callDele_EventOnStartHeroDungeon(arg1, arg2);
      }

      public void __clearDele_EventOnStartHeroDungeon(Hero arg1, UIIntent arg2)
      {
        this.m_owner.__clearDele_EventOnStartHeroDungeon(arg1, arg2);
      }

      public UITaskBase.LayerDesc[] m_layerDescArray
      {
        get
        {
          return this.m_owner.m_layerDescArray;
        }
        set
        {
          this.m_owner.m_layerDescArray = value;
        }
      }

      public UITaskBase.UIControllerDesc[] m_uiCtrlDescArray
      {
        get
        {
          return this.m_owner.m_uiCtrlDescArray;
        }
        set
        {
          this.m_owner.m_uiCtrlDescArray = value;
        }
      }

      public FettersUIController m_fettersUIController
      {
        get
        {
          return this.m_owner.m_fettersUIController;
        }
        set
        {
          this.m_owner.m_fettersUIController = value;
        }
      }

      public FettersFavorabilityUIController m_fettersFavorabilityUIController
      {
        get
        {
          return this.m_owner.m_fettersFavorabilityUIController;
        }
        set
        {
          this.m_owner.m_fettersFavorabilityUIController = value;
        }
      }

      public FettersInformationUIController m_fettersInformationUIController
      {
        get
        {
          return this.m_owner.m_fettersInformationUIController;
        }
        set
        {
          this.m_owner.m_fettersInformationUIController = value;
        }
      }

      public FettersConfessionUIController m_fettersConfessionUIController
      {
        get
        {
          return this.m_owner.m_fettersConfessionUIController;
        }
        set
        {
          this.m_owner.m_fettersConfessionUIController = value;
        }
      }

      public string m_curMode
      {
        get
        {
          return this.m_owner.m_curMode;
        }
        set
        {
          this.m_owner.m_curMode = value;
        }
      }

      public string m_lastMode
      {
        get
        {
          return this.m_owner.m_lastMode;
        }
        set
        {
          this.m_owner.m_lastMode = value;
        }
      }

      public int m_curLayerDescIndex
      {
        get
        {
          return this.m_owner.m_curLayerDescIndex;
        }
        set
        {
          this.m_owner.m_curLayerDescIndex = value;
        }
      }

      public Hero m_hero
      {
        get
        {
          return this.m_owner.m_hero;
        }
        set
        {
          this.m_owner.m_hero = value;
        }
      }

      public int m_lastHeroId
      {
        get
        {
          return this.m_owner.m_lastHeroId;
        }
        set
        {
          this.m_owner.m_lastHeroId = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public int m_nowSeconds
      {
        get
        {
          return this.m_owner.m_nowSeconds;
        }
        set
        {
          this.m_owner.m_nowSeconds = value;
        }
      }

      public bool m_isNeedShowFadeIn
      {
        get
        {
          return this.m_owner.m_isNeedShowFadeIn;
        }
        set
        {
          this.m_owner.m_isNeedShowFadeIn = value;
        }
      }

      public UITaskBase.LayerDesc[] LayerDescArray
      {
        get
        {
          return this.m_owner.LayerDescArray;
        }
      }

      public UITaskBase.UIControllerDesc[] UICtrlDescArray
      {
        get
        {
          return this.m_owner.UICtrlDescArray;
        }
      }

      public bool OnStart(UIIntent intent)
      {
        return this.m_owner.OnStart(intent);
      }

      public bool OnResume(UIIntent intent)
      {
        return this.m_owner.OnResume(intent);
      }

      public void OnStop()
      {
        this.m_owner.OnStop();
      }

      public void SaveUIStateToIntent()
      {
        this.m_owner.SaveUIStateToIntent();
      }

      public void GetUIStateFromIntent(UIIntent uiIntent)
      {
        this.m_owner.GetUIStateFromIntent(uiIntent);
      }

      public bool IsNeedLoadDynamicRes()
      {
        return this.m_owner.IsNeedLoadDynamicRes();
      }

      public void InitLayerStateOnLoadAllResCompleted()
      {
        this.m_owner.InitLayerStateOnLoadAllResCompleted();
      }

      public bool IsNeedLoadStaticRes()
      {
        return this.m_owner.IsNeedLoadStaticRes();
      }

      public List<UITaskBase.LayerDesc> CollectAllStaticResDescForLoad()
      {
        return this.m_owner.CollectAllStaticResDescForLoad();
      }

      public void InitAllUIControllers()
      {
        this.m_owner.InitAllUIControllers();
      }

      public void ClearAllContextAndRes()
      {
        this.m_owner.ClearAllContextAndRes();
      }

      public void OnMemoryWarning()
      {
        this.m_owner.OnMemoryWarning();
      }

      public void UpdateView()
      {
        this.m_owner.UpdateView();
      }

      public void PushAndPopLayerByState()
      {
        this.m_owner.PushAndPopLayerByState();
      }

      public void FettersUIController_OnReturn()
      {
        this.m_owner.FettersUIController_OnReturn();
      }

      public void FettersUIController_OnShowHelp()
      {
        this.m_owner.FettersUIController_OnShowHelp();
      }

      public void FettersUIController_OnListItemClick(Hero hero)
      {
        this.m_owner.FettersUIController_OnListItemClick(hero);
      }

      public void FettersUIController_OnInformationButtonClick(Hero hero)
      {
        this.m_owner.FettersUIController_OnInformationButtonClick(hero);
      }

      public void FettersUIController_OnFettersButtonClick(Hero hero)
      {
        this.m_owner.FettersUIController_OnFettersButtonClick(hero);
      }

      public void FettersFavorabilityUIController_OnReturn()
      {
        this.m_owner.FettersFavorabilityUIController_OnReturn();
      }

      public void FettersFavorabilityUIController_OnShowHelp()
      {
        this.m_owner.FettersFavorabilityUIController_OnShowHelp();
      }

      public void FettersFavorabilityUIController_OnInformationButtonClick(Hero hero)
      {
        this.m_owner.FettersFavorabilityUIController_OnInformationButtonClick(hero);
      }

      public void FettersFavorabilityUIController_OnGiftButtonClick(Hero hero)
      {
        this.m_owner.FettersFavorabilityUIController_OnGiftButtonClick(hero);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void FettersFavorabilityUIController_OnUseGift(
        GoodsType goodsType,
        int itemId,
        int count,
        Action<List<Goods>> Onsucceed)
      {
        // ISSUE: unable to decompile the method.
      }

      public void FettersFavorabilityUIController_GotoBagFullUITask()
      {
        this.m_owner.FettersFavorabilityUIController_GotoBagFullUITask();
      }

      public void FettersFavorabilityUIController_OnHeroInteract()
      {
        this.m_owner.FettersFavorabilityUIController_OnHeroInteract();
      }

      public void FettersFavorabilityUIController_OnMemoryButtonClick(Hero hero)
      {
        this.m_owner.FettersFavorabilityUIController_OnMemoryButtonClick(hero);
      }

      public void FettersFavorabilityUIController_OnFettersButtonClick(Hero hero)
      {
        this.m_owner.FettersFavorabilityUIController_OnFettersButtonClick(hero);
      }

      public void FettersInformationUIController_OnReturn()
      {
        this.m_owner.FettersInformationUIController_OnReturn();
      }

      public void FettersInformationUIController_OnVoiceItemClick(int heroPerformanceId)
      {
        this.m_owner.FettersInformationUIController_OnVoiceItemClick(heroPerformanceId);
      }

      public void FettersGiftUIController_OnReturn()
      {
        this.m_owner.FettersGiftUIController_OnReturn();
      }

      public void FettersConfessionUIController_OnReturn()
      {
        this.m_owner.FettersConfessionUIController_OnReturn();
      }

      public void FettersConfessionUIControlle_OnShowHelp()
      {
        this.m_owner.FettersConfessionUIControlle_OnShowHelp();
      }

      public void FettersConfessionUIController_OnAddGold()
      {
        this.m_owner.FettersConfessionUIController_OnAddGold();
      }

      public void FettersConfessionUIController_OnAddCrystal()
      {
        this.m_owner.FettersConfessionUIController_OnAddCrystal();
      }

      public void FettersConfessionUIController_OnSkillUnlockButtonClick(
        int heroId,
        int fetterId,
        Action<List<Goods>> OnSucceed)
      {
        this.m_owner.FettersConfessionUIController_OnSkillUnlockButtonClick(heroId, fetterId, OnSucceed);
      }

      public void FettersConfessionUIController_OnEvolutionButtonClick(
        int heroId,
        int fetterId,
        Action OnSucceed)
      {
        this.m_owner.FettersConfessionUIController_OnEvolutionButtonClick(heroId, fetterId, OnSucceed);
      }

      public void FettersConfessionUIController_OnHeroFetterConfess(int heroId)
      {
        this.m_owner.FettersConfessionUIController_OnHeroFetterConfess(heroId);
      }

      public void FettersFavorabilityUIController_OnConfessionButtonClick(Hero hero)
      {
        this.m_owner.FettersFavorabilityUIController_OnConfessionButtonClick(hero);
      }

      public void FettersConfessionUIController_OnUnlockCenterHeartFetter(
        int heroId,
        Action OnSucceed)
      {
        this.m_owner.FettersConfessionUIController_OnUnlockCenterHeartFetter(heroId, OnSucceed);
      }

      public void FettersConfessionUIController_OnEvolutionHeartFetter(int heroId, Action OnSucceed)
      {
        this.m_owner.FettersConfessionUIController_OnEvolutionHeartFetter(heroId, OnSucceed);
      }

      public void FettersConfessionUIController_OnEvolutionMaterialClick(
        GoodsType goodsType,
        int goodsId,
        int needCount)
      {
        this.m_owner.FettersConfessionUIController_OnEvolutionMaterialClick(goodsType, goodsId, needCount);
      }

      public void FettersConfessionUIController_OnGotoGetPath(
        GetPathData getPath,
        NeedGoods needGoods)
      {
        this.m_owner.FettersConfessionUIController_OnGotoGetPath(getPath, needGoods);
      }

      public void OnTick()
      {
        this.m_owner.OnTick();
      }

      public void UpdateTouchTimeText()
      {
        this.m_owner.UpdateTouchTimeText();
      }
    }
  }
}
