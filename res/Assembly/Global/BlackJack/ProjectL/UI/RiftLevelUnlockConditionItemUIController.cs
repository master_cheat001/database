﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.RiftLevelUnlockConditionItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class RiftLevelUnlockConditionItemUIController : UIControllerBase
  {
    [AutoBind("./Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_text;
    [AutoBind("./Button", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_goButton;
    private int m_riftLevelID;
    private int m_achievementRiftLevelID;
    private int m_scenarioID;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    [DoNotToLua]
    private RiftLevelUnlockConditionItemUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_SetConditionRiftLevelUnlockConditionTypeInt32Int32_hotfix;
    private LuaFunction m_GoScenario_hotfix;
    private LuaFunction m_GoAchievement_hotfix;
    private LuaFunction m_GoRiftLevel_hotfix;
    private LuaFunction m_add_EventOnGoToScenarioAction`1_hotfix;
    private LuaFunction m_remove_EventOnGoToScenarioAction`1_hotfix;
    private LuaFunction m_add_EventOnGoToRiftLevelAction`2_hotfix;
    private LuaFunction m_remove_EventOnGoToRiftLevelAction`2_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public RiftLevelUnlockConditionItemUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCondition(RiftLevelUnlockConditionType condition, int param1, int param2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GoScenario()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GoAchievement()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GoRiftLevel()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnGoToScenario
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int> EventOnGoToRiftLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public RiftLevelUnlockConditionItemUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      this.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnGoToScenario(int obj)
    {
    }

    private void __clearDele_EventOnGoToScenario(int obj)
    {
      this.EventOnGoToScenario = (Action<int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnGoToRiftLevel(int arg1, int arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnGoToRiftLevel(int arg1, int arg2)
    {
      this.EventOnGoToRiftLevel = (Action<int, int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private RiftLevelUnlockConditionItemUIController m_owner;

      public LuaExportHelper(RiftLevelUnlockConditionItemUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnGoToScenario(int obj)
      {
        this.m_owner.__callDele_EventOnGoToScenario(obj);
      }

      public void __clearDele_EventOnGoToScenario(int obj)
      {
        this.m_owner.__clearDele_EventOnGoToScenario(obj);
      }

      public void __callDele_EventOnGoToRiftLevel(int arg1, int arg2)
      {
        this.m_owner.__callDele_EventOnGoToRiftLevel(arg1, arg2);
      }

      public void __clearDele_EventOnGoToRiftLevel(int arg1, int arg2)
      {
        this.m_owner.__clearDele_EventOnGoToRiftLevel(arg1, arg2);
      }

      public Text m_text
      {
        get
        {
          return this.m_owner.m_text;
        }
        set
        {
          this.m_owner.m_text = value;
        }
      }

      public Button m_goButton
      {
        get
        {
          return this.m_owner.m_goButton;
        }
        set
        {
          this.m_owner.m_goButton = value;
        }
      }

      public int m_riftLevelID
      {
        get
        {
          return this.m_owner.m_riftLevelID;
        }
        set
        {
          this.m_owner.m_riftLevelID = value;
        }
      }

      public int m_achievementRiftLevelID
      {
        get
        {
          return this.m_owner.m_achievementRiftLevelID;
        }
        set
        {
          this.m_owner.m_achievementRiftLevelID = value;
        }
      }

      public int m_scenarioID
      {
        get
        {
          return this.m_owner.m_scenarioID;
        }
        set
        {
          this.m_owner.m_scenarioID = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public void GoScenario()
      {
        this.m_owner.GoScenario();
      }

      public void GoAchievement()
      {
        this.m_owner.GoAchievement();
      }

      public void GoRiftLevel()
      {
        this.m_owner.GoRiftLevel();
      }
    }
  }
}
