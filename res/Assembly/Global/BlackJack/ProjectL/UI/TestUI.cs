﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.TestUI
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.Battle;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Scene;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class TestUI : MonoBehaviour
  {
    private ClientBattle m_clientBattle;
    private ClientWorld m_clientWorld;
    private CollectionActivityWorld m_collectionActivityWorld;
    private int m_updateCount;
    private float m_fps;
    private float m_lastFpsResetTime;
    private float m_lastMemoryWarningTime;
    private float m_timeScale;
    private bool m_isTimePaused;
    private bool m_isShowTestToolToggle;
    private bool m_isShowTestToolBar;
    private bool m_isShowTouches;
    private bool m_isLowResolution;
    private bool m_isFrameRateLimit;
    private bool m_isShowTime;
    private bool m_isShowMemory;
    private bool m_isShowGMCommand;
    private int m_initScreenWidth;
    private int m_initScreenHeight;
    private string m_GMCommandStr;
    private int m_unit;
    private GUIStyle m_buttonStyle;
    private GUIStyle m_textFieldStyle;
    private GUIStyle m_textStyle;
    private GUIStyle m_textStyleSmall;
    private GUIStyle m_textStyleSmallCenter;
    [DoNotToLua]
    private TestUI.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_Awake_hotfix;
    private LuaFunction m_InitializeBattleClientBattle_hotfix;
    private LuaFunction m_UninitializeBattle_hotfix;
    private LuaFunction m_InitializeWorldClientWorld_hotfix;
    private LuaFunction m_UninitializeWorld_hotfix;
    private LuaFunction m_InitializeCollectionActivityWorldCollectionActivityWorld_hotfix;
    private LuaFunction m_UninitializeCollectionActivityWorld_hotfix;
    private LuaFunction m_SetTimeScaleSingle_hotfix;
    private LuaFunction m_ToolButtonInt32_Int32_SingleString_hotfix;
    private LuaFunction m_ToolToggleInt32_Int32_SingleStringBoolean_hotfix;
    private LuaFunction m_TextLineInt32Int32_String_hotfix;
    private LuaFunction m_GUIBattleTestTools_hotfix;
    private LuaFunction m_GUIWorldTestTools_hotfix;
    private LuaFunction m_GUIGMReloginButtonInt32_Int32__hotfix;
    private LuaFunction m_GUIToolToggleInt32_Int32__hotfix;
    private LuaFunction m_GUISpeedToggleInt32_Int32__hotfix;
    private LuaFunction m_GUIAudioToggleInt32_Int32__hotfix;
    private LuaFunction m_GUITouchToggleInt32_Int32__hotfix;
    private LuaFunction m_GUIMultiTouchToggleInt32_Int32__hotfix;
    private LuaFunction m_GUIResolutionToggleInt32_Int32__hotfix;
    private LuaFunction m_GUIFrameRateToggleInt32_Int32__hotfix;
    private LuaFunction m_GUITimeToggleInt32_Int32__hotfix;
    private LuaFunction m_GUIMemoryToggleInt32_Int32__hotfix;
    private LuaFunction m_GUILowMemoryTestInt32_Int32__hotfix;
    private LuaFunction m_GUIGMCommandToggleInt32_Int32__hotfix;
    private LuaFunction m_GUIGMCommandInt32_Int32__hotfix;
    private LuaFunction m_LogResources_hotfix;
    private LuaFunction m_GridPositionToScreenPositionGridPosition_hotfix;
    private LuaFunction m_GUIBattleActorBattleActor_hotfix;
    private LuaFunction m_GUIBattleActors_hotfix;
    private LuaFunction m_GUIBattleMap_hotfix;
    private LuaFunction m_GUITouch_hotfix;
    private LuaFunction m_CombatPositionToScreenPositionVector2i_hotfix;
    private LuaFunction m_GUICombatActorCombatActor_hotfix;
    private LuaFunction m_GUICombatActors_hotfix;
    private LuaFunction m_Update_hotfix;
    private LuaFunction m_TestMemoryWarnning_hotfix;
    private LuaFunction m_ComputeFps_hotfix;
    private LuaFunction m_UpdateTestUIBackground_hotfix;
    private LuaFunction m_OnGUI_hotfix;
    private LuaFunction m_InitStyleInt32_hotfix;
    private LuaFunction m_add_EventOnExitBattleAction_hotfix;
    private LuaFunction m_remove_EventOnExitBattleAction_hotfix;
    private LuaFunction m_add_EventOnRestartBattleAction_hotfix;
    private LuaFunction m_remove_EventOnRestartBattleAction_hotfix;
    private LuaFunction m_add_EventOnReplayBattleAction_hotfix;
    private LuaFunction m_remove_EventOnReplayBattleAction_hotfix;
    private LuaFunction m_add_EventOnPrepareBattleAction_hotfix;
    private LuaFunction m_remove_EventOnPrepareBattleAction_hotfix;
    private LuaFunction m_add_EventOnStopBattleAction`1_hotfix;
    private LuaFunction m_remove_EventOnStopBattleAction`1_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public TestUI()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Awake()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitializeBattle(ClientBattle clientBattle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UninitializeBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitializeWorld(ClientWorld clientWorld)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UninitializeWorld()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitializeCollectionActivityWorld(CollectionActivityWorld world)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UninitializeCollectionActivityWorld()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetTimeScale(float ts)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool ToolButton(ref int x, ref int y, float size, string text)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool ToolToggle(ref int x, ref int y, float size, string text, bool oldValue)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TextLine(int x, ref int y, string text)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GUIBattleTestTools()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GUIWorldTestTools()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GUIGMReloginButton(ref int x, ref int y)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GUIToolToggle(ref int x, ref int y)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GUISpeedToggle(ref int x, ref int y)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GUIAudioToggle(ref int x, ref int y)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GUITouchToggle(ref int x, ref int y)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GUIMultiTouchToggle(ref int x, ref int y)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GUIResolutionToggle(ref int x, ref int y)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GUIFrameRateToggle(ref int x, ref int y)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GUITimeToggle(ref int x, ref int y)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GUIMemoryToggle(ref int x, ref int y)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GUILowMemoryTest(ref int x, ref int y)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GUIGMCommandToggle(ref int x, ref int y)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GUIGMCommand(ref int x, ref int y)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LogResources()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int CompareObjectRuntimeMemroySize(
      TestUI.ObjectAndSize os1,
      TestUI.ObjectAndSize os2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Vector3 GridPositionToScreenPosition(GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GUIBattleActor(BattleActor actor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GUIBattleActors()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GUIBattleMap()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GUITouch()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Vector3 CombatPositionToScreenPosition(Vector2i p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GUICombatActor(CombatActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GUICombatActors()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TestMemoryWarnning()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ComputeFps()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateTestUIBackground()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGUI()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitStyle(int unit)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool PreProcessGMCommand(string cmd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void SendGMCommand(string cmd)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnExitBattle
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnRestartBattle
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnReplayBattle
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnPrepareBattle
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<bool> EventOnStopBattle
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public TestUI.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnExitBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnExitBattle()
    {
      this.EventOnExitBattle = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnRestartBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnRestartBattle()
    {
      this.EventOnRestartBattle = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnReplayBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnReplayBattle()
    {
      this.EventOnReplayBattle = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnPrepareBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnPrepareBattle()
    {
      this.EventOnPrepareBattle = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnStopBattle(bool obj)
    {
    }

    private void __clearDele_EventOnStopBattle(bool obj)
    {
      this.EventOnStopBattle = (Action<bool>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public struct ObjectAndSize
    {
      public UnityEngine.Object m_object;
      public int m_size;
    }

    public class LuaExportHelper
    {
      private TestUI m_owner;

      public LuaExportHelper(TestUI owner)
      {
        this.m_owner = owner;
      }

      public void __callDele_EventOnExitBattle()
      {
        this.m_owner.__callDele_EventOnExitBattle();
      }

      public void __clearDele_EventOnExitBattle()
      {
        this.m_owner.__clearDele_EventOnExitBattle();
      }

      public void __callDele_EventOnRestartBattle()
      {
        this.m_owner.__callDele_EventOnRestartBattle();
      }

      public void __clearDele_EventOnRestartBattle()
      {
        this.m_owner.__clearDele_EventOnRestartBattle();
      }

      public void __callDele_EventOnReplayBattle()
      {
        this.m_owner.__callDele_EventOnReplayBattle();
      }

      public void __clearDele_EventOnReplayBattle()
      {
        this.m_owner.__clearDele_EventOnReplayBattle();
      }

      public void __callDele_EventOnPrepareBattle()
      {
        this.m_owner.__callDele_EventOnPrepareBattle();
      }

      public void __clearDele_EventOnPrepareBattle()
      {
        this.m_owner.__clearDele_EventOnPrepareBattle();
      }

      public void __callDele_EventOnStopBattle(bool obj)
      {
        this.m_owner.__callDele_EventOnStopBattle(obj);
      }

      public void __clearDele_EventOnStopBattle(bool obj)
      {
        this.m_owner.__clearDele_EventOnStopBattle(obj);
      }

      public ClientBattle m_clientBattle
      {
        get
        {
          return this.m_owner.m_clientBattle;
        }
        set
        {
          this.m_owner.m_clientBattle = value;
        }
      }

      public ClientWorld m_clientWorld
      {
        get
        {
          return this.m_owner.m_clientWorld;
        }
        set
        {
          this.m_owner.m_clientWorld = value;
        }
      }

      public CollectionActivityWorld m_collectionActivityWorld
      {
        get
        {
          return this.m_owner.m_collectionActivityWorld;
        }
        set
        {
          this.m_owner.m_collectionActivityWorld = value;
        }
      }

      public int m_updateCount
      {
        get
        {
          return this.m_owner.m_updateCount;
        }
        set
        {
          this.m_owner.m_updateCount = value;
        }
      }

      public float m_fps
      {
        get
        {
          return this.m_owner.m_fps;
        }
        set
        {
          this.m_owner.m_fps = value;
        }
      }

      public float m_lastFpsResetTime
      {
        get
        {
          return this.m_owner.m_lastFpsResetTime;
        }
        set
        {
          this.m_owner.m_lastFpsResetTime = value;
        }
      }

      public float m_lastMemoryWarningTime
      {
        get
        {
          return this.m_owner.m_lastMemoryWarningTime;
        }
        set
        {
          this.m_owner.m_lastMemoryWarningTime = value;
        }
      }

      public float m_timeScale
      {
        get
        {
          return this.m_owner.m_timeScale;
        }
        set
        {
          this.m_owner.m_timeScale = value;
        }
      }

      public bool m_isTimePaused
      {
        get
        {
          return this.m_owner.m_isTimePaused;
        }
        set
        {
          this.m_owner.m_isTimePaused = value;
        }
      }

      public bool m_isShowTestToolToggle
      {
        get
        {
          return this.m_owner.m_isShowTestToolToggle;
        }
        set
        {
          this.m_owner.m_isShowTestToolToggle = value;
        }
      }

      public bool m_isShowTestToolBar
      {
        get
        {
          return this.m_owner.m_isShowTestToolBar;
        }
        set
        {
          this.m_owner.m_isShowTestToolBar = value;
        }
      }

      public bool m_isShowTouches
      {
        get
        {
          return this.m_owner.m_isShowTouches;
        }
        set
        {
          this.m_owner.m_isShowTouches = value;
        }
      }

      public bool m_isLowResolution
      {
        get
        {
          return this.m_owner.m_isLowResolution;
        }
        set
        {
          this.m_owner.m_isLowResolution = value;
        }
      }

      public bool m_isFrameRateLimit
      {
        get
        {
          return this.m_owner.m_isFrameRateLimit;
        }
        set
        {
          this.m_owner.m_isFrameRateLimit = value;
        }
      }

      public bool m_isShowTime
      {
        get
        {
          return this.m_owner.m_isShowTime;
        }
        set
        {
          this.m_owner.m_isShowTime = value;
        }
      }

      public bool m_isShowMemory
      {
        get
        {
          return this.m_owner.m_isShowMemory;
        }
        set
        {
          this.m_owner.m_isShowMemory = value;
        }
      }

      public bool m_isShowGMCommand
      {
        get
        {
          return this.m_owner.m_isShowGMCommand;
        }
        set
        {
          this.m_owner.m_isShowGMCommand = value;
        }
      }

      public int m_initScreenWidth
      {
        get
        {
          return this.m_owner.m_initScreenWidth;
        }
        set
        {
          this.m_owner.m_initScreenWidth = value;
        }
      }

      public int m_initScreenHeight
      {
        get
        {
          return this.m_owner.m_initScreenHeight;
        }
        set
        {
          this.m_owner.m_initScreenHeight = value;
        }
      }

      public string m_GMCommandStr
      {
        get
        {
          return this.m_owner.m_GMCommandStr;
        }
        set
        {
          this.m_owner.m_GMCommandStr = value;
        }
      }

      public int m_unit
      {
        get
        {
          return this.m_owner.m_unit;
        }
        set
        {
          this.m_owner.m_unit = value;
        }
      }

      public GUIStyle m_buttonStyle
      {
        get
        {
          return this.m_owner.m_buttonStyle;
        }
        set
        {
          this.m_owner.m_buttonStyle = value;
        }
      }

      public GUIStyle m_textFieldStyle
      {
        get
        {
          return this.m_owner.m_textFieldStyle;
        }
        set
        {
          this.m_owner.m_textFieldStyle = value;
        }
      }

      public GUIStyle m_textStyle
      {
        get
        {
          return this.m_owner.m_textStyle;
        }
        set
        {
          this.m_owner.m_textStyle = value;
        }
      }

      public GUIStyle m_textStyleSmall
      {
        get
        {
          return this.m_owner.m_textStyleSmall;
        }
        set
        {
          this.m_owner.m_textStyleSmall = value;
        }
      }

      public GUIStyle m_textStyleSmallCenter
      {
        get
        {
          return this.m_owner.m_textStyleSmallCenter;
        }
        set
        {
          this.m_owner.m_textStyleSmallCenter = value;
        }
      }

      public void Awake()
      {
        this.m_owner.Awake();
      }

      public void SetTimeScale(float ts)
      {
        this.m_owner.SetTimeScale(ts);
      }

      public bool ToolButton(ref int x, ref int y, float size, string text)
      {
        return this.m_owner.ToolButton(ref x, ref y, size, text);
      }

      public bool ToolToggle(ref int x, ref int y, float size, string text, bool oldValue)
      {
        return this.m_owner.ToolToggle(ref x, ref y, size, text, oldValue);
      }

      public void TextLine(int x, ref int y, string text)
      {
        this.m_owner.TextLine(x, ref y, text);
      }

      public void GUIBattleTestTools()
      {
        this.m_owner.GUIBattleTestTools();
      }

      public void GUIWorldTestTools()
      {
        this.m_owner.GUIWorldTestTools();
      }

      public void GUIGMReloginButton(ref int x, ref int y)
      {
        this.m_owner.GUIGMReloginButton(ref x, ref y);
      }

      public void GUIToolToggle(ref int x, ref int y)
      {
        this.m_owner.GUIToolToggle(ref x, ref y);
      }

      public void GUISpeedToggle(ref int x, ref int y)
      {
        this.m_owner.GUISpeedToggle(ref x, ref y);
      }

      public void GUIAudioToggle(ref int x, ref int y)
      {
        this.m_owner.GUIAudioToggle(ref x, ref y);
      }

      public void GUITouchToggle(ref int x, ref int y)
      {
        this.m_owner.GUITouchToggle(ref x, ref y);
      }

      public void GUIMultiTouchToggle(ref int x, ref int y)
      {
        this.m_owner.GUIMultiTouchToggle(ref x, ref y);
      }

      public void GUIResolutionToggle(ref int x, ref int y)
      {
        this.m_owner.GUIResolutionToggle(ref x, ref y);
      }

      public void GUIFrameRateToggle(ref int x, ref int y)
      {
        this.m_owner.GUIFrameRateToggle(ref x, ref y);
      }

      public void GUITimeToggle(ref int x, ref int y)
      {
        this.m_owner.GUITimeToggle(ref x, ref y);
      }

      public void GUIMemoryToggle(ref int x, ref int y)
      {
        this.m_owner.GUIMemoryToggle(ref x, ref y);
      }

      public void GUILowMemoryTest(ref int x, ref int y)
      {
        this.m_owner.GUILowMemoryTest(ref x, ref y);
      }

      public void GUIGMCommandToggle(ref int x, ref int y)
      {
        this.m_owner.GUIGMCommandToggle(ref x, ref y);
      }

      public void GUIGMCommand(ref int x, ref int y)
      {
        this.m_owner.GUIGMCommand(ref x, ref y);
      }

      public void LogResources()
      {
        this.m_owner.LogResources();
      }

      public static int CompareObjectRuntimeMemroySize(
        TestUI.ObjectAndSize os1,
        TestUI.ObjectAndSize os2)
      {
        return TestUI.CompareObjectRuntimeMemroySize(os1, os2);
      }

      public Vector3 GridPositionToScreenPosition(GridPosition p)
      {
        return this.m_owner.GridPositionToScreenPosition(p);
      }

      public void GUIBattleActor(BattleActor actor)
      {
        this.m_owner.GUIBattleActor(actor);
      }

      public void GUIBattleActors()
      {
        this.m_owner.GUIBattleActors();
      }

      public void GUIBattleMap()
      {
        this.m_owner.GUIBattleMap();
      }

      public void GUITouch()
      {
        this.m_owner.GUITouch();
      }

      public Vector3 CombatPositionToScreenPosition(Vector2i p)
      {
        return this.m_owner.CombatPositionToScreenPosition(p);
      }

      public void GUICombatActor(CombatActor a)
      {
        this.m_owner.GUICombatActor(a);
      }

      public void GUICombatActors()
      {
        this.m_owner.GUICombatActors();
      }

      public void Update()
      {
        this.m_owner.Update();
      }

      public void TestMemoryWarnning()
      {
        this.m_owner.TestMemoryWarnning();
      }

      public void ComputeFps()
      {
        this.m_owner.ComputeFps();
      }

      public void UpdateTestUIBackground()
      {
        this.m_owner.UpdateTestUIBackground();
      }

      public void OnGUI()
      {
        this.m_owner.OnGUI();
      }

      public void InitStyle(int unit)
      {
        this.m_owner.InitStyle(unit);
      }

      public static bool PreProcessGMCommand(string cmd)
      {
        return TestUI.PreProcessGMCommand(cmd);
      }
    }
  }
}
