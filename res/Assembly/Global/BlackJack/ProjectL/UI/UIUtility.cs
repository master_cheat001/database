﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.UIUtility
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using SLua;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class UIUtility
  {
    public const float PointerLongDownTime = 0.5f;
    public const float PointerClickTorrent = 0.02f;
    public const float PointerDragTorrent = 0.01f;
    private static Dictionary<string, string> m_translateTable;
    private const float MarginFixX = 88f;
    private const float MarginFixY = 40f;
    private static int s_DefaultPixelDragThreshold;
    private static int s_longFrameCountdown;
    private static float s_defaultMaximumDeltaTime;
    public static Color MyGreenColor;
    public static Color MyGrayColor;
    public const float MAX_DEBUG_REPORT_TIME = 10f;
    public const string STR_UPDATE_REPORT_DESC = "UploadLogByTouches";
    [DoNotToLua]
    private UIUtility.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public UIUtility()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static string Translate(string srcStr)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void LoadTranslateTable()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Vector2 WorldToLocalPosition(
      Vector3 p,
      Camera worldCam,
      RectTransform rt,
      Camera uiCam)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Vector2 ScreenToLocalPosition(Vector2 p, RectTransform rt, Camera uiCam)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string AddColorTag(string txt, Color c)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void ActivateLayer(UIControllerBase controller, bool a)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsLayerActive(UIControllerBase controller)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void ClickButton(UIControllerBase controller, string buttonName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void ClickToggle(UIControllerBase controller, string toggleName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string TimeSpanToString(TimeSpan timeSpan)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string TimeSpanToString2(TimeSpan timeSpan)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string TimeSpanToString3(TimeSpan timeSpan)
    {
      // ISSUE: unable to decompile the method.
    }

    public static string DateTimeToString(DateTime dateTime)
    {
      return dateTime.ToString("MM/dd/yyyy");
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string TimeSpanToString4(TimeSpan timeSpan)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string DateTimeToGMTString(DateTime dateTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void SetDailyRewardCount(Text text, int restCount, int allCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void SetDailyChallengeCount(Text text, int restCount, int allCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void ResetTween(GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void ReplayTween(GameObject go, Action onFinished = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void ReversePlayTween(GameObject go, Action onFinished = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool SetTweenFinished(TweenMain[] tweens, Action onFinished)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void RemoveTweenFinished(TweenMain[] tweens)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void SetTweenIgnoreTimeScale(GameObject go, bool ignore)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void SetUIStateOpen(
      CommonUIStateController ctrl,
      string stateName,
      Action onEnd = null,
      bool disableInput = false,
      bool allowToRefreshSameState = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void SetUIStateClose(
      CommonUIStateController ctrl,
      string stateName,
      Action onEnd = null,
      bool disableInput = false,
      bool allowToRefreshSameState = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void SetUIStateOpenAndClose(
      CommonUIStateController ctrl,
      string openStateName,
      string closeStateName,
      Action onEnd = null,
      bool disableInput = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void SetUIState(
      CommonUIStateController ctrl,
      string stateName,
      Action onEnd = null,
      bool disableInput = false,
      bool allowToRefreshSameState = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetUIStateCurrentStateName(CommonUIStateController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void PlayAnimation(GameObject go, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void ShowGameObjectChildrenByColor(GameObject obj, int num)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void SetGameObjectChildrenActiveCount(GameObject obj, int num)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void ReverseShowGameObjectChildrenByActive(GameObject obj, int num)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetHeroItemFrameNameByRank(int rank)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetGoodsFrameNameByRank(int rank)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetPropertyRatingImageName(char rating)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetGoodsName(GoodsType goodsType, int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int GetGoodsRank(GoodsType goodsType, int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetGoodsIconName(GoodsType goodsType, int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetGoodsIconMaterialName(GoodsType goodsType, int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetGoodsFrameName(GoodsType goodsType, int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsGoodsHeroFragment(GoodsType goodsType, int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsGoodsGoblin(GoodsType goodsType, int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetGoodsDesc(GoodsType goodsType, int goodsId = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetGuildTitleText(GuildTitle title)
    {
      // ISSUE: unable to decompile the method.
    }

    public static bool IsRankSSR(int rank)
    {
      if (rank != 4)
        return rank == 5;
      return true;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int GetBagItemAlchemyGold(BagItemBase bagItem)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int GetBagItemDropID(BagItemBase bagItem)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int GetBagItemDropDisplayCount(BagItemBase bagItem)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool GoodTypeHaveID(GoodsType goodsType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void AppendRandomDropRewardGoodsToList(int dropId, List<Goods> goods)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int GetGoodsCount(GoodsType goodsType, int goodsId = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int GetCurrencyCount(GoodsType currencyType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetPlayerHeadIconImageName(int headIconId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetPlayerSmallHeadIconImageName(int headIconId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetPlayerRoundHeadIconImageName(int headIconId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static ConfigDataCharImageInfo GetPlayerHeadIconCharImageInfo(
      int headIconId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetPlayerHeadFrameImageName(int headFrameId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static GameObject SetPlayerHeadFrame(
      Transform parent,
      int headFrameId,
      bool ingoreDefaultHeadFame = true,
      string uiState = "Normal")
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetBattlePlayerTagImageName(int playerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetSoldierCurSkillDesc(
      ConfigDataSoldierInfo soldierInfo,
      List<TrainingTech> techs)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetSoldierCurSkillDesc(ConfigDataSoldierInfo soldierInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int GetSkillIdFromEquipment(
      ConfigDataEquipmentInfo equipmentInfo,
      int equipmentLevel,
      ref string skillLv)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetRankImage(int rank)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetSoldierRankImage(int rank)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetHeroCharAssetPath(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetHeroModelAssetPath(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetSoldierModelAssetPath(Hero hero, int soldierId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetHeroModelAssetPath(BattleHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetSelectedSoldierModelAssetPath(BattleHero hero, int team = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ConfigDataCharImageInfo GetCharImageInfoRelatedToCharSkin(
      int charSkinId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ConfigDataCharImageInfo GetCharImageInfoRelatedToCharSkin(
      Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ConfigDataHeroInformationInfo GetHeroInformationInfoRelatedToCharSkin(
      int charSkinId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ConfigDataHeroInformationInfo GetHeroInformationInfoRelatedToCharSkin(
      Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetMemberOnlineText(bool isOnline, DateTime logountTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetSepAlphaTextureName(string colorTextureName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetGameFunctionOpenMessage(GameFunctionType gameFunctionType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetLeaderboardRankingImageName(int ranking)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetDescText(string format, List<string> args)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool MarginAdjustHorizontal(RectTransform rt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool MarginAdjustVertical(RectTransform rt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void SetLongFrame()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void CheckLongFrame()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public static IEnumerator TweenHorizontalScrollRecPosition(
      ScrollRect scrollRect,
      float from,
      float to,
      float duration)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static PDSDKGoodType GetGiftItemPdsdkGoodType(
      ConfigDataGiftStoreItemInfo giftInfo,
      bool isfristBuy)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static PDSDKGoodType GetGiftItemPdsdkGoodType(
      double firstPrice,
      double normalPrice,
      bool isfristBuy,
      bool IsAppleSubscribe)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int GetInputFieldCharacterLimit(int limit)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int GetRichTextLength(string richText)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static UITaskBase FindUITaskWithType(System.Type taskType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static T FindUITaskWithType<T>() where T : class
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsUITaskRunning(System.Type type)
    {
      // ISSUE: unable to decompile the method.
    }

    public static void SetDefaultPixelDragThreshold(int t)
    {
      UIUtility.s_DefaultPixelDragThreshold = t;
    }

    public static int GetDefaultPixelDragThreshold()
    {
      return UIUtility.s_DefaultPixelDragThreshold;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsIosSubscribe(ConfigDataGiftStoreItemInfo giftItemInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsIosSubscribe(bool isSubscribe)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsYYBChannel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsZiLongChannel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsOppoChannel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetUIPrefabPath<T>() where T : class
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static T CreateSubUI<T>(Transform parent) where T : class
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Share(int sharePlatform, int heroId = 0, int archiveId = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void LocalizedString(GameObject goRoot)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool HasVoicePermission()
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public UIUtility.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    static UIUtility()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private UIUtility m_owner;

      public LuaExportHelper(UIUtility owner)
      {
        this.m_owner = owner;
      }

      public static Dictionary<string, string> m_translateTable
      {
        get
        {
          return UIUtility.m_translateTable;
        }
        set
        {
          UIUtility.m_translateTable = value;
        }
      }

      public static float MarginFixX
      {
        get
        {
          return 88f;
        }
      }

      public static float MarginFixY
      {
        get
        {
          return 40f;
        }
      }

      public static int s_DefaultPixelDragThreshold
      {
        get
        {
          return UIUtility.s_DefaultPixelDragThreshold;
        }
        set
        {
          UIUtility.s_DefaultPixelDragThreshold = value;
        }
      }

      public static int s_longFrameCountdown
      {
        get
        {
          return UIUtility.s_longFrameCountdown;
        }
        set
        {
          UIUtility.s_longFrameCountdown = value;
        }
      }

      public static float s_defaultMaximumDeltaTime
      {
        get
        {
          return UIUtility.s_defaultMaximumDeltaTime;
        }
        set
        {
          UIUtility.s_defaultMaximumDeltaTime = value;
        }
      }

      public static string Translate(string srcStr)
      {
        return UIUtility.Translate(srcStr);
      }

      public static bool SetTweenFinished(TweenMain[] tweens, Action onFinished)
      {
        return UIUtility.SetTweenFinished(tweens, onFinished);
      }

      public static void RemoveTweenFinished(TweenMain[] tweens)
      {
        UIUtility.RemoveTweenFinished(tweens);
      }

      public static ConfigDataCharImageInfo GetPlayerHeadIconCharImageInfo(
        int headIconId)
      {
        return UIUtility.GetPlayerHeadIconCharImageInfo(headIconId);
      }
    }
  }
}
