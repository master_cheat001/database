﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.RaffleRewardItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class RaffleRewardItemUIController : UIControllerBase
  {
    protected List<RewardGoodsUIController> m_goodsCtrlList;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController UIStateCtrl;
    [AutoBind("./Detail/TitleGroup/Text", AutoBindAttribute.InitState.NotInit, false)]
    public Text LevelText;
    [AutoBind("./Detail/ItemContent", AutoBindAttribute.InitState.NotInit, false)]
    public Transform GoodsItemRoot;

    [MethodImpl((MethodImplOptions) 32768)]
    public RaffleRewardItemUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateRaffleRewardItemInfo(
      int level,
      List<RaffleItem> itemList,
      HashSet<int> drawedRaffleIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateRewardItemGotState(List<RaffleItem> itemList, HashSet<int> drawedRaffleIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetLvNameByLevel(int level)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected StringTableId GetLevelStrByLevel(int level)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected string GetLvStateNameByLevel(int level)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
