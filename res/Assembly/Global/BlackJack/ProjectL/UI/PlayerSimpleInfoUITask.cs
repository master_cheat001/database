﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PlayerSimpleInfoUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectLBasic;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class PlayerSimpleInfoUITask : UITask
  {
    private PlayerSimpleInfoUIController m_mainCtrl;
    private BusinessCard m_currInfo;
    private UIIntent m_returnUITaskIntent;
    private Vector3 m_panelPos;
    private PlayerSimpleInfoUITask.PostionType m_showPanelType;
    private string m_deleteFriendUserID;
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    public const string ModeParams_Stranger = "StrangerInfoMode";
    public const string ModeParams_Friend = "FriendInfoMode";
    public const string ModeParams_Guild = "GuildInfoMode";
    public const string Params_PanelPos = "PlayerSimpleInfoPanelPostion";
    public const string Params_PanelType = "PlayerSimpleInfoPanelType";
    public const string Params_ReturnToIntent = "PlayerSimpleInfoReturnToIntent";
    private List<string> m_friendAddList;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    [DoNotToLua]
    private PlayerSimpleInfoUITask.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctorString_hotfix;
    private LuaFunction m_InitlizeBeforeManagerStartIt_hotfix;
    private LuaFunction m_OnStartUIIntent_hotfix;
    private LuaFunction m_OnStop_hotfix;
    private LuaFunction m_OnResumeUIIntent_hotfix;
    private LuaFunction m_InitAllUIControllers_hotfix;
    private LuaFunction m_IsNeedUpdateDataCache_hotfix;
    private LuaFunction m_UpdateDataCache_hotfix;
    private LuaFunction m_UpdateView_hotfix;
    private LuaFunction m_IsInBattle_hotfix;
    private LuaFunction m_PlayerSimpleInfoUIController_OnWatchCardString_hotfix;
    private LuaFunction m_PlayerSimpleInfoUIController_OnPrivateChat_hotfix;
    private LuaFunction m_PlayerSimpleInfoUIController_OnPKString_hotfix;
    private LuaFunction m_PlayerSimpleInfoUIController_OnDeleteFriendBusinessCard_hotfix;
    private LuaFunction m_OnDeleteFreindDialogBoxCallbackDialogBoxResult_hotfix;
    private LuaFunction m_PlayerSimpleInfoUIController_OnAddFriendString_hotfix;
    private LuaFunction m_PlayerSimpleInfoUIController_OnBlockString_hotfix;
    private LuaFunction m_PlayerSimpleInfoUIController_OnLikeString_hotfix;
    private LuaFunction m_PlayerSimpleInfoUIController_OnReturn_hotfix;
    private LuaFunction m_PlayerSimpleInfoUIController_OnExpelString_hotfix;
    private LuaFunction m_PlayerSimpleInfoUIController_OnVChairmanChangeStringBoolean_hotfix;
    private LuaFunction m_PlayerSimpleInfoUIController_ChairmanMoveString_hotfix;
    private LuaFunction m_PlayerSimpleInfoUIController_OnChairmanRelieveString_hotfix;
    private LuaFunction m_ClosePanel_hotfix;
    private LuaFunction m_RegisterUIEvent_hotfix;
    private LuaFunction m_UnRegiterUIEvent_hotfix;
    private LuaFunction m_SetPanelPostion_hotfix;
    private LuaFunction m_get_LayerDescArray_hotfix;
    private LuaFunction m_get_UICtrlDescArray_hotfix;
    private LuaFunction m_add_EventOnCloseAction_hotfix;
    private LuaFunction m_remove_EventOnCloseAction_hotfix;
    private LuaFunction m_add_EventOnGetSocialRelationAction_hotfix;
    private LuaFunction m_remove_EventOnGetSocialRelationAction_hotfix;
    private LuaFunction m_add_EventOnPrivateChatButtonClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnPrivateChatButtonClickAction`1_hotfix;
    private LuaFunction m_add_EventOnBlockUserAction_hotfix;
    private LuaFunction m_remove_EventOnBlockUserAction_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public PlayerSimpleInfoUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void OnShowPlayerInfo(
      string userID,
      Vector3 pos,
      PlayerSimpleInfoUITask.PostionType postionType = PlayerSimpleInfoUITask.PostionType.Right,
      UIIntent returnIntent = null,
      bool isFromGuild = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static PlayerSimpleInfoUITask ShowPlayerSimpleInfoPanel(
      Vector3 pos,
      PlayerSimpleInfoUITask.PostionType postionType = PlayerSimpleInfoUITask.PostionType.Right,
      UIIntent returnIntent = null,
      bool isFromGuild = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void InitlizeBeforeManagerStartIt()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnResume(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedUpdateDataCache()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateDataCache()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsInBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerSimpleInfoUIController_OnWatchCard(string userID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerSimpleInfoUIController_OnPrivateChat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerSimpleInfoUIController_OnPK(string userID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerSimpleInfoUIController_OnDeleteFriend(BusinessCard userInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDeleteFreindDialogBoxCallback(DialogBoxResult r)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerSimpleInfoUIController_OnAddFriend(string userID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerSimpleInfoUIController_OnBlock(string userID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerSimpleInfoUIController_OnLike(string userID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerSimpleInfoUIController_OnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerSimpleInfoUIController_OnExpel(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerSimpleInfoUIController_OnVChairmanChange(string userId, bool isAppiont)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerSimpleInfoUIController_ChairmanMove(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerSimpleInfoUIController_OnChairmanRelieve(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClosePanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RegisterUIEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UnRegiterUIEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPanelPostion()
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnGetSocialRelation
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<BusinessCard> EventOnPrivateChatButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBlockUser
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public PlayerSimpleInfoUITask.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private bool __callBase_OnStart(UIIntent intent)
    {
      return base.OnStart(intent);
    }

    private void __callBase_OnStop()
    {
      base.OnStop();
    }

    private void __callBase_OnPause()
    {
      this.OnPause();
    }

    private bool __callBase_OnResume(UIIntent intent)
    {
      return base.OnResume(intent);
    }

    private bool __callBase_OnNewIntent(UIIntent intent)
    {
      return this.OnNewIntent(intent);
    }

    private void __callBase_EnableUIInput(bool isEnable, bool? isGlobalEnable)
    {
      this.EnableUIInput(isEnable, isGlobalEnable);
    }

    private bool __callBase_IsNeedLoadDynamicRes()
    {
      return this.IsNeedLoadDynamicRes();
    }

    private List<string> __callBase_CollectAllDynamicResForLoad()
    {
      return this.CollectAllDynamicResForLoad();
    }

    private void __callBase_RegisterPlayerContextEvents()
    {
      this.RegisterPlayerContextEvents();
    }

    private void __callBase_UnregisterPlayerContextEvents()
    {
      this.UnregisterPlayerContextEvents();
    }

    private void __callBase_PostUpdateView()
    {
      this.PostUpdateView();
    }

    private void __callBase_CollectPreloadResourceList()
    {
      this.CollectPreloadResourceList();
    }

    private void __callBase_ClearAssetList()
    {
      this.ClearAssetList();
    }

    private void __callBase_CollectAsset(string assetName)
    {
      this.CollectAsset(assetName);
    }

    private void __callBase_CollectSpriteAsset(string assetName)
    {
      this.CollectSpriteAsset(assetName);
    }

    private void __callBase_CollectFxAsset(string assetName)
    {
      this.CollectFxAsset(assetName);
    }

    private void __callBase_StartLoadCustomAssets(Action onLoadCompleted)
    {
      this.StartLoadCustomAssets(onLoadCompleted);
    }

    private bool __callBase_IsLoadingCustomAssets()
    {
      return this.IsLoadingCustomAssets();
    }

    private bool __callBase_IsPipeLineRunning()
    {
      return this.IsPipeLineRunning();
    }

    private bool __callBase_IsOpeningUI()
    {
      return this.IsOpeningUI();
    }

    private UITaskBase __callBase_ReturnPrevUITask()
    {
      return this.ReturnPrevUITask();
    }

    private void __callBase_ClearUnusedDynamicResourceCache()
    {
      this.ClearUnusedDynamicResourceCache();
    }

    private void __callBase_ClearDynamicResourceCache(List<string> assets)
    {
      this.ClearDynamicResourceCache(assets);
    }

    private void __callBase_OnMemoryWarning()
    {
      this.OnMemoryWarning();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnClose()
    {
      this.EventOnClose = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnGetSocialRelation()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnGetSocialRelation()
    {
      this.EventOnGetSocialRelation = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnPrivateChatButtonClick(BusinessCard obj)
    {
    }

    private void __clearDele_EventOnPrivateChatButtonClick(BusinessCard obj)
    {
      this.EventOnPrivateChatButtonClick = (Action<BusinessCard>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnBlockUser()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnBlockUser()
    {
      this.EventOnBlockUser = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public enum PostionType
    {
      UseInput,
      Left,
      Right,
    }

    public class LuaExportHelper
    {
      private PlayerSimpleInfoUITask m_owner;

      public LuaExportHelper(PlayerSimpleInfoUITask owner)
      {
        this.m_owner = owner;
      }

      public bool __callBase_OnStart(UIIntent intent)
      {
        return this.m_owner.__callBase_OnStart(intent);
      }

      public void __callBase_OnStop()
      {
        this.m_owner.__callBase_OnStop();
      }

      public void __callBase_OnPause()
      {
        this.m_owner.__callBase_OnPause();
      }

      public bool __callBase_OnResume(UIIntent intent)
      {
        return this.m_owner.__callBase_OnResume(intent);
      }

      public bool __callBase_OnNewIntent(UIIntent intent)
      {
        return this.m_owner.__callBase_OnNewIntent(intent);
      }

      public void __callBase_EnableUIInput(bool isEnable, bool? isGlobalEnable)
      {
        this.m_owner.__callBase_EnableUIInput(isEnable, isGlobalEnable);
      }

      public bool __callBase_IsNeedLoadDynamicRes()
      {
        return this.m_owner.__callBase_IsNeedLoadDynamicRes();
      }

      public List<string> __callBase_CollectAllDynamicResForLoad()
      {
        return this.m_owner.__callBase_CollectAllDynamicResForLoad();
      }

      public void __callBase_RegisterPlayerContextEvents()
      {
        this.m_owner.__callBase_RegisterPlayerContextEvents();
      }

      public void __callBase_UnregisterPlayerContextEvents()
      {
        this.m_owner.__callBase_UnregisterPlayerContextEvents();
      }

      public void __callBase_PostUpdateView()
      {
        this.m_owner.__callBase_PostUpdateView();
      }

      public void __callBase_CollectPreloadResourceList()
      {
        this.m_owner.__callBase_CollectPreloadResourceList();
      }

      public void __callBase_ClearAssetList()
      {
        this.m_owner.__callBase_ClearAssetList();
      }

      public void __callBase_CollectAsset(string assetName)
      {
        this.m_owner.__callBase_CollectAsset(assetName);
      }

      public void __callBase_CollectSpriteAsset(string assetName)
      {
        this.m_owner.__callBase_CollectSpriteAsset(assetName);
      }

      public void __callBase_CollectFxAsset(string assetName)
      {
        this.m_owner.__callBase_CollectFxAsset(assetName);
      }

      public void __callBase_StartLoadCustomAssets(Action onLoadCompleted)
      {
        this.m_owner.__callBase_StartLoadCustomAssets(onLoadCompleted);
      }

      public bool __callBase_IsLoadingCustomAssets()
      {
        return this.m_owner.__callBase_IsLoadingCustomAssets();
      }

      public bool __callBase_IsPipeLineRunning()
      {
        return this.m_owner.__callBase_IsPipeLineRunning();
      }

      public bool __callBase_IsOpeningUI()
      {
        return this.m_owner.__callBase_IsOpeningUI();
      }

      public UITaskBase __callBase_ReturnPrevUITask()
      {
        return this.m_owner.__callBase_ReturnPrevUITask();
      }

      public void __callBase_ClearUnusedDynamicResourceCache()
      {
        this.m_owner.__callBase_ClearUnusedDynamicResourceCache();
      }

      public void __callBase_ClearDynamicResourceCache(List<string> assets)
      {
        this.m_owner.__callBase_ClearDynamicResourceCache(assets);
      }

      public void __callBase_OnMemoryWarning()
      {
        this.m_owner.__callBase_OnMemoryWarning();
      }

      public void __callDele_EventOnClose()
      {
        this.m_owner.__callDele_EventOnClose();
      }

      public void __clearDele_EventOnClose()
      {
        this.m_owner.__clearDele_EventOnClose();
      }

      public void __callDele_EventOnGetSocialRelation()
      {
        this.m_owner.__callDele_EventOnGetSocialRelation();
      }

      public void __clearDele_EventOnGetSocialRelation()
      {
        this.m_owner.__clearDele_EventOnGetSocialRelation();
      }

      public void __callDele_EventOnPrivateChatButtonClick(BusinessCard obj)
      {
        this.m_owner.__callDele_EventOnPrivateChatButtonClick(obj);
      }

      public void __clearDele_EventOnPrivateChatButtonClick(BusinessCard obj)
      {
        this.m_owner.__clearDele_EventOnPrivateChatButtonClick(obj);
      }

      public void __callDele_EventOnBlockUser()
      {
        this.m_owner.__callDele_EventOnBlockUser();
      }

      public void __clearDele_EventOnBlockUser()
      {
        this.m_owner.__clearDele_EventOnBlockUser();
      }

      public PlayerSimpleInfoUIController m_mainCtrl
      {
        get
        {
          return this.m_owner.m_mainCtrl;
        }
        set
        {
          this.m_owner.m_mainCtrl = value;
        }
      }

      public BusinessCard m_currInfo
      {
        get
        {
          return this.m_owner.m_currInfo;
        }
        set
        {
          this.m_owner.m_currInfo = value;
        }
      }

      public UIIntent m_returnUITaskIntent
      {
        get
        {
          return this.m_owner.m_returnUITaskIntent;
        }
        set
        {
          this.m_owner.m_returnUITaskIntent = value;
        }
      }

      public Vector3 m_panelPos
      {
        get
        {
          return this.m_owner.m_panelPos;
        }
        set
        {
          this.m_owner.m_panelPos = value;
        }
      }

      public PlayerSimpleInfoUITask.PostionType m_showPanelType
      {
        get
        {
          return this.m_owner.m_showPanelType;
        }
        set
        {
          this.m_owner.m_showPanelType = value;
        }
      }

      public string m_deleteFriendUserID
      {
        get
        {
          return this.m_owner.m_deleteFriendUserID;
        }
        set
        {
          this.m_owner.m_deleteFriendUserID = value;
        }
      }

      public UITaskBase.LayerDesc[] m_layerDescArray
      {
        get
        {
          return this.m_owner.m_layerDescArray;
        }
        set
        {
          this.m_owner.m_layerDescArray = value;
        }
      }

      public UITaskBase.UIControllerDesc[] m_uiCtrlDescArray
      {
        get
        {
          return this.m_owner.m_uiCtrlDescArray;
        }
        set
        {
          this.m_owner.m_uiCtrlDescArray = value;
        }
      }

      public List<string> m_friendAddList
      {
        get
        {
          return this.m_owner.m_friendAddList;
        }
        set
        {
          this.m_owner.m_friendAddList = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public UITaskBase.LayerDesc[] LayerDescArray
      {
        get
        {
          return this.m_owner.LayerDescArray;
        }
      }

      public UITaskBase.UIControllerDesc[] UICtrlDescArray
      {
        get
        {
          return this.m_owner.UICtrlDescArray;
        }
      }

      public bool OnStart(UIIntent intent)
      {
        return this.m_owner.OnStart(intent);
      }

      public void OnStop()
      {
        this.m_owner.OnStop();
      }

      public bool OnResume(UIIntent intent)
      {
        return this.m_owner.OnResume(intent);
      }

      public void InitAllUIControllers()
      {
        this.m_owner.InitAllUIControllers();
      }

      public bool IsNeedUpdateDataCache()
      {
        return this.m_owner.IsNeedUpdateDataCache();
      }

      public void UpdateDataCache()
      {
        this.m_owner.UpdateDataCache();
      }

      public void UpdateView()
      {
        this.m_owner.UpdateView();
      }

      public bool IsInBattle()
      {
        return this.m_owner.IsInBattle();
      }

      public void PlayerSimpleInfoUIController_OnWatchCard(string userID)
      {
        this.m_owner.PlayerSimpleInfoUIController_OnWatchCard(userID);
      }

      public void PlayerSimpleInfoUIController_OnPrivateChat()
      {
        this.m_owner.PlayerSimpleInfoUIController_OnPrivateChat();
      }

      public void PlayerSimpleInfoUIController_OnPK(string userID)
      {
        this.m_owner.PlayerSimpleInfoUIController_OnPK(userID);
      }

      public void PlayerSimpleInfoUIController_OnDeleteFriend(BusinessCard userInfo)
      {
        this.m_owner.PlayerSimpleInfoUIController_OnDeleteFriend(userInfo);
      }

      public void OnDeleteFreindDialogBoxCallback(DialogBoxResult r)
      {
        this.m_owner.OnDeleteFreindDialogBoxCallback(r);
      }

      public void PlayerSimpleInfoUIController_OnAddFriend(string userID)
      {
        this.m_owner.PlayerSimpleInfoUIController_OnAddFriend(userID);
      }

      public void PlayerSimpleInfoUIController_OnBlock(string userID)
      {
        this.m_owner.PlayerSimpleInfoUIController_OnBlock(userID);
      }

      public void PlayerSimpleInfoUIController_OnLike(string userID)
      {
        this.m_owner.PlayerSimpleInfoUIController_OnLike(userID);
      }

      public void PlayerSimpleInfoUIController_OnReturn()
      {
        this.m_owner.PlayerSimpleInfoUIController_OnReturn();
      }

      public void PlayerSimpleInfoUIController_OnExpel(string userId)
      {
        this.m_owner.PlayerSimpleInfoUIController_OnExpel(userId);
      }

      public void PlayerSimpleInfoUIController_OnVChairmanChange(string userId, bool isAppiont)
      {
        this.m_owner.PlayerSimpleInfoUIController_OnVChairmanChange(userId, isAppiont);
      }

      public void PlayerSimpleInfoUIController_ChairmanMove(string userId)
      {
        this.m_owner.PlayerSimpleInfoUIController_ChairmanMove(userId);
      }

      public void PlayerSimpleInfoUIController_OnChairmanRelieve(string userId)
      {
        this.m_owner.PlayerSimpleInfoUIController_OnChairmanRelieve(userId);
      }

      public void ClosePanel()
      {
        this.m_owner.ClosePanel();
      }

      public void RegisterUIEvent()
      {
        this.m_owner.RegisterUIEvent();
      }

      public void UnRegiterUIEvent()
      {
        this.m_owner.UnRegiterUIEvent();
      }

      public void SetPanelPostion()
      {
        this.m_owner.SetPanelPostion();
      }
    }
  }
}
