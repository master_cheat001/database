﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.TarotUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectLBasic;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class TarotUIController : UIControllerBase
  {
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_tarotAnimation;
    [AutoBind("./BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_backgroundButton;
    [AutoBind("./Detail/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_closeButton;
    [AutoBind("./Detail/DescButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_descButton;
    [AutoBind("./Detail/WatchRewardButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_watchRewardButton;
    [AutoBind("./Detail/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_confirmButton;
    [AutoBind("./Detail/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_confirmButtonAnimation;
    [AutoBind("./Detail/TicketGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_ticketGroupAnimation;
    [AutoBind("./Detail/TicketGroup/IconImage/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_iconImage;
    [AutoBind("./Detail/TicketGroup/ConsumeTitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_consumeTitleText;
    [AutoBind("./Detail/TicketGroup/ConsumeValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_consumeValueText;
    [AutoBind("./Detail/TicketGroup/HaveValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_haveValueText;
    [AutoBind("./Detail/TicketGroup/FreeValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_freeValueText;
    [AutoBind("./Detail/CardGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_cardGroupAnimation;
    [AutoBind("./Detail/CardGroup/RotationPoint", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_cardGroupRotationAnimation;
    [AutoBind("./RewardListPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_rewardListAnimation;
    [AutoBind("./CardShowPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_cardShowAnimation;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private List<Toggle> m_cardList;
    private TarotUITask m_tarotUITask;
    private TarotRewardListPanelUIController m_tarotRewardListPanelUIController;
    private TarotCardShowPanelUIController m_tarotCardShowPanelUIController;
    private List<Goods> m_rewardGoods;
    private int m_poolID;
    [DoNotToLua]
    private TarotUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_InitInt32_hotfix;
    private LuaFunction m_RemainRaffleCount_hotfix;
    private LuaFunction m_IsCardSelected_hotfix;
    private LuaFunction m_IsSrBoxGoods_hotfix;
    private LuaFunction m_OpenSrBoxGoodsAction`1_hotfix;
    private LuaFunction m_Refresh_hotfix;
    private LuaFunction m_ShowCardInt32_hotfix;
    private LuaFunction m_ClearCardSelectedState_hotfix;
    private LuaFunction m_UpdateCostPanelRafflePool_hotfix;
    private LuaFunction m_ShowNotEnoughCrystalMsgBox_hotfix;
    private LuaFunction m_ShowNotEoughItemMsgBox_hotfix;
    private LuaFunction m_OnRechargeDialogResultDialogBoxResult_hotfix;
    private LuaFunction m_OnWatchRewardClick_hotfix;
    private LuaFunction m_OnCloseClick_hotfix;
    private LuaFunction m_OnBackgroundClick_hotfix;
    private LuaFunction m_OnConfirmClick_hotfix;
    private LuaFunction m_OnDescriptionClick_hotfix;
    private LuaFunction m_OnCardShowPanelCloseClick_hotfix;
    private LuaFunction m_OnRewardGoodsUIClose_hotfix;
    private LuaFunction m_OnCardSelect_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public TarotUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init(int poolID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int RemainRaffleCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsCardSelected()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsSrBox(Goods goods)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OpenSrBox(Goods goods, Action<List<Goods>> onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Refresh()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowCard(int displayCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearCardSelectedState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateCostPanel(RafflePool rafflePool)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void ShowNotEnoughCrystalMsgBox()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void ShowNotEoughItemMsgBox()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnRechargeDialogResult(DialogBoxResult result)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnWatchRewardClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBackgroundClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnConfirmClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDescriptionClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCardShowPanelCloseClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRewardGoodsUIClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCardSelect()
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public TarotUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private TarotUIController m_owner;

      public LuaExportHelper(TarotUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public CommonUIStateController m_tarotAnimation
      {
        get
        {
          return this.m_owner.m_tarotAnimation;
        }
        set
        {
          this.m_owner.m_tarotAnimation = value;
        }
      }

      public Button m_backgroundButton
      {
        get
        {
          return this.m_owner.m_backgroundButton;
        }
        set
        {
          this.m_owner.m_backgroundButton = value;
        }
      }

      public Button m_closeButton
      {
        get
        {
          return this.m_owner.m_closeButton;
        }
        set
        {
          this.m_owner.m_closeButton = value;
        }
      }

      public Button m_descButton
      {
        get
        {
          return this.m_owner.m_descButton;
        }
        set
        {
          this.m_owner.m_descButton = value;
        }
      }

      public Button m_watchRewardButton
      {
        get
        {
          return this.m_owner.m_watchRewardButton;
        }
        set
        {
          this.m_owner.m_watchRewardButton = value;
        }
      }

      public Button m_confirmButton
      {
        get
        {
          return this.m_owner.m_confirmButton;
        }
        set
        {
          this.m_owner.m_confirmButton = value;
        }
      }

      public CommonUIStateController m_confirmButtonAnimation
      {
        get
        {
          return this.m_owner.m_confirmButtonAnimation;
        }
        set
        {
          this.m_owner.m_confirmButtonAnimation = value;
        }
      }

      public CommonUIStateController m_ticketGroupAnimation
      {
        get
        {
          return this.m_owner.m_ticketGroupAnimation;
        }
        set
        {
          this.m_owner.m_ticketGroupAnimation = value;
        }
      }

      public Image m_iconImage
      {
        get
        {
          return this.m_owner.m_iconImage;
        }
        set
        {
          this.m_owner.m_iconImage = value;
        }
      }

      public Text m_consumeTitleText
      {
        get
        {
          return this.m_owner.m_consumeTitleText;
        }
        set
        {
          this.m_owner.m_consumeTitleText = value;
        }
      }

      public Text m_consumeValueText
      {
        get
        {
          return this.m_owner.m_consumeValueText;
        }
        set
        {
          this.m_owner.m_consumeValueText = value;
        }
      }

      public Text m_haveValueText
      {
        get
        {
          return this.m_owner.m_haveValueText;
        }
        set
        {
          this.m_owner.m_haveValueText = value;
        }
      }

      public Text m_freeValueText
      {
        get
        {
          return this.m_owner.m_freeValueText;
        }
        set
        {
          this.m_owner.m_freeValueText = value;
        }
      }

      public CommonUIStateController m_cardGroupAnimation
      {
        get
        {
          return this.m_owner.m_cardGroupAnimation;
        }
        set
        {
          this.m_owner.m_cardGroupAnimation = value;
        }
      }

      public CommonUIStateController m_cardGroupRotationAnimation
      {
        get
        {
          return this.m_owner.m_cardGroupRotationAnimation;
        }
        set
        {
          this.m_owner.m_cardGroupRotationAnimation = value;
        }
      }

      public CommonUIStateController m_rewardListAnimation
      {
        get
        {
          return this.m_owner.m_rewardListAnimation;
        }
        set
        {
          this.m_owner.m_rewardListAnimation = value;
        }
      }

      public CommonUIStateController m_cardShowAnimation
      {
        get
        {
          return this.m_owner.m_cardShowAnimation;
        }
        set
        {
          this.m_owner.m_cardShowAnimation = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public List<Toggle> m_cardList
      {
        get
        {
          return this.m_owner.m_cardList;
        }
        set
        {
          this.m_owner.m_cardList = value;
        }
      }

      public TarotUITask m_tarotUITask
      {
        get
        {
          return this.m_owner.m_tarotUITask;
        }
        set
        {
          this.m_owner.m_tarotUITask = value;
        }
      }

      public TarotRewardListPanelUIController m_tarotRewardListPanelUIController
      {
        get
        {
          return this.m_owner.m_tarotRewardListPanelUIController;
        }
        set
        {
          this.m_owner.m_tarotRewardListPanelUIController = value;
        }
      }

      public TarotCardShowPanelUIController m_tarotCardShowPanelUIController
      {
        get
        {
          return this.m_owner.m_tarotCardShowPanelUIController;
        }
        set
        {
          this.m_owner.m_tarotCardShowPanelUIController = value;
        }
      }

      public List<Goods> m_rewardGoods
      {
        get
        {
          return this.m_owner.m_rewardGoods;
        }
        set
        {
          this.m_owner.m_rewardGoods = value;
        }
      }

      public int m_poolID
      {
        get
        {
          return this.m_owner.m_poolID;
        }
        set
        {
          this.m_owner.m_poolID = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public int RemainRaffleCount()
      {
        return this.m_owner.RemainRaffleCount();
      }

      public bool IsCardSelected()
      {
        return this.m_owner.IsCardSelected();
      }

      public bool IsSrBox(Goods goods)
      {
        return this.m_owner.IsSrBox(goods);
      }

      public void OpenSrBox(Goods goods, Action<List<Goods>> onEnd)
      {
        this.m_owner.OpenSrBox(goods, onEnd);
      }

      public void ShowCard(int displayCount)
      {
        this.m_owner.ShowCard(displayCount);
      }

      public void ClearCardSelectedState()
      {
        this.m_owner.ClearCardSelectedState();
      }

      public void UpdateCostPanel(RafflePool rafflePool)
      {
        this.m_owner.UpdateCostPanel(rafflePool);
      }

      public void ShowNotEnoughCrystalMsgBox()
      {
        this.m_owner.ShowNotEnoughCrystalMsgBox();
      }

      public void ShowNotEoughItemMsgBox()
      {
        this.m_owner.ShowNotEoughItemMsgBox();
      }

      public void OnRechargeDialogResult(DialogBoxResult result)
      {
        this.m_owner.OnRechargeDialogResult(result);
      }

      public void OnWatchRewardClick()
      {
        this.m_owner.OnWatchRewardClick();
      }

      public void OnCloseClick()
      {
        this.m_owner.OnCloseClick();
      }

      public void OnBackgroundClick()
      {
        this.m_owner.OnBackgroundClick();
      }

      public void OnConfirmClick()
      {
        this.m_owner.OnConfirmClick();
      }

      public void OnDescriptionClick()
      {
        this.m_owner.OnDescriptionClick();
      }

      public void OnCardShowPanelCloseClick()
      {
        this.m_owner.OnCardShowPanelCloseClick();
      }

      public void OnRewardGoodsUIClose()
      {
        this.m_owner.OnRewardGoodsUIClose();
      }

      public void OnCardSelect()
      {
        this.m_owner.OnCardSelect();
      }
    }
  }
}
