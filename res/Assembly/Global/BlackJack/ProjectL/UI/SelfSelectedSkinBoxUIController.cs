﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.SelfSelectedSkinBoxUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.Scene;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class SelfSelectedSkinBoxUIController : UIControllerBase
  {
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_selfSelectedSkinBoxAnimation;
    [AutoBind("./FacelifPanel/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_closeButton;
    [AutoBind("./FacelifPanel/Detail/ConfirmPanel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_confirmButton;
    [AutoBind("./FacelifPanel/Detail/ConfirmPanel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_confirmButtonAnimation;
    [AutoBind("./FacelifPanel/Detail/Prefab/SkinItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skinItem;
    [AutoBind("./FacelifPanel/Detail/SkinGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skinGroup;
    [AutoBind("./FacelifPanel/Detail/ConfirmPanel/InfoGroup/CountGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_selectSkinCount;
    [AutoBind("./FacelifPanel/Detail/ConfirmPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_skinPanelAnimation;
    [AutoBind("./FacelifPanel/Detail/DescText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descText;
    [AutoBind("./FacelifPanel/Detail/ConfirmPanel/InfoGroup/CountGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_selectCountText;
    [AutoBind("./FacelifPanel/Detail/ConfirmPanel/BottomText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_skinBoxDescText;
    [AutoBind("./FacelifPanel/Detail/InfoPanel/JobScrollView/Viewport/JobGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobGroupDummy;
    [AutoBind("./FacelifPanel/Detail/InfoPanel/Char", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_smallHeroDummy;
    [AutoBind("./FacelifPanel/Char/0", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_bigHeroDummy;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private SelfSelectedSkinBoxUITask.SelfSelectedSkinBoxMode m_selfSelectedSkinBoxMode;
    private UISpineGraphic m_smallHeroGraphic;
    private UISpineGraphic m_bigHeroGraphic;
    private List<SelfSelectedSkinBoxItemUIController> m_skinItemUIControllerList;
    private SelfSelectedSkinBoxUITask m_selfSelectedSkinBoxUITask;
    private ConfigDataItemInfo m_itemInfo;
    [DoNotToLua]
    private SelfSelectedSkinBoxUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_InitSelfSelectedSkinBoxUITaskInt32_hotfix;
    private LuaFunction m_SetViewMode_hotfix;
    private LuaFunction m_ShowHeroSkinInt32_hotfix;
    private LuaFunction m_RefreshInt32_hotfix;
    private LuaFunction m_RefreshConfirmState_hotfix;
    private LuaFunction m_SetSubItemNoneState_hotfix;
    private LuaFunction m_SetSubItemSelectStete_hotfix;
    private LuaFunction m_OnCloseClick_hotfix;
    private LuaFunction m_OnConfirmClick_hotfix;
    private LuaFunction m_OnSkinItemStateChange_hotfix;
    private LuaFunction m_OnItemToggleSelectInt32_hotfix;
    private LuaFunction m_add_EventOnConfirmClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnConfirmClickAction`1_hotfix;
    private LuaFunction m_add_EventOnCancelClickAction_hotfix;
    private LuaFunction m_remove_EventOnCancelClickAction_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public SelfSelectedSkinBoxUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init(
      SelfSelectedSkinBoxUITask selfSelectedSkinBoxUITask,
      int selfSelectedSkinBoxID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetViewMode()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowHeroSkin(int heroSkinID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Refresh(int heroSkinID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RefreshConfirmState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSubItemNoneState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSubItemSelectStete()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnConfirmClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSkinItemStateChange()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnItemToggleSelect(int heroSkinID)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<List<int>> EventOnConfirmClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnCancelClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public SelfSelectedSkinBoxUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnConfirmClick(List<int> obj)
    {
    }

    private void __clearDele_EventOnConfirmClick(List<int> obj)
    {
      this.EventOnConfirmClick = (Action<List<int>>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnCancelClick()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnCancelClick()
    {
      this.EventOnCancelClick = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private SelfSelectedSkinBoxUIController m_owner;

      public LuaExportHelper(SelfSelectedSkinBoxUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnConfirmClick(List<int> obj)
      {
        this.m_owner.__callDele_EventOnConfirmClick(obj);
      }

      public void __clearDele_EventOnConfirmClick(List<int> obj)
      {
        this.m_owner.__clearDele_EventOnConfirmClick(obj);
      }

      public void __callDele_EventOnCancelClick()
      {
        this.m_owner.__callDele_EventOnCancelClick();
      }

      public void __clearDele_EventOnCancelClick()
      {
        this.m_owner.__clearDele_EventOnCancelClick();
      }

      public CommonUIStateController m_selfSelectedSkinBoxAnimation
      {
        get
        {
          return this.m_owner.m_selfSelectedSkinBoxAnimation;
        }
        set
        {
          this.m_owner.m_selfSelectedSkinBoxAnimation = value;
        }
      }

      public Button m_closeButton
      {
        get
        {
          return this.m_owner.m_closeButton;
        }
        set
        {
          this.m_owner.m_closeButton = value;
        }
      }

      public Button m_confirmButton
      {
        get
        {
          return this.m_owner.m_confirmButton;
        }
        set
        {
          this.m_owner.m_confirmButton = value;
        }
      }

      public CommonUIStateController m_confirmButtonAnimation
      {
        get
        {
          return this.m_owner.m_confirmButtonAnimation;
        }
        set
        {
          this.m_owner.m_confirmButtonAnimation = value;
        }
      }

      public GameObject m_skinItem
      {
        get
        {
          return this.m_owner.m_skinItem;
        }
        set
        {
          this.m_owner.m_skinItem = value;
        }
      }

      public GameObject m_skinGroup
      {
        get
        {
          return this.m_owner.m_skinGroup;
        }
        set
        {
          this.m_owner.m_skinGroup = value;
        }
      }

      public Text m_selectSkinCount
      {
        get
        {
          return this.m_owner.m_selectSkinCount;
        }
        set
        {
          this.m_owner.m_selectSkinCount = value;
        }
      }

      public CommonUIStateController m_skinPanelAnimation
      {
        get
        {
          return this.m_owner.m_skinPanelAnimation;
        }
        set
        {
          this.m_owner.m_skinPanelAnimation = value;
        }
      }

      public Text m_descText
      {
        get
        {
          return this.m_owner.m_descText;
        }
        set
        {
          this.m_owner.m_descText = value;
        }
      }

      public Text m_selectCountText
      {
        get
        {
          return this.m_owner.m_selectCountText;
        }
        set
        {
          this.m_owner.m_selectCountText = value;
        }
      }

      public Text m_skinBoxDescText
      {
        get
        {
          return this.m_owner.m_skinBoxDescText;
        }
        set
        {
          this.m_owner.m_skinBoxDescText = value;
        }
      }

      public GameObject m_jobGroupDummy
      {
        get
        {
          return this.m_owner.m_jobGroupDummy;
        }
        set
        {
          this.m_owner.m_jobGroupDummy = value;
        }
      }

      public GameObject m_smallHeroDummy
      {
        get
        {
          return this.m_owner.m_smallHeroDummy;
        }
        set
        {
          this.m_owner.m_smallHeroDummy = value;
        }
      }

      public GameObject m_bigHeroDummy
      {
        get
        {
          return this.m_owner.m_bigHeroDummy;
        }
        set
        {
          this.m_owner.m_bigHeroDummy = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public SelfSelectedSkinBoxUITask.SelfSelectedSkinBoxMode m_selfSelectedSkinBoxMode
      {
        get
        {
          return this.m_owner.m_selfSelectedSkinBoxMode;
        }
        set
        {
          this.m_owner.m_selfSelectedSkinBoxMode = value;
        }
      }

      public UISpineGraphic m_smallHeroGraphic
      {
        get
        {
          return this.m_owner.m_smallHeroGraphic;
        }
        set
        {
          this.m_owner.m_smallHeroGraphic = value;
        }
      }

      public UISpineGraphic m_bigHeroGraphic
      {
        get
        {
          return this.m_owner.m_bigHeroGraphic;
        }
        set
        {
          this.m_owner.m_bigHeroGraphic = value;
        }
      }

      public List<SelfSelectedSkinBoxItemUIController> m_skinItemUIControllerList
      {
        get
        {
          return this.m_owner.m_skinItemUIControllerList;
        }
        set
        {
          this.m_owner.m_skinItemUIControllerList = value;
        }
      }

      public SelfSelectedSkinBoxUITask m_selfSelectedSkinBoxUITask
      {
        get
        {
          return this.m_owner.m_selfSelectedSkinBoxUITask;
        }
        set
        {
          this.m_owner.m_selfSelectedSkinBoxUITask = value;
        }
      }

      public ConfigDataItemInfo m_itemInfo
      {
        get
        {
          return this.m_owner.m_itemInfo;
        }
        set
        {
          this.m_owner.m_itemInfo = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void ShowHeroSkin(int heroSkinID)
      {
        this.m_owner.ShowHeroSkin(heroSkinID);
      }

      public void Refresh(int heroSkinID)
      {
        this.m_owner.Refresh(heroSkinID);
      }

      public void RefreshConfirmState()
      {
        this.m_owner.RefreshConfirmState();
      }

      public void SetSubItemNoneState()
      {
        this.m_owner.SetSubItemNoneState();
      }

      public void SetSubItemSelectStete()
      {
        this.m_owner.SetSubItemSelectStete();
      }

      public void OnCloseClick()
      {
        this.m_owner.OnCloseClick();
      }

      public void OnConfirmClick()
      {
        this.m_owner.OnConfirmClick();
      }

      public void OnSkinItemStateChange()
      {
        this.m_owner.OnSkinItemStateChange();
      }

      public void OnItemToggleSelect(int heroSkinID)
      {
        this.m_owner.OnItemToggleSelect(heroSkinID);
      }
    }
  }
}
