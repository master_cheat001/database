﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.RecommendGiftBoxUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Misc;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class RecommendGiftBoxUIController : UIControllerBase
  {
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiAnimation;
    [AutoBind("./BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_bgButton;
    [AutoBind("./Detail", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_detailGameObject;
    [AutoBind("./Detail/PackageDetail/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_closeButton;
    [AutoBind("./Detail/ToggleGroupScrollView/Viewport/ToggleGroup", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_toggleGroup;
    [AutoBind("./Detail/ToggleGroupScrollView/Viewport/ToggleGroup/PackageToggle", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_packagePrefab;
    [AutoBind("./Detail/PackageDetail/ActiveImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_activeImage;
    [AutoBind("./Detail/PackageDetail/ActiveImage/DetailText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_giftBoxName;
    [AutoBind("./Detail/PackageDetail/ActiveImage/NoticText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_giftBoxDesc;
    [AutoBind("./Detail/PackageDetail/ActiveTime", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_activeTimeAnimation;
    [AutoBind("./Detail/PackageDetail/ActiveTime/TimeValue", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_activiTimeText;
    [AutoBind("./Detail/PackageDetail/BuyTimes", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_buyTimesAnimation;
    [AutoBind("./Detail/PackageDetail/BuyTimes", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_buyTimesGameObject;
    [AutoBind("./Detail/PackageDetail/BuyTimes/TimeValue", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_buyTimesText;
    [AutoBind("./Detail/PackageDetail/ValueOffImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_saleOffGameObject;
    [AutoBind("./Detail/PackageDetail/ValueOffImage/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_saleOffText;
    [AutoBind("./Detail/PackageDetail/Button", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_buyButton;
    [AutoBind("./Detail/PackageDetail/Button/IconImage/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_buyButtonIcon;
    [AutoBind("./Detail/PackageDetail/Button/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_buyButtonText;
    [AutoBind("./Detail/PackageDetail/ItemGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_itemGroupGameObject;
    [AutoBind("./Detail/PackageDetail/ItemGroup/ItemScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_itemScrollView;
    [AutoBind("./Detail/PackageDetail/ItemGroup/ItemScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_itemContent;
    [AutoBind("./Prefab/PackageItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_ItemPrefab;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private RecommendGiftBoxUITask m_recommendGiftBoxUITask;
    private RecommendGiftBoxToggleUIController m_selectToggleUIController;
    private List<RecommendGiftBoxToggleUIController> m_toggleUIControllerList;
    private ItemPay m_itemPay;
    private GameObjectPool<RecommendGiftBoxItemUIController> m_ItemPool;
    private bool m_isProcessingPaySuccess;
    [DoNotToLua]
    private RecommendGiftBoxUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_InitList`1_hotfix;
    private LuaFunction m_SortGiftStoreItemGiftStoreItem_hotfix;
    private LuaFunction m_UpdateView_hotfix;
    private LuaFunction m_UpdateViewToggleList_hotfix;
    private LuaFunction m_OnGiftBoxClickRecommendGiftBoxToggleUIController_hotfix;
    private LuaFunction m_OnCloseClick_hotfix;
    private LuaFunction m_OnBuyClick_hotfix;
    private LuaFunction m_OnPaySuccess_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public RecommendGiftBoxUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Init(List<PDSDKGood> pdSDKGoods)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int Sort(GiftStoreItem itemA, GiftStoreItem itemB)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateViewToggleList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGiftBoxClick(
      RecommendGiftBoxToggleUIController recommendGiftBoxItemUIController)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBuyClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPaySuccess()
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public RecommendGiftBoxUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private RecommendGiftBoxUIController m_owner;

      public LuaExportHelper(RecommendGiftBoxUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public CommonUIStateController m_uiAnimation
      {
        get
        {
          return this.m_owner.m_uiAnimation;
        }
        set
        {
          this.m_owner.m_uiAnimation = value;
        }
      }

      public Button m_bgButton
      {
        get
        {
          return this.m_owner.m_bgButton;
        }
        set
        {
          this.m_owner.m_bgButton = value;
        }
      }

      public GameObject m_detailGameObject
      {
        get
        {
          return this.m_owner.m_detailGameObject;
        }
        set
        {
          this.m_owner.m_detailGameObject = value;
        }
      }

      public Button m_closeButton
      {
        get
        {
          return this.m_owner.m_closeButton;
        }
        set
        {
          this.m_owner.m_closeButton = value;
        }
      }

      public Transform m_toggleGroup
      {
        get
        {
          return this.m_owner.m_toggleGroup;
        }
        set
        {
          this.m_owner.m_toggleGroup = value;
        }
      }

      public GameObject m_packagePrefab
      {
        get
        {
          return this.m_owner.m_packagePrefab;
        }
        set
        {
          this.m_owner.m_packagePrefab = value;
        }
      }

      public Image m_activeImage
      {
        get
        {
          return this.m_owner.m_activeImage;
        }
        set
        {
          this.m_owner.m_activeImage = value;
        }
      }

      public Text m_giftBoxName
      {
        get
        {
          return this.m_owner.m_giftBoxName;
        }
        set
        {
          this.m_owner.m_giftBoxName = value;
        }
      }

      public Text m_giftBoxDesc
      {
        get
        {
          return this.m_owner.m_giftBoxDesc;
        }
        set
        {
          this.m_owner.m_giftBoxDesc = value;
        }
      }

      public CommonUIStateController m_activeTimeAnimation
      {
        get
        {
          return this.m_owner.m_activeTimeAnimation;
        }
        set
        {
          this.m_owner.m_activeTimeAnimation = value;
        }
      }

      public Text m_activiTimeText
      {
        get
        {
          return this.m_owner.m_activiTimeText;
        }
        set
        {
          this.m_owner.m_activiTimeText = value;
        }
      }

      public CommonUIStateController m_buyTimesAnimation
      {
        get
        {
          return this.m_owner.m_buyTimesAnimation;
        }
        set
        {
          this.m_owner.m_buyTimesAnimation = value;
        }
      }

      public GameObject m_buyTimesGameObject
      {
        get
        {
          return this.m_owner.m_buyTimesGameObject;
        }
        set
        {
          this.m_owner.m_buyTimesGameObject = value;
        }
      }

      public Text m_buyTimesText
      {
        get
        {
          return this.m_owner.m_buyTimesText;
        }
        set
        {
          this.m_owner.m_buyTimesText = value;
        }
      }

      public GameObject m_saleOffGameObject
      {
        get
        {
          return this.m_owner.m_saleOffGameObject;
        }
        set
        {
          this.m_owner.m_saleOffGameObject = value;
        }
      }

      public Text m_saleOffText
      {
        get
        {
          return this.m_owner.m_saleOffText;
        }
        set
        {
          this.m_owner.m_saleOffText = value;
        }
      }

      public Button m_buyButton
      {
        get
        {
          return this.m_owner.m_buyButton;
        }
        set
        {
          this.m_owner.m_buyButton = value;
        }
      }

      public Image m_buyButtonIcon
      {
        get
        {
          return this.m_owner.m_buyButtonIcon;
        }
        set
        {
          this.m_owner.m_buyButtonIcon = value;
        }
      }

      public Text m_buyButtonText
      {
        get
        {
          return this.m_owner.m_buyButtonText;
        }
        set
        {
          this.m_owner.m_buyButtonText = value;
        }
      }

      public GameObject m_itemGroupGameObject
      {
        get
        {
          return this.m_owner.m_itemGroupGameObject;
        }
        set
        {
          this.m_owner.m_itemGroupGameObject = value;
        }
      }

      public ScrollRect m_itemScrollView
      {
        get
        {
          return this.m_owner.m_itemScrollView;
        }
        set
        {
          this.m_owner.m_itemScrollView = value;
        }
      }

      public Transform m_itemContent
      {
        get
        {
          return this.m_owner.m_itemContent;
        }
        set
        {
          this.m_owner.m_itemContent = value;
        }
      }

      public GameObject m_ItemPrefab
      {
        get
        {
          return this.m_owner.m_ItemPrefab;
        }
        set
        {
          this.m_owner.m_ItemPrefab = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public RecommendGiftBoxUITask m_recommendGiftBoxUITask
      {
        get
        {
          return this.m_owner.m_recommendGiftBoxUITask;
        }
        set
        {
          this.m_owner.m_recommendGiftBoxUITask = value;
        }
      }

      public RecommendGiftBoxToggleUIController m_selectToggleUIController
      {
        get
        {
          return this.m_owner.m_selectToggleUIController;
        }
        set
        {
          this.m_owner.m_selectToggleUIController = value;
        }
      }

      public List<RecommendGiftBoxToggleUIController> m_toggleUIControllerList
      {
        get
        {
          return this.m_owner.m_toggleUIControllerList;
        }
        set
        {
          this.m_owner.m_toggleUIControllerList = value;
        }
      }

      public ItemPay m_itemPay
      {
        get
        {
          return this.m_owner.m_itemPay;
        }
        set
        {
          this.m_owner.m_itemPay = value;
        }
      }

      public GameObjectPool<RecommendGiftBoxItemUIController> m_ItemPool
      {
        get
        {
          return this.m_owner.m_ItemPool;
        }
        set
        {
          this.m_owner.m_ItemPool = value;
        }
      }

      public bool m_isProcessingPaySuccess
      {
        get
        {
          return this.m_owner.m_isProcessingPaySuccess;
        }
        set
        {
          this.m_owner.m_isProcessingPaySuccess = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void Init(List<PDSDKGood> pdSDKGoods)
      {
        this.m_owner.Init(pdSDKGoods);
      }

      public int Sort(GiftStoreItem itemA, GiftStoreItem itemB)
      {
        return this.m_owner.Sort(itemA, itemB);
      }

      public void UpdateView()
      {
        this.m_owner.UpdateView();
      }

      public void UpdateViewToggleList()
      {
        this.m_owner.UpdateViewToggleList();
      }

      public void OnGiftBoxClick(
        RecommendGiftBoxToggleUIController recommendGiftBoxItemUIController)
      {
        this.m_owner.OnGiftBoxClick(recommendGiftBoxItemUIController);
      }

      public void OnCloseClick()
      {
        this.m_owner.OnCloseClick();
      }

      public void OnBuyClick()
      {
        this.m_owner.OnBuyClick();
      }

      public void OnPaySuccess()
      {
        this.m_owner.OnPaySuccess();
      }
    }
  }
}
