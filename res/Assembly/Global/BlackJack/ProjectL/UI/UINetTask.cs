﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.UINetTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.TaskNs;
using BlackJack.BJFramework.Runtime.UI;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class UINetTask : NetWorkTransactionTask
  {
    protected bool m_isDisableInput;

    [MethodImpl((MethodImplOptions) 32768)]
    public UINetTask(float timeout = 10f, UITaskBase blockedUITask = null, bool autoRetry = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(object param)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnTimeOut()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnReLoginSuccess()
    {
      // ISSUE: unable to decompile the method.
    }

    public int Result { protected set; get; }

    public bool IsReloginSuccess { protected set; get; }
  }
}
