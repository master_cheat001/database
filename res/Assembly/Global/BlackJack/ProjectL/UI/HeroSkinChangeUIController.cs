﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroSkinChangeUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.Scene;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class HeroSkinChangeUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_stateCtrl;
    [AutoBind("./FacelifPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_facelifPanel;
    [AutoBind("./FacelifPanel/ReturnBGImage", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./FacelifPanel/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_closeButton;
    [AutoBind("./BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_blackBgButton;
    [AutoBind("./FacelifPanel/Detail/SkinSelectPanel", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_skinsScrollRect;
    [AutoBind("./FacelifPanel/Detail/SkinSelectPanel/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skinSelectContent;
    [AutoBind("./FacelifPanel/Detail/InfoPanel/JobScrollView/Viewport/JobGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skinJobGroup;
    [AutoBind("./FacelifPanel/Detail/InfoPanel/JobScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_infoHeroJobScrollRect;
    [AutoBind("./FacelifPanel/Detail/InfoPanel/Char", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skinCharGo;
    [AutoBind("./FacelifPanel/Detail/Scrollrect/Viewport/Content/DescText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_skinDescText;
    [AutoBind("./FacelifPanel/Detail/ButtonGroupState", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_buttonGroupStateCtrl;
    [AutoBind("./FacelifPanel/Detail/ButtonGroupState/FromText/Title/DetailText ", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_fromText;
    [AutoBind("./FacelifPanel/Detail/ButtonGroupState/GetOn", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_infoSwitchButton;
    [AutoBind("./FacelifPanel/Detail/ButtonGroupState/Buy", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_infoBuyButton;
    [AutoBind("./FacelifPanel/Detail/ButtonGroupState/Buy/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoBuyValueText;
    [AutoBind("./FacelifPanel/PlayerResource/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_skinTicketCountText;
    [AutoBind("./FacelifPanel/PlayerResource/AddButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_skinTicketAddButton;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private Hero m_hero;
    private string m_mode;
    private int m_heroSkinInfoId;
    private int m_curCharSkinIndex;
    private ConfigDataHeroInfo m_heroInfo;
    private UISpineGraphic m_playerHeroGraphic;
    private ScrollSnapCenter m_skinsScrollSnapCenter;
    private HeroCharUIController m_heroCharUIController;
    private HeroCharSkinItemUIController m_curSelectHeroCharSkinItemCtrl;
    private List<HeroCharSkinItemUIController> m_charSkinCtrlsList;
    [DoNotToLua]
    private HeroSkinChangeUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_OpenString_hotfix;
    private LuaFunction m_UpdateViewInHeroCharSkinInt32StringHero_hotfix;
    private LuaFunction m_LateUpdate_hotfix;
    private LuaFunction m_SetSelectSkinPanelInfo_hotfix;
    private LuaFunction m_CreateHeroDefaultSkin_hotfix;
    private LuaFunction m_SetSkinInfoPanelHeroCharSkinItemUIController_hotfix;
    private LuaFunction m_SetNotDefaultSkinInfoPanelHeroCharSkinItemUIController_hotfix;
    private LuaFunction m_OnHeroSkinJobItemClickConfigDataJobConnectionInfo_hotfix;
    private LuaFunction m_SetButtonStateHeroCharSkinItemUIController_hotfix;
    private LuaFunction m_SetHeroSkinCharAndSpineHeroCharSkinItemUIController_hotfix;
    private LuaFunction m_SetDefaultSkinInfoPanelHeroCharSkinItemUIController_hotfix;
    private LuaFunction m_OnHeroCharSkinItemClickHeroCharSkinItemUIController_hotfix;
    private LuaFunction m_SkinsScrollSnapCenter_OnEndDrag_hotfix;
    private LuaFunction m_OnInfoBuyButtonClick_hotfix;
    private LuaFunction m_OnInfoSwitchButtonClick_hotfix;
    private LuaFunction m_ShowSwitchHeroSpinePanel_hotfix;
    private LuaFunction m_ShowSwitchDefaultHeroSpinePanel_hotfix;
    private LuaFunction m_OnReturnButtonClick_hotfix;
    private LuaFunction m_OnSkinTicketButtonClick_hotfix;
    private LuaFunction m_CloseHeroSkinChangePanelAction_hotfix;
    private LuaFunction m_OnBuyButtonClick_hotfix;
    private LuaFunction m_ClearData_hotfix;
    private LuaFunction m_add_EventOnReturnAction_hotfix;
    private LuaFunction m_remove_EventOnReturnAction_hotfix;
    private LuaFunction m_add_EventOnBuySkinAction`1_hotfix;
    private LuaFunction m_remove_EventOnBuySkinAction`1_hotfix;
    private LuaFunction m_add_EventOnWearCharSkinAction`3_hotfix;
    private LuaFunction m_remove_EventOnWearCharSkinAction`3_hotfix;
    private LuaFunction m_add_EventOnTakeOffCharSkinAction`2_hotfix;
    private LuaFunction m_remove_EventOnTakeOffCharSkinAction`2_hotfix;
    private LuaFunction m_add_EventOnSkinChangedPreviewAction`2_hotfix;
    private LuaFunction m_remove_EventOnSkinChangedPreviewAction`2_hotfix;
    private LuaFunction m_add_EventOnSkinTicketButtonClickAction_hotfix;
    private LuaFunction m_remove_EventOnSkinTicketButtonClickAction_hotfix;
    private LuaFunction m_add_EventOnWearModelSkinAction`3_hotfix;
    private LuaFunction m_remove_EventOnWearModelSkinAction`3_hotfix;
    private LuaFunction m_add_EventOnTakeOffModelSkinAction`2_hotfix;
    private LuaFunction m_remove_EventOnTakeOffModelSkinAction`2_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroSkinChangeUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open(string mode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateViewInHeroCharSkin(int heroSkinInfoId, string mode, Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LateUpdate()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSelectSkinPanelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateHeroDefaultSkin()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSkinInfoPanel(HeroCharSkinItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetNotDefaultSkinInfoPanel(HeroCharSkinItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroSkinJobItemClick(ConfigDataJobConnectionInfo heroJobConnectionInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetButtonState(HeroCharSkinItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetHeroSkinCharAndSpine(HeroCharSkinItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetDefaultSkinInfoPanel(HeroCharSkinItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroCharSkinItemClick(HeroCharSkinItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SkinsScrollSnapCenter_OnEndDrag()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInfoBuyButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInfoSwitchButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowSwitchHeroSpinePanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowSwitchDefaultHeroSpinePanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSkinTicketButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CloseHeroSkinChangePanel(Action onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBuyButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearData()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnBuySkin
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int, Action> EventOnWearCharSkin
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, Action> EventOnTakeOffCharSkin
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string, int> EventOnSkinChangedPreview
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnSkinTicketButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int, int> EventOnWearModelSkin
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int> EventOnTakeOffModelSkin
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public HeroSkinChangeUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnReturn()
    {
      this.EventOnReturn = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnBuySkin(int obj)
    {
    }

    private void __clearDele_EventOnBuySkin(int obj)
    {
      this.EventOnBuySkin = (Action<int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnWearCharSkin(int arg1, int arg2, Action arg3)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnWearCharSkin(int arg1, int arg2, Action arg3)
    {
      this.EventOnWearCharSkin = (Action<int, int, Action>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnTakeOffCharSkin(int arg1, Action arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnTakeOffCharSkin(int arg1, Action arg2)
    {
      this.EventOnTakeOffCharSkin = (Action<int, Action>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnSkinChangedPreview(string arg1, int arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnSkinChangedPreview(string arg1, int arg2)
    {
      this.EventOnSkinChangedPreview = (Action<string, int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnSkinTicketButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnSkinTicketButtonClick()
    {
      this.EventOnSkinTicketButtonClick = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnWearModelSkin(int arg1, int arg2, int arg3)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnWearModelSkin(int arg1, int arg2, int arg3)
    {
      this.EventOnWearModelSkin = (Action<int, int, int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnTakeOffModelSkin(int arg1, int arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnTakeOffModelSkin(int arg1, int arg2)
    {
      this.EventOnTakeOffModelSkin = (Action<int, int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private HeroSkinChangeUIController m_owner;

      public LuaExportHelper(HeroSkinChangeUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnReturn()
      {
        this.m_owner.__callDele_EventOnReturn();
      }

      public void __clearDele_EventOnReturn()
      {
        this.m_owner.__clearDele_EventOnReturn();
      }

      public void __callDele_EventOnBuySkin(int obj)
      {
        this.m_owner.__callDele_EventOnBuySkin(obj);
      }

      public void __clearDele_EventOnBuySkin(int obj)
      {
        this.m_owner.__clearDele_EventOnBuySkin(obj);
      }

      public void __callDele_EventOnWearCharSkin(int arg1, int arg2, Action arg3)
      {
        this.m_owner.__callDele_EventOnWearCharSkin(arg1, arg2, arg3);
      }

      public void __clearDele_EventOnWearCharSkin(int arg1, int arg2, Action arg3)
      {
        this.m_owner.__clearDele_EventOnWearCharSkin(arg1, arg2, arg3);
      }

      public void __callDele_EventOnTakeOffCharSkin(int arg1, Action arg2)
      {
        this.m_owner.__callDele_EventOnTakeOffCharSkin(arg1, arg2);
      }

      public void __clearDele_EventOnTakeOffCharSkin(int arg1, Action arg2)
      {
        this.m_owner.__clearDele_EventOnTakeOffCharSkin(arg1, arg2);
      }

      public void __callDele_EventOnSkinChangedPreview(string arg1, int arg2)
      {
        this.m_owner.__callDele_EventOnSkinChangedPreview(arg1, arg2);
      }

      public void __clearDele_EventOnSkinChangedPreview(string arg1, int arg2)
      {
        this.m_owner.__clearDele_EventOnSkinChangedPreview(arg1, arg2);
      }

      public void __callDele_EventOnSkinTicketButtonClick()
      {
        this.m_owner.__callDele_EventOnSkinTicketButtonClick();
      }

      public void __clearDele_EventOnSkinTicketButtonClick()
      {
        this.m_owner.__clearDele_EventOnSkinTicketButtonClick();
      }

      public void __callDele_EventOnWearModelSkin(int arg1, int arg2, int arg3)
      {
        this.m_owner.__callDele_EventOnWearModelSkin(arg1, arg2, arg3);
      }

      public void __clearDele_EventOnWearModelSkin(int arg1, int arg2, int arg3)
      {
        this.m_owner.__clearDele_EventOnWearModelSkin(arg1, arg2, arg3);
      }

      public void __callDele_EventOnTakeOffModelSkin(int arg1, int arg2)
      {
        this.m_owner.__callDele_EventOnTakeOffModelSkin(arg1, arg2);
      }

      public void __clearDele_EventOnTakeOffModelSkin(int arg1, int arg2)
      {
        this.m_owner.__clearDele_EventOnTakeOffModelSkin(arg1, arg2);
      }

      public RectTransform m_marginTransform
      {
        get
        {
          return this.m_owner.m_marginTransform;
        }
        set
        {
          this.m_owner.m_marginTransform = value;
        }
      }

      public CommonUIStateController m_stateCtrl
      {
        get
        {
          return this.m_owner.m_stateCtrl;
        }
        set
        {
          this.m_owner.m_stateCtrl = value;
        }
      }

      public GameObject m_facelifPanel
      {
        get
        {
          return this.m_owner.m_facelifPanel;
        }
        set
        {
          this.m_owner.m_facelifPanel = value;
        }
      }

      public Button m_returnButton
      {
        get
        {
          return this.m_owner.m_returnButton;
        }
        set
        {
          this.m_owner.m_returnButton = value;
        }
      }

      public Button m_closeButton
      {
        get
        {
          return this.m_owner.m_closeButton;
        }
        set
        {
          this.m_owner.m_closeButton = value;
        }
      }

      public Button m_blackBgButton
      {
        get
        {
          return this.m_owner.m_blackBgButton;
        }
        set
        {
          this.m_owner.m_blackBgButton = value;
        }
      }

      public ScrollRect m_skinsScrollRect
      {
        get
        {
          return this.m_owner.m_skinsScrollRect;
        }
        set
        {
          this.m_owner.m_skinsScrollRect = value;
        }
      }

      public GameObject m_skinSelectContent
      {
        get
        {
          return this.m_owner.m_skinSelectContent;
        }
        set
        {
          this.m_owner.m_skinSelectContent = value;
        }
      }

      public GameObject m_skinJobGroup
      {
        get
        {
          return this.m_owner.m_skinJobGroup;
        }
        set
        {
          this.m_owner.m_skinJobGroup = value;
        }
      }

      public ScrollRect m_infoHeroJobScrollRect
      {
        get
        {
          return this.m_owner.m_infoHeroJobScrollRect;
        }
        set
        {
          this.m_owner.m_infoHeroJobScrollRect = value;
        }
      }

      public GameObject m_skinCharGo
      {
        get
        {
          return this.m_owner.m_skinCharGo;
        }
        set
        {
          this.m_owner.m_skinCharGo = value;
        }
      }

      public Text m_skinDescText
      {
        get
        {
          return this.m_owner.m_skinDescText;
        }
        set
        {
          this.m_owner.m_skinDescText = value;
        }
      }

      public CommonUIStateController m_buttonGroupStateCtrl
      {
        get
        {
          return this.m_owner.m_buttonGroupStateCtrl;
        }
        set
        {
          this.m_owner.m_buttonGroupStateCtrl = value;
        }
      }

      public Text m_fromText
      {
        get
        {
          return this.m_owner.m_fromText;
        }
        set
        {
          this.m_owner.m_fromText = value;
        }
      }

      public Button m_infoSwitchButton
      {
        get
        {
          return this.m_owner.m_infoSwitchButton;
        }
        set
        {
          this.m_owner.m_infoSwitchButton = value;
        }
      }

      public Button m_infoBuyButton
      {
        get
        {
          return this.m_owner.m_infoBuyButton;
        }
        set
        {
          this.m_owner.m_infoBuyButton = value;
        }
      }

      public Text m_infoBuyValueText
      {
        get
        {
          return this.m_owner.m_infoBuyValueText;
        }
        set
        {
          this.m_owner.m_infoBuyValueText = value;
        }
      }

      public Text m_skinTicketCountText
      {
        get
        {
          return this.m_owner.m_skinTicketCountText;
        }
        set
        {
          this.m_owner.m_skinTicketCountText = value;
        }
      }

      public Button m_skinTicketAddButton
      {
        get
        {
          return this.m_owner.m_skinTicketAddButton;
        }
        set
        {
          this.m_owner.m_skinTicketAddButton = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public Hero m_hero
      {
        get
        {
          return this.m_owner.m_hero;
        }
        set
        {
          this.m_owner.m_hero = value;
        }
      }

      public string m_mode
      {
        get
        {
          return this.m_owner.m_mode;
        }
        set
        {
          this.m_owner.m_mode = value;
        }
      }

      public int m_heroSkinInfoId
      {
        get
        {
          return this.m_owner.m_heroSkinInfoId;
        }
        set
        {
          this.m_owner.m_heroSkinInfoId = value;
        }
      }

      public int m_curCharSkinIndex
      {
        get
        {
          return this.m_owner.m_curCharSkinIndex;
        }
        set
        {
          this.m_owner.m_curCharSkinIndex = value;
        }
      }

      public ConfigDataHeroInfo m_heroInfo
      {
        get
        {
          return this.m_owner.m_heroInfo;
        }
        set
        {
          this.m_owner.m_heroInfo = value;
        }
      }

      public UISpineGraphic m_playerHeroGraphic
      {
        get
        {
          return this.m_owner.m_playerHeroGraphic;
        }
        set
        {
          this.m_owner.m_playerHeroGraphic = value;
        }
      }

      public ScrollSnapCenter m_skinsScrollSnapCenter
      {
        get
        {
          return this.m_owner.m_skinsScrollSnapCenter;
        }
        set
        {
          this.m_owner.m_skinsScrollSnapCenter = value;
        }
      }

      public HeroCharUIController m_heroCharUIController
      {
        get
        {
          return this.m_owner.m_heroCharUIController;
        }
        set
        {
          this.m_owner.m_heroCharUIController = value;
        }
      }

      public HeroCharSkinItemUIController m_curSelectHeroCharSkinItemCtrl
      {
        get
        {
          return this.m_owner.m_curSelectHeroCharSkinItemCtrl;
        }
        set
        {
          this.m_owner.m_curSelectHeroCharSkinItemCtrl = value;
        }
      }

      public List<HeroCharSkinItemUIController> m_charSkinCtrlsList
      {
        get
        {
          return this.m_owner.m_charSkinCtrlsList;
        }
        set
        {
          this.m_owner.m_charSkinCtrlsList = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void LateUpdate()
      {
        this.m_owner.LateUpdate();
      }

      public void SetSelectSkinPanelInfo()
      {
        this.m_owner.SetSelectSkinPanelInfo();
      }

      public void CreateHeroDefaultSkin()
      {
        this.m_owner.CreateHeroDefaultSkin();
      }

      public void SetSkinInfoPanel(HeroCharSkinItemUIController ctrl)
      {
        this.m_owner.SetSkinInfoPanel(ctrl);
      }

      public void SetNotDefaultSkinInfoPanel(HeroCharSkinItemUIController ctrl)
      {
        this.m_owner.SetNotDefaultSkinInfoPanel(ctrl);
      }

      public void OnHeroSkinJobItemClick(ConfigDataJobConnectionInfo heroJobConnectionInfo)
      {
        this.m_owner.OnHeroSkinJobItemClick(heroJobConnectionInfo);
      }

      public void SetButtonState(HeroCharSkinItemUIController ctrl)
      {
        this.m_owner.SetButtonState(ctrl);
      }

      public void SetHeroSkinCharAndSpine(HeroCharSkinItemUIController ctrl)
      {
        this.m_owner.SetHeroSkinCharAndSpine(ctrl);
      }

      public void SetDefaultSkinInfoPanel(HeroCharSkinItemUIController ctrl)
      {
        this.m_owner.SetDefaultSkinInfoPanel(ctrl);
      }

      public void OnHeroCharSkinItemClick(HeroCharSkinItemUIController ctrl)
      {
        this.m_owner.OnHeroCharSkinItemClick(ctrl);
      }

      public void SkinsScrollSnapCenter_OnEndDrag()
      {
        this.m_owner.SkinsScrollSnapCenter_OnEndDrag();
      }

      public void OnInfoBuyButtonClick()
      {
        this.m_owner.OnInfoBuyButtonClick();
      }

      public void OnInfoSwitchButtonClick()
      {
        this.m_owner.OnInfoSwitchButtonClick();
      }

      public void ShowSwitchHeroSpinePanel()
      {
        this.m_owner.ShowSwitchHeroSpinePanel();
      }

      public void ShowSwitchDefaultHeroSpinePanel()
      {
        this.m_owner.ShowSwitchDefaultHeroSpinePanel();
      }

      public void OnReturnButtonClick()
      {
        this.m_owner.OnReturnButtonClick();
      }

      public void OnSkinTicketButtonClick()
      {
        this.m_owner.OnSkinTicketButtonClick();
      }

      public void OnBuyButtonClick()
      {
        this.m_owner.OnBuyButtonClick();
      }
    }
  }
}
