﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ARHeroCardUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class ARHeroCardUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_heroItemButton;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_heroItemUIStateController;
    [AutoBind("./HeroStars", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroStar;
    [AutoBind("./HeroTypeImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroTypeImg;
    [AutoBind("./HeroLvText/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroLvText;
    [AutoBind("./SSRFrameEffect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroSSRFrameEffect;
    [AutoBind("./FrameImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroFrameImg;
    [AutoBind("./HeroIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroIconImg;
    [AutoBind("./ARSelectFrameImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroSelectFrame;
    [AutoBind("./ARSelectFrontImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroSelectFront;
    public ARHeroListUIController.HeroWrap m_heroWrap;
    private IConfigDataLoader m_configDataLoader;
    [DoNotToLua]
    private ARHeroCardUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_SetHeroListItemInfoHeroWrap_hotfix;
    private LuaFunction m_RefreshHeroSelect_hotfix;
    private LuaFunction m_OnHeroItemClick_hotfix;
    private LuaFunction m_add_EventOnSelectHeroItemAction`1_hotfix;
    private LuaFunction m_remove_EventOnSelectHeroItemAction`1_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHeroListItemInfo(ARHeroListUIController.HeroWrap heroWrap)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RefreshHeroSelect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroItemClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<ARHeroCardUIController> EventOnSelectHeroItem
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public ARHeroCardUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnSelectHeroItem(ARHeroCardUIController obj)
    {
    }

    private void __clearDele_EventOnSelectHeroItem(ARHeroCardUIController obj)
    {
      this.EventOnSelectHeroItem = (Action<ARHeroCardUIController>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private ARHeroCardUIController m_owner;

      public LuaExportHelper(ARHeroCardUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnSelectHeroItem(ARHeroCardUIController obj)
      {
        this.m_owner.__callDele_EventOnSelectHeroItem(obj);
      }

      public void __clearDele_EventOnSelectHeroItem(ARHeroCardUIController obj)
      {
        this.m_owner.__clearDele_EventOnSelectHeroItem(obj);
      }

      public Button m_heroItemButton
      {
        get
        {
          return this.m_owner.m_heroItemButton;
        }
        set
        {
          this.m_owner.m_heroItemButton = value;
        }
      }

      public CommonUIStateController m_heroItemUIStateController
      {
        get
        {
          return this.m_owner.m_heroItemUIStateController;
        }
        set
        {
          this.m_owner.m_heroItemUIStateController = value;
        }
      }

      public GameObject m_heroStar
      {
        get
        {
          return this.m_owner.m_heroStar;
        }
        set
        {
          this.m_owner.m_heroStar = value;
        }
      }

      public Image m_heroTypeImg
      {
        get
        {
          return this.m_owner.m_heroTypeImg;
        }
        set
        {
          this.m_owner.m_heroTypeImg = value;
        }
      }

      public Text m_heroLvText
      {
        get
        {
          return this.m_owner.m_heroLvText;
        }
        set
        {
          this.m_owner.m_heroLvText = value;
        }
      }

      public GameObject m_heroSSRFrameEffect
      {
        get
        {
          return this.m_owner.m_heroSSRFrameEffect;
        }
        set
        {
          this.m_owner.m_heroSSRFrameEffect = value;
        }
      }

      public Image m_heroFrameImg
      {
        get
        {
          return this.m_owner.m_heroFrameImg;
        }
        set
        {
          this.m_owner.m_heroFrameImg = value;
        }
      }

      public Image m_heroIconImg
      {
        get
        {
          return this.m_owner.m_heroIconImg;
        }
        set
        {
          this.m_owner.m_heroIconImg = value;
        }
      }

      public GameObject m_heroSelectFrame
      {
        get
        {
          return this.m_owner.m_heroSelectFrame;
        }
        set
        {
          this.m_owner.m_heroSelectFrame = value;
        }
      }

      public GameObject m_heroSelectFront
      {
        get
        {
          return this.m_owner.m_heroSelectFront;
        }
        set
        {
          this.m_owner.m_heroSelectFront = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void OnHeroItemClick()
      {
        this.m_owner.OnHeroItemClick();
      }
    }
  }
}
