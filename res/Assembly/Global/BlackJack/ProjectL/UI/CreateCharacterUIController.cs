﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.CreateCharacterUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class CreateCharacterUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./CharacterName/InputField", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_characterNameInputField;
    [AutoBind("./CreateButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_createButton;
    [AutoBind("./AutoButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_autoButton;
    [AutoBind("./Message", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_messageGameObject;
    [AutoBind("./Message/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_messageText;
    private int m_characterNameLimit;
    private float m_hideMessageTime;

    private CreateCharacterUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Start()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Close(Action onEnd)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCharacterNameLimit(int count)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCharacterName(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    public string GetCharacterName()
    {
      return this.m_characterNameInputField.text;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowMessage(string text, int time = 0)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowMessage(StringTableId id, int time = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideMessage()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCreateButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAutoButtonClick()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CharacterNameInputField_OnEndEdit(string str)
    {
    }

    public event Action EventOnCreate
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    public event Action EventOnAutoName
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }
  }
}
