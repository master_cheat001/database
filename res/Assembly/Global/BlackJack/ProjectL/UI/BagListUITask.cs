﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BagListUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class BagListUITask : UITask
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    public string BagMode;
    public string AlchemyMode;
    private string m_curMode;
    private BagListUIController m_bagListUIController;
    private AlchemyUIController m_alchemyUIController;
    private int m_curLayerDescIndex;
    private bool m_isNeedShowFadeIn;
    private BagListUIController.DisplayType m_displayType;
    private ulong m_clickBagItemInstanceId;
    [DoNotToLua]
    private BagListUITask.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctorString_hotfix;
    private LuaFunction m_OnStartUIIntent_hotfix;
    private LuaFunction m_OnStop_hotfix;
    private LuaFunction m_OnResumeUIIntent_hotfix;
    private LuaFunction m_SaveUIStateToIntent_hotfix;
    private LuaFunction m_GetUIStateFromIntentUIIntent_hotfix;
    private LuaFunction m_IsNeedLoadStaticRes_hotfix;
    private LuaFunction m_CollectAllStaticResDescForLoad_hotfix;
    private LuaFunction m_InitAllUIControllers_hotfix;
    private LuaFunction m_ClearAllContextAndRes_hotfix;
    private LuaFunction m_UpdateView_hotfix;
    private LuaFunction m_PushAndPopLayerByState_hotfix;
    private LuaFunction m_BagListUIController_OnReturn_hotfix;
    private LuaFunction m_BagListUIController_OnAddAllItem_hotfix;
    private LuaFunction m_BagListUIController_OnSpeedUpInt32UInt64DisplayType_hotfix;
    private LuaFunction m_BagListUIController_OnAddAllEquipment_hotfix;
    private LuaFunction m_BagListUIController_OnAddItemString_hotfix;
    private LuaFunction m_BagListUIController_OnClearBag_hotfix;
    private LuaFunction m_BagListUIController_OnUseItemGoodsTypeInt32Int32DisplayType_hotfix;
    private LuaFunction m_ShowRewardGoodsUITaskInt32Int32List`1_hotfix;
    private LuaFunction m_BagListUIController_OnSellItemUInt64Int32_hotfix;
    private LuaFunction m_BagListUIController_OnGotoEquipmentForgeInt32UInt64DisplayType_hotfix;
    private LuaFunction m_EquipmentForgeUITask_OnLoadAllResCompleted_hotfix;
    private LuaFunction m_BagListUIController_OnAlchemyButtonClick_hotfix;
    private LuaFunction m_BagListUIController_OnLockButtonClickUInt64_hotfix;
    private LuaFunction m_Bag_OnShowGetPathBagItemBaseDisplayType_hotfix;
    private LuaFunction m_Bag_OnGotoGetPathGetPathDataNeedGoods_hotfix;
    private LuaFunction m_AlchemyUIController_OnReturn_hotfix;
    private LuaFunction m_AlchemyUIController_OnShowHelp_hotfix;
    private LuaFunction m_AlchemyUIController_OnAlchemyButtonClickList`1Action`1_hotfix;
    private LuaFunction m_AlchemyUIController_OnLockButtonClickUInt64Action_hotfix;
    private LuaFunction m_get_LayerDescArray_hotfix;
    private LuaFunction m_get_UICtrlDescArray_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public BagListUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnResume(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SaveUIStateToIntent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GetUIStateFromIntent(UIIntent uiIntent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadStaticRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override List<UITaskBase.LayerDesc> CollectAllStaticResDescForLoad()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PushAndPopLayerByState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BagListUIController_OnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BagListUIController_OnAddAllItem()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BagListUIController_OnSpeedUp(
      int slot,
      ulong equipmentInstanceId,
      BagListUIController.DisplayType displayType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BagListUIController_OnAddAllEquipment()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BagListUIController_OnAddItem(string str)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BagListUIController_OnClearBag()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BagListUIController_OnUseItem(
      GoodsType type,
      int id,
      int count,
      BagListUIController.DisplayType displayType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowRewardGoodsUITask(int itemId, int count, List<Goods> goods)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BagListUIController_OnSellItem(ulong instanceId, int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BagListUIController_OnGotoEquipmentForge(
      int slot,
      ulong equipmentInstanceId,
      BagListUIController.DisplayType displayType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void EquipmentForgeUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BagListUIController_OnAlchemyButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BagListUIController_OnLockButtonClick(ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Bag_OnShowGetPath(BagItemBase goods, BagListUIController.DisplayType displayType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Bag_OnGotoGetPath(GetPathData getPath, NeedGoods needGoods = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AlchemyUIController_OnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AlchemyUIController_OnShowHelp()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AlchemyUIController_OnAlchemyButtonClick(
      List<ProGoods> proGoods,
      Action<List<Goods>> OnSucceed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AlchemyUIController_OnLockButtonClick(ulong instanceId, Action onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public BagListUITask.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private bool __callBase_OnStart(UIIntent intent)
    {
      return base.OnStart(intent);
    }

    private void __callBase_OnStop()
    {
      base.OnStop();
    }

    private void __callBase_OnPause()
    {
      this.OnPause();
    }

    private bool __callBase_OnResume(UIIntent intent)
    {
      return base.OnResume(intent);
    }

    private bool __callBase_OnNewIntent(UIIntent intent)
    {
      return this.OnNewIntent(intent);
    }

    private void __callBase_EnableUIInput(bool isEnable, bool? isGlobalEnable)
    {
      this.EnableUIInput(isEnable, isGlobalEnable);
    }

    private bool __callBase_IsNeedLoadDynamicRes()
    {
      return this.IsNeedLoadDynamicRes();
    }

    private List<string> __callBase_CollectAllDynamicResForLoad()
    {
      return this.CollectAllDynamicResForLoad();
    }

    private void __callBase_RegisterPlayerContextEvents()
    {
      this.RegisterPlayerContextEvents();
    }

    private void __callBase_UnregisterPlayerContextEvents()
    {
      this.UnregisterPlayerContextEvents();
    }

    private void __callBase_PostUpdateView()
    {
      this.PostUpdateView();
    }

    private void __callBase_CollectPreloadResourceList()
    {
      this.CollectPreloadResourceList();
    }

    private void __callBase_ClearAssetList()
    {
      this.ClearAssetList();
    }

    private void __callBase_CollectAsset(string assetName)
    {
      this.CollectAsset(assetName);
    }

    private void __callBase_CollectSpriteAsset(string assetName)
    {
      this.CollectSpriteAsset(assetName);
    }

    private void __callBase_CollectFxAsset(string assetName)
    {
      this.CollectFxAsset(assetName);
    }

    private void __callBase_StartLoadCustomAssets(Action onLoadCompleted)
    {
      this.StartLoadCustomAssets(onLoadCompleted);
    }

    private bool __callBase_IsLoadingCustomAssets()
    {
      return this.IsLoadingCustomAssets();
    }

    private bool __callBase_IsPipeLineRunning()
    {
      return this.IsPipeLineRunning();
    }

    private bool __callBase_IsOpeningUI()
    {
      return this.IsOpeningUI();
    }

    private UITaskBase __callBase_ReturnPrevUITask()
    {
      return this.ReturnPrevUITask();
    }

    private void __callBase_ClearUnusedDynamicResourceCache()
    {
      this.ClearUnusedDynamicResourceCache();
    }

    private void __callBase_ClearDynamicResourceCache(List<string> assets)
    {
      this.ClearDynamicResourceCache(assets);
    }

    private void __callBase_OnMemoryWarning()
    {
      this.OnMemoryWarning();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private BagListUITask m_owner;

      public LuaExportHelper(BagListUITask owner)
      {
        this.m_owner = owner;
      }

      public bool __callBase_OnStart(UIIntent intent)
      {
        return this.m_owner.__callBase_OnStart(intent);
      }

      public void __callBase_OnStop()
      {
        this.m_owner.__callBase_OnStop();
      }

      public void __callBase_OnPause()
      {
        this.m_owner.__callBase_OnPause();
      }

      public bool __callBase_OnResume(UIIntent intent)
      {
        return this.m_owner.__callBase_OnResume(intent);
      }

      public bool __callBase_OnNewIntent(UIIntent intent)
      {
        return this.m_owner.__callBase_OnNewIntent(intent);
      }

      public void __callBase_EnableUIInput(bool isEnable, bool? isGlobalEnable)
      {
        this.m_owner.__callBase_EnableUIInput(isEnable, isGlobalEnable);
      }

      public bool __callBase_IsNeedLoadDynamicRes()
      {
        return this.m_owner.__callBase_IsNeedLoadDynamicRes();
      }

      public List<string> __callBase_CollectAllDynamicResForLoad()
      {
        return this.m_owner.__callBase_CollectAllDynamicResForLoad();
      }

      public void __callBase_RegisterPlayerContextEvents()
      {
        this.m_owner.__callBase_RegisterPlayerContextEvents();
      }

      public void __callBase_UnregisterPlayerContextEvents()
      {
        this.m_owner.__callBase_UnregisterPlayerContextEvents();
      }

      public void __callBase_PostUpdateView()
      {
        this.m_owner.__callBase_PostUpdateView();
      }

      public void __callBase_CollectPreloadResourceList()
      {
        this.m_owner.__callBase_CollectPreloadResourceList();
      }

      public void __callBase_ClearAssetList()
      {
        this.m_owner.__callBase_ClearAssetList();
      }

      public void __callBase_CollectAsset(string assetName)
      {
        this.m_owner.__callBase_CollectAsset(assetName);
      }

      public void __callBase_CollectSpriteAsset(string assetName)
      {
        this.m_owner.__callBase_CollectSpriteAsset(assetName);
      }

      public void __callBase_CollectFxAsset(string assetName)
      {
        this.m_owner.__callBase_CollectFxAsset(assetName);
      }

      public void __callBase_StartLoadCustomAssets(Action onLoadCompleted)
      {
        this.m_owner.__callBase_StartLoadCustomAssets(onLoadCompleted);
      }

      public bool __callBase_IsLoadingCustomAssets()
      {
        return this.m_owner.__callBase_IsLoadingCustomAssets();
      }

      public bool __callBase_IsPipeLineRunning()
      {
        return this.m_owner.__callBase_IsPipeLineRunning();
      }

      public bool __callBase_IsOpeningUI()
      {
        return this.m_owner.__callBase_IsOpeningUI();
      }

      public UITaskBase __callBase_ReturnPrevUITask()
      {
        return this.m_owner.__callBase_ReturnPrevUITask();
      }

      public void __callBase_ClearUnusedDynamicResourceCache()
      {
        this.m_owner.__callBase_ClearUnusedDynamicResourceCache();
      }

      public void __callBase_ClearDynamicResourceCache(List<string> assets)
      {
        this.m_owner.__callBase_ClearDynamicResourceCache(assets);
      }

      public void __callBase_OnMemoryWarning()
      {
        this.m_owner.__callBase_OnMemoryWarning();
      }

      public UITaskBase.LayerDesc[] m_layerDescArray
      {
        get
        {
          return this.m_owner.m_layerDescArray;
        }
        set
        {
          this.m_owner.m_layerDescArray = value;
        }
      }

      public UITaskBase.UIControllerDesc[] m_uiCtrlDescArray
      {
        get
        {
          return this.m_owner.m_uiCtrlDescArray;
        }
        set
        {
          this.m_owner.m_uiCtrlDescArray = value;
        }
      }

      public string m_curMode
      {
        get
        {
          return this.m_owner.m_curMode;
        }
        set
        {
          this.m_owner.m_curMode = value;
        }
      }

      public BagListUIController m_bagListUIController
      {
        get
        {
          return this.m_owner.m_bagListUIController;
        }
        set
        {
          this.m_owner.m_bagListUIController = value;
        }
      }

      public AlchemyUIController m_alchemyUIController
      {
        get
        {
          return this.m_owner.m_alchemyUIController;
        }
        set
        {
          this.m_owner.m_alchemyUIController = value;
        }
      }

      public int m_curLayerDescIndex
      {
        get
        {
          return this.m_owner.m_curLayerDescIndex;
        }
        set
        {
          this.m_owner.m_curLayerDescIndex = value;
        }
      }

      public bool m_isNeedShowFadeIn
      {
        get
        {
          return this.m_owner.m_isNeedShowFadeIn;
        }
        set
        {
          this.m_owner.m_isNeedShowFadeIn = value;
        }
      }

      public BagListUIController.DisplayType m_displayType
      {
        get
        {
          return this.m_owner.m_displayType;
        }
        set
        {
          this.m_owner.m_displayType = value;
        }
      }

      public ulong m_clickBagItemInstanceId
      {
        get
        {
          return this.m_owner.m_clickBagItemInstanceId;
        }
        set
        {
          this.m_owner.m_clickBagItemInstanceId = value;
        }
      }

      public UITaskBase.LayerDesc[] LayerDescArray
      {
        get
        {
          return this.m_owner.LayerDescArray;
        }
      }

      public UITaskBase.UIControllerDesc[] UICtrlDescArray
      {
        get
        {
          return this.m_owner.UICtrlDescArray;
        }
      }

      public bool OnStart(UIIntent intent)
      {
        return this.m_owner.OnStart(intent);
      }

      public void OnStop()
      {
        this.m_owner.OnStop();
      }

      public bool OnResume(UIIntent intent)
      {
        return this.m_owner.OnResume(intent);
      }

      public void SaveUIStateToIntent()
      {
        this.m_owner.SaveUIStateToIntent();
      }

      public void GetUIStateFromIntent(UIIntent uiIntent)
      {
        this.m_owner.GetUIStateFromIntent(uiIntent);
      }

      public bool IsNeedLoadStaticRes()
      {
        return this.m_owner.IsNeedLoadStaticRes();
      }

      public List<UITaskBase.LayerDesc> CollectAllStaticResDescForLoad()
      {
        return this.m_owner.CollectAllStaticResDescForLoad();
      }

      public void InitAllUIControllers()
      {
        this.m_owner.InitAllUIControllers();
      }

      public void ClearAllContextAndRes()
      {
        this.m_owner.ClearAllContextAndRes();
      }

      public void UpdateView()
      {
        this.m_owner.UpdateView();
      }

      public void PushAndPopLayerByState()
      {
        this.m_owner.PushAndPopLayerByState();
      }

      public void BagListUIController_OnReturn()
      {
        this.m_owner.BagListUIController_OnReturn();
      }

      public void BagListUIController_OnAddAllItem()
      {
        this.m_owner.BagListUIController_OnAddAllItem();
      }

      public void BagListUIController_OnSpeedUp(
        int slot,
        ulong equipmentInstanceId,
        BagListUIController.DisplayType displayType)
      {
        this.m_owner.BagListUIController_OnSpeedUp(slot, equipmentInstanceId, displayType);
      }

      public void BagListUIController_OnAddAllEquipment()
      {
        this.m_owner.BagListUIController_OnAddAllEquipment();
      }

      public void BagListUIController_OnAddItem(string str)
      {
        this.m_owner.BagListUIController_OnAddItem(str);
      }

      public void BagListUIController_OnClearBag()
      {
        this.m_owner.BagListUIController_OnClearBag();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void BagListUIController_OnUseItem(
        GoodsType type,
        int id,
        int count,
        BagListUIController.DisplayType displayType)
      {
        // ISSUE: unable to decompile the method.
      }

      public void ShowRewardGoodsUITask(int itemId, int count, List<Goods> goods)
      {
        this.m_owner.ShowRewardGoodsUITask(itemId, count, goods);
      }

      public void BagListUIController_OnSellItem(ulong instanceId, int count)
      {
        this.m_owner.BagListUIController_OnSellItem(instanceId, count);
      }

      public void BagListUIController_OnGotoEquipmentForge(
        int slot,
        ulong equipmentInstanceId,
        BagListUIController.DisplayType displayType)
      {
        this.m_owner.BagListUIController_OnGotoEquipmentForge(slot, equipmentInstanceId, displayType);
      }

      public void EquipmentForgeUITask_OnLoadAllResCompleted()
      {
        this.m_owner.EquipmentForgeUITask_OnLoadAllResCompleted();
      }

      public void BagListUIController_OnAlchemyButtonClick()
      {
        this.m_owner.BagListUIController_OnAlchemyButtonClick();
      }

      public void BagListUIController_OnLockButtonClick(ulong instanceId)
      {
        this.m_owner.BagListUIController_OnLockButtonClick(instanceId);
      }

      public void Bag_OnShowGetPath(BagItemBase goods, BagListUIController.DisplayType displayType)
      {
        this.m_owner.Bag_OnShowGetPath(goods, displayType);
      }

      public void Bag_OnGotoGetPath(GetPathData getPath, NeedGoods needGoods)
      {
        this.m_owner.Bag_OnGotoGetPath(getPath, needGoods);
      }

      public void AlchemyUIController_OnReturn()
      {
        this.m_owner.AlchemyUIController_OnReturn();
      }

      public void AlchemyUIController_OnShowHelp()
      {
        this.m_owner.AlchemyUIController_OnShowHelp();
      }

      public void AlchemyUIController_OnAlchemyButtonClick(
        List<ProGoods> proGoods,
        Action<List<Goods>> OnSucceed)
      {
        this.m_owner.AlchemyUIController_OnAlchemyButtonClick(proGoods, OnSucceed);
      }

      public void AlchemyUIController_OnLockButtonClick(ulong instanceId, Action onEnd)
      {
        this.m_owner.AlchemyUIController_OnLockButtonClick(instanceId, onEnd);
      }
    }
  }
}
