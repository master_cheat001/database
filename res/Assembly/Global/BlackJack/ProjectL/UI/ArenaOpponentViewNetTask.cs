﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ArenaOpponentViewNetTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class ArenaOpponentViewNetTask : UINetTask
  {
    private int m_opponentIndex;

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaOpponentViewNetTask(int opponentIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterNetworkEvent()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool StartNetWorking()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnArenaOpponentViewAck(int result, List<ProBattleHero> heros, int battlePower)
    {
    }

    public List<ProBattleHero> Heros { private set; get; }

    public int BattlePower { private set; get; }
  }
}
