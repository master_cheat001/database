﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.SoldierDescUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Scene;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class SoldierDescUIController : UIControllerBase
  {
    [AutoBind("./Faction/JobImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_soldierIconImg;
    [AutoBind("./Graphic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_soldierDetailGraphic;
    [AutoBind("./Faction/RangeValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierRangeText;
    [AutoBind("./Faction/MoveValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierMoveText;
    [AutoBind("./Faction/TypeBgImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_typeBgImg;
    [AutoBind("./Faction/TypeBgImage2", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_typeBgImg2;
    [AutoBind("./TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierTitleText;
    [AutoBind("./Desc/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDescText;
    [AutoBind("./Desc/Weak/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierWeakText;
    [AutoBind("./Desc/Restrain/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierStrongText;
    [AutoBind("./HP/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierPropHPValueText;
    [AutoBind("./DF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierPropDFValueText;
    [AutoBind("./AT/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierPropATValueText;
    [AutoBind("./MagicDF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierPropMagicDFValueText;
    [AutoBind("./HP/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierPropHPAddText;
    [AutoBind("./DF/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierPropDFAddText;
    [AutoBind("./AT/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierPropATAddText;
    [AutoBind("./MagicDF/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierPropMagicDFAddText;
    private UISpineGraphic m_soldierGraphic;
    [DoNotToLua]
    private SoldierDescUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_InitSoldierDescConfigDataSoldierInfoHero_hotfix;
    private LuaFunction m_InitSoldierDescConfigDataSoldierInfoBattleHeroList`1ConfigDataModelSkinResourceInfo_hotfix;
    private LuaFunction m_SetSoldierDetailPanelConfigDataSoldierInfoHeroPropertyComputerString_hotfix;
    private LuaFunction m_CalcPropValueInt32Int32Boolean_hotfix;
    private LuaFunction m_ShowPanel_hotfix;
    private LuaFunction m_ClosePanel_hotfix;
    private LuaFunction m_CreateSpineGraphicStringSingleVector2Int32GameObjectList`1_hotfix;
    private LuaFunction m_DestroySpineGraphicUISpineGraphic_hotfix;
    private LuaFunction m_add_EventOnCloseAction_hotfix;
    private LuaFunction m_remove_EventOnCloseAction_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitSoldierDesc(ConfigDataSoldierInfo soldierInfo, Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitSoldierDesc(
      ConfigDataSoldierInfo soldierInfo,
      BattleHero hero,
      List<TrainingTech> techs,
      ConfigDataModelSkinResourceInfo soldierSkinResInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSoldierDetailPanel(
      ConfigDataSoldierInfo soldierInfo,
      HeroPropertyComputer computer,
      string assetName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string CalcPropValue(int v0, int v1, bool isAdd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClosePanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UISpineGraphic CreateSpineGraphic(
      string assetName,
      float scale,
      Vector2 offset,
      int team,
      GameObject parent,
      List<ReplaceAnim> replaceAnims)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DestroySpineGraphic(UISpineGraphic g)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public SoldierDescUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnClose()
    {
      this.EventOnClose = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private SoldierDescUIController m_owner;

      public LuaExportHelper(SoldierDescUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnClose()
      {
        this.m_owner.__callDele_EventOnClose();
      }

      public void __clearDele_EventOnClose()
      {
        this.m_owner.__clearDele_EventOnClose();
      }

      public Image m_soldierIconImg
      {
        get
        {
          return this.m_owner.m_soldierIconImg;
        }
        set
        {
          this.m_owner.m_soldierIconImg = value;
        }
      }

      public GameObject m_soldierDetailGraphic
      {
        get
        {
          return this.m_owner.m_soldierDetailGraphic;
        }
        set
        {
          this.m_owner.m_soldierDetailGraphic = value;
        }
      }

      public Text m_soldierRangeText
      {
        get
        {
          return this.m_owner.m_soldierRangeText;
        }
        set
        {
          this.m_owner.m_soldierRangeText = value;
        }
      }

      public Text m_soldierMoveText
      {
        get
        {
          return this.m_owner.m_soldierMoveText;
        }
        set
        {
          this.m_owner.m_soldierMoveText = value;
        }
      }

      public Image m_typeBgImg
      {
        get
        {
          return this.m_owner.m_typeBgImg;
        }
        set
        {
          this.m_owner.m_typeBgImg = value;
        }
      }

      public Image m_typeBgImg2
      {
        get
        {
          return this.m_owner.m_typeBgImg2;
        }
        set
        {
          this.m_owner.m_typeBgImg2 = value;
        }
      }

      public Text m_soldierTitleText
      {
        get
        {
          return this.m_owner.m_soldierTitleText;
        }
        set
        {
          this.m_owner.m_soldierTitleText = value;
        }
      }

      public Text m_soldierDescText
      {
        get
        {
          return this.m_owner.m_soldierDescText;
        }
        set
        {
          this.m_owner.m_soldierDescText = value;
        }
      }

      public Text m_soldierWeakText
      {
        get
        {
          return this.m_owner.m_soldierWeakText;
        }
        set
        {
          this.m_owner.m_soldierWeakText = value;
        }
      }

      public Text m_soldierStrongText
      {
        get
        {
          return this.m_owner.m_soldierStrongText;
        }
        set
        {
          this.m_owner.m_soldierStrongText = value;
        }
      }

      public Text m_soldierPropHPValueText
      {
        get
        {
          return this.m_owner.m_soldierPropHPValueText;
        }
        set
        {
          this.m_owner.m_soldierPropHPValueText = value;
        }
      }

      public Text m_soldierPropDFValueText
      {
        get
        {
          return this.m_owner.m_soldierPropDFValueText;
        }
        set
        {
          this.m_owner.m_soldierPropDFValueText = value;
        }
      }

      public Text m_soldierPropATValueText
      {
        get
        {
          return this.m_owner.m_soldierPropATValueText;
        }
        set
        {
          this.m_owner.m_soldierPropATValueText = value;
        }
      }

      public Text m_soldierPropMagicDFValueText
      {
        get
        {
          return this.m_owner.m_soldierPropMagicDFValueText;
        }
        set
        {
          this.m_owner.m_soldierPropMagicDFValueText = value;
        }
      }

      public Text m_soldierPropHPAddText
      {
        get
        {
          return this.m_owner.m_soldierPropHPAddText;
        }
        set
        {
          this.m_owner.m_soldierPropHPAddText = value;
        }
      }

      public Text m_soldierPropDFAddText
      {
        get
        {
          return this.m_owner.m_soldierPropDFAddText;
        }
        set
        {
          this.m_owner.m_soldierPropDFAddText = value;
        }
      }

      public Text m_soldierPropATAddText
      {
        get
        {
          return this.m_owner.m_soldierPropATAddText;
        }
        set
        {
          this.m_owner.m_soldierPropATAddText = value;
        }
      }

      public Text m_soldierPropMagicDFAddText
      {
        get
        {
          return this.m_owner.m_soldierPropMagicDFAddText;
        }
        set
        {
          this.m_owner.m_soldierPropMagicDFAddText = value;
        }
      }

      public UISpineGraphic m_soldierGraphic
      {
        get
        {
          return this.m_owner.m_soldierGraphic;
        }
        set
        {
          this.m_owner.m_soldierGraphic = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void SetSoldierDetailPanel(
        ConfigDataSoldierInfo soldierInfo,
        HeroPropertyComputer computer,
        string assetName)
      {
        this.m_owner.SetSoldierDetailPanel(soldierInfo, computer, assetName);
      }

      public string CalcPropValue(int v0, int v1, bool isAdd)
      {
        return this.m_owner.CalcPropValue(v0, v1, isAdd);
      }

      public void ShowPanel()
      {
        this.m_owner.ShowPanel();
      }

      public void ClosePanel()
      {
        this.m_owner.ClosePanel();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UISpineGraphic CreateSpineGraphic(
        string assetName,
        float scale,
        Vector2 offset,
        int team,
        GameObject parent,
        List<ReplaceAnim> replaceAnims)
      {
        // ISSUE: unable to decompile the method.
      }

      public void DestroySpineGraphic(UISpineGraphic g)
      {
        this.m_owner.DestroySpineGraphic(g);
      }
    }
  }
}
