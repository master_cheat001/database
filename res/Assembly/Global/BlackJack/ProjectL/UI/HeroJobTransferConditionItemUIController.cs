﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroJobTransferConditionItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using SLua;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class HeroJobTransferConditionItemUIController : UIControllerBase
  {
    [AutoBind("./Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_text;
    [AutoBind("./FinishTag", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_finishTag;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitConditionItem(ConfigDataJobUnlockConditionInfo condition, HeroJob heroJob)
    {
      // ISSUE: unable to decompile the method.
    }

    public ConfigDataJobUnlockConditionInfo ConditionInfo { private set; get; }
  }
}
