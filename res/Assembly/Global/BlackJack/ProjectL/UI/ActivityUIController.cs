﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ActivityUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using Assets.Script.ProjectL.Runtime.UI.Activity;
using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class ActivityUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_activityStateCtrl;
    [AutoBind("./Prefab/ToggleItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_toggleItemPrefab;
    [AutoBind("./Prefab/ToggleItem/ItemContent/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_activityNameText;
    [AutoBind("./Prefab/ToggleItem/ItemContent/NameTextDark", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_activityNameDarkText;
    [AutoBind("./Prefab/ToggleItem/ItemContent/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_tabIcon;
    [AutoBind("./Prefab/ToggleItem/ItemContent/NewLogo", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_tabNewLogo;
    [AutoBind("./Prefab/ToggleItem/ItemContent/RedPoint", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_tabRedPoint;
    [AutoBind("./LeftSidebar/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_leftSidebarScrollView;
    [AutoBind("./LeftSidebar/ScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_leftSidebarScrollViewContent;
    [AutoBind("./Right/AnnouncementPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_announcementPanelObj;
    [AutoBind("./Right/AnnouncementPanel/ScrollView/Viewport/Content/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_announcementContentText;
    [AutoBind("./Right/AnnouncementPanel/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_announcementScrollView;
    [AutoBind("./Right/ActivityPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_activityPanelObj;
    [AutoBind("./Right/ActivityPanel/DetaileScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_activityScrollView;
    [AutoBind("./Right/ActivityPanel/DetaileScrollView/Viewport/Content/Time/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_activityTimeText;
    [AutoBind("./Right/ActivityPanel/DetaileScrollView/Viewport/Content/Time/ValueText2", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_activityTimeText2;
    [AutoBind("./Right/ActivityPanel/DetaileScrollView/Viewport/Content/Detail/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_activityContentText;
    [AutoBind("./Right/ActivityPanel/DetaileScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_activityPanelStateCtrl;
    [AutoBind("./Right/ActivityPanel/DetaileScrollView/Viewport/Content/BillboardImage/BGImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_activityBillboardImage;
    [AutoBind("./Right/ActivityPanel/DetaileScrollView/Viewport/Content/ActivityRewardGroup/Title", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_activityRewardTitleObj;
    [AutoBind("./Right/ActivityPanel/DetaileScrollView/Viewport/Content/ActivityRewardGroup/ActivityRewardList", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_activityRewardList;
    [AutoBind("./Prefab/ActivityReward", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_activityRewardPrefab;
    [AutoBind("./Right/ActivityPanel/GoToButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_goButton;
    [AutoBind("./Right/ActivityPanel/GetRewardButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_getRewardButton;
    [AutoBind("./Right/ActivityPanel/ReceivedReward", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_ReceivedRewardObj;
    [AutoBind("./Right/ActivityPanel/BigImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_BigImage;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./ActivityGM", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_activityGMObj;
    [AutoBind("./ActivityGM/ActivityInputField", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_activityInputField;
    [AutoBind("./ActivityGM/AddActivityButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_addActivityButton;
    public bool m_activityIsOn;
    private int announceCount;
    private OperationalActivityBase m_currentActivity;
    private List<GameObject> m_tabList;
    private List<ulong> m_instanceIDList;
    private List<ulong> m_readAnnounceActivityList;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    [DoNotToLua]
    private ActivityUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_Open_hotfix;
    private LuaFunction m_UpdateViewInActivity_hotfix;
    private LuaFunction m_OpenSpecificActivityInt32_hotfix;
    private LuaFunction m_SetTab_hotfix;
    private LuaFunction m_IsActivityInOperationPeriodOperationalActivityBase_hotfix;
    private LuaFunction m_SetCurrentAnnouncementAnnouncement_hotfix;
    private LuaFunction m_SetCurrentActivityBoolean_hotfix;
    private LuaFunction m_SetTabRedPoint_hotfix;
    private LuaFunction m_SetActivityRewardPanelOperationalActivityBase_hotfix;
    private LuaFunction m_SetRewardListPanelOperationalActivityBase_hotfix;
    private LuaFunction m_SetRaffleRewardPanelOperationalActivityBase_hotfix;
    private LuaFunction m_GainRaffleRewardLevelInfoRafflePool_hotfix;
    private LuaFunction m_GetRedeemContent_hotfix;
    private LuaFunction m_AnnouncementComparerAnnouncementAnnouncement_hotfix;
    private LuaFunction m_ActivityComparerOperationalActivityBaseOperationalActivityBase_hotfix;
    private LuaFunction m_OnGainRewardButtonClickUInt64Int32ActivityRewardUIController_hotfix;
    private LuaFunction m_OnExchangeItemGroupButtonClickUInt64Int32ActivityRewardUIController_hotfix;
    private LuaFunction m_OnExchangeItemGroupCrystalNotEnough_hotfix;
    private LuaFunction m_OnReturnButtonClick_hotfix;
    private LuaFunction m_OnAddActivityButtonClick_hotfix;
    private LuaFunction m_OnGotoButtonClick_hotfix;
    private LuaFunction m_OnGetRewardButtonClick_hotfix;
    private LuaFunction m_add_EventOnReturnAction_hotfix;
    private LuaFunction m_remove_EventOnReturnAction_hotfix;
    private LuaFunction m_add_EventOnGainRewardAction`3_hotfix;
    private LuaFunction m_remove_EventOnGainRewardAction`3_hotfix;
    private LuaFunction m_add_EventOnExchangeItemGroupAction`3_hotfix;
    private LuaFunction m_remove_EventOnExchangeItemGroupAction`3_hotfix;
    private LuaFunction m_add_EventOnAddActivityAction`1_hotfix;
    private LuaFunction m_remove_EventOnAddActivityAction`1_hotfix;
    private LuaFunction m_add_EventOnGoToButtonClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnGoToButtonClickAction`1_hotfix;
    private LuaFunction m_add_EventOnGetRewardButtonClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnGetRewardButtonClickAction`1_hotfix;
    private LuaFunction m_add_EventOnCrystalNotEnoughAction_hotfix;
    private LuaFunction m_remove_EventOnCrystalNotEnoughAction_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ActivityUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateViewInActivity()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OpenSpecificActivity(int activityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetTab()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsActivityInOperationPeriod(OperationalActivityBase activity)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetCurrentAnnouncement(Announcement currAnnouncement)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCurrentActivity(bool needRefreshToTop)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetTabRedPoint()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetActivityRewardPanel(OperationalActivityBase activity)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetRewardListPanel(OperationalActivityBase activity)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetRaffleRewardPanel(OperationalActivityBase activity)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected Dictionary<int, List<RaffleItem>> GainRaffleRewardLevelInfo(
      RafflePool rafflePool)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string GetRedeemContent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int AnnouncementComparer(Announcement announcementA, Announcement announcementB)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int ActivityComparer(
      OperationalActivityBase activityA,
      OperationalActivityBase activityB)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGainRewardButtonClick(
      ulong activityInstanceID,
      int rewardIndex,
      ActivityRewardUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnExchangeItemGroupButtonClick(
      ulong activityInstanceID,
      int itemGroupIndex,
      ActivityRewardUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnExchangeItemGroupCrystalNotEnough()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddActivityButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnGotoButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnGetRewardButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ulong, int, List<Goods>> EventOnGainReward
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ulong, int, List<Goods>> EventOnExchangeItemGroup
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string> EventOnAddActivity
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<OperationalActivityBase> EventOnGoToButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<OperationalActivityBase> EventOnGetRewardButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnCrystalNotEnough
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public ActivityUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnReturn()
    {
      this.EventOnReturn = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnGainReward(ulong arg1, int arg2, List<Goods> arg3)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnGainReward(ulong arg1, int arg2, List<Goods> arg3)
    {
      this.EventOnGainReward = (Action<ulong, int, List<Goods>>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnExchangeItemGroup(ulong arg1, int arg2, List<Goods> arg3)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnExchangeItemGroup(ulong arg1, int arg2, List<Goods> arg3)
    {
      this.EventOnExchangeItemGroup = (Action<ulong, int, List<Goods>>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnAddActivity(string obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnAddActivity(string obj)
    {
      this.EventOnAddActivity = (Action<string>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnGoToButtonClick(OperationalActivityBase obj)
    {
    }

    private void __clearDele_EventOnGoToButtonClick(OperationalActivityBase obj)
    {
      this.EventOnGoToButtonClick = (Action<OperationalActivityBase>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnGetRewardButtonClick(OperationalActivityBase obj)
    {
    }

    private void __clearDele_EventOnGetRewardButtonClick(OperationalActivityBase obj)
    {
      this.EventOnGetRewardButtonClick = (Action<OperationalActivityBase>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnCrystalNotEnough()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnCrystalNotEnough()
    {
      this.EventOnCrystalNotEnough = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private ActivityUIController m_owner;

      public LuaExportHelper(ActivityUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnReturn()
      {
        this.m_owner.__callDele_EventOnReturn();
      }

      public void __clearDele_EventOnReturn()
      {
        this.m_owner.__clearDele_EventOnReturn();
      }

      public void __callDele_EventOnGainReward(ulong arg1, int arg2, List<Goods> arg3)
      {
        this.m_owner.__callDele_EventOnGainReward(arg1, arg2, arg3);
      }

      public void __clearDele_EventOnGainReward(ulong arg1, int arg2, List<Goods> arg3)
      {
        this.m_owner.__clearDele_EventOnGainReward(arg1, arg2, arg3);
      }

      public void __callDele_EventOnExchangeItemGroup(ulong arg1, int arg2, List<Goods> arg3)
      {
        this.m_owner.__callDele_EventOnExchangeItemGroup(arg1, arg2, arg3);
      }

      public void __clearDele_EventOnExchangeItemGroup(ulong arg1, int arg2, List<Goods> arg3)
      {
        this.m_owner.__clearDele_EventOnExchangeItemGroup(arg1, arg2, arg3);
      }

      public void __callDele_EventOnAddActivity(string obj)
      {
        this.m_owner.__callDele_EventOnAddActivity(obj);
      }

      public void __clearDele_EventOnAddActivity(string obj)
      {
        this.m_owner.__clearDele_EventOnAddActivity(obj);
      }

      public void __callDele_EventOnGoToButtonClick(OperationalActivityBase obj)
      {
        this.m_owner.__callDele_EventOnGoToButtonClick(obj);
      }

      public void __clearDele_EventOnGoToButtonClick(OperationalActivityBase obj)
      {
        this.m_owner.__clearDele_EventOnGoToButtonClick(obj);
      }

      public void __callDele_EventOnGetRewardButtonClick(OperationalActivityBase obj)
      {
        this.m_owner.__callDele_EventOnGetRewardButtonClick(obj);
      }

      public void __clearDele_EventOnGetRewardButtonClick(OperationalActivityBase obj)
      {
        this.m_owner.__clearDele_EventOnGetRewardButtonClick(obj);
      }

      public void __callDele_EventOnCrystalNotEnough()
      {
        this.m_owner.__callDele_EventOnCrystalNotEnough();
      }

      public void __clearDele_EventOnCrystalNotEnough()
      {
        this.m_owner.__clearDele_EventOnCrystalNotEnough();
      }

      public CommonUIStateController m_activityStateCtrl
      {
        get
        {
          return this.m_owner.m_activityStateCtrl;
        }
        set
        {
          this.m_owner.m_activityStateCtrl = value;
        }
      }

      public GameObject m_toggleItemPrefab
      {
        get
        {
          return this.m_owner.m_toggleItemPrefab;
        }
        set
        {
          this.m_owner.m_toggleItemPrefab = value;
        }
      }

      public Text m_activityNameText
      {
        get
        {
          return this.m_owner.m_activityNameText;
        }
        set
        {
          this.m_owner.m_activityNameText = value;
        }
      }

      public Text m_activityNameDarkText
      {
        get
        {
          return this.m_owner.m_activityNameDarkText;
        }
        set
        {
          this.m_owner.m_activityNameDarkText = value;
        }
      }

      public Image m_tabIcon
      {
        get
        {
          return this.m_owner.m_tabIcon;
        }
        set
        {
          this.m_owner.m_tabIcon = value;
        }
      }

      public GameObject m_tabNewLogo
      {
        get
        {
          return this.m_owner.m_tabNewLogo;
        }
        set
        {
          this.m_owner.m_tabNewLogo = value;
        }
      }

      public GameObject m_tabRedPoint
      {
        get
        {
          return this.m_owner.m_tabRedPoint;
        }
        set
        {
          this.m_owner.m_tabRedPoint = value;
        }
      }

      public GameObject m_leftSidebarScrollView
      {
        get
        {
          return this.m_owner.m_leftSidebarScrollView;
        }
        set
        {
          this.m_owner.m_leftSidebarScrollView = value;
        }
      }

      public GameObject m_leftSidebarScrollViewContent
      {
        get
        {
          return this.m_owner.m_leftSidebarScrollViewContent;
        }
        set
        {
          this.m_owner.m_leftSidebarScrollViewContent = value;
        }
      }

      public GameObject m_announcementPanelObj
      {
        get
        {
          return this.m_owner.m_announcementPanelObj;
        }
        set
        {
          this.m_owner.m_announcementPanelObj = value;
        }
      }

      public Text m_announcementContentText
      {
        get
        {
          return this.m_owner.m_announcementContentText;
        }
        set
        {
          this.m_owner.m_announcementContentText = value;
        }
      }

      public GameObject m_announcementScrollView
      {
        get
        {
          return this.m_owner.m_announcementScrollView;
        }
        set
        {
          this.m_owner.m_announcementScrollView = value;
        }
      }

      public GameObject m_activityPanelObj
      {
        get
        {
          return this.m_owner.m_activityPanelObj;
        }
        set
        {
          this.m_owner.m_activityPanelObj = value;
        }
      }

      public GameObject m_activityScrollView
      {
        get
        {
          return this.m_owner.m_activityScrollView;
        }
        set
        {
          this.m_owner.m_activityScrollView = value;
        }
      }

      public Text m_activityTimeText
      {
        get
        {
          return this.m_owner.m_activityTimeText;
        }
        set
        {
          this.m_owner.m_activityTimeText = value;
        }
      }

      public Text m_activityTimeText2
      {
        get
        {
          return this.m_owner.m_activityTimeText2;
        }
        set
        {
          this.m_owner.m_activityTimeText2 = value;
        }
      }

      public Text m_activityContentText
      {
        get
        {
          return this.m_owner.m_activityContentText;
        }
        set
        {
          this.m_owner.m_activityContentText = value;
        }
      }

      public CommonUIStateController m_activityPanelStateCtrl
      {
        get
        {
          return this.m_owner.m_activityPanelStateCtrl;
        }
        set
        {
          this.m_owner.m_activityPanelStateCtrl = value;
        }
      }

      public Image m_activityBillboardImage
      {
        get
        {
          return this.m_owner.m_activityBillboardImage;
        }
        set
        {
          this.m_owner.m_activityBillboardImage = value;
        }
      }

      public GameObject m_activityRewardTitleObj
      {
        get
        {
          return this.m_owner.m_activityRewardTitleObj;
        }
        set
        {
          this.m_owner.m_activityRewardTitleObj = value;
        }
      }

      public GameObject m_activityRewardList
      {
        get
        {
          return this.m_owner.m_activityRewardList;
        }
        set
        {
          this.m_owner.m_activityRewardList = value;
        }
      }

      public GameObject m_activityRewardPrefab
      {
        get
        {
          return this.m_owner.m_activityRewardPrefab;
        }
        set
        {
          this.m_owner.m_activityRewardPrefab = value;
        }
      }

      public Button m_goButton
      {
        get
        {
          return this.m_owner.m_goButton;
        }
        set
        {
          this.m_owner.m_goButton = value;
        }
      }

      public Button m_getRewardButton
      {
        get
        {
          return this.m_owner.m_getRewardButton;
        }
        set
        {
          this.m_owner.m_getRewardButton = value;
        }
      }

      public GameObject m_ReceivedRewardObj
      {
        get
        {
          return this.m_owner.m_ReceivedRewardObj;
        }
        set
        {
          this.m_owner.m_ReceivedRewardObj = value;
        }
      }

      public Image m_BigImage
      {
        get
        {
          return this.m_owner.m_BigImage;
        }
        set
        {
          this.m_owner.m_BigImage = value;
        }
      }

      public Button m_returnButton
      {
        get
        {
          return this.m_owner.m_returnButton;
        }
        set
        {
          this.m_owner.m_returnButton = value;
        }
      }

      public GameObject m_activityGMObj
      {
        get
        {
          return this.m_owner.m_activityGMObj;
        }
        set
        {
          this.m_owner.m_activityGMObj = value;
        }
      }

      public InputField m_activityInputField
      {
        get
        {
          return this.m_owner.m_activityInputField;
        }
        set
        {
          this.m_owner.m_activityInputField = value;
        }
      }

      public Button m_addActivityButton
      {
        get
        {
          return this.m_owner.m_addActivityButton;
        }
        set
        {
          this.m_owner.m_addActivityButton = value;
        }
      }

      public int announceCount
      {
        get
        {
          return this.m_owner.announceCount;
        }
        set
        {
          this.m_owner.announceCount = value;
        }
      }

      public OperationalActivityBase m_currentActivity
      {
        get
        {
          return this.m_owner.m_currentActivity;
        }
        set
        {
          this.m_owner.m_currentActivity = value;
        }
      }

      public List<GameObject> m_tabList
      {
        get
        {
          return this.m_owner.m_tabList;
        }
        set
        {
          this.m_owner.m_tabList = value;
        }
      }

      public List<ulong> m_instanceIDList
      {
        get
        {
          return this.m_owner.m_instanceIDList;
        }
        set
        {
          this.m_owner.m_instanceIDList = value;
        }
      }

      public List<ulong> m_readAnnounceActivityList
      {
        get
        {
          return this.m_owner.m_readAnnounceActivityList;
        }
        set
        {
          this.m_owner.m_readAnnounceActivityList = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void SetTab()
      {
        this.m_owner.SetTab();
      }

      public bool IsActivityInOperationPeriod(OperationalActivityBase activity)
      {
        return this.m_owner.IsActivityInOperationPeriod(activity);
      }

      public void SetCurrentAnnouncement(Announcement currAnnouncement)
      {
        this.m_owner.SetCurrentAnnouncement(currAnnouncement);
      }

      public void SetTabRedPoint()
      {
        this.m_owner.SetTabRedPoint();
      }

      public void SetActivityRewardPanel(OperationalActivityBase activity)
      {
        this.m_owner.SetActivityRewardPanel(activity);
      }

      public void SetRewardListPanel(OperationalActivityBase activity)
      {
        this.m_owner.SetRewardListPanel(activity);
      }

      public void SetRaffleRewardPanel(OperationalActivityBase activity)
      {
        this.m_owner.SetRaffleRewardPanel(activity);
      }

      public Dictionary<int, List<RaffleItem>> GainRaffleRewardLevelInfo(
        RafflePool rafflePool)
      {
        return this.m_owner.GainRaffleRewardLevelInfo(rafflePool);
      }

      public string GetRedeemContent()
      {
        return this.m_owner.GetRedeemContent();
      }

      public int AnnouncementComparer(Announcement announcementA, Announcement announcementB)
      {
        return this.m_owner.AnnouncementComparer(announcementA, announcementB);
      }

      public int ActivityComparer(
        OperationalActivityBase activityA,
        OperationalActivityBase activityB)
      {
        return this.m_owner.ActivityComparer(activityA, activityB);
      }

      public void OnGainRewardButtonClick(
        ulong activityInstanceID,
        int rewardIndex,
        ActivityRewardUIController ctrl)
      {
        this.m_owner.OnGainRewardButtonClick(activityInstanceID, rewardIndex, ctrl);
      }

      public void OnExchangeItemGroupButtonClick(
        ulong activityInstanceID,
        int itemGroupIndex,
        ActivityRewardUIController ctrl)
      {
        this.m_owner.OnExchangeItemGroupButtonClick(activityInstanceID, itemGroupIndex, ctrl);
      }

      public void OnExchangeItemGroupCrystalNotEnough()
      {
        this.m_owner.OnExchangeItemGroupCrystalNotEnough();
      }

      public void OnReturnButtonClick()
      {
        this.m_owner.OnReturnButtonClick();
      }

      public void OnAddActivityButtonClick()
      {
        this.m_owner.OnAddActivityButtonClick();
      }

      public void OnGotoButtonClick()
      {
        this.m_owner.OnGotoButtonClick();
      }

      public void OnGetRewardButtonClick()
      {
        this.m_owner.OnGetRewardButtonClick();
      }
    }
  }
}
