﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.GoddessDialogUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime;
using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Misc;
using SLua;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class GoddessDialogUIController : UIControllerBase
  {
    [AutoBind("./BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_backgroundButton;
    [AutoBind("./Words", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_wordsGameObject;
    [AutoBind("./Char/0", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_charGameObject;
    [AutoBind("./Choice", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_choiceUIStateController;
    [AutoBind("./Choice/1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_choice1GameObject;
    [AutoBind("./Choice/2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_choice2GameObject;
    [AutoBind("./Choice/3", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_choice3GameObject;
    [AutoBind("./Job", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobGameObject;
    [AutoBind("./Job/Panel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_jobUIStateController;
    [AutoBind("./Job/Panel/HeroIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroIconImage;
    [AutoBind("./Job/Panel/HeroNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroNameText;
    [AutoBind("./Job/Panel/Job1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_job1GameObject;
    [AutoBind("./Job/Panel/Job2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_job2GameObject;
    [AutoBind("./Job/Panel/ButtonGroup1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobButtonGroup1GameObject;
    [AutoBind("./Job/Panel/ButtonGroup1/AcceptButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_acceptButton;
    [AutoBind("./Job/Panel/ButtonGroup1/RetryButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_retryButton;
    [AutoBind("./Job/Panel/ButtonGroup1/SelectButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_selectButton;
    [AutoBind("./Job/Panel/ButtonGroup2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobButtonGroup2GameObject;
    [AutoBind("./Job/Panel/ButtonGroup2/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_confirmButton;
    [AutoBind("./Job/Panel/ButtonGroup2/RetryButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_retryButton2;
    [AutoBind("./Job/Panel/ButtonGroup2/PrevButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_prevButton;
    [AutoBind("./Job/Panel/ButtonGroup2/NextButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_nextButton;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    private DialogCharUIController m_dialogCharUIController;
    private GoddessDialogBoxUIController m_dialogBoxUIController;
    private GoddessDialogChoiceButton[] m_choiceButtons;
    private GoddessDialogJobButton[] m_jobButtons;
    private ConfigDataGoddessDialogInfo m_dialogInfo;
    private List<string> m_feedbackVoices;
    private List<string> m_feedbackTexts;
    private bool m_isClickBackground;
    private bool m_isDoingChoose;

    [MethodImpl((MethodImplOptions) 32768)]
    private GoddessDialogUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Start()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CreateChar(ConfigDataCharImageInfo charImageInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDialog(ConfigDataGoddessDialogInfo dialogInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    public void CloseDialog()
    {
      this.StartCoroutine(this.Co_CloseDialog());
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowJobs(
      ConfigDataHeroInfo heroInfo,
      List<ConfigDataJobConnectionInfo> jobConnectionInfos,
      bool hasPrev,
      bool hasNext)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideJobConnections()
    {
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_CloseDialog()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_ShowWords(float delay)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowChoice()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_DoChoice(int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_WaitForSecondsOrClick(float time)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_WaitForClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_WaitForAllWordsDisplayed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool HasChoice()
    {
      // ISSUE: unable to decompile the method.
    }

    private IAudioPlayback PlayVoice(string name)
    {
      return AudioUtility.PlaySound(name);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAcceptButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRetryButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSelectButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnConfirmButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRetryButton2Click()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPrevButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnNextButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChoiceButtonClick(GoddessDialogChoiceButton b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnJobButtonClick(GoddessDialogJobButton b)
    {
    }

    public event Action EventOnNext
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    public event Action<int> EventOnChoose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    public event Action<ConfigDataJobConnectionInfo> EventOnShowJobInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    public event Action EventOnConfirmJob
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    public event Action EventOnRetry
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    public event Action EventOnSelectJob
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    public event Action EventOnPrevJob
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }

    public event Action EventOnNextJob
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }
  }
}
