﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.FettersConfessionUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class FettersConfessionUIController : UIControllerBase
  {
    [AutoBind("./Margin", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_commonUIStateCtrl;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./HelpButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_helpButton;
    [AutoBind("./SkillPanel/Skill5Group", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillGroupContent;
    [AutoBind("./SkillPanel/CenterEvent", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_centerHeartStateCtrl;
    [AutoBind("./SkillPanel/CenterEvent", AutoBindAttribute.InitState.NotInit, false)]
    private ButtonEx m_confessionHeartButton;
    [AutoBind("./SkillPanel/CenterEvent/SelectImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_confessionHeartButtonSelectImage;
    [AutoBind("./SkillPanel/CenterEvent/Female", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_confessionHeartFemaleImage;
    [AutoBind("./SkillPanel/CenterEvent/Male", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_confessionHeartMaleImage;
    [AutoBind("./Margin/SpiritEventInfoPanel/DetailAlphaTweenState", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_detailPanelTweenStateStateCtrl;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_skillDetailStateCtrl;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/SkillInfoGroup/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_skillDetailInfoIconImage;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/SkillInfoGroup/LockIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillDetailInfoLockIconGo;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/SkillInfoGroup/SkillNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_skillDetailInfoSkillNameText;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/SkillInfoGroup/LvGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_skillDetailInfoSkillLvText;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/SkillInfoGroup/LvGroup/Max", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillDetailInfoSkillLvMaxText;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/LockAndUnlockState/ConditionGroup/FetterEffect/UnlockInfoText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_skillDetailDescText;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/LockAndUnlockState/ConditionGroup/UnlockCondition/ConditionGroup/UnlockInfo1", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_skillDetailCondition1StateCtrl;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/LockAndUnlockState/ConditionGroup/UnlockCondition/ConditionGroup/UnlockInfo1/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_skillDetailConditionUnlockInfoText1;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/LockAndUnlockState/ConditionGroup/UnlockCondition/ConditionGroup/UnlockInfo2", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_skillDetailCondition2StateCtrl;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/LockAndUnlockState/ConditionGroup/UnlockCondition/ConditionGroup/UnlockInfo2/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_skillDetailConditionUnlockInfoText2;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/LockAndUnlockState/UnlockReward/ItemDummy/RewardGoods", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillDetailUnlockRewardGoods;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/LockAndUnlockState/UnlockReward/Button", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_skillDetailUnlockButton;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/LockAndUnlockState/UnlockReward/Button", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_skillDetailUnlockButtonStateCtrl;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/LockAndUnlockState/UnlockReward/Button/Grey", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_skillDetailUnlockGreyButton;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/LockAndUnlockState/UnlockReward/ItemNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_skillDetailUnlockRewardNameText;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/PromoteState", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_skillDetailUnlockStateCtrl;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/PromoteState/NowInfoText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_skillDetailUnlockStateNowInfoText;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/PromoteState/AfterInfoPanel/AfterText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_skillDetailUnlockStateAfterText;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/PromoteState/AfterInfoPanel/ConsumeValueText", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_skillDetailUnlockStateConsumeTextStateCtrl;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/PromoteState/AfterInfoPanel/ConsumeValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_skillDetailUnlockStateConsumeValueText;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/PromoteState/AfterInfoPanel/EvolutionMaterial1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillDetailUnlockEvolutionMaterial1;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/PromoteState/AfterInfoPanel/EvolutionMaterial2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillDetailUnlockEvolutionMaterial2;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/PromoteState/AfterInfoPanel/EvolutionMaterial1", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_skillDetailUnlockEvolutionMaterialStateCtrl1;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/PromoteState/AfterInfoPanel/EvolutionMaterial2", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_skillDetailUnlockEvolutionMaterialStateCtrl2;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/PromoteState/AfterInfoPanel/EvolutionButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_skillDetailUnlockStateEvolutionButton;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/PromoteState/AfterInfoPanel/EvolutionButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_skillDetailUnlockStateEvolutionButtonStateController;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/EnhanceSuccessEffectPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_skillDetailEnhanceSuccessEffectPanelStateCtrl;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/ConfessionAndReward", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_skillDetailConfessionAndRewardStateCtrl;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/ConfessionAndReward/ConditionScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillDetailConfessionAndRewardScrollViewContent;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/ConfessionAndReward/Female/RewardGroup/Button", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_skillDetailConfessionAndRewardFemaleButton;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/ConfessionAndReward/Female/RewardGroup/ItemDummy/RewardGoods", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillDetailConfessionAndRewardFemaleRewardGoods;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/ConfessionAndReward/Female/RewardGroup/ItemNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_skillDetailConfessionAndRewardFemaleRewardGoodsNameText;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/ConfessionAndReward/Male/RewardGroup/Button", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_skillDetailConfessionAndRewardMaleButton;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/ConfessionAndReward/Male/RewardGroup/ItemDummy/RewardGoods", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillDetailConfessionAndRewardMaleRewardGoods;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/ConfessionAndReward/Male/RewardGroup/ItemNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_skillDetailConfessionAndRewardMaleRewardGoodsNameText;
    [AutoBind("./Prefab/Condition", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillDetailUnlockStateConditionPrefab;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/HeartInfoGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_heartFetterSexInfoStateCtrl;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/HeartInfoGroup/SkillLevelGroup/LvText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heartFetterLvText;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/HeartInfoGroup/SkillLevelGroup/Max", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heartFetterMaxLvTextGo;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/HeartInfoGroup/Male", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heartFetterLvMaleHeartImage;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/HeartInfoGroup/Female", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heartFetterLvFemaleHeartImage;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/HeartInfoGroup/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
    private Slider m_heartFetterLvProgressSlider;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/HeartInfoGroup/Skill1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heartFetterSkill1;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/HeartInfoGroup/Skill2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heartFetterSkill2;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/HeartInfoGroup/Skill3", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heartFetterSkill3;
    [AutoBind("./Margin/SkillDescPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_heartFetterSkillDescPanelStateCtrl;
    [AutoBind("./Margin/SkillDescPanel/Detail/SkillIcon/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heartFetterSkillDescPanelIconImage;
    [AutoBind("./Margin/SkillDescPanel/Detail/DescInfoScrollView/Viewport/DescInfo/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heartFetterSkillDescPanelDescText;
    [AutoBind("./ConditionPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_unlockHeartFetterConditionPanelStateCtrl;
    [AutoBind("./ConditionPanel/BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_unlockHeartFetterConditionPanelBGButton;
    [AutoBind("./ConditionPanel/Detail/FrameImage/ConditionGroup/Condition1", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_unlockHeartFetterCondition1StateCtrl;
    [AutoBind("./ConditionPanel/Detail/FrameImage/ConditionGroup/Condition1/ConditionText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_unlockHeartFetterCondition1Text;
    [AutoBind("./ConditionPanel/Detail/FrameImage/ConditionGroup/Condition1/NowLevelText/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_unlockHeartFetterCondition1LvText;
    [AutoBind("./ConditionPanel/Detail/FrameImage/ConditionGroup/Condition2", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_unlockHeartFetterCondition2StateCtrl;
    [AutoBind("./ConditionPanel/Detail/FrameImage/ConditionGroup/Condition2/ConditionText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_unlockHeartFetterCondition2Text;
    [AutoBind("./ConditionPanel/Detail/FrameImage/ConditionGroup/Condition2/NowLevelText/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_unlockHeartFetterCondition2LvText;
    [AutoBind("./SkillInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_unlockHeartFetterSkillInfoPanelStateCtrl;
    [AutoBind("./SkillInfoPanel/BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_unlockHeartFetterSkillInfoPanelBGButton;
    [AutoBind("./SkillInfoPanel/Detail/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_unlockHeartFetterSkillInfoPanelIconImage;
    [AutoBind("./SkillInfoPanel/Detail/DescTextScrollView/Mask/Content/DescText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_unlockHeartFetterSkillInfoPanelDescText;
    [AutoBind("./PlayerResource/Crystal/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerResourceCrystalText;
    [AutoBind("./PlayerResource/Crystal/AddButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_playerResourceCrystalAddButton;
    [AutoBind("./PlayerResource/Golden/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerResourceGoldenText;
    [AutoBind("./PlayerResource/Golden/AddButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_playerResourceGoldenAddButton;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/FastLevelButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_fastLevelButton;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/FastLevelButton/InputField", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_fastLevelButtonInputField;
    private Hero m_hero;
    private bool isFirstIn;
    private bool m_isMale;
    private int m_lastHeroHeartFetterLevel;
    private bool m_isDetailPanelInHeartState;
    private FettersConfessionSkillItemUIController m_curFetterSkillCtrl;
    private List<HeartFettersSkillItemUIController> m_heartFettersSkillItemCtrlList;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private HeartFettersSkillItemUIController m_curClickHeartFettersSkillItemUICtrl;
    [DoNotToLua]
    private FettersConfessionUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_OnDisable_hotfix;
    private LuaFunction m_UpdatePlayerResource_hotfix;
    private LuaFunction m_UpdateViewInFettersConfessionHero_hotfix;
    private LuaFunction m_SetCenterHeartFetterInfo_hotfix;
    private LuaFunction m_SetSkillDetailPanelConfigDataHeroFetterInfo_hotfix;
    private LuaFunction m_SetSkillInfoGroupConfigDataHeroFetterInfo_hotfix;
    private LuaFunction m_SetLockStateInfoConfigDataHeroFetterInfo_hotfix;
    private LuaFunction m_GetFettersCoditionDescHeroFetterCompletionCondition_hotfix;
    private LuaFunction m_SetUnlockStateInfoConfigDataHeroFetterInfo_hotfix;
    private LuaFunction m_OnFettersSkillItemClickFettersConfessionSkillItemUIController_hotfix;
    private LuaFunction m_OnConfessionHeartButtonClick_hotfix;
    private LuaFunction m_OnUnlockCenterHeartFetterFinish_hotfix;
    private LuaFunction m_SetHeartFetterDetailPanel_hotfix;
    private LuaFunction m_OnHeartFetterSkillItemClickList`1_hotfix;
    private LuaFunction m_SetUnlockHeartFetterConditionPanel_hotfix;
    private LuaFunction m_CloseHeartFetterUnlockConditionPanel_hotfix;
    private LuaFunction m_ShowHeartFetterSkillInfoPanel_hotfix;
    private LuaFunction m_CloseHeartFetterSkillInfoPanel_hotfix;
    private LuaFunction m_OnEvolutionMaterialClickGoodsTypeInt32Int32_hotfix;
    private LuaFunction m_OnSkillUnlockButtonClick_hotfix;
    private LuaFunction m_OnSkillDetailUnlockGreyButtonClick_hotfix;
    private LuaFunction m_OnUnlockStateEvolutionButtonClcik_hotfix;
    private LuaFunction m_OnEvolutionHeartFetterFinished_hotfix;
    private LuaFunction m_ShowHeartUpdatePerformance_hotfix;
    private LuaFunction m_OnConfessionAndRewardButtonClick_hotfix;
    private LuaFunction m_OnReturnButtonClick_hotfix;
    private LuaFunction m_OnFastLevelButtonClick_hotfix;
    private LuaFunction m_OnHelpButtonClick_hotfix;
    private LuaFunction m_OnGoldAddButtonClick_hotfix;
    private LuaFunction m_OnCrystalAddButtonClick_hotfix;
    private LuaFunction m_add_EventOnReturnAction_hotfix;
    private LuaFunction m_remove_EventOnReturnAction_hotfix;
    private LuaFunction m_add_EventOnShowHelpAction_hotfix;
    private LuaFunction m_remove_EventOnShowHelpAction_hotfix;
    private LuaFunction m_add_EventOnGoldAddAction_hotfix;
    private LuaFunction m_remove_EventOnGoldAddAction_hotfix;
    private LuaFunction m_add_EventOnAddCrystalAction_hotfix;
    private LuaFunction m_remove_EventOnAddCrystalAction_hotfix;
    private LuaFunction m_add_EventOnHeroFetterConfessAction`1_hotfix;
    private LuaFunction m_remove_EventOnHeroFetterConfessAction`1_hotfix;
    private LuaFunction m_add_EventOnEvolutionFetterSkillAction`3_hotfix;
    private LuaFunction m_remove_EventOnEvolutionFetterSkillAction`3_hotfix;
    private LuaFunction m_add_EventOnEvolutionHeartFetterAction`2_hotfix;
    private LuaFunction m_remove_EventOnEvolutionHeartFetterAction`2_hotfix;
    private LuaFunction m_add_EventOnEvolutionMaterialClickAction`3_hotfix;
    private LuaFunction m_remove_EventOnEvolutionMaterialClickAction`3_hotfix;
    private LuaFunction m_add_EventOnSkillUnlockButtonClickAction`3_hotfix;
    private LuaFunction m_remove_EventOnSkillUnlockButtonClickAction`3_hotfix;
    private LuaFunction m_add_EventOnUnlockCenterHeartFetterAction`2_hotfix;
    private LuaFunction m_remove_EventOnUnlockCenterHeartFetterAction`2_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public FettersConfessionUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDisable()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdatePlayerResource()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateViewInFettersConfession(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetCenterHeartFetterInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSkillDetailPanel(ConfigDataHeroFetterInfo heroFetterInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSkillInfoGroup(ConfigDataHeroFetterInfo heroFetterInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetLockStateInfo(ConfigDataHeroFetterInfo heroFetterInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string GetFettersCoditionDesc(HeroFetterCompletionCondition condition)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetUnlockStateInfo(ConfigDataHeroFetterInfo heroFetterInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFettersSkillItemClick(FettersConfessionSkillItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnConfessionHeartButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnUnlockCenterHeartFetterFinish()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetHeartFetterDetailPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeartFetterSkillItemClick(List<int> skillIdList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetUnlockHeartFetterConditionPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseHeartFetterUnlockConditionPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowHeartFetterSkillInfoPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseHeartFetterSkillInfoPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEvolutionMaterialClick(GoodsType goodsType, int id, int needCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSkillUnlockButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSkillDetailUnlockGreyButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnUnlockStateEvolutionButtonClcik()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEvolutionHeartFetterFinished()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator ShowHeartUpdatePerformance()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnConfessionAndRewardButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFastLevelButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHelpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGoldAddButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCrystalAddButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowHelp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnGoldAdd
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnAddCrystal
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnHeroFetterConfess
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int, Action> EventOnEvolutionFetterSkill
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, Action> EventOnEvolutionHeartFetter
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<GoodsType, int, int> EventOnEvolutionMaterialClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int, Action<List<Goods>>> EventOnSkillUnlockButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, Action> EventOnUnlockCenterHeartFetter
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public FettersConfessionUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnReturn()
    {
      this.EventOnReturn = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowHelp()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowHelp()
    {
      this.EventOnShowHelp = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnGoldAdd()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnGoldAdd()
    {
      this.EventOnGoldAdd = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnAddCrystal()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnAddCrystal()
    {
      this.EventOnAddCrystal = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnHeroFetterConfess(int obj)
    {
    }

    private void __clearDele_EventOnHeroFetterConfess(int obj)
    {
      this.EventOnHeroFetterConfess = (Action<int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnEvolutionFetterSkill(int arg1, int arg2, Action arg3)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnEvolutionFetterSkill(int arg1, int arg2, Action arg3)
    {
      this.EventOnEvolutionFetterSkill = (Action<int, int, Action>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnEvolutionHeartFetter(int arg1, Action arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnEvolutionHeartFetter(int arg1, Action arg2)
    {
      this.EventOnEvolutionHeartFetter = (Action<int, Action>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnEvolutionMaterialClick(GoodsType arg1, int arg2, int arg3)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnEvolutionMaterialClick(GoodsType arg1, int arg2, int arg3)
    {
      this.EventOnEvolutionMaterialClick = (Action<GoodsType, int, int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnSkillUnlockButtonClick(
      int arg1,
      int arg2,
      Action<List<Goods>> arg3)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnSkillUnlockButtonClick(
      int arg1,
      int arg2,
      Action<List<Goods>> arg3)
    {
      this.EventOnSkillUnlockButtonClick = (Action<int, int, Action<List<Goods>>>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnUnlockCenterHeartFetter(int arg1, Action arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnUnlockCenterHeartFetter(int arg1, Action arg2)
    {
      this.EventOnUnlockCenterHeartFetter = (Action<int, Action>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private FettersConfessionUIController m_owner;

      public LuaExportHelper(FettersConfessionUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnReturn()
      {
        this.m_owner.__callDele_EventOnReturn();
      }

      public void __clearDele_EventOnReturn()
      {
        this.m_owner.__clearDele_EventOnReturn();
      }

      public void __callDele_EventOnShowHelp()
      {
        this.m_owner.__callDele_EventOnShowHelp();
      }

      public void __clearDele_EventOnShowHelp()
      {
        this.m_owner.__clearDele_EventOnShowHelp();
      }

      public void __callDele_EventOnGoldAdd()
      {
        this.m_owner.__callDele_EventOnGoldAdd();
      }

      public void __clearDele_EventOnGoldAdd()
      {
        this.m_owner.__clearDele_EventOnGoldAdd();
      }

      public void __callDele_EventOnAddCrystal()
      {
        this.m_owner.__callDele_EventOnAddCrystal();
      }

      public void __clearDele_EventOnAddCrystal()
      {
        this.m_owner.__clearDele_EventOnAddCrystal();
      }

      public void __callDele_EventOnHeroFetterConfess(int obj)
      {
        this.m_owner.__callDele_EventOnHeroFetterConfess(obj);
      }

      public void __clearDele_EventOnHeroFetterConfess(int obj)
      {
        this.m_owner.__clearDele_EventOnHeroFetterConfess(obj);
      }

      public void __callDele_EventOnEvolutionFetterSkill(int arg1, int arg2, Action arg3)
      {
        this.m_owner.__callDele_EventOnEvolutionFetterSkill(arg1, arg2, arg3);
      }

      public void __clearDele_EventOnEvolutionFetterSkill(int arg1, int arg2, Action arg3)
      {
        this.m_owner.__clearDele_EventOnEvolutionFetterSkill(arg1, arg2, arg3);
      }

      public void __callDele_EventOnEvolutionHeartFetter(int arg1, Action arg2)
      {
        this.m_owner.__callDele_EventOnEvolutionHeartFetter(arg1, arg2);
      }

      public void __clearDele_EventOnEvolutionHeartFetter(int arg1, Action arg2)
      {
        this.m_owner.__clearDele_EventOnEvolutionHeartFetter(arg1, arg2);
      }

      public void __callDele_EventOnEvolutionMaterialClick(GoodsType arg1, int arg2, int arg3)
      {
        this.m_owner.__callDele_EventOnEvolutionMaterialClick(arg1, arg2, arg3);
      }

      public void __clearDele_EventOnEvolutionMaterialClick(GoodsType arg1, int arg2, int arg3)
      {
        this.m_owner.__clearDele_EventOnEvolutionMaterialClick(arg1, arg2, arg3);
      }

      public void __callDele_EventOnSkillUnlockButtonClick(
        int arg1,
        int arg2,
        Action<List<Goods>> arg3)
      {
        this.m_owner.__callDele_EventOnSkillUnlockButtonClick(arg1, arg2, arg3);
      }

      public void __clearDele_EventOnSkillUnlockButtonClick(
        int arg1,
        int arg2,
        Action<List<Goods>> arg3)
      {
        this.m_owner.__clearDele_EventOnSkillUnlockButtonClick(arg1, arg2, arg3);
      }

      public void __callDele_EventOnUnlockCenterHeartFetter(int arg1, Action arg2)
      {
        this.m_owner.__callDele_EventOnUnlockCenterHeartFetter(arg1, arg2);
      }

      public void __clearDele_EventOnUnlockCenterHeartFetter(int arg1, Action arg2)
      {
        this.m_owner.__clearDele_EventOnUnlockCenterHeartFetter(arg1, arg2);
      }

      public RectTransform m_marginTransform
      {
        get
        {
          return this.m_owner.m_marginTransform;
        }
        set
        {
          this.m_owner.m_marginTransform = value;
        }
      }

      public CommonUIStateController m_commonUIStateCtrl
      {
        get
        {
          return this.m_owner.m_commonUIStateCtrl;
        }
        set
        {
          this.m_owner.m_commonUIStateCtrl = value;
        }
      }

      public Button m_returnButton
      {
        get
        {
          return this.m_owner.m_returnButton;
        }
        set
        {
          this.m_owner.m_returnButton = value;
        }
      }

      public Button m_helpButton
      {
        get
        {
          return this.m_owner.m_helpButton;
        }
        set
        {
          this.m_owner.m_helpButton = value;
        }
      }

      public GameObject m_skillGroupContent
      {
        get
        {
          return this.m_owner.m_skillGroupContent;
        }
        set
        {
          this.m_owner.m_skillGroupContent = value;
        }
      }

      public CommonUIStateController m_centerHeartStateCtrl
      {
        get
        {
          return this.m_owner.m_centerHeartStateCtrl;
        }
        set
        {
          this.m_owner.m_centerHeartStateCtrl = value;
        }
      }

      public ButtonEx m_confessionHeartButton
      {
        get
        {
          return this.m_owner.m_confessionHeartButton;
        }
        set
        {
          this.m_owner.m_confessionHeartButton = value;
        }
      }

      public GameObject m_confessionHeartButtonSelectImage
      {
        get
        {
          return this.m_owner.m_confessionHeartButtonSelectImage;
        }
        set
        {
          this.m_owner.m_confessionHeartButtonSelectImage = value;
        }
      }

      public Image m_confessionHeartFemaleImage
      {
        get
        {
          return this.m_owner.m_confessionHeartFemaleImage;
        }
        set
        {
          this.m_owner.m_confessionHeartFemaleImage = value;
        }
      }

      public Image m_confessionHeartMaleImage
      {
        get
        {
          return this.m_owner.m_confessionHeartMaleImage;
        }
        set
        {
          this.m_owner.m_confessionHeartMaleImage = value;
        }
      }

      public CommonUIStateController m_detailPanelTweenStateStateCtrl
      {
        get
        {
          return this.m_owner.m_detailPanelTweenStateStateCtrl;
        }
        set
        {
          this.m_owner.m_detailPanelTweenStateStateCtrl = value;
        }
      }

      public CommonUIStateController m_skillDetailStateCtrl
      {
        get
        {
          return this.m_owner.m_skillDetailStateCtrl;
        }
        set
        {
          this.m_owner.m_skillDetailStateCtrl = value;
        }
      }

      public Image m_skillDetailInfoIconImage
      {
        get
        {
          return this.m_owner.m_skillDetailInfoIconImage;
        }
        set
        {
          this.m_owner.m_skillDetailInfoIconImage = value;
        }
      }

      public GameObject m_skillDetailInfoLockIconGo
      {
        get
        {
          return this.m_owner.m_skillDetailInfoLockIconGo;
        }
        set
        {
          this.m_owner.m_skillDetailInfoLockIconGo = value;
        }
      }

      public Text m_skillDetailInfoSkillNameText
      {
        get
        {
          return this.m_owner.m_skillDetailInfoSkillNameText;
        }
        set
        {
          this.m_owner.m_skillDetailInfoSkillNameText = value;
        }
      }

      public Text m_skillDetailInfoSkillLvText
      {
        get
        {
          return this.m_owner.m_skillDetailInfoSkillLvText;
        }
        set
        {
          this.m_owner.m_skillDetailInfoSkillLvText = value;
        }
      }

      public GameObject m_skillDetailInfoSkillLvMaxText
      {
        get
        {
          return this.m_owner.m_skillDetailInfoSkillLvMaxText;
        }
        set
        {
          this.m_owner.m_skillDetailInfoSkillLvMaxText = value;
        }
      }

      public Text m_skillDetailDescText
      {
        get
        {
          return this.m_owner.m_skillDetailDescText;
        }
        set
        {
          this.m_owner.m_skillDetailDescText = value;
        }
      }

      public CommonUIStateController m_skillDetailCondition1StateCtrl
      {
        get
        {
          return this.m_owner.m_skillDetailCondition1StateCtrl;
        }
        set
        {
          this.m_owner.m_skillDetailCondition1StateCtrl = value;
        }
      }

      public Text m_skillDetailConditionUnlockInfoText1
      {
        get
        {
          return this.m_owner.m_skillDetailConditionUnlockInfoText1;
        }
        set
        {
          this.m_owner.m_skillDetailConditionUnlockInfoText1 = value;
        }
      }

      public CommonUIStateController m_skillDetailCondition2StateCtrl
      {
        get
        {
          return this.m_owner.m_skillDetailCondition2StateCtrl;
        }
        set
        {
          this.m_owner.m_skillDetailCondition2StateCtrl = value;
        }
      }

      public Text m_skillDetailConditionUnlockInfoText2
      {
        get
        {
          return this.m_owner.m_skillDetailConditionUnlockInfoText2;
        }
        set
        {
          this.m_owner.m_skillDetailConditionUnlockInfoText2 = value;
        }
      }

      public GameObject m_skillDetailUnlockRewardGoods
      {
        get
        {
          return this.m_owner.m_skillDetailUnlockRewardGoods;
        }
        set
        {
          this.m_owner.m_skillDetailUnlockRewardGoods = value;
        }
      }

      public Button m_skillDetailUnlockButton
      {
        get
        {
          return this.m_owner.m_skillDetailUnlockButton;
        }
        set
        {
          this.m_owner.m_skillDetailUnlockButton = value;
        }
      }

      public CommonUIStateController m_skillDetailUnlockButtonStateCtrl
      {
        get
        {
          return this.m_owner.m_skillDetailUnlockButtonStateCtrl;
        }
        set
        {
          this.m_owner.m_skillDetailUnlockButtonStateCtrl = value;
        }
      }

      public Button m_skillDetailUnlockGreyButton
      {
        get
        {
          return this.m_owner.m_skillDetailUnlockGreyButton;
        }
        set
        {
          this.m_owner.m_skillDetailUnlockGreyButton = value;
        }
      }

      public Text m_skillDetailUnlockRewardNameText
      {
        get
        {
          return this.m_owner.m_skillDetailUnlockRewardNameText;
        }
        set
        {
          this.m_owner.m_skillDetailUnlockRewardNameText = value;
        }
      }

      public CommonUIStateController m_skillDetailUnlockStateCtrl
      {
        get
        {
          return this.m_owner.m_skillDetailUnlockStateCtrl;
        }
        set
        {
          this.m_owner.m_skillDetailUnlockStateCtrl = value;
        }
      }

      public Text m_skillDetailUnlockStateNowInfoText
      {
        get
        {
          return this.m_owner.m_skillDetailUnlockStateNowInfoText;
        }
        set
        {
          this.m_owner.m_skillDetailUnlockStateNowInfoText = value;
        }
      }

      public Text m_skillDetailUnlockStateAfterText
      {
        get
        {
          return this.m_owner.m_skillDetailUnlockStateAfterText;
        }
        set
        {
          this.m_owner.m_skillDetailUnlockStateAfterText = value;
        }
      }

      public CommonUIStateController m_skillDetailUnlockStateConsumeTextStateCtrl
      {
        get
        {
          return this.m_owner.m_skillDetailUnlockStateConsumeTextStateCtrl;
        }
        set
        {
          this.m_owner.m_skillDetailUnlockStateConsumeTextStateCtrl = value;
        }
      }

      public Text m_skillDetailUnlockStateConsumeValueText
      {
        get
        {
          return this.m_owner.m_skillDetailUnlockStateConsumeValueText;
        }
        set
        {
          this.m_owner.m_skillDetailUnlockStateConsumeValueText = value;
        }
      }

      public GameObject m_skillDetailUnlockEvolutionMaterial1
      {
        get
        {
          return this.m_owner.m_skillDetailUnlockEvolutionMaterial1;
        }
        set
        {
          this.m_owner.m_skillDetailUnlockEvolutionMaterial1 = value;
        }
      }

      public GameObject m_skillDetailUnlockEvolutionMaterial2
      {
        get
        {
          return this.m_owner.m_skillDetailUnlockEvolutionMaterial2;
        }
        set
        {
          this.m_owner.m_skillDetailUnlockEvolutionMaterial2 = value;
        }
      }

      public CommonUIStateController m_skillDetailUnlockEvolutionMaterialStateCtrl1
      {
        get
        {
          return this.m_owner.m_skillDetailUnlockEvolutionMaterialStateCtrl1;
        }
        set
        {
          this.m_owner.m_skillDetailUnlockEvolutionMaterialStateCtrl1 = value;
        }
      }

      public CommonUIStateController m_skillDetailUnlockEvolutionMaterialStateCtrl2
      {
        get
        {
          return this.m_owner.m_skillDetailUnlockEvolutionMaterialStateCtrl2;
        }
        set
        {
          this.m_owner.m_skillDetailUnlockEvolutionMaterialStateCtrl2 = value;
        }
      }

      public Button m_skillDetailUnlockStateEvolutionButton
      {
        get
        {
          return this.m_owner.m_skillDetailUnlockStateEvolutionButton;
        }
        set
        {
          this.m_owner.m_skillDetailUnlockStateEvolutionButton = value;
        }
      }

      public CommonUIStateController m_skillDetailUnlockStateEvolutionButtonStateController
      {
        get
        {
          return this.m_owner.m_skillDetailUnlockStateEvolutionButtonStateController;
        }
        set
        {
          this.m_owner.m_skillDetailUnlockStateEvolutionButtonStateController = value;
        }
      }

      public CommonUIStateController m_skillDetailEnhanceSuccessEffectPanelStateCtrl
      {
        get
        {
          return this.m_owner.m_skillDetailEnhanceSuccessEffectPanelStateCtrl;
        }
        set
        {
          this.m_owner.m_skillDetailEnhanceSuccessEffectPanelStateCtrl = value;
        }
      }

      public CommonUIStateController m_skillDetailConfessionAndRewardStateCtrl
      {
        get
        {
          return this.m_owner.m_skillDetailConfessionAndRewardStateCtrl;
        }
        set
        {
          this.m_owner.m_skillDetailConfessionAndRewardStateCtrl = value;
        }
      }

      public GameObject m_skillDetailConfessionAndRewardScrollViewContent
      {
        get
        {
          return this.m_owner.m_skillDetailConfessionAndRewardScrollViewContent;
        }
        set
        {
          this.m_owner.m_skillDetailConfessionAndRewardScrollViewContent = value;
        }
      }

      public Button m_skillDetailConfessionAndRewardFemaleButton
      {
        get
        {
          return this.m_owner.m_skillDetailConfessionAndRewardFemaleButton;
        }
        set
        {
          this.m_owner.m_skillDetailConfessionAndRewardFemaleButton = value;
        }
      }

      public GameObject m_skillDetailConfessionAndRewardFemaleRewardGoods
      {
        get
        {
          return this.m_owner.m_skillDetailConfessionAndRewardFemaleRewardGoods;
        }
        set
        {
          this.m_owner.m_skillDetailConfessionAndRewardFemaleRewardGoods = value;
        }
      }

      public Text m_skillDetailConfessionAndRewardFemaleRewardGoodsNameText
      {
        get
        {
          return this.m_owner.m_skillDetailConfessionAndRewardFemaleRewardGoodsNameText;
        }
        set
        {
          this.m_owner.m_skillDetailConfessionAndRewardFemaleRewardGoodsNameText = value;
        }
      }

      public Button m_skillDetailConfessionAndRewardMaleButton
      {
        get
        {
          return this.m_owner.m_skillDetailConfessionAndRewardMaleButton;
        }
        set
        {
          this.m_owner.m_skillDetailConfessionAndRewardMaleButton = value;
        }
      }

      public GameObject m_skillDetailConfessionAndRewardMaleRewardGoods
      {
        get
        {
          return this.m_owner.m_skillDetailConfessionAndRewardMaleRewardGoods;
        }
        set
        {
          this.m_owner.m_skillDetailConfessionAndRewardMaleRewardGoods = value;
        }
      }

      public Text m_skillDetailConfessionAndRewardMaleRewardGoodsNameText
      {
        get
        {
          return this.m_owner.m_skillDetailConfessionAndRewardMaleRewardGoodsNameText;
        }
        set
        {
          this.m_owner.m_skillDetailConfessionAndRewardMaleRewardGoodsNameText = value;
        }
      }

      public GameObject m_skillDetailUnlockStateConditionPrefab
      {
        get
        {
          return this.m_owner.m_skillDetailUnlockStateConditionPrefab;
        }
        set
        {
          this.m_owner.m_skillDetailUnlockStateConditionPrefab = value;
        }
      }

      public CommonUIStateController m_heartFetterSexInfoStateCtrl
      {
        get
        {
          return this.m_owner.m_heartFetterSexInfoStateCtrl;
        }
        set
        {
          this.m_owner.m_heartFetterSexInfoStateCtrl = value;
        }
      }

      public Text m_heartFetterLvText
      {
        get
        {
          return this.m_owner.m_heartFetterLvText;
        }
        set
        {
          this.m_owner.m_heartFetterLvText = value;
        }
      }

      public GameObject m_heartFetterMaxLvTextGo
      {
        get
        {
          return this.m_owner.m_heartFetterMaxLvTextGo;
        }
        set
        {
          this.m_owner.m_heartFetterMaxLvTextGo = value;
        }
      }

      public Image m_heartFetterLvMaleHeartImage
      {
        get
        {
          return this.m_owner.m_heartFetterLvMaleHeartImage;
        }
        set
        {
          this.m_owner.m_heartFetterLvMaleHeartImage = value;
        }
      }

      public Image m_heartFetterLvFemaleHeartImage
      {
        get
        {
          return this.m_owner.m_heartFetterLvFemaleHeartImage;
        }
        set
        {
          this.m_owner.m_heartFetterLvFemaleHeartImage = value;
        }
      }

      public Slider m_heartFetterLvProgressSlider
      {
        get
        {
          return this.m_owner.m_heartFetterLvProgressSlider;
        }
        set
        {
          this.m_owner.m_heartFetterLvProgressSlider = value;
        }
      }

      public GameObject m_heartFetterSkill1
      {
        get
        {
          return this.m_owner.m_heartFetterSkill1;
        }
        set
        {
          this.m_owner.m_heartFetterSkill1 = value;
        }
      }

      public GameObject m_heartFetterSkill2
      {
        get
        {
          return this.m_owner.m_heartFetterSkill2;
        }
        set
        {
          this.m_owner.m_heartFetterSkill2 = value;
        }
      }

      public GameObject m_heartFetterSkill3
      {
        get
        {
          return this.m_owner.m_heartFetterSkill3;
        }
        set
        {
          this.m_owner.m_heartFetterSkill3 = value;
        }
      }

      public CommonUIStateController m_heartFetterSkillDescPanelStateCtrl
      {
        get
        {
          return this.m_owner.m_heartFetterSkillDescPanelStateCtrl;
        }
        set
        {
          this.m_owner.m_heartFetterSkillDescPanelStateCtrl = value;
        }
      }

      public Image m_heartFetterSkillDescPanelIconImage
      {
        get
        {
          return this.m_owner.m_heartFetterSkillDescPanelIconImage;
        }
        set
        {
          this.m_owner.m_heartFetterSkillDescPanelIconImage = value;
        }
      }

      public Text m_heartFetterSkillDescPanelDescText
      {
        get
        {
          return this.m_owner.m_heartFetterSkillDescPanelDescText;
        }
        set
        {
          this.m_owner.m_heartFetterSkillDescPanelDescText = value;
        }
      }

      public CommonUIStateController m_unlockHeartFetterConditionPanelStateCtrl
      {
        get
        {
          return this.m_owner.m_unlockHeartFetterConditionPanelStateCtrl;
        }
        set
        {
          this.m_owner.m_unlockHeartFetterConditionPanelStateCtrl = value;
        }
      }

      public Button m_unlockHeartFetterConditionPanelBGButton
      {
        get
        {
          return this.m_owner.m_unlockHeartFetterConditionPanelBGButton;
        }
        set
        {
          this.m_owner.m_unlockHeartFetterConditionPanelBGButton = value;
        }
      }

      public CommonUIStateController m_unlockHeartFetterCondition1StateCtrl
      {
        get
        {
          return this.m_owner.m_unlockHeartFetterCondition1StateCtrl;
        }
        set
        {
          this.m_owner.m_unlockHeartFetterCondition1StateCtrl = value;
        }
      }

      public Text m_unlockHeartFetterCondition1Text
      {
        get
        {
          return this.m_owner.m_unlockHeartFetterCondition1Text;
        }
        set
        {
          this.m_owner.m_unlockHeartFetterCondition1Text = value;
        }
      }

      public Text m_unlockHeartFetterCondition1LvText
      {
        get
        {
          return this.m_owner.m_unlockHeartFetterCondition1LvText;
        }
        set
        {
          this.m_owner.m_unlockHeartFetterCondition1LvText = value;
        }
      }

      public CommonUIStateController m_unlockHeartFetterCondition2StateCtrl
      {
        get
        {
          return this.m_owner.m_unlockHeartFetterCondition2StateCtrl;
        }
        set
        {
          this.m_owner.m_unlockHeartFetterCondition2StateCtrl = value;
        }
      }

      public Text m_unlockHeartFetterCondition2Text
      {
        get
        {
          return this.m_owner.m_unlockHeartFetterCondition2Text;
        }
        set
        {
          this.m_owner.m_unlockHeartFetterCondition2Text = value;
        }
      }

      public Text m_unlockHeartFetterCondition2LvText
      {
        get
        {
          return this.m_owner.m_unlockHeartFetterCondition2LvText;
        }
        set
        {
          this.m_owner.m_unlockHeartFetterCondition2LvText = value;
        }
      }

      public CommonUIStateController m_unlockHeartFetterSkillInfoPanelStateCtrl
      {
        get
        {
          return this.m_owner.m_unlockHeartFetterSkillInfoPanelStateCtrl;
        }
        set
        {
          this.m_owner.m_unlockHeartFetterSkillInfoPanelStateCtrl = value;
        }
      }

      public Button m_unlockHeartFetterSkillInfoPanelBGButton
      {
        get
        {
          return this.m_owner.m_unlockHeartFetterSkillInfoPanelBGButton;
        }
        set
        {
          this.m_owner.m_unlockHeartFetterSkillInfoPanelBGButton = value;
        }
      }

      public Image m_unlockHeartFetterSkillInfoPanelIconImage
      {
        get
        {
          return this.m_owner.m_unlockHeartFetterSkillInfoPanelIconImage;
        }
        set
        {
          this.m_owner.m_unlockHeartFetterSkillInfoPanelIconImage = value;
        }
      }

      public Text m_unlockHeartFetterSkillInfoPanelDescText
      {
        get
        {
          return this.m_owner.m_unlockHeartFetterSkillInfoPanelDescText;
        }
        set
        {
          this.m_owner.m_unlockHeartFetterSkillInfoPanelDescText = value;
        }
      }

      public Text m_playerResourceCrystalText
      {
        get
        {
          return this.m_owner.m_playerResourceCrystalText;
        }
        set
        {
          this.m_owner.m_playerResourceCrystalText = value;
        }
      }

      public Button m_playerResourceCrystalAddButton
      {
        get
        {
          return this.m_owner.m_playerResourceCrystalAddButton;
        }
        set
        {
          this.m_owner.m_playerResourceCrystalAddButton = value;
        }
      }

      public Text m_playerResourceGoldenText
      {
        get
        {
          return this.m_owner.m_playerResourceGoldenText;
        }
        set
        {
          this.m_owner.m_playerResourceGoldenText = value;
        }
      }

      public Button m_playerResourceGoldenAddButton
      {
        get
        {
          return this.m_owner.m_playerResourceGoldenAddButton;
        }
        set
        {
          this.m_owner.m_playerResourceGoldenAddButton = value;
        }
      }

      public Button m_fastLevelButton
      {
        get
        {
          return this.m_owner.m_fastLevelButton;
        }
        set
        {
          this.m_owner.m_fastLevelButton = value;
        }
      }

      public InputField m_fastLevelButtonInputField
      {
        get
        {
          return this.m_owner.m_fastLevelButtonInputField;
        }
        set
        {
          this.m_owner.m_fastLevelButtonInputField = value;
        }
      }

      public Hero m_hero
      {
        get
        {
          return this.m_owner.m_hero;
        }
        set
        {
          this.m_owner.m_hero = value;
        }
      }

      public bool isFirstIn
      {
        get
        {
          return this.m_owner.isFirstIn;
        }
        set
        {
          this.m_owner.isFirstIn = value;
        }
      }

      public bool m_isMale
      {
        get
        {
          return this.m_owner.m_isMale;
        }
        set
        {
          this.m_owner.m_isMale = value;
        }
      }

      public int m_lastHeroHeartFetterLevel
      {
        get
        {
          return this.m_owner.m_lastHeroHeartFetterLevel;
        }
        set
        {
          this.m_owner.m_lastHeroHeartFetterLevel = value;
        }
      }

      public bool m_isDetailPanelInHeartState
      {
        get
        {
          return this.m_owner.m_isDetailPanelInHeartState;
        }
        set
        {
          this.m_owner.m_isDetailPanelInHeartState = value;
        }
      }

      public FettersConfessionSkillItemUIController m_curFetterSkillCtrl
      {
        get
        {
          return this.m_owner.m_curFetterSkillCtrl;
        }
        set
        {
          this.m_owner.m_curFetterSkillCtrl = value;
        }
      }

      public List<HeartFettersSkillItemUIController> m_heartFettersSkillItemCtrlList
      {
        get
        {
          return this.m_owner.m_heartFettersSkillItemCtrlList;
        }
        set
        {
          this.m_owner.m_heartFettersSkillItemCtrlList = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public HeartFettersSkillItemUIController m_curClickHeartFettersSkillItemUICtrl
      {
        get
        {
          return this.m_owner.m_curClickHeartFettersSkillItemUICtrl;
        }
        set
        {
          this.m_owner.m_curClickHeartFettersSkillItemUICtrl = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void OnDisable()
      {
        this.m_owner.OnDisable();
      }

      public void SetCenterHeartFetterInfo()
      {
        this.m_owner.SetCenterHeartFetterInfo();
      }

      public void SetSkillDetailPanel(ConfigDataHeroFetterInfo heroFetterInfo)
      {
        this.m_owner.SetSkillDetailPanel(heroFetterInfo);
      }

      public void SetSkillInfoGroup(ConfigDataHeroFetterInfo heroFetterInfo)
      {
        this.m_owner.SetSkillInfoGroup(heroFetterInfo);
      }

      public void SetLockStateInfo(ConfigDataHeroFetterInfo heroFetterInfo)
      {
        this.m_owner.SetLockStateInfo(heroFetterInfo);
      }

      public string GetFettersCoditionDesc(HeroFetterCompletionCondition condition)
      {
        return this.m_owner.GetFettersCoditionDesc(condition);
      }

      public void SetUnlockStateInfo(ConfigDataHeroFetterInfo heroFetterInfo)
      {
        this.m_owner.SetUnlockStateInfo(heroFetterInfo);
      }

      public void OnFettersSkillItemClick(FettersConfessionSkillItemUIController ctrl)
      {
        this.m_owner.OnFettersSkillItemClick(ctrl);
      }

      public void OnConfessionHeartButtonClick()
      {
        this.m_owner.OnConfessionHeartButtonClick();
      }

      public void OnUnlockCenterHeartFetterFinish()
      {
        this.m_owner.OnUnlockCenterHeartFetterFinish();
      }

      public void SetHeartFetterDetailPanel()
      {
        this.m_owner.SetHeartFetterDetailPanel();
      }

      public void OnHeartFetterSkillItemClick(List<int> skillIdList)
      {
        this.m_owner.OnHeartFetterSkillItemClick(skillIdList);
      }

      public void SetUnlockHeartFetterConditionPanel()
      {
        this.m_owner.SetUnlockHeartFetterConditionPanel();
      }

      public void CloseHeartFetterUnlockConditionPanel()
      {
        this.m_owner.CloseHeartFetterUnlockConditionPanel();
      }

      public void ShowHeartFetterSkillInfoPanel()
      {
        this.m_owner.ShowHeartFetterSkillInfoPanel();
      }

      public void CloseHeartFetterSkillInfoPanel()
      {
        this.m_owner.CloseHeartFetterSkillInfoPanel();
      }

      public void OnEvolutionMaterialClick(GoodsType goodsType, int id, int needCount)
      {
        this.m_owner.OnEvolutionMaterialClick(goodsType, id, needCount);
      }

      public void OnSkillUnlockButtonClick()
      {
        this.m_owner.OnSkillUnlockButtonClick();
      }

      public void OnSkillDetailUnlockGreyButtonClick()
      {
        this.m_owner.OnSkillDetailUnlockGreyButtonClick();
      }

      public void OnUnlockStateEvolutionButtonClcik()
      {
        this.m_owner.OnUnlockStateEvolutionButtonClcik();
      }

      public void OnEvolutionHeartFetterFinished()
      {
        this.m_owner.OnEvolutionHeartFetterFinished();
      }

      public IEnumerator ShowHeartUpdatePerformance()
      {
        return this.m_owner.ShowHeartUpdatePerformance();
      }

      public void OnConfessionAndRewardButtonClick()
      {
        this.m_owner.OnConfessionAndRewardButtonClick();
      }

      public void OnReturnButtonClick()
      {
        this.m_owner.OnReturnButtonClick();
      }

      public void OnFastLevelButtonClick()
      {
        this.m_owner.OnFastLevelButtonClick();
      }

      public void OnHelpButtonClick()
      {
        this.m_owner.OnHelpButtonClick();
      }

      public void OnGoldAddButtonClick()
      {
        this.m_owner.OnGoldAddButtonClick();
      }

      public void OnCrystalAddButtonClick()
      {
        this.m_owner.OnCrystalAddButtonClick();
      }
    }
  }
}
