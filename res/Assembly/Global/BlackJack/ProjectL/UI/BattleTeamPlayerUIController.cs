﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BattleTeamPlayerUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class BattleTeamPlayerUIController : UIControllerBase
  {
    [AutoBind("./PlayerIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_headIconImage;
    [AutoBind("./PlayerIcon/HeadFrameDummy", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_headFrameTransform;
    [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameText;
    [AutoBind("./LevelText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_levelText;
    [AutoBind("./PlayerTag", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_playerTagImage;
    [AutoBind("./Chat", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_chatUIStateController;
    [AutoBind("./Chat/BGImage/Text", AutoBindAttribute.InitState.NotInit, false)]
    private EmojiText m_dialogText;
    [AutoBind("./Chat/BGImage/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_expressionImage;
    [AutoBind("./Chat/BGImage/Voice", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_voiceButton;
    [AutoBind("./Chat/BGImage/Voice/Voice/SpeakImage", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_voiceSpeakImageStateCtrl;
    [AutoBind("./Chat/BGImage/Voice/Voice/TimeButton/ContentText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_voiceTimeButtonText;
    [AutoBind("./Chat/BGImage/Voice/ContentText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_voiceContentText;
    [AutoBind("./StateGroup/ActionImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_statusActionGameObject;
    [AutoBind("./StateGroup/ReadyImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_statusReadyGameObject;
    [AutoBind("./StateGroup/AutoImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_statusAutoGameObject;
    [AutoBind("./StateGroup/OfflineImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_statusOfflineGameObject;
    private float m_hideChatTime;
    private ChatVoiceMessage m_voiceMessage;
    [DoNotToLua]
    private BattleTeamPlayerUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_InitEmojiTextInt32Image_hotfix;
    private LuaFunction m_SetStatusPlayerBattleStatusBoolean_hotfix;
    private LuaFunction m_SetActionBoolean_hotfix;
    private LuaFunction m_SetHeadIconInt32_hotfix;
    private LuaFunction m_SetNameString_hotfix;
    private LuaFunction m_SetLevelInt32_hotfix;
    private LuaFunction m_SetPlayerIndexInt32_hotfix;
    private LuaFunction m_ShowPlayerIndexBoolean_hotfix;
    private LuaFunction m_SetHeroCountInt32_hotfix;
    private LuaFunction m_SetHeroAliveInt32Boolean_hotfix;
    private LuaFunction m_ShowChatString_hotfix;
    private LuaFunction m_ShowBigExpressionInt32_hotfix;
    private LuaFunction m_ShowVoiceChatVoiceMessage_hotfix;
    private LuaFunction m_OnVoiceTimeButtonClick_hotfix;
    private LuaFunction m_HideChat_hotfix;
    private LuaFunction m_HideBigExpression_hotfix;
    private LuaFunction m_GetHeroPointBGGameObjectInt32_hotfix;
    private LuaFunction m_GetHeroPointGameObjectInt32_hotfix;
    private LuaFunction m_GetHeroRedPointGameObjectInt32_hotfix;
    private LuaFunction m_Update_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitEmojiText(int fontSize, Image image)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetStatus(PlayerBattleStatus status, bool isOffline)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAction(bool isAction)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHeadIcon(int headIconId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetName(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLevel(int level)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayerIndex(int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowPlayerIndex(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHeroCount(int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHeroAlive(int heroIdx, bool isAlive)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowChat(string txt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBigExpression(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowVoice(ChatVoiceMessage voiceMessage)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnVoiceTimeButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HideChat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HideBigExpression()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private GameObject GetHeroPointBGGameObject(int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private GameObject GetHeroPointGameObject(int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private GameObject GetHeroRedPointGameObject(int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public BattleTeamPlayerUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private BattleTeamPlayerUIController m_owner;

      public LuaExportHelper(BattleTeamPlayerUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public Image m_headIconImage
      {
        get
        {
          return this.m_owner.m_headIconImage;
        }
        set
        {
          this.m_owner.m_headIconImage = value;
        }
      }

      public Transform m_headFrameTransform
      {
        get
        {
          return this.m_owner.m_headFrameTransform;
        }
        set
        {
          this.m_owner.m_headFrameTransform = value;
        }
      }

      public Text m_nameText
      {
        get
        {
          return this.m_owner.m_nameText;
        }
        set
        {
          this.m_owner.m_nameText = value;
        }
      }

      public Text m_levelText
      {
        get
        {
          return this.m_owner.m_levelText;
        }
        set
        {
          this.m_owner.m_levelText = value;
        }
      }

      public Image m_playerTagImage
      {
        get
        {
          return this.m_owner.m_playerTagImage;
        }
        set
        {
          this.m_owner.m_playerTagImage = value;
        }
      }

      public CommonUIStateController m_chatUIStateController
      {
        get
        {
          return this.m_owner.m_chatUIStateController;
        }
        set
        {
          this.m_owner.m_chatUIStateController = value;
        }
      }

      public EmojiText m_dialogText
      {
        get
        {
          return this.m_owner.m_dialogText;
        }
        set
        {
          this.m_owner.m_dialogText = value;
        }
      }

      public Image m_expressionImage
      {
        get
        {
          return this.m_owner.m_expressionImage;
        }
        set
        {
          this.m_owner.m_expressionImage = value;
        }
      }

      public Button m_voiceButton
      {
        get
        {
          return this.m_owner.m_voiceButton;
        }
        set
        {
          this.m_owner.m_voiceButton = value;
        }
      }

      public CommonUIStateController m_voiceSpeakImageStateCtrl
      {
        get
        {
          return this.m_owner.m_voiceSpeakImageStateCtrl;
        }
        set
        {
          this.m_owner.m_voiceSpeakImageStateCtrl = value;
        }
      }

      public Text m_voiceTimeButtonText
      {
        get
        {
          return this.m_owner.m_voiceTimeButtonText;
        }
        set
        {
          this.m_owner.m_voiceTimeButtonText = value;
        }
      }

      public Text m_voiceContentText
      {
        get
        {
          return this.m_owner.m_voiceContentText;
        }
        set
        {
          this.m_owner.m_voiceContentText = value;
        }
      }

      public GameObject m_statusActionGameObject
      {
        get
        {
          return this.m_owner.m_statusActionGameObject;
        }
        set
        {
          this.m_owner.m_statusActionGameObject = value;
        }
      }

      public GameObject m_statusReadyGameObject
      {
        get
        {
          return this.m_owner.m_statusReadyGameObject;
        }
        set
        {
          this.m_owner.m_statusReadyGameObject = value;
        }
      }

      public GameObject m_statusAutoGameObject
      {
        get
        {
          return this.m_owner.m_statusAutoGameObject;
        }
        set
        {
          this.m_owner.m_statusAutoGameObject = value;
        }
      }

      public GameObject m_statusOfflineGameObject
      {
        get
        {
          return this.m_owner.m_statusOfflineGameObject;
        }
        set
        {
          this.m_owner.m_statusOfflineGameObject = value;
        }
      }

      public float m_hideChatTime
      {
        get
        {
          return this.m_owner.m_hideChatTime;
        }
        set
        {
          this.m_owner.m_hideChatTime = value;
        }
      }

      public ChatVoiceMessage m_voiceMessage
      {
        get
        {
          return this.m_owner.m_voiceMessage;
        }
        set
        {
          this.m_owner.m_voiceMessage = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void OnVoiceTimeButtonClick()
      {
        this.m_owner.OnVoiceTimeButtonClick();
      }

      public void HideChat()
      {
        this.m_owner.HideChat();
      }

      public void HideBigExpression()
      {
        this.m_owner.HideBigExpression();
      }

      public GameObject GetHeroPointBGGameObject(int idx)
      {
        return this.m_owner.GetHeroPointBGGameObject(idx);
      }

      public GameObject GetHeroPointGameObject(int idx)
      {
        return this.m_owner.GetHeroPointGameObject(idx);
      }

      public GameObject GetHeroRedPointGameObject(int idx)
      {
        return this.m_owner.GetHeroRedPointGameObject(idx);
      }

      public void Update()
      {
        this.m_owner.Update();
      }
    }
  }
}
