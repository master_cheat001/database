﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProBusinessCard
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "ProBusinessCard")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class ProBusinessCard : IExtensible
  {
    private string _UserId;
    private string _Name;
    private int _Level;
    private int _HeadIcon;
    private int _ArenaPoints;
    private int _Likes;
    private bool _IsOnLine;
    private readonly List<ProBattleHero> _Heroes;
    private ProBusinessCardInfoSet _SetInfo;
    private ProBusinessCardStatisticalData _StatisticalData;
    private readonly List<ProTrainingTech> _Techs;
    private readonly List<ProBattleHero> _MostSkilledHeroes;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProBusinessCard()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = true, Name = "UserId")]
    public string UserId
    {
      get
      {
        return this._UserId;
      }
      set
      {
        this._UserId = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Level")]
    public int Level
    {
      get
      {
        return this._Level;
      }
      set
      {
        this._Level = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HeadIcon")]
    public int HeadIcon
    {
      get
      {
        return this._HeadIcon;
      }
      set
      {
        this._HeadIcon = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ArenaPoints")]
    public int ArenaPoints
    {
      get
      {
        return this._ArenaPoints;
      }
      set
      {
        this._ArenaPoints = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Likes")]
    public int Likes
    {
      get
      {
        return this._Likes;
      }
      set
      {
        this._Likes = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, IsRequired = true, Name = "IsOnLine")]
    public bool IsOnLine
    {
      get
      {
        return this._IsOnLine;
      }
      set
      {
        this._IsOnLine = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.Default, Name = "Heroes")]
    public List<ProBattleHero> Heroes
    {
      get
      {
        return this._Heroes;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.Default, IsRequired = true, Name = "SetInfo")]
    public ProBusinessCardInfoSet SetInfo
    {
      get
      {
        return this._SetInfo;
      }
      set
      {
        this._SetInfo = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.Default, IsRequired = true, Name = "StatisticalData")]
    public ProBusinessCardStatisticalData StatisticalData
    {
      get
      {
        return this._StatisticalData;
      }
      set
      {
        this._StatisticalData = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.Default, Name = "Techs")]
    public List<ProTrainingTech> Techs
    {
      get
      {
        return this._Techs;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.Default, Name = "MostSkilledHeroes")]
    public List<ProBattleHero> MostSkilledHeroes
    {
      get
      {
        return this._MostSkilledHeroes;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
