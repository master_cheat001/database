﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.GuildCreateReq
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "GuildCreateReq")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class GuildCreateReq : IExtensible
  {
    private string _Name;
    private string _HiringDeclaration;
    private bool _AutoJoin;
    private int _JoinLevel;
    private string _Announcement;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public GuildCreateReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = false, Name = "HiringDeclaration")]
    [DefaultValue("")]
    public string HiringDeclaration
    {
      get
      {
        return this._HiringDeclaration;
      }
      set
      {
        this._HiringDeclaration = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "AutoJoin")]
    public bool AutoJoin
    {
      get
      {
        return this._AutoJoin;
      }
      set
      {
        this._AutoJoin = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "JoinLevel")]
    public int JoinLevel
    {
      get
      {
        return this._JoinLevel;
      }
      set
      {
        this._JoinLevel = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "Announcement")]
    public string Announcement
    {
      get
      {
        return this._Announcement;
      }
      set
      {
        this._Announcement = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
