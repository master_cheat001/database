﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProHeroCommentEntry
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "ProHeroCommentEntry")]
  [Serializable]
  public class ProHeroCommentEntry : IExtensible
  {
    private ulong _InstanceId;
    private string _Content;
    private string _CommenterUserId;
    private string _CommenterName;
    private int _CommenterLevel;
    private int _PraiseNums;
    private long _CommentTime;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProHeroCommentEntry()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "InstanceId")]
    public ulong InstanceId
    {
      get
      {
        return this._InstanceId;
      }
      set
      {
        this._InstanceId = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = true, Name = "Content")]
    public string Content
    {
      get
      {
        return this._Content;
      }
      set
      {
        this._Content = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "CommenterUserId")]
    public string CommenterUserId
    {
      get
      {
        return this._CommenterUserId;
      }
      set
      {
        this._CommenterUserId = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "CommenterName")]
    public string CommenterName
    {
      get
      {
        return this._CommenterName;
      }
      set
      {
        this._CommenterName = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CommenterLevel")]
    public int CommenterLevel
    {
      get
      {
        return this._CommenterLevel;
      }
      set
      {
        this._CommenterLevel = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "PraiseNums")]
    public int PraiseNums
    {
      get
      {
        return this._PraiseNums;
      }
      set
      {
        this._PraiseNums = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CommentTime")]
    public long CommentTime
    {
      get
      {
        return this._CommentTime;
      }
      set
      {
        this._CommentTime = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
