﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProChatContentVoice
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "ProChatContentVoice")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class ProChatContentVoice : IExtensible
  {
    private ulong _InstanceId;
    private byte[] _Voice;
    private int _VoiceLenth;
    private int _AudioFrequency;
    private int _AudioSampleLength;
    private string _TranslateText;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProChatContentVoice()
    {
      // ISSUE: unable to decompile the method.
    }

    [DefaultValue(0.0f)]
    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "InstanceId")]
    public ulong InstanceId
    {
      get
      {
        return this._InstanceId;
      }
      set
      {
        this._InstanceId = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = false, Name = "Voice")]
    [DefaultValue(null)]
    public byte[] Voice
    {
      get
      {
        return this._Voice;
      }
      set
      {
        this._Voice = value;
      }
    }

    [DefaultValue(0)]
    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "VoiceLenth")]
    public int VoiceLenth
    {
      get
      {
        return this._VoiceLenth;
      }
      set
      {
        this._VoiceLenth = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "AudioFrequency")]
    [DefaultValue(0)]
    public int AudioFrequency
    {
      get
      {
        return this._AudioFrequency;
      }
      set
      {
        this._AudioFrequency = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "AudioSampleLength")]
    [DefaultValue(0)]
    public int AudioSampleLength
    {
      get
      {
        return this._AudioSampleLength;
      }
      set
      {
        this._AudioSampleLength = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = false, Name = "TranslateText")]
    [DefaultValue("")]
    public string TranslateText
    {
      get
      {
        return this._TranslateText;
      }
      set
      {
        this._TranslateText = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
