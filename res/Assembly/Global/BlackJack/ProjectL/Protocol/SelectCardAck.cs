﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.SelectCardAck
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "SelectCardAck")]
  [Serializable]
  public class SelectCardAck : IExtensible
  {
    private int _Result;
    private int _CardPoolId;
    private bool _IsSingleSelect;
    private bool _IsUsingTicket;
    private ProChangedGoodsNtf _Ntf;
    private readonly List<ProGoods> _SelectedGoods;
    private int _GuaranteedAccumulatedValue;
    private int _GuaranteedSelectCardCount;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public SelectCardAck()
    {
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Result")]
    public int Result
    {
      get
      {
        return this._Result;
      }
      set
      {
        this._Result = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CardPoolId")]
    public int CardPoolId
    {
      get
      {
        return this._CardPoolId;
      }
      set
      {
        this._CardPoolId = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "IsSingleSelect")]
    public bool IsSingleSelect
    {
      get
      {
        return this._IsSingleSelect;
      }
      set
      {
        this._IsSingleSelect = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "IsUsingTicket")]
    public bool IsUsingTicket
    {
      get
      {
        return this._IsUsingTicket;
      }
      set
      {
        this._IsUsingTicket = value;
      }
    }

    [DefaultValue(null)]
    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = false, Name = "Ntf")]
    public ProChangedGoodsNtf Ntf
    {
      get
      {
        return this._Ntf;
      }
      set
      {
        this._Ntf = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, Name = "SelectedGoods")]
    public List<ProGoods> SelectedGoods
    {
      get
      {
        return this._SelectedGoods;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "GuaranteedAccumulatedValue")]
    public int GuaranteedAccumulatedValue
    {
      get
      {
        return this._GuaranteedAccumulatedValue;
      }
      set
      {
        this._GuaranteedAccumulatedValue = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "GuaranteedSelectCardCount")]
    public int GuaranteedSelectCardCount
    {
      get
      {
        return this._GuaranteedSelectCardCount;
      }
      set
      {
        this._GuaranteedSelectCardCount = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
