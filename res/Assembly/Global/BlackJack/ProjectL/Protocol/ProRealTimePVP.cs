﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProRealTimePVP
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "ProRealTimePVP")]
  [Serializable]
  public class ProRealTimePVP : IExtensible
  {
    private bool _IsPromotion;
    private readonly List<ProRealTimePVPBattleReport> _PromotionReports;
    private readonly List<ProRealTimePVPBattleReport> _Reports;
    private ProRealTimePVPMatchStats _FriendlyMatchStats;
    private ProRealTimePVPMatchStats _LadderMatchStats;
    private ProRealTimePVPMatchStats _FriendlyCareerMatchStats;
    private ProRealTimePVPMatchStats _LadderCareerMatchStats;
    private readonly List<int> _WinsBonusIdAcquired;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProRealTimePVP()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = true, Name = "IsPromotion")]
    public bool IsPromotion
    {
      get
      {
        return this._IsPromotion;
      }
      set
      {
        this._IsPromotion = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, Name = "PromotionReports")]
    public List<ProRealTimePVPBattleReport> PromotionReports
    {
      get
      {
        return this._PromotionReports;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, Name = "Reports")]
    public List<ProRealTimePVPBattleReport> Reports
    {
      get
      {
        return this._Reports;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "FriendlyMatchStats")]
    public ProRealTimePVPMatchStats FriendlyMatchStats
    {
      get
      {
        return this._FriendlyMatchStats;
      }
      set
      {
        this._FriendlyMatchStats = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "LadderMatchStats")]
    public ProRealTimePVPMatchStats LadderMatchStats
    {
      get
      {
        return this._LadderMatchStats;
      }
      set
      {
        this._LadderMatchStats = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = true, Name = "FriendlyCareerMatchStats")]
    public ProRealTimePVPMatchStats FriendlyCareerMatchStats
    {
      get
      {
        return this._FriendlyCareerMatchStats;
      }
      set
      {
        this._FriendlyCareerMatchStats = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, IsRequired = true, Name = "LadderCareerMatchStats")]
    public ProRealTimePVPMatchStats LadderCareerMatchStats
    {
      get
      {
        return this._LadderCareerMatchStats;
      }
      set
      {
        this._LadderCareerMatchStats = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, Name = "WinsBonusIdAcquired")]
    public List<int> WinsBonusIdAcquired
    {
      get
      {
        return this._WinsBonusIdAcquired;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
