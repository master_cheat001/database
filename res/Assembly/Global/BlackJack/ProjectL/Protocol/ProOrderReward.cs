﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProOrderReward
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "ProOrderReward")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class ProOrderReward : IExtensible
  {
    private string _OrderId;
    private readonly List<ProGoods> _Rewards;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProOrderReward()
    {
    }

    [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = true, Name = "OrderId")]
    public string OrderId
    {
      get
      {
        return this._OrderId;
      }
      set
      {
        this._OrderId = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, Name = "Rewards")]
    public List<ProGoods> Rewards
    {
      get
      {
        return this._Rewards;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
