﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProChatInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "ProChatInfo")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class ProChatInfo : IExtensible
  {
    private int _ChatSrcType;
    private string _SrcName;
    private int _AvatarId;
    private int _ChatContentType;
    private int _SrcPlayerLevel;
    private string _SrcGameUserID;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProChatInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ChatSrcType")]
    public int ChatSrcType
    {
      get
      {
        return this._ChatSrcType;
      }
      set
      {
        this._ChatSrcType = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = true, Name = "SrcName")]
    public string SrcName
    {
      get
      {
        return this._SrcName;
      }
      set
      {
        this._SrcName = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "AvatarId")]
    public int AvatarId
    {
      get
      {
        return this._AvatarId;
      }
      set
      {
        this._AvatarId = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ChatContentType")]
    public int ChatContentType
    {
      get
      {
        return this._ChatContentType;
      }
      set
      {
        this._ChatContentType = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "SrcPlayerLevel")]
    public int SrcPlayerLevel
    {
      get
      {
        return this._SrcPlayerLevel;
      }
      set
      {
        this._SrcPlayerLevel = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = true, Name = "SrcGameUserID")]
    public string SrcGameUserID
    {
      get
      {
        return this._SrcGameUserID;
      }
      set
      {
        this._SrcGameUserID = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
