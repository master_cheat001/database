﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.FriendshipPointsClaimAck
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "FriendshipPointsClaimAck")]
  [Serializable]
  public class FriendshipPointsClaimAck : IExtensible
  {
    private int _Result;
    private FriendshipPointsClaimReq _Req;
    private readonly List<string> _ReceivedUserIds;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public FriendshipPointsClaimAck()
    {
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Result")]
    public int Result
    {
      get
      {
        return this._Result;
      }
      set
      {
        this._Result = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = true, Name = "Req")]
    public FriendshipPointsClaimReq Req
    {
      get
      {
        return this._Req;
      }
      set
      {
        this._Req = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, Name = "ReceivedUserIds")]
    public List<string> ReceivedUserIds
    {
      get
      {
        return this._ReceivedUserIds;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
