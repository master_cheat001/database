﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.RiftChapterRewardGainAck
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "RiftChapterRewardGainAck")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class RiftChapterRewardGainAck : IExtensible
  {
    private int _Result;
    private int _ChapterId;
    private int _Index;
    private ProChangedGoodsNtf _Ntf;
    private readonly List<ProGoods> _NormalRewards;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public RiftChapterRewardGainAck()
    {
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Result")]
    public int Result
    {
      get
      {
        return this._Result;
      }
      set
      {
        this._Result = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ChapterId")]
    public int ChapterId
    {
      get
      {
        return this._ChapterId;
      }
      set
      {
        this._ChapterId = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Index")]
    public int Index
    {
      get
      {
        return this._Index;
      }
      set
      {
        this._Index = value;
      }
    }

    [DefaultValue(null)]
    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = false, Name = "Ntf")]
    public ProChangedGoodsNtf Ntf
    {
      get
      {
        return this._Ntf;
      }
      set
      {
        this._Ntf = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, Name = "NormalRewards")]
    public List<ProGoods> NormalRewards
    {
      get
      {
        return this._NormalRewards;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
