﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProAdvertisementFlowLayout
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "ProAdvertisementFlowLayout")]
  [Serializable]
  public class ProAdvertisementFlowLayout : IExtensible
  {
    private int _ActivityId;
    private bool _IsLarge;
    private int _SortId;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProAdvertisementFlowLayout()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ActivityId")]
    public int ActivityId
    {
      get
      {
        return this._ActivityId;
      }
      set
      {
        this._ActivityId = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = true, Name = "IsLarge")]
    public bool IsLarge
    {
      get
      {
        return this._IsLarge;
      }
      set
      {
        this._IsLarge = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "SortId")]
    public int SortId
    {
      get
      {
        return this._SortId;
      }
      set
      {
        this._SortId = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
