﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.RaffleDrawAck
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "RaffleDrawAck")]
  [Serializable]
  public class RaffleDrawAck : IExtensible
  {
    private int _Result;
    private int _PoolId;
    private int _DrawedRaffleId;
    private int _FreeDrawedCount;
    private int _DrawedCount;
    private ProChangedGoodsNtf _Ntf;
    private ProGoods _DrawedGoods;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public RaffleDrawAck()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Result")]
    public int Result
    {
      get
      {
        return this._Result;
      }
      set
      {
        this._Result = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "PoolId")]
    public int PoolId
    {
      get
      {
        return this._PoolId;
      }
      set
      {
        this._PoolId = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DrawedRaffleId")]
    public int DrawedRaffleId
    {
      get
      {
        return this._DrawedRaffleId;
      }
      set
      {
        this._DrawedRaffleId = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "FreeDrawedCount")]
    public int FreeDrawedCount
    {
      get
      {
        return this._FreeDrawedCount;
      }
      set
      {
        this._FreeDrawedCount = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DrawedCount")]
    public int DrawedCount
    {
      get
      {
        return this._DrawedCount;
      }
      set
      {
        this._DrawedCount = value;
      }
    }

    [DefaultValue(null)]
    [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = false, Name = "Ntf")]
    public ProChangedGoodsNtf Ntf
    {
      get
      {
        return this._Ntf;
      }
      set
      {
        this._Ntf = value;
      }
    }

    [DefaultValue(null)]
    [ProtoMember(7, DataFormat = DataFormat.Default, IsRequired = false, Name = "DrawedGoods")]
    public ProGoods DrawedGoods
    {
      get
      {
        return this._DrawedGoods;
      }
      set
      {
        this._DrawedGoods = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
