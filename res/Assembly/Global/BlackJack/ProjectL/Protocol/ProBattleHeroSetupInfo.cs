﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProBattleHeroSetupInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "ProBattleHeroSetupInfo")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class ProBattleHeroSetupInfo : IExtensible
  {
    private int _PlayerIndex;
    private int _Position;
    private ProBattleHero _Hero;
    private int _Flag;
    private ProBattleHero _PrevHero;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProBattleHeroSetupInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "PlayerIndex")]
    public int PlayerIndex
    {
      get
      {
        return this._PlayerIndex;
      }
      set
      {
        this._PlayerIndex = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Position")]
    public int Position
    {
      get
      {
        return this._Position;
      }
      set
      {
        this._Position = value;
      }
    }

    [DefaultValue(null)]
    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = false, Name = "Hero")]
    public ProBattleHero Hero
    {
      get
      {
        return this._Hero;
      }
      set
      {
        this._Hero = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Flag")]
    public int Flag
    {
      get
      {
        return this._Flag;
      }
      set
      {
        this._Flag = value;
      }
    }

    [DefaultValue(null)]
    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = false, Name = "PrevHero")]
    public ProBattleHero PrevHero
    {
      get
      {
        return this._PrevHero;
      }
      set
      {
        this._PrevHero = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
