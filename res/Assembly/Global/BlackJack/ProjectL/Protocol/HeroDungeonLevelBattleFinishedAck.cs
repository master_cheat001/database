﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.HeroDungeonLevelBattleFinishedAck
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "HeroDungeonLevelBattleFinishedAck")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class HeroDungeonLevelBattleFinishedAck : IExtensible
  {
    private int _Result;
    private int _HeroDungeonLevelId;
    private readonly List<int> _AchievementIds;
    private int _GainStars;
    private ProChangedGoodsNtf _Ntf;
    private ProChangedGoodsNtf _Treasure;
    private readonly List<ProGoods> _NormalRewards;
    private readonly List<int> _BattleTreasures;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroDungeonLevelBattleFinishedAck()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Result")]
    public int Result
    {
      get
      {
        return this._Result;
      }
      set
      {
        this._Result = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HeroDungeonLevelId")]
    public int HeroDungeonLevelId
    {
      get
      {
        return this._HeroDungeonLevelId;
      }
      set
      {
        this._HeroDungeonLevelId = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, Name = "AchievementIds")]
    public List<int> AchievementIds
    {
      get
      {
        return this._AchievementIds;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "GainStars")]
    public int GainStars
    {
      get
      {
        return this._GainStars;
      }
      set
      {
        this._GainStars = value;
      }
    }

    [DefaultValue(null)]
    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = false, Name = "Ntf")]
    public ProChangedGoodsNtf Ntf
    {
      get
      {
        return this._Ntf;
      }
      set
      {
        this._Ntf = value;
      }
    }

    [DefaultValue(null)]
    [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = false, Name = "Treasure")]
    public ProChangedGoodsNtf Treasure
    {
      get
      {
        return this._Treasure;
      }
      set
      {
        this._Treasure = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, Name = "NormalRewards")]
    public List<ProGoods> NormalRewards
    {
      get
      {
        return this._NormalRewards;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, Name = "BattleTreasures")]
    public List<int> BattleTreasures
    {
      get
      {
        return this._BattleTreasures;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
