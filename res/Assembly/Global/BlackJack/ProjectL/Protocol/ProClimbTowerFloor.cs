﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProClimbTowerFloor
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "ProClimbTowerFloor")]
  [Serializable]
  public class ProClimbTowerFloor : IExtensible
  {
    private int _LevelId;
    private int _RuleId;
    private int _ArmyRandomSeed;
    private int _BonusHeroGroupId;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProClimbTowerFloor()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "LevelId")]
    public int LevelId
    {
      get
      {
        return this._LevelId;
      }
      set
      {
        this._LevelId = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "RuleId")]
    public int RuleId
    {
      get
      {
        return this._RuleId;
      }
      set
      {
        this._RuleId = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ArmyRandomSeed")]
    public int ArmyRandomSeed
    {
      get
      {
        return this._ArmyRandomSeed;
      }
      set
      {
        this._ArmyRandomSeed = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BonusHeroGroupId")]
    public int BonusHeroGroupId
    {
      get
      {
        return this._BonusHeroGroupId;
      }
      set
      {
        this._BonusHeroGroupId = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
