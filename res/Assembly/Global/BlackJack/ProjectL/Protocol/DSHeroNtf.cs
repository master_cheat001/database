﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.DSHeroNtf
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "DSHeroNtf")]
  [Serializable]
  public class DSHeroNtf : IExtensible
  {
    private uint _Version;
    private readonly List<ProHero> _Heroes;
    private int _ProtagonistId;
    private long _HeroInteractNumsFlushTime;
    private int _HeroInteractNums;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public DSHeroNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Version")]
    public uint Version
    {
      get
      {
        return this._Version;
      }
      set
      {
        this._Version = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, Name = "Heroes")]
    public List<ProHero> Heroes
    {
      get
      {
        return this._Heroes;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ProtagonistId")]
    public int ProtagonistId
    {
      get
      {
        return this._ProtagonistId;
      }
      set
      {
        this._ProtagonistId = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HeroInteractNumsFlushTime")]
    public long HeroInteractNumsFlushTime
    {
      get
      {
        return this._HeroInteractNumsFlushTime;
      }
      set
      {
        this._HeroInteractNumsFlushTime = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HeroInteractNums")]
    public int HeroInteractNums
    {
      get
      {
        return this._HeroInteractNums;
      }
      set
      {
        this._HeroInteractNums = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
