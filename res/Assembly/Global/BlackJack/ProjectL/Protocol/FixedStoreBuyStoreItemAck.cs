﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.FixedStoreBuyStoreItemAck
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "FixedStoreBuyStoreItemAck")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class FixedStoreBuyStoreItemAck : IExtensible
  {
    private int _Result;
    private int _StoreId;
    private int _GoodsId;
    private int _SelectedIndex;
    private long _GoodsNextFlushTime;
    private bool _IsResetNextFlushTime;
    private ProChangedGoodsNtf _Ntf;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public FixedStoreBuyStoreItemAck()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Result")]
    public int Result
    {
      get
      {
        return this._Result;
      }
      set
      {
        this._Result = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "StoreId")]
    public int StoreId
    {
      get
      {
        return this._StoreId;
      }
      set
      {
        this._StoreId = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "GoodsId")]
    public int GoodsId
    {
      get
      {
        return this._GoodsId;
      }
      set
      {
        this._GoodsId = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "SelectedIndex")]
    public int SelectedIndex
    {
      get
      {
        return this._SelectedIndex;
      }
      set
      {
        this._SelectedIndex = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "GoodsNextFlushTime")]
    [DefaultValue(0)]
    public long GoodsNextFlushTime
    {
      get
      {
        return this._GoodsNextFlushTime;
      }
      set
      {
        this._GoodsNextFlushTime = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = false, Name = "IsResetNextFlushTime")]
    [DefaultValue(false)]
    public bool IsResetNextFlushTime
    {
      get
      {
        return this._IsResetNextFlushTime;
      }
      set
      {
        this._IsResetNextFlushTime = value;
      }
    }

    [DefaultValue(null)]
    [ProtoMember(7, DataFormat = DataFormat.Default, IsRequired = false, Name = "Ntf")]
    public ProChangedGoodsNtf Ntf
    {
      get
      {
        return this._Ntf;
      }
      set
      {
        this._Ntf = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
