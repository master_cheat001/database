﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.GlobalRankingListComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.Common;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  [CustomLuaClass]
  public class GlobalRankingListComponent : GlobalRankingListComponentCommon
  {
    protected Dictionary<RankingListType, GlobalRankingListComponent.CachedRankingListInfo> m_cachedRankingListDict;
    protected Dictionary<int, GlobalRankingListComponent.CachedRankingListInfo> m_cachedSingleHeroRankListDict;
    protected const float OutDateTime = 15f;

    [MethodImpl((MethodImplOptions) 32768)]
    public GlobalRankingListComponent()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddRankingListInfoToCache(
      RankingListType rankingType,
      RankingListInfo rankingListInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RankingListInfo GetRankingListInfoByType(RankingListType rankingType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsRankingListInfoValid(RankingListType rankingListType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddSingleHeroRankingListInfoToCache(int heroId, RankingListInfo rankingListInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RankingListInfo GetSingleHeroRankingListInfoByHeroId(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsAbleQueryRankingListInfo(RankingListType rankingListType, out int errorCode)
    {
      return this.CheckRankingListInfoQuery(rankingListType, out errorCode);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsSingleHeroRankingListInfoValid(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [CustomLuaClass]
    public class CachedRankingListInfo
    {
      public DateTime m_outDateTime;
      public RankingListInfo m_rankingListInfo;
    }
  }
}
