﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.BattleReward
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.PlayerContext
{
  [HotFix]
  public class BattleReward
  {
    public int PlayerExp;
    public int HeroExp;
    public int Gold;
    public int FriendshipPoints;
    public List<BlackJack.ConfigData.Goods> Goods;
    public List<BlackJack.ConfigData.Goods> TeamGoods1;
    public List<BlackJack.ConfigData.Goods> TeamGoods2;
    public List<BlackJack.ConfigData.Goods> FriendGoods;
    public List<BlackJack.ConfigData.Goods> DailyGoods;
    public List<BlackJack.ConfigData.Goods> ExtraGoods;
    public List<BlackJack.ConfigData.Goods> ScoreGoods;
    public int Score;
    public int DailyScoreBonus;
    public int HeroScoreBonus;
    public int ExtraScore;
    public int TotalScore;
    [DoNotToLua]
    private BattleReward.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_Clear_hotfix;
    private LuaFunction m_IsEmpty_hotfix;
    private LuaFunction m_IsChestGoodsEmpty_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleReward()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Clear()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsEmpty()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsChestGoodsEmpty()
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public BattleReward.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private BattleReward m_owner;

      public LuaExportHelper(BattleReward owner)
      {
        this.m_owner = owner;
      }
    }
  }
}
