﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.GmCommand
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.PlayerContext
{
  [HotFix]
  public class GmCommand
  {
    public const string AddItem = "ADD_ITEM";
    public const string RemoveItem = "REMOVE_ITEM";
    public const string ClearBag = "CLEAR_BAG";
    public const string AddHero = "ADD_HERO";
    public const string SendTemplateMail = "POST_TEMPLATEMAIL";
    public const string CleanUserGuide = "CLEAN_USER_GUIDE";
    public const string AddActivity = "POST_GLOBALOPERATIONALACTIVITY";
    public const string BattleCheat = "BATTLE_CHEAT";
    public const string MaxTrainingTechs = "SET_MAXTRAININGTECHS";
    public const string LevelUpHeroFetter = "LevelUpHeroFetter";
    public const string LevelUpHeroHeartFetter = "Set_HeroHeartFetter";
    [DoNotToLua]
    private GmCommand.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public GmCommand()
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public GmCommand.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private GmCommand m_owner;

      public LuaExportHelper(GmCommand owner)
      {
        this.m_owner = owner;
      }
    }
  }
}
