﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.MailComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  [CustomLuaClass]
  public class MailComponent : MailComponentCommon
  {
    public int UnReadMailNums;

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Init()
    {
    }

    public override void PostInit()
    {
      base.PostInit();
    }

    public override void DeInit()
    {
      base.DeInit();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeSerialize(DSMailNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    public ushort GetDSVersion()
    {
      return this.m_mailDS.ClientVersion;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitMails(List<ProMail> pbMails)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAutoGetMailAttachment()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override Mail GetRealMail(Mail mail)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsOfficialMail(Mail mail)
    {
      return mail.TemplateId == 0;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override Mail InitTemplateMail(Mail mail)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
