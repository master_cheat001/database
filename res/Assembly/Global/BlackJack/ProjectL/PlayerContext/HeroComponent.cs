﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.HeroComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  [CustomLuaClass]
  public class HeroComponent : HeroComponentCommon
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public override void Init()
    {
    }

    public override void PostInit()
    {
      base.PostInit();
    }

    public override void DeInit()
    {
      base.DeInit();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeSerialize(DSHeroNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InteractHero(int heroId, int addFavorabilityExp)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroInteractionInfo GetHeroInteractionInfo(
      int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroInteractHeroPerformanceId(int heroId, HeroInteractionResultType resultType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanUnlockHeroPerformance(int heroPerformanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanUnlockHeroBiography(int biographyId)
    {
      // ISSUE: unable to decompile the method.
    }

    public ushort GetDSVersion()
    {
      return this.m_heroDS.ClientVersion;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearInitedData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetWearedEquipmentHeroIdByEquipmentId(ulong equipmentId)
    {
      // ISSUE: unable to decompile the method.
    }

    public int GetHeroInteractNums()
    {
      return this.m_heroDS.HeroInteractNums;
    }

    public DateTime GetHeroInteractNumsFlushTime()
    {
      return this.m_heroDS.HeroInteractNumsFlushTime;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ImitateUseHeroFavorabilityExpItem(
      int heroId,
      int itemId,
      int nums,
      GoodsType goodsType = GoodsType.GoodsType_Item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AutoEquipment(int heroId, List<ulong> equipmentInstanceIds)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
