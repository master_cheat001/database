﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.GuildComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using BlackJack.ProjectL.UI;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.PlayerContext
{
  [HotFix]
  public class GuildComponent : GuildComponentCommon
  {
    private BattleComponent m_battle;
    public Guild m_guild;
    public List<string> m_guildInviteUserIdList;
    public List<GuildSearchInfo> m_guildSearchList;
    public List<GuildSearchInfo> m_guildRecommendList;
    public List<GuildJoinInvitation> m_guildJoinInvitationList;
    public List<UserSummary> m_guildJoinApplyList;
    [DoNotToLua]
    private GuildComponent.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_Init_hotfix;
    private LuaFunction m_PostInit_hotfix;
    private LuaFunction m_GetGuildLogContentGuildLog_hotfix;
    private LuaFunction m_SetGuildDataSectionDSGuildNtf_hotfix;
    private LuaFunction m_RefreshGuildProGuild_hotfix;
    private LuaFunction m_RefreshGuildListJoinStateStringBoolean_hotfix;
    private LuaFunction m_PlayerRefuseGuildString_hotfix;
    private LuaFunction m_GetFinishedCombatThisWeekGuildMassiveCombatGeneral_hotfix;
    private LuaFunction m_FinishGuildCombatInt32List`1_hotfix;
    private LuaFunction m_CanStartMassiveCombatInt32_hotfix;
    private LuaFunction m_ResetGuild_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public GuildComponent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetGuildLogContent(GuildLog log)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGuildDataSection(DSGuildNtf dsGuildNtf)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RefreshGuild(ProGuild proGuild)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RefreshGuildListJoinState(string id, bool isJoinRequest)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayerRefuseGuild(string guildID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetFinishedCombatThisWeek(GuildMassiveCombatGeneral generalInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FinishGuildCombat(int massiveCombat, List<int> heroes = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override int CanStartMassiveCombat(int Difficulty)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ResetGuild()
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public GuildComponent.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private string __callBase_GetName()
    {
      return this.GetName();
    }

    private void __callBase_Init()
    {
      base.Init();
    }

    private void __callBase_PostInit()
    {
      base.PostInit();
    }

    private void __callBase_DeInit()
    {
      this.DeInit();
    }

    private void __callBase_Tick(uint deltaMillisecond)
    {
      this.Tick(deltaMillisecond);
    }

    private void __callBase_PostDeSerialize()
    {
      this.PostDeSerialize();
    }

    private bool __callBase_HasOwnGuild()
    {
      return this.HasOwnGuild();
    }

    private void __callBase_QuitGuild(DateTime nextJoinTime)
    {
      this.QuitGuild(nextJoinTime);
    }

    private string __callBase_GetGuildId()
    {
      return this.GetGuildId();
    }

    private void __callBase_SetGuildId(string id)
    {
      this.SetGuildId(id);
    }

    private int __callBase_CanCreateGuild(
      string guildName,
      string hiringDeclaration,
      int joinLevel)
    {
      return this.CanCreateGuild(guildName, hiringDeclaration, joinLevel);
    }

    private int __callBase_CanJoinGuild()
    {
      return this.CanJoinGuild();
    }

    private int __callBase_CanQuitGuild()
    {
      return this.CanQuitGuild();
    }

    private int __callBase_CanKickOutGuild()
    {
      return this.CanKickOutGuild();
    }

    private int __callBase_CanApplyToJoinGuild()
    {
      return this.CanApplyToJoinGuild();
    }

    private int __callBase_CanConfirmJoinGuildInvitation(string guildId)
    {
      return this.CanConfirmJoinGuildInvitation(guildId);
    }

    private void __callBase_RefuseJoinGuildInvitation(string guildId)
    {
      this.RefuseJoinGuildInvitation(guildId);
    }

    private void __callBase_RefuseAllJoinGuildInvitation()
    {
      this.RefuseAllJoinGuildInvitation();
    }

    private int __callBase_CheckGuildName(string name)
    {
      return this.CheckGuildName(name);
    }

    private int __callBase_CheckGuildSearch(string searchText)
    {
      return this.CheckGuildSearch(searchText);
    }

    private int __callBase_CheckGuildRandomList()
    {
      return this.CheckGuildRandomList();
    }

    private int __callBase_CheckGuildInvitePlayerList()
    {
      return this.CheckGuildInvitePlayerList();
    }

    private int __callBase_CanSetGuildHiringDeclaration(string hiringDeclaration)
    {
      return this.CanSetGuildHiringDeclaration(hiringDeclaration);
    }

    private int __callBase_CheckGuildHiringDeclaration(string hiringDeclaration)
    {
      return this.CheckGuildHiringDeclaration(hiringDeclaration);
    }

    private int __callBase_CanSetGuildAnnouncement(string announcement)
    {
      return this.CanSetGuildAnnouncement(announcement);
    }

    private int __callBase_CheckGuildAnnouncement(string announcement)
    {
      return this.CheckGuildAnnouncement(announcement);
    }

    private int __callBase_CanStartMassiveCombat(int difficulty)
    {
      return base.CanStartMassiveCombat(difficulty);
    }

    private int __callBase_CanTheseHeroesAttackStronghold(List<int> heroIds)
    {
      return this.CanTheseHeroesAttackStronghold(heroIds);
    }

    private int __callBase_CanAttackStronghold(int levelId)
    {
      return this.CanAttackStronghold(levelId);
    }

    private List<Hero> __callBase_GetMassiveCombatUnusedHeroes()
    {
      return this.GetMassiveCombatUnusedHeroes();
    }

    private int __callBase_GetEliminateRate(GuildMassiveCombatInfo combat)
    {
      return this.GetEliminateRate(combat);
    }

    private int __callBase_GetStrongholdEliminateRate(GuildMassiveCombatStronghold stronghold)
    {
      return this.GetStrongholdEliminateRate(stronghold);
    }

    private int __callBase_GetStartedCombatThisWeek(GuildMassiveCombatGeneral generalInfo)
    {
      return this.GetStartedCombatThisWeek(generalInfo);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private GuildComponent m_owner;

      public LuaExportHelper(GuildComponent owner)
      {
        this.m_owner = owner;
      }

      public string __callBase_GetName()
      {
        return this.m_owner.__callBase_GetName();
      }

      public void __callBase_Init()
      {
        this.m_owner.__callBase_Init();
      }

      public void __callBase_PostInit()
      {
        this.m_owner.__callBase_PostInit();
      }

      public void __callBase_DeInit()
      {
        this.m_owner.__callBase_DeInit();
      }

      public void __callBase_Tick(uint deltaMillisecond)
      {
        this.m_owner.__callBase_Tick(deltaMillisecond);
      }

      public void __callBase_PostDeSerialize()
      {
        this.m_owner.__callBase_PostDeSerialize();
      }

      public bool __callBase_HasOwnGuild()
      {
        return this.m_owner.__callBase_HasOwnGuild();
      }

      public void __callBase_QuitGuild(DateTime nextJoinTime)
      {
        this.m_owner.__callBase_QuitGuild(nextJoinTime);
      }

      public string __callBase_GetGuildId()
      {
        return this.m_owner.__callBase_GetGuildId();
      }

      public void __callBase_SetGuildId(string id)
      {
        this.m_owner.__callBase_SetGuildId(id);
      }

      public int __callBase_CanCreateGuild(
        string guildName,
        string hiringDeclaration,
        int joinLevel)
      {
        return this.m_owner.__callBase_CanCreateGuild(guildName, hiringDeclaration, joinLevel);
      }

      public int __callBase_CanJoinGuild()
      {
        return this.m_owner.__callBase_CanJoinGuild();
      }

      public int __callBase_CanQuitGuild()
      {
        return this.m_owner.__callBase_CanQuitGuild();
      }

      public int __callBase_CanKickOutGuild()
      {
        return this.m_owner.__callBase_CanKickOutGuild();
      }

      public int __callBase_CanApplyToJoinGuild()
      {
        return this.m_owner.__callBase_CanApplyToJoinGuild();
      }

      public int __callBase_CanConfirmJoinGuildInvitation(string guildId)
      {
        return this.m_owner.__callBase_CanConfirmJoinGuildInvitation(guildId);
      }

      public void __callBase_RefuseJoinGuildInvitation(string guildId)
      {
        this.m_owner.__callBase_RefuseJoinGuildInvitation(guildId);
      }

      public void __callBase_RefuseAllJoinGuildInvitation()
      {
        this.m_owner.__callBase_RefuseAllJoinGuildInvitation();
      }

      public int __callBase_CheckGuildName(string name)
      {
        return this.m_owner.__callBase_CheckGuildName(name);
      }

      public int __callBase_CheckGuildSearch(string searchText)
      {
        return this.m_owner.__callBase_CheckGuildSearch(searchText);
      }

      public int __callBase_CheckGuildRandomList()
      {
        return this.m_owner.__callBase_CheckGuildRandomList();
      }

      public int __callBase_CheckGuildInvitePlayerList()
      {
        return this.m_owner.__callBase_CheckGuildInvitePlayerList();
      }

      public int __callBase_CanSetGuildHiringDeclaration(string hiringDeclaration)
      {
        return this.m_owner.__callBase_CanSetGuildHiringDeclaration(hiringDeclaration);
      }

      public int __callBase_CheckGuildHiringDeclaration(string hiringDeclaration)
      {
        return this.m_owner.__callBase_CheckGuildHiringDeclaration(hiringDeclaration);
      }

      public int __callBase_CanSetGuildAnnouncement(string announcement)
      {
        return this.m_owner.__callBase_CanSetGuildAnnouncement(announcement);
      }

      public int __callBase_CheckGuildAnnouncement(string announcement)
      {
        return this.m_owner.__callBase_CheckGuildAnnouncement(announcement);
      }

      public int __callBase_CanStartMassiveCombat(int difficulty)
      {
        return this.m_owner.__callBase_CanStartMassiveCombat(difficulty);
      }

      public int __callBase_CanTheseHeroesAttackStronghold(List<int> heroIds)
      {
        return this.m_owner.__callBase_CanTheseHeroesAttackStronghold(heroIds);
      }

      public int __callBase_CanAttackStronghold(int levelId)
      {
        return this.m_owner.__callBase_CanAttackStronghold(levelId);
      }

      public List<Hero> __callBase_GetMassiveCombatUnusedHeroes()
      {
        return this.m_owner.__callBase_GetMassiveCombatUnusedHeroes();
      }

      public int __callBase_GetEliminateRate(GuildMassiveCombatInfo combat)
      {
        return this.m_owner.__callBase_GetEliminateRate(combat);
      }

      public int __callBase_GetStrongholdEliminateRate(GuildMassiveCombatStronghold stronghold)
      {
        return this.m_owner.__callBase_GetStrongholdEliminateRate(stronghold);
      }

      public int __callBase_GetStartedCombatThisWeek(GuildMassiveCombatGeneral generalInfo)
      {
        return this.m_owner.__callBase_GetStartedCombatThisWeek(generalInfo);
      }

      public BattleComponent m_battle
      {
        get
        {
          return this.m_owner.m_battle;
        }
        set
        {
          this.m_owner.m_battle = value;
        }
      }
    }
  }
}
