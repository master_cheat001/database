﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.RealTimePVPComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  [CustomLuaClass]
  public class RealTimePVPComponent : RealTimePVPComponentCommon
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public override void Init()
    {
    }

    public override void PostInit()
    {
      base.PostInit();
    }

    public override void DeInit()
    {
      base.DeInit();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeSerialize(DSRealTimePVPNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    public ushort GetDSVersion()
    {
      return this.m_realTimePVP.ClientVersion;
    }

    public RealTimePVPMatchStats GetLadderMatchStats()
    {
      return this.m_realTimePVP.LadderMatchStats;
    }

    public RealTimePVPMatchStats GetFriendlyMatchStats()
    {
      return this.m_realTimePVP.FriendlyMatchStats;
    }

    public int Dan { get; set; }

    public int Score { get; set; }
  }
}
