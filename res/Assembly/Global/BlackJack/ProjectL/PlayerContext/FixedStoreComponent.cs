﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.FixedStoreComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.PlayerContext
{
  [HotFix]
  public class FixedStoreComponent : FixedStoreComponentCommon
  {
    [DoNotToLua]
    private FixedStoreComponent.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_Init_hotfix;
    private LuaFunction m_PostInit_hotfix;
    private LuaFunction m_DeInit_hotfix;
    private LuaFunction m_DeSerializeDSFixedStoreNtf_hotfix;
    private LuaFunction m_InitStoresList`1_hotfix;
    private LuaFunction m_IsStoreInfoEmpty_hotfix;
    private LuaFunction m_FindStoreByIdInt32_hotfix;
    private LuaFunction m_InitStoreInfoProFixedStore_hotfix;
    private LuaFunction m_BuyStoreItemInt32Int32Int64Boolean_hotfix;
    private LuaFunction m_GetDSVersion_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public FixedStoreComponent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeSerialize(DSFixedStoreNtf ntf)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitStores(List<ProFixedStore> pbStores)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsStoreInfoEmpty()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public FixedStore FindStoreById(int storeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitStoreInfo(ProFixedStore pbStore)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void BuyStoreItem(int storeId, int goodsId, long nextFlushTime, bool isReseted)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ushort GetDSVersion()
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public FixedStoreComponent.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private string __callBase_GetName()
    {
      return this.GetName();
    }

    private void __callBase_Init()
    {
      base.Init();
    }

    private void __callBase_PostInit()
    {
      base.PostInit();
    }

    private void __callBase_DeInit()
    {
      base.DeInit();
    }

    private void __callBase_Tick(uint deltaMillisecond)
    {
      this.Tick(deltaMillisecond);
    }

    private void __callBase_PostDeSerialize()
    {
      this.PostDeSerialize();
    }

    private bool __callBase_IsSoldOut(ConfigDataFixedStoreItemInfo itemConfig, FixedStoreItem item)
    {
      return this.IsSoldOut(itemConfig, item);
    }

    private bool __callBase_IsInSaleTime(ConfigDataFixedStoreItemInfo itemConfig)
    {
      return this.IsInSaleTime(itemConfig);
    }

    private int __callBase_CanBuyGoods(int storeId, int goodsId, int selectedIndex)
    {
      return this.CanBuyGoods(storeId, goodsId, selectedIndex);
    }

    private int __callBase_CanBuyFixedStoreItem(
      ConfigDataFixedStoreItemInfo itemConfig,
      FixedStoreItem item)
    {
      return this.CanBuyFixedStoreItem(itemConfig, item);
    }

    private int __callBase_CaculateCurrencyCount(
      ConfigDataFixedStoreItemInfo storeItemConfig,
      bool isFirstBuy)
    {
      return this.CaculateCurrencyCount(storeItemConfig, isFirstBuy);
    }

    private bool __callBase_IsOnDiscountPeriod(ConfigDataFixedStoreItemInfo storeItemConfig)
    {
      return this.IsOnDiscountPeriod(storeItemConfig);
    }

    private FixedStore __callBase_GetStore(int storeId)
    {
      return this.GetStore(storeId);
    }

    private void __callBase_BuyStoreItem(int storeId, FixedStoreItem storeItem, int count)
    {
      this.BuyStoreItem(storeId, storeItem, count);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private FixedStoreComponent m_owner;

      public LuaExportHelper(FixedStoreComponent owner)
      {
        this.m_owner = owner;
      }

      public string __callBase_GetName()
      {
        return this.m_owner.__callBase_GetName();
      }

      public void __callBase_Init()
      {
        this.m_owner.__callBase_Init();
      }

      public void __callBase_PostInit()
      {
        this.m_owner.__callBase_PostInit();
      }

      public void __callBase_DeInit()
      {
        this.m_owner.__callBase_DeInit();
      }

      public void __callBase_Tick(uint deltaMillisecond)
      {
        this.m_owner.__callBase_Tick(deltaMillisecond);
      }

      public void __callBase_PostDeSerialize()
      {
        this.m_owner.__callBase_PostDeSerialize();
      }

      public bool __callBase_IsSoldOut(ConfigDataFixedStoreItemInfo itemConfig, FixedStoreItem item)
      {
        return this.m_owner.__callBase_IsSoldOut(itemConfig, item);
      }

      public bool __callBase_IsInSaleTime(ConfigDataFixedStoreItemInfo itemConfig)
      {
        return this.m_owner.__callBase_IsInSaleTime(itemConfig);
      }

      public int __callBase_CanBuyGoods(int storeId, int goodsId, int selectedIndex)
      {
        return this.m_owner.__callBase_CanBuyGoods(storeId, goodsId, selectedIndex);
      }

      public int __callBase_CanBuyFixedStoreItem(
        ConfigDataFixedStoreItemInfo itemConfig,
        FixedStoreItem item)
      {
        return this.m_owner.__callBase_CanBuyFixedStoreItem(itemConfig, item);
      }

      public int __callBase_CaculateCurrencyCount(
        ConfigDataFixedStoreItemInfo storeItemConfig,
        bool isFirstBuy)
      {
        return this.m_owner.__callBase_CaculateCurrencyCount(storeItemConfig, isFirstBuy);
      }

      public bool __callBase_IsOnDiscountPeriod(ConfigDataFixedStoreItemInfo storeItemConfig)
      {
        return this.m_owner.__callBase_IsOnDiscountPeriod(storeItemConfig);
      }

      public FixedStore __callBase_GetStore(int storeId)
      {
        return this.m_owner.__callBase_GetStore(storeId);
      }

      public void __callBase_BuyStoreItem(int storeId, FixedStoreItem storeItem, int count)
      {
        this.m_owner.__callBase_BuyStoreItem(storeId, storeItem, count);
      }
    }
  }
}
