﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataCardPoolInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataCardPoolInfo")]
  [CustomLuaClass]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class ConfigDataCardPoolInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private CardPoolType _PoolType;
    private CardSelectType _SelectType;
    private int _CrystalCost;
    private int _TenSelectDiscount;
    private int _TenSelectCount;
    private int _TicketId;
    private int _CardPoolSelectMaxCount;
    private int _SortID;
    private int _GuaranteedGroupID;
    private string _Icon;
    private string _AdsImage;
    private int _SelectProbabilityInfoID;
    private int _SelectContentInfoID;
    private string _ToggleClickImage;
    private string _ToggleUnClickImage;
    private MissionCardPoolType _MissionCardPoolType;
    private int _TenSelectGuaranteedMinCount;
    private int _ReplaceHeroGroup;
    private int _EquipmentSuitGroup;
    private int _EquipmentSuitGuaranteedMinValue;
    private int _EquipmentSuitGuaranteedMaxValue;
    private string _CardPoolDetailDesc;
    private IExtension extensionObject;
    public Dictionary<int, ConfigDataCardPoolGroupInfo> Groups;
    public ConfigDataCardPoolGroupInfo m_replaceHeroGroup;
    public ConfigDataCardPoolGroupInfo m_equipmentSuitGroup;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCardPoolInfo()
    {
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "PoolType")]
    public CardPoolType PoolType
    {
      get
      {
        return this._PoolType;
      }
      set
      {
        this._PoolType = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "SelectType")]
    public CardSelectType SelectType
    {
      get
      {
        return this._SelectType;
      }
      set
      {
        this._SelectType = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CrystalCost")]
    public int CrystalCost
    {
      get
      {
        return this._CrystalCost;
      }
      set
      {
        this._CrystalCost = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "TenSelectDiscount")]
    public int TenSelectDiscount
    {
      get
      {
        return this._TenSelectDiscount;
      }
      set
      {
        this._TenSelectDiscount = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "TenSelectCount")]
    public int TenSelectCount
    {
      get
      {
        return this._TenSelectCount;
      }
      set
      {
        this._TenSelectCount = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "TicketId")]
    public int TicketId
    {
      get
      {
        return this._TicketId;
      }
      set
      {
        this._TicketId = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CardPoolSelectMaxCount")]
    public int CardPoolSelectMaxCount
    {
      get
      {
        return this._CardPoolSelectMaxCount;
      }
      set
      {
        this._CardPoolSelectMaxCount = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "SortID")]
    public int SortID
    {
      get
      {
        return this._SortID;
      }
      set
      {
        this._SortID = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "GuaranteedGroupID")]
    public int GuaranteedGroupID
    {
      get
      {
        return this._GuaranteedGroupID;
      }
      set
      {
        this._GuaranteedGroupID = value;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.Default, IsRequired = true, Name = "Icon")]
    public string Icon
    {
      get
      {
        return this._Icon;
      }
      set
      {
        this._Icon = value;
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.Default, IsRequired = true, Name = "AdsImage")]
    public string AdsImage
    {
      get
      {
        return this._AdsImage;
      }
      set
      {
        this._AdsImage = value;
      }
    }

    [ProtoMember(15, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "SelectProbabilityInfoID")]
    public int SelectProbabilityInfoID
    {
      get
      {
        return this._SelectProbabilityInfoID;
      }
      set
      {
        this._SelectProbabilityInfoID = value;
      }
    }

    [ProtoMember(16, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "SelectContentInfoID")]
    public int SelectContentInfoID
    {
      get
      {
        return this._SelectContentInfoID;
      }
      set
      {
        this._SelectContentInfoID = value;
      }
    }

    [ProtoMember(17, DataFormat = DataFormat.Default, IsRequired = true, Name = "ToggleClickImage")]
    public string ToggleClickImage
    {
      get
      {
        return this._ToggleClickImage;
      }
      set
      {
        this._ToggleClickImage = value;
      }
    }

    [ProtoMember(18, DataFormat = DataFormat.Default, IsRequired = true, Name = "ToggleUnClickImage")]
    public string ToggleUnClickImage
    {
      get
      {
        return this._ToggleUnClickImage;
      }
      set
      {
        this._ToggleUnClickImage = value;
      }
    }

    [ProtoMember(19, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MissionCardPoolType")]
    public MissionCardPoolType MissionCardPoolType
    {
      get
      {
        return this._MissionCardPoolType;
      }
      set
      {
        this._MissionCardPoolType = value;
      }
    }

    [ProtoMember(20, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "TenSelectGuaranteedMinCount")]
    public int TenSelectGuaranteedMinCount
    {
      get
      {
        return this._TenSelectGuaranteedMinCount;
      }
      set
      {
        this._TenSelectGuaranteedMinCount = value;
      }
    }

    [ProtoMember(21, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ReplaceHeroGroup")]
    public int ReplaceHeroGroup
    {
      get
      {
        return this._ReplaceHeroGroup;
      }
      set
      {
        this._ReplaceHeroGroup = value;
      }
    }

    [ProtoMember(22, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "EquipmentSuitGroup")]
    public int EquipmentSuitGroup
    {
      get
      {
        return this._EquipmentSuitGroup;
      }
      set
      {
        this._EquipmentSuitGroup = value;
      }
    }

    [ProtoMember(23, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "EquipmentSuitGuaranteedMinValue")]
    public int EquipmentSuitGuaranteedMinValue
    {
      get
      {
        return this._EquipmentSuitGuaranteedMinValue;
      }
      set
      {
        this._EquipmentSuitGuaranteedMinValue = value;
      }
    }

    [ProtoMember(24, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "EquipmentSuitGuaranteedMaxValue")]
    public int EquipmentSuitGuaranteedMaxValue
    {
      get
      {
        return this._EquipmentSuitGuaranteedMaxValue;
      }
      set
      {
        this._EquipmentSuitGuaranteedMaxValue = value;
      }
    }

    [ProtoMember(25, DataFormat = DataFormat.Default, IsRequired = true, Name = "CardPoolDetailDesc")]
    public string CardPoolDetailDesc
    {
      get
      {
        return this._CardPoolDetailDesc;
      }
      set
      {
        this._CardPoolDetailDesc = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
