﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.SkillType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "SkillType")]
  public enum SkillType
  {
    [ProtoEnum(Name = "SkillType_None", Value = 0)] SkillType_None,
    [ProtoEnum(Name = "SkillType_Slash", Value = 1)] SkillType_Slash,
    [ProtoEnum(Name = "SkillType_Shoot", Value = 2)] SkillType_Shoot,
    [ProtoEnum(Name = "SkillType_Charge", Value = 3)] SkillType_Charge,
    [ProtoEnum(Name = "SkillType_MagicDamage", Value = 4)] SkillType_MagicDamage,
    [ProtoEnum(Name = "SkillType_BF_Heal", Value = 5)] SkillType_BF_Heal,
    [ProtoEnum(Name = "SkillType_BF_HealRemoveCD", Value = 6)] SkillType_BF_HealRemoveCD,
    [ProtoEnum(Name = "SkillType_BF_HealNewTurn", Value = 7)] SkillType_BF_HealNewTurn,
    [ProtoEnum(Name = "SkillType_BF_Teleport", Value = 8)] SkillType_BF_Teleport,
    [ProtoEnum(Name = "SkillType_BF_Summon", Value = 9)] SkillType_BF_Summon,
    [ProtoEnum(Name = "SkillType_BF_MagicDamage", Value = 10)] SkillType_BF_MagicDamage,
    [ProtoEnum(Name = "SkillType_Passive", Value = 11)] SkillType_Passive,
    [ProtoEnum(Name = "SkillType_BF_DamageHeal", Value = 12)] SkillType_BF_DamageHeal,
    [ProtoEnum(Name = "SkillType_BF_HealPercent", Value = 13)] SkillType_BF_HealPercent,
    [ProtoEnum(Name = "SkillType_BF_AddBuff", Value = 14)] SkillType_BF_AddBuff,
  }
}
