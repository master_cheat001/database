﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataArenaDefendRuleInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ConfigData
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "ConfigDataArenaDefendRuleInfo")]
  [CustomLuaClass]
  [Serializable]
  public class ConfigDataArenaDefendRuleInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _DefendWinDesc;
    private string _DefendLoseDesc;
    private string _WinDesc;
    private string _LoseDesc;
    private int _TurnMax;
    private List<int> _WinConditions_ID;
    private List<int> _LoseConditions_ID;
    private string _Icon;
    private IExtension extensionObject;
    public ConfigDataBattleWinConditionInfo[] m_battleWinConditionInfos;
    public ConfigDataBattleLoseConditionInfo[] m_battleLoseConditionInfos;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataArenaDefendRuleInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "DefendWinDesc")]
    public string DefendWinDesc
    {
      get
      {
        return this._DefendWinDesc;
      }
      set
      {
        this._DefendWinDesc = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, IsRequired = true, Name = "DefendLoseDesc")]
    public string DefendLoseDesc
    {
      get
      {
        return this._DefendLoseDesc;
      }
      set
      {
        this._DefendLoseDesc = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.Default, IsRequired = true, Name = "WinDesc")]
    public string WinDesc
    {
      get
      {
        return this._WinDesc;
      }
      set
      {
        this._WinDesc = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.Default, IsRequired = true, Name = "LoseDesc")]
    public string LoseDesc
    {
      get
      {
        return this._LoseDesc;
      }
      set
      {
        this._LoseDesc = value;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "TurnMax")]
    public int TurnMax
    {
      get
      {
        return this._TurnMax;
      }
      set
      {
        this._TurnMax = value;
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.TwosComplement, Name = "WinConditions_ID")]
    public List<int> WinConditions_ID
    {
      get
      {
        return this._WinConditions_ID;
      }
    }

    [ProtoMember(15, DataFormat = DataFormat.TwosComplement, Name = "LoseConditions_ID")]
    public List<int> LoseConditions_ID
    {
      get
      {
        return this._LoseConditions_ID;
      }
    }

    [ProtoMember(16, DataFormat = DataFormat.Default, IsRequired = true, Name = "Icon")]
    public string Icon
    {
      get
      {
        return this._Icon;
      }
      set
      {
        this._Icon = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
