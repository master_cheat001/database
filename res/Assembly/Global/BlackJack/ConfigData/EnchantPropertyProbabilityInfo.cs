﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.EnchantPropertyProbabilityInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System.Collections.Generic;

namespace BlackJack.ConfigData
{
  [CustomLuaClass]
  public class EnchantPropertyProbabilityInfo
  {
    public PropertyModifyType Id { get; set; }

    public List<EnchantPropertyValueInfo> PropertyValues { get; set; }

    public int Weight { get; set; }
  }
}
