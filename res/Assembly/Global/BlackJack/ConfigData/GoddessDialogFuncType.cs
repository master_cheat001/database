﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.GoddessDialogFuncType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "GoddessDialogFuncType")]
  public enum GoddessDialogFuncType
  {
    [ProtoEnum(Name = "GoddessDialogFuncType_None", Value = 0)] GoddessDialogFuncType_None,
    [ProtoEnum(Name = "GoddessDialogFuncType_Start", Value = 1)] GoddessDialogFuncType_Start,
    [ProtoEnum(Name = "GoddessDialogFuncType_QuestionStart", Value = 2)] GoddessDialogFuncType_QuestionStart,
    [ProtoEnum(Name = "GoddessDialogFuncType_Result", Value = 3)] GoddessDialogFuncType_Result,
    [ProtoEnum(Name = "GoddessDialogFuncType_Select", Value = 4)] GoddessDialogFuncType_Select,
    [ProtoEnum(Name = "GoddessDialogFuncType_Final", Value = 5)] GoddessDialogFuncType_Final,
  }
}
