﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.LabelType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "LabelType")]
  public enum LabelType
  {
    [ProtoEnum(Name = "LabelType_None", Value = 0)] LabelType_None,
    [ProtoEnum(Name = "LabelType_FixedTime", Value = 1)] LabelType_FixedTime,
    [ProtoEnum(Name = "LabelType_Recommand", Value = 2)] LabelType_Recommand,
    [ProtoEnum(Name = "LabelType_Discount", Value = 3)] LabelType_Discount,
    [ProtoEnum(Name = "LabelType_FirstDiscount", Value = 4)] LabelType_FirstDiscount,
  }
}
