﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.UserGuideCondition
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "UserGuideCondition")]
  public enum UserGuideCondition
  {
    [ProtoEnum(Name = "UserGuideCondition_", Value = 0)] UserGuideCondition_,
    [ProtoEnum(Name = "UserGuideCondition_StoryNFinished", Value = 1)] UserGuideCondition_StoryNFinished,
    [ProtoEnum(Name = "UserGuideCondition_TodayIsSignedEqual", Value = 2)] UserGuideCondition_TodayIsSignedEqual,
    [ProtoEnum(Name = "UserGuideCondition_GuideNFinished", Value = 3)] UserGuideCondition_GuideNFinished,
    [ProtoEnum(Name = "UserGuideCondition_RiftChapterNUnlockable", Value = 4)] UserGuideCondition_RiftChapterNUnlockable,
    [ProtoEnum(Name = "UserGuideCondition_RiftLevelNPassed", Value = 5)] UserGuideCondition_RiftLevelNPassed,
    [ProtoEnum(Name = "UserGuideCondition_EventNHappened", Value = 6)] UserGuideCondition_EventNHappened,
    [ProtoEnum(Name = "UserGuideCondition_IsRunningBattleN", Value = 7)] UserGuideCondition_IsRunningBattleN,
    [ProtoEnum(Name = "UserGuideCondition_IsWayPointNArrived", Value = 8)] UserGuideCondition_IsWayPointNArrived,
    [ProtoEnum(Name = "UserGuideCondition_IsHeroNComposable", Value = 9)] UserGuideCondition_IsHeroNComposable,
    [ProtoEnum(Name = "UserGuideCondition_GuideNFinishedAndMNotFinished", Value = 10)] UserGuideCondition_GuideNFinishedAndMNotFinished,
    [ProtoEnum(Name = "UserGuideCondition_NeverOpenedActivityNoticeAndPlayerLevelReachN", Value = 11)] UserGuideCondition_NeverOpenedActivityNoticeAndPlayerLevelReachN,
    [ProtoEnum(Name = "UserGuideCondition_PlayerLevelReachN", Value = 12)] UserGuideCondition_PlayerLevelReachN,
    [ProtoEnum(Name = "UserGuideCondition_GuideNUnfinished", Value = 13)] UserGuideCondition_GuideNUnfinished,
  }
}
