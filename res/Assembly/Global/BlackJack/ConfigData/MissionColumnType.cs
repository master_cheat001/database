﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.MissionColumnType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "MissionColumnType")]
  public enum MissionColumnType
  {
    [ProtoEnum(Name = "MissionColumnType_Everyday", Value = 1)] MissionColumnType_Everyday = 1,
    [ProtoEnum(Name = "MissionColumnType_Challenge", Value = 2)] MissionColumnType_Challenge = 2,
    [ProtoEnum(Name = "MissionColumnType_Achievements", Value = 3)] MissionColumnType_Achievements = 3,
    [ProtoEnum(Name = "MissionColumnType_HeroFetter", Value = 4)] MissionColumnType_HeroFetter = 4,
    [ProtoEnum(Name = "MissionColumnType_BeginningOfTheLegend", Value = 5)] MissionColumnType_BeginningOfTheLegend = 5,
    [ProtoEnum(Name = "MissionColumnType_RefluxActivity", Value = 6)] MissionColumnType_RefluxActivity = 6,
  }
}
