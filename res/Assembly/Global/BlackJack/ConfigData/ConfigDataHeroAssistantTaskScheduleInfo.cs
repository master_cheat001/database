﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataHeroAssistantTaskScheduleInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ConfigData
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "ConfigDataHeroAssistantTaskScheduleInfo")]
  [Serializable]
  public class ConfigDataHeroAssistantTaskScheduleInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private List<string> _Resource;
    private List<int> _TaskIDs;
    private int _Weekday;
    private List<int> _RecommendHeroes;
    private List<int> _SoldierTypeList;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroAssistantTaskScheduleInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, Name = "Resource")]
    public List<string> Resource
    {
      get
      {
        return this._Resource;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, Name = "TaskIDs")]
    public List<int> TaskIDs
    {
      get
      {
        return this._TaskIDs;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Weekday")]
    public int Weekday
    {
      get
      {
        return this._Weekday;
      }
      set
      {
        this._Weekday = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, Name = "RecommendHeroes")]
    public List<int> RecommendHeroes
    {
      get
      {
        return this._RecommendHeroes;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, Name = "SoldierTypeList")]
    public List<int> SoldierTypeList
    {
      get
      {
        return this._SoldierTypeList;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
