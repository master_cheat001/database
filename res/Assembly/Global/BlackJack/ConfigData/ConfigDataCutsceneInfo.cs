﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataCutsceneInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ConfigData
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "ConfigDataCutsceneInfo")]
  [Serializable]
  public class ConfigDataCutsceneInfo : IExtensible
  {
    private int _ID;
    private int _Time;
    private string _Fx;
    private string _Head;
    private string _CameraAnimation;
    private string _Sound1;
    private string _Sound2;
    private string _Sound3;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCutsceneInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Time")]
    public int Time
    {
      get
      {
        return this._Time;
      }
      set
      {
        this._Time = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = true, Name = "Fx")]
    public string Fx
    {
      get
      {
        return this._Fx;
      }
      set
      {
        this._Fx = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, IsRequired = true, Name = "Head")]
    public string Head
    {
      get
      {
        return this._Head;
      }
      set
      {
        this._Head = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.Default, IsRequired = true, Name = "CameraAnimation")]
    public string CameraAnimation
    {
      get
      {
        return this._CameraAnimation;
      }
      set
      {
        this._CameraAnimation = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.Default, IsRequired = true, Name = "Sound1")]
    public string Sound1
    {
      get
      {
        return this._Sound1;
      }
      set
      {
        this._Sound1 = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.Default, IsRequired = true, Name = "Sound2")]
    public string Sound2
    {
      get
      {
        return this._Sound2;
      }
      set
      {
        this._Sound2 = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.Default, IsRequired = true, Name = "Sound3")]
    public string Sound3
    {
      get
      {
        return this._Sound3;
      }
      set
      {
        this._Sound3 = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
