﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.StoreId
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "StoreId")]
  public enum StoreId
  {
    [ProtoEnum(Name = "StoreId_None", Value = 0)] StoreId_None,
    [ProtoEnum(Name = "StoreId_BlackMarket", Value = 1)] StoreId_BlackMarket,
    [ProtoEnum(Name = "StoreId_Crystal", Value = 2)] StoreId_Crystal,
    [ProtoEnum(Name = "StoreId_Honor", Value = 3)] StoreId_Honor,
    [ProtoEnum(Name = "StoreId_Friendship", Value = 4)] StoreId_Friendship,
    [ProtoEnum(Name = "StoreId_SkinHero", Value = 5)] StoreId_SkinHero,
    [ProtoEnum(Name = "StoreId_SkinSoldier", Value = 6)] StoreId_SkinSoldier,
    [ProtoEnum(Name = "StoreId_Gift", Value = 7)] StoreId_Gift,
    [ProtoEnum(Name = "StoreId_Privilege", Value = 8)] StoreId_Privilege,
    [ProtoEnum(Name = "StoreId_Medal", Value = 9)] StoreId_Medal,
    [ProtoEnum(Name = "StoreId_Union", Value = 10)] StoreId_Union,
    [ProtoEnum(Name = "StoreId_Recharge", Value = 11)] StoreId_Recharge,
    [ProtoEnum(Name = "StoreId_Memory", Value = 12)] StoreId_Memory,
    [ProtoEnum(Name = "StoreId_Equipment", Value = 13)] StoreId_Equipment,
    [ProtoEnum(Name = "StoreId_GuildPerson", Value = 14)] StoreId_GuildPerson,
  }
}
