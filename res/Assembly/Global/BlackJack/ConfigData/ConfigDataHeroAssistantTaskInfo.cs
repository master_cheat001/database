﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataHeroAssistantTaskInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ConfigData
{
  [HotFix(true, m_isCtorOnly = true)]
  [CustomLuaClass]
  [ProtoContract(Name = "ConfigDataHeroAssistantTaskInfo")]
  [Serializable]
  public class ConfigDataHeroAssistantTaskInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private List<string> _Resource;
    private int _RequiredUserLevel;
    private int _CompletePoints;
    private List<CompleteValueDropID> _Rewards;
    private IExtension extensionObject;
    public ConfigDataHeroAssistantTaskScheduleInfo m_schedule;
    public ConfigDataHeroAssistantTaskGeneralInfo m_general;
    public List<int> m_rewardCompleteRate;
    public List<int> m_rewardDropId;
    public List<int> m_rewardWorkSeconds;
    public List<int> m_rewardDropCount;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroAssistantTaskInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, Name = "Resource")]
    public List<string> Resource
    {
      get
      {
        return this._Resource;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "RequiredUserLevel")]
    public int RequiredUserLevel
    {
      get
      {
        return this._RequiredUserLevel;
      }
      set
      {
        this._RequiredUserLevel = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CompletePoints")]
    public int CompletePoints
    {
      get
      {
        return this._CompletePoints;
      }
      set
      {
        this._CompletePoints = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, Name = "Rewards")]
    public List<CompleteValueDropID> Rewards
    {
      get
      {
        return this._Rewards;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
