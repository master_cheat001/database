﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.EventFuncType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "EventFuncType")]
  public enum EventFuncType
  {
    [ProtoEnum(Name = "EventFuncType_None", Value = 0)] EventFuncType_None,
    [ProtoEnum(Name = "EventFuncType_Monster", Value = 1)] EventFuncType_Monster,
    [ProtoEnum(Name = "EventFuncType_Mission", Value = 2)] EventFuncType_Mission,
    [ProtoEnum(Name = "EventFuncType_Dialog", Value = 3)] EventFuncType_Dialog,
    [ProtoEnum(Name = "EventFuncType_Treasure", Value = 4)] EventFuncType_Treasure,
    [ProtoEnum(Name = "EventFuncType_Shop", Value = 5)] EventFuncType_Shop,
  }
}
