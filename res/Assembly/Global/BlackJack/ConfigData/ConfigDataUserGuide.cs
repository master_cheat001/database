﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataUserGuide
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataUserGuide")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class ConfigDataUserGuide : IExtensible
  {
    private int _ID;
    private string _Name;
    private List<UserGuideTrigger> _OpenTrigger;
    private List<string> _OTParam;
    private UserGuideCondition _OpenCondition;
    private string _OCParam;
    private int _FirstStepID;
    private UserGuideTrigger _CompleteTrigger;
    private string _CTParam;
    private List<int> _EnforceHeros;
    private UserGuideCondition _OpenCondition2;
    private string _OCParam2;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataUserGuide()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, Name = "OpenTrigger")]
    public List<UserGuideTrigger> OpenTrigger
    {
      get
      {
        return this._OpenTrigger;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, Name = "OTParam")]
    public List<string> OTParam
    {
      get
      {
        return this._OTParam;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "OpenCondition")]
    public UserGuideCondition OpenCondition
    {
      get
      {
        return this._OpenCondition;
      }
      set
      {
        this._OpenCondition = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, IsRequired = true, Name = "OCParam")]
    public string OCParam
    {
      get
      {
        return this._OCParam;
      }
      set
      {
        this._OCParam = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "FirstStepID")]
    public int FirstStepID
    {
      get
      {
        return this._FirstStepID;
      }
      set
      {
        this._FirstStepID = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CompleteTrigger")]
    public UserGuideTrigger CompleteTrigger
    {
      get
      {
        return this._CompleteTrigger;
      }
      set
      {
        this._CompleteTrigger = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.Default, IsRequired = true, Name = "CTParam")]
    public string CTParam
    {
      get
      {
        return this._CTParam;
      }
      set
      {
        this._CTParam = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, Name = "EnforceHeros")]
    public List<int> EnforceHeros
    {
      get
      {
        return this._EnforceHeros;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "OpenCondition2")]
    public UserGuideCondition OpenCondition2
    {
      get
      {
        return this._OpenCondition2;
      }
      set
      {
        this._OpenCondition2 = value;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.Default, IsRequired = true, Name = "OCParam2")]
    public string OCParam2
    {
      get
      {
        return this._OCParam2;
      }
      set
      {
        this._OCParam2 = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
