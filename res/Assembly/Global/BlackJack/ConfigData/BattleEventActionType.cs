﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.BattleEventActionType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "BattleEventActionType")]
  public enum BattleEventActionType
  {
    [ProtoEnum(Name = "BattleEventActionType_None", Value = 0)] BattleEventActionType_None,
    [ProtoEnum(Name = "BattleEventActionType_Relief", Value = 1)] BattleEventActionType_Relief,
    [ProtoEnum(Name = "BattleEventActionType_Retreat", Value = 2)] BattleEventActionType_Retreat,
    [ProtoEnum(Name = "BattleEventActionType_Dialog", Value = 3)] BattleEventActionType_Dialog,
    [ProtoEnum(Name = "BattleEventActionType_ChangeTeam", Value = 4)] BattleEventActionType_ChangeTeam,
    [ProtoEnum(Name = "BattleEventActionType_AttachBuff", Value = 5)] BattleEventActionType_AttachBuff,
    [ProtoEnum(Name = "BattleEventActionType_Music", Value = 6)] BattleEventActionType_Music,
    [ProtoEnum(Name = "BattleEventActionType_ChangeTerrain", Value = 7)] BattleEventActionType_ChangeTerrain,
    [ProtoEnum(Name = "BattleEventActionType_Perform", Value = 8)] BattleEventActionType_Perform,
    [ProtoEnum(Name = "BattleEventActionType_ChangeBehavior", Value = 9)] BattleEventActionType_ChangeBehavior,
    [ProtoEnum(Name = "BattleEventActionType_RetreatPosition", Value = 10)] BattleEventActionType_RetreatPosition,
    [ProtoEnum(Name = "BattleEventActionType_Replace", Value = 11)] BattleEventActionType_Replace,
    [ProtoEnum(Name = "BattleEventActionType_RemoveBuff", Value = 12)] BattleEventActionType_RemoveBuff,
    [ProtoEnum(Name = "BattleEventActionType_AttachBuffPosition", Value = 13)] BattleEventActionType_AttachBuffPosition,
  }
}
