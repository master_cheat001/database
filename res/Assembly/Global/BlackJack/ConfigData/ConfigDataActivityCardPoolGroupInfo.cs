﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataActivityCardPoolGroupInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ConfigData
{
  [HotFix(true, m_isCtorOnly = true)]
  [CustomLuaClass]
  [ProtoContract(Name = "ConfigDataActivityCardPoolGroupInfo")]
  [Serializable]
  public class ConfigDataActivityCardPoolGroupInfo : IExtensible, ICardPoolItemWeight
  {
    private int _ID;
    private string _Description;
    private int _GroupID;
    private List<Goods> _CardItems;
    private int _DefaultWeight;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataActivityCardPoolGroupInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Description")]
    public string Description
    {
      get
      {
        return this._Description;
      }
      set
      {
        this._Description = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "GroupID")]
    public int GroupID
    {
      get
      {
        return this._GroupID;
      }
      set
      {
        this._GroupID = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, Name = "CardItems")]
    public List<Goods> CardItems
    {
      get
      {
        return this._CardItems;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DefaultWeight")]
    public int DefaultWeight
    {
      get
      {
        return this._DefaultWeight;
      }
      set
      {
        this._DefaultWeight = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    public int CardPoolItemID
    {
      get
      {
        return this.ID;
      }
    }

    public List<Goods> Items
    {
      get
      {
        return this.CardItems;
      }
    }

    public int ItemDefaultWeight
    {
      get
      {
        return this.DefaultWeight;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
