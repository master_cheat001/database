﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataBattleEventActionInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataBattleEventActionInfo")]
  [CustomLuaClass]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class ConfigDataBattleEventActionInfo : IExtensible
  {
    private int _ID;
    private BattleEventActionType _ActionType;
    private List<int> _Param1;
    private List<int> _Param2;
    private List<ParamPosition> _Param3;
    private List<int> _Param4;
    private string _Param5;
    private List<int> _Param6;
    private List<int> _ReliefActorsBehavior;
    private List<int> _ReliefGroups;
    private List<int> _ReliefGroupBehavior;
    private List<int> _ReliefRandomArmies_ID;
    private IExtension extensionObject;
    public Dictionary<int, int> m_reliefGroupBehaviors;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBattleEventActionInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ActionType")]
    public BattleEventActionType ActionType
    {
      get
      {
        return this._ActionType;
      }
      set
      {
        this._ActionType = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, Name = "Param1")]
    public List<int> Param1
    {
      get
      {
        return this._Param1;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, Name = "Param2")]
    public List<int> Param2
    {
      get
      {
        return this._Param2;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.Default, Name = "Param3")]
    public List<ParamPosition> Param3
    {
      get
      {
        return this._Param3;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, Name = "Param4")]
    public List<int> Param4
    {
      get
      {
        return this._Param4;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.Default, IsRequired = true, Name = "Param5")]
    public string Param5
    {
      get
      {
        return this._Param5;
      }
      set
      {
        this._Param5 = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, Name = "Param6")]
    public List<int> Param6
    {
      get
      {
        return this._Param6;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, Name = "ReliefActorsBehavior")]
    public List<int> ReliefActorsBehavior
    {
      get
      {
        return this._ReliefActorsBehavior;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.TwosComplement, Name = "ReliefGroups")]
    public List<int> ReliefGroups
    {
      get
      {
        return this._ReliefGroups;
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.TwosComplement, Name = "ReliefGroupBehavior")]
    public List<int> ReliefGroupBehavior
    {
      get
      {
        return this._ReliefGroupBehavior;
      }
    }

    [ProtoMember(15, DataFormat = DataFormat.TwosComplement, Name = "ReliefRandomArmies_ID")]
    public List<int> ReliefRandomArmies_ID
    {
      get
      {
        return this._ReliefRandomArmies_ID;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Initialize()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Dictionary<int, int> ComputeGroupBehaviorDict(
      List<int> groups,
      List<int> behaviors)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int Param1FirstValue()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int Param2FirstValue()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
