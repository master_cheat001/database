﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataRiftChapterInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ConfigData
{
  [CustomLuaClass]
  [ProtoContract(Name = "ConfigDataRiftChapterInfo")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class ConfigDataRiftChapterInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _TitleName;
    private string _Desc;
    private string _Place;
    private List<RiftChapterInfoUnlockConditions> _UnlockConditions;
    private int _Hard;
    private int _HardChapter;
    private List<int> _RiftLevels_ID;
    private int _Reward1Star;
    private List<Goods> _Reward1;
    private int _Reward2Star;
    private List<Goods> _Reward2;
    private int _Reward3Star;
    private List<Goods> _Reward3;
    private string _Image;
    private string _ChapterBGPrefabName;
    private bool _IsOpened;
    private IExtension extensionObject;
    public ConfigDataRiftChapterInfo m_hardChapterInfo;
    public ConfigDataRiftLevelInfo[] m_levelInfos;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRiftChapterInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "TitleName")]
    public string TitleName
    {
      get
      {
        return this._TitleName;
      }
      set
      {
        this._TitleName = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = true, Name = "Desc")]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.Default, IsRequired = true, Name = "Place")]
    public string Place
    {
      get
      {
        return this._Place;
      }
      set
      {
        this._Place = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.Default, Name = "UnlockConditions")]
    public List<RiftChapterInfoUnlockConditions> UnlockConditions
    {
      get
      {
        return this._UnlockConditions;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Hard")]
    public int Hard
    {
      get
      {
        return this._Hard;
      }
      set
      {
        this._Hard = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HardChapter")]
    public int HardChapter
    {
      get
      {
        return this._HardChapter;
      }
      set
      {
        this._HardChapter = value;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.TwosComplement, Name = "RiftLevels_ID")]
    public List<int> RiftLevels_ID
    {
      get
      {
        return this._RiftLevels_ID;
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Reward1Star")]
    public int Reward1Star
    {
      get
      {
        return this._Reward1Star;
      }
      set
      {
        this._Reward1Star = value;
      }
    }

    [ProtoMember(15, DataFormat = DataFormat.Default, Name = "Reward1")]
    public List<Goods> Reward1
    {
      get
      {
        return this._Reward1;
      }
    }

    [ProtoMember(16, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Reward2Star")]
    public int Reward2Star
    {
      get
      {
        return this._Reward2Star;
      }
      set
      {
        this._Reward2Star = value;
      }
    }

    [ProtoMember(17, DataFormat = DataFormat.Default, Name = "Reward2")]
    public List<Goods> Reward2
    {
      get
      {
        return this._Reward2;
      }
    }

    [ProtoMember(18, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Reward3Star")]
    public int Reward3Star
    {
      get
      {
        return this._Reward3Star;
      }
      set
      {
        this._Reward3Star = value;
      }
    }

    [ProtoMember(19, DataFormat = DataFormat.Default, Name = "Reward3")]
    public List<Goods> Reward3
    {
      get
      {
        return this._Reward3;
      }
    }

    [ProtoMember(20, DataFormat = DataFormat.Default, IsRequired = true, Name = "Image")]
    public string Image
    {
      get
      {
        return this._Image;
      }
      set
      {
        this._Image = value;
      }
    }

    [ProtoMember(21, DataFormat = DataFormat.Default, IsRequired = true, Name = "ChapterBGPrefabName")]
    public string ChapterBGPrefabName
    {
      get
      {
        return this._ChapterBGPrefabName;
      }
      set
      {
        this._ChapterBGPrefabName = value;
      }
    }

    [ProtoMember(22, DataFormat = DataFormat.Default, IsRequired = true, Name = "IsOpened")]
    public bool IsOpened
    {
      get
      {
        return this._IsOpened;
      }
      set
      {
        this._IsOpened = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
