﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.OperationalActivityType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "OperationalActivityType")]
  public enum OperationalActivityType
  {
    [ProtoEnum(Name = "OperationalActivityType_PlayerLevelUp", Value = 1)] OperationalActivityType_PlayerLevelUp = 1,
    [ProtoEnum(Name = "OperationalActivityType_LimitedTimeExchange", Value = 2)] OperationalActivityType_LimitedTimeExchange = 2,
    [ProtoEnum(Name = "OperationalActivityType_SpecificDaysLogin", Value = 3)] OperationalActivityType_SpecificDaysLogin = 3,
    [ProtoEnum(Name = "OperationalActivityType_AccumulateDaysLogin", Value = 4)] OperationalActivityType_AccumulateDaysLogin = 4,
    [ProtoEnum(Name = "OperationalActivityType_RewardAdd", Value = 5)] OperationalActivityType_RewardAdd = 5,
    [ProtoEnum(Name = "OperationalActivityType_ChallengeNumsAdd", Value = 6)] OperationalActivityType_ChallengeNumsAdd = 6,
    [ProtoEnum(Name = "OperationalActivityType_ActivitySelectCard", Value = 7)] OperationalActivityType_ActivitySelectCard = 7,
    [ProtoEnum(Name = "OperationalActivityType_Raffle", Value = 8)] OperationalActivityType_Raffle = 8,
    [ProtoEnum(Name = "OperationalActivityType_AccumulateRecharge", Value = 9)] OperationalActivityType_AccumulateRecharge = 9,
    [ProtoEnum(Name = "OperationalActivityType_AccumulateConsumeCrystal", Value = 10)] OperationalActivityType_AccumulateConsumeCrystal = 10, // 0x0000000A
    [ProtoEnum(Name = "OperationalActivityType_UnchartedScore", Value = 11)] OperationalActivityType_UnchartedScore = 11, // 0x0000000B
    [ProtoEnum(Name = "OperationalActivityType_Redeem", Value = 12)] OperationalActivityType_Redeem = 12, // 0x0000000C
    [ProtoEnum(Name = "OperationalActivityType_Web", Value = 13)] OperationalActivityType_Web = 13, // 0x0000000D
    [ProtoEnum(Name = "OperationalActivityType_BuyItemGuide", Value = 14)] OperationalActivityType_BuyItemGuide = 14, // 0x0000000E
    [ProtoEnum(Name = "OperationalActivityType_PBTCBTFansRewards", Value = 15)] OperationalActivityType_PBTCBTFansRewards = 15, // 0x0000000F
    [ProtoEnum(Name = "OperationalActivityType_DailyNumsAdd", Value = 16)] OperationalActivityType_DailyNumsAdd = 16, // 0x00000010
    [ProtoEnum(Name = "OperationalActivityType_CollectionActivity", Value = 17)] OperationalActivityType_CollectionActivity = 17, // 0x00000011
    [ProtoEnum(Name = "OperationalActivityType_DailyExtraRewardAdd", Value = 18)] OperationalActivityType_DailyExtraRewardAdd = 18, // 0x00000012
  }
}
