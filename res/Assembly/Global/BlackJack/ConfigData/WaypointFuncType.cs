﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.WaypointFuncType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "WaypointFuncType")]
  public enum WaypointFuncType
  {
    [ProtoEnum(Name = "WaypointFuncType_None", Value = 0)] WaypointFuncType_None,
    [ProtoEnum(Name = "WaypointFuncType_Scenario", Value = 1)] WaypointFuncType_Scenario,
    [ProtoEnum(Name = "WaypointFuncType_Portal", Value = 2)] WaypointFuncType_Portal,
    [ProtoEnum(Name = "WaypointFuncType_Event", Value = 3)] WaypointFuncType_Event,
  }
}
