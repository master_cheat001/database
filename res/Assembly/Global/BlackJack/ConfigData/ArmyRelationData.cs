﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ArmyRelationData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

namespace BlackJack.ConfigData
{
  public struct ArmyRelationData
  {
    public int Attack;
    public int Defend;
    public int Magic;
    public int MagicDefend;
  }
}
