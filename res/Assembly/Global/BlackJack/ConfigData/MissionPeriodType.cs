﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.MissionPeriodType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "MissionPeriodType")]
  public enum MissionPeriodType
  {
    [ProtoEnum(Name = "MissionPeriodType_Everyday", Value = 1)] MissionPeriodType_Everyday = 1,
    [ProtoEnum(Name = "MissionPeriodType_OneOff", Value = 2)] MissionPeriodType_OneOff = 2,
  }
}
