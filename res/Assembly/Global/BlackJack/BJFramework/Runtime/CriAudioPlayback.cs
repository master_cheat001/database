﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.CriAudioPlayback
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using System.Runtime.CompilerServices;

namespace BlackJack.BJFramework.Runtime
{
  internal class CriAudioPlayback : IAudioPlayback
  {
    private CriAtomExPlayback m_playback;
    private string m_cueName;

    [MethodImpl((MethodImplOptions) 32768)]
    public CriAudioPlayback(CriAtomExPlayback playback, string cueName)
    {
      // ISSUE: unable to decompile the method.
    }

    public void Stop()
    {
      this.m_playback.Stop();
    }

    public string CueName
    {
      get
      {
        return this.m_cueName;
      }
    }

    public float Seconds
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsStoped()
    {
      return this.m_playback.status == CriAtomExPlayback.Status.Removed;
    }
  }
}
