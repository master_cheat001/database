﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.TaskNs.NetWorkTransactionTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.UI;
using SLua;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.BJFramework.Runtime.TaskNs
{
  [CustomLuaClassWithProtected]
  public class NetWorkTransactionTask : Task
  {
    public static int m_delayTimeForUIWaiting = 1;
    protected UITaskBase m_blockedUITask;
    protected bool m_autoRetry;
    protected float m_timeout;
    protected bool m_isTimeOuted;
    protected DateTime? m_timeoutTime;
    protected DateTime m_showWaitingUITime;
    protected bool m_isUIWaitingShown;
    protected bool m_isReturnToLoginByDirtyData;
    protected bool m_isLaunchedReloginUITask;
    public const float TimeOutForEditor = 60f;
    [DoNotToLua]
    private NetWorkTransactionTask.LuaExportHelper luaExportHelper;

    [MethodImpl((MethodImplOptions) 32768)]
    public NetWorkTransactionTask(float timeout = 10f, UITaskBase blockedUITask = null, bool autoRetry = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(object param)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnPause()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnResume(object param = null)
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual bool StartNetWorking()
    {
      return true;
    }

    protected virtual void OnTransactionComplete()
    {
      this.Stop();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void RegisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void UnregisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnTick()
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual void OnTimeOut()
    {
      this.OnNetworkError();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnGameServerDataUnsyncNotify()
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual void OnGameServerNetworkError(int err)
    {
      this.OnNetworkError();
    }

    protected virtual void OnGameServerDisconnected()
    {
      this.OnNetworkError();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnNetworkError()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnReLoginSuccess()
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual void OnUIManagerReturnToLoginUI(bool obj)
    {
      this.Stop();
    }

    public static event Action<bool> EventShowUIWaiting
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public static event Action<bool> EventReturnToLoginUI
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public static event Action<Action> EventReLoginBySession
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public NetWorkTransactionTask.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private bool __callBase_Start(object param)
    {
      return this.Start(param);
    }

    private void __callBase_Stop()
    {
      this.Stop();
    }

    private void __callBase_Pause()
    {
      this.Pause();
    }

    private bool __callBase_Resume(object param)
    {
      return this.Resume(param);
    }

    private void __callBase_ClearOnStopEvent()
    {
      this.ClearOnStopEvent();
    }

    private void __callBase_ExecAfterTicks(Action action, ulong delayTickCount)
    {
      this.ExecAfterTicks(action, delayTickCount);
    }

    private bool __callBase_OnStart(object param)
    {
      return base.OnStart(param);
    }

    private void __callBase_OnPause()
    {
      base.OnPause();
    }

    private bool __callBase_OnResume(object param)
    {
      return base.OnResume(param);
    }

    private void __callBase_OnStop()
    {
      base.OnStop();
    }

    private void __callBase_OnTick()
    {
      base.OnTick();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void __callDele_EventShowUIWaiting(bool obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private static void __clearDele_EventShowUIWaiting(bool obj)
    {
      NetWorkTransactionTask.EventShowUIWaiting = (Action<bool>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void __callDele_EventReturnToLoginUI(bool obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private static void __clearDele_EventReturnToLoginUI(bool obj)
    {
      NetWorkTransactionTask.EventReturnToLoginUI = (Action<bool>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void __callDele_EventReLoginBySession(Action obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private static void __clearDele_EventReLoginBySession(Action obj)
    {
      NetWorkTransactionTask.EventReLoginBySession = (Action<Action>) null;
    }

    public class LuaExportHelper
    {
      private NetWorkTransactionTask m_owner;

      public LuaExportHelper(NetWorkTransactionTask owner)
      {
        this.m_owner = owner;
      }

      public bool __callBase_Start(object param)
      {
        return this.m_owner.__callBase_Start(param);
      }

      public void __callBase_Stop()
      {
        this.m_owner.__callBase_Stop();
      }

      public void __callBase_Pause()
      {
        this.m_owner.__callBase_Pause();
      }

      public bool __callBase_Resume(object param)
      {
        return this.m_owner.__callBase_Resume(param);
      }

      public void __callBase_ClearOnStopEvent()
      {
        this.m_owner.__callBase_ClearOnStopEvent();
      }

      public void __callBase_ExecAfterTicks(Action action, ulong delayTickCount)
      {
        this.m_owner.__callBase_ExecAfterTicks(action, delayTickCount);
      }

      public bool __callBase_OnStart(object param)
      {
        return this.m_owner.__callBase_OnStart(param);
      }

      public void __callBase_OnPause()
      {
        this.m_owner.__callBase_OnPause();
      }

      public bool __callBase_OnResume(object param)
      {
        return this.m_owner.__callBase_OnResume(param);
      }

      public void __callBase_OnStop()
      {
        this.m_owner.__callBase_OnStop();
      }

      public void __callBase_OnTick()
      {
        this.m_owner.__callBase_OnTick();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public static void __callDele_EventShowUIWaiting(bool obj)
      {
        NetWorkTransactionTask.__callDele_EventShowUIWaiting(obj);
      }

      public static void __clearDele_EventShowUIWaiting(bool obj)
      {
        NetWorkTransactionTask.__clearDele_EventShowUIWaiting(obj);
      }

      public static void __callDele_EventReturnToLoginUI(bool obj)
      {
        NetWorkTransactionTask.__callDele_EventReturnToLoginUI(obj);
      }

      public static void __clearDele_EventReturnToLoginUI(bool obj)
      {
        NetWorkTransactionTask.__clearDele_EventReturnToLoginUI(obj);
      }

      public static void __callDele_EventReLoginBySession(Action obj)
      {
        NetWorkTransactionTask.__callDele_EventReLoginBySession(obj);
      }

      public static void __clearDele_EventReLoginBySession(Action obj)
      {
        NetWorkTransactionTask.__clearDele_EventReLoginBySession(obj);
      }

      public UITaskBase m_blockedUITask
      {
        get
        {
          return this.m_owner.m_blockedUITask;
        }
        set
        {
          this.m_owner.m_blockedUITask = value;
        }
      }

      public bool m_autoRetry
      {
        get
        {
          return this.m_owner.m_autoRetry;
        }
        set
        {
          this.m_owner.m_autoRetry = value;
        }
      }

      public float m_timeout
      {
        get
        {
          return this.m_owner.m_timeout;
        }
        set
        {
          this.m_owner.m_timeout = value;
        }
      }

      public bool m_isTimeOuted
      {
        get
        {
          return this.m_owner.m_isTimeOuted;
        }
        set
        {
          this.m_owner.m_isTimeOuted = value;
        }
      }

      public DateTime? m_timeoutTime
      {
        get
        {
          return this.m_owner.m_timeoutTime;
        }
        set
        {
          this.m_owner.m_timeoutTime = value;
        }
      }

      public DateTime m_showWaitingUITime
      {
        get
        {
          return this.m_owner.m_showWaitingUITime;
        }
        set
        {
          this.m_owner.m_showWaitingUITime = value;
        }
      }

      public bool m_isUIWaitingShown
      {
        get
        {
          return this.m_owner.m_isUIWaitingShown;
        }
        set
        {
          this.m_owner.m_isUIWaitingShown = value;
        }
      }

      public bool m_isReturnToLoginByDirtyData
      {
        get
        {
          return this.m_owner.m_isReturnToLoginByDirtyData;
        }
        set
        {
          this.m_owner.m_isReturnToLoginByDirtyData = value;
        }
      }

      public bool m_isLaunchedReloginUITask
      {
        get
        {
          return this.m_owner.m_isLaunchedReloginUITask;
        }
        set
        {
          this.m_owner.m_isLaunchedReloginUITask = value;
        }
      }

      public bool OnStart(object param)
      {
        return this.m_owner.OnStart(param);
      }

      public void OnStop()
      {
        this.m_owner.OnStop();
      }

      public void OnPause()
      {
        this.m_owner.OnPause();
      }

      public bool OnResume(object param)
      {
        return this.m_owner.OnResume(param);
      }

      public bool StartNetWorking()
      {
        return this.m_owner.StartNetWorking();
      }

      public void OnTransactionComplete()
      {
        this.m_owner.OnTransactionComplete();
      }

      public void RegisterNetworkEvent()
      {
        this.m_owner.RegisterNetworkEvent();
      }

      public void UnregisterNetworkEvent()
      {
        this.m_owner.UnregisterNetworkEvent();
      }

      public void OnTick()
      {
        this.m_owner.OnTick();
      }

      public void OnTimeOut()
      {
        this.m_owner.OnTimeOut();
      }

      public void OnGameServerDataUnsyncNotify()
      {
        this.m_owner.OnGameServerDataUnsyncNotify();
      }

      public void OnGameServerNetworkError(int err)
      {
        this.m_owner.OnGameServerNetworkError(err);
      }

      public void OnGameServerDisconnected()
      {
        this.m_owner.OnGameServerDisconnected();
      }

      public void OnNetworkError()
      {
        this.m_owner.OnNetworkError();
      }

      public void OnReLoginSuccess()
      {
        this.m_owner.OnReLoginSuccess();
      }

      public void OnUIManagerReturnToLoginUI(bool obj)
      {
        this.m_owner.OnUIManagerReturnToLoginUI(obj);
      }
    }
  }
}
