﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.Util
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Profiling;

namespace BlackJack.BJFramework.Runtime
{
  [CustomLuaClass]
  public class Util
  {
    private static Dictionary<string, string> m_dnNameDict = new Dictionary<string, string>();
    private static string m_platform;
    private static string m_platformSuffix;
    private static string m_persistentPath;
    private static string m_streamingAssetsPath;

    public static long GetRuntimeMemorySize()
    {
      return Profiler.GetTotalAllocatedMemoryLong();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void LogMemorySize(string prefix)
    {
      Debug.Log(string.Format("{0} AllocateMemory: {1:N0}, GC Heap:{2:N0}", (object) prefix, (object) Util.GetRuntimeMemorySize(), (object) GC.GetTotalMemory(true)));
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public static IEnumerator DownloadHttpFile(
      string url,
      Action<bool, WWW> onReceive,
      Action<WWW> onUpdate = null)
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new Util.\u003CDownloadHttpFile\u003Ec__Iterator0()
      {
        url = url,
        onUpdate = onUpdate,
        onReceive = onReceive
      };
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetCurrentTargetPlatform()
    {
      if (string.IsNullOrEmpty(Util.m_platform))
      {
        if (!Application.isEditor)
        {
          switch (Application.platform)
          {
            case RuntimePlatform.OSXPlayer:
              Util.m_platform = "StandaloneOSXIntel";
              break;
            case RuntimePlatform.WindowsPlayer:
              Util.m_platform = IntPtr.Size != 8 ? "StandaloneWindows" : "StandaloneWindows64";
              break;
            case RuntimePlatform.IPhonePlayer:
              Util.m_platform = "iOS";
              break;
            case RuntimePlatform.Android:
              Util.m_platform = "Android";
              break;
          }
        }
        else
          Util.m_platform = Util.GetCurrentTargetPlatformInEditor == null ? "Android" : Util.GetCurrentTargetPlatformInEditor();
        Util.m_platform += Util.m_platformSuffix;
      }
      return Util.m_platform;
    }

    public static string PlatformSuffix
    {
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public static Func<string> GetCurrentTargetPlatformInEditor { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetFilePersistentPath(string assetPath)
    {
      if (Util.m_persistentPath == null)
        Util.m_persistentPath = Application.persistentDataPath + "/";
      return Util.m_persistentPath + assetPath;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetFileStreamingAssetsPath(string assetPath)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetFileStreamingAssetsPath4WWW(string assetPath)
    {
      if (Util.m_streamingAssetsPath == null)
        Util.m_streamingAssetsPath = Application.streamingAssetsPath + "/";
      if (!Util.m_streamingAssetsPath.Contains("://"))
        return string.Format("file://{0}{1}", (object) Util.m_streamingAssetsPath, (object) assetPath);
      return string.Format("{0}{1}", (object) Util.m_streamingAssetsPath, (object) assetPath);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string ReformPathString(string path)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetAssetDNNameFromPath(
      string path,
      string replaceLastFolderNameStr = "",
      bool autoExtension = true)
    {
      if (Util.m_dnNameDict.ContainsKey(path))
        return Util.m_dnNameDict[path];
      if (!string.IsNullOrEmpty(replaceLastFolderNameStr))
        path = Util.ReplaceLastFolderName(path, replaceLastFolderNameStr);
      string str = path.Replace('/', '_');
      if (str.Contains("\\\\"))
        str = str.Replace("\\\\", "_");
      if (str.Contains("\\"))
        str = str.Replace("\\", "_");
      if (str.Contains("."))
        str = str.Replace(".", "_");
      if (autoExtension)
        str += ".b";
      Util.m_dnNameDict[path] = str;
      return str;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static string ReplaceLastFolderName(string path, string replaceStr)
    {
      // ISSUE: unable to decompile the method.
    }

    public static string GetBundleNameByAssetPath(
      string path,
      string replaceLastFolderNameStr = "",
      bool autoExtension = true)
    {
      return Util.GetAssetDNNameFromPath(path, replaceLastFolderNameStr, autoExtension).ToLower();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static long GetFileSize(string path)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void RemoveCloneFromGameObjectName(GameObject go)
    {
      if (!go.name.EndsWith("(Clone)"))
        return;
      go.name = go.name.Replace("(Clone)", string.Empty);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void SetTransformParent(Transform child, Transform parent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<long> GetAllFilesSize(List<string> fileList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<string> CreateMd5FromFileList(List<string> fileList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string CreateMd5FromFile(string filePath)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string CreateMd5FromBytes(byte[] bytes)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string TimeSpanToString(TimeSpan timeSpan)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetTypeDefaultValueString(System.Type type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static System.Type FindType(string typeName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetTypeDefinitionString(System.Type type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetGenericTypeFriendlyName(System.Type type)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
