﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.SceneSettings
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using System;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.BJFramework.Runtime
{
  [Serializable]
  public class SceneSettings
  {
    public int DesignScreenWidth = 1920;
    public int DesignScreenHeight = 1080;
    public Vector3 SceneLayerOffset = new Vector3(1000f, 0.0f, 0.0f);
    public bool UseOrthographicForUILayer;
    public int TrigerWidth2ShrinkScale;
    public int TrigerHeight2ShrinkScale;

    [MethodImpl((MethodImplOptions) 32768)]
    public SceneSettings()
    {
    }
  }
}
