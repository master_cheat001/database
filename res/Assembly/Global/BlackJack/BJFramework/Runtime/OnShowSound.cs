﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.OnShowSound
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.BJFramework.Runtime
{
  [CustomLuaClass]
  public class OnShowSound : MonoBehaviour
  {
    public string m_soundName = string.Empty;
    public ClickedTester m_clickedTester;
    private bool m_isStarted;

    [MethodImpl((MethodImplOptions) 32768)]
    public OnShowSound()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEnable()
    {
      if (!this.m_isStarted || string.IsNullOrEmpty(this.m_soundName) || (Object) this.m_clickedTester != (Object) null && !this.m_clickedTester.Clicked || (GameManager.Instance == null || GameManager.Instance.AudioManager == null))
        return;
      GameManager.Instance.AudioManager.PlaySound(this.m_soundName, 1f);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Start()
    {
      this.m_isStarted = true;
      if ((Object) this.m_clickedTester != (Object) null && !this.m_clickedTester.Clicked || (GameManager.Instance == null || GameManager.Instance.AudioManager == null))
        return;
      GameManager.Instance.AudioManager.PlaySound(this.m_soundName, 1f);
    }
  }
}
