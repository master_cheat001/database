﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.Log.FileLogger
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using System;
using System.IO;
using System.Runtime.CompilerServices;
using System.Threading;
using UnityEngine;

namespace BlackJack.BJFramework.Runtime.Log
{
  public class FileLogger
  {
    private string _logFileRoot;
    private string _logName;
    private string _logFileFullPath;
    private StreamWriter _logStreamWriter;

    [MethodImpl((MethodImplOptions) 32768)]
    public FileLogger(string logFileRoot, string logName)
    {
      if (!Directory.Exists(logFileRoot))
      {
        try
        {
          Directory.CreateDirectory(logFileRoot);
        }
        catch (Exception ex)
        {
          Debug.Log((object) ("Create log directory fail " + ex.ToString()));
          return;
        }
      }
      this._logFileRoot = logFileRoot;
      this._logName = logName;
      this._logFileFullPath = this.GetNewFileFullPath();
      this._logStreamWriter = new StreamWriter(this._logFileFullPath);
      Debug.Log((object) ("Create Log File " + this._logFileFullPath));
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string GetNewFileFullPath()
    {
      return this._logFileRoot + this._logName + DateTime.Now.ToString("yyyy_MMdd_HHmm_ss") + ".txt";
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void WriteLog(string msg, string level)
    {
      if (this._logStreamWriter == null)
        return;
      lock ((object) this._logStreamWriter)
      {
        try
        {
          string str = string.Format("[{0}][{1}] {2}", (object) level, (object) DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:fff"), (object) msg);
          this._logStreamWriter.WriteLine(str);
          this._logStreamWriter.Flush();
          if (this.EventOnLog == null)
            return;
          this.EventOnLog(str);
        }
        catch (Exception ex)
        {
        }
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Close()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<string> EventOnLog
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        Action<string> comparand = this.EventOnLog;
        Action<string> action;
        do
        {
          action = comparand;
          comparand = Interlocked.CompareExchange<Action<string>>(ref this.EventOnLog, action + value, comparand);
        }
        while (comparand != action);
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
      }
    }
  }
}
