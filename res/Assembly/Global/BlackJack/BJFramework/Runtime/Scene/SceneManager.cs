﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.Scene.SceneManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Resource;
using BlackJack.Utils;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;

namespace BlackJack.BJFramework.Runtime.Scene
{
  [CustomLuaClass]
  public class SceneManager : ITickable
  {
    private static string m_sceneRootAssetPath = "SceneRoot";
    private static string m_3DLayerRootAssetPath = "3DLayerRoot";
    private static string m_uiLayerRootAssetPath = "UILayerRoot";
    private static int m_cameraDepthMax = 80;
    private TinyCorutineHelper m_corutineHelper = new TinyCorutineHelper();
    private Dictionary<string, SceneLayerBase> m_layerDict = new Dictionary<string, SceneLayerBase>();
    private List<SceneLayerBase> m_unusedLayerList = new List<SceneLayerBase>();
    private List<SceneLayerBase> m_loadingLayerList = new List<SceneLayerBase>();
    private List<SceneLayerBase> m_layerStack = new List<SceneLayerBase>();
    private static SceneManager m_instance;
    private bool m_useOrthographicForUILayer;
    private int m_designScreenWidth;
    private int m_designScreenHeight;
    private int m_trigerWidth2ShrinkScale;
    private int m_trigerHeight2ShrinkScale;
    private Vector3 m_layerOffset;
    private GameObject m_sceneRootGo;
    private GameObject m_unusedLayerRootGo;
    private GameObject m_loadingLayerRootGo;
    private GameObject m_3DSceneRootGo;
    private GameObject m_uiSceneGroup1RootCanvasGo;
    private GameObject m_uiSceneGroup2RootCanvasGo;
    public Camera m_uiSceneGroup1Camera;
    public Camera m_uiSceneGroup2Camera;
    private Canvas m_uiSceneGroup1RootCanvas;
    private Canvas m_uiSceneGroup2RootCanvas;
    private CanvasScaler m_uiSceneGroup1RootCanvasScaler;
    private CanvasScaler m_uiSceneGroup2RootCanvasScaler;
    private GameObject m_3DLayerRootPrefab;
    private GameObject m_uiLayerRootPrefab;
    private bool m_stackDirty;
    private bool m_is3DLayerBlurActive;
    private LayerRenderSettingDesc m_currRenderSetting;
    private LayerRenderSettingDesc m_defaultRenderSetting;
    private bool m_enableLayerReserve;

    [MethodImpl((MethodImplOptions) 32768)]
    private SceneManager()
    {
    }

    [DoNotToLua]
    [MethodImpl((MethodImplOptions) 32768)]
    public static SceneManager CreateSceneManager()
    {
      if (SceneManager.m_instance == null)
        SceneManager.m_instance = new SceneManager();
      return SceneManager.m_instance;
    }

    [DoNotToLua]
    [MethodImpl((MethodImplOptions) 32768)]
    public bool Initlize(
      int designScreenWidth,
      int designScreenHeight,
      int trigerWidth2ShrinkScale,
      int trigerHeight2ShrinkScale,
      Vector3 layerOffset,
      bool enableLayerReserve = false,
      bool useOrthographicForUILayer = false)
    {
      Debug.Log("SceneManager.Initlize start");
      this.m_designScreenWidth = designScreenWidth;
      this.m_designScreenHeight = designScreenHeight;
      this.m_trigerWidth2ShrinkScale = trigerWidth2ShrinkScale;
      this.m_trigerHeight2ShrinkScale = trigerHeight2ShrinkScale;
      this.m_layerOffset = layerOffset;
      this.m_useOrthographicForUILayer = useOrthographicForUILayer;
      this.m_enableLayerReserve = enableLayerReserve;
      if (!this.CreateSceneRoot())
      {
        Debug.LogError("SceneManager.Initlize CreateSceneRoot fail");
        return false;
      }
      this.m_uiLayerRootPrefab = UnityEngine.Resources.Load<GameObject>(SceneManager.m_uiLayerRootAssetPath);
      this.m_3DLayerRootPrefab = UnityEngine.Resources.Load<GameObject>(SceneManager.m_3DLayerRootAssetPath);
      if ((UnityEngine.Object) this.m_uiLayerRootPrefab == (UnityEngine.Object) null || (UnityEngine.Object) this.m_3DLayerRootPrefab == (UnityEngine.Object) null)
      {
        Debug.LogError("SceneManager.Initlize m_uiLayerRootPrefab == null || m_3DLayerRootPrefab == null");
        return false;
      }
      LayerRenderSettingDesc defaultRenderSetting = this.GetDefaultRenderSetting();
      if ((UnityEngine.Object) defaultRenderSetting == (UnityEngine.Object) null)
      {
        Debug.LogError("SceneManager.Initlize GetDefaultRenderSetting fail");
        return false;
      }
      this.ApplyRenderSetting(defaultRenderSetting);
      return true;
    }

    [DoNotToLua]
    [MethodImpl((MethodImplOptions) 32768)]
    public void Uninitlize()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CreateSceneRoot()
    {
      Debug.Log("SceneManager.CreateSceneRoot start");
      GameObject original = UnityEngine.Resources.Load<GameObject>(SceneManager.m_sceneRootAssetPath);
      if ((UnityEngine.Object) original == (UnityEngine.Object) null)
      {
        Debug.LogError("SceneManager.CreateSceneRoot fail sceneRootPrefab = null");
        return false;
      }
      this.m_sceneRootGo = UnityEngine.Object.Instantiate<GameObject>(original);
      if ((UnityEngine.Object) this.m_sceneRootGo == (UnityEngine.Object) null)
      {
        Debug.LogError("SceneManager.CreateSceneRoot fail sceneRootGo = null");
        return false;
      }
      Util.RemoveCloneFromGameObjectName(this.m_sceneRootGo);
      this.m_sceneRootGo.transform.localPosition = Vector3.zero;
      this.m_sceneRootGo.transform.localRotation = Quaternion.identity;
      this.m_sceneRootGo.transform.localScale = Vector3.one;
      this.m_3DSceneRootGo = this.m_sceneRootGo.transform.Find("3DSceneRoot").gameObject;
      this.m_unusedLayerRootGo = this.m_sceneRootGo.transform.Find("UnusedLayerRoot").gameObject;
      this.m_loadingLayerRootGo = this.m_sceneRootGo.transform.Find("LoadingLayerRoot").gameObject;
      this.m_uiSceneGroup1RootCanvasGo = this.m_sceneRootGo.transform.Find("UISceneRoot/UILayerGroupRoot1/Canvas").gameObject;
      this.m_uiSceneGroup2RootCanvasGo = this.m_sceneRootGo.transform.Find("UISceneRoot/UILayerGroupRoot2/Canvas").gameObject;
      this.m_uiSceneGroup1Camera = this.m_sceneRootGo.transform.Find("UISceneRoot/UILayerGroupRoot1").gameObject.GetComponent<Camera>();
      this.m_uiSceneGroup2Camera = this.m_sceneRootGo.transform.Find("UISceneRoot/UILayerGroupRoot2").gameObject.GetComponent<Camera>();
      this.m_uiSceneGroup1RootCanvas = this.m_uiSceneGroup1RootCanvasGo.GetComponent<Canvas>();
      this.m_uiSceneGroup2RootCanvas = this.m_uiSceneGroup2RootCanvasGo.GetComponent<Canvas>();
      this.m_uiSceneGroup1RootCanvasScaler = this.m_uiSceneGroup1RootCanvasGo.GetComponent<CanvasScaler>();
      this.m_uiSceneGroup2RootCanvasScaler = this.m_uiSceneGroup2RootCanvasGo.GetComponent<CanvasScaler>();
      this.m_uiSceneGroup1RootCanvasScaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
      this.m_uiSceneGroup1RootCanvasScaler.referenceResolution = new Vector2((float) this.m_designScreenWidth, (float) this.m_designScreenHeight);
      if (Screen.width < this.m_trigerWidth2ShrinkScale || Screen.height < this.m_trigerHeight2ShrinkScale)
        this.m_uiSceneGroup1RootCanvasScaler.screenMatchMode = CanvasScaler.ScreenMatchMode.Shrink;
      this.m_uiSceneGroup2RootCanvasScaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
      this.m_uiSceneGroup2RootCanvasScaler.referenceResolution = new Vector2((float) this.m_designScreenWidth, (float) this.m_designScreenHeight);
      if (Screen.width < this.m_trigerWidth2ShrinkScale || Screen.height < this.m_trigerHeight2ShrinkScale)
        this.m_uiSceneGroup2RootCanvasScaler.screenMatchMode = CanvasScaler.ScreenMatchMode.Shrink;
      if (this.m_useOrthographicForUILayer)
      {
        this.m_sceneRootGo.transform.Find("UISceneRoot/UILayerGroupRoot1").GetComponent<Camera>().orthographic = true;
        this.m_sceneRootGo.transform.Find("UISceneRoot/UILayerGroupRoot2").GetComponent<Camera>().orthographic = true;
      }
      return true;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CreateLayer(
      System.Type layerType,
      string name,
      string resPath,
      Action<SceneLayerBase> onComplete,
      bool enableReserve = false)
    {
      if (string.IsNullOrEmpty(name))
        throw new Exception("CreateLayer need a name");
      if (!this.m_enableLayerReserve)
        enableReserve = false;
      SceneLayerBase layerByName = this.FindLayerByName(name);
      if ((UnityEngine.Object) layerByName != (UnityEngine.Object) null)
      {
        if (layerByName.IsReserve() && layerByName.State == SceneLayerBase.LayerState.Unused)
          onComplete(layerByName);
        else
          onComplete((SceneLayerBase) null);
      }
      else
      {
        if (this.m_layerDict.ContainsKey(name))
          throw new Exception(string.Format("CreateLayer name conflict {0}", (object) name));
        if (layerType == typeof (UISceneLayer))
          this.CreateUILayer(name, resPath, onComplete, enableReserve);
        else if (layerType == typeof (ThreeDSceneLayer))
        {
          this.Create3DLayer(name, resPath, onComplete, enableReserve);
        }
        else
        {
          if (layerType != typeof (UnitySceneLayer))
            return;
          this.CreateUnitySceneLayer(name, resPath, onComplete);
        }
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateUILayer(
      string name,
      string resPath,
      Action<SceneLayerBase> onComplete,
      bool enableReserve = false)
    {
      GameObject go = UnityEngine.Object.Instantiate<GameObject>(this.m_uiLayerRootPrefab);
      Util.RemoveCloneFromGameObjectName(go);
      go.name = name + "_LayerRoot";
      UISceneLayer layer = go.GetComponent<UISceneLayer>();
      layer.SetName(name);
      layer.SetReserve(enableReserve);
      this.m_layerDict.Add(name, (SceneLayerBase) layer);
      this.AddLayerToLoading((SceneLayerBase) layer);
      this.m_corutineHelper.StartCorutine(ResourceManager.Instance.LoadAsset<GameObject>(resPath, (Action<string, GameObject>) ((lpath, lasset) =>
      {
        if (layer.State == SceneLayerBase.LayerState.WaitForFree)
        {
          this.FreeLayer((SceneLayerBase) layer);
        }
        else
        {
          this.OnLayerLoadAssetComplete((SceneLayerBase) layer, lasset);
          onComplete(!((UnityEngine.Object) lasset == (UnityEngine.Object) null) ? (SceneLayerBase) layer : (SceneLayerBase) null);
        }
      }), false, false));
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Create3DLayer(
      string name,
      string resPath,
      Action<SceneLayerBase> onComplete,
      bool enableReserve = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateUnitySceneLayer(
      string name,
      string resPath,
      Action<SceneLayerBase> onComplete)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FreeLayer(SceneLayerBase layer)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool PushLayer(SceneLayerBase layer)
    {
      if (layer.State != SceneLayerBase.LayerState.Unused)
      {
        Debug.LogError(string.Format("PushLayer in wrong state layer={0} state={1}", (object) layer.name, (object) layer.State));
        return false;
      }
      this.m_unusedLayerList.Remove(layer);
      this.m_layerStack.Add(layer);
      layer.State = SceneLayerBase.LayerState.InStack;
      if (layer.GetType() == typeof (UISceneLayer))
        layer.transform.SetParent(this.m_uiSceneGroup1RootCanvasGo.transform, false);
      else if (layer.GetType() == typeof (ThreeDSceneLayer))
        layer.transform.SetParent(this.m_3DSceneRootGo.transform, false);
      else if (layer.GetType() != typeof (UnitySceneLayer))
        ;
      if (layer.GetType() != typeof (UnitySceneLayer))
        layer.gameObject.SetActive(true);
      this.m_stackDirty = true;
      return true;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PopLayer(SceneLayerBase layer)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void BringLayerToTop(UISceneLayer layer)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Enable3DLayerBlur(bool isEnable)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddLayerToLoading(SceneLayerBase layer)
    {
      if (layer.GetType() == typeof (UnitySceneLayer))
        return;
      layer.gameObject.transform.SetParent(this.m_loadingLayerRootGo.transform, false);
      layer.gameObject.transform.localPosition = Vector3.zero;
      layer.gameObject.transform.localScale = Vector3.one;
      layer.gameObject.transform.localRotation = Quaternion.identity;
      layer.gameObject.SetActive(false);
      this.m_loadingLayerList.Add(layer);
      layer.State = SceneLayerBase.LayerState.Loading;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddLayerToUnused(SceneLayerBase layer)
    {
      if (layer.GetType() == typeof (UnitySceneLayer))
        return;
      this.m_loadingLayerList.Remove(layer);
      this.m_layerStack.Remove(layer);
      layer.gameObject.transform.SetParent(this.m_unusedLayerRootGo.transform, false);
      layer.gameObject.transform.localPosition = Vector3.zero;
      layer.gameObject.transform.localScale = Vector3.one;
      layer.gameObject.transform.localRotation = Quaternion.identity;
      layer.gameObject.SetActive(false);
      this.m_unusedLayerList.Add(layer);
      layer.State = SceneLayerBase.LayerState.Unused;
    }

    public GameObject RootCanvasGo
    {
      get
      {
        return this.m_uiSceneGroup1RootCanvasGo;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnLayerLoadAssetComplete(SceneLayerBase layer, GameObject asset)
    {
      if (layer.GetType() == typeof (UnitySceneLayer))
        return;
      if ((UnityEngine.Object) asset == (UnityEngine.Object) null)
      {
        Debug.LogError(string.Format("CreateUILayer LoadAsset fail name={0}", (object) layer.LayerName));
        this.FreeLayer(layer);
      }
      else
      {
        GameObject go = UnityEngine.Object.Instantiate<GameObject>(asset);
        Util.RemoveCloneFromGameObjectName(go);
        layer.AttachGameObject(go);
        this.AddLayerToUnused(layer);
      }
    }

    [DoNotToLua]
    [MethodImpl((MethodImplOptions) 32768)]
    public void Tick()
    {
      this.m_corutineHelper.Tick();
      this.UpdateLayerStack();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateLayerStack()
    {
      if (!this.m_stackDirty)
        return;
      this.m_stackDirty = false;
      this.SortLayerStack(this.m_layerStack);
      bool flag1 = false;
      int num = SceneManager.m_cameraDepthMax;
      Vector3 zero = Vector3.zero;
      UnitySceneLayer unitySceneLayer = (UnitySceneLayer) null;
      this.m_uiSceneGroup2RootCanvasGo.SetActive(false);
      for (int index = this.m_layerStack.Count - 1; index >= 0; --index)
      {
        SceneLayerBase layer = this.m_layerStack[index];
        if (layer.GetType() == typeof (UISceneLayer))
        {
          if (!flag1)
          {
            layer.gameObject.transform.SetParent(this.m_uiSceneGroup1RootCanvasGo.transform, false);
            layer.gameObject.transform.localPosition = Vector3.zero;
            layer.gameObject.transform.localScale = Vector3.one;
            layer.gameObject.transform.SetAsFirstSibling();
            layer.LayerCamera.depth = (float) num;
          }
          else
          {
            if (!this.m_uiSceneGroup2RootCanvasGo.activeSelf)
              this.m_uiSceneGroup2RootCanvasGo.SetActive(true);
            layer.gameObject.transform.SetParent(this.m_uiSceneGroup2RootCanvasGo.transform, false);
            layer.gameObject.transform.localPosition = Vector3.zero;
            layer.gameObject.transform.SetAsFirstSibling();
            layer.LayerCamera.depth = (float) num;
          }
        }
        else if (layer.GetType() == typeof (ThreeDSceneLayer))
        {
          layer.gameObject.transform.SetParent(this.m_3DSceneRootGo.transform, false);
          layer.gameObject.transform.localPosition = zero;
          zero += this.m_layerOffset;
          num = (int) (layer.LayerCamera.depth = (float) (num - 1)) - 1;
          MonoBehaviour component = layer.LayerCamera.gameObject.GetComponent("UnityStandardAssets.ImageEffects.BlurOptimized") as MonoBehaviour;
          if ((UnityEngine.Object) component != (UnityEngine.Object) null)
            component.enabled = this.m_is3DLayerBlurActive;
          flag1 = true;
        }
        else
        {
          unitySceneLayer = !((UnityEngine.Object) unitySceneLayer == (UnityEngine.Object) null) ? unitySceneLayer : (UnitySceneLayer) layer;
          num = (int) (layer.LayerCamera.depth = (float) (num - 1)) - 1;
          flag1 = true;
        }
      }
      bool flag2 = false;
      LayerRenderSettingDesc renderSetting = (LayerRenderSettingDesc) null;
      for (int index = this.m_layerStack.Count - 1; index >= 0; --index)
      {
        SceneLayerBase layer = this.m_layerStack[index];
        renderSetting = !((UnityEngine.Object) renderSetting == (UnityEngine.Object) null) ? renderSetting : layer.GetRenderSetting();
        if (!flag2)
          layer.gameObject.SetActive(true);
        else
          layer.gameObject.SetActive(false);
        if (layer.IsOpaque() && layer.IsFullScreen())
          flag2 = true;
      }
      if ((UnityEngine.Object) renderSetting == (UnityEngine.Object) null)
        renderSetting = this.GetDefaultRenderSetting();
      if ((UnityEngine.Object) unitySceneLayer != (UnityEngine.Object) null)
      {
        if (!(UnityEngine.SceneManagement.SceneManager.GetActiveScene() != unitySceneLayer.Scene))
          return;
        UnityEngine.SceneManagement.SceneManager.SetActiveScene(unitySceneLayer.Scene);
      }
      else
        this.ApplyRenderSetting(renderSetting);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SortLayerStack(List<SceneLayerBase> layerStack)
    {
      for (int index1 = 1; index1 < layerStack.Count; ++index1)
      {
        int index2 = index1;
        SceneLayerBase layer;
        for (layer = layerStack[index1]; index2 > 0 && !layer.IsStayOnTop() && layerStack[index2 - 1].IsStayOnTop(); --index2)
          layerStack[index2] = layerStack[index2 - 1];
        layerStack[index2] = layer;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private LayerRenderSettingDesc GetDefaultRenderSetting()
    {
      if ((UnityEngine.Object) this.m_defaultRenderSetting != (UnityEngine.Object) null)
        return this.m_defaultRenderSetting;
      this.m_defaultRenderSetting = this.m_sceneRootGo.GetComponentInChildren<LayerRenderSettingDesc>(true);
      return this.m_defaultRenderSetting;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ApplyRenderSetting(LayerRenderSettingDesc renderSetting)
    {
      if ((UnityEngine.Object) this.m_currRenderSetting == (UnityEngine.Object) renderSetting)
        return;
      this.m_currRenderSetting = renderSetting;
      this.ApplyRenderSettingImp(this.m_currRenderSetting);
    }

    public void ApplyCurrRenderSetting()
    {
      this.ApplyRenderSettingImp(this.m_currRenderSetting);
    }

    public LayerRenderSettingDesc GetCurrRenderSetting()
    {
      return this.m_currRenderSetting;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ApplyRenderSettingImp(LayerRenderSettingDesc renderSetting)
    {
      RenderSettings.ambientMode = renderSetting.enviornmentLighting.AmbientSource;
      if (renderSetting.enviornmentLighting.AmbientSource == AmbientMode.Skybox)
      {
        RenderSettings.skybox = renderSetting.enviornmentLighting.SkyBox;
        RenderSettings.ambientLight = renderSetting.enviornmentLighting.AmbientColor;
      }
      else if (renderSetting.enviornmentLighting.AmbientSource == AmbientMode.Flat)
      {
        RenderSettings.skybox = renderSetting.enviornmentLighting.SkyBox;
        RenderSettings.ambientLight = renderSetting.enviornmentLighting.AmbientColor;
      }
      RenderSettings.ambientIntensity = renderSetting.enviornmentLighting.AmbientIntensity;
      RenderSettings.fog = renderSetting.fog.EnableFog;
      RenderSettings.fogMode = renderSetting.fog.FogMode;
      RenderSettings.fogColor = renderSetting.fog.FogColor;
      if (renderSetting.fog.FogMode == FogMode.Linear)
      {
        RenderSettings.fogStartDistance = renderSetting.fog.FogStart;
        RenderSettings.fogEndDistance = renderSetting.fog.FogEnd;
      }
      else
        RenderSettings.fogDensity = renderSetting.fog.FogDensity;
      DynamicGI.UpdateEnvironment();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GameObject GetRootCanvas(int index = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public SceneLayerBase FindLayerByName(string name)
    {
      SceneLayerBase sceneLayerBase;
      this.m_layerDict.TryGetValue(name, out sceneLayerBase);
      return sceneLayerBase;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public SceneLayerBase FindUILayerUnderLayer(
      string name,
      Func<SceneLayerBase, bool> filter)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Camera GetLayerCameraFromSceneByPath(string rectTransPath)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RectTransform GetRectTransformFromSceneByPath(string rectTransPath)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetObjectPath(GameObject obj)
    {
      // ISSUE: unable to decompile the method.
    }

    public static SceneManager Instance
    {
      get
      {
        return SceneManager.m_instance;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    static SceneManager()
    {
    }
  }
}
