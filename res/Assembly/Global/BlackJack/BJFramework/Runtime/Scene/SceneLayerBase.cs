﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.Scene.SceneLayerBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.BJFramework.Runtime.Scene
{
  [CustomLuaClass]
  public class SceneLayerBase : MonoBehaviour
  {
    public SceneLayerBase.LayerState State;
    private LayerLayoutDesc m_layerLayoutDesc;
    private LayerRenderSettingDesc m_renderSetting;
    private bool m_renderSettingSearched;
    private bool m_isReserve;

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetName(string name)
    {
      if (!string.IsNullOrEmpty(this.LayerName))
        throw new Exception(string.Format("SceneLayerBase.SetName={0} to layer already have name={1}", (object) name, (object) this.LayerName));
      this.LayerName = name;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsOpaque()
    {
      LayerLayoutDesc desc = this.GetDesc();
      if ((UnityEngine.Object) desc == (UnityEngine.Object) null)
        return true;
      return desc.Opaque;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsFullScreen()
    {
      LayerLayoutDesc desc = this.GetDesc();
      if ((UnityEngine.Object) desc == (UnityEngine.Object) null)
        return true;
      return desc.FullScreen;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsStayOnTop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private LayerLayoutDesc GetDesc()
    {
      if ((UnityEngine.Object) this.m_layerLayoutDesc == (UnityEngine.Object) null)
      {
        LayerLayoutDesc[] componentsInChildren = this.transform.GetComponentsInChildren<LayerLayoutDesc>(true);
        if (componentsInChildren.Length != 0)
          this.m_layerLayoutDesc = componentsInChildren[0];
      }
      return this.m_layerLayoutDesc;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AttachGameObject(GameObject go)
    {
      if ((UnityEngine.Object) go == (UnityEngine.Object) null)
        return;
      Vector3 localPosition = go.transform.localPosition;
      Vector3 localScale = go.transform.localScale;
      Quaternion localRotation = go.transform.localRotation;
      go.transform.SetParent(this.transform);
      go.transform.localPosition = localPosition;
      go.transform.localScale = localScale;
      go.transform.localRotation = localRotation;
      this.LayerPrefabRoot = go;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public LayerRenderSettingDesc GetRenderSetting()
    {
      if (this.m_renderSettingSearched)
        return this.m_renderSetting;
      this.m_renderSettingSearched = true;
      LayerRenderSettingDesc[] componentsInChildren = this.GetComponentsInChildren<LayerRenderSettingDesc>(true);
      if (componentsInChildren.Length != 0)
        this.m_renderSetting = componentsInChildren[0];
      return this.m_renderSetting;
    }

    public bool IsReserve()
    {
      return this.m_isReserve;
    }

    public void SetReserve(bool value)
    {
      this.m_isReserve = value;
    }

    public string LayerName { get; private set; }

    public virtual Camera LayerCamera
    {
      get
      {
        return (Camera) null;
      }
    }

    public GameObject LayerPrefabRoot { get; private set; }

    public enum LayerState
    {
      None,
      Loading,
      Unused,
      InStack,
      WaitForFree,
    }
  }
}
