﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.IGameManagerLogicExtension
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using System;

namespace BlackJack.BJFramework.Runtime
{
  public interface IGameManagerLogicExtension
  {
    void OnLoadConfigDataEnd(Action<bool> onEnd);

    void OnLoadDynamicAssemblyEnd();

    void OnClear4Relogin();

    void Tick();
  }
}
