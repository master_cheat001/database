﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.UI.UICollider
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.BJFramework.Runtime.UI
{
  [AddComponentMenu("UIExtend/UICollider", 16)]
  public class UICollider : Graphic
  {
    public override bool Raycast(Vector2 sp, Camera eventCamera)
    {
      return true;
    }

    protected override void OnPopulateMesh(VertexHelper vh)
    {
      vh.Clear();
    }
  }
}
