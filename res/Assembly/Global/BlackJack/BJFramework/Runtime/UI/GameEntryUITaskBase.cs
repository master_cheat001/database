﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.UI.GameEntryUITaskBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Resource;
using System;
using System.Collections;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace BlackJack.BJFramework.Runtime.UI
{
  public abstract class GameEntryUITaskBase : UITaskBase
  {
    protected bool? m_startBundlePreDownloadByPlayer;
    protected int m_configDataInitLoadCount;

    public GameEntryUITaskBase(string name)
      : base(name)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void PostUpdateView()
    {
      if (ResourceManager.Instance.State != ResourceManager.RMState.Inited)
        return;
      this.m_corutineHelper.StartCorutine(new Func<IEnumerator>(this.EntryPipeLine));
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual IEnumerator DownloadAudioFiles()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual IEnumerator EntryPipeLine()
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new GameEntryUITaskBase.\u003CEntryPipeLine\u003Ec__Iterator1()
      {
        \u0024this = this
      };
    }

    protected virtual void OnStreamingAssetsFilesProcessingStart()
    {
      throw new NotImplementedException();
    }

    protected virtual void OnStreamingAssetsFilesProcessingEnd(bool ret)
    {
      throw new NotImplementedException();
    }

    protected virtual void OnBundleDataLoadingStart()
    {
      throw new NotImplementedException();
    }

    protected virtual void OnBundleDataLoadingEnd(bool ret)
    {
      throw new NotImplementedException();
    }

    protected virtual IEnumerator OnBasicVersionUnmatch()
    {
      throw new NotImplementedException();
    }

    protected virtual void NotifyUserDownloadAndWait(long totalDownloadByte)
    {
      throw new NotImplementedException();
    }

    public void StartPreDownload()
    {
      this.m_startBundlePreDownloadByPlayer = new bool?(true);
    }

    public void RefusePreDownload()
    {
      this.m_startBundlePreDownloadByPlayer = new bool?(false);
    }

    protected virtual void OnAssetBundlePreUpdateingRefuse()
    {
      throw new NotImplementedException();
    }

    protected virtual void OnAssetBundlePreUpdateingStart()
    {
      throw new NotImplementedException();
    }

    protected virtual void OnAssetBundlePreUpdateingEnd(bool ret)
    {
      throw new NotImplementedException();
    }

    protected virtual void OnAssetBundleManifestLoadingStart()
    {
      throw new NotImplementedException();
    }

    protected virtual void OnAssetBundleManifestLoadingEnd(bool ret)
    {
      throw new NotImplementedException();
    }

    protected virtual void OnLoadDynamicAssemblysStart()
    {
      throw new NotImplementedException();
    }

    protected virtual void OnLoadDynamicAssemblysEnd(bool ret)
    {
      throw new NotImplementedException();
    }

    protected virtual void OnStartLuaManagerStart()
    {
      throw new NotImplementedException();
    }

    protected virtual void OnStartLuaManagerEnd(bool ret)
    {
      throw new NotImplementedException();
    }

    protected virtual void OnLoadConfigDataStart()
    {
      throw new NotImplementedException();
    }

    protected virtual void OnLoadConfigDataEnd(bool ret)
    {
      throw new NotImplementedException();
    }

    protected virtual void OnStartAudioManagerStart()
    {
      throw new NotImplementedException();
    }

    protected virtual void OnStartAudioManagerEnd(bool ret)
    {
      throw new NotImplementedException();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual bool InitUITaskRegister()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void LaunchLogin()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnTick()
    {
      this.m_corutineHelper.Tick();
      switch (ResourceManager.Instance.State)
      {
        case ResourceManager.RMState.StreamingAssetsFilesProcessing:
          this.UpdateView4StreamingAssetsFilesProcessing();
          break;
        case ResourceManager.RMState.BundleDataLoading:
          this.UpdateView4BundleDataLoading();
          break;
        case ResourceManager.RMState.AssetBundlePreUpdateing:
          this.UpdateView4AssetBundlePreUpdateing();
          break;
        case ResourceManager.RMState.AssetBundleManifestLoading:
          this.UpdateView4AssetBundleManifestLoading();
          break;
      }
    }

    protected virtual void UpdateView4StreamingAssetsFilesProcessing()
    {
      throw new NotImplementedException();
    }

    protected virtual void UpdateView4BundleDataLoading()
    {
      throw new NotImplementedException();
    }

    protected virtual void UpdateView4AssetBundlePreUpdateing()
    {
      throw new NotImplementedException();
    }

    protected virtual void UpdateView4AssetBundleManifestLoading()
    {
      throw new NotImplementedException();
    }
  }
}
