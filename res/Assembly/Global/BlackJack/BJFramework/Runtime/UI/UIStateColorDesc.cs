﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.UI.UIStateColorDesc
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.BJFramework.Runtime.UI
{
  [Serializable]
  public class UIStateColorDesc
  {
    public Color TargetColor;
    public GameObject ChangeColorGo;
    [HideInInspector]
    public List<Image> ChangeColorImageList;
    [HideInInspector]
    public List<Text> ChangeColorTextList;

    [MethodImpl((MethodImplOptions) 32768)]
    public UIStateColorDesc()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
