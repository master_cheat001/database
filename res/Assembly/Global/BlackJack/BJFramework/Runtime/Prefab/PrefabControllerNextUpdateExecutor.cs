﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.Prefab.PrefabControllerNextUpdateExecutor
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.BJFramework.Runtime.Prefab
{
  public class PrefabControllerNextUpdateExecutor
  {
    private List<Action> m_nextUpdateExecutionList = new List<Action>();

    [MethodImpl((MethodImplOptions) 32768)]
    public PrefabControllerNextUpdateExecutor()
    {
    }

    public void AddNextUpdateExecution(Action action)
    {
      this.m_nextUpdateExecutionList.Add(action);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Tick()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
