﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.Prefab.PrefabControllerBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime.Lua;
using SLua;
using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.BJFramework.Runtime.Prefab
{
  [CustomLuaClass]
  public class PrefabControllerBase : MonoBehaviour
  {
    protected PrefabControllerNextUpdateExecutor m_nextUpdateExecutor = new PrefabControllerNextUpdateExecutor();
    protected bool m_inited;
    protected string m_ctrlName;
    public PrefabResourceContainer m_resContainer;

    [MethodImpl((MethodImplOptions) 32768)]
    public PrefabControllerBase()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Awake()
    {
      if (PrefabControllerBase.m_onAwake == null)
        return;
      PrefabControllerBase.m_onAwake(this.gameObject);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Initlize(string ctrlName, bool bindNow)
    {
      if (this.m_inited)
        return;
      this.m_inited = true;
      this.m_ctrlName = ctrlName;
      if (bindNow)
        this.BindFields();
      this.BindResContainer();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public T GetAssetInContainer<T>(string resName) where T : UnityEngine.Object
    {
      // ISSUE: unable to decompile the method.
    }

    public UnityEngine.Object GetAssetInContainer(string resName)
    {
      return this.GetAssetInContainer<UnityEngine.Object>(resName);
    }

    public void BindResContainer()
    {
      this.m_resContainer = this.GetComponent<PrefabResourceContainer>();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void BindFields()
    {
      foreach (FieldInfo field in this.GetType().GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic))
      {
        if (field.GetValue((object) this) == null)
        {
          object[] customAttributes = field.GetCustomAttributes(typeof (AutoBindAttribute), false);
          if (customAttributes.Length != 0)
          {
            AutoBindAttribute autoBindAttribute = (AutoBindAttribute) null;
            foreach (object obj in customAttributes)
            {
              if (obj.GetType() == typeof (AutoBindAttribute))
              {
                autoBindAttribute = obj as AutoBindAttribute;
                break;
              }
            }
            if (autoBindAttribute != null)
            {
              string path = autoBindAttribute.m_path;
              bool optional = autoBindAttribute.m_optional;
              System.Type fieldType = field.FieldType;
              UnityEngine.Object @object = this.BindFieldImpl(fieldType, path, autoBindAttribute.m_initState, field.Name, fieldType.Name, optional);
              if (@object != (UnityEngine.Object) null)
                field.SetValue((object) this, (object) @object);
            }
          }
        }
      }
      this.OnBindFiledsCompleted();
    }

    public PrefabControllerNextUpdateExecutor GetNextUpdateExecutor()
    {
      return this.m_nextUpdateExecutor;
    }

    protected virtual void OnBindFiledsCompleted()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual UnityEngine.Object BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName = null,
      bool optional = false)
    {
      UnityEngine.Object @object = (UnityEngine.Object) null;
      GameObject childByPath = this.GetChildByPath(path);
      if ((UnityEngine.Object) childByPath == (UnityEngine.Object) null)
      {
        Debug.LogError(string.Format("BindFields fail can not found child {0} in {1} {2}", (object) path, (object) this.gameObject, (object) this.GetType().Name));
        return (UnityEngine.Object) null;
      }
      if (fieldType == typeof (GameObject))
        @object = (UnityEngine.Object) childByPath;
      else if (fieldType.IsSubclassOf(typeof (PrefabControllerBase)))
      {
        PrefabControllerBase component = childByPath.GetComponent(fieldType) as PrefabControllerBase;
        if ((UnityEngine.Object) component == (UnityEngine.Object) null)
        {
          if (!optional)
            Debug.LogError(string.Format("BindFields fail can not found comp in child {0} {1}", (object) path, (object) fieldType.Name));
          return (UnityEngine.Object) null;
        }
        @object = (UnityEngine.Object) component;
      }
      else if (fieldType.IsSubclassOf(typeof (Component)))
      {
        Component component = childByPath.GetComponent(fieldType);
        if ((UnityEngine.Object) component == (UnityEngine.Object) null)
        {
          if (!optional)
            Debug.LogError(string.Format("BindFields fail can not found comp in child {0} {1}", (object) path, (object) fieldType.Name));
          return (UnityEngine.Object) null;
        }
        @object = (UnityEngine.Object) component;
      }
      return @object;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GameObject GetChildByPath(string path)
    {
      int num = path.IndexOf('/');
      if (num == -1)
        return this.gameObject;
      Transform transform = this.transform.Find(path.Substring(num + 1));
      if ((UnityEngine.Object) transform == (UnityEngine.Object) null)
        return (GameObject) null;
      return transform.gameObject;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static PrefabControllerBase AddControllerToGameObject(
      GameObject root,
      string path,
      TypeDNName ctrlTypeDNName,
      string ctrlName,
      string luaModuleName = null,
      bool autoBind = false)
    {
      System.Type type = ClassLoader.Instance.LoadType(ctrlTypeDNName);
      if (type == null)
      {
        Debug.LogError(string.Format("AddControllerToGameObject fail for {0}", (object) ctrlTypeDNName.m_typeFullName));
        return (PrefabControllerBase) null;
      }
      int num = path.IndexOf('/');
      if (num == -1)
      {
        if (root.name.Contains(path))
        {
          PrefabControllerBase component = root.GetComponent(type) as PrefabControllerBase;
          if ((UnityEngine.Object) component != (UnityEngine.Object) null)
            return component;
          root.GetComponent(type);
          PrefabControllerBase prefabControllerBase = root.AddComponent(type) as PrefabControllerBase;
          if ((UnityEngine.Object) prefabControllerBase != (UnityEngine.Object) null)
          {
            if (type.IsDefined(typeof (HotFixManuallyAttribute), false) && !string.IsNullOrEmpty(luaModuleName))
              LuaManager.TryInitHotfixForObj((object) prefabControllerBase, luaModuleName, (System.Type) null);
            prefabControllerBase.Initlize(ctrlName, autoBind);
          }
          return prefabControllerBase;
        }
        Debug.LogError(string.Format("AddControllerToGameObject fail path error {0}", (object) path));
        return (PrefabControllerBase) null;
      }
      string name = path.Substring(num + 1);
      Transform transform = root.transform.Find(name);
      if ((UnityEngine.Object) transform == (UnityEngine.Object) null)
      {
        Debug.LogError(string.Format("AddControllerToGameObject fail path error {0}", (object) path));
        return (PrefabControllerBase) null;
      }
      GameObject gameObject = transform.gameObject;
      PrefabControllerBase component1 = gameObject.GetComponent(type) as PrefabControllerBase;
      if ((UnityEngine.Object) component1 != (UnityEngine.Object) null)
        return component1;
      PrefabControllerBase prefabControllerBase1 = gameObject.AddComponent(type) as PrefabControllerBase;
      if ((UnityEngine.Object) prefabControllerBase1 != (UnityEngine.Object) null)
        prefabControllerBase1.Initlize(ctrlName, autoBind);
      return prefabControllerBase1;
    }

    public string CtrlName
    {
      get
      {
        return this.m_ctrlName;
      }
    }

    public static event Action<GameObject> m_onAwake
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
