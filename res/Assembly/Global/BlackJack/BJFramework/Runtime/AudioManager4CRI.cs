﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.AudioManager4CRI
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.Utils;
using SLua;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.BJFramework.Runtime
{
  [CustomLuaClass]
  public class AudioManager4CRI : IAudioManager, ITickable
  {
    public static bool m_isDiskFull = false;
    private static string m_CRIWAREAssetPath = "CRI/CRIWARE";
    private static string m_CriWareErrorHandlerAssetPath = "CRI/CriWareErrorHandler";
    private static string m_CriWareLibraryInitializerAssetPath = "CRI/CriWareLibraryInitializer";
    private static List<AudioManager4CRI.FileInfo> m_clientCriAssetFileInfos = new List<AudioManager4CRI.FileInfo>();
    private static List<AudioManager4CRI.FileInfo> m_serverCriAssetFileInfos = new List<AudioManager4CRI.FileInfo>();
    private static List<AudioManager4CRI.FileInfo> m_cacheCriAssetFileInfos = new List<AudioManager4CRI.FileInfo>();
    private static bool m_isAndroidOBB = false;
    private static AudioManager4CRI m_instance;
    private AudioListener m_mainAudioListener;
    private CRIDesc m_criDesc;
    private bool m_muteSound;
    private AudioSource m_playerVoiceAudioSource;
    private ICRIProvider m_CRIProvider;
    public static System.Type m_criProviderType;
    private const float m_delayRemoveEmptySheetSeconds = 15f;
    private Dictionary<string, AudioManager4CRI.PlaybacksOfSheet> m_playbacksOfSheets;
    public const string FilenameOfAllFilesPath = "AllFilesPath.txt";
    private TinyCorutineHelper m_corutineHelper;
    private const float m_invertSmoothDuration = 1.428571f;
    private List<string> m_languages;
    private string m_currentLanguage;
    private const string m_lastLanguagePlayerPrefName = "m_lastLanguagePlayerPrefName";

    [MethodImpl((MethodImplOptions) 32768)]
    public AudioManager4CRI()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public static IEnumerator CopyAllResourceFromeObbToCache()
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      AudioManager4CRI.\u003CCopyAllResourceFromeObbToCache\u003Ec__Iterator0 toCacheCIterator0 = new AudioManager4CRI.\u003CCopyAllResourceFromeObbToCache\u003Ec__Iterator0();
      return (IEnumerator) toCacheCIterator0;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator CopyResourceFromObb(
      string obbFilePath,
      string cacheFilePath,
      Action<bool> onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public static IEnumerator DownloadAudioFiles(
      Action<bool> onEnd,
      Action<long, long> onUpdate = null,
      Action<long> onEndAfterGotDownloadLength = null,
      MonoBehaviour coroutineOwner = null)
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new AudioManager4CRI.\u003CDownloadAudioFiles\u003Ec__Iterator2()
      {
        onEnd = onEnd,
        onEndAfterGotDownloadLength = onEndAfterGotDownloadLength,
        coroutineOwner = coroutineOwner,
        onUpdate = onUpdate
      };
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static long GetTotalFileLengh(List<AudioManager4CRI.FileInfo> fileList)
    {
      long num = 0;
      foreach (AudioManager4CRI.FileInfo file in fileList)
        num += file.m_length;
      return num;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool SaveFileInfos(string filepath, List<AudioManager4CRI.FileInfo> fileInfos)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator DownloadAudioFileToCache(
      AudioManager4CRI.FileInfo fi,
      Action<bool> onEnd,
      Action<WWW> onUpdate = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static List<AudioManager4CRI.FileInfo> DifferentiateFileInfos(
      List<AudioManager4CRI.FileInfo> serverInfos,
      List<AudioManager4CRI.FileInfo> clientInfos,
      List<AudioManager4CRI.FileInfo> cacheInfos)
    {
      List<AudioManager4CRI.FileInfo> fileInfoList = new List<AudioManager4CRI.FileInfo>();
      if (serverInfos == null || clientInfos == null)
        return fileInfoList;
      for (int index = 0; index < serverInfos.Count; ++index)
      {
        AudioManager4CRI.FileInfo i = serverInfos[index];
        if (!i.m_path.Contains("AllFilesPath.txt") && !clientInfos.Exists((Predicate<AudioManager4CRI.FileInfo>) (info =>
        {
          if (info.m_path == i.m_path)
            return info.m_md5 == i.m_md5;
          return false;
        })))
        {
          i.m_isAtClient = false;
          serverInfos[index] = i;
          if (cacheInfos == null || !cacheInfos.Exists((Predicate<AudioManager4CRI.FileInfo>) (info =>
          {
            if (info.m_path == i.m_path)
              return info.m_md5 == i.m_md5;
            return false;
          })))
            fileInfoList.Add(i);
        }
      }
      return fileInfoList;
    }

    public static string ResourceFromOBBPath
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        return string.Format("{0}/CRIResFromOBB", (object) Application.persistentDataPath);
      }
    }

    public static string CacheFolderPath
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        return string.Format("{0}/{1}", (object) Application.persistentDataPath, (object) GameManager.Instance.GameClientSetting.AudioSetting.CRIAudioManagerAsset);
      }
    }

    public static string DownloadUrlRoot { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<string> GetResourceFiles(string endStr)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateSheet()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetVolume(string category, float volume, bool isSmooth)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator SetVolumeSmoothly(string category, float volume)
    {
      // ISSUE: unable to decompile the method.
    }

    public float GetVolume(string category)
    {
      return this.m_CRIProvider.GetCategoryVolume(category);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static AudioManager4CRI CreateAudioManager()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitLanguages(List<string> languages, int defaultLanguageIdx)
    {
      // ISSUE: unable to decompile the method.
    }

    public List<string> GetLanguages()
    {
      return new List<string>((IEnumerable<string>) this.m_languages);
    }

    public string GetLanguage()
    {
      return this.m_currentLanguage;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLanguage(string language)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Initlize()
    {
      // ISSUE: unable to decompile the method.
    }

    public void Pause(bool pause)
    {
      this.m_CRIProvider.Pause(pause);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<AudioManager4CRI.FileInfo> ParseFileInfosText(string text)
    {
      List<AudioManager4CRI.FileInfo> fileInfoList = new List<AudioManager4CRI.FileInfo>();
      if (string.IsNullOrEmpty(text))
        return fileInfoList;
      string str1 = text;
      char[] chArray = new char[1]{ '\n' };
      foreach (string str2 in str1.Split(chArray))
      {
        if (!string.IsNullOrEmpty(str2))
        {
          string[] strArray = str2.Trim().Split(',');
          if (strArray.Length >= 3)
            fileInfoList.Add(new AudioManager4CRI.FileInfo(strArray[0], strArray[1], long.Parse(strArray[2]), true));
        }
      }
      return fileInfoList;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool LoadAllFilesPaths()
    {
      if (AudioManager4CRI.m_clientCriAssetFileInfos != null && AudioManager4CRI.m_clientCriAssetFileInfos.Count > 0)
        return true;
      AudioManager4CRI.m_clientCriAssetFileInfos = AudioManager4CRI.LoadFileInfos(Util.GetFileStreamingAssetsPath4WWW(Path.Combine(GameManager.Instance.GameClientSetting.AudioSetting.CRIAudioManagerAsset, "AllFilesPath.txt")));
      if (AudioManager4CRI.m_clientCriAssetFileInfos == null)
      {
        Debug.LogError("AudioManager4CRI.LoadFileInfos Error: m_criAssetFileInfos == null ");
        return false;
      }
      if (AudioManager4CRI.m_isAndroidOBB)
        AudioManager4CRI.AdjustFileListByFirstFiles();
      return true;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void AdjustFileListByFirstFiles()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string LoadTextFile(string filepath)
    {
      try
      {
        if (filepath.Contains(AudioManager4CRI.CacheFolderPath))
        {
          if (!File.Exists(filepath))
            return (string) null;
          return File.ReadAllText(filepath);
        }
        if (!filepath.Contains("://"))
          filepath = string.Format("file://{0}", (object) filepath);
        WWW www = new WWW(filepath);
        do
          ;
        while (!www.isDone);
        if (string.IsNullOrEmpty(www.error))
          return www.text;
        Debug.LogError(string.Format("AudioManager4CRI.LoadTextFile {0} Error: {1}", (object) filepath, (object) www.error));
        return (string) null;
      }
      catch (Exception ex)
      {
        Debug.LogError(string.Format("AudioManager4CRI.LoadTextFile exception: {0}", (object) ex.Message));
        return (string) null;
      }
    }

    public static List<AudioManager4CRI.FileInfo> LoadFileInfos(string filepath)
    {
      return AudioManager4CRI.ParseFileInfosText(AudioManager4CRI.LoadTextFile(filepath));
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Uninitlize()
    {
      // ISSUE: unable to decompile the method.
    }

    public ICRIProvider CRIProvider
    {
      set
      {
        this.m_CRIProvider = value;
      }
      get
      {
        return this.m_CRIProvider;
      }
    }

    private static string GetCRIResourcePath()
    {
      return "CRIRes";
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator Start(Action<bool> onEnd, string criAudioManagerAssetPath)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CreateCRI()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreatePlayerVoiceSource()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Tick()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string GetSheetName(string cueName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private CRIDesc.SheetDesc GetSheetDesc(string sheetName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private CRIDesc.SheetDesc GetSheetDescImpl(string sheetName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool AllocSheetByCueName(string cueName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public float GetCueLength(string cueName)
    {
      // ISSUE: unable to decompile the method.
    }

    public void StopAll()
    {
    }

    public void SetMute(
      bool muteBackGroundMusic,
      bool muteSound,
      bool muteMovieBackGroundMusic,
      bool mutePlayerVoice,
      bool muteSpeech)
    {
      Debug.LogError("Don't call function: AudioManager4CRI.SetMute!");
    }

    public void SetBackGroundMusicMute(bool muteBackGroundMusic)
    {
      Debug.LogError("Don't call function: AudioManager4CRI.SetBackGroundMusicMute!");
    }

    public void SetVolume(
      float backGroundMusicVolume,
      float soundVolume,
      float playerVoiceVolume,
      float speechVolume)
    {
      Debug.LogError("Don't call function: AudioManager4CRI.SetVolume!");
    }

    public void PlayBackGroundMusic(string music)
    {
      Debug.LogError("Don't call function: AudioManager4CRI.PlayBackGroundMusic!");
    }

    public void StopBackGroundMusic()
    {
      Debug.LogError("Don't call function: AudioManager4CRI.StopBackGroundMusic!");
    }

    public void PlaySound(AudioClip ac, float volume = 1f)
    {
      Debug.LogError("Don't call function: AudioManager4CRI.PlaySound!");
    }

    public void PlaySound(
      string sound,
      AudioClip audioClip,
      float volume = 1f,
      bool allowRepeatedPlaying = false)
    {
      Debug.LogError("Don't call function: AudioManager4CRI.PlaySound!");
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IAudioPlayback PlaySound(string sound, float volume = 1f)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StopSound(string sound)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddPlaybackToSheet(string sheetName, IAudioPlayback playback)
    {
    }

    public void StopSound()
    {
      Debug.LogError("Don't call function: AudioManager4CRI.StopSound!");
    }

    public void PlaySpeech(string sound, float volume = 1f)
    {
      Debug.LogError("Don't call function: AudioManager4CRI.PlaySpeech!");
    }

    public void StopSpeech()
    {
      Debug.LogError("Don't call function: AudioManager4CRI.StopSpeech!");
    }

    public bool IsSpeechPlaying()
    {
      Debug.LogError("Don't call function: AudioManager4CRI.IsSpeechPlaying!");
      return false;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayPlayerVoice(AudioClip sound, float volume = 1f)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsSoundPlaying()
    {
      Debug.LogError("Don't call function: AudioManager4CRI.IsSoundPlaying!");
      return false;
    }

    public void StopPlayerVoice()
    {
      this.StopPlayerVoiceByAudioSource();
    }

    public bool IsPlayerVoicePlaying()
    {
      return this.m_playerVoiceAudioSource.isPlaying;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StopPlayerVoiceByAudioSource()
    {
    }

    public static AudioManager4CRI Instance
    {
      get
      {
        return AudioManager4CRI.m_instance;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    static AudioManager4CRI()
    {
    }

    [CustomLuaClass]
    public class PlaybacksOfSheet
    {
      public List<IAudioPlayback> m_playbacks;
      public float m_removeTime;

      [MethodImpl((MethodImplOptions) 32768)]
      public PlaybacksOfSheet()
      {
      }
    }

    public struct FileInfo
    {
      public string m_path;
      public string m_md5;
      public long m_length;
      public bool m_isAtClient;

      public FileInfo(string path, string md5, long length, bool isAtClient = true)
      {
        this.m_path = path;
        this.m_md5 = md5;
        this.m_length = length;
        this.m_isAtClient = isAtClient;
      }
    }
  }
}
