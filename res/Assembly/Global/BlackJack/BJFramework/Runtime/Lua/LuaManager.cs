﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.Lua.LuaManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace BlackJack.BJFramework.Runtime.Lua
{
  [CustomLuaClass]
  public class LuaManager
  {
    private Dictionary<string, LuaTable> m_hotfixDict = new Dictionary<string, LuaTable>();
    private HashSet<string> m_bindInfoStrings;
    private static LuaManager m_instance;
    private string m_luaRootPath;
    private string m_luaInitModuleName;
    private LuaSvr m_luaSvr;
    private bool m_enableProtobufExtension;
    private LuaProtoBufExtensionHandler m_luaProtoBufExtensionHandler;
    public const string LuaObjHelperMemberName = "m_luaObjHelper";

    [MethodImpl((MethodImplOptions) 32768)]
    private LuaManager()
    {
    }

    [DoNotToLua]
    [MethodImpl((MethodImplOptions) 32768)]
    public static LuaManager CreateLuaManager()
    {
      if (LuaManager.m_instance == null)
        LuaManager.m_instance = new LuaManager();
      return LuaManager.m_instance;
    }

    [DoNotToLua]
    [MethodImpl((MethodImplOptions) 32768)]
    public bool Initlize(string luaRootPath, string luaInitModule = "LuaManagerInit", bool enableProtobufExtension = false)
    {
      this.m_luaRootPath = luaRootPath;
      this.m_luaInitModuleName = luaInitModule;
      this.m_enableProtobufExtension = enableProtobufExtension;
      return true;
    }

    [DoNotToLua]
    public void Uninitlize()
    {
    }

    [DoNotToLua]
    public void SetEventBindAllInPublishVersion(Action bindAction)
    {
    }

    [DoNotToLua]
    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator LoadBindInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    [MethodImpl((MethodImplOptions) 32768)]
    public void FilterBindInfo(List<Action<IntPtr>> list)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator StartLuaSvr(Action<bool> onComplete)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public LuaState GetLuaState()
    {
      return (LuaState) LuaSvr.mainState;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public LuaTable GetHotFixLuaModuleByTypeFullName(System.Type type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RegHotFix(string typeFullName, LuaTable module)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    [MethodImpl((MethodImplOptions) 32768)]
    public static bool TryInitHotfixForObj(object obj, string luaModuleName = null, System.Type objType = null)
    {
      if (LuaManager.m_instance == null || obj == null)
        return false;
      System.Type type = objType;
      if (objType == null)
        type = obj.GetType();
      LuaTable luaTable = !string.IsNullOrEmpty(luaModuleName) ? LuaManager.Instance.RequireModule(luaModuleName) : LuaManager.Instance.GetHotFixLuaModuleByTypeFullName(type);
      if ((LuaVar) luaTable == (LuaVar) null)
        return false;
      MethodInfo method = type.GetMethod("InitHotFix", BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.NonPublic);
      if (method == null)
      {
        Debug.LogWarning(string.Format("LuaManager.TryInitHotfixForObj Can't find InitHotFix method in :{0}", (object) luaModuleName));
        return false;
      }
      return (bool) method.Invoke(obj, new object[1]
      {
        (object) luaTable
      });
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public LuaTable RequireModule(string moduleName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private byte[] LuaLoader(string moduleName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsLuaModuleExist(string moduleName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string GetModulePath(string moduleName)
    {
      // ISSUE: unable to decompile the method.
    }

    public static LuaManager Instance
    {
      get
      {
        return LuaManager.m_instance;
      }
    }
  }
}
