﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectLBasic.ShowHideEvent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectLBasic
{
  [CustomLuaClass]
  public class ShowHideEvent : MonoBehaviour
  {
    private bool m_isStarted;
    private bool m_bNeedCallShow;
    public string m_eventParam;

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEnable()
    {
      // ISSUE: unable to decompile the method.
    }

    private void Start()
    {
      this.m_isStarted = true;
      this.CallOnShowEvent();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
    }

    private void OnDisable()
    {
      this.CallOnHideEvent();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CallOnShowEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CallOnHideEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    private string Param
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public static event Action<string> m_onShowEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public static event Action<string> m_onHideEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
