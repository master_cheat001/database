﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectLBasic.MultiLanguageManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using BlackJack.BJFramework.Runtime;
using PD.SDK;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectLBasic
{
  [CustomLuaClass]
  public class MultiLanguageManager
  {
    private static Dictionary<string, string[]> m_LanguageDict = (Dictionary<string, string[]>) null;
    public static string CurrentLanguage = string.Empty;
    private static List<string> s_ForceUpdateBundleList = new List<string>();
    private static List<string> s_SkipUpdateBundleList = new List<string>();
    private const string MULTI_LANGUAGE_KEY = "USER_CURRENT_LANGUAGE";
    private const string FORCE_UPDATE_LANGUAGE_RESOURCE_KEY = "FORCE_UPDATE_LANGUAGE_RESOURCE";

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool InitMultiLanguageSetting()
    {
      List<string[]> strArrayList = new List<string[]>();
      Dictionary<string, string[]> dictionary = (Dictionary<string, string[]>) null;
      if (MultiLanguageManager.LoadSetting())
        dictionary = MultiLanguageManager.GetLanguageDict();
      if (dictionary != null && dictionary.Count > 0)
        strArrayList = new List<string[]>((IEnumerable<string[]>) dictionary.Values);
      if (strArrayList.Count <= 0)
        return false;
      MultiLanguageManager.CurrentLanguage = string.Empty;
      if (PlayerPrefs.HasKey("USER_CURRENT_LANGUAGE") && dictionary.ContainsKey(PlayerPrefs.GetString("USER_CURRENT_LANGUAGE")))
      {
        MultiLanguageManager.CurrentLanguage = PlayerPrefs.GetString("USER_CURRENT_LANGUAGE");
      }
      else
      {
        int index = 0;
        if ((UnityEngine.Object) PDSDK.Instance != (UnityEngine.Object) null && PDSDK.IsInit)
          index = Math.Max(0, strArrayList.FindIndex((Predicate<string[]>) (ss => ss[2] == PDSDK.Instance.getSysLangue())));
        MultiLanguageManager.CurrentLanguage = strArrayList[index][0];
        PlayerPrefs.SetString("USER_CURRENT_LANGUAGE", MultiLanguageManager.CurrentLanguage);
        PlayerPrefs.Save();
      }
      if ((UnityEngine.Object) PDSDK.Instance != (UnityEngine.Object) null && PDSDK.IsInit)
        PDSDK.Instance.setSDKLangue(dictionary[MultiLanguageManager.CurrentLanguage][2]);
      return true;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void ChangeLanguage(string nextLanguage)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsNeedToForceUpdate()
    {
      return PlayerPrefs.HasKey("FORCE_UPDATE_LANGUAGE_RESOURCE") && PlayerPrefs.GetInt("FORCE_UPDATE_LANGUAGE_RESOURCE") == 1;
    }

    public static void ClearForceUpdateFlag()
    {
      PlayerPrefs.SetInt("FORCE_UPDATE_LANGUAGE_RESOURCE", 0);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool LoadSetting()
    {
      MultiLanguageManager.m_LanguageDict = new Dictionary<string, string[]>();
      TextAsset textAsset = Resources.Load<TextAsset>("MultiLanguage");
      if ((UnityEngine.Object) textAsset == (UnityEngine.Object) null)
      {
        Debug.LogError("MultiLanguage.Load Error");
        return false;
      }
      string[] strArray1 = textAsset.text.Split(new char[1]
      {
        '\n'
      }, StringSplitOptions.RemoveEmptyEntries);
      string[] strArray2 = (string[]) null;
      foreach (string str in strArray1)
      {
        string[] strArray3 = str.Trim().Split(new string[1]
        {
          ","
        }, StringSplitOptions.RemoveEmptyEntries);
        if (strArray3.Length < 3 || string.IsNullOrEmpty(strArray3[0]) || (string.IsNullOrEmpty(strArray3[1]) || string.IsNullOrEmpty(strArray3[2])))
        {
          Debug.LogError(string.Format("MultiLanguageMananger.LoadSetting invalid line: {0}", (object) str));
        }
        else
        {
          if (MultiLanguageManager.m_LanguageDict.TryGetValue(strArray3[0], out strArray2))
          {
            Debug.LogError(string.Format("MultiLanguage.Load() key {0} repeated.", (object) strArray3[0]));
            return false;
          }
          MultiLanguageManager.m_LanguageDict.Add(strArray3[0], strArray3);
          Debug.Log("m_LanguageDict.Add key=" + strArray3[0] + " value=" + strArray3[1]);
        }
      }
      return true;
    }

    public static Dictionary<string, string[]> GetLanguageDict()
    {
      return MultiLanguageManager.m_LanguageDict;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<string> GetLanguageList()
    {
      if (MultiLanguageManager.m_LanguageDict != null)
        return new List<string>((IEnumerable<string>) MultiLanguageManager.m_LanguageDict.Keys);
      return (List<string>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsDefaultLanguage()
    {
      if (MultiLanguageManager.m_LanguageDict.Count != 1)
        return MultiLanguageManager.CurrentLanguage == string.Empty;
      return true;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GMJson2String(string json)
    {
      // ISSUE: unable to decompile the method.
    }

    public static List<string> GetCustomPreUpdateBundleList()
    {
      return MultiLanguageManager.s_ForceUpdateBundleList;
    }

    public static List<string> GetCustomSkipPreUpdateBundleList()
    {
      return MultiLanguageManager.s_SkipUpdateBundleList;
    }

    public static void AddBundleNameToSkipUpdateList(string bundleName)
    {
      MultiLanguageManager.s_SkipUpdateBundleList.Add(bundleName);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void AddBundleNameToForceUpdateList(string bundleName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void SetConfigDataPath(ConfigDataSettings configSetting)
    {
      if (MultiLanguageManager.IsDefaultLanguage())
        return;
      configSetting.RuntimeConfigDataAssetTargetPath = configSetting.ConfigDataAssetTargetPath.Replace("ConfigData_ABS", string.Format("ConfigData_{0}_ABS", (object) MultiLanguageManager.CurrentLanguage));
    }

    [MethodImpl((MethodImplOptions) 32768)]
    static MultiLanguageManager()
    {
    }
  }
}
