﻿// Decompiled with JetBrains decompiler
// Type: JsonException
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using System;
using System.Runtime.CompilerServices;

public class JsonException : ApplicationException
{
  public JsonException()
  {
  }

  [MethodImpl((MethodImplOptions) 32768)]
  internal JsonException(ParserToken token)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  internal JsonException(ParserToken token, Exception inner_exception)
  {
  }

  [MethodImpl((MethodImplOptions) 32768)]
  internal JsonException(int c)
  {
  }

  [MethodImpl((MethodImplOptions) 32768)]
  internal JsonException(int c, Exception inner_exception)
  {
  }

  public JsonException(string message)
    : base(message)
  {
  }

  public JsonException(string message, Exception inner_exception)
    : base(message, inner_exception)
  {
  }
}
