﻿// Decompiled with JetBrains decompiler
// Type: DebugCmdManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using UnityEngine;

public class DebugCmdManager
{
  private List<string> _oldCommands = new List<string>(50);
  private Dictionary<string, IDebugCmd> _cmdMap;
  private static DebugCmdManager _instance;
  private int _showOldCommandIdx;

  [MethodImpl((MethodImplOptions) 32768)]
  private DebugCmdManager()
  {
    DebugCmdManager._instance = this;
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static DebugCmdManager Create(IConsoleMode debugConsoleMode)
  {
    DebugCmdManager debugCmdManager = new DebugCmdManager();
    debugCmdManager._Init(debugConsoleMode);
    return debugCmdManager;
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void AddCmd(System.Type cmdType)
  {
    IDebugCmd instance = (IDebugCmd) Activator.CreateInstance(cmdType);
    this._cmdMap.Add(instance.GetName().ToLower(), instance);
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void LoadIDebugCmds(Assembly a)
  {
    if (a == null)
      return;
    if (this._cmdMap == null)
      this._cmdMap = new Dictionary<string, IDebugCmd>();
    foreach (System.Type type in a.GetTypes())
    {
      if (type.GetInterface("IDebugCmd") != null)
        this.AddCmd(type);
    }
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void _Init(IConsoleMode consoleMode)
  {
    this.LoadIDebugCmds(Assembly.GetExecutingAssembly());
    this.LoadOldCommands();
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void RunInstruct(string instruct)
  {
    // ISSUE: unable to decompile the method.
  }

  private void _PrintWrongCommandHint()
  {
    Debug.LogError("Wrong command!use \"help\" to show all command.");
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private string _GetCommandName(string instruct)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private string _GetCommandParams(string instruct)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private IDebugCmd _GetCommand(string cmdName)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void PringAllCmdDescription()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public int GetCommandNumber()
  {
    // ISSUE: unable to decompile the method.
  }

  public static DebugCmdManager instance
  {
    get
    {
      return DebugCmdManager._instance;
    }
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void SaveOldCommands()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void LoadOldCommands()
  {
    string path = string.Format("{0}{1}DebugConsoleCommands.txt", (object) Application.persistentDataPath, (object) Path.AltDirectorySeparatorChar);
    try
    {
      if (!File.Exists(path))
        return;
      StreamReader streamReader = new StreamReader(path);
      this._oldCommands.Clear();
      while (!streamReader.EndOfStream)
      {
        string str = streamReader.ReadLine();
        if (!string.IsNullOrEmpty(str))
          this._oldCommands.Add(str);
      }
      streamReader.Close();
    }
    catch (FileNotFoundException ex)
    {
      Debug.Log(string.Format("[{0}] not found.", (object) path));
    }
    catch (FileLoadException ex)
    {
      Debug.LogError(string.Format("Failed to load [{0}].", (object) path));
    }
    catch (Exception ex)
    {
      Debug.LogError(ex.Message);
    }
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public string GetNextOldCommand()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public string GetPreviousOldCommand()
  {
    // ISSUE: unable to decompile the method.
  }
}
