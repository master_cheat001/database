﻿// Decompiled with JetBrains decompiler
// Type: ObjectMetadata
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using System.Collections.Generic;
using System.Runtime.CompilerServices;

internal struct ObjectMetadata
{
  private System.Type element_type;
  private bool is_dictionary;
  private IDictionary<string, PropertyMetadata> properties;

  public System.Type ElementType
  {
    [MethodImpl((MethodImplOptions) 32768)] get
    {
      // ISSUE: unable to decompile the method.
    }
    set
    {
      this.element_type = value;
    }
  }

  public bool IsDictionary
  {
    get
    {
      return this.is_dictionary;
    }
    set
    {
      this.is_dictionary = value;
    }
  }

  public IDictionary<string, PropertyMetadata> Properties
  {
    get
    {
      return this.properties;
    }
    set
    {
      this.properties = value;
    }
  }
}
