﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataCollectionActivityWaypointInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataCollectionActivityWaypointInfo")]
  [HotFix(true, m_isCtorOnly = true)]
  [CustomLuaClass]
  [Serializable]
  public class ConfigDataCollectionActivityWaypointInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _Desc;
    private CollectionActivityWaypointFuncType _FuncType;
    private List<int> _Waypoints_ID;
    private string _Model;
    private CollectionActivityWaypointStateType _InitState;
    private List<int> _ScenarioLevelIdList;
    private List<int> _ChallengeLevelIdList;
    private List<int> _LootLevelIdList;
    private List<int> _EventIdList;
    private IExtension extensionObject;
    public ConfigDataCollectionActivityInfo CollectionActivity;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionActivityWaypointInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "Desc")]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "FuncType")]
    public CollectionActivityWaypointFuncType FuncType
    {
      get
      {
        return this._FuncType;
      }
      set
      {
        this._FuncType = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, Name = "Waypoints_ID")]
    public List<int> Waypoints_ID
    {
      get
      {
        return this._Waypoints_ID;
      }
      set
      {
        this._Waypoints_ID = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, IsRequired = true, Name = "Model")]
    public string Model
    {
      get
      {
        return this._Model;
      }
      set
      {
        this._Model = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "InitState")]
    public CollectionActivityWaypointStateType InitState
    {
      get
      {
        return this._InitState;
      }
      set
      {
        this._InitState = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, Name = "ScenarioLevelIdList")]
    public List<int> ScenarioLevelIdList
    {
      get
      {
        return this._ScenarioLevelIdList;
      }
      set
      {
        this._ScenarioLevelIdList = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, Name = "ChallengeLevelIdList")]
    public List<int> ChallengeLevelIdList
    {
      get
      {
        return this._ChallengeLevelIdList;
      }
      set
      {
        this._ChallengeLevelIdList = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, Name = "LootLevelIdList")]
    public List<int> LootLevelIdList
    {
      get
      {
        return this._LootLevelIdList;
      }
      set
      {
        this._LootLevelIdList = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, Name = "EventIdList")]
    public List<int> EventIdList
    {
      get
      {
        return this._EventIdList;
      }
      set
      {
        this._EventIdList = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
