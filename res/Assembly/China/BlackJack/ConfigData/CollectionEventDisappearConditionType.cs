﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.CollectionEventDisappearConditionType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "CollectionEventDisappearConditionType")]
  public enum CollectionEventDisappearConditionType
  {
    [ProtoEnum(Name = "CollectionEventDisappearConditionType_None", Value = 0)] CollectionEventDisappearConditionType_None,
    [ProtoEnum(Name = "CollectionEventDisappearConditionType_CompleteScenario", Value = 1)] CollectionEventDisappearConditionType_CompleteScenario,
    [ProtoEnum(Name = "CollectionEventDisappearConditionType_CompleteChallengeLevel", Value = 2)] CollectionEventDisappearConditionType_CompleteChallengeLevel,
    [ProtoEnum(Name = "CollectionEventDisappearConditionType_Complete", Value = 3)] CollectionEventDisappearConditionType_Complete,
    [ProtoEnum(Name = "CollectionEventDisappearConditionType_LifeTime", Value = 4)] CollectionEventDisappearConditionType_LifeTime,
  }
}
