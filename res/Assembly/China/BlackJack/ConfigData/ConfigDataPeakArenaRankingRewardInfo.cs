﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataPeakArenaRankingRewardInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ConfigData
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "ConfigDataPeakArenaRankingRewardInfo")]
  [Serializable]
  public class ConfigDataPeakArenaRankingRewardInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private int _Rank;
    private int _RankingRewardMailTemplateId;
    private int _SeasonId;
    private int _SeasonRankTitleId;
    private string _RankListName;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataPeakArenaRankingRewardInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Rank")]
    public int Rank
    {
      get
      {
        return this._Rank;
      }
      set
      {
        this._Rank = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "RankingRewardMailTemplateId")]
    public int RankingRewardMailTemplateId
    {
      get
      {
        return this._RankingRewardMailTemplateId;
      }
      set
      {
        this._RankingRewardMailTemplateId = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "SeasonId")]
    public int SeasonId
    {
      get
      {
        return this._SeasonId;
      }
      set
      {
        this._SeasonId = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "SeasonRankTitleId")]
    public int SeasonRankTitleId
    {
      get
      {
        return this._SeasonRankTitleId;
      }
      set
      {
        this._SeasonRankTitleId = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.Default, IsRequired = true, Name = "RankListName")]
    public string RankListName
    {
      get
      {
        return this._RankListName;
      }
      set
      {
        this._RankListName = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
