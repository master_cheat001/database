﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.LabelType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "LabelType")]
  public enum LabelType
  {
    [ProtoEnum(Name = "LabelType_None", Value = 0)] LabelType_None,
    [ProtoEnum(Name = "LabelType_FixedTime", Value = 1)] LabelType_FixedTime,
    [ProtoEnum(Name = "LabelType_Recommand", Value = 2)] LabelType_Recommand,
    [ProtoEnum(Name = "LabelType_Discount", Value = 3)] LabelType_Discount,
    [ProtoEnum(Name = "LabelType_FirstDiscount", Value = 4)] LabelType_FirstDiscount,
  }
}
