﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.TrainingTechInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [CustomLuaClass]
  public class TrainingTechInfo
  {
    public int ID { get; set; }

    public int TechID { get; set; }

    public int RoomExp { get; set; }

    public int SoldierIDUnlocked { get; set; }

    public int SoldierSkillLevelup { get; set; }

    public int SoldierSkillID { get; set; }

    public int Gold { get; set; }

    public int RoomLevel { get; set; }

    public List<Goods> Materials { get; set; }

    public List<int> PreIds { get; set; }

    public List<int> PreTechLevels { get; set; }

    public ConfigDataTrainingTechLevelInfo Config { get; set; }

    public TrainingTechResourceRequirements LevelupRequirements
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
