﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.CollectionActivityLevelType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "CollectionActivityLevelType")]
  public enum CollectionActivityLevelType
  {
    [ProtoEnum(Name = "CollectionActivityLevelType_None", Value = 0)] CollectionActivityLevelType_None,
    [ProtoEnum(Name = "CollectionActivityLevelType_Scenario", Value = 1)] CollectionActivityLevelType_Scenario,
    [ProtoEnum(Name = "CollectionActivityLevelType_Challenge", Value = 2)] CollectionActivityLevelType_Challenge,
    [ProtoEnum(Name = "CollectionActivityLevelType_Loot", Value = 3)] CollectionActivityLevelType_Loot,
  }
}
