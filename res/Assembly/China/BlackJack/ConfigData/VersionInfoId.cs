﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.VersionInfoId
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "VersionInfoId")]
  public enum VersionInfoId
  {
    [ProtoEnum(Name = "VersionInfoId_ClientProgram", Value = 1)] VersionInfoId_ClientProgram = 1,
    [ProtoEnum(Name = "VersionInfoId_AssetBundle", Value = 2)] VersionInfoId_AssetBundle = 2,
    [ProtoEnum(Name = "VersionInfoId_BattleReport", Value = 3)] VersionInfoId_BattleReport = 3,
  }
}
