﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataArmyRelation
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ConfigData
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "ConfigDataArmyRelation")]
  [Serializable]
  public class ConfigDataArmyRelation : IExtensible
  {
    private int _ID;
    private ArmyTag _ArmyTagA;
    private ArmyTag _ArmyTagB;
    private int _Attack;
    private int _Defend;
    private int _Magic;
    private int _MagicDefend;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataArmyRelation()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ArmyTagA")]
    public ArmyTag ArmyTagA
    {
      get
      {
        return this._ArmyTagA;
      }
      set
      {
        this._ArmyTagA = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ArmyTagB")]
    public ArmyTag ArmyTagB
    {
      get
      {
        return this._ArmyTagB;
      }
      set
      {
        this._ArmyTagB = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Attack")]
    public int Attack
    {
      get
      {
        return this._Attack;
      }
      set
      {
        this._Attack = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Defend")]
    public int Defend
    {
      get
      {
        return this._Defend;
      }
      set
      {
        this._Defend = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Magic")]
    public int Magic
    {
      get
      {
        return this._Magic;
      }
      set
      {
        this._Magic = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MagicDefend")]
    public int MagicDefend
    {
      get
      {
        return this._MagicDefend;
      }
      set
      {
        this._MagicDefend = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
