﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.CollectionActivityWaypointStateType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "CollectionActivityWaypointStateType")]
  public enum CollectionActivityWaypointStateType
  {
    [ProtoEnum(Name = "CollectionActivityWaypointStateType_None", Value = 0)] CollectionActivityWaypointStateType_None,
    [ProtoEnum(Name = "CollectionActivityWaypointStateType_Hidden", Value = 1)] CollectionActivityWaypointStateType_Hidden,
    [ProtoEnum(Name = "CollectionActivityWaypointStateType_Locked", Value = 2)] CollectionActivityWaypointStateType_Locked,
    [ProtoEnum(Name = "CollectionActivityWaypointStateType_Unlocked", Value = 3)] CollectionActivityWaypointStateType_Unlocked,
  }
}
