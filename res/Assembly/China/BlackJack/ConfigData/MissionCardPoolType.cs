﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.MissionCardPoolType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "MissionCardPoolType")]
  public enum MissionCardPoolType
  {
    [ProtoEnum(Name = "MissionCardPoolType_None", Value = 0)] MissionCardPoolType_None,
    [ProtoEnum(Name = "MissionCardPoolType_Free", Value = 1)] MissionCardPoolType_Free,
    [ProtoEnum(Name = "MissionCardPoolType_Hero", Value = 2)] MissionCardPoolType_Hero,
    [ProtoEnum(Name = "MissionCardPoolType_Equipment", Value = 3)] MissionCardPoolType_Equipment,
  }
}
