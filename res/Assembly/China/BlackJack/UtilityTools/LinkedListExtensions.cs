﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.UtilityTools.LinkedListExtensions
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.UtilityTools
{
  public static class LinkedListExtensions
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public static LinkedListNode<T> Find<T>(
      this LinkedList<T> list,
      Predicate<T> pred)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static LinkedListNode<T> AddAfterFromLast<T>(
      this LinkedList<T> list,
      T element,
      Predicate<T> pred)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static LinkedListNode<T> AddAfterFromSpecific<T>(
      this LinkedList<T> list,
      T element,
      LinkedListNode<T> specific,
      Predicate<T> pred)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void CorrectNode<T>(
      this LinkedList<T> list,
      LinkedListNode<T> correctNode,
      Predicate<T> pred)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
