﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.TaskNs.MapSceneUpdatePipeLineCtxBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.BJFramework.Runtime.TaskNs
{
  [CustomLuaClassWithProtected]
  public class MapSceneUpdatePipeLineCtxBase
  {
    protected bool m_runing;
    public bool m_lockResCache;
    public int m_loadingStaticResCorutineCount;
    public int m_loadingDynamicResCorutineCount;
    public Action m_updateViewAction;
    [DoNotToLua]
    private MapSceneUpdatePipeLineCtxBase.LuaExportHelper luaExportHelper;

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool Start()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Clear()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsRunning()
    {
      return this.m_runing;
    }

    [DoNotToLua]
    public MapSceneUpdatePipeLineCtxBase.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    public class LuaExportHelper
    {
      private MapSceneUpdatePipeLineCtxBase m_owner;

      public LuaExportHelper(MapSceneUpdatePipeLineCtxBase owner)
      {
        this.m_owner = owner;
      }

      public bool m_runing
      {
        get
        {
          return this.m_owner.m_runing;
        }
        set
        {
          this.m_owner.m_runing = value;
        }
      }
    }
  }
}
