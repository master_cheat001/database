﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.TaskNs.Task
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading;

namespace BlackJack.BJFramework.Runtime.TaskNs
{
  [CustomLuaClassWithProtected]
  public class Task : ITickable
  {
    private TaskManager m_taskManager;
    private ulong m_currTickCount;
    private LinkedList<Task.DelayExecItem> m_delayExecList;
    [DoNotToLua]
    private Task.LuaExportHelper luaExportHelper;

    [MethodImpl((MethodImplOptions) 32768)]
    public Task(string name)
    {
      this.Name = name;
      this.State = Task.TaskState.Init;
      this.m_taskManager = TaskManager.Instance;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Start(object param = null)
    {
      if (this.State != Task.TaskState.Init)
      {
        Debug.LogError(string.Format("Task.Start {0} State != TaskState.Init", (object) this.Name));
        return false;
      }
      if (!this.m_taskManager.RegisterTask(this))
      {
        Debug.LogError(string.Format("Task.Start {0} RegisterTask fail", (object) this.Name));
        return false;
      }
      this.State = Task.TaskState.Running;
      if (!this.OnStart(param))
      {
        Debug.LogError(string.Format("Task.Start {0} OnStart fail", (object) this.Name));
        this.Stop();
        return false;
      }
      if (this.EventOnStart != null)
        this.EventOnStart(this);
      return true;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Stop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Pause()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Resume(object param = null)
    {
      // ISSUE: unable to decompile the method.
    }

    protected void ClearOnStopEvent()
    {
      this.EventOnStop = (Action<Task>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    void ITickable.Tick()
    {
      ++this.m_currTickCount;
      if (this.State == Task.TaskState.Stopped)
      {
        if (this.m_currTickCount % 200UL != 0UL)
          return;
        Debug.LogError("Still ticking an stopped task: " + (object) this);
      }
      else
      {
        if (this.State != Task.TaskState.Running)
          return;
        if (this.m_delayExecList != null)
        {
          while (this.m_delayExecList.First != null)
          {
            Task.DelayExecItem delayExecItem = this.m_delayExecList.First.Value;
            if (delayExecItem.m_execTargetTick <= this.m_currTickCount)
            {
              delayExecItem.m_action();
              this.m_delayExecList.RemoveFirst();
            }
            else
              break;
          }
        }
        this.OnTick();
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void ExecAfterTicks(Action action, ulong delayTickCount = 1)
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual bool OnStart(object param)
    {
      return true;
    }

    protected virtual void OnPause()
    {
    }

    protected virtual bool OnResume(object param = null)
    {
      return true;
    }

    protected virtual void OnStop()
    {
    }

    protected virtual void OnTick()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override string ToString()
    {
      return string.Format("[Task: name={0}, state={1}]", (object) this.GetType().Name, (object) this.State);
    }

    public string Name { get; private set; }

    public Task.TaskState State { get; private set; }

    public DateTime PauseStartTime { get; private set; }

    public event Action<Task> EventOnStart
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<Task> EventOnStop
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        Action<Task> comparand = this.EventOnStop;
        Action<Task> action;
        do
        {
          action = comparand;
          comparand = Interlocked.CompareExchange<Action<Task>>(ref this.EventOnStop, action + value, comparand);
        }
        while (comparand != action);
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<Task> EventOnPause
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<Task> EventOnResume
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public Task.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnStart(Task obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnStart(Task obj)
    {
      this.EventOnStart = (Action<Task>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnStop(Task obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnStop(Task obj)
    {
      this.EventOnStop = (Action<Task>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnPause(Task obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnPause(Task obj)
    {
      this.EventOnPause = (Action<Task>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnResume(Task obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnResume(Task obj)
    {
      this.EventOnResume = (Action<Task>) null;
    }

    public enum TaskState
    {
      Init,
      Running,
      Paused,
      Stopped,
    }

    public class DelayExecItem
    {
      public ulong m_execTargetTick;
      public Action m_action;
    }

    public class LuaExportHelper
    {
      private Task m_owner;

      public LuaExportHelper(Task owner)
      {
        this.m_owner = owner;
      }

      public void __callDele_EventOnStart(Task obj)
      {
        this.m_owner.__callDele_EventOnStart(obj);
      }

      public void __clearDele_EventOnStart(Task obj)
      {
        this.m_owner.__clearDele_EventOnStart(obj);
      }

      public void __callDele_EventOnStop(Task obj)
      {
        this.m_owner.__callDele_EventOnStop(obj);
      }

      public void __clearDele_EventOnStop(Task obj)
      {
        this.m_owner.__clearDele_EventOnStop(obj);
      }

      public void __callDele_EventOnPause(Task obj)
      {
        this.m_owner.__callDele_EventOnPause(obj);
      }

      public void __clearDele_EventOnPause(Task obj)
      {
        this.m_owner.__clearDele_EventOnPause(obj);
      }

      public void __callDele_EventOnResume(Task obj)
      {
        this.m_owner.__callDele_EventOnResume(obj);
      }

      public void __clearDele_EventOnResume(Task obj)
      {
        this.m_owner.__clearDele_EventOnResume(obj);
      }

      public TaskManager m_taskManager
      {
        get
        {
          return this.m_owner.m_taskManager;
        }
        set
        {
          this.m_owner.m_taskManager = value;
        }
      }

      public ulong m_currTickCount
      {
        get
        {
          return this.m_owner.m_currTickCount;
        }
        set
        {
          this.m_owner.m_currTickCount = value;
        }
      }

      public LinkedList<Task.DelayExecItem> m_delayExecList
      {
        get
        {
          return this.m_owner.m_delayExecList;
        }
        set
        {
          this.m_owner.m_delayExecList = value;
        }
      }

      public string Name
      {
        set
        {
          this.m_owner.Name = value;
        }
      }

      public Task.TaskState State
      {
        set
        {
          this.m_owner.State = value;
        }
      }

      public DateTime PauseStartTime
      {
        set
        {
          this.m_owner.PauseStartTime = value;
        }
      }

      public void ClearOnStopEvent()
      {
        this.m_owner.ClearOnStopEvent();
      }

      public void ExecAfterTicks(Action action, ulong delayTickCount)
      {
        this.m_owner.ExecAfterTicks(action, delayTickCount);
      }

      public bool OnStart(object param)
      {
        return this.m_owner.OnStart(param);
      }

      public void OnPause()
      {
        this.m_owner.OnPause();
      }

      public bool OnResume(object param)
      {
        return this.m_owner.OnResume(param);
      }

      public void OnStop()
      {
        this.m_owner.OnStop();
      }

      public void OnTick()
      {
        this.m_owner.OnTick();
      }
    }
  }
}
