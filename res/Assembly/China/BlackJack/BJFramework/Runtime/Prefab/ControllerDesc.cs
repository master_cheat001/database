﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.Prefab.ControllerDesc
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using System;

namespace BlackJack.BJFramework.Runtime.Prefab
{
  [Serializable]
  public struct ControllerDesc
  {
    public string m_ctrlName;
    public TypeDNName m_ctrlTypeDNName;
    public string m_luaModuleName;
    public string m_attachPath;
  }
}
