﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.UI.UIStateGradientColorDesc
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using System;
using UnityEngine;

namespace BlackJack.BJFramework.Runtime.UI
{
  [Serializable]
  public class UIStateGradientColorDesc
  {
    public Gradient GradientComptent;
    public Color Color1;
    public Color Color2;
  }
}
