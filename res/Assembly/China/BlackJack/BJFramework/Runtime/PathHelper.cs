﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.PathHelper
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.BJFramework.Runtime
{
  public static class PathHelper
  {
    public static string AssetBundlesBuildOutputPath = "AssetBundles";
    public static string BundleDataPath = "Assets/GameProject/RuntimeAssets/BundleData";
    public static string BundleDataVersionFileName = "BundleDataVersion.txt";
    public static string StreamingAssetsBundlePathDirName = "ExportAssetBundle";
    private static string m_assetBundlesBuildOutputPathWithPlatform;
    private static string m_assetBundlesDownloadPath4Editor;
    private static string m_bundleDataAssetName;
    private static string m_bundleDataAssetPathNoPostfix;
    private static string m_bundleDataAssetPath;
    private static string m_streamAssetBundleDataAssetPathInEditor;
    private static string m_streamAssetBundleDataAssetPath;
    private static string m_bundleDataBundleName;
    private static string m_bundleDataVersionFilePath;
    private static string m_streamingAssetsRootPath;
    private static string m_streamingAssetsBundlePath;
    private static string m_streamingAssetsFileListPath;
    private static string m_streamingAssetsFileListPathNoPostfix;
    private static string m_streamingAssetsFileListResourcesName;

    public static string AssetBundlesBuildOutputPathWithPlatform
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public static string AssetBundlesDownloadPath4Editor
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public static string BundleDataAssetName
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        if (Application.isEditor)
          return string.Format("BundleData{0}", (object) Util.GetCurrentTargetPlatform());
        if (PathHelper.m_bundleDataAssetName == null)
          PathHelper.m_bundleDataAssetName = string.Format("BundleData{0}", (object) Util.GetCurrentTargetPlatform());
        return PathHelper.m_bundleDataAssetName;
      }
    }

    public static string BundleDataAssetPathNoPostfix
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public static string BundleDataAssetPath
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        if (Application.isEditor)
          return string.Format("{0}{1}.asset", (object) PathHelper.BundleDataPath, (object) Util.GetCurrentTargetPlatform());
        if (PathHelper.m_bundleDataAssetPath == null)
          PathHelper.m_bundleDataAssetPath = string.Format("{0}{1}.asset", (object) PathHelper.BundleDataPath, (object) Util.GetCurrentTargetPlatform());
        return PathHelper.m_bundleDataAssetPath;
      }
    }

    public static string StreamAssetBundleDataAssetPathInEditor
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public static string StreamAssetBundleDataResourcesPath
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        if (Application.isEditor)
          return string.Format("StreamingAssetsRes/{0}", (object) PathHelper.BundleDataAssetName);
        if (PathHelper.m_streamAssetBundleDataAssetPath == null)
          PathHelper.m_streamAssetBundleDataAssetPath = string.Format("StreamingAssetsRes/{0}", (object) PathHelper.BundleDataAssetName);
        return PathHelper.m_streamAssetBundleDataAssetPath;
      }
    }

    public static string BundleDataBundleName
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        if (Application.isEditor)
          return Util.GetBundleNameByAssetPath(PathHelper.BundleDataAssetPath, string.Empty, true);
        if (PathHelper.m_bundleDataBundleName == null)
          PathHelper.m_bundleDataBundleName = Util.GetBundleNameByAssetPath(PathHelper.BundleDataAssetPath, string.Empty, true);
        return PathHelper.m_bundleDataBundleName;
      }
    }

    public static string BundleDataVersionFilePath
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public static string StreamingAssetsRootPath
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public static string StreamingAssetsBundlePath
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public static string StreamingAssetsFileListPath
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public static string StreamingAssetsFileListPathNoPostfix
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public static string StreamingAssetsFileListResourcesName
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        if (Application.isEditor)
          return string.Format("StreamingAssetsRes/StreamingAssetsFileList{0}", (object) Util.GetCurrentTargetPlatform());
        if (PathHelper.m_streamingAssetsFileListResourcesName == null)
          PathHelper.m_streamingAssetsFileListResourcesName = string.Format("StreamingAssetsRes/StreamingAssetsFileList{0}", (object) Util.GetCurrentTargetPlatform());
        return PathHelper.m_streamingAssetsFileListResourcesName;
      }
    }

    public static string DynamicAssemblyRoot
    {
      get
      {
        return "Assets/plugins/BlackJack";
      }
    }

    public static string DynamicAssemblyAssetRoot
    {
      get
      {
        return "Assets/GameProject/RuntimeAssets/Assemblys";
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    static PathHelper()
    {
    }
  }
}
