﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.LoginSettings
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using System;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.BJFramework.Runtime
{
  [Serializable]
  public class LoginSettings
  {
    public bool LoginUseSettings = true;
    public string GameServerAddress = "192.168.1.111";
    public int GameServerPort = 16001;
    public string LoginAccount = "testUser1";
    public string ClientVersion = string.Empty;
    public int AndroidBasicVersion = 1;
    public int IOSBasicVersion = 1;
    public bool LoginUseSDK;

    [MethodImpl((MethodImplOptions) 32768)]
    public LoginSettings()
    {
    }

    public int BasicVersion
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        if (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.OSXEditor)
          return this.IOSBasicVersion;
        return this.AndroidBasicVersion;
      }
    }
  }
}
