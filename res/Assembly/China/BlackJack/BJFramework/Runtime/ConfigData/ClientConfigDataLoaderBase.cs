﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.ConfigData.ClientConfigDataLoaderBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Resource;
using ProtoBuf;
using SLua;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.BJFramework.Runtime.ConfigData
{
  [HotFix]
  public abstract class ClientConfigDataLoaderBase
  {
    protected GameClientSetting m_gameClientSetting;
    [DoNotToLua]
    protected Dictionary<string, UnityEngine.Object> m_assetDict;
    [DoNotToLua]
    protected Dictionary<string, string> m_assetMD5Dict;
    private Dictionary<string, List<ClientConfigDataLoaderBase.LazyLoadConfigAssetInfo>> m_lazyLoadConfigDataAssetPathDict;
    [DoNotToLua]
    private ClientConfigDataLoaderBase.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnInitLoadFromAssetEndWorkerForLuaDummyType_hotfix;
    private LuaFunction m_AddConfigDataItemForLuaDummyTypeStringDummyType_hotfix;
    private LuaFunction m_GetAllInitLoadConfigDataTypeNameForLuaDummy_hotfix;
    private LuaFunction m_GetAllInitLoadConfigDataAssetPath_hotfix;
    private LuaFunction m_LoadLazyLoadConfigDataAssetStringStringAction`1_hotfix;
    private LuaFunction m_OnLazyLoadFromAssetEndStringStringObjectAction`1_hotfix;
    private LuaFunction m_GetLazyLoadConfigAssetNameByKeyStringInt32_hotfix;
    private LuaFunction m_GetConfigDataPath_hotfix;
    private LuaFunction m_GetAllLazyLoadConfigDataAssetPath_hotfix;
    private LuaFunction m_GetLazyLoadConfigAssetInfoStringString_hotfix;
    private LuaFunction m_DeserializeExtensionTableOnLoadFromAssetEndBytesScriptableObjectMD5StringString_hotfix;
    private LuaFunction m_ReportNullAssetInfo_hotfix;
    private LuaFunction m_FireEventOnConfigDataTableLoadEnd_hotfix;
    private LuaFunction m_add_EventOnConfigDataTableLoadEndAction_hotfix;
    private LuaFunction m_remove_EventOnConfigDataTableLoadEndAction_hotfix;
    private LuaFunction m_get_ConfigDataAssetAllowNull_hotfix;
    private LuaFunction m_set_ConfigDataAssetAllowNullBoolean_hotfix;
    private LuaFunction m_get_ConfigDataMD5Dict_hotfix;
    private LuaFunction m_get_InitLoadDataCount_hotfix;
    private LuaFunction m_set_InitLoadDataCountInt32_hotfix;

    [DoNotToLua]
    [MethodImpl((MethodImplOptions) 32768)]
    public ClientConfigDataLoaderBase(string luaModuleName)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool StartInitializeFromAsset(Action<bool> onEnd, out int initLoadDataCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [DoNotToLua]
    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual IEnumerator OnInitLoadFromAssetEnd(
      bool ret,
      int initThreadCount,
      int loadCountForSingleYield,
      Action<bool> onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    protected abstract IEnumerator OnInitLoadFromAssetEndWorker(
      int totalWorkerCount,
      int workerId,
      int loadCountForSingleYield,
      Action<bool> onEnd,
      List<string> skipTypeList = null,
      List<string> filterTypeList = null);

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual bool OnInitLoadFromAssetEndWorkerForLuaDummyType()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void AddConfigDataItemForLuaDummyType(string typeName, DummyType dataItem)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual List<string> GetAllInitLoadConfigDataTypeNameForLuaDummy()
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsUseMulitThreadOnInitLoadFromAssetEnd()
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    protected void ClearAsset()
    {
      this.m_assetDict.Clear();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual HashSet<string> GetAllInitLoadConfigDataAssetPath()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void LoadLazyLoadConfigDataAsset(
      string configDataName,
      string configAssetName,
      Action<bool> onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnLazyLoadFromAssetEnd(
      string configDataName,
      string configAssetName,
      UnityEngine.Object lasset,
      Action<bool> onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual string GetLazyLoadConfigAssetNameByKey(string configDataName, int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetConfigDataPath()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual Dictionary<string, List<ClientConfigDataLoaderBase.LazyLoadConfigAssetInfo>> GetAllLazyLoadConfigDataAssetPath()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientConfigDataLoaderBase.LazyLoadConfigAssetInfo GetLazyLoadConfigAssetInfo(
      string configDataName,
      string configAssetName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<DummyType> DeserializeExtensionTableOnLoadFromAssetEnd(
      BytesScriptableObjectMD5 dataObj,
      string assetPath,
      string typeName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ReportNullAssetInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void FireEventOnConfigDataTableLoadEnd()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnConfigDataTableLoadEnd
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public bool ConfigDataAssetAllowNull
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public Dictionary<string, string> ConfigDataMD5Dict
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public int InitLoadDataCount
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public ClientConfigDataLoaderBase.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnConfigDataTableLoadEnd()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnConfigDataTableLoadEnd()
    {
      this.EventOnConfigDataTableLoadEnd = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public enum LazyLoadConfigAsssetState
    {
      Unload,
      Loading,
      Loaded,
      Loadfailed,
    }

    [DoNotToLua]
    public class LazyLoadConfigAssetInfo
    {
      public string m_configAssetName;
      public ClientConfigDataLoaderBase.LazyLoadConfigAsssetState m_state;
    }

    public class LuaExportHelper
    {
      private ClientConfigDataLoaderBase m_owner;

      public LuaExportHelper(ClientConfigDataLoaderBase owner)
      {
        this.m_owner = owner;
      }

      public void __callDele_EventOnConfigDataTableLoadEnd()
      {
        this.m_owner.__callDele_EventOnConfigDataTableLoadEnd();
      }

      public void __clearDele_EventOnConfigDataTableLoadEnd()
      {
        this.m_owner.__clearDele_EventOnConfigDataTableLoadEnd();
      }

      public GameClientSetting m_gameClientSetting
      {
        get
        {
          return this.m_owner.m_gameClientSetting;
        }
        set
        {
          this.m_owner.m_gameClientSetting = value;
        }
      }

      public Dictionary<string, List<ClientConfigDataLoaderBase.LazyLoadConfigAssetInfo>> m_lazyLoadConfigDataAssetPathDict
      {
        get
        {
          return this.m_owner.m_lazyLoadConfigDataAssetPathDict;
        }
        set
        {
          this.m_owner.m_lazyLoadConfigDataAssetPathDict = value;
        }
      }

      public IEnumerator OnInitLoadFromAssetEndWorker(
        int totalWorkerCount,
        int workerId,
        int loadCountForSingleYield,
        Action<bool> onEnd,
        List<string> skipTypeList,
        List<string> filterTypeList)
      {
        return this.m_owner.OnInitLoadFromAssetEndWorker(totalWorkerCount, workerId, loadCountForSingleYield, onEnd, skipTypeList, filterTypeList);
      }

      public bool OnInitLoadFromAssetEndWorkerForLuaDummyType()
      {
        return this.m_owner.OnInitLoadFromAssetEndWorkerForLuaDummyType();
      }

      public void AddConfigDataItemForLuaDummyType(string typeName, DummyType dataItem)
      {
        this.m_owner.AddConfigDataItemForLuaDummyType(typeName, dataItem);
      }

      public List<string> GetAllInitLoadConfigDataTypeNameForLuaDummy()
      {
        return this.m_owner.GetAllInitLoadConfigDataTypeNameForLuaDummy();
      }

      public HashSet<string> GetAllInitLoadConfigDataAssetPath()
      {
        return this.m_owner.GetAllInitLoadConfigDataAssetPath();
      }

      public void OnLazyLoadFromAssetEnd(
        string configDataName,
        string configAssetName,
        UnityEngine.Object lasset,
        Action<bool> onEnd)
      {
        this.m_owner.OnLazyLoadFromAssetEnd(configDataName, configAssetName, lasset, onEnd);
      }

      public Dictionary<string, List<ClientConfigDataLoaderBase.LazyLoadConfigAssetInfo>> GetAllLazyLoadConfigDataAssetPath()
      {
        return this.m_owner.GetAllLazyLoadConfigDataAssetPath();
      }

      public void ReportNullAssetInfo()
      {
        this.m_owner.ReportNullAssetInfo();
      }

      public void FireEventOnConfigDataTableLoadEnd()
      {
        this.m_owner.FireEventOnConfigDataTableLoadEnd();
      }
    }
  }
}
