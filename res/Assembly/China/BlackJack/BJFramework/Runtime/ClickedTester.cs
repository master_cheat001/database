﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.ClickedTester
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.EventSystems;

namespace BlackJack.BJFramework.Runtime
{
  public class ClickedTester : MonoBehaviour, IPointerClickHandler, IEventSystemHandler
  {
    private bool m_isClicked;

    public bool Clicked
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        bool isClicked = this.m_isClicked;
        this.m_isClicked = false;
        return isClicked;
      }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
      this.m_isClicked = true;
    }
  }
}
