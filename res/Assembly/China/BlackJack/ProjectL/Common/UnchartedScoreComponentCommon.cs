﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.UnchartedScoreComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class UnchartedScoreComponentCommon : IComponentBase
  {
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected IConfigDataLoader m_configDataLoader;
    protected BattleComponentCommon m_battle;
    protected BagComponentCommon m_bag;
    protected HeroComponentCommon m_hero;
    protected OperationalActivityCompomentCommon m_operationalActivity;
    protected DataSectionUnchartedScore m_UnchartedScoreDS;

    [MethodImpl((MethodImplOptions) 32768)]
    public UnchartedScoreComponentCommon()
    {
      // ISSUE: unable to decompile the method.
    }

    public string GetName()
    {
      return "UnchartedScore";
    }

    public virtual void Init()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    public void DeInit()
    {
    }

    public void Tick(uint deltaMillisecond)
    {
    }

    public virtual bool Serialize<T>(T dest)
    {
      return false;
    }

    public virtual void DeSerialize<T>(T source)
    {
    }

    public virtual void PostDeSerialize()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int IsUnchartedScoreOnActivityTime(int UnchartedScoreId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetAllOpenActivityUnchartedScore()
    {
      // ISSUE: unable to decompile the method.
    }

    public OperationalActivityBase FindOperationalActivityByUnchartedScoreId(
      int UnchartedScoreId)
    {
      return this.m_operationalActivity.FindOperationalActivityByUnchartedScoreId(UnchartedScoreId);
    }

    public bool IsChallengeLevelContains(UnchartedScore unchartedScore, int levelId)
    {
      return unchartedScore.ChallengeLevelCompleteIds.Contains(levelId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsChallengeLevelExist(UnchartedScore unchartedScore, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsChallengeLevelTimeUnlock(
      OperationalActivityBase operationalActivity,
      ConfigDataChallengeLevelInfo levelInfo,
      DateTime curDateTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsChallengePrevLevelComplete(
      UnchartedScore unchartedScore,
      ConfigDataChallengeLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsScoreLevelContains(UnchartedScore unchartedScore, int levelId)
    {
      return unchartedScore.SocreLevelCompleteIds.Contains(levelId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsScoreLevelExist(UnchartedScore unchartedScore, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsScoreLevelTimeUnlock(
      OperationalActivityBase operationalActivity,
      ConfigDataScoreLevelInfo levelInfo,
      DateTime curDateTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPlayerLevelVaild(ConfigDataScoreLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CalcScoreLevelScore(
      UnchartedScore unchartedScore,
      ConfigDataScoreLevelInfo levelInfo,
      List<int> heroIdList,
      bool levelBonus)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CalcChallengeLevelScore(
      UnchartedScore unchartedScore,
      ConfigDataChallengeLevelInfo levelInfo,
      List<int> heroIdList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CalcAllHeroBonus(UnchartedScore unchartedScore, List<int> heroIdList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsChallengeLevelUnlock(
      UnchartedScore unchartedScore,
      int levelId,
      DateTime curDateTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsScoreLevelUnlock(
      UnchartedScore unchartedScore,
      int levelId,
      DateTime curDateTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsScoreLevelUnlock(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetAllUnlockedScoreLevels()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsChallengeLevelComplete(UnchartedScore unchartedScore, int levelId)
    {
      return unchartedScore.ChallengeLevelCompleteIds.Contains(levelId);
    }

    public bool IsScoreLevelComplete(UnchartedScore unchartedScore, int levelId)
    {
      return unchartedScore.SocreLevelCompleteIds.Contains(levelId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int IsUnchartedScoreExist(int unchartedScoreId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Goods> GetReward(int unchartedScoreId, int score)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected int CanRewardGain(int unchartedScoreId, int score)
    {
      // ISSUE: unable to decompile the method.
    }

    public void RemoveUnchartedScore(int unchartedScoreId)
    {
      this.m_UnchartedScoreDS.RemoveUnchartedScore(unchartedScoreId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CheckChallengeLevelEnergy(ConfigDataChallengeLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CheckScoreLevelEnergy(ConfigDataScoreLevelInfo levelInfo, bool isTeamBattle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int CanAttackChallengeLevel(int unchartedScoreId, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AttackChallengeLevel(int unchartedScoreId, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCommonSuccessChallengeLevel(
      UnchartedScore unchartedScore,
      ConfigDataChallengeLevelInfo levelInfo,
      List<int> battleTreasures,
      List<int> heroes,
      int energyCost,
      bool isBattleTeam)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCommonSuccessScoreLevel(
      UnchartedScore unchartedScore,
      ConfigDataScoreLevelInfo levelInfo,
      List<int> battleTreasures,
      List<int> heroes,
      List<int> allHeroes,
      int energyCost,
      bool isBattleTeam)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int CanAttackScoreLevel(int unchartedScoreId, int levelId, bool isTeamBattle = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AttackScoreLevel(int unchartedScoreId, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void CheckScoreReward(UnchartedScore unchartedScore, GameFunctionType causeId = GameFunctionType.GameFunctionType_None)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FlushBounsCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetScoreRewardGain(UnchartedScore unchartedScore, int score)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public UnchartedScore GetUnchartedScore(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    public IComponentOwner Owner { get; set; }
  }
}
