﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.TeamComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Protocol;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Common
{
  [HotFix]
  public class TeamComponentCommon : IComponentBase
  {
    protected IConfigDataLoader m_configDataLoader;
    protected DataSectionTeam m_teamDS;
    protected BagComponentCommon m_bag;
    protected AnikiGymComponentCommon m_anikiGym;
    protected MemoryCorridorCompomentCommon m_memoryCorridor;
    protected ThearchyTrialCompomentCommon m_thearchyTrial;
    protected EternalShrineCompomentCommon m_eternalShrine;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected HeroTrainningComponentCommon m_heroTrainning;
    protected CooperateBattleCompomentCommon m_cooperateBattle;
    protected UnchartedScoreComponentCommon m_unchartedScore;
    protected CollectionComponentCommon m_collectionActivity;
    protected BattleComponentCommon m_battle;
    public Action FinishTeamBattleMissionEvent;
    [DoNotToLua]
    private TeamComponentCommon.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_GetName_hotfix;
    private LuaFunction m_Init_hotfix;
    private LuaFunction m_PostInit_hotfix;
    private LuaFunction m_DeInit_hotfix;
    private LuaFunction m_TickUInt32_hotfix;
    private LuaFunction m_SerializeT_hotfix;
    private LuaFunction m_DeSerializeT_hotfix;
    private LuaFunction m_PostDeSerialize_hotfix;
    private LuaFunction m_get_Owner_hotfix;
    private LuaFunction m_set_OwnerIComponentOwner_hotfix;
    private LuaFunction m_IsLevelUnlockedGameFunctionTypeInt32_hotfix;
    private LuaFunction m_CanCreateTeamProTeamRoomSetting_hotfix;
    private LuaFunction m_CreateTeamInt32GameFunctionTypeInt32_hotfix;
    private LuaFunction m_IsTeamRoomAuthorityValidInt32_hotfix;
    private LuaFunction m_CanAttackTeamGameFunctionGameFunctionTypeInt32_hotfix;
    private LuaFunction m_CanViewTeamRoomGameFunctionTypeInt32Int32_hotfix;
    private LuaFunction m_CanAutoMatchTeamRoomGameFunctionTypeInt32_hotfix;
    private LuaFunction m_AutoMatchTeamRoomInt32GameFunctionTypeInt32_hotfix;
    private LuaFunction m_JoinTeamRoomInt32GameFunctionTypeInt32_hotfix;
    private LuaFunction m_GetTeamRoomInfoGameFunctionType_Int32__hotfix;
    private LuaFunction m_QuitTeamRoom_hotfix;
    private LuaFunction m_CanCancelAutoMatchTeamRoom_hotfix;
    private LuaFunction m_CanChangeTeamRoomAuthorityInt32_hotfix;
    private LuaFunction m_IsInTeam_hotfix;
    private LuaFunction m_IsInRoom_hotfix;
    private LuaFunction m_IsInWaitingList_hotfix;
    private LuaFunction m_CancelAutoMatchTeamRoom_hotfix;
    private LuaFunction m_CanJoinTeamRoomGameFunctionTypeInt32_hotfix;
    private LuaFunction m_CanQuitTeamRoom_hotfix;
    private LuaFunction m_CanGetTeamRoom_hotfix;
    private LuaFunction m_CanChangeTeamRoomPlayerPosition_hotfix;
    private LuaFunction m_CanInviteTeamRoom_hotfix;
    private LuaFunction m_InviteTeamRoom_hotfix;
    private LuaFunction m_SetTeamRoomInviteInfoTeamRoomInviteInfo_hotfix;
    private LuaFunction m_IsInvitedUInt64Int32_hotfix;
    private LuaFunction m_ClearAInviteInfoUInt64Int32_hotfix;
    private LuaFunction m_ClearTeamRoom_hotfix;
    private LuaFunction m_CanRefuseInvitationUInt64Int32_hotfix;
    private LuaFunction m_GetInviteInfos_hotfix;
    private LuaFunction m_DeductTeamPveBattleEnergyGameFunctionTypeInt32_hotfix;
    private LuaFunction m_CanGetInviteeInfoList`1_hotfix;
    private LuaFunction m_get_WaitingFunctionTypeId_hotfix;
    private LuaFunction m_get_WaitingLocationId_hotfix;
    private LuaFunction m_get_RoomId_hotfix;
    private LuaFunction m_get_GameFunctionTypeId_hotfix;
    private LuaFunction m_get_LocationId_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public TeamComponentCommon()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Tick(uint deltaMillisecond)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool Serialize<T>(T dest)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeSerialize<T>(T source)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostDeSerialize()
    {
      // ISSUE: unable to decompile the method.
    }

    public IComponentOwner Owner
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int IsLevelUnlocked(GameFunctionType gameFunctionTypeId, int locationId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanCreateTeam(ProTeamRoomSetting setting)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CreateTeam(int roomId, GameFunctionType gameFunctionTypeId, int locationId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsTeamRoomAuthorityValid(int authority)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAttackTeamGameFunction(GameFunctionType gameFunctionTypeId, int locationId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanViewTeamRoom(GameFunctionType gameFunctionTypeId, int chapterId, int locationId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAutoMatchTeamRoom(GameFunctionType gameFunctionTypeId, int locationId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AutoMatchTeamRoom(int roomId, GameFunctionType gameFunctionTypeId, int locationId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void JoinTeamRoom(int roomId, GameFunctionType gameFunctionTypeId, int locationId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void GetTeamRoomInfo(out GameFunctionType typeId, out int locationId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void QuitTeamRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanCancelAutoMatchTeamRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanChangeTeamRoomAuthority(int authority)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsInTeam()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsInRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsInWaitingList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CancelAutoMatchTeamRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanJoinTeamRoom(GameFunctionType gameFunctionTypeId, int locationId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanQuitTeamRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanGetTeamRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanChangeTeamRoomPlayerPosition()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int CanInviteTeamRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InviteTeamRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SetTeamRoomInviteInfo(TeamRoomInviteInfo info)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsInvited(ulong sessionId, int roomId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearAInviteInfo(ulong sessionId, int roomId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearTeamRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanRefuseInvitation(ulong sessionId, int roomId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<TeamRoomInviteInfo> GetInviteInfos()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeductTeamPveBattleEnergy(GameFunctionType gameFunctionTypeId, int locationId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanGetInviteeInfo(List<string> userIds)
    {
      // ISSUE: unable to decompile the method.
    }

    public GameFunctionType WaitingFunctionTypeId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int WaitingLocationId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int RoomId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public GameFunctionType GameFunctionTypeId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int LocationId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public TeamComponentCommon.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private TeamComponentCommon m_owner;

      public LuaExportHelper(TeamComponentCommon owner)
      {
        this.m_owner = owner;
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public DataSectionTeam m_teamDS
      {
        get
        {
          return this.m_owner.m_teamDS;
        }
        set
        {
          this.m_owner.m_teamDS = value;
        }
      }

      public BagComponentCommon m_bag
      {
        get
        {
          return this.m_owner.m_bag;
        }
        set
        {
          this.m_owner.m_bag = value;
        }
      }

      public AnikiGymComponentCommon m_anikiGym
      {
        get
        {
          return this.m_owner.m_anikiGym;
        }
        set
        {
          this.m_owner.m_anikiGym = value;
        }
      }

      public MemoryCorridorCompomentCommon m_memoryCorridor
      {
        get
        {
          return this.m_owner.m_memoryCorridor;
        }
        set
        {
          this.m_owner.m_memoryCorridor = value;
        }
      }

      public ThearchyTrialCompomentCommon m_thearchyTrial
      {
        get
        {
          return this.m_owner.m_thearchyTrial;
        }
        set
        {
          this.m_owner.m_thearchyTrial = value;
        }
      }

      public EternalShrineCompomentCommon m_eternalShrine
      {
        get
        {
          return this.m_owner.m_eternalShrine;
        }
        set
        {
          this.m_owner.m_eternalShrine = value;
        }
      }

      public PlayerBasicInfoComponentCommon m_basicInfo
      {
        get
        {
          return this.m_owner.m_basicInfo;
        }
        set
        {
          this.m_owner.m_basicInfo = value;
        }
      }

      public HeroTrainningComponentCommon m_heroTrainning
      {
        get
        {
          return this.m_owner.m_heroTrainning;
        }
        set
        {
          this.m_owner.m_heroTrainning = value;
        }
      }

      public CooperateBattleCompomentCommon m_cooperateBattle
      {
        get
        {
          return this.m_owner.m_cooperateBattle;
        }
        set
        {
          this.m_owner.m_cooperateBattle = value;
        }
      }

      public UnchartedScoreComponentCommon m_unchartedScore
      {
        get
        {
          return this.m_owner.m_unchartedScore;
        }
        set
        {
          this.m_owner.m_unchartedScore = value;
        }
      }

      public CollectionComponentCommon m_collectionActivity
      {
        get
        {
          return this.m_owner.m_collectionActivity;
        }
        set
        {
          this.m_owner.m_collectionActivity = value;
        }
      }

      public BattleComponentCommon m_battle
      {
        get
        {
          return this.m_owner.m_battle;
        }
        set
        {
          this.m_owner.m_battle = value;
        }
      }

      public bool IsTeamRoomAuthorityValid(int authority)
      {
        return this.m_owner.IsTeamRoomAuthorityValid(authority);
      }
    }
  }
}
