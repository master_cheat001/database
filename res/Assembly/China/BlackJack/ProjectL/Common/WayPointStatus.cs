﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.WayPointStatus
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

namespace BlackJack.ProjectL.Common
{
  public enum WayPointStatus
  {
    None = 0,
    Close = 1,
    Open = 2,
    Public = 4,
    Arrived = 8,
    NormalEvent = 16, // 0x00000010
    RandomEvent = 32, // 0x00000020
  }
}
