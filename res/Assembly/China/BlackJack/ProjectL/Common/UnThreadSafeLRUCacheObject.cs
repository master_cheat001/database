﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.UnThreadSafeLRUCacheObject
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class UnThreadSafeLRUCacheObject : CacheObject, ILRUCacheObject
  {
    private long m_lastReadTime;
    private long m_lastUpdateTime;

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLastUpdateTime()
    {
      // ISSUE: unable to decompile the method.
    }

    public long GetLastUpdateTime()
    {
      return this.m_lastUpdateTime;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLastReadTime()
    {
      // ISSUE: unable to decompile the method.
    }

    public long GetLastReadTime()
    {
      return this.m_lastReadTime;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public long GetNewestTime()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
