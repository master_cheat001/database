﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.GuildMember
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class GuildMember
  {
    public string UserId { get; set; }

    public string Name { get; set; }

    public int Level { get; set; }

    public GuildTitle Title { get; set; }

    public int TotalActivities { get; set; }

    public int ThisWeekActivities { get; set; }

    public bool Online { get; set; }

    public DateTime LogoutTime { get; set; }

    public int TopHeroBattlePower { get; set; }

    public int HeadIcon { get; set; }

    public void WeeklyFlush()
    {
      this.ThisWeekActivities = 0;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsAdmin()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProGuildMember ToPb(GuildMember m)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static GuildMember FromPb(ProGuildMember pb)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
