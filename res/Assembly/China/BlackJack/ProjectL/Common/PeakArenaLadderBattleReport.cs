﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.PeakArenaLadderBattleReport
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class PeakArenaLadderBattleReport : BattleReportHead
  {
    public List<PeakArenaBattleReportPlayerSummaryInfo> PlayerSummaryInfos;
    public List<int> WinPlayerIndexes;
    public List<string> FirstHandUserIds;
    public List<PeakArenaBattleReportBattleInfo> BattleInfos;

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaLadderBattleReport()
    {
      // ISSUE: unable to decompile the method.
    }

    public DateTime CreateTime { get; set; }

    public string ForfeitUserId { get; set; }

    public RealTimePVPMode Mode { get; set; }

    public int Round { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public override ProCommonBattleReport ToPb()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override ProCommonBattleReport ToPbSummary()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaBattleReportPlayerSummaryInfo GetPlayerSummaryInfoByBORound(
      int boRound,
      int leftOrRight)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaBattleReportPlayerSummaryInfo GetWinPlayerSummaryInfo(
      int boRound)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetBattleReportBoRoundCount()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
