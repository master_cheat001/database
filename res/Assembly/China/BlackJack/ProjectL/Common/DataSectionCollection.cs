﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionCollection
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class DataSectionCollection : DataSection
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionCollection()
    {
      // ISSUE: unable to decompile the method.
    }

    public override void ClearInitedData()
    {
      this.CollectionActivities.Clear();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivity FindCollectionActivity(ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveCollectionActivity(ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddCollectionActivity(CollectionActivity activity)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddChallengeLevelId(CollectionActivity acitivity, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddLootLevelId(CollectionActivity acitivity, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsChallengeLevelFinished(CollectionActivity acitivity, int levelId)
    {
      return acitivity.FinishedChallengeLevelIds.Contains(levelId);
    }

    public bool IsLootLevelFinished(CollectionActivity acitivity, int levelId)
    {
      return acitivity.FinishedLootLevelIds.Contains(levelId);
    }

    public void SetCurrentWayPointId(CollectionActivity acitivity, int waypointId)
    {
      acitivity.CurrentWayPointId = waypointId;
      this.SetDirty(true);
    }

    public void SetScenarioId(CollectionActivity acitivity, int id)
    {
      acitivity.LastFinishedScenarioId = id;
      this.SetDirty(true);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddScore(CollectionActivity acitivity, int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveCollectionEvents(List<CollectionEvent> Events, ulong activityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddCollectionEvents(List<CollectionEvent> Events, ulong activityId)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddExchangeCount(CollectionActivity activity, int exchangeID, int delta)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetExchangeCount(CollectionActivity activity, int exchangeID)
    {
      // ISSUE: unable to decompile the method.
    }

    public List<CollectionActivityPlayerExchangeInfo> GetExchangeInfoList(
      CollectionActivity activity)
    {
      return activity.ExchangeInfoList;
    }

    public List<CollectionActivity> CollectionActivities { get; set; }
  }
}
