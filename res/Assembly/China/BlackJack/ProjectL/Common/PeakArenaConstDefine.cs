﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.PeakArenaConstDefine
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class PeakArenaConstDefine
  {
    public const int NoRank = -2147483648;
    public const DayOfWeek StartDayOfWeek = DayOfWeek.Monday;
    public const int RankingRewardSingleOperationTimeThreshold = 2000;
    public const string PlayOffWinnerIdForfeited = "0";
    public const int TotalPlayOffRound = 8;
    public const int TotalPlayOffAttenders = 256;
    public const int MatchupInfoDataSendMaxCount = 20;
    public const int MatchupIndexEnd = -1;
    public const int MatchRoomIdUndefined = 0;
    public const string GMClearWinnerId = "";
  }
}
