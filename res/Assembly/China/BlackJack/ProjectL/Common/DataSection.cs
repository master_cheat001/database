﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSection
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  [Serializable]
  public class DataSection
  {
    protected ushort m_dbVersion;
    protected ushort m_clientVersion;
    protected ushort m_dbCommitedVersion;
    protected ushort m_clientCommitedVersion;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSection()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitVersion(ushort dbVersion, ushort clientVersion)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDirty(bool needCommit2Client = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool NeedSyncToDB()
    {
      // ISSUE: unable to decompile the method.
    }

    public void OnDBSynced()
    {
      this.m_dbCommitedVersion = this.m_dbVersion;
    }

    public void SetClientCommitedVersion(ushort version)
    {
      this.m_clientCommitedVersion = version;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool NeedSyncToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    public void OnClientSynced()
    {
      this.m_clientCommitedVersion = this.m_clientVersion;
    }

    public virtual object SerializeToClient()
    {
      return (object) null;
    }

    public virtual void ClearInitedData()
    {
    }

    public ushort Version
    {
      get
      {
        return this.m_dbVersion;
      }
    }

    public ushort ClientVersion
    {
      get
      {
        return this.m_clientVersion;
      }
    }

    public ushort ClientCommitedVersion
    {
      get
      {
        return this.m_clientCommitedVersion;
      }
    }
  }
}
