﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionComment
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class DataSectionComment : DataSection
  {
    public DateTime m_bannedTime;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionComment()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void ClearInitedData()
    {
      // ISSUE: unable to decompile the method.
    }

    public void InitPlayerHeroCommentEntry(PlayerHeroCommentEntry playerCommentEntry)
    {
      this.m_playerHeroCommentEntries.Add(playerCommentEntry);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PlayerHeroCommentEntry FindPlayerHeroCommentEntry(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PraiseHeroComment(int heroId, ulong commentInstanceId)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CommentHero(int heroId, HeroCommentEntry commentEntry)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBanned(DateTime currentTime)
    {
      // ISSUE: unable to decompile the method.
    }

    public void Ban(DateTime bannedTime)
    {
      this.m_bannedTime = bannedTime;
      this.SetDirty(true);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Unban()
    {
      // ISSUE: unable to decompile the method.
    }

    public List<PlayerHeroCommentEntry> m_playerHeroCommentEntries { get; private set; }
  }
}
