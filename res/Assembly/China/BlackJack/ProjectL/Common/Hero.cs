﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.Hero
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Protocol;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class Hero
  {
    private int m_heroId;
    public List<int> SkillIds;
    public List<int> SoldierIds;
    public List<int> SelectedSkills;
    public List<int> NeedGetAchievementsJobRelatedIdList;
    public ulong[] EquipmentIds;
    public Dictionary<int, int> Fetters;
    private int m_selectedSoldierId;
    public List<HeroJob> Jobs;
    public List<int> UnlockedJobs;
    public List<SoldierSkin> SoldierSkins;

    [MethodImpl((MethodImplOptions) 32768)]
    public Hero()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Hero(Hero other)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Hero GetHeroCopy()
    {
      // ISSUE: unable to decompile the method.
    }

    public int ActionPositionIndex { get; set; }

    public int ActionValue { get; set; }

    public int HeroId
    {
      set
      {
        this.m_heroId = value;
        this.UpdateHeroInfo();
      }
      get
      {
        return this.m_heroId;
      }
    }

    public int Level { get; set; }

    public int Exp { get; set; }

    public int StarLevel { get; set; }

    public int FightNums { get; set; }

    public int ActiveHeroJobRelatedId { get; set; }

    public int CharSkinId { get; set; }

    public int FavorabilityLevel { get; set; }

    public int FavorabilityExp { get; set; }

    public bool Confessed { get; set; }

    public int HeartFetterLevel { get; set; }

    public DateTime HeroHeartFetterUnlockTime { get; set; }

    public DateTime HeroHeartFetterLevelMaxTime { get; set; }

    public bool Mine { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroFetterTotalLevel()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsHeroHeartFetterLocked()
    {
      return this.HeartFetterLevel == 0;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsEquipmentWeared(int slot)
    {
      // ISSUE: unable to decompile the method.
    }

    public void TakeOffEquipment(int slot)
    {
      this.EquipmentIds[slot] = 0UL;
    }

    public bool HasOwnSoldier(int soldierId)
    {
      return this.SoldierIds.Contains(soldierId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetSoldierSkinId(int soldierId)
    {
      // ISSUE: unable to decompile the method.
    }

    public int SelectedSoldierId
    {
      set
      {
        this.m_selectedSoldierId = value;
        this.UpdateSoldierInfo();
      }
      get
      {
        return this.m_selectedSoldierId;
      }
    }

    public int CanTransferMaxRank { get; set; }

    public int BattlePower { get; set; }

    public DateTime BattlePowerUpdateTime { get; set; }

    public int LastRank { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroJob GetJob(int jobRelatedId)
    {
      // ISSUE: unable to decompile the method.
    }

    public HeroJob GetActiveJob()
    {
      return this.GetJob(this.ActiveHeroJobRelatedId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProHero HeroToPBHero(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Hero PBHeroToHero(ProHero pbHero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateHeroInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateSoldierInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCharImageSkinResourceInfo GetHeroCharImageSkinResourceInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataModelSkinResourceInfo GetSoldierModelSkinResourceInfo(
      int soldierId)
    {
      // ISSUE: unable to decompile the method.
    }

    public ConfigDataHeroInfo HeroInfo { private set; get; }

    public ConfigDataSoldierInfo SelectedSoldierInfo { private set; get; }
  }
}
