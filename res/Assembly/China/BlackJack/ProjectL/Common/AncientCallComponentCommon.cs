﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.AncientCallComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class AncientCallComponentCommon : IComponentBase
  {
    protected DataSectionAncientCall m_ancientCallDS;
    protected IConfigDataLoader m_configDataLoader;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected BattleComponentCommon m_battle;
    protected HeroComponentCommon m_hero;

    [MethodImpl((MethodImplOptions) 32768)]
    public AncientCallComponentCommon()
    {
      // ISSUE: unable to decompile the method.
    }

    public string GetName()
    {
      return "AncientCallComponent";
    }

    public virtual void Init()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    public virtual void DeInit()
    {
    }

    public virtual void Tick(uint deltaMillisecond)
    {
    }

    public virtual bool Serialize<T>(T dest)
    {
      return false;
    }

    public virtual void DeSerialize<T>(T source)
    {
    }

    public virtual void PostDeSerialize()
    {
    }

    public IComponentOwner Owner { get; set; }

    public bool IsCurrentPeriodExist()
    {
      return this.m_ancientCallDS.IsCurrentPeriodExist();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnFlush()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ConfigDataAncientCallPeriodInfo GetCurrentPeriodInfo(
      DateTime currentTime,
      IConfigDataLoader configDataLoader)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public AncientCallPeriod GetCurrentAncientCallPeriod()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetCurrentBossId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool SetCommonBossSuccess(
      int bossId,
      int damage,
      List<int> teamList,
      DateTime updateTime,
      List<int> newAchievementRelationIds)
    {
      // ISSUE: unable to decompile the method.
    }

    public int GetBossCurrentPeriodMaxDamage(int bossId)
    {
      return this.m_ancientCallDS.GetBossCurrentPeriodMaxDamage(bossId);
    }

    public int GetCurrentPeriodScore()
    {
      return this.m_ancientCallDS.GetCurrentPeriodScore();
    }

    public AncientCallBoss FindBossById(int bossId)
    {
      return this.m_ancientCallDS.FindBossById(bossId);
    }

    public AncientCall GetAncientCallInfo()
    {
      return this.m_ancientCallDS.AncientCallInfo;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCurrentPeriodInfo(int periodId, int bossIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCurrentBoss(int currentBossIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AttackAncientCallBoss(int bossId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int CanAttackAncientCallBoss(int bossId)
    {
      // ISSUE: unable to decompile the method.
    }

    private bool IsGameFunctionOpened()
    {
      return this.m_basicInfo.IsGameFunctionOpened(GameFunctionType.GameFunctionType_AncientCall);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasGotAchievementRelationId(int achievementId)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> GetAncientCallBossAchievementMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
