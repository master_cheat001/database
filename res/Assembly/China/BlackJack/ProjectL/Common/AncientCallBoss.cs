﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.AncientCallBoss
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class AncientCallBoss
  {
    public List<int> TeamList;

    [MethodImpl((MethodImplOptions) 32768)]
    public AncientCallBoss(int bossId)
    {
      // ISSUE: unable to decompile the method.
    }

    public int BossId { get; set; }

    public int CurrentPeriodMaxDamage { get; set; }

    public int CurrentMaxDamage { get; set; }

    public int TeamListBattlePower { get; set; }

    public DateTime UpdateTime { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProAncientCallBoss ToPB(AncientCallBoss boss)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static AncientCallBoss FromPB(ProAncientCallBoss pbBoss)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool UpdateDamage(
      int damage,
      List<int> teamList,
      DateTime updateTime,
      int teamListBattlePower)
    {
      // ISSUE: unable to decompile the method.
    }

    public void Reset()
    {
      this.CurrentMaxDamage = 0;
    }
  }
}
