﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.PeakArenaBattleReportBattleInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class PeakArenaBattleReportBattleInfo
  {
    public List<PeakArenaBattleReportBattleInitInfo> BattleInitInfos;
    public List<BPStageCommand> BPCommands;
    public List<BattleCommand> BattleCommands;

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaBattleReportBattleInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    public int RandomSeed { get; set; }

    public int BattleId { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProPeakArenaBattleReportBattleInfo ToPb(
      PeakArenaBattleReportBattleInfo info)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static PeakArenaBattleReportBattleInfo FromPb(
      ProPeakArenaBattleReportBattleInfo info)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
