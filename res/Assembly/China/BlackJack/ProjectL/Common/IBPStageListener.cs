﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.IBPStageListener
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System.Collections.Generic;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public interface IBPStageListener
  {
    BPStage GetBPStage();

    void OnExecuteCommand(int playerIndex, BPStageCommand command);

    void OnBPFinish(bool early);

    void OnBan(int playerIndex, int currentTurn, List<int> actorIds);

    void OnPick(int playerIndex, int currentTurn, List<int> actorIds);
  }
}
