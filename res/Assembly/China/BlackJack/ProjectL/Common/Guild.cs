﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.Guild
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class Guild
  {
    public List<GuildLog> Logs;
    public GuildMassiveCombatGeneral MassiveCombat;
    public static int GuildMemberCountMax;

    [MethodImpl((MethodImplOptions) 32768)]
    public Guild()
    {
      // ISSUE: unable to decompile the method.
    }

    public string Id { get; set; }

    public string Name { get; set; }

    public string Bulletin { get; set; }

    public string HiringDeclaration { get; set; }

    public bool AutoJoin { get; set; }

    public int JoinLevel { get; set; }

    public int TotalBattlePower { get; set; }

    public int TotalActivities { get; set; }

    public int LastWeekActivities { get; set; }

    public int CurrentWeekActivities { get; set; }

    public int Activities { get; set; }

    public DateTime GuildNextWeekFlushTime { get; set; }

    public List<GuildMemberCacheObject> Members { get; set; }

    public bool IsMemberFull
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool WeeklyFlush(DateTime nextWeekFlushTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GuildMemberCacheObject AddMember(GuildMember member)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GuildMemberCacheObject FindMember(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveMember(GuildMemberCacheObject cache)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsEmpty()
    {
      return this.Members.Count == 0;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GuildMemberCacheObject GetPresident()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetVicePresidentNums()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<string> GetMemberUserIds()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<string> GetAdminUserIds()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProGuild ToPb(Guild g)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Guild FromPb(ProGuild pb)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
