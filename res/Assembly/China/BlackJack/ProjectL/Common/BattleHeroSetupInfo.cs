﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.BattleHeroSetupInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class BattleHeroSetupInfo
  {
    public int PlayerIndex;
    public int Position;
    private BattleHero m_hero;
    public SetupBattleHeroFlag Flag;
    public BattleHero PrevHero;

    public BattleHero Hero
    {
      get
      {
        return this.m_hero;
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool HasFlag(SetupBattleHeroFlag flag)
    {
      return (this.Flag & flag) != SetupBattleHeroFlag.None;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProBattleHeroSetupInfo BattleHeroSetupInfoToPbProBattleHeroSetupInfo(
      BattleHeroSetupInfo info)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static BattleHeroSetupInfo PbBattleHeroSetupInfoToProBattleHeroSetupInfo(
      ProBattleHeroSetupInfo pbInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleHeroSetupInfo Copy()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
