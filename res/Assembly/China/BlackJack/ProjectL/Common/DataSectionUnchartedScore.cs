﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionUnchartedScore
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class DataSectionUnchartedScore : DataSection
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionUnchartedScore()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    public override void ClearInitedData()
    {
      this.UnchartedScores.Clear();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveUnchartedScore(int unchartedScoreId)
    {
      // ISSUE: unable to decompile the method.
    }

    public Dictionary<int, UnchartedScore> UnchartedScores { get; set; }
  }
}
