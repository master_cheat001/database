﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.ChatComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using SLua;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class ChatComponentCommon : IComponentBase
  {
    protected DataSectionChat m_chatDS;
    protected IConfigDataLoader m_configDataLoader;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected GuildComponentCommon m_guild;

    public string GetName()
    {
      return "Chat";
    }

    public virtual void Init()
    {
      this.m_chatDS = new DataSectionChat();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    public virtual void DeInit()
    {
    }

    public virtual void Tick(uint deltaMillisecond)
    {
    }

    public virtual bool Serialize<T>(T dest)
    {
      return false;
    }

    public virtual void DeSerialize<T>(T source)
    {
    }

    public virtual void PostDeSerialize()
    {
    }

    public IComponentOwner Owner { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int CanSendChatMessage(int channelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string TruncateChatMessage(string content)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBanned()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Ban(DateTime bannedTime)
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual void OnBan()
    {
    }

    protected virtual void OnUnBan()
    {
    }
  }
}
