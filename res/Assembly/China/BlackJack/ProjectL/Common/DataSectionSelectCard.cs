﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionSelectCard
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class DataSectionSelectCard : DataSection
  {
    private int m_guaranteedAccumulatedValue;
    private int m_guaranteedSelectCardCount;
    private SelectCardGuaranteedStatus m_selectCardGuaranteedStatus;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionSelectCard()
    {
      // ISSUE: unable to decompile the method.
    }

    public override void ClearInitedData()
    {
      this.CardPools.Clear();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitCardPools(List<CardPool> cardPools)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetCardPools(List<CardPool> cardPools)
    {
      this.InitCardPools(cardPools);
      this.SetDirty(true);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanTenSelectDiscount(CardPool cardPool)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CardPool FindCardPool(int cardPoolId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveCardPool(int cardPoolId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CardPool AddCardPool(CardPool newCardPool)
    {
      // ISSUE: unable to decompile the method.
    }

    public void AddSelectCount(CardPool cardPool, int count = 1)
    {
      cardPool.SelectCardCount += count;
    }

    public void SummonRareHero(CardPool cardPool)
    {
      cardPool.SummonedRareHero = true;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddGuaranteedSelectCount(int count)
    {
      // ISSUE: unable to decompile the method.
    }

    public void AddTenSelectDiscountCount(CardPool cardPool)
    {
      ++cardPool.DisconnectCount;
    }

    public bool IsFirstSingleSelectCard(CardPool cardPool)
    {
      return cardPool.IsFirstSignleSelect;
    }

    public bool IsFirstTenSelectCard(CardPool cardPool)
    {
      return cardPool.IsFirstTenSelect;
    }

    public bool IsFirstSelectCard(CardPool cardPool)
    {
      return cardPool.SelectCardCount == 0;
    }

    public void FinishFirstSingleSelect(CardPool cardPool)
    {
      cardPool.IsFirstSignleSelect = false;
    }

    public void FinishFirstTenSelect(CardPool cardPool)
    {
      cardPool.IsFirstTenSelect = false;
    }

    public void SetGuaranteedAccumulatedValue(int value)
    {
      this.m_guaranteedAccumulatedValue = value;
      this.SetDirty(true);
    }

    public void InitGuaranteedAccumulatedValue(int value)
    {
      this.m_guaranteedAccumulatedValue = value;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsReachGuaranteedAccumulatedValue(int value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsAboveGuaranteedAccumulatedValue()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsCardPoolExistGuaranteedMechanism()
    {
      return this.m_guaranteedAccumulatedValue != 0;
    }

    public void SetGuaranteedSelectCardCount(int count)
    {
      this.m_guaranteedSelectCardCount = count;
      this.SetDirty(true);
    }

    public void InitGuaranteedSelectCardCount(int count)
    {
      this.m_guaranteedSelectCardCount = count;
    }

    public void InitSelectCardGuaranteedStatus(SelectCardGuaranteedStatus status)
    {
      this.m_selectCardGuaranteedStatus = status;
    }

    public void SetSelectCardGuaranteedStatus(SelectCardGuaranteedStatus status)
    {
      this.m_selectCardGuaranteedStatus = status;
      this.SetDirty(true);
    }

    public Dictionary<int, CardPool> CardPools { get; set; }

    public int GuaranteedAccumulatedValue
    {
      get
      {
        return this.m_guaranteedAccumulatedValue;
      }
    }

    public int GuaranteedSelectCardCount
    {
      get
      {
        return this.m_guaranteedSelectCardCount;
      }
    }

    public SelectCardGuaranteedStatus GuaranteedStatus
    {
      get
      {
        return this.m_selectCardGuaranteedStatus;
      }
    }
  }
}
