﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.PeakArenaBattleReportPlayerSummaryInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class PeakArenaBattleReportPlayerSummaryInfo
  {
    public string Name { get; set; }

    public string UserId { get; set; }

    public int HeadIcon { get; set; }

    public int Dan { get; set; }

    public int Level { get; set; }

    public bool Preranking { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProPeakArenaBattleReportPlayerSummaryInfo ToPb(
      PeakArenaBattleReportPlayerSummaryInfo info)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static PeakArenaBattleReportPlayerSummaryInfo FromPb(
      ProPeakArenaBattleReportPlayerSummaryInfo info)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
