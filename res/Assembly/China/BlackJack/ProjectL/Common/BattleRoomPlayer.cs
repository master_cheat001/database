﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.BattleRoomPlayer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class BattleRoomPlayer
  {
    public List<BattleHero> Heroes;
    public List<TrainingTech> Techs;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleRoomPlayer()
    {
      // ISSUE: unable to decompile the method.
    }

    public string UserId { get; set; }

    public ulong SessionId { get; set; }

    public int ChannelId { get; set; }

    public string Name { get; set; }

    public int HeadIcon { get; set; }

    public int Level { get; set; }

    public RealTimePVPInfo RTPVPInfo { get; set; }

    public bool IsOffline { get; set; }

    public PlayerBattleStatus PlayerBattleStatus { get; set; }

    public bool Blessing { get; set; }

    public long PublicTimeSpan { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleHero FindBattleHero(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProBattleRoomPlayer BattleRoomPlayerToPbBattleRoomPlayer(
      BattleRoomPlayer player)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static BattleRoomPlayer PbBattleRoomPlayerToBattleRoomPlayer(
      ProBattleRoomPlayer pbPlayer)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProPeakArenaBattleReportPlayerSummaryInfo ToPeakArenaBattleReportPlayerSummaryInfo(
      BattleRoomPlayer player)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
