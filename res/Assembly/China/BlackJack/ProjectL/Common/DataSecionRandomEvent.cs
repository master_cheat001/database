﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSecionRandomEvent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class DataSecionRandomEvent : DataSection
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public DataSecionRandomEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void ClearInitedData()
    {
      // ISSUE: unable to decompile the method.
    }

    public void InitDefiniteLevelZone(RandomEventLevelZone zone)
    {
      this.DefiniteLevelZone = zone;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitRandomLevelZone(List<RandomEventLevelZone> zones)
    {
      // ISSUE: unable to decompile the method.
    }

    public void InitGenerateEventTime(List<int> timeList)
    {
      this.RandomEventTimeList = new List<int>((IEnumerable<int>) timeList);
    }

    public void InitGenerateRandomEventTotalCount(int count)
    {
      this.GenerateRandomEventTotalCount = count;
    }

    public void InitRandomEventTotalCount(int count)
    {
      this.RandomEventTotalCount = count;
    }

    public void SetDefiniteEventLevelZone(RandomEventLevelZone zone)
    {
      this.DefiniteLevelZone = zone;
      this.SetDirty(true);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DefiniteGroupGenerateRandomEvent(int groupId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RandomGroupGenerateRandomEvent(int levelZoneId, int groupId, int maxCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddRandomEventLevelZone(int index, RandomEventLevelZone zone)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetRandomEventTotalCount(int count)
    {
      this.RandomEventTotalCount = count;
      this.SetDirty(true);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRandomEventTimes(List<int> timeList)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetNextRandomEventFlushTime(DateTime setTime)
    {
      this.NextRandomEventFlushTime = setTime;
      this.SetDirty(true);
    }

    public void SetDefiniteEventMaxCount(int maxCount)
    {
      this.DefiniteEventMaxCount = maxCount;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddRandomEventCount(int count = 1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Dictionary<int, RandomEventGroup> GetDefiniteGroupRandomEventGroups()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddDefiniteGroupRandomEventGroup(RandomEventGroup newGroup)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Dictionary<int, RandomEventGroup> GetRandomGroupLevelZoneEventGroups(
      int levelZoneId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearRandomInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveRandomEventTime(int time)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCompletedRandomEventGenerate()
    {
      // ISSUE: unable to decompile the method.
    }

    public int RandomEventTotalCount { get; set; }

    public int GenerateRandomEventTotalCount { get; set; }

    public int DefiniteEventMaxCount { get; set; }

    public DateTime NextRandomEventFlushTime { get; set; }

    public RandomEventLevelZone DefiniteLevelZone { get; set; }

    public Dictionary<int, RandomEventLevelZone> RandomLevelZone { get; set; }

    public List<int> RandomEventTimeList { get; set; }
  }
}
