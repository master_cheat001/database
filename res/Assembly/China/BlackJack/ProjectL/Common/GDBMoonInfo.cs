﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.GDBMoonInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Common
{
  [ProtoContract(Name = "GDBMoonInfo")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class GDBMoonInfo : IExtensible
  {
    private int _Id;
    private string _name;
    private double _locationX;
    private double _locationZ;
    private uint _radius;
    private ulong _orbitRadius;
    private double _orbitalPeriod;
    private float _eccentricity;
    private double _mass;
    private float _gravity;
    private float _escapeVelocity;
    private uint _temperature;
    private uint _artResourcesId;
    private bool _isNeedLocalization;
    private string _localizationKey;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public GDBMoonInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Id")]
    public int Id
    {
      get
      {
        return this._Id;
      }
      set
      {
        this._Id = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = false, Name = "name")]
    [DefaultValue("")]
    public string Name
    {
      get
      {
        return this._name;
      }
      set
      {
        this._name = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "locationX")]
    public double LocationX
    {
      get
      {
        return this._locationX;
      }
      set
      {
        this._locationX = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "locationZ")]
    public double LocationZ
    {
      get
      {
        return this._locationZ;
      }
      set
      {
        this._locationZ = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "radius")]
    public uint Radius
    {
      get
      {
        return this._radius;
      }
      set
      {
        this._radius = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "orbitRadius")]
    public ulong OrbitRadius
    {
      get
      {
        return this._orbitRadius;
      }
      set
      {
        this._orbitRadius = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "orbitalPeriod")]
    public double OrbitalPeriod
    {
      get
      {
        return this._orbitalPeriod;
      }
      set
      {
        this._orbitalPeriod = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.FixedSize, IsRequired = true, Name = "eccentricity")]
    public float Eccentricity
    {
      get
      {
        return this._eccentricity;
      }
      set
      {
        this._eccentricity = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "mass")]
    public double Mass
    {
      get
      {
        return this._mass;
      }
      set
      {
        this._mass = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.FixedSize, IsRequired = true, Name = "gravity")]
    public float Gravity
    {
      get
      {
        return this._gravity;
      }
      set
      {
        this._gravity = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.FixedSize, IsRequired = true, Name = "escapeVelocity")]
    public float EscapeVelocity
    {
      get
      {
        return this._escapeVelocity;
      }
      set
      {
        this._escapeVelocity = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "temperature")]
    public uint Temperature
    {
      get
      {
        return this._temperature;
      }
      set
      {
        this._temperature = value;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "artResourcesId")]
    public uint ArtResourcesId
    {
      get
      {
        return this._artResourcesId;
      }
      set
      {
        this._artResourcesId = value;
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.Default, IsRequired = true, Name = "isNeedLocalization")]
    public bool IsNeedLocalization
    {
      get
      {
        return this._isNeedLocalization;
      }
      set
      {
        this._isNeedLocalization = value;
      }
    }

    [DefaultValue("")]
    [ProtoMember(15, DataFormat = DataFormat.Default, IsRequired = false, Name = "localizationKey")]
    public string LocalizationKey
    {
      get
      {
        return this._localizationKey;
      }
      set
      {
        this._localizationKey = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
