﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.UseableBagItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;

namespace BlackJack.ProjectL.Common
{
  public abstract class UseableBagItem : BagItemBase
  {
    public UseableBagItem(GoodsType goodsTypeId, int contentId, int nums, ulong instanceId)
      : base(goodsTypeId, contentId, nums, instanceId)
    {
    }

    public abstract int HaveEffect(IComponentOwner owner, params object[] param);
  }
}
