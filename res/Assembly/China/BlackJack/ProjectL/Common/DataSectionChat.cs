﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionChat
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class DataSectionChat : DataSection
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionChat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    public void InitBannedTime(long bannedTime)
    {
      this.m_bannedTime = new DateTime(bannedTime);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBanned(DateTime currentTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBannedTimeExpired(DateTime current)
    {
      // ISSUE: unable to decompile the method.
    }

    public void Ban(DateTime bannedTime)
    {
      this.m_bannedTime = bannedTime;
      this.SetDirty(true);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Unban()
    {
      // ISSUE: unable to decompile the method.
    }

    public void InitSilentBannedTime(long bannedTime)
    {
      this.m_silentlyBannedTime = new DateTime(bannedTime);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsSilentlyBanned(DateTime currentTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsSilentBannedTimeExpired(DateTime current)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SilentBan(DateTime bannedTime)
    {
      this.m_silentlyBannedTime = bannedTime;
      this.SetDirty(true);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SilentUnban()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanSendWorldChatByTime(int intervalTime, DateTime currentTime)
    {
      // ISSUE: unable to decompile the method.
    }

    public DateTime m_bannedTime { get; private set; }

    public DateTime m_silentlyBannedTime { get; private set; }

    public DateTime LastWorldChannelChatSendTime { get; set; }
  }
}
