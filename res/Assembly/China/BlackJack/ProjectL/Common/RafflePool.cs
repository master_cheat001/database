﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.RafflePool
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Protocol;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class RafflePool
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public RafflePool(int poolId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<ProRafflePool> RafflePools2PbActivityRafflePools(
      List<RafflePool> rafflePools)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static ProRafflePool RafflePool2PbRafflePool(RafflePool rafflePool)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static RafflePool PBRafflePoolToRafflePool(ProRafflePool pbRafflePool)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<RafflePool> PBRafflePoolsToRafflePools(
      List<ProRafflePool> pbRafflePools)
    {
      // ISSUE: unable to decompile the method.
    }

    public void Drawed(int darwedRaffleId)
    {
      this.DrawedRaffleIds.Add(darwedRaffleId);
    }

    public ulong ActivityInstanceId { get; set; }

    public int PoolId { get; set; }

    public HashSet<int> DrawedRaffleIds { get; set; }

    public int DrawedCount { get; set; }

    public int FreeDrawedCount { get; set; }

    public ConfigDataRafflePoolInfo Config { get; set; }
  }
}
