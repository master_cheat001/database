﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.FriendComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class FriendComponentCommon : IComponentBase
  {
    protected IConfigDataLoader m_configDataLoader;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected HeroComponentCommon m_hero;
    protected BagComponentCommon m_bag;
    protected DataSectionFriend m_friendDS;

    [MethodImpl((MethodImplOptions) 32768)]
    public FriendComponentCommon()
    {
      // ISSUE: unable to decompile the method.
    }

    public string GetName()
    {
      return "Friend";
    }

    public virtual void Init()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    public virtual void DeInit()
    {
    }

    public virtual void Tick(uint deltaMillisecond)
    {
    }

    public virtual bool Serialize<T>(T dest)
    {
      return false;
    }

    public virtual void DeSerialize<T>(T source)
    {
    }

    public virtual void PostDeSerialize()
    {
    }

    public IComponentOwner Owner { get; set; }

    private void OnFlushFriend()
    {
      this.m_friendDS.OnFlushOfTime();
    }

    public virtual void Ban(DateTime banTime)
    {
      this.m_friendDS.Ban(banTime);
    }

    public bool IsBanned(DateTime currentTime)
    {
      return this.m_friendDS.IsBanned(currentTime);
    }

    public void SetBusinessCardInfo(BusinessCardInfoSet setInfo)
    {
      this.m_friendDS.SetBusinessCardInfo(setInfo);
    }

    public void SetLikes(int likes)
    {
      this.m_friendDS.SetLikes(likes);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanSendLikes(string targetUserId)
    {
      // ISSUE: unable to decompile the method.
    }

    public void AddLikedUser(string userId)
    {
      this.m_friendDS.AddLikedUser(userId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int SendFriendshipPointsCheck(string targetUserId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<string> FilterInvalidSendFriendPointsTargets(List<string> targetUserIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int SendFriendshipPoints(string targetUserId)
    {
      // ISSUE: unable to decompile the method.
    }

    public virtual int ReceiveFriendshipPointsFromFriend(string targetUserId)
    {
      this.m_friendDS.AddFriendshipPointsReceivedUser(targetUserId);
      return 0;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ClaimFriendshipPointsFromFriendCheck(string targetUserId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int ClaimFriendshipPointsFromFriend(string targetUserId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClaimFriendShipPoints(int point)
    {
      // ISSUE: unable to decompile the method.
    }

    public int GetFriendshipPointsFromFightWithFriendsToday()
    {
      return this.m_friendDS.FriendshipPointsFromFightWithFriendsToday;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddFriendshipPointsFromFightWithFriendsToday(int pointsToAdd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAddHeroicMomentBattleReport(ulong battleReportId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddHeroicMomentBattleReport(ulong battleReportId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int RemoveHeroicMomentBattleReport(ulong battleReportId)
    {
      // ISSUE: unable to decompile the method.
    }

    public List<ulong> GetHeroicMomentBattleReports()
    {
      return this.m_friendDS.HeroicMomentBattleReports;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanUpdateBusinessCardDesc(string desc)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanUpdateBusinessCardHeroSets(List<BusinessCardHeroSet> heroSets)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsMySelf(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool HasSendLikesToday(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasSentFriendShipPoints(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasReceivedFriendShipPoints(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetBusinessCardDesc(string desc)
    {
      this.m_friendDS.SetBusinessCardDesc(desc);
    }

    public void SetBusinessCardHeroSet(List<BusinessCardHeroSet> heroSets)
    {
      this.m_friendDS.SetBusinessCardHeroSet(heroSets);
    }

    public void SetRandomHeroAction(bool actionRandom)
    {
      this.m_friendDS.SetRandomHeroAction(actionRandom);
    }

    public void RemoveFriendshipPointsReceivedUser(string UserId)
    {
      this.m_friendDS.RemoveFriendshipPointsReceivedUser(UserId);
    }

    public List<string> GetFriendshipPointsReceivedUsers()
    {
      return this.m_friendDS.FriendshipPointsReceived;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnInviteFriendSuccess()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action InviteFriendMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
