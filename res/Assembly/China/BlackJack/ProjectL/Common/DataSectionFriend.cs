﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionFriend
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class DataSectionFriend : DataSection
  {
    public List<string> LikedUsers;
    public List<string> FriendshipPointsSent;
    public List<string> FriendshipPointsReceived;
    public List<ulong> HeroicMomentBattleReports;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionFriend()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void ClearInitedData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetBusinessCardInfo(BusinessCardInfoSet setInfo)
    {
      this.BusinessCardSetInfo = setInfo;
      this.SetDirty(true);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnFlushOfTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearLikedUsers()
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetLikes(int likes)
    {
      this.Likes = likes;
      this.SetDirty(true);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddLikes(int addValue = 1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddLikedUser(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddFriendshipPointsReceivedUser(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool RemoveFriendshipPointsReceivedUser(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool RemoveFriendshipPointsSentUser(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddFriendshipPointsSentUser(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBusinessCardDesc(string desc)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBusinessCardHeroSet(List<BusinessCardHeroSet> heroSets)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRandomHeroAction(bool actionRandom)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetFriendshipPointsFromFightWithFriendsToday(int points)
    {
      this.FriendshipPointsFromFightWithFriendsToday = points;
      this.SetDirty(true);
    }

    public void SetFriendshipPointsClaimedToday(int times)
    {
      this.FriendshipPointsClaimedToday = times;
      this.SetDirty(true);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBanned(DateTime currentTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Ban(DateTime banTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddHeroicMomentBattleReport(ulong battleReportId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveHeroicMomentBattleReport(ulong battleReportId)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool ExistHeroicMomentBattleReport(ulong battleReportId)
    {
      return this.HeroicMomentBattleReports.Contains(battleReportId);
    }

    public DateTime BannedTime { get; set; }

    public BusinessCardInfoSet BusinessCardSetInfo { get; set; }

    public int Likes { get; set; }

    public int FriendshipPointsFromFightWithFriendsToday { get; set; }

    public int FriendshipPointsClaimedToday { get; set; }
  }
}
