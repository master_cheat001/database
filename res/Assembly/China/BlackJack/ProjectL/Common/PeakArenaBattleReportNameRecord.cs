﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.PeakArenaBattleReportNameRecord
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class PeakArenaBattleReportNameRecord
  {
    public ulong InstanceId { get; set; }

    public string Name { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProPeakArenaBattleReportNameRecord ToProtocol(
      PeakArenaBattleReportNameRecord record)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<ProPeakArenaBattleReportNameRecord> ToProtocols(
      List<PeakArenaBattleReportNameRecord> records)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static PeakArenaBattleReportNameRecord FromProtocol(
      ProPeakArenaBattleReportNameRecord pbRecord)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<PeakArenaBattleReportNameRecord> FromProtocols(
      List<ProPeakArenaBattleReportNameRecord> pbRecords)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
