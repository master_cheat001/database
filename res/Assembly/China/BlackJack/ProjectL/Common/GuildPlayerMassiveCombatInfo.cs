﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.GuildPlayerMassiveCombatInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class GuildPlayerMassiveCombatInfo
  {
    public List<int> UsedHeroIds;

    [MethodImpl((MethodImplOptions) 32768)]
    public GuildPlayerMassiveCombatInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    public string BindedGuildId { get; set; }

    public int Points { get; set; }
  }
}
