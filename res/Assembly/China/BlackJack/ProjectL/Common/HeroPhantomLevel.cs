﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.HeroPhantomLevel
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class HeroPhantomLevel
  {
    public DateTime FirstClear;
    public List<int> AchievementIdsFinished;
    public HeroPhantom WhichPhantom;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroPhantomLevel()
    {
      // ISSUE: unable to decompile the method.
    }

    public int ID { get; set; }

    public string Name
    {
      get
      {
        return this.Config.Name;
      }
    }

    public int PreLevel
    {
      get
      {
        return this.Config.PreLevel;
      }
    }

    public int EnergySuccess
    {
      get
      {
        return this.Config.EnergySuccess;
      }
    }

    public int EnergyFail
    {
      get
      {
        return this.Config.EnergyFail;
      }
    }

    public int MonsterLevel
    {
      get
      {
        return this.Config.MonsterLevel;
      }
    }

    public ConfigDataBattleInfo BattleInfo
    {
      get
      {
        return this.Config.m_battleInfo;
      }
    }

    public int UserExp
    {
      get
      {
        return this.Config.PlayerExp;
      }
    }

    public int HeroExp
    {
      get
      {
        return this.Config.HeroExp;
      }
    }

    public int GoldBonus
    {
      get
      {
        return this.Config.Gold;
      }
    }

    public int RandomDropID
    {
      get
      {
        return this.Config.DropID;
      }
    }

    public List<Goods> FirstClearDropItems
    {
      get
      {
        return this.Config.FirstClearDropItems;
      }
    }

    public BattleLevelAchievement[] Achievements
    {
      get
      {
        return this.Config.m_achievements;
      }
    }

    public IConfigDataLoader ConfigDataLoader { get; set; }

    public ConfigDataHeroPhantomLevelInfo Config
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool Cleared
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsAvailableForChallenge
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
