﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.CommonConstDefine
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class CommonConstDefine
  {
    public const int MaxEquipmentNums = 4;
    public const int GuildMassiveCombatLeastSetupHeroNums = 1;
    public const int SSRRank = 4;
    public const int TopSlotEquipmentNums = 10;
    public const int HeroJobSpecificRefinerySlotId = 4;
    public const int HeroJobRefinerySlotMaxSlot = 4;
  }
}
