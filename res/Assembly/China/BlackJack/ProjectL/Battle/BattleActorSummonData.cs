﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.BattleActorSummonData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Battle
{
  [CustomLuaClass]
  public class BattleActorSummonData
  {
    public BattleActor Summoner;
    public ConfigDataSkillInfo SummonSkillInfo;
    public int Attack;
    public int Defense;
    public int Magic;
    public int MagicDefense;
    public int Dexterity;
    public int HealthPointMax;
    public int HealthPoint;
    public int AttackDistance;
    public ConfigDataSkillInfo MeleeSkillInfo;
    public ConfigDataSkillInfo RangeSkillInfo;
    public ConfigDataArmyInfo ArmyInfo;
    public MoveType MoveType;
    public int MovePoint;

    public BattleActorSummonData()
    {
      this.Clear();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Clear()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
