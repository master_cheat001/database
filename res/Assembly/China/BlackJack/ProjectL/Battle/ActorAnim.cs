﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.ActorAnim
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;

namespace BlackJack.ProjectL.Battle
{
  [CustomLuaClass]
  public class ActorAnim
  {
    public const string Idle = "idle";
    public const string Walk = "walk";
    public const string Run = "run";
    public const string Attack = "attack1";
    public const string SuperAttack = "superattack";
    public const string Sing = "sing";
    public const string Cast = "cast";
    public const string Hurt = "hurt";
    public const string Stun = "stun";
    public const string Die = "death";
    public const string Die1 = "death1";
    public const string Die2 = "death2";
  }
}
