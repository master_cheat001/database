﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.BattleTeam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Battle
{
  [CustomLuaClass]
  public class BattleTeam
  {
    private int m_initNotNpcActorCount;
    private int m_deadActorCount;
    private List<BattleActor> m_actors;
    private BattleBase m_battle;
    private int m_teamNumber;
    private Dictionary<int, int> m_groupBehaviorDict;
    private List<BehaviorGroup> m_groups;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleTeam(BattleBase battle, int teamNumer)
    {
      // ISSUE: unable to decompile the method.
    }

    public void RemoveDeleted()
    {
      EntityList.RemoveDeleted<BattleActor>(this.m_actors);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleActor CreateActor()
    {
      // ISSUE: unable to decompile the method.
    }

    public void RemoveAll()
    {
      EntityList.RemoveAll<BattleActor>(this.m_actors);
    }

    public List<BattleActor> GetActors()
    {
      return this.m_actors;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BattleActor> GetActorsWithoutId(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleActor GetActorById(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleActor GetActorByHeroId(int heroId, bool ignoreDead = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StopBattle(bool win)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasAliveActor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasNotActionFinishedActor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasFinishActionNpcActor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleTeam GetOtherTeam()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnMyActorDie(BattleActor actor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnMyActorRetreat(
      BattleActor actor,
      int effectType,
      string fxName,
      bool notifyListener)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ComputeDeadActorCount(
      List<int> heroIds,
      NpcCondition npcCondition,
      int killerHeroId = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ComputeAliveActorCount(List<int> heroIds, NpcCondition npcCondition)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsGroupDie(int groupId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void NextTurn()
    {
      // ISSUE: unable to decompile the method.
    }

    public BattleBase Battle
    {
      get
      {
        return this.m_battle;
      }
    }

    public int TeamNumber
    {
      get
      {
        return this.m_teamNumber;
      }
    }

    public int InitNotNpcActorCount
    {
      get
      {
        return this.m_initNotNpcActorCount;
      }
    }

    public int DeadActorCount
    {
      get
      {
        return this.m_deadActorCount;
      }
    }

    public void SetGroupBehaviorDict(Dictionary<int, int> behaviors)
    {
      this.m_groupBehaviorDict = behaviors;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddGroupBehaviors(Dictionary<int, int> behaviors)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CreateGroups()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private BehaviorGroup GetGroup(int groupID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private BehaviorGroup CreateGroup(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DoGroupBehavior()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateGroups()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
