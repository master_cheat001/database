﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.BattleLoseConditionState
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using SLua;

namespace BlackJack.ProjectL.Battle
{
  [CustomLuaClass]
  public class BattleLoseConditionState
  {
    public int m_conditionId;
    public BattleConditionStatus m_status;
    public ConfigDataBattleLoseConditionInfo m_loseConditionInfo;
  }
}
