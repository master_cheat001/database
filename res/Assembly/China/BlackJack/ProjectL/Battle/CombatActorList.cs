﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.CombatActorList
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System.Collections.Generic;

namespace BlackJack.ProjectL.Battle
{
  [CustomLuaClass]
  public class CombatActorList
  {
    public static void RemoveAll(List<CombatActor> list)
    {
      EntityList.RemoveAll<CombatActor>(list);
    }

    public static void RemoveDeleted(List<CombatActor> list)
    {
      EntityList.RemoveDeleted<CombatActor>(list);
    }

    public static void Tick(List<CombatActor> list)
    {
      EntityList.Tick<CombatActor>(list);
    }

    public static void TickGraphic(List<CombatActor> list, float dt)
    {
      EntityList.TickGraphic<CombatActor>(list, dt);
    }

    public static void Draw(List<CombatActor> list)
    {
      EntityList.Draw<CombatActor>(list);
    }

    public static void Pause(List<CombatActor> list, bool pause)
    {
      EntityList.Pause<CombatActor>(list, pause);
    }
  }
}
