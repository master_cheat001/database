﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.BehaviorGroup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Battle
{
  [CustomLuaClass]
  public class BehaviorGroup
  {
    private int m_ID;
    private BattleTeam m_team;
    private List<BattleActor> m_actors;
    private ConfigDataGroupBehavior m_curBehaviorCfg;
    private BattleActor m_leader;
    private int m_leaderNormalActionPriority;
    private List<int> m_singleBehaviors;

    [MethodImpl((MethodImplOptions) 32768)]
    public BehaviorGroup(int id, BattleTeam team)
    {
      // ISSUE: unable to decompile the method.
    }

    public int ID
    {
      get
      {
        return this.m_ID;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddActor(BattleActor actor)
    {
      // ISSUE: unable to decompile the method.
    }

    public BattleActor Leader
    {
      get
      {
        return this.m_leader;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBahvior(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    public ConfigDataGroupBehavior Behavior
    {
      get
      {
        return this.m_curBehaviorCfg;
      }
    }

    public List<BattleActor> Actors
    {
      get
      {
        return this.m_actors;
      }
    }

    public string InstanceID
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CheckGroupMemberDead()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DoBehavor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DoOtherBehavorCheck(BehaviorCondition condition)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
