﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProBPStageSyncCommand
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "ProBPStageSyncCommand")]
  [Serializable]
  public class ProBPStageSyncCommand : IExtensible
  {
    private int _PlayerIndex;
    private ProBPStageCommand _Command;
    private long _TurnActionTimeSpan;
    private readonly List<long> _PublicTimeSpan;
    private long _CreateTime;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProBPStageSyncCommand()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "PlayerIndex")]
    public int PlayerIndex
    {
      get
      {
        return this._PlayerIndex;
      }
      set
      {
        this._PlayerIndex = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = true, Name = "Command")]
    public ProBPStageCommand Command
    {
      get
      {
        return this._Command;
      }
      set
      {
        this._Command = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "TurnActionTimeSpan")]
    public long TurnActionTimeSpan
    {
      get
      {
        return this._TurnActionTimeSpan;
      }
      set
      {
        this._TurnActionTimeSpan = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, Name = "PublicTimeSpan")]
    public List<long> PublicTimeSpan
    {
      get
      {
        return this._PublicTimeSpan;
      }
    }

    [DefaultValue(0)]
    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "CreateTime")]
    public long CreateTime
    {
      get
      {
        return this._CreateTime;
      }
      set
      {
        this._CreateTime = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
