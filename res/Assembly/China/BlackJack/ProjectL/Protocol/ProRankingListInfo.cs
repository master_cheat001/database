﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProRankingListInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "ProRankingListInfo")]
  [Serializable]
  public class ProRankingListInfo : IExtensible
  {
    private int _Type;
    private int _Score;
    private int _CurrRank;
    private int _LastRank;
    private readonly List<ProRankingPlayerInfo> _PlayerList;
    private int _ChampionHeroId;
    private int _TotalTargetCount;
    private int _BossId;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProRankingListInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Type")]
    public int Type
    {
      get
      {
        return this._Type;
      }
      set
      {
        this._Type = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Score")]
    public int Score
    {
      get
      {
        return this._Score;
      }
      set
      {
        this._Score = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CurrRank")]
    public int CurrRank
    {
      get
      {
        return this._CurrRank;
      }
      set
      {
        this._CurrRank = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "LastRank")]
    public int LastRank
    {
      get
      {
        return this._LastRank;
      }
      set
      {
        this._LastRank = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, Name = "PlayerList")]
    public List<ProRankingPlayerInfo> PlayerList
    {
      get
      {
        return this._PlayerList;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "ChampionHeroId")]
    [DefaultValue(0)]
    public int ChampionHeroId
    {
      get
      {
        return this._ChampionHeroId;
      }
      set
      {
        this._ChampionHeroId = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "TotalTargetCount")]
    public int TotalTargetCount
    {
      get
      {
        return this._TotalTargetCount;
      }
      set
      {
        this._TotalTargetCount = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BossId")]
    public int BossId
    {
      get
      {
        return this._BossId;
      }
      set
      {
        this._BossId = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
