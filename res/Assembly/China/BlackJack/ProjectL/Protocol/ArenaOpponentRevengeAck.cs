﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ArenaOpponentRevengeAck
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "ArenaOpponentRevengeAck")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class ArenaOpponentRevengeAck : IExtensible
  {
    private int _Result;
    private ulong _BattleReportInstanceId;
    private bool _AutoBattle;
    private ProArenaDefensiveBattleInfo _OpponentDefensiveBattleInfo;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaOpponentRevengeAck()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Result")]
    public int Result
    {
      get
      {
        return this._Result;
      }
      set
      {
        this._Result = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BattleReportInstanceId")]
    public ulong BattleReportInstanceId
    {
      get
      {
        return this._BattleReportInstanceId;
      }
      set
      {
        this._BattleReportInstanceId = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "AutoBattle")]
    public bool AutoBattle
    {
      get
      {
        return this._AutoBattle;
      }
      set
      {
        this._AutoBattle = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = false, Name = "OpponentDefensiveBattleInfo")]
    [DefaultValue(null)]
    public ProArenaDefensiveBattleInfo OpponentDefensiveBattleInfo
    {
      get
      {
        return this._OpponentDefensiveBattleInfo;
      }
      set
      {
        this._OpponentDefensiveBattleInfo = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
