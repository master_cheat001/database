﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProAncientCallBoss
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "ProAncientCallBoss")]
  [Serializable]
  public class ProAncientCallBoss : IExtensible
  {
    private int _BossId;
    private int _CurrentPeriodMaxDamage;
    private int _CurrentMaxDamage;
    private readonly List<int> _TeamList;
    private long _UpdateTime;
    private int _TeamListBattlePower;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProAncientCallBoss()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BossId")]
    public int BossId
    {
      get
      {
        return this._BossId;
      }
      set
      {
        this._BossId = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CurrentPeriodMaxDamage")]
    public int CurrentPeriodMaxDamage
    {
      get
      {
        return this._CurrentPeriodMaxDamage;
      }
      set
      {
        this._CurrentPeriodMaxDamage = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CurrentMaxDamage")]
    public int CurrentMaxDamage
    {
      get
      {
        return this._CurrentMaxDamage;
      }
      set
      {
        this._CurrentMaxDamage = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, Name = "TeamList")]
    public List<int> TeamList
    {
      get
      {
        return this._TeamList;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "UpdateTime")]
    public long UpdateTime
    {
      get
      {
        return this._UpdateTime;
      }
      set
      {
        this._UpdateTime = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "TeamListBattlePower")]
    public int TeamListBattlePower
    {
      get
      {
        return this._TeamListBattlePower;
      }
      set
      {
        this._TeamListBattlePower = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
