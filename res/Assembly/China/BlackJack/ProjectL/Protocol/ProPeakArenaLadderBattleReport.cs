﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProPeakArenaLadderBattleReport
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "ProPeakArenaLadderBattleReport")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class ProPeakArenaLadderBattleReport : IExtensible
  {
    private ProCommonBattleReportHead _Head;
    private readonly List<ProPeakArenaBattleReportPlayerSummaryInfo> _PlayerSummaryInfos;
    private long _CreateTime;
    private readonly List<int> _WinPlayerIndexes;
    private int _Mode;
    private int _Round;
    private readonly List<ProPeakArenaBattleReportBattleInfo> _BattleInfos;
    private readonly List<string> _FirstHandUserIds;
    private string _ForfeitUserId;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProPeakArenaLadderBattleReport()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = true, Name = "Head")]
    public ProCommonBattleReportHead Head
    {
      get
      {
        return this._Head;
      }
      set
      {
        this._Head = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, Name = "PlayerSummaryInfos")]
    public List<ProPeakArenaBattleReportPlayerSummaryInfo> PlayerSummaryInfos
    {
      get
      {
        return this._PlayerSummaryInfos;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CreateTime")]
    public long CreateTime
    {
      get
      {
        return this._CreateTime;
      }
      set
      {
        this._CreateTime = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, Name = "WinPlayerIndexes")]
    public List<int> WinPlayerIndexes
    {
      get
      {
        return this._WinPlayerIndexes;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Mode")]
    public int Mode
    {
      get
      {
        return this._Mode;
      }
      set
      {
        this._Mode = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Round")]
    public int Round
    {
      get
      {
        return this._Round;
      }
      set
      {
        this._Round = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, Name = "BattleInfos")]
    public List<ProPeakArenaBattleReportBattleInfo> BattleInfos
    {
      get
      {
        return this._BattleInfos;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.Default, Name = "FirstHandUserIds")]
    public List<string> FirstHandUserIds
    {
      get
      {
        return this._FirstHandUserIds;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.Default, IsRequired = true, Name = "ForfeitUserId")]
    public string ForfeitUserId
    {
      get
      {
        return this._ForfeitUserId;
      }
      set
      {
        this._ForfeitUserId = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
