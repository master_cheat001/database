﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProCollectionEvent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "ProCollectionEvent")]
  [Serializable]
  public class ProCollectionEvent : IExtensible
  {
    private int _CollectionEventId;
    private int _CompletionTimes;
    private long _ExpiredTime;
    private int _WapPointId;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProCollectionEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CollectionEventId")]
    public int CollectionEventId
    {
      get
      {
        return this._CollectionEventId;
      }
      set
      {
        this._CollectionEventId = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CompletionTimes")]
    public int CompletionTimes
    {
      get
      {
        return this._CompletionTimes;
      }
      set
      {
        this._CompletionTimes = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ExpiredTime")]
    public long ExpiredTime
    {
      get
      {
        return this._ExpiredTime;
      }
      set
      {
        this._ExpiredTime = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "WapPointId")]
    public int WapPointId
    {
      get
      {
        return this._WapPointId;
      }
      set
      {
        this._WapPointId = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
