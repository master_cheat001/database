﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProLimitedTimeExchangeOperationActivity
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "ProLimitedTimeExchangeOperationActivity")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class ProLimitedTimeExchangeOperationActivity : IExtensible
  {
    private ProOperationalActivityBasicInfo _BasicInfo;
    private readonly List<ProLimitedTimeExchangeOperationActivityItemGroup> _ExchangedItemGroups;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProLimitedTimeExchangeOperationActivity()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = true, Name = "BasicInfo")]
    public ProOperationalActivityBasicInfo BasicInfo
    {
      get
      {
        return this._BasicInfo;
      }
      set
      {
        this._BasicInfo = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, Name = "ExchangedItemGroups")]
    public List<ProLimitedTimeExchangeOperationActivityItemGroup> ExchangedItemGroups
    {
      get
      {
        return this._ExchangedItemGroups;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
