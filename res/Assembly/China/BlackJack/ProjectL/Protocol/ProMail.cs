﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProMail
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "ProMail")]
  [Serializable]
  public class ProMail : IExtensible
  {
    private int _TemplateId;
    private ulong _InstanceId;
    private int _Status;
    private long _SendTime;
    private long _ReadedOrGotAttachmentTime;
    private string _Title;
    private string _Content;
    private readonly List<ProGoods> _Attachments;
    private uint _ExpiredTime;
    private int _ReadedExpiredTime;
    private bool _GotDeleted;
    private int _MailTypeId;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProMail()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "TemplateId")]
    public int TemplateId
    {
      get
      {
        return this._TemplateId;
      }
      set
      {
        this._TemplateId = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "InstanceId")]
    public ulong InstanceId
    {
      get
      {
        return this._InstanceId;
      }
      set
      {
        this._InstanceId = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Status")]
    public int Status
    {
      get
      {
        return this._Status;
      }
      set
      {
        this._Status = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "SendTime")]
    public long SendTime
    {
      get
      {
        return this._SendTime;
      }
      set
      {
        this._SendTime = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ReadedOrGotAttachmentTime")]
    public long ReadedOrGotAttachmentTime
    {
      get
      {
        return this._ReadedOrGotAttachmentTime;
      }
      set
      {
        this._ReadedOrGotAttachmentTime = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = false, Name = "Title")]
    [DefaultValue("")]
    public string Title
    {
      get
      {
        return this._Title;
      }
      set
      {
        this._Title = value;
      }
    }

    [DefaultValue("")]
    [ProtoMember(7, DataFormat = DataFormat.Default, IsRequired = false, Name = "Content")]
    public string Content
    {
      get
      {
        return this._Content;
      }
      set
      {
        this._Content = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.Default, Name = "Attachments")]
    public List<ProGoods> Attachments
    {
      get
      {
        return this._Attachments;
      }
    }

    [DefaultValue(0)]
    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "ExpiredTime")]
    public uint ExpiredTime
    {
      get
      {
        return this._ExpiredTime;
      }
      set
      {
        this._ExpiredTime = value;
      }
    }

    [DefaultValue(0)]
    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "ReadedExpiredTime")]
    public int ReadedExpiredTime
    {
      get
      {
        return this._ReadedExpiredTime;
      }
      set
      {
        this._ReadedExpiredTime = value;
      }
    }

    [DefaultValue(false)]
    [ProtoMember(11, DataFormat = DataFormat.Default, IsRequired = false, Name = "GotDeleted")]
    public bool GotDeleted
    {
      get
      {
        return this._GotDeleted;
      }
      set
      {
        this._GotDeleted = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MailTypeId")]
    public int MailTypeId
    {
      get
      {
        return this._MailTypeId;
      }
      set
      {
        this._MailTypeId = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
