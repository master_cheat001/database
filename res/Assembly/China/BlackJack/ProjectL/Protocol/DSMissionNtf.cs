﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.DSMissionNtf
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "DSMissionNtf")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class DSMissionNtf : IExtensible
  {
    private uint _Version;
    private readonly List<int> _FinishedEverydayMissionList;
    private readonly List<int> _FinishedOneOffMissionList;
    private readonly List<ProMission> _ProcessingMissionList;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public DSMissionNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Version")]
    public uint Version
    {
      get
      {
        return this._Version;
      }
      set
      {
        this._Version = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, Name = "FinishedEverydayMissionList")]
    public List<int> FinishedEverydayMissionList
    {
      get
      {
        return this._FinishedEverydayMissionList;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, Name = "FinishedOneOffMissionList")]
    public List<int> FinishedOneOffMissionList
    {
      get
      {
        return this._FinishedOneOffMissionList;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, Name = "ProcessingMissionList")]
    public List<ProMission> ProcessingMissionList
    {
      get
      {
        return this._ProcessingMissionList;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
