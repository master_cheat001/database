﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ArenaBattleReportPlayBackDataGetAck
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "ArenaBattleReportPlayBackDataGetAck")]
  [Serializable]
  public class ArenaBattleReportPlayBackDataGetAck : IExtensible
  {
    private int _Result;
    private ulong _ArenaBattleReportInstanceId;
    private ProArenaBattleReport _BattlePlayBackData;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaBattleReportPlayBackDataGetAck()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Result")]
    public int Result
    {
      get
      {
        return this._Result;
      }
      set
      {
        this._Result = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ArenaBattleReportInstanceId")]
    public ulong ArenaBattleReportInstanceId
    {
      get
      {
        return this._ArenaBattleReportInstanceId;
      }
      set
      {
        this._ArenaBattleReportInstanceId = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = false, Name = "BattlePlayBackData")]
    [DefaultValue(null)]
    public ProArenaBattleReport BattlePlayBackData
    {
      get
      {
        return this._BattlePlayBackData;
      }
      set
      {
        this._BattlePlayBackData = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
