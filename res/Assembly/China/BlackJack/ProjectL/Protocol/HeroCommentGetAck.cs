﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.HeroCommentGetAck
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "HeroCommentGetAck")]
  [Serializable]
  public class HeroCommentGetAck : IExtensible
  {
    private int _Result;
    private int _HeroId;
    private ProHeroComment _HeroComment;
    private ProPlayerHeroCommentEntry _PlayerCommentEntry;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroCommentGetAck()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Result")]
    public int Result
    {
      get
      {
        return this._Result;
      }
      set
      {
        this._Result = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HeroId")]
    public int HeroId
    {
      get
      {
        return this._HeroId;
      }
      set
      {
        this._HeroId = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = false, Name = "HeroComment")]
    [DefaultValue(null)]
    public ProHeroComment HeroComment
    {
      get
      {
        return this._HeroComment;
      }
      set
      {
        this._HeroComment = value;
      }
    }

    [DefaultValue(null)]
    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = false, Name = "PlayerCommentEntry")]
    public ProPlayerHeroCommentEntry PlayerCommentEntry
    {
      get
      {
        return this._PlayerCommentEntry;
      }
      set
      {
        this._PlayerCommentEntry = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
