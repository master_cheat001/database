﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProHero
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "ProHero")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class ProHero : IExtensible
  {
    private int _HeroId;
    private int _Level;
    private int _Exp;
    private int _StarLevel;
    private int _ActiveJobRelatedId;
    private readonly List<ProHeroJob> _Jobs;
    private readonly List<int> _SelectedSkills;
    private int _SelectedSoldierId;
    private readonly List<int> _UnlockedJobs;
    private int _FightNums;
    private readonly List<ulong> _EquipmentIds;
    private int _FavorabilityLevel;
    private int _FavorabilityExp;
    private readonly List<ProHeroFetter> _Fetters;
    private bool _Confessed;
    private int _Power;
    private int _CharSkinId;
    private readonly List<ProSoldierSkin> _SoldierSkins;
    private int _HeartFetterLevel;
    private long _HeroHeartFetterUnlockTime;
    private long _HeroHeartFetterLevelMaxTime;
    private bool _Mine;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProHero()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HeroId")]
    public int HeroId
    {
      get
      {
        return this._HeroId;
      }
      set
      {
        this._HeroId = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Level")]
    public int Level
    {
      get
      {
        return this._Level;
      }
      set
      {
        this._Level = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Exp")]
    public int Exp
    {
      get
      {
        return this._Exp;
      }
      set
      {
        this._Exp = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "StarLevel")]
    public int StarLevel
    {
      get
      {
        return this._StarLevel;
      }
      set
      {
        this._StarLevel = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ActiveJobRelatedId")]
    public int ActiveJobRelatedId
    {
      get
      {
        return this._ActiveJobRelatedId;
      }
      set
      {
        this._ActiveJobRelatedId = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, Name = "Jobs")]
    public List<ProHeroJob> Jobs
    {
      get
      {
        return this._Jobs;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, Name = "SelectedSkills")]
    public List<int> SelectedSkills
    {
      get
      {
        return this._SelectedSkills;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "SelectedSoldierId")]
    public int SelectedSoldierId
    {
      get
      {
        return this._SelectedSoldierId;
      }
      set
      {
        this._SelectedSoldierId = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, Name = "UnlockedJobs")]
    public List<int> UnlockedJobs
    {
      get
      {
        return this._UnlockedJobs;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "FightNums")]
    public int FightNums
    {
      get
      {
        return this._FightNums;
      }
      set
      {
        this._FightNums = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, Name = "EquipmentIds")]
    public List<ulong> EquipmentIds
    {
      get
      {
        return this._EquipmentIds;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "FavorabilityLevel")]
    public int FavorabilityLevel
    {
      get
      {
        return this._FavorabilityLevel;
      }
      set
      {
        this._FavorabilityLevel = value;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "FavorabilityExp")]
    public int FavorabilityExp
    {
      get
      {
        return this._FavorabilityExp;
      }
      set
      {
        this._FavorabilityExp = value;
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.Default, Name = "Fetters")]
    public List<ProHeroFetter> Fetters
    {
      get
      {
        return this._Fetters;
      }
    }

    [ProtoMember(15, DataFormat = DataFormat.Default, IsRequired = true, Name = "Confessed")]
    public bool Confessed
    {
      get
      {
        return this._Confessed;
      }
      set
      {
        this._Confessed = value;
      }
    }

    [ProtoMember(16, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Power")]
    public int Power
    {
      get
      {
        return this._Power;
      }
      set
      {
        this._Power = value;
      }
    }

    [ProtoMember(17, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CharSkinId")]
    public int CharSkinId
    {
      get
      {
        return this._CharSkinId;
      }
      set
      {
        this._CharSkinId = value;
      }
    }

    [ProtoMember(18, DataFormat = DataFormat.Default, Name = "SoldierSkins")]
    public List<ProSoldierSkin> SoldierSkins
    {
      get
      {
        return this._SoldierSkins;
      }
    }

    [ProtoMember(19, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HeartFetterLevel")]
    public int HeartFetterLevel
    {
      get
      {
        return this._HeartFetterLevel;
      }
      set
      {
        this._HeartFetterLevel = value;
      }
    }

    [ProtoMember(20, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HeroHeartFetterUnlockTime")]
    public long HeroHeartFetterUnlockTime
    {
      get
      {
        return this._HeroHeartFetterUnlockTime;
      }
      set
      {
        this._HeroHeartFetterUnlockTime = value;
      }
    }

    [ProtoMember(21, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HeroHeartFetterLevelMaxTime")]
    public long HeroHeartFetterLevelMaxTime
    {
      get
      {
        return this._HeroHeartFetterLevelMaxTime;
      }
      set
      {
        this._HeroHeartFetterLevelMaxTime = value;
      }
    }

    [ProtoMember(22, DataFormat = DataFormat.Default, IsRequired = true, Name = "Mine")]
    public bool Mine
    {
      get
      {
        return this._Mine;
      }
      set
      {
        this._Mine = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
