﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.PeakArenaPlayOffLiveRoomInfoGetAck
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "PeakArenaPlayOffLiveRoomInfoGetAck")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class PeakArenaPlayOffLiveRoomInfoGetAck : IExtensible
  {
    private PeakArenaPlayOffLiveRoomGetReq _Req;
    private readonly List<ProPeakArenaLiveRoomInfo> _Info;
    private int _StartMatchIndex;
    private int _NextMatchIndex;
    private int _Result;
    private long _Version;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaPlayOffLiveRoomInfoGetAck()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = true, Name = "Req")]
    public PeakArenaPlayOffLiveRoomGetReq Req
    {
      get
      {
        return this._Req;
      }
      set
      {
        this._Req = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, Name = "Info")]
    public List<ProPeakArenaLiveRoomInfo> Info
    {
      get
      {
        return this._Info;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "StartMatchIndex")]
    public int StartMatchIndex
    {
      get
      {
        return this._StartMatchIndex;
      }
      set
      {
        this._StartMatchIndex = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "NextMatchIndex")]
    public int NextMatchIndex
    {
      get
      {
        return this._NextMatchIndex;
      }
      set
      {
        this._NextMatchIndex = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Result")]
    public int Result
    {
      get
      {
        return this._Result;
      }
      set
      {
        this._Result = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Version")]
    public long Version
    {
      get
      {
        return this._Version;
      }
      set
      {
        this._Version = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
