﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.BattleRoomTeamBattleFinishNtf
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "BattleRoomTeamBattleFinishNtf")]
  [Serializable]
  public class BattleRoomTeamBattleFinishNtf : IExtensible
  {
    private int _Stars;
    private ProChangedGoodsNtf _Ntf;
    private readonly List<ProGoods> _NormalRewards;
    private readonly List<ProGoods> _DailyRewards;
    private readonly List<ProGoods> _TeamRewards1;
    private readonly List<ProGoods> _TeamRewards2;
    private readonly List<ProGoods> _FriendRewards;
    private int _FriendshipPoints;
    private readonly List<ProGoods> _UnchartedScoreRewards;
    private readonly List<ProGoods> _AdditionalRewards;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleRoomTeamBattleFinishNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Stars")]
    public int Stars
    {
      get
      {
        return this._Stars;
      }
      set
      {
        this._Stars = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = true, Name = "Ntf")]
    public ProChangedGoodsNtf Ntf
    {
      get
      {
        return this._Ntf;
      }
      set
      {
        this._Ntf = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, Name = "NormalRewards")]
    public List<ProGoods> NormalRewards
    {
      get
      {
        return this._NormalRewards;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, Name = "DailyRewards")]
    public List<ProGoods> DailyRewards
    {
      get
      {
        return this._DailyRewards;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, Name = "TeamRewards1")]
    public List<ProGoods> TeamRewards1
    {
      get
      {
        return this._TeamRewards1;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, Name = "TeamRewards2")]
    public List<ProGoods> TeamRewards2
    {
      get
      {
        return this._TeamRewards2;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, Name = "FriendRewards")]
    public List<ProGoods> FriendRewards
    {
      get
      {
        return this._FriendRewards;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "FriendshipPoints")]
    public int FriendshipPoints
    {
      get
      {
        return this._FriendshipPoints;
      }
      set
      {
        this._FriendshipPoints = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.Default, Name = "UnchartedScoreRewards")]
    public List<ProGoods> UnchartedScoreRewards
    {
      get
      {
        return this._UnchartedScoreRewards;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.Default, Name = "AdditionalRewards")]
    public List<ProGoods> AdditionalRewards
    {
      get
      {
        return this._AdditionalRewards;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
