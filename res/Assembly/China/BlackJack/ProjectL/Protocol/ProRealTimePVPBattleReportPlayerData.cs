﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProRealTimePVPBattleReportPlayerData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "ProRealTimePVPBattleReportPlayerData")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class ProRealTimePVPBattleReportPlayerData : IExtensible
  {
    private string _UserId;
    private string _Name;
    private int _HeadIcon;
    private int _Level;
    private readonly List<ProBattleHero> _Heroes;
    private readonly List<ProTrainingTech> _Techs;
    private int _ScoreDiff;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProRealTimePVPBattleReportPlayerData()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = true, Name = "UserId")]
    public string UserId
    {
      get
      {
        return this._UserId;
      }
      set
      {
        this._UserId = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HeadIcon")]
    public int HeadIcon
    {
      get
      {
        return this._HeadIcon;
      }
      set
      {
        this._HeadIcon = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Level")]
    public int Level
    {
      get
      {
        return this._Level;
      }
      set
      {
        this._Level = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, Name = "Heroes")]
    public List<ProBattleHero> Heroes
    {
      get
      {
        return this._Heroes;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, Name = "Techs")]
    public List<ProTrainingTech> Techs
    {
      get
      {
        return this._Techs;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ScoreDiff")]
    public int ScoreDiff
    {
      get
      {
        return this._ScoreDiff;
      }
      set
      {
        this._ScoreDiff = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
