﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProPeakArenaSeasonInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "ProPeakArenaSeasonInfo")]
  [Serializable]
  public class ProPeakArenaSeasonInfo : IExtensible
  {
    private int _Season;
    private readonly List<ProPeakArenaPlayOffMatchupInfo> _MatchupInfos;
    private long _SeasonEndTime;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProPeakArenaSeasonInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Season")]
    public int Season
    {
      get
      {
        return this._Season;
      }
      set
      {
        this._Season = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, Name = "MatchupInfos")]
    public List<ProPeakArenaPlayOffMatchupInfo> MatchupInfos
    {
      get
      {
        return this._MatchupInfos;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "SeasonEndTime")]
    public long SeasonEndTime
    {
      get
      {
        return this._SeasonEndTime;
      }
      set
      {
        this._SeasonEndTime = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
