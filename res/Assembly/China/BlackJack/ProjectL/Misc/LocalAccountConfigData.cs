﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Misc.LocalAccountConfigData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Misc
{
  [CustomLuaClass]
  public class LocalAccountConfigData
  {
    public string[] HaveReadAnnounceActivities;
    public Dictionary<ulong, string> LastReadActivityTimeList;
    public Dictionary<int, string> LastReadRecommendGiftBoxTimeDic;
    public int[] HaveReadHeroBiographyIds;
    public int[] HaveReadHeroPerformanceIds;
    public int[] UnlockHeroBiographyIds;
    public int[] UnlockHeroPerformanceIds;
    public int[] UnlockHeroDungeonLevelIds;
    public int[] UnlockHeroFetterIds;
    public int[] ArenaAttackerHeroIds;
    public int TeamPlayerLevelMin;
    public int TeamPlayerLevelMax;
    public bool IsRealtimePVPShowNotice;
    public bool HaveDoneMemoryExtraction;
    public DateTime LastNotifyPeakArenaTime1;
    public DateTime LastNotifyPeakArenaTime2;
    public string GMString;

    [MethodImpl((MethodImplOptions) 32768)]
    public LocalAccountConfigData()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
