﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Misc.SystemUtility
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using SLua;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.Misc
{
  [CustomLuaClass]
  public class SystemUtility
  {
    private static readonly System.Random _random = new System.Random();
    private static int _timeScale = (int) ((double) Time.timeScale * 10000.0);
    private static readonly int _timeScaleMask = SystemUtility._random.Next(int.MinValue, int.MaxValue);
    private static ConfigDataDeviceSetting s_deviceSetting = (ConfigDataDeviceSetting) null;
    private static bool s_needUpdateDeviceSetting = true;

    public static float TimeScale
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public static void SetTimeScale(float scale)
    {
      SystemUtility.TimeScale = scale;
      Time.timeScale = scale;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ConfigDataDeviceSetting GetConfigDataDeviceSetting()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsiPhoneX()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsLowSystemMemory()
    {
      return SystemInfo.systemMemorySize <= 1100;
    }

    public static bool IsLargeSystemMemory()
    {
      return SystemInfo.systemMemorySize > 4000;
    }

    public static BatteryStatus GetBatteryStatus()
    {
      return SystemInfo.batteryStatus;
    }

    public static float GetBatteryLevel()
    {
      return SystemInfo.batteryLevel;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void LogBatteryStatus()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    static SystemUtility()
    {
    }
  }
}
