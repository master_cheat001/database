﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.CollectionActivityWaypointList
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Battle;
using SLua;
using System.Collections.Generic;

namespace BlackJack.ProjectL.Scene
{
  [CustomLuaClass]
  public class CollectionActivityWaypointList
  {
    public static void RemoveAll(List<CollectionActivityWaypoint> list)
    {
      EntityList.RemoveAll<CollectionActivityWaypoint>(list);
    }

    public static void RemoveDeleted(List<CollectionActivityWaypoint> list)
    {
      EntityList.RemoveDeleted<CollectionActivityWaypoint>(list);
    }

    public static void Tick(List<CollectionActivityWaypoint> list)
    {
      EntityList.Tick<CollectionActivityWaypoint>(list);
    }

    public static void TickGraphic(List<CollectionActivityWaypoint> list, float dt)
    {
      EntityList.TickGraphic<CollectionActivityWaypoint>(list, dt);
    }

    public static void Draw(List<CollectionActivityWaypoint> list)
    {
      EntityList.Draw<CollectionActivityWaypoint>(list);
    }

    public static void Pause(List<CollectionActivityWaypoint> list, bool pause)
    {
      EntityList.Pause<CollectionActivityWaypoint>(list, pause);
    }
  }
}
