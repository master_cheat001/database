﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.ClientActorActSkill
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Battle;
using BlackJack.ProjectL.Common;
using SLua;
using System.Collections.Generic;

namespace BlackJack.ProjectL.Scene
{
  [CustomLuaClass]
  public class ClientActorActSkill : ClientActorAct
  {
    public int m_direction;
    public ConfigDataSkillInfo m_skillInfo;
    public GridPosition m_targetPosition;
    public List<BattleActor> m_targetActors;
    public List<BattleActor> m_combineActors;
    public List<ClientActorHitInfo> m_selfHits;
  }
}
