﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.ClientBattleConst
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;

namespace BlackJack.ProjectL.Scene
{
  [CustomLuaClass]
  public class ClientBattleConst
  {
    public const int TickRate = 30;
    public const float TickTime = 0.03333334f;
    public const float GridWidth = 2f;
    public const float GridHeight = 2f;
    public const float FootHeightScale = 0.25f;
    public const float SkillFadeZOffset = 30f;
    public const float GuardZOffset = -1f;
    public const float ActorFxZOffset = -20f;
    public const float TreasureIdleZOffset = 1f;
    public const float TreasureOpenZOffset = -1f;
    public const int HpBarBuffCountMax = 3;
    public const int BattleResultHeroCountMax = 5;
  }
}
