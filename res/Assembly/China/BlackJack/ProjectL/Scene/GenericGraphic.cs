﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.GenericGraphic
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Art;
using BlackJack.ProjectL.Battle;
using BlackJack.ProjectL.Common;
using FixMath.NET;
using SLua;
using Spine;
using Spine.Unity;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.Scene
{
  [CustomLuaClass]
  public class GenericGraphic : IBattleGraphic
  {
    private IGenericGraphicContainer m_container;
    private FxPlayer m_fxPlayer;
    private GameObject m_gameObject;
    private GameObject m_prefab;
    private Animator m_animator;
    private int m_animatorTriggerNameId;
    private string m_assetName;
    private Vector3 m_initScale;
    private Vector3 m_prefabScale;
    private int m_direction;
    private int m_layer;
    private int m_sortingOrder;
    private Colori m_color;
    private float m_intensity;
    private float m_saturation;
    private bool m_isPaused;
    private bool m_isVisible;
    private GenericGraphic.SpineInfo m_spineInfo;
    private GenericGraphic.FxInfo m_fxInfo;
    private GenericGraphic.EffectInfo m_effectInfo;
    private List<GenericGraphic> m_attachFxs;
    private MaterialPropertyBlock s_materialPropertyBlock;

    [MethodImpl((MethodImplOptions) 32768)]
    public GenericGraphic()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Create(string assetName, bool isFx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HandleSpineAnimationEvent(Spine.AnimationState state, int trackIndex, Spine.Event e)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HandleSpineAnimationStart(Spine.AnimationState state, int trackIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HandleSpineAnimationEnd(Spine.AnimationState state, int trackIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Destroy()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetActive(bool active)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GetAnimationInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetParent(GameObject parent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLayer(int layer)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSortingOrder(int order)
    {
      // ISSUE: unable to decompile the method.
    }

    public GameObject GetPrefab()
    {
      return this.m_prefab;
    }

    public void SetContainer(IGenericGraphicContainer container)
    {
      this.m_container = container;
    }

    public void SetFxPlayer(FxPlayer player)
    {
      this.m_fxPlayer = player;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayAnimation(string name, bool loop)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayDeathAnimation(int deathType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public float GetAnimationDuration(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPlayAnimation(int trackIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayFx(string name, int tag = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetReplaceAnimations(List<ReplaceAnim> replaceAnims)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHeight(float height, float footHeight)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCombatPosition(Vector2i pos, Fix64 z, int zoffset)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCombatDirection(Vector2i front, Fix64 frontZ)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCombatDirection(int dir)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDirection(int dir)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRotationZ(float rz)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRotation(Quaternion q)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetPrefabScale(float scale)
    {
      this.SetPrefabScale(new Vector3(scale, scale, scale));
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPrefabScale(Vector3 scale)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetName(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetVisible(bool visible)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetColor(Colori color)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetIntensity(float intensity)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSaturation(float saturation)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateMaterialPropertyBlock()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetEffect(GraphicEffect e, float param1, float param2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearEffect(GraphicEffect e)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearAttachFxs(int tagMask)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Pause(bool pause)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FastForward(float t)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FadeStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ForceUpdateSpine()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Tick(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FxStop(bool fadeOut)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetFxCanLoop(bool loop)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsFxLoop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TickEffect(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TickFx(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsFxLifeEnd()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TickAttachFx(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPosition(Vector3 p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Vector3 GetPosition()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAnchoredPosition(Vector3 p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetWorldPosition(Vector3 p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Vector3 GetWorldPosition()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckIsCulled(Vector3 p, bool isCombat)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCulled()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsVisible()
    {
      return this.m_isVisible;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FlashEffect(Colori color, float intensity, float time, int repeat)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GhostEffect(int count, float distance, float fade)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearGhosts()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HighlightEffect(
      GraphicEffect effectType,
      Colori color,
      float intensity,
      float time)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FadeInEffect(float time)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FadeOutEffect(float time)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AutoDelete(bool autoDelete)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeleteMe()
    {
      // ISSUE: unable to decompile the method.
    }

    public string AssetName
    {
      get
      {
        return this.m_assetName;
      }
    }

    public bool IsDeleteMe
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsAutoDelete
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [CustomLuaClass]
    public class SpineInfo
    {
      public SkeletonAnimation m_spine;
      public string m_initAnimationName;
      public bool m_isAnimationLoop;
      public Vector2 m_boundsMin;
      public Vector2 m_boundsMax;
      public bool m_isCulled;
      public float m_height;
      public float m_footHeight;
      public List<ReplaceAnim> m_replaceAnims;
    }

    [CustomLuaClass]
    public class FxInfo
    {
      public FxDesc m_fxDesc;
      public float m_initLife;
      public float m_lifeCountdown;
      public bool m_isInitLoop;
      public bool m_isPlayLoop;
      public bool m_isDeleteMe;
      public bool m_isAutoDelete;
      public int m_tag;
      public List<ParticleSystem> m_stopParticleSystems;
      public List<Renderer> m_stopRenderers;
      public TrailRenderer[] m_trailRenderers;
      public Bone m_attachBone;
      public float m_yOffset;
    }

    [CustomLuaClass]
    public class Ghost
    {
      public float m_speed;
      public float m_followTime;
      public Vector2 m_followStartPosition;
      public int m_state;
      public int m_turnCount;
      public Vector2 m_targetPosition;
      public GenericGraphic m_fx;
    }

    [CustomLuaClass]
    public class EffectInfo
    {
      public Colori m_color;
      public float m_intensity;
      public float m_totalTime;
      public float m_curTime;
      public int m_repeatCount;
      public GraphicEffect m_type;
      public float m_ghostDistance;
      public List<GenericGraphic.Ghost> m_ghosts;
    }
  }
}
