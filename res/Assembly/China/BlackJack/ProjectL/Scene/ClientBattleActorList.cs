﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.ClientBattleActorList
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Battle;
using SLua;
using System.Collections.Generic;

namespace BlackJack.ProjectL.Scene
{
  [CustomLuaClass]
  public class ClientBattleActorList
  {
    public static void RemoveAll(List<ClientBattleActor> list)
    {
      EntityList.RemoveAll<ClientBattleActor>(list);
    }

    public static void RemoveDeleted(List<ClientBattleActor> list)
    {
      EntityList.RemoveDeleted<ClientBattleActor>(list);
    }

    public static void Tick(List<ClientBattleActor> list)
    {
      EntityList.Tick<ClientBattleActor>(list);
    }

    public static void TickGraphic(List<ClientBattleActor> list, float dt)
    {
      EntityList.TickGraphic<ClientBattleActor>(list, dt);
    }

    public static void Draw(List<ClientBattleActor> list)
    {
      EntityList.Draw<ClientBattleActor>(list);
    }

    public static void Pause(List<ClientBattleActor> list, bool pause)
    {
      EntityList.Pause<ClientBattleActor>(list, pause);
    }
  }
}
