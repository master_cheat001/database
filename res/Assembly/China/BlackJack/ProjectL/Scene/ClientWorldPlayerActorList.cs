﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.ClientWorldPlayerActorList
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Battle;
using SLua;
using System.Collections.Generic;

namespace BlackJack.ProjectL.Scene
{
  [CustomLuaClass]
  public class ClientWorldPlayerActorList
  {
    public static void RemoveAll(List<ClientWorldPlayerActor> list)
    {
      EntityList.RemoveAll<ClientWorldPlayerActor>(list);
    }

    public static void RemoveDeleted(List<ClientWorldPlayerActor> list)
    {
      EntityList.RemoveDeleted<ClientWorldPlayerActor>(list);
    }

    public static void Tick(List<ClientWorldPlayerActor> list)
    {
      EntityList.Tick<ClientWorldPlayerActor>(list);
    }

    public static void TickGraphic(List<ClientWorldPlayerActor> list, float dt)
    {
      EntityList.TickGraphic<ClientWorldPlayerActor>(list, dt);
    }

    public static void Draw(List<ClientWorldPlayerActor> list)
    {
      EntityList.Draw<ClientWorldPlayerActor>(list);
    }

    public static void Pause(List<ClientWorldPlayerActor> list, bool pause)
    {
      EntityList.Pause<ClientWorldPlayerActor>(list, pause);
    }
  }
}
