﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.ClientNullActorActBattleLoseCondition
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using SLua;

namespace BlackJack.ProjectL.Scene
{
  [CustomLuaClass]
  public class ClientNullActorActBattleLoseCondition : ClientActorAct
  {
    public ConfigDataBattleLoseConditionInfo m_loseConditionInfo;
    public GridPosition m_targetPosition;
  }
}
