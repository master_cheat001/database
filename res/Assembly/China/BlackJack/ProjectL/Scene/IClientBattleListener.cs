﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.IClientBattleListener
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Battle;
using BlackJack.ProjectL.Common;
using System;

namespace BlackJack.ProjectL.Scene
{
  public interface IClientBattleListener
  {
    void OnStartBattle();

    void OnStopBattle(bool win, bool skipPerform);

    void OnShowBattleWinCondition(
      ConfigDataBattleWinConditionInfo winConditionInfo,
      GridPosition targetPos);

    void OnHideBattleWinCondition(
      ConfigDataBattleWinConditionInfo winConditionInfo,
      GridPosition targetPos);

    void OnShowBattleLoseCondition(
      ConfigDataBattleLoseConditionInfo loseConditionInfo,
      GridPosition targetPos);

    void OnHideBattleLoseCondition(
      ConfigDataBattleLoseConditionInfo loseConditionInfo,
      GridPosition targetPos);

    void OnNextTurn(int turn);

    void OnNextTurnAnimationEnd(int turn);

    void OnNextTeam(int team);

    void OnNextPlayer(int playerIndex);

    void OnNextActor(BattleActor actor);

    void OnClientActorActive(ClientBattleActor a, bool newStep, int step, int turn);

    void OnClientActorMove(ClientBattleActor a);

    void OnClientActorTryMove(ClientBattleActor a);

    void OnClientActorNoAct(ClientBattleActor a, int endStep);

    void OnClientActorTarget(
      ClientBattleActor a,
      ConfigDataSkillInfo skill,
      GridPosition pos,
      int armyRelationValue);

    void OnClientActorSkill(ClientBattleActor a, ConfigDataSkillInfo skillInfo);

    void OnClientActorSkillEnd(ClientBattleActor a, ConfigDataSkillInfo skillInfo);

    void OnClientActorSkillHit(
      ClientBattleActor a,
      ConfigDataSkillInfo skill,
      int heroHpModify,
      int soldierHpModify,
      DamageNumberType damageNumberType);

    void OnClientActorBuffHit(
      ClientBattleActor a,
      ConfigDataBuffInfo buffInfo,
      int heroHpModify,
      int soldierHpModify,
      DamageNumberType damageNumberType);

    void OnClientActorTerrainHit(
      ClientBattleActor a,
      int heroHpModify,
      int soldierHpModify,
      DamageNumberType damageNumberType);

    void OnClientActorImmune(ClientBattleActor a);

    void OnClientActorPassiveSkill(ClientBattleActor a, BuffState sourceBuffState);

    void OnClientActorGuard(ClientBattleActor a, ClientBattleActor target);

    void OnClientActorDie(ClientBattleActor a);

    void OnClientActorAppear(ClientBattleActor a);

    void OnCancelCombat();

    void OnPreStartCombat(BattleActor a, BattleActor b);

    void OnLoadCombatAssets(BattleActor a, BattleActor b, Action onEnd);

    void OnStartCombat(BattleActor a, BattleActor b, bool splitScreen);

    void OnPreStopCombat();

    void OnStopCombat();

    void OnPrepareFastCombat(BattleActor a, BattleActor b, ConfigDataSkillInfo attackerSkillInfo);

    void OnStartFastCombat(FastCombatActorInfo a, FastCombatActorInfo b);

    void OnCombatActorHit(
      CombatActor a,
      CombatActor attacker,
      ConfigDataSkillInfo skill,
      int hpModify,
      int totalDamage,
      DamageNumberType damageNumberType);

    void OnStartSkillCutscene(
      ConfigDataSkillInfo skillInfo,
      ConfigDataCutsceneInfo cutsceneInfo,
      int team);

    void OnStartPassiveSkillCutscene(BuffState sourceBuffState, int team);

    void OnStopSkillCutscene();

    void OnStartBattleDialog(ConfigDataBattleDialogInfo dialogInfo);

    void OnStartBattleTreasureDialog(ConfigDataBattleTreasureInfo treasureInfo);

    void OnShowBattleTreasureReward(ConfigDataBattleTreasureInfo treasureInfo);

    void OnStartBattlePerform();

    void OnStopBattlePerform();

    void OnScreenEffect(string name);
  }
}
