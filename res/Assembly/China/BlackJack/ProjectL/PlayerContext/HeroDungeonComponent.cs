﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.HeroDungeonComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.PlayerContext
{
  [HotFix]
  public class HeroDungeonComponent : HeroDungeonComponentCommon
  {
    [DoNotToLua]
    private HeroDungeonComponent.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_Init_hotfix;
    private LuaFunction m_PostInit_hotfix;
    private LuaFunction m_DeInit_hotfix;
    private LuaFunction m_DeSerializeDSHeroDungeonNtf_hotfix;
    private LuaFunction m_InitChaptersList`1_hotfix;
    private LuaFunction m_InitChapterProHeroDungeonChapter_hotfix;
    private LuaFunction m_FinishBattleHeroDungeonLevelInt32List`1Int32List`1_hotfix;
    private LuaFunction m_SetSuccessHeroDungeonLevelConfigDataHeroDungeonLevelInfoList`1Int32List`1_hotfix;
    private LuaFunction m_RaidHeroDungeonLevelInt32Int32_hotfix;
    private LuaFunction m_IsLevelChallengedInt32_hotfix;
    private LuaFunction m_GetDSVersion_hotfix;
    private LuaFunction m_GetMaxFinishedLevelIdInt32_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroDungeonComponent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeSerialize(DSHeroDungeonNtf ntf)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitChapters(List<ProHeroDungeonChapter> pbChapters)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitChapter(ProHeroDungeonChapter pbChapter)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int FinishBattleHeroDungeonLevel(
      int levelId,
      List<int> gotAchievementIds,
      int stars,
      List<int> battleTreasures)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void SetSuccessHeroDungeonLevel(
      ConfigDataHeroDungeonLevelInfo levelInfo,
      List<int> newGotAchievementRelationInds,
      int stars,
      List<int> battleTreasures)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int RaidHeroDungeonLevel(int leveld, int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsLevelChallenged(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ushort GetDSVersion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMaxFinishedLevelId(int chapterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public HeroDungeonComponent.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private string __callBase_GetName()
    {
      return this.GetName();
    }

    private void __callBase_Init()
    {
      base.Init();
    }

    private void __callBase_PostInit()
    {
      base.PostInit();
    }

    private void __callBase_DeInit()
    {
      base.DeInit();
    }

    private void __callBase_Tick(uint deltaMillisecond)
    {
      this.Tick(deltaMillisecond);
    }

    private void __callBase_PostDeSerialize()
    {
      this.PostDeSerialize();
    }

    private bool __callBase_HasGotAchievementRelationId(int achievementRelationId)
    {
      return this.HasGotAchievementRelationId(achievementRelationId);
    }

    private bool __callBase_IsLevelFirstPass(int levelId)
    {
      return this.IsLevelFirstPass(levelId);
    }

    private bool __callBase_IsFinishedLevel(int levelId)
    {
      return this.IsFinishedLevel(levelId);
    }

    private int __callBase_GetLevelCanChallengeMaxNums(ConfigDataHeroDungeonLevelInfo levelInfo)
    {
      return this.GetLevelCanChallengeMaxNums(levelInfo);
    }

    private int __callBase_GetDailyChallengeMaxNums()
    {
      return this.GetDailyChallengeMaxNums();
    }

    private void __callBase_InitLevel(int chapterId, int levelId, int stars, int nums)
    {
      this.InitLevel(chapterId, levelId, stars, nums);
    }

    private void __callBase_SetLevel(int chapterId, int LevelId, int stars, int nums)
    {
      this.SetLevel(chapterId, LevelId, stars, nums);
    }

    private HeroDungeonLevel __callBase_FindLevel(int chapterId, int levelId)
    {
      return this.FindLevel(chapterId, levelId);
    }

    private int __callBase_AttackHeroDungeonLevel(int levelId)
    {
      return this.AttackHeroDungeonLevel(levelId);
    }

    private void __callBase_SetSuccessHeroDungeonLevel(
      ConfigDataHeroDungeonLevelInfo levelInfo,
      List<int> newGotAchievementRelationInds,
      int stars,
      List<int> battleTreasures)
    {
      base.SetSuccessHeroDungeonLevel(levelInfo, newGotAchievementRelationInds, stars, battleTreasures);
    }

    private void __callBase_SetRaidSuccessHeroDungeonLevel(ConfigDataHeroDungeonLevelInfo levelInfo)
    {
      this.SetRaidSuccessHeroDungeonLevel(levelInfo);
    }

    private int __callBase_CanAttackHeroDungeonLevel(int levelId)
    {
      return this.CanAttackHeroDungeonLevel(levelId);
    }

    private int __callBase_CanRaidHeroDungeonLevel(int levelId)
    {
      return this.CanRaidHeroDungeonLevel(levelId);
    }

    private int __callBase_CanUnlockHeroDungeonLevel(int levelId)
    {
      return this.CanUnlockHeroDungeonLevel(levelId);
    }

    private int __callBase_GetHeroDungeonChapterStar(int chapterId)
    {
      return this.GetHeroDungeonChapterStar(chapterId);
    }

    private bool __callBase_HasGotChapterStarReward(int chapterId, int index)
    {
      return this.HasGotChapterStarReward(chapterId, index);
    }

    private int __callBase_CanGainHeroDungeonChapterStarRewards(int chapterId, int index)
    {
      return this.CanGainHeroDungeonChapterStarRewards(chapterId, index);
    }

    private List<Goods> __callBase_GetHeroDungeonChapterStarRewards(
      ConfigDataHeroInformationInfo chapterInfo,
      int index)
    {
      return this.GetHeroDungeonChapterStarRewards(chapterInfo, index);
    }

    private int __callBase_GainHeroDungeonChapterStarRewards(int chapterId, int index, bool check)
    {
      return this.GainHeroDungeonChapterStarRewards(chapterId, index, check);
    }

    private void __callBase_GenerateHeroDungeonChapterStarRewards(int chapterId, int index)
    {
      this.GenerateHeroDungeonChapterStarRewards(chapterId, index);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private HeroDungeonComponent m_owner;

      public LuaExportHelper(HeroDungeonComponent owner)
      {
        this.m_owner = owner;
      }

      public string __callBase_GetName()
      {
        return this.m_owner.__callBase_GetName();
      }

      public void __callBase_Init()
      {
        this.m_owner.__callBase_Init();
      }

      public void __callBase_PostInit()
      {
        this.m_owner.__callBase_PostInit();
      }

      public void __callBase_DeInit()
      {
        this.m_owner.__callBase_DeInit();
      }

      public void __callBase_Tick(uint deltaMillisecond)
      {
        this.m_owner.__callBase_Tick(deltaMillisecond);
      }

      public void __callBase_PostDeSerialize()
      {
        this.m_owner.__callBase_PostDeSerialize();
      }

      public bool __callBase_HasGotAchievementRelationId(int achievementRelationId)
      {
        return this.m_owner.__callBase_HasGotAchievementRelationId(achievementRelationId);
      }

      public bool __callBase_IsLevelFirstPass(int levelId)
      {
        return this.m_owner.__callBase_IsLevelFirstPass(levelId);
      }

      public bool __callBase_IsFinishedLevel(int levelId)
      {
        return this.m_owner.__callBase_IsFinishedLevel(levelId);
      }

      public int __callBase_GetLevelCanChallengeMaxNums(ConfigDataHeroDungeonLevelInfo levelInfo)
      {
        return this.m_owner.__callBase_GetLevelCanChallengeMaxNums(levelInfo);
      }

      public int __callBase_GetDailyChallengeMaxNums()
      {
        return this.m_owner.__callBase_GetDailyChallengeMaxNums();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void __callBase_InitLevel(int chapterId, int levelId, int stars, int nums)
      {
        // ISSUE: unable to decompile the method.
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void __callBase_SetLevel(int chapterId, int LevelId, int stars, int nums)
      {
        // ISSUE: unable to decompile the method.
      }

      public HeroDungeonLevel __callBase_FindLevel(int chapterId, int levelId)
      {
        return this.m_owner.__callBase_FindLevel(chapterId, levelId);
      }

      public int __callBase_AttackHeroDungeonLevel(int levelId)
      {
        return this.m_owner.__callBase_AttackHeroDungeonLevel(levelId);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void __callBase_SetSuccessHeroDungeonLevel(
        ConfigDataHeroDungeonLevelInfo levelInfo,
        List<int> newGotAchievementRelationInds,
        int stars,
        List<int> battleTreasures)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_SetRaidSuccessHeroDungeonLevel(ConfigDataHeroDungeonLevelInfo levelInfo)
      {
        this.m_owner.__callBase_SetRaidSuccessHeroDungeonLevel(levelInfo);
      }

      public int __callBase_CanAttackHeroDungeonLevel(int levelId)
      {
        return this.m_owner.__callBase_CanAttackHeroDungeonLevel(levelId);
      }

      public int __callBase_CanRaidHeroDungeonLevel(int levelId)
      {
        return this.m_owner.__callBase_CanRaidHeroDungeonLevel(levelId);
      }

      public int __callBase_CanUnlockHeroDungeonLevel(int levelId)
      {
        return this.m_owner.__callBase_CanUnlockHeroDungeonLevel(levelId);
      }

      public int __callBase_GetHeroDungeonChapterStar(int chapterId)
      {
        return this.m_owner.__callBase_GetHeroDungeonChapterStar(chapterId);
      }

      public bool __callBase_HasGotChapterStarReward(int chapterId, int index)
      {
        return this.m_owner.__callBase_HasGotChapterStarReward(chapterId, index);
      }

      public int __callBase_CanGainHeroDungeonChapterStarRewards(int chapterId, int index)
      {
        return this.m_owner.__callBase_CanGainHeroDungeonChapterStarRewards(chapterId, index);
      }

      public List<Goods> __callBase_GetHeroDungeonChapterStarRewards(
        ConfigDataHeroInformationInfo chapterInfo,
        int index)
      {
        return this.m_owner.__callBase_GetHeroDungeonChapterStarRewards(chapterInfo, index);
      }

      public int __callBase_GainHeroDungeonChapterStarRewards(int chapterId, int index, bool check)
      {
        return this.m_owner.__callBase_GainHeroDungeonChapterStarRewards(chapterId, index, check);
      }

      public void __callBase_GenerateHeroDungeonChapterStarRewards(int chapterId, int index)
      {
        this.m_owner.__callBase_GenerateHeroDungeonChapterStarRewards(chapterId, index);
      }

      public void InitChapters(List<ProHeroDungeonChapter> pbChapters)
      {
        this.m_owner.InitChapters(pbChapters);
      }

      public void InitChapter(ProHeroDungeonChapter pbChapter)
      {
        this.m_owner.InitChapter(pbChapter);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void SetSuccessHeroDungeonLevel(
        ConfigDataHeroDungeonLevelInfo levelInfo,
        List<int> newGotAchievementRelationInds,
        int stars,
        List<int> battleTreasures)
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
