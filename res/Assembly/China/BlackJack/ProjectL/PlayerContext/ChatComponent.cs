﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.ChatComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  [CustomLuaClass]
  public class ChatComponent : ChatComponentCommon
  {
    private static int s_chatRecentLinkManMaxCount = 10;
    private static int s_chatRecentLinkChatGroupCount = 10;
    public int m_loadChatHistoryMessageCount;
    public List<ChatComponent.ChatMessageClient> m_worldChatMsgList;
    public List<ChatComponent.ChatMessageClient> m_sysChatMsgList;
    public List<ChatComponent.ChatMessageClient> m_teamChatMsgList;
    public List<ChatComponent.ChatMessageClient> m_guildChatMsgList;
    public Dictionary<string, List<ChatComponent.ChatMessageClient>> m_groupChatMsgDict;
    public List<string> m_recentLinkChatGroupIdList;
    public Dictionary<string, List<ChatComponent.ChatMessageClient>> m_privateChatMsgDict;
    public List<string> m_recentPrivateChatPlayerIdList;
    public Dictionary<string, UserSummary> m_recentLinkPlayerInfoDict;
    public Dictionary<string, bool> m_privateChatHistoryStateList;
    public Dictionary<string, bool> m_groupChatHistoryStateList;
    public string m_currGroupChatGroupId;
    public string m_currPrivateChatPlayerGameUserId;
    public ChatGroupComponent m_chatGroupComponent;
    public FriendComponent m_friendComponent;
    private BagComponent m_bagComponent;
    public DateTime m_guildChatLastReadTime;
    public int m_currRoomIndex;
    private bool m_dataDirtyState;
    private bool m_isLoadHistoryRecordSuccess;
    private string m_chatDirectoryPath;
    private string s_chatDataSavePath;
    protected static int s_chatHistoryRecordTimeLimit;
    protected static int s_chatHistoryRecordCountLimit;
    protected static int s_chatSaveHistoryRecordCountOnClear;

    [MethodImpl((MethodImplOptions) 32768)]
    public ChatComponent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    public override void DeInit()
    {
      base.DeInit();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeSerialize(DSChatNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ReadChatBeforeDate(ChatChannel type, DateTime date)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override int CanSendChatMessage(int channelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanSendDanmaku(DateTime lastDanmakuSendTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void NotifyGetPlayerInfo(List<ProUserSummary> userInfoList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ChatMessage NotifyChatMessage(ChatMessageNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ChatComponent.ChatMessageClient CreateASystemTip(
      ChatChannel channel,
      string tips)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsHasHistoryRecord4Chat(ChatChannel channel, string id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void LoadChatHistoryData(DateTime currTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SaveChatHistoryData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetSaveFileName(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetGroupChatTarget()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGroupChatTarget(string newGroupId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPrivateChatTarget(string gameUserId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public UserSummary GetUserInfo(string gameUserId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddUserInfo(UserSummary userSummary)
    {
      // ISSUE: unable to decompile the method.
    }

    public void NotifyEnterNewRoom(int roomindex)
    {
      this.m_currRoomIndex = roomindex;
    }

    public ushort GetDSVersion()
    {
      return this.m_chatDS.ClientVersion;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ChatComponent.ChatMessageClient> GetChatMessageList(
      ChatChannel channel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetUnReadChatMsgCount4PointPlayerOrGroup(ChatChannel channel, string id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public KeyValuePair<List<string>, List<string>> GetRecentChatTargetList(
      ChatChannel channel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetAllUnReadChatMsgCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetGroupUnreadChatMsgCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetAssignGroupUnreadChatMsgCount(string groupID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetAssignPrivateUnreadChatMsgCount(string userID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPrivateUnreadChatMsgCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetGuildUnreadChatMsgCount()
    {
      // ISSUE: unable to decompile the method.
    }

    public void ReadChat(ChatComponent.ChatMessageClient msg)
    {
      msg.isRead = true;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRecentTeamMsgRead()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGroupChatMsgReadByID(string groupID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPrivateChatMsgReadByID(string userID)
    {
      // ISSUE: unable to decompile the method.
    }

    private void ClearChatMessage4LimitCondition(DateTime currTime)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddMsgToGroupChatDict(ChatComponent.ChatMessageClient chatMessage)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddMsgToPrivateChatDict(ChatComponent.ChatMessageClient chatMessage)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private ChatMessage NotifySystemChatMessage(ChatTextMessage chatMessage)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string CombineSystemSelectHeroMessage(int contentId, string sourceContent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string CombineSystemSelectEquipmentMessage(int contentId, string sourceContent)
    {
      // ISSUE: unable to decompile the method.
    }

    private ChatMessage NotifyWorldChatMessage(ChatMessage chatMessage)
    {
      return chatMessage;
    }

    private ChatMessage NotifyTeamChatMessage(ChatMessage chatMessage)
    {
      return chatMessage;
    }

    private ChatMessage NotifyGroupChatMessage(ChatMessage chatMessage)
    {
      return chatMessage;
    }

    private ChatMessage NotifyPrivateChatMessage(ChatMessage chatMessage)
    {
      return chatMessage;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private ChatMessage NotifyGuildChatMessage(ChatMessage chatMessage)
    {
      // ISSUE: unable to decompile the method.
    }

    public int RoomIndex
    {
      get
      {
        return this.m_currRoomIndex;
      }
    }

    public bool DataDirty
    {
      get
      {
        return this.m_dataDirtyState;
      }
      set
      {
        this.m_dataDirtyState = value;
      }
    }

    [CustomLuaClass]
    [Serializable]
    public class ChatMessageClient
    {
      public ChatMessage ChatMessageInfo;
      public bool isRead;
      public bool isHistoryRecord;
      public bool isLocalSystemTip;

      [MethodImpl((MethodImplOptions) 32768)]
      public ChatMessageClient(ChatMessage msgInfo)
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [CustomLuaClass]
    [Serializable]
    public class ChatSaveData
    {
      public List<string> m_recentLinkManIdList = new List<string>();
      public List<string> m_recentLinkGroupIdList = new List<string>();
      public Dictionary<string, List<ChatComponent.ChatMessageClient>> m_privateChatDataDict = new Dictionary<string, List<ChatComponent.ChatMessageClient>>();
      public Dictionary<string, List<ChatComponent.ChatMessageClient>> m_groupChatDataDict = new Dictionary<string, List<ChatComponent.ChatMessageClient>>();
      public string m_userId;
      public DateTime m_guildChatLastReadTime;
    }
  }
}
