﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.RandomStoreComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.PlayerContext
{
  [HotFix]
  public class RandomStoreComponent : RandomStoreComponentCommon
  {
    [DoNotToLua]
    private RandomStoreComponent.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_Init_hotfix;
    private LuaFunction m_PostInit_hotfix;
    private LuaFunction m_DeInit_hotfix;
    private LuaFunction m_DeSerializeDSRandomStoreNtf_hotfix;
    private LuaFunction m_InitStoresList`1_hotfix;
    private LuaFunction m_GetDSVersion_hotfix;
    private LuaFunction m_IsStoreInfoEmpty_hotfix;
    private LuaFunction m_SetStoreRandomStore_hotfix;
    private LuaFunction m_GetManualFlushNumsInt32_hotfix;
    private LuaFunction m_CanManualFlushRandomStoreRandomStore_hotfix;
    private LuaFunction m_ManualFlushRandomStoreRandomStore_hotfix;
    private LuaFunction m_GetStoreInt32_hotfix;
    private LuaFunction m_GetStoreNextFlushTimeInt32_hotfix;
    private LuaFunction m_BuyStoreItemInt32Int32_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public RandomStoreComponent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeSerialize(DSRandomStoreNtf ntf)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitStores(List<ProRandomStore> pbStores)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ushort GetDSVersion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsStoreInfoEmpty()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetStore(RandomStore store)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetManualFlushNums(int storeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanManualFlushRandomStore(RandomStore store)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ManualFlushRandomStore(RandomStore store)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RandomStore GetStore(int storeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetStoreNextFlushTime(int storeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int BuyStoreItem(int storeId, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public RandomStoreComponent.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private string __callBase_GetName()
    {
      return this.GetName();
    }

    private void __callBase_Init()
    {
      base.Init();
    }

    private void __callBase_PostInit()
    {
      base.PostInit();
    }

    private void __callBase_DeInit()
    {
      base.DeInit();
    }

    private void __callBase_Tick(uint deltaMillisecond)
    {
      this.Tick(deltaMillisecond);
    }

    private void __callBase_PostDeSerialize()
    {
      this.PostDeSerialize();
    }

    private void __callBase_OnFlushManualFlushNums()
    {
      this.OnFlushManualFlushNums();
    }

    private bool __callBase_CanAutoFlushStore(RandomStore store)
    {
      return this.CanAutoFlushStore(store);
    }

    private bool __callBase_IsBoughtStoreItem(RandomStoreItem storeItem)
    {
      return this.IsBoughtStoreItem(storeItem);
    }

    private int __callBase_CanManualFlushStore(
      RandomStore store,
      ConfigDataRandomStoreInfo storeInfo)
    {
      return this.CanManualFlushStore(store, storeInfo);
    }

    private int __callBase_CanBuyRandomStoreItem(int storeId, int index, int selectedIndex)
    {
      return this.CanBuyRandomStoreItem(storeId, index, selectedIndex);
    }

    private void __callBase_OnBuyStoreItem(int storeId, int goodsId)
    {
      this.OnBuyStoreItem(storeId, goodsId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private RandomStoreComponent m_owner;

      public LuaExportHelper(RandomStoreComponent owner)
      {
        this.m_owner = owner;
      }

      public string __callBase_GetName()
      {
        return this.m_owner.__callBase_GetName();
      }

      public void __callBase_Init()
      {
        this.m_owner.__callBase_Init();
      }

      public void __callBase_PostInit()
      {
        this.m_owner.__callBase_PostInit();
      }

      public void __callBase_DeInit()
      {
        this.m_owner.__callBase_DeInit();
      }

      public void __callBase_Tick(uint deltaMillisecond)
      {
        this.m_owner.__callBase_Tick(deltaMillisecond);
      }

      public void __callBase_PostDeSerialize()
      {
        this.m_owner.__callBase_PostDeSerialize();
      }

      public void __callBase_OnFlushManualFlushNums()
      {
        this.m_owner.__callBase_OnFlushManualFlushNums();
      }

      public bool __callBase_CanAutoFlushStore(RandomStore store)
      {
        return this.m_owner.__callBase_CanAutoFlushStore(store);
      }

      public bool __callBase_IsBoughtStoreItem(RandomStoreItem storeItem)
      {
        return this.m_owner.__callBase_IsBoughtStoreItem(storeItem);
      }

      public int __callBase_CanManualFlushStore(
        RandomStore store,
        ConfigDataRandomStoreInfo storeInfo)
      {
        return this.m_owner.__callBase_CanManualFlushStore(store, storeInfo);
      }

      public int __callBase_CanBuyRandomStoreItem(int storeId, int index, int selectedIndex)
      {
        return this.m_owner.__callBase_CanBuyRandomStoreItem(storeId, index, selectedIndex);
      }

      public void __callBase_OnBuyStoreItem(int storeId, int goodsId)
      {
        this.m_owner.__callBase_OnBuyStoreItem(storeId, goodsId);
      }
    }
  }
}
