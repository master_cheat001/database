﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.OperationalActivityCompoment
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  [CustomLuaClass]
  public class OperationalActivityCompoment : OperationalActivityCompomentCommon
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public override void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    public override void PostInit()
    {
      base.PostInit();
    }

    public override void DeInit()
    {
      base.DeInit();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ExchangeItemGroup(ulong operationalActivityInstanceId, int itemGroupIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GainReward(ulong operationalActivityInstanceId, int rewardIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Deserialize(DSOperationalActivityNtf ntf)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Deserialize(DSAnnouncementNtf ntf)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Deserialize(AdvertisementFlowLayoutUpdateNtf ntf)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void PostDeSerialize()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ResetAllEffectFlag()
    {
      // ISSUE: unable to decompile the method.
    }

    public ushort GetDSVersion()
    {
      return this.m_operationalActivityDS.ClientVersion;
    }

    public ushort GetAnnouncementDSVersion()
    {
      return this.m_announcementDS.ClientVersion;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool AddNewOperationalActivity(
      ProOperationalActivityBasicInfo operationalActivityBasicInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool AddRedeemActivity(int activityId, RedeemInfoAck info)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool AddFansRewardFromPBTCBTActivity(
      int activityID,
      long startDate,
      long endDate,
      bool isClaimed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddAnnouncement(ProAnnouncement pbAnnouncement)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateOperationalActivity(
      ProOperationalActivityBasicInfo opertioanlActivityInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Announcement> GetValidAnnouncements()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveGlobalAnnouncement(ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveAllExpiredAnnouncements()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
