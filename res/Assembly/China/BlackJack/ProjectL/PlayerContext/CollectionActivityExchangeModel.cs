﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.CollectionActivityExchangeModel
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.PlayerContext
{
  [HotFix]
  public class CollectionActivityExchangeModel
  {
    private bool m_isInitialized;
    private IConfigDataLoader m_configDataLoader;
    private CollectionActivityComponent m_collectionComponent;
    private BagComponent m_bagComponent;
    private PlayerBasicInfoComponent m_playerBasicInfoComponent;
    private CollectionActivity m_currentActivity;
    private int m_currentActivityRid;
    private string m_currentActivityName;
    private string m_exchangePanelBgResPath;
    private string m_exchangePanelStateName;
    private Stack<CollectionActivityCurrency> m_currencyPoolStack;
    private Stack<CollectionActivityPage> m_pagePoolStack;
    private Stack<CollectionActivityMission> m_missionPoolStack;
    private Stack<CollectionActivityItem> m_itemPoolStack;
    private Dictionary<int, List<ConfigDataCollectionActivityExchangeTableInfo>> m_exchangeGroupMap;
    public List<CollectionActivityPage> pageList;
    public List<CollectionActivityCurrency> currencyList;
    [DoNotToLua]
    private CollectionActivityExchangeModel.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_Init_hotfix;
    private LuaFunction m_UpdateExchangeGroupMap_hotfix;
    private LuaFunction m_UpdateCachedDataUInt64_hotfix;
    private LuaFunction m_NewResource_hotfix;
    private LuaFunction m_NewPage_hotfix;
    private LuaFunction m_NewMission_hotfix;
    private LuaFunction m_NewItem_hotfix;
    private LuaFunction m_ClearCachedData_hotfix;
    private LuaFunction m_get_CurrentActivity_hotfix;
    private LuaFunction m_get_CurrentActivityRid_hotfix;
    private LuaFunction m_get_CurrentActivityName_hotfix;
    private LuaFunction m_get_ExchangePanelBgResPath_hotfix;
    private LuaFunction m_get_ExchangePanelStateName_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivityExchangeModel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateExchangeGroupMap()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateCachedData(ulong activityUid)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private CollectionActivityCurrency NewResource()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private CollectionActivityPage NewPage()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private CollectionActivityMission NewMission()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private CollectionActivityItem NewItem()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearCachedData()
    {
      // ISSUE: unable to decompile the method.
    }

    public CollectionActivity CurrentActivity
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int CurrentActivityRid
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public string CurrentActivityName
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public string ExchangePanelBgResPath
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public string ExchangePanelStateName
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public CollectionActivityExchangeModel.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private CollectionActivityExchangeModel m_owner;

      public LuaExportHelper(CollectionActivityExchangeModel owner)
      {
        this.m_owner = owner;
      }

      public bool m_isInitialized
      {
        get
        {
          return this.m_owner.m_isInitialized;
        }
        set
        {
          this.m_owner.m_isInitialized = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public CollectionActivityComponent m_collectionComponent
      {
        get
        {
          return this.m_owner.m_collectionComponent;
        }
        set
        {
          this.m_owner.m_collectionComponent = value;
        }
      }

      public BagComponent m_bagComponent
      {
        get
        {
          return this.m_owner.m_bagComponent;
        }
        set
        {
          this.m_owner.m_bagComponent = value;
        }
      }

      public PlayerBasicInfoComponent m_playerBasicInfoComponent
      {
        get
        {
          return this.m_owner.m_playerBasicInfoComponent;
        }
        set
        {
          this.m_owner.m_playerBasicInfoComponent = value;
        }
      }

      public CollectionActivity m_currentActivity
      {
        get
        {
          return this.m_owner.m_currentActivity;
        }
        set
        {
          this.m_owner.m_currentActivity = value;
        }
      }

      public int m_currentActivityRid
      {
        get
        {
          return this.m_owner.m_currentActivityRid;
        }
        set
        {
          this.m_owner.m_currentActivityRid = value;
        }
      }

      public string m_currentActivityName
      {
        get
        {
          return this.m_owner.m_currentActivityName;
        }
        set
        {
          this.m_owner.m_currentActivityName = value;
        }
      }

      public string m_exchangePanelBgResPath
      {
        get
        {
          return this.m_owner.m_exchangePanelBgResPath;
        }
        set
        {
          this.m_owner.m_exchangePanelBgResPath = value;
        }
      }

      public string m_exchangePanelStateName
      {
        get
        {
          return this.m_owner.m_exchangePanelStateName;
        }
        set
        {
          this.m_owner.m_exchangePanelStateName = value;
        }
      }

      public Stack<CollectionActivityCurrency> m_currencyPoolStack
      {
        get
        {
          return this.m_owner.m_currencyPoolStack;
        }
        set
        {
          this.m_owner.m_currencyPoolStack = value;
        }
      }

      public Stack<CollectionActivityPage> m_pagePoolStack
      {
        get
        {
          return this.m_owner.m_pagePoolStack;
        }
        set
        {
          this.m_owner.m_pagePoolStack = value;
        }
      }

      public Stack<CollectionActivityMission> m_missionPoolStack
      {
        get
        {
          return this.m_owner.m_missionPoolStack;
        }
        set
        {
          this.m_owner.m_missionPoolStack = value;
        }
      }

      public Stack<CollectionActivityItem> m_itemPoolStack
      {
        get
        {
          return this.m_owner.m_itemPoolStack;
        }
        set
        {
          this.m_owner.m_itemPoolStack = value;
        }
      }

      public Dictionary<int, List<ConfigDataCollectionActivityExchangeTableInfo>> m_exchangeGroupMap
      {
        get
        {
          return this.m_owner.m_exchangeGroupMap;
        }
        set
        {
          this.m_owner.m_exchangeGroupMap = value;
        }
      }

      public void Init()
      {
        this.m_owner.Init();
      }

      public void UpdateExchangeGroupMap()
      {
        this.m_owner.UpdateExchangeGroupMap();
      }

      public CollectionActivityCurrency NewResource()
      {
        return this.m_owner.NewResource();
      }

      public CollectionActivityPage NewPage()
      {
        return this.m_owner.NewPage();
      }

      public CollectionActivityMission NewMission()
      {
        return this.m_owner.NewMission();
      }

      public CollectionActivityItem NewItem()
      {
        return this.m_owner.NewItem();
      }

      public void ClearCachedData()
      {
        this.m_owner.ClearCachedData();
      }
    }
  }
}
