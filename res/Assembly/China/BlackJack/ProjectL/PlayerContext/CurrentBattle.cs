﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.CurrentBattle
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  [CustomLuaClass]
  public class CurrentBattle
  {
    public BattleType BattleType;
    public ConfigDataBattleInfo BattleInfo;
    public ConfigDataPVPBattleInfo PVPBattleInfo;
    public ConfigDataRealTimePVPBattleInfo RealTimePVPBattleInfo;
    public RealTimePVPBattleReport RealTimePVPBattleReport;
    public ConfigDataPeakArenaBattleInfo PeakArenaBattleInfo;
    public PeakArenaLadderBattleReport PeakArenaBattleReport;
    public string PeakArenaBattleReportUserId;
    public PeakArenaBattleReportSourceType PeakArenaBattleReportSourceType;
    public int PeakArenaMatchGroupId;
    public ConfigDataArenaBattleInfo ArenaBattleInfo;
    public ConfigDataArenaDefendRuleInfo ArenaDefendRuleInfo;
    public int ArenaDefenderPlayerLevel;
    public List<BattleHero> ArenaDefenderHeros;
    public List<TrainingTech> ArenaDefenderTrainTechs;
    public ArenaBattleReport ArenaBattleReport;
    public bool IsArenaRevenge;

    [MethodImpl((MethodImplOptions) 32768)]
    public void Clear()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetPrepareMusic()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetBattleMusic()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetDefendMusic()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsAnyBattleReport()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
