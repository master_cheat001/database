﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.PeakArenaBattleLiveRoomInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Common;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  public class PeakArenaBattleLiveRoomInfo
  {
    public long RoomCreateTime;
    public DateTime RoomCreateDateTime;
    public List<int> WinPlayerIndexes;
    public List<UserSummary> Players;
    public List<string> FirstHandUserIds;

    public int GatBattleCount()
    {
      return this.WinPlayerIndexes.Count;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetLeftPlayerBattleScore()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetRightPlayerBattleScore()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
