﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.BattleRoom
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.PlayerContext
{
  [HotFix]
  public class BattleRoom
  {
    public ulong RoomId;
    public ulong LiveRoomId;
    public BattleRoomType BattleRoomType;
    public int BattleId;
    public GameFunctionType GameFunctionType;
    public int LocationId;
    public BattleRoomStatus BattleRoomStatus;
    public BattleLiveRoomStatus BattleLiveRoomStatus;
    public DateTime ReadyTimeout;
    public DateTime LastPlayerBeginActionTime;
    public List<BattleRoomPlayer> Players;
    public BattleRoomPlayerHeroSetup BattleRoomPlayerHeroSetup;
    public int MyPlayerIndex;
    public int LeaderPlayerIndex;
    public List<BattleCommand> BattleCommands;
    public List<BattleRoomQuitNtf> PlayerQuitNtfs;
    public int BattleStars;
    public BattleReward BattleReward;
    public int PVPWinPlayerIndex;
    public RealTimePVPMode RealtimePVPBattleMode;
    public BattleRoomBPRule RealtimePVPBPRule;
    public int RealtimePVPBPTurn;
    public BattleRoomBPStatus RealtimePVPBPStatus;
    public DateTime RealtimePVPLatestTurnChangeDateTime;
    public ulong RealtimePVPBattleInstanceId;
    public List<int> GuildMassiveCombatPreferredHeroTagIds;
    public ulong GuildMassiveCombatInstanceId;
    public RealTimePVPMode PeakArenaMode;
    public int PeakArenaRound;
    public int PeakArenaBoRound;
    public int PeakArenaPlayOffMatchupId;
    public List<string> PeakArenaWinPlayerUserIds;
    public List<DateTime> PeakArenaBattleEndTimes;
    public BPStage PeakArenaBPStage;
    public BPStagePeakArenaRule PeakArenaBPStageRule;
    public DateTime PeakArenaLastTickBPStageCountdownTime;
    public long PeakArenaBattleTurnActionTimeSpan;
    public DateTime PeakArenaLastTickBattleActionCountdownTime;
    public ulong PeakArenaBattleInstanceId;
    public string LivePeakArenaBPStageHeros0UserId;
    public List<BPStageHero> LivePeakArenaBPStageHeros0;
    public List<BPStageHero> LivePeakArenaBPStageHeros1;
    public DateTime LivePeakArenaBoRoundStartTime;
    public string LivePeakArenaForfeitUserId;
    public TimeSpan LivePeakArenaBattleTurnActionTimeSpanFix;
    [DoNotToLua]
    private BattleRoom.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_Clear_hotfix;
    private LuaFunction m_InitPlayersList`1_hotfix;
    private LuaFunction m_InitPlayersList`1_hotfix;
    private LuaFunction m_InitPlayersList`1_hotfix;
    private LuaFunction m_InitPeakArenaBPStageIConfigDataLoaderInt32BPStage_hotfix;
    private LuaFunction m_FindPlayerBySessionIdUInt64_hotfix;
    private LuaFunction m_FindPlayerByUserIdString_hotfix;
    private LuaFunction m_SetMyPlayerString_hotfix;
    private LuaFunction m_SetLeaderPlayerString_hotfix;
    private LuaFunction m_FindPlayerIndexByUserIdString_hotfix;
    private LuaFunction m_FindPlayerIndexBySessiongIdUInt64_hotfix;
    private LuaFunction m_GetMyPlayer_hotfix;
    private LuaFunction m_IsTeamOrGuildMassiveCombatRoomType_hotfix;
    private LuaFunction m_IsAnyPVPBattleRoomType_hotfix;
    private LuaFunction m_IsRealTimePVPBattleRoomType_hotfix;
    private LuaFunction m_IsPeakArenaBattleRoomType_hotfix;
    private LuaFunction m_IsPeakArenaPlayOffMatch_hotfix;
    private LuaFunction m_IsAnyPVPOrPeakArenaBattleRoomType_hotfix;
    private LuaFunction m_IsBattleRoomStatusBPStageOrReady_hotfix;
    private LuaFunction m_GetPlayerIndexInCurrentPickTurn_hotfix;
    private LuaFunction m_GetHeroSetupCountMaxInPickTurnInt32_hotfix;
    private LuaFunction m_GetHeroSetupCountInCurrentPickTurn_hotfix;
    private LuaFunction m_GetRemainHeroSetupCountInCurrentPickTurn_hotfix;
    private LuaFunction m_GetHeroSetupFlagCountInt32SetupBattleHeroFlag_hotfix;
    private LuaFunction m_IsHeroSetupHasFlagInt32Int32SetupBattleHeroFlag_hotfix;
    private LuaFunction m_TickPeakArenaBPStageCountdown_hotfix;
    private LuaFunction m_TickPeakArenaBPStageCountdownTimeSpan_hotfix;
    private LuaFunction m_TickPeakArenaBattleActionCountdownInt32_hotfix;
    private LuaFunction m_TickPeakArenaBattleActionCountdownTimeSpanInt32_hotfix;
    private LuaFunction m_SetPeakArenaWinPlayerIdInt32String_hotfix;
    private LuaFunction m_IsLive_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Clear()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitPlayers(List<ProBattleRoomPlayer> players)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitPlayers(List<ProUserSummary> players)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitPlayers(List<UserSummary> players)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitPeakArenaBPStage(
      IConfigDataLoader configDataLoader,
      int randomSeed,
      BPStage bpStage = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleRoomPlayer FindPlayerBySessionId(ulong sessionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleRoomPlayer FindPlayerByUserId(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetMyPlayer(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLeaderPlayer(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int FindPlayerIndexByUserId(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int FindPlayerIndexBySessiongId(ulong sessionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleRoomPlayer GetMyPlayer()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsTeamOrGuildMassiveCombatRoomType()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsAnyPVPBattleRoomType()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsRealTimePVPBattleRoomType()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPeakArenaBattleRoomType()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPeakArenaPlayOffMatch()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsAnyPVPOrPeakArenaBattleRoomType()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBattleRoomStatusBPStageOrReady()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPlayerIndexInCurrentPickTurn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroSetupCountMaxInPickTurn(int turn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroSetupCountInCurrentPickTurn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetRemainHeroSetupCountInCurrentPickTurn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroSetupFlagCount(int playerIndex, SetupBattleHeroFlag flag)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsHeroSetupHasFlag(int playerIndex, int heroId, SetupBattleHeroFlag flag)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void TickPeakArenaBPStageCountdown()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void TickPeakArenaBPStageCountdown(TimeSpan ts)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void TickPeakArenaBattleActionCountdown(int playerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void TickPeakArenaBattleActionCountdown(TimeSpan ts, int playerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPeakArenaWinPlayerId(int boRound, string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsLive()
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public BattleRoom.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private BattleRoom m_owner;

      public LuaExportHelper(BattleRoom owner)
      {
        this.m_owner = owner;
      }
    }
  }
}
