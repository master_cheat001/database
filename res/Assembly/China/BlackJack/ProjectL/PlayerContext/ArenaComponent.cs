﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.ArenaComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  [CustomLuaClass]
  public class ArenaComponent : ArenaComponentCommon
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public override void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    public override void PostInit()
    {
      base.PostInit();
    }

    public override void DeInit()
    {
      base.DeInit();
    }

    public override void Tick(uint deltaMillisecond)
    {
      base.Tick(deltaMillisecond);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetArenaTicketNextGivenTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeSerialize(DSArenaNtf ntf)
    {
      // ISSUE: unable to decompile the method.
    }

    public ushort GetDSVersion()
    {
      return this.m_arenaDS.ClientVersion;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddArenaBattleReportPlayBackData(ProArenaBattleReport pbArenaBattleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartArenaBattle(ProArenaDefensiveBattleInfo pbBattleInfo, bool autoBattle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ReconnectArenaBattle(ProArenaDefensiveBattleInfo pbBattleInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RevengeOpponent(
      ulong battleReportInstanceId,
      bool autoBattle,
      ProArenaDefensiveBattleInfo pbBattleInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnStartArenaBattle(ArenaOpponentDefensiveBattleInfo battleInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDefensiveTeam(ProArenaPlayerDefensiveTeam pbDefensiveTeamInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FinsihArenaBattle(bool isWin)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FlushOpponents(List<ProArenaOpponent> pbOpponents, long nextFlushTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitArenaPlayInfo(ProArenaPlayerInfo pbArenaPlayerInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitBattleReportBasicInfo(
      List<ProArenaBattleReport> pbArenaBattleReports,
      int nextBattleReportIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    public void GainVictoryPointsReward(int victoryPointsRewardIndex)
    {
      this.AddReceivedVictoryPointsRewardIndex(victoryPointsRewardIndex);
    }

    public ArenaPlayerInfo GetArenaPlayerInfo()
    {
      return this.m_arenaDS.ArenaPlayerInfo;
    }

    public ArenaBattleReport GetArenaBattleReport(ulong instanceId)
    {
      return this.m_arenaBattleReportDS.FindArenaBattleReportByInstanceId(instanceId);
    }
  }
}
