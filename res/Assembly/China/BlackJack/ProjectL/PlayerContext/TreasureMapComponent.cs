﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.TreasureMapComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.PlayerContext
{
  [HotFix]
  public class TreasureMapComponent : TreasureMapComponentCommon
  {
    [DoNotToLua]
    private TreasureMapComponent.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_Init_hotfix;
    private LuaFunction m_PostInit_hotfix;
    private LuaFunction m_DeInit_hotfix;
    private LuaFunction m_DeSerializeDSTreasureMapNtf_hotfix;
    private LuaFunction m_GetDSVersion_hotfix;
    private LuaFunction m_GetCurrentTicketNums_hotfix;
    private LuaFunction m_FinishedTreasureLevelInt32BooleanList`1Int32_hotfix;
    private LuaFunction m_SetSuccessTreasureMapLevelConfigDataTreasureLevelInfoList`1_hotfix;
    private LuaFunction m_GetMaxFinishedLevelId_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public TreasureMapComponent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeSerialize(DSTreasureMapNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ushort GetDSVersion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetCurrentTicketNums()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int FinishedTreasureLevel(
      int levelId,
      bool isWin,
      List<int> battleTreasureIds,
      int killGoblinNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void SetSuccessTreasureMapLevel(
      ConfigDataTreasureLevelInfo levelInfo,
      List<int> battleTreasures)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMaxFinishedLevelId()
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public TreasureMapComponent.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private string __callBase_GetName()
    {
      return this.GetName();
    }

    private void __callBase_Init()
    {
      base.Init();
    }

    private void __callBase_PostInit()
    {
      base.PostInit();
    }

    private void __callBase_DeInit()
    {
      base.DeInit();
    }

    private void __callBase_Tick(uint deltaMillisecond)
    {
      this.Tick(deltaMillisecond);
    }

    private void __callBase_PostDeSerialize()
    {
      this.PostDeSerialize();
    }

    private bool __callBase_IsLevelFinished(int levelId)
    {
      return this.IsLevelFinished(levelId);
    }

    private int __callBase_AttackTreasureLevel(int levelId)
    {
      return this.AttackTreasureLevel(levelId);
    }

    private int __callBase_CanAttackTreasureLevel(int leveId)
    {
      return this.CanAttackTreasureLevel(leveId);
    }

    private void __callBase_SetSuccessTreasureMapLevel(
      ConfigDataTreasureLevelInfo levelInfo,
      List<int> battleTreasures)
    {
      base.SetSuccessTreasureMapLevel(levelInfo, battleTreasures);
    }

    private void __callBase_KillGoblin(int nums)
    {
      this.KillGoblin(nums);
    }

    private void __callBase_OnFinishedLevel(int levelId)
    {
      this.OnFinishedLevel(levelId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private TreasureMapComponent m_owner;

      public LuaExportHelper(TreasureMapComponent owner)
      {
        this.m_owner = owner;
      }

      public string __callBase_GetName()
      {
        return this.m_owner.__callBase_GetName();
      }

      public void __callBase_Init()
      {
        this.m_owner.__callBase_Init();
      }

      public void __callBase_PostInit()
      {
        this.m_owner.__callBase_PostInit();
      }

      public void __callBase_DeInit()
      {
        this.m_owner.__callBase_DeInit();
      }

      public void __callBase_Tick(uint deltaMillisecond)
      {
        this.m_owner.__callBase_Tick(deltaMillisecond);
      }

      public void __callBase_PostDeSerialize()
      {
        this.m_owner.__callBase_PostDeSerialize();
      }

      public bool __callBase_IsLevelFinished(int levelId)
      {
        return this.m_owner.__callBase_IsLevelFinished(levelId);
      }

      public int __callBase_AttackTreasureLevel(int levelId)
      {
        return this.m_owner.__callBase_AttackTreasureLevel(levelId);
      }

      public int __callBase_CanAttackTreasureLevel(int leveId)
      {
        return this.m_owner.__callBase_CanAttackTreasureLevel(leveId);
      }

      public void __callBase_SetSuccessTreasureMapLevel(
        ConfigDataTreasureLevelInfo levelInfo,
        List<int> battleTreasures)
      {
        this.m_owner.__callBase_SetSuccessTreasureMapLevel(levelInfo, battleTreasures);
      }

      public void __callBase_KillGoblin(int nums)
      {
        this.m_owner.__callBase_KillGoblin(nums);
      }

      public void __callBase_OnFinishedLevel(int levelId)
      {
        this.m_owner.__callBase_OnFinishedLevel(levelId);
      }

      public void SetSuccessTreasureMapLevel(
        ConfigDataTreasureLevelInfo levelInfo,
        List<int> battleTreasures)
      {
        this.m_owner.SetSuccessTreasureMapLevel(levelInfo, battleTreasures);
      }
    }
  }
}
