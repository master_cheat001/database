﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.ChatGroupCompactInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  [CustomLuaClass]
  public class ChatGroupCompactInfo
  {
    public string ChatGroupId;
    public string ChatGroupName;
    public ChatUserCompactInfo Owner;
    public int UserCount;
    public int OnlineUserCount;

    [MethodImpl((MethodImplOptions) 32768)]
    public ChatGroupCompactInfo(ProChatGroupCompactInfo pbGroupInfo)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
