﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PeakArenaSoldierUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.Scene;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class PeakArenaSoldierUIController : UIControllerBase
  {
    [AutoBind("./SoldierInfo/SoldierIconImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_soldierIconImage;
    [AutoBind("./SoldierInfo/QualityBGImage/QualityIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_soldierQualityIcon;
    [AutoBind("./SoldierInfo/SoldierNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierNameText;
    [AutoBind("./SoldierInfo/Graphic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_soldierGraphic;
    [AutoBind("./SoldierInfo/Faction/RangeValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierFactionRangeText;
    [AutoBind("./SoldierInfo/Faction/MoveValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierFactionMoveText;
    [AutoBind("./SoldierInfo/HP/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierHPValueText;
    [AutoBind("./SoldierInfo/HP/AddPercentText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierHPAddPercentText;
    [AutoBind("./SoldierInfo/HP/AddPercentText/AddValue", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierHPAddValueText;
    [AutoBind("./SoldierInfo/AT/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierATValueText;
    [AutoBind("./SoldierInfo/AT/AddPercentText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierATAddPercentText;
    [AutoBind("./SoldierInfo/AT/AddPercentText/AddValue", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierATAddValueText;
    [AutoBind("./SoldierInfo/DF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDFValueText;
    [AutoBind("./SoldierInfo/DF/AddPercentText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDFAddPercentText;
    [AutoBind("./SoldierInfo/DF/AddPercentText/AddValue", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDFAddValueText;
    [AutoBind("./SoldierInfo/MagicDF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierMagicDFValueText;
    [AutoBind("./SoldierInfo/MagicDF/AddPercentText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierMagicDFAddPercentText;
    [AutoBind("./SoldierInfo/MagicDF/AddPercentText/AddValue", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierMagicDFAddValueText;
    [AutoBind("./SoldierInfo/Strong/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierStrongText;
    [AutoBind("./SoldierInfo/Weak/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierWeakText;
    [AutoBind("./SoldierInfo/DescTextScroll/Mask/DescText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDescText;
    [AutoBind("./SoldierSelet/SoldierItemScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_soldierSelectContentScrollRect;
    [AutoBind("./SoldierSelet/SoldierItemScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_soldierSelectContentObj;
    [AutoBind("./SoldierSelet/SoldierGetCondition", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_soldierGetCondition;
    [AutoBind("./SoldierSelet/SoldierGetCondition/Texts", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_soldierGetConditionDescStateCtrl;
    [AutoBind("./SoldierSelet/SoldierGetCondition/Texts/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierGetConditionDescJobNameText;
    [AutoBind("./SoldierSelet/SoldierGetCondition/Texts/Text4", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierGetConditionDescText;
    [AutoBind("./SoldierSelet/SoldierGetCondition/GotoButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_soldierGetConditionGotoButton;
    [AutoBind("./Prefab/SoldierItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_soldierItemPrefabObj;
    [AutoBind("./SoldierInfo/SkinInfoButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_skinInfoButton;
    private Hero m_hero;
    private UISpineGraphic m_soldierInfoGraphic;
    private Dictionary<int, ConfigDataJobConnectionInfo> m_soldierIDToJobInfoDict;
    private SoldierItemUIController m_lastClickSoldierItemCtrl;
    private ConfigDataSoldierInfo m_lastClickSoldierInfo;
    private List<SoldierItemUIController> m_soliderItemListCtrl;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    [DoNotToLua]
    private PeakArenaSoldierUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_UpdateViewInSoldierStateHero_hotfix;
    private LuaFunction m_ShowSoldierInfoPanelConfigDataSoldierInfoBoolean_hotfix;
    private LuaFunction m_ShowSoldierSelectPanelHero_hotfix;
    private LuaFunction m_GetSoliderSortCompararConfigDataSoldierInfoConfigDataSoldierInfo_hotfix;
    private LuaFunction m_NotGetSoliderSortCompararConfigDataSoldierInfoConfigDataSoldierInfo_hotfix;
    private LuaFunction m_OnSoldierItemClickSoldierItemUIController_hotfix;
    private LuaFunction m_OnSoldierGetConditionGotoButtonClick_hotfix;
    private LuaFunction m_OnSoldierItemAttackButtonClickConfigDataSoldierInfo_hotfix;
    private LuaFunction m_SetCommonUIStateString_hotfix;
    private LuaFunction m_CleanSoldierPanelDataOnHeroChanged_hotfix;
    private LuaFunction m_OnSkinInfoButtonClick_hotfix;
    private LuaFunction m_add_EventOnSoldierAttackButtonClickAction`3_hotfix;
    private LuaFunction m_remove_EventOnSoldierAttackButtonClickAction`3_hotfix;
    private LuaFunction m_add_EventOnGotoDrillAction`1_hotfix;
    private LuaFunction m_remove_EventOnGotoDrillAction`1_hotfix;
    private LuaFunction m_add_EventOnGotoJobTransferAction`1_hotfix;
    private LuaFunction m_remove_EventOnGotoJobTransferAction`1_hotfix;
    private LuaFunction m_add_EventOnSkinInfoButtonClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnSkinInfoButtonClickAction`1_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaSoldierUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateViewInSoldierState(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowSoldierInfoPanel(ConfigDataSoldierInfo soldierInfo, bool isSoldierGet)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowSoldierSelectPanel(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetSoliderSortComparar(ConfigDataSoldierInfo s1, ConfigDataSoldierInfo s2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int NotGetSoliderSortComparar(ConfigDataSoldierInfo s1, ConfigDataSoldierInfo s2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSoldierItemClick(SoldierItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSoldierGetConditionGotoButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSoldierItemAttackButtonClick(ConfigDataSoldierInfo soldierInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCommonUIState(string stateName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CleanSoldierPanelDataOnHeroChanged()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSkinInfoButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int, int, Action> EventOnSoldierAttackButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnGotoDrill
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ConfigDataJobConnectionInfo> EventOnGotoJobTransfer
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ConfigDataSoldierInfo> EventOnSkinInfoButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public PeakArenaSoldierUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnSoldierAttackButtonClick(int arg1, int arg2, Action arg3)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnSoldierAttackButtonClick(int arg1, int arg2, Action arg3)
    {
      this.EventOnSoldierAttackButtonClick = (Action<int, int, Action>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnGotoDrill(int obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnGotoDrill(int obj)
    {
      this.EventOnGotoDrill = (Action<int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnGotoJobTransfer(ConfigDataJobConnectionInfo obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnGotoJobTransfer(ConfigDataJobConnectionInfo obj)
    {
      this.EventOnGotoJobTransfer = (Action<ConfigDataJobConnectionInfo>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnSkinInfoButtonClick(ConfigDataSoldierInfo obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnSkinInfoButtonClick(ConfigDataSoldierInfo obj)
    {
      this.EventOnSkinInfoButtonClick = (Action<ConfigDataSoldierInfo>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private PeakArenaSoldierUIController m_owner;

      public LuaExportHelper(PeakArenaSoldierUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnSoldierAttackButtonClick(int arg1, int arg2, Action arg3)
      {
        this.m_owner.__callDele_EventOnSoldierAttackButtonClick(arg1, arg2, arg3);
      }

      public void __clearDele_EventOnSoldierAttackButtonClick(int arg1, int arg2, Action arg3)
      {
        this.m_owner.__clearDele_EventOnSoldierAttackButtonClick(arg1, arg2, arg3);
      }

      public void __callDele_EventOnGotoDrill(int obj)
      {
        this.m_owner.__callDele_EventOnGotoDrill(obj);
      }

      public void __clearDele_EventOnGotoDrill(int obj)
      {
        this.m_owner.__clearDele_EventOnGotoDrill(obj);
      }

      public void __callDele_EventOnGotoJobTransfer(ConfigDataJobConnectionInfo obj)
      {
        this.m_owner.__callDele_EventOnGotoJobTransfer(obj);
      }

      public void __clearDele_EventOnGotoJobTransfer(ConfigDataJobConnectionInfo obj)
      {
        this.m_owner.__clearDele_EventOnGotoJobTransfer(obj);
      }

      public void __callDele_EventOnSkinInfoButtonClick(ConfigDataSoldierInfo obj)
      {
        this.m_owner.__callDele_EventOnSkinInfoButtonClick(obj);
      }

      public void __clearDele_EventOnSkinInfoButtonClick(ConfigDataSoldierInfo obj)
      {
        this.m_owner.__clearDele_EventOnSkinInfoButtonClick(obj);
      }

      public Image m_soldierIconImage
      {
        get
        {
          return this.m_owner.m_soldierIconImage;
        }
        set
        {
          this.m_owner.m_soldierIconImage = value;
        }
      }

      public Image m_soldierQualityIcon
      {
        get
        {
          return this.m_owner.m_soldierQualityIcon;
        }
        set
        {
          this.m_owner.m_soldierQualityIcon = value;
        }
      }

      public Text m_soldierNameText
      {
        get
        {
          return this.m_owner.m_soldierNameText;
        }
        set
        {
          this.m_owner.m_soldierNameText = value;
        }
      }

      public GameObject m_soldierGraphic
      {
        get
        {
          return this.m_owner.m_soldierGraphic;
        }
        set
        {
          this.m_owner.m_soldierGraphic = value;
        }
      }

      public Text m_soldierFactionRangeText
      {
        get
        {
          return this.m_owner.m_soldierFactionRangeText;
        }
        set
        {
          this.m_owner.m_soldierFactionRangeText = value;
        }
      }

      public Text m_soldierFactionMoveText
      {
        get
        {
          return this.m_owner.m_soldierFactionMoveText;
        }
        set
        {
          this.m_owner.m_soldierFactionMoveText = value;
        }
      }

      public Text m_soldierHPValueText
      {
        get
        {
          return this.m_owner.m_soldierHPValueText;
        }
        set
        {
          this.m_owner.m_soldierHPValueText = value;
        }
      }

      public Text m_soldierHPAddPercentText
      {
        get
        {
          return this.m_owner.m_soldierHPAddPercentText;
        }
        set
        {
          this.m_owner.m_soldierHPAddPercentText = value;
        }
      }

      public Text m_soldierHPAddValueText
      {
        get
        {
          return this.m_owner.m_soldierHPAddValueText;
        }
        set
        {
          this.m_owner.m_soldierHPAddValueText = value;
        }
      }

      public Text m_soldierATValueText
      {
        get
        {
          return this.m_owner.m_soldierATValueText;
        }
        set
        {
          this.m_owner.m_soldierATValueText = value;
        }
      }

      public Text m_soldierATAddPercentText
      {
        get
        {
          return this.m_owner.m_soldierATAddPercentText;
        }
        set
        {
          this.m_owner.m_soldierATAddPercentText = value;
        }
      }

      public Text m_soldierATAddValueText
      {
        get
        {
          return this.m_owner.m_soldierATAddValueText;
        }
        set
        {
          this.m_owner.m_soldierATAddValueText = value;
        }
      }

      public Text m_soldierDFValueText
      {
        get
        {
          return this.m_owner.m_soldierDFValueText;
        }
        set
        {
          this.m_owner.m_soldierDFValueText = value;
        }
      }

      public Text m_soldierDFAddPercentText
      {
        get
        {
          return this.m_owner.m_soldierDFAddPercentText;
        }
        set
        {
          this.m_owner.m_soldierDFAddPercentText = value;
        }
      }

      public Text m_soldierDFAddValueText
      {
        get
        {
          return this.m_owner.m_soldierDFAddValueText;
        }
        set
        {
          this.m_owner.m_soldierDFAddValueText = value;
        }
      }

      public Text m_soldierMagicDFValueText
      {
        get
        {
          return this.m_owner.m_soldierMagicDFValueText;
        }
        set
        {
          this.m_owner.m_soldierMagicDFValueText = value;
        }
      }

      public Text m_soldierMagicDFAddPercentText
      {
        get
        {
          return this.m_owner.m_soldierMagicDFAddPercentText;
        }
        set
        {
          this.m_owner.m_soldierMagicDFAddPercentText = value;
        }
      }

      public Text m_soldierMagicDFAddValueText
      {
        get
        {
          return this.m_owner.m_soldierMagicDFAddValueText;
        }
        set
        {
          this.m_owner.m_soldierMagicDFAddValueText = value;
        }
      }

      public Text m_soldierStrongText
      {
        get
        {
          return this.m_owner.m_soldierStrongText;
        }
        set
        {
          this.m_owner.m_soldierStrongText = value;
        }
      }

      public Text m_soldierWeakText
      {
        get
        {
          return this.m_owner.m_soldierWeakText;
        }
        set
        {
          this.m_owner.m_soldierWeakText = value;
        }
      }

      public Text m_soldierDescText
      {
        get
        {
          return this.m_owner.m_soldierDescText;
        }
        set
        {
          this.m_owner.m_soldierDescText = value;
        }
      }

      public ScrollRect m_soldierSelectContentScrollRect
      {
        get
        {
          return this.m_owner.m_soldierSelectContentScrollRect;
        }
        set
        {
          this.m_owner.m_soldierSelectContentScrollRect = value;
        }
      }

      public GameObject m_soldierSelectContentObj
      {
        get
        {
          return this.m_owner.m_soldierSelectContentObj;
        }
        set
        {
          this.m_owner.m_soldierSelectContentObj = value;
        }
      }

      public GameObject m_soldierGetCondition
      {
        get
        {
          return this.m_owner.m_soldierGetCondition;
        }
        set
        {
          this.m_owner.m_soldierGetCondition = value;
        }
      }

      public CommonUIStateController m_soldierGetConditionDescStateCtrl
      {
        get
        {
          return this.m_owner.m_soldierGetConditionDescStateCtrl;
        }
        set
        {
          this.m_owner.m_soldierGetConditionDescStateCtrl = value;
        }
      }

      public Text m_soldierGetConditionDescJobNameText
      {
        get
        {
          return this.m_owner.m_soldierGetConditionDescJobNameText;
        }
        set
        {
          this.m_owner.m_soldierGetConditionDescJobNameText = value;
        }
      }

      public Text m_soldierGetConditionDescText
      {
        get
        {
          return this.m_owner.m_soldierGetConditionDescText;
        }
        set
        {
          this.m_owner.m_soldierGetConditionDescText = value;
        }
      }

      public Button m_soldierGetConditionGotoButton
      {
        get
        {
          return this.m_owner.m_soldierGetConditionGotoButton;
        }
        set
        {
          this.m_owner.m_soldierGetConditionGotoButton = value;
        }
      }

      public GameObject m_soldierItemPrefabObj
      {
        get
        {
          return this.m_owner.m_soldierItemPrefabObj;
        }
        set
        {
          this.m_owner.m_soldierItemPrefabObj = value;
        }
      }

      public Button m_skinInfoButton
      {
        get
        {
          return this.m_owner.m_skinInfoButton;
        }
        set
        {
          this.m_owner.m_skinInfoButton = value;
        }
      }

      public Hero m_hero
      {
        get
        {
          return this.m_owner.m_hero;
        }
        set
        {
          this.m_owner.m_hero = value;
        }
      }

      public UISpineGraphic m_soldierInfoGraphic
      {
        get
        {
          return this.m_owner.m_soldierInfoGraphic;
        }
        set
        {
          this.m_owner.m_soldierInfoGraphic = value;
        }
      }

      public Dictionary<int, ConfigDataJobConnectionInfo> m_soldierIDToJobInfoDict
      {
        get
        {
          return this.m_owner.m_soldierIDToJobInfoDict;
        }
        set
        {
          this.m_owner.m_soldierIDToJobInfoDict = value;
        }
      }

      public SoldierItemUIController m_lastClickSoldierItemCtrl
      {
        get
        {
          return this.m_owner.m_lastClickSoldierItemCtrl;
        }
        set
        {
          this.m_owner.m_lastClickSoldierItemCtrl = value;
        }
      }

      public ConfigDataSoldierInfo m_lastClickSoldierInfo
      {
        get
        {
          return this.m_owner.m_lastClickSoldierInfo;
        }
        set
        {
          this.m_owner.m_lastClickSoldierInfo = value;
        }
      }

      public List<SoldierItemUIController> m_soliderItemListCtrl
      {
        get
        {
          return this.m_owner.m_soliderItemListCtrl;
        }
        set
        {
          this.m_owner.m_soliderItemListCtrl = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void ShowSoldierInfoPanel(ConfigDataSoldierInfo soldierInfo, bool isSoldierGet)
      {
        this.m_owner.ShowSoldierInfoPanel(soldierInfo, isSoldierGet);
      }

      public void ShowSoldierSelectPanel(Hero hero)
      {
        this.m_owner.ShowSoldierSelectPanel(hero);
      }

      public int GetSoliderSortComparar(ConfigDataSoldierInfo s1, ConfigDataSoldierInfo s2)
      {
        return this.m_owner.GetSoliderSortComparar(s1, s2);
      }

      public int NotGetSoliderSortComparar(ConfigDataSoldierInfo s1, ConfigDataSoldierInfo s2)
      {
        return this.m_owner.NotGetSoliderSortComparar(s1, s2);
      }

      public void OnSoldierItemClick(SoldierItemUIController ctrl)
      {
        this.m_owner.OnSoldierItemClick(ctrl);
      }

      public void OnSoldierGetConditionGotoButtonClick()
      {
        this.m_owner.OnSoldierGetConditionGotoButtonClick();
      }

      public void OnSoldierItemAttackButtonClick(ConfigDataSoldierInfo soldierInfo)
      {
        this.m_owner.OnSoldierItemAttackButtonClick(soldierInfo);
      }

      public void CleanSoldierPanelDataOnHeroChanged()
      {
        this.m_owner.CleanSoldierPanelDataOnHeroChanged();
      }

      public void OnSkinInfoButtonClick()
      {
        this.m_owner.OnSkinInfoButtonClick();
      }
    }
  }
}
