﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ManualUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using SLua;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class ManualUIController : UIControllerBase
  {
    [AutoBind("./EquipmentPanel/EquipmentButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_equipmentButton;
    [AutoBind("./HeroPanel/HeroButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_heroButton;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./DetailButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_detailButton;
    private ArchiveUITask m_task;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetTask(ArchiveUITask task)
    {
      this.m_task = task;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnEquipmentClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnHeroClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnReturnClick()
    {
      // ISSUE: unable to decompile the method.
    }

    protected void OnDetailClick()
    {
      CommonUIController.Instance.ShowExplanation(ExplanationId.ExplanationId_Archive);
    }
  }
}
