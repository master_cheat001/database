﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.MailDetailUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class MailDetailUIController : UIControllerBase
  {
    private Mail m_currentMailInfo;
    private ProjectLPlayerContext m_playerContext;
    private List<RewardGoodsUIController> m_attachmentUICtrlList;
    private GameObject ItemUICtrlPrefab;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController BgUIState;
    [AutoBind("./MailDetail_Title/Text", AutoBindAttribute.InitState.NotInit, false)]
    public Text TitleText;
    [AutoBind("./TimeOutTextPanel/TimeOutText", AutoBindAttribute.InitState.NotInit, false)]
    public Text TimeOutText;
    [AutoBind("./MailDetail/ListScrollView/Viewport/Content/Text", AutoBindAttribute.InitState.NotInit, false)]
    public Text ContextText;
    [AutoBind("./MailDetail/ListScrollViewSmall/Viewport/Content/Text", AutoBindAttribute.InitState.NotInit, false)]
    public Text SmallContextText;
    [AutoBind("./MailDetail/RewardItem/Content", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject RewardRoot;
    [AutoBind("./MailDetail/GetButton", AutoBindAttribute.InitState.NotInit, false)]
    public ButtonEx GetButton;
    [AutoBind("./MailDetail/GotoButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button GotoButton;
    [AutoBind("./NoMailPanel/ChooseText", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject ChooseTextGo;
    [AutoBind("./NoMailPanel/NoneText", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject NoneTextGo;

    [MethodImpl((MethodImplOptions) 32768)]
    public MailDetailUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateMailDetail(Mail mailInfo, bool haveMail)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowAttachmentsUI(Mail mailInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGotoButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGetAttachmentButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<Mail> EventOnGetAttachmentButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<Mail> EventOnGotoButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
