﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroAnthemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class HeroAnthemUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./HelpButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_helpButton;
    [AutoBind("./RankButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_rankButton;
    [AutoBind("./PlayerResource/Challenge/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_challengeValueText;
    [AutoBind("./AchievementButton/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_AchievementValueText;
    [AutoBind("./Chapters/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_chapterListScrollRect;
    [AutoBind("./RankPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_rankPanelStateCtrl;
    [AutoBind("./RankPanel/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_rankPanelBgButton;
    [AutoBind("./RankPanel/Detail/ScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private DynamicGrid m_rankPanelGrid;
    [AutoBind("./RankPanel/Detail/Player/Ranking", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_rankPanelPlayerRankingStateCtrl;
    [AutoBind("./RankPanel/Detail/Player/Ranking/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_rankPanelPlayerRankImage;
    [AutoBind("./RankPanel/Detail/Player/Ranking/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_rankPanelPlayerRankText;
    [AutoBind("./RankPanel/Detail/Player/PlayerNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_rankPanelPlayerNameText;
    [AutoBind("./RankPanel/Detail/Player/HeadIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_rankPanelPlayerHeadIcon;
    [AutoBind("./RankPanel/Detail/Player/HeadIcon/HeadFrameDummy", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_rankPanelPlayerHeadFrameDummy;
    [AutoBind("./RankPanel/Detail/Player/HeadIcon/LevelText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_rankPanelPlayerLevelText;
    [AutoBind("./RankPanel/Detail/Player/ArenaPoints/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_rankPanelPlayerArenaPointsText;
    [AutoBind("./Prefab", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefab/HeroAnthemButton", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_HeroAnthemChapterItemPrefab;
    [AutoBind("./Prefab/RankListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_rankListItemPrefab;
    private List<HeroAnthemChapterItemUIController> m_heroAnthemChapterItemList;
    private IConfigDataLoader m_configDataLoader;
    private ProjectLPlayerContext m_playerContext;
    private ScrollSnapCenter m_chaptersScrollSnapCenter;
    private HeroAnthemChapterItemUIController m_curHeroAnthemChapterItemCtrl;
    private int m_curChapterCtrlIndex;
    private List<RankingTargetPlayerInfo> m_rankPlayerList;
    [DoNotToLua]
    private HeroAnthemUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_Open_hotfix;
    private LuaFunction m_ResetScrollViewToTop_hotfix;
    private LuaFunction m_LateUpdate_hotfix;
    private LuaFunction m_SetHeroAnthemChaptersList`1_hotfix;
    private LuaFunction m_SetCurChapterAchievements_hotfix;
    private LuaFunction m_ChaptersScrollSnapCenter_OnEndDrag_hotfix;
    private LuaFunction m_AddHeroAnthemChapterItemConfigDataHeroAnthemInfoBooleanBoolean_hotfix;
    private LuaFunction m_SetCurrentChapterConfigDataHeroAnthemInfo_hotfix;
    private LuaFunction m_ClearHeroAnthemChapterListItems_hotfix;
    private LuaFunction m_OnRankButtonClick_hotfix;
    private LuaFunction m_ShowRankPanel_hotfix;
    private LuaFunction m_SetRankPanel_hotfix;
    private LuaFunction m_SetPlayerRankInfo_hotfix;
    private LuaFunction m_CloseRanlPanel_hotfix;
    private LuaFunction m_OnRankPanelGridCellInitGameObject_hotfix;
    private LuaFunction m_OnRankPanelGridCellUpdateDynamicGridCellController_hotfix;
    private LuaFunction m_OnReturnButtonClick_hotfix;
    private LuaFunction m_OnHelpButtonClick_hotfix;
    private LuaFunction m_HeroAnthemChapterItem_OnButtonClickHeroAnthemChapterItemUIController_hotfix;
    private LuaFunction m_add_EventOnReturnAction_hotfix;
    private LuaFunction m_remove_EventOnReturnAction_hotfix;
    private LuaFunction m_add_EventOnShowHelpAction_hotfix;
    private LuaFunction m_remove_EventOnShowHelpAction_hotfix;
    private LuaFunction m_add_EventOnRankButtonClickAction_hotfix;
    private LuaFunction m_remove_EventOnRankButtonClickAction_hotfix;
    private LuaFunction m_add_EventOnGotoHeroAnthemChapterAction`1_hotfix;
    private LuaFunction m_remove_EventOnGotoHeroAnthemChapterAction`1_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    private HeroAnthemUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ResetScrollViewToTop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LateUpdate()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHeroAnthemChapters(List<ConfigDataHeroAnthemInfo> heroAnthemInfoList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetCurChapterAchievements()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ChaptersScrollSnapCenter_OnEndDrag()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddHeroAnthemChapterItem(
      ConfigDataHeroAnthemInfo anthemInfo,
      bool isLocked,
      bool isNew)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCurrentChapter(ConfigDataHeroAnthemInfo heroAnthemInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearHeroAnthemChapterListItems()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRankButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowRankPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetRankPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPlayerRankInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseRanlPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private DynamicGridCellController OnRankPanelGridCellInit(GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRankPanelGridCellUpdate(DynamicGridCellController cell)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHelpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroAnthemChapterItem_OnButtonClick(HeroAnthemChapterItemUIController b)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowHelp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnRankButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ConfigDataHeroAnthemInfo> EventOnGotoHeroAnthemChapter
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public HeroAnthemUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnReturn()
    {
      this.EventOnReturn = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowHelp()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowHelp()
    {
      this.EventOnShowHelp = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnRankButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnRankButtonClick()
    {
      this.EventOnRankButtonClick = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnGotoHeroAnthemChapter(ConfigDataHeroAnthemInfo obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnGotoHeroAnthemChapter(ConfigDataHeroAnthemInfo obj)
    {
      this.EventOnGotoHeroAnthemChapter = (Action<ConfigDataHeroAnthemInfo>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private HeroAnthemUIController m_owner;

      public LuaExportHelper(HeroAnthemUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnReturn()
      {
        this.m_owner.__callDele_EventOnReturn();
      }

      public void __clearDele_EventOnReturn()
      {
        this.m_owner.__clearDele_EventOnReturn();
      }

      public void __callDele_EventOnShowHelp()
      {
        this.m_owner.__callDele_EventOnShowHelp();
      }

      public void __clearDele_EventOnShowHelp()
      {
        this.m_owner.__clearDele_EventOnShowHelp();
      }

      public void __callDele_EventOnRankButtonClick()
      {
        this.m_owner.__callDele_EventOnRankButtonClick();
      }

      public void __clearDele_EventOnRankButtonClick()
      {
        this.m_owner.__clearDele_EventOnRankButtonClick();
      }

      public void __callDele_EventOnGotoHeroAnthemChapter(ConfigDataHeroAnthemInfo obj)
      {
        this.m_owner.__callDele_EventOnGotoHeroAnthemChapter(obj);
      }

      public void __clearDele_EventOnGotoHeroAnthemChapter(ConfigDataHeroAnthemInfo obj)
      {
        this.m_owner.__clearDele_EventOnGotoHeroAnthemChapter(obj);
      }

      public CommonUIStateController m_uiStateController
      {
        get
        {
          return this.m_owner.m_uiStateController;
        }
        set
        {
          this.m_owner.m_uiStateController = value;
        }
      }

      public Button m_returnButton
      {
        get
        {
          return this.m_owner.m_returnButton;
        }
        set
        {
          this.m_owner.m_returnButton = value;
        }
      }

      public Button m_helpButton
      {
        get
        {
          return this.m_owner.m_helpButton;
        }
        set
        {
          this.m_owner.m_helpButton = value;
        }
      }

      public Button m_rankButton
      {
        get
        {
          return this.m_owner.m_rankButton;
        }
        set
        {
          this.m_owner.m_rankButton = value;
        }
      }

      public Text m_challengeValueText
      {
        get
        {
          return this.m_owner.m_challengeValueText;
        }
        set
        {
          this.m_owner.m_challengeValueText = value;
        }
      }

      public Text m_AchievementValueText
      {
        get
        {
          return this.m_owner.m_AchievementValueText;
        }
        set
        {
          this.m_owner.m_AchievementValueText = value;
        }
      }

      public ScrollRect m_chapterListScrollRect
      {
        get
        {
          return this.m_owner.m_chapterListScrollRect;
        }
        set
        {
          this.m_owner.m_chapterListScrollRect = value;
        }
      }

      public CommonUIStateController m_rankPanelStateCtrl
      {
        get
        {
          return this.m_owner.m_rankPanelStateCtrl;
        }
        set
        {
          this.m_owner.m_rankPanelStateCtrl = value;
        }
      }

      public Button m_rankPanelBgButton
      {
        get
        {
          return this.m_owner.m_rankPanelBgButton;
        }
        set
        {
          this.m_owner.m_rankPanelBgButton = value;
        }
      }

      public DynamicGrid m_rankPanelGrid
      {
        get
        {
          return this.m_owner.m_rankPanelGrid;
        }
        set
        {
          this.m_owner.m_rankPanelGrid = value;
        }
      }

      public CommonUIStateController m_rankPanelPlayerRankingStateCtrl
      {
        get
        {
          return this.m_owner.m_rankPanelPlayerRankingStateCtrl;
        }
        set
        {
          this.m_owner.m_rankPanelPlayerRankingStateCtrl = value;
        }
      }

      public Image m_rankPanelPlayerRankImage
      {
        get
        {
          return this.m_owner.m_rankPanelPlayerRankImage;
        }
        set
        {
          this.m_owner.m_rankPanelPlayerRankImage = value;
        }
      }

      public Text m_rankPanelPlayerRankText
      {
        get
        {
          return this.m_owner.m_rankPanelPlayerRankText;
        }
        set
        {
          this.m_owner.m_rankPanelPlayerRankText = value;
        }
      }

      public Text m_rankPanelPlayerNameText
      {
        get
        {
          return this.m_owner.m_rankPanelPlayerNameText;
        }
        set
        {
          this.m_owner.m_rankPanelPlayerNameText = value;
        }
      }

      public Image m_rankPanelPlayerHeadIcon
      {
        get
        {
          return this.m_owner.m_rankPanelPlayerHeadIcon;
        }
        set
        {
          this.m_owner.m_rankPanelPlayerHeadIcon = value;
        }
      }

      public Transform m_rankPanelPlayerHeadFrameDummy
      {
        get
        {
          return this.m_owner.m_rankPanelPlayerHeadFrameDummy;
        }
        set
        {
          this.m_owner.m_rankPanelPlayerHeadFrameDummy = value;
        }
      }

      public Text m_rankPanelPlayerLevelText
      {
        get
        {
          return this.m_owner.m_rankPanelPlayerLevelText;
        }
        set
        {
          this.m_owner.m_rankPanelPlayerLevelText = value;
        }
      }

      public Text m_rankPanelPlayerArenaPointsText
      {
        get
        {
          return this.m_owner.m_rankPanelPlayerArenaPointsText;
        }
        set
        {
          this.m_owner.m_rankPanelPlayerArenaPointsText = value;
        }
      }

      public GameObject m_prefabsGameObject
      {
        get
        {
          return this.m_owner.m_prefabsGameObject;
        }
        set
        {
          this.m_owner.m_prefabsGameObject = value;
        }
      }

      public GameObject m_HeroAnthemChapterItemPrefab
      {
        get
        {
          return this.m_owner.m_HeroAnthemChapterItemPrefab;
        }
        set
        {
          this.m_owner.m_HeroAnthemChapterItemPrefab = value;
        }
      }

      public GameObject m_rankListItemPrefab
      {
        get
        {
          return this.m_owner.m_rankListItemPrefab;
        }
        set
        {
          this.m_owner.m_rankListItemPrefab = value;
        }
      }

      public List<HeroAnthemChapterItemUIController> m_heroAnthemChapterItemList
      {
        get
        {
          return this.m_owner.m_heroAnthemChapterItemList;
        }
        set
        {
          this.m_owner.m_heroAnthemChapterItemList = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public ScrollSnapCenter m_chaptersScrollSnapCenter
      {
        get
        {
          return this.m_owner.m_chaptersScrollSnapCenter;
        }
        set
        {
          this.m_owner.m_chaptersScrollSnapCenter = value;
        }
      }

      public HeroAnthemChapterItemUIController m_curHeroAnthemChapterItemCtrl
      {
        get
        {
          return this.m_owner.m_curHeroAnthemChapterItemCtrl;
        }
        set
        {
          this.m_owner.m_curHeroAnthemChapterItemCtrl = value;
        }
      }

      public int m_curChapterCtrlIndex
      {
        get
        {
          return this.m_owner.m_curChapterCtrlIndex;
        }
        set
        {
          this.m_owner.m_curChapterCtrlIndex = value;
        }
      }

      public List<RankingTargetPlayerInfo> m_rankPlayerList
      {
        get
        {
          return this.m_owner.m_rankPlayerList;
        }
        set
        {
          this.m_owner.m_rankPlayerList = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void ResetScrollViewToTop()
      {
        this.m_owner.ResetScrollViewToTop();
      }

      public void LateUpdate()
      {
        this.m_owner.LateUpdate();
      }

      public void SetCurChapterAchievements()
      {
        this.m_owner.SetCurChapterAchievements();
      }

      public void ChaptersScrollSnapCenter_OnEndDrag()
      {
        this.m_owner.ChaptersScrollSnapCenter_OnEndDrag();
      }

      public void AddHeroAnthemChapterItem(
        ConfigDataHeroAnthemInfo anthemInfo,
        bool isLocked,
        bool isNew)
      {
        this.m_owner.AddHeroAnthemChapterItem(anthemInfo, isLocked, isNew);
      }

      public void ClearHeroAnthemChapterListItems()
      {
        this.m_owner.ClearHeroAnthemChapterListItems();
      }

      public void OnRankButtonClick()
      {
        this.m_owner.OnRankButtonClick();
      }

      public void SetRankPanel()
      {
        this.m_owner.SetRankPanel();
      }

      public void SetPlayerRankInfo()
      {
        this.m_owner.SetPlayerRankInfo();
      }

      public void CloseRanlPanel()
      {
        this.m_owner.CloseRanlPanel();
      }

      public DynamicGridCellController OnRankPanelGridCellInit(
        GameObject go)
      {
        return this.m_owner.OnRankPanelGridCellInit(go);
      }

      public void OnRankPanelGridCellUpdate(DynamicGridCellController cell)
      {
        this.m_owner.OnRankPanelGridCellUpdate(cell);
      }

      public void OnReturnButtonClick()
      {
        this.m_owner.OnReturnButtonClick();
      }

      public void OnHelpButtonClick()
      {
        this.m_owner.OnHelpButtonClick();
      }

      public void HeroAnthemChapterItem_OnButtonClick(HeroAnthemChapterItemUIController b)
      {
        this.m_owner.HeroAnthemChapterItem_OnButtonClick(b);
      }
    }
  }
}
