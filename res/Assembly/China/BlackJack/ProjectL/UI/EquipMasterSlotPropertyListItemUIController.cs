﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.EquipMasterSlotPropertyListItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using System;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class EquipMasterSlotPropertyListItemUIController : UIControllerBase
  {
    private int m_index;
    private HeroJobSlotRefineryProperty m_property;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController UIStateCtrl;
    [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
    public Text NameText;
    [AutoBind("./ValueText", AutoBindAttribute.InitState.NotInit, false)]
    public Text ValueText;
    [AutoBind("./ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
    public Image ProgressBarImage;
    [AutoBind("./RefineButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button RefineButton;

    protected override void OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
      this.RegistEvents();
    }

    public void SetIndex(int index)
    {
      this.m_index = index;
    }

    public int GetIndex()
    {
      return this.m_index;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateProperty(
      HeroJobSlotRefineryProperty heroJobSlotRefineryProperty)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RegistEvents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UnRegistEvents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRefineButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int, HeroJobSlotRefineryProperty> EventOnRefineButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
