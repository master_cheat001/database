﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ArenaActionOrderButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class ArenaActionOrderButton : UIControllerBase, IBeginDragHandler, IEndDragHandler, IDragHandler, IDropHandler, IEventSystemHandler
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_button;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroIconImage;
    [AutoBind("./ArmyIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_armyIconImage;
    private BattleHero m_hero;
    private int m_index;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    public void Destroy()
    {
      UnityEngine.Object.Destroy((UnityEngine.Object) this.gameObject);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHero(BattleHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    public BattleHero GetHero()
    {
      return this.m_hero;
    }

    public void SetIndex(int idx)
    {
      this.m_index = idx;
    }

    public int GetIndex()
    {
      return this.m_index;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBeginDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnEndDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnDrop(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<ArenaActionOrderButton> EventOnClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ArenaActionOrderButton, PointerEventData> EventOnBeginDrag
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ArenaActionOrderButton, PointerEventData> EventOnEndDrag
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<PointerEventData> EventOnDrag
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<PointerEventData> EventOnDrop
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
