﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.RewardHeroUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using SLua;
using System;
using System.Collections;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class RewardHeroUIController : UIControllerBase
  {
    [AutoBind("./IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_iconImage;
    [AutoBind("./LevelAndExp", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_levelAndExpGameObject;
    [AutoBind("./LevelAndExp/LvNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_levelText;
    [AutoBind("./LevelAndExp/EXPNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_expText;
    [AutoBind("./LevelAndExp/ExpProgressBar", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_expImage;
    [AutoBind("./LevelUpEffect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_levelUpGameObject;
    [AutoBind("./EffectRoot", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_effectRoot;
    private float m_heroExpTotalWidth;
    private Hero m_hero;
    private float m_singleAddExp;
    private float m_beforeHeroExp;
    private int m_finalHeroExp;
    [DoNotToLua]
    private RewardHeroUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_SetHeroHero_hotfix;
    private LuaFunction m_ShowExpAndLevelBoolean_hotfix;
    private LuaFunction m_AddExpBarWidthSingle_hotfix;
    private LuaFunction m_UpdateTextValue_hotfix;
    private LuaFunction m_SetLevelInt32_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHero(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowExpAndLevel(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator AddExpBarWidth(float intervalTime = 2f)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateTextValue()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetLevel(int level)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public RewardHeroUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private RewardHeroUIController m_owner;

      public LuaExportHelper(RewardHeroUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public Image m_iconImage
      {
        get
        {
          return this.m_owner.m_iconImage;
        }
        set
        {
          this.m_owner.m_iconImage = value;
        }
      }

      public GameObject m_levelAndExpGameObject
      {
        get
        {
          return this.m_owner.m_levelAndExpGameObject;
        }
        set
        {
          this.m_owner.m_levelAndExpGameObject = value;
        }
      }

      public Text m_levelText
      {
        get
        {
          return this.m_owner.m_levelText;
        }
        set
        {
          this.m_owner.m_levelText = value;
        }
      }

      public Text m_expText
      {
        get
        {
          return this.m_owner.m_expText;
        }
        set
        {
          this.m_owner.m_expText = value;
        }
      }

      public Image m_expImage
      {
        get
        {
          return this.m_owner.m_expImage;
        }
        set
        {
          this.m_owner.m_expImage = value;
        }
      }

      public GameObject m_levelUpGameObject
      {
        get
        {
          return this.m_owner.m_levelUpGameObject;
        }
        set
        {
          this.m_owner.m_levelUpGameObject = value;
        }
      }

      public GameObject m_effectRoot
      {
        get
        {
          return this.m_owner.m_effectRoot;
        }
        set
        {
          this.m_owner.m_effectRoot = value;
        }
      }

      public float m_heroExpTotalWidth
      {
        get
        {
          return this.m_owner.m_heroExpTotalWidth;
        }
        set
        {
          this.m_owner.m_heroExpTotalWidth = value;
        }
      }

      public Hero m_hero
      {
        get
        {
          return this.m_owner.m_hero;
        }
        set
        {
          this.m_owner.m_hero = value;
        }
      }

      public float m_singleAddExp
      {
        get
        {
          return this.m_owner.m_singleAddExp;
        }
        set
        {
          this.m_owner.m_singleAddExp = value;
        }
      }

      public float m_beforeHeroExp
      {
        get
        {
          return this.m_owner.m_beforeHeroExp;
        }
        set
        {
          this.m_owner.m_beforeHeroExp = value;
        }
      }

      public int m_finalHeroExp
      {
        get
        {
          return this.m_owner.m_finalHeroExp;
        }
        set
        {
          this.m_owner.m_finalHeroExp = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public IEnumerator AddExpBarWidth(float intervalTime)
      {
        return this.m_owner.AddExpBarWidth(intervalTime);
      }

      public void UpdateTextValue()
      {
        this.m_owner.UpdateTextValue();
      }

      public void SetLevel(int level)
      {
        this.m_owner.SetLevel(level);
      }
    }
  }
}
