﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.AncientCallRankRewardUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class AncientCallRankRewardUIController : UIControllerBase
  {
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiState;
    [AutoBind("./BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_bgButton;
    [AutoBind("./Detail/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_closeButton;
    [AutoBind("./Detail/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_explainText;
    [AutoBind("./Detail/ScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_itemContent;
    [AutoBind("./Detail/ScrollView/Viewport/Content/RewardListItem", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_itemPrefab;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.NotInit, false)]
    private PrefabResourceContainer m_prefabResourceContainer;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private AncientCallUITask m_ancientCallUITask;
    [DoNotToLua]
    private AncientCallRankRewardUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_UpdateView_hotfix;
    private LuaFunction m_Open_hotfix;
    private LuaFunction m_OnCloseClick_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public AncientCallRankRewardUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    private class RewardItem : UIControllerBase
    {
      [AutoBind("./NoRewardText", AutoBindAttribute.InitState.NotInit, false)]
      private GameObject m_explainGameObject;
      [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
      private Text m_rankRangeText;
      [AutoBind("./RewardGroup", AutoBindAttribute.InitState.NotInit, false)]
      private Transform m_rewardGroup;
      private ProjectLPlayerContext m_playerContext;
      private IConfigDataLoader m_configDataLoader;
      private GameObject m_rewardGoodsPrefab;
      private ConfigDataAncientCallTotalBossPointsInfo m_afterBossPointsInfo;
      private ConfigDataAncientCallTotalBossPointsInfo m_bossPointsInfo;

      [MethodImpl((MethodImplOptions) 32768)]
      protected override void OnBindFiledsCompleted()
      {
        // ISSUE: unable to decompile the method.
      }

      public void Init(GameObject rewardGoodsPrefab)
      {
        this.m_rewardGoodsPrefab = rewardGoodsPrefab;
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void SetData(
        ConfigDataAncientCallTotalBossPointsInfo bossPointsInfo,
        ConfigDataAncientCallTotalBossPointsInfo afterBossPointsInfo)
      {
        // ISSUE: unable to decompile the method.
      }

      [MethodImpl((MethodImplOptions) 32768)]
      private void UpdateView()
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public class LuaExportHelper
    {
      private AncientCallRankRewardUIController m_owner;

      public LuaExportHelper(AncientCallRankRewardUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        return this.m_owner.__callBase_BindFieldImpl(fieldType, path, initState, fieldName, ctrlName, optional);
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public CommonUIStateController m_uiState
      {
        get
        {
          return this.m_owner.m_uiState;
        }
        set
        {
          this.m_owner.m_uiState = value;
        }
      }

      public Button m_bgButton
      {
        get
        {
          return this.m_owner.m_bgButton;
        }
        set
        {
          this.m_owner.m_bgButton = value;
        }
      }

      public Button m_closeButton
      {
        get
        {
          return this.m_owner.m_closeButton;
        }
        set
        {
          this.m_owner.m_closeButton = value;
        }
      }

      public Text m_explainText
      {
        get
        {
          return this.m_owner.m_explainText;
        }
        set
        {
          this.m_owner.m_explainText = value;
        }
      }

      public Transform m_itemContent
      {
        get
        {
          return this.m_owner.m_itemContent;
        }
        set
        {
          this.m_owner.m_itemContent = value;
        }
      }

      public GameObject m_itemPrefab
      {
        get
        {
          return this.m_owner.m_itemPrefab;
        }
        set
        {
          this.m_owner.m_itemPrefab = value;
        }
      }

      public PrefabResourceContainer m_prefabResourceContainer
      {
        get
        {
          return this.m_owner.m_prefabResourceContainer;
        }
        set
        {
          this.m_owner.m_prefabResourceContainer = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public AncientCallUITask m_ancientCallUITask
      {
        get
        {
          return this.m_owner.m_ancientCallUITask;
        }
        set
        {
          this.m_owner.m_ancientCallUITask = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void UpdateView()
      {
        this.m_owner.UpdateView();
      }

      public void OnCloseClick()
      {
        this.m_owner.OnCloseClick();
      }
    }
  }
}
