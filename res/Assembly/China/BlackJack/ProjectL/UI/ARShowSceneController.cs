﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ARShowSceneController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.AR;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using Spine.Unity;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public abstract class ARShowSceneController : UIControllerBase
  {
    protected SkeletonAnimation m_roleSkeleton;
    protected List<SkeletonAnimation> m_roleSkeletonList;
    protected ARUITask m_task;
    protected ProjectLPlayerContext m_playerContext;
    protected ARPlaneTrace m_arPlaneTrace;
    protected bool isPlaneTrace;
    protected string[] m_roleAnimationList;

    [MethodImpl((MethodImplOptions) 32768)]
    protected ARShowSceneController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnCharClick(GameObject obj)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetUITask(ARUITask task)
    {
      this.m_task = task;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SummonHeroBattleShow(int selectHeroID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SummonHeroDrawShow(int selectHeroID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SummonHeroTeamShow()
    {
      // ISSUE: unable to decompile the method.
    }

    public void PlaySingleCharAnimation(string animationName, bool isLoop)
    {
      this.PlayAnimation(this.m_roleSkeleton, animationName, isLoop);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayAnimation(
      SkeletonAnimation skeletonAnimation,
      string animationName,
      bool isLoop)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayAnimation(
      SkeletonAnimation skeletonAnimation,
      HeroActionType actionType,
      bool isLoop)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetCharDirection(bool isLookRight)
    {
      this.SetCharDirection(this.charNode, isLookRight);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCharDirection(GameObject charObj, bool isLookRight)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCharScale(float scale)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTeamDistance(float distance)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DestroyChar()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlaneTraceEnable(bool isActive)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsFindPlane()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPause()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnResume()
    {
      // ISSUE: unable to decompile the method.
    }

    protected abstract GameObject focusSquare { get; }

    protected abstract GameObject charNode { get; }

    protected abstract GameObject charDrawNode { get; }

    protected abstract GameObject charGroupNode { get; }

    protected abstract Renderer focusSquareRenderer { get; }

    protected abstract ARPlaneTrace CreatePlaneTrace();
  }
}
