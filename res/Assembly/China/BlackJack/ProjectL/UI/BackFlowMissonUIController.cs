﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BackFlowMissonUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class BackFlowMissonUIController : UIControllerBase
  {
    [AutoBind("./InfoText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoText;
    [AutoBind("./ItemDummy", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_itemDummy;
    [AutoBind("./ProgressBarBGImage/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_progressBar;
    [AutoBind("./ProgressBarBGImage/ProgressBarValueGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_progressBarValueStateCtrl;
    [AutoBind("./ProgressBarBGImage/ProgressBarValueGroup/HaveCount", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_progressHaveCount;
    [AutoBind("./ProgressBarBGImage/ProgressBarValueGroup/NeedCount", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_progressNeedCount;
    [AutoBind("./ProgressBarBGImage/DoneText", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_progressDoneText;
    [AutoBind("./ButtonGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_buttonGroupStateCtrl;
    [AutoBind("./ButtonGroup/GoToButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_gotoButton;
    [AutoBind("./ButtonGroup/GetButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_getButton;
    [DoNotToLua]
    private BackFlowMissonUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_InitOpenServiceMissonInfoMissionBoolean_hotfix;
    private LuaFunction m_SetOpenServiceMissionInfo_hotfix;
    private LuaFunction m_SetOpenServiceMissionStateBoolean_hotfix;
    private LuaFunction m_OnGotoButtonClick_hotfix;
    private LuaFunction m_OnGetButtonClick_hotfix;
    private LuaFunction m_PlayGetRewardEffectAction_hotfix;
    private LuaFunction m_add_EventOnGotoButtonClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnGotoButtonClickAction`1_hotfix;
    private LuaFunction m_add_EventOnGetButtonClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnGetButtonClickAction`1_hotfix;
    private LuaFunction m_set_MissionMission_hotfix;
    private LuaFunction m_get_Mission_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitOpenServiceMissonInfo(Mission mission, bool hasGot)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetOpenServiceMissionInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetOpenServiceMissionState(bool hasGot)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGotoButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGetButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayGetRewardEffect(Action OnStateFinish)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<GetPathData> EventOnGotoButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<BackFlowMissonUIController> EventOnGetButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Mission Mission
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public BackFlowMissonUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnGotoButtonClick(GetPathData obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnGotoButtonClick(GetPathData obj)
    {
      this.EventOnGotoButtonClick = (Action<GetPathData>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnGetButtonClick(BackFlowMissonUIController obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnGetButtonClick(BackFlowMissonUIController obj)
    {
      this.EventOnGetButtonClick = (Action<BackFlowMissonUIController>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private BackFlowMissonUIController m_owner;

      public LuaExportHelper(BackFlowMissonUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnGotoButtonClick(GetPathData obj)
      {
        this.m_owner.__callDele_EventOnGotoButtonClick(obj);
      }

      public void __clearDele_EventOnGotoButtonClick(GetPathData obj)
      {
        this.m_owner.__clearDele_EventOnGotoButtonClick(obj);
      }

      public void __callDele_EventOnGetButtonClick(BackFlowMissonUIController obj)
      {
        this.m_owner.__callDele_EventOnGetButtonClick(obj);
      }

      public void __clearDele_EventOnGetButtonClick(BackFlowMissonUIController obj)
      {
        this.m_owner.__clearDele_EventOnGetButtonClick(obj);
      }

      public Text m_infoText
      {
        get
        {
          return this.m_owner.m_infoText;
        }
        set
        {
          this.m_owner.m_infoText = value;
        }
      }

      public GameObject m_itemDummy
      {
        get
        {
          return this.m_owner.m_itemDummy;
        }
        set
        {
          this.m_owner.m_itemDummy = value;
        }
      }

      public Image m_progressBar
      {
        get
        {
          return this.m_owner.m_progressBar;
        }
        set
        {
          this.m_owner.m_progressBar = value;
        }
      }

      public CommonUIStateController m_progressBarValueStateCtrl
      {
        get
        {
          return this.m_owner.m_progressBarValueStateCtrl;
        }
        set
        {
          this.m_owner.m_progressBarValueStateCtrl = value;
        }
      }

      public Text m_progressHaveCount
      {
        get
        {
          return this.m_owner.m_progressHaveCount;
        }
        set
        {
          this.m_owner.m_progressHaveCount = value;
        }
      }

      public Text m_progressNeedCount
      {
        get
        {
          return this.m_owner.m_progressNeedCount;
        }
        set
        {
          this.m_owner.m_progressNeedCount = value;
        }
      }

      public GameObject m_progressDoneText
      {
        get
        {
          return this.m_owner.m_progressDoneText;
        }
        set
        {
          this.m_owner.m_progressDoneText = value;
        }
      }

      public CommonUIStateController m_buttonGroupStateCtrl
      {
        get
        {
          return this.m_owner.m_buttonGroupStateCtrl;
        }
        set
        {
          this.m_owner.m_buttonGroupStateCtrl = value;
        }
      }

      public Button m_gotoButton
      {
        get
        {
          return this.m_owner.m_gotoButton;
        }
        set
        {
          this.m_owner.m_gotoButton = value;
        }
      }

      public Button m_getButton
      {
        get
        {
          return this.m_owner.m_getButton;
        }
        set
        {
          this.m_owner.m_getButton = value;
        }
      }

      public Mission Mission
      {
        set
        {
          this.m_owner.Mission = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void SetOpenServiceMissionInfo()
      {
        this.m_owner.SetOpenServiceMissionInfo();
      }

      public void SetOpenServiceMissionState(bool hasGot)
      {
        this.m_owner.SetOpenServiceMissionState(hasGot);
      }

      public void OnGotoButtonClick()
      {
        this.m_owner.OnGotoButtonClick();
      }

      public void OnGetButtonClick()
      {
        this.m_owner.OnGetButtonClick();
      }
    }
  }
}
