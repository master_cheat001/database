﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ChatUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using MarchingBytes;
using SLua;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class ChatUIController : UIControllerBase
  {
    private bool m_InputFocused;
    private Action<ChatComponent.ChatMessageClient> m_itemVoiceButtonClickEvent;
    private Action<ChatMessage, GameObject> m_itemPlayerIconClickEvent;
    private Action<ulong, ChatBagItemMessage> m_heroEquipmentLinkClickAction;
    private Action<ChatPeakArenaBattleReportMessage> m_battleReportClickAction;
    private ChatChannel m_currentTableType;
    private List<ChatComponent.ChatMessageClient> m_currentChatList;
    private List<GameObject> m_redMarkObjs;
    private ProjectLPlayerContext m_playerContext;
    private FieldInfo m_inputPanelFieldInfo_AllowInput;
    private Coroutine m_coroutineTalkButtonTestHolding;
    private IConfigDataLoader m_configDataLoader;
    private int m_lastUpdateChatInfoListMaxIndex;
    private List<string> m_idList;
    private List<string> m_nameList;
    private ChatComponent.ChatMessageClient m_currPlayVoiceMsg;
    private ChatChannel m_currentChannel;
    private Button m_lastSelectButton;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("./ChatPanel/ChatDetail/RoomIDInput/InputField", AutoBindAttribute.InitState.NotInit, false)]
    public InputField RoomInput;
    [AutoBind("./ChatPanel/ChatDetail/RoomIDInput/InputField/Placeholder", AutoBindAttribute.InitState.NotInit, false)]
    public Text RoomIDShowText;
    [AutoBind("./ChatPanel/ChatDetail/RoomIDInput/InputField/Text", AutoBindAttribute.InitState.NotInit, false)]
    public Text RoomRequireIDText;
    [AutoBind("./ChatPanel/ChatDetail/RoomIDInput", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject RoomIdShowRoot;
    [AutoBind("./ChatPanel/ChatDetail/RoomIDInput/ChangeRoomButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button ChangeRoomButton;
    [AutoBind("./ChatPanel/SoundMessage/Send/Text", AutoBindAttribute.InitState.NotInit, false)]
    public Text voiceTimeText;
    [AutoBind("./ChatPanel/SoundMessage", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController voiceRecordStateUICtrl;
    [AutoBind("./ChatPanel/SoundMessage/Send/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
    public Image recordTimeProgressBar;
    [AutoBind("./ChatPanel/ChatDetail/ListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    public EasyObjectPool m_objPool;
    [AutoBind("./ChatPanel/ChatDetail/UnusedRoot", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject UnusedRoot;
    [AutoBind("./ChatPanel/ChatDetail/ListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject ListScrollViewContent;
    [AutoBind("./ChatPanel/ChatDetail/ListScrollView/", AutoBindAttribute.InitState.NotInit, false)]
    public ChatLoopVerticalScrollRect ScrollView;
    [AutoBind("./ChatPanel/ChatDetail/ListScrollView/Viewport", AutoBindAttribute.InitState.NotInit, false)]
    public RectTransform ViewPort;
    [AutoBind("./ChatPanel/ChannelPanel/WorldToggle", AutoBindAttribute.InitState.NotInit, false)]
    public Button WorldButton;
    [AutoBind("./ChatPanel/ChannelPanel/SystemToggle", AutoBindAttribute.InitState.NotInit, false)]
    public Button SystemButton;
    [AutoBind("./ChatPanel/ChannelPanel/TeamToggle", AutoBindAttribute.InitState.NotInit, false)]
    public Button TeamButton;
    [AutoBind("./ChatPanel/ChannelPanel/GroupToggle", AutoBindAttribute.InitState.NotInit, false)]
    public Button GroupButton;
    [AutoBind("./ChatPanel/ChannelPanel/PrivateToggle", AutoBindAttribute.InitState.NotInit, false)]
    public Button PrivateButton;
    [AutoBind("./ChatPanel/ChannelPanel/SociatyToggle", AutoBindAttribute.InitState.NotInit, false)]
    public Button GuildButton;
    [AutoBind("./ChatPanel/ChannelPanel/WorldToggle/RedMark", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject WorldToggleRedMark;
    [AutoBind("./ChatPanel/ChannelPanel/SystemToggle/RedMark", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject SystemToggleRedMark;
    [AutoBind("./ChatPanel/ChannelPanel/TeamToggle/RedMark", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject TeamToggleRedMark;
    [AutoBind("./ChatPanel/ChannelPanel/GroupToggle/RedMark", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject GroupToggleRedMark;
    [AutoBind("./ChatPanel/ChannelPanel/PrivateToggle/RedMark", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject PrivateToggleRedMark;
    [AutoBind("./ChatPanel/ChannelPanel/SociatyToggle/RedMark", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject GuildToggleRedMark;
    [AutoBind("./ChatPanel/CloseBtn", AutoBindAttribute.InitState.NotInit, false)]
    public Button CloseButton;
    [AutoBind("./BGButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button BGButton;
    [AutoBind("./EmptyPanel", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject EmptyPanel;
    [AutoBind("./ChatPanel/NewChatTip", AutoBindAttribute.InitState.NotInit, false)]
    public Button NewChatTip;
    [AutoBind("./ChatPanel/InputPanel/TalkButtonImage/TalkButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button TalkButton;
    [AutoBind("./ChatPanel/InputPanel/EmoticonButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button ExpressionFunctionButton;
    [AutoBind("./ChatPanel/InputPanel/InputField", AutoBindAttribute.InitState.NotInit, false)]
    public InputField InputPanel;
    [AutoBind("./ChatPanel/InputPanel/InputField/Text", AutoBindAttribute.InitState.NotInit, false)]
    public Text Placeholder;
    [AutoBind("./ChatPanel/InputPanel/InputField/Text", AutoBindAttribute.InitState.NotInit, false)]
    public Text ContentText;
    [AutoBind("./ChatPanel/InputPanel/SendButton", AutoBindAttribute.InitState.NotInit, false)]
    public ButtonEx SendButton;
    [AutoBind("./ChatPanel/InputPanel/CantInput", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject CantInputObj;
    [AutoBind("./ChatPanel/ChatDetail/PrivatePlayerChoose", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject ChooseListRoot;
    [AutoBind("./ChatPanel/ChatDetail/PrivatePlayerChoose", AutoBindAttribute.InitState.NotInit, false)]
    public Dropdown ChatTargetChooseDropdown;
    [AutoBind("./ChatPanel/ChatDetail/PrivatePlayerChoose/Label", AutoBindAttribute.InitState.NotInit, false)]
    public Text PlayerNameLabel;
    private const string emojiInfoTxtName = "EmojiUnicodeInfo";
    private const string emojiShowAssetName = "EmojiShowImage";
    private const string ChatManagerItem = "ChatManagerItem";
    private const string LeftWorldChatItem = "WorldChatItemLeft";
    private const string RightWorldChatItem = "WorldChatItemRight";
    private const string LeftSystemChatItem = "SystemChatItem";
    private const string ChatManagerItemPoolName = "ChatManagerPool";
    private const string LeftWorldChatItemPoolName = "WorldLeftPool";
    private const string RightWorldChatItemPoolName = "WorldRightPool";
    private const string LeftSystemChatItemPoolName = "SystemPool";
    [DoNotToLua]
    private ChatUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_OnPoolObjectCreatedStringGameObject_hotfix;
    private LuaFunction m_Update_hotfix;
    private LuaFunction m_OnRoomIdInputEndString_hotfix;
    private LuaFunction m_OnExpressionFunctionButtonClick_hotfix;
    private LuaFunction m_OnTalkButtonClickDownGameObject_hotfix;
    private LuaFunction m_OnTalkButtonTestHoding_hotfix;
    private LuaFunction m_OnTalkButtonClickUpGameObject_hotfix;
    private LuaFunction m_OnExitTalkButtonGameObject_hotfix;
    private LuaFunction m_OnEnterTalkButtonGameObject_hotfix;
    private LuaFunction m_OnSendButtonClick_hotfix;
    private LuaFunction m_OnCloseButtonClick_hotfix;
    private LuaFunction m_OnTableSelectedButton_hotfix;
    private LuaFunction m_OnChatTargetChangedInt32_hotfix;
    private LuaFunction m_OnChooseTargetRootClick_hotfix;
    private LuaFunction m_OnChangeRoomButtonClick_hotfix;
    private LuaFunction m_RefreshDropDownListChatChannel_hotfix;
    private LuaFunction m_InputPanel_OnEndEditString_hotfix;
    private LuaFunction m_ShowOrHidePanelBoolean_hotfix;
    private LuaFunction m_ShowNewChatTip_hotfix;
    private LuaFunction m_InputFieldTextAppendString_hotfix;
    private LuaFunction m_UpdateChatListList`1ChatChannelBooleanBooleanBoolean_hotfix;
    private LuaFunction m_UpdateChatCacheInfoList`1_hotfix;
    private LuaFunction m_GetChannelToggleSelectStateChatChannel_hotfix;
    private LuaFunction m_SetChannelToggleSelectedChatChannel_hotfix;
    private LuaFunction m_IsScrollViewInEnd_hotfix;
    private LuaFunction m_UpdateChooseChatPlayerOrGroupListInfoChatChannelList`1List`1_hotfix;
    private LuaFunction m_ShowOrHideChooseChatTargetPannelBoolean_hotfix;
    private LuaFunction m_SetTalkButtonSizeVector2_hotfix;
    private LuaFunction m_ShowVoiceRecordTip_hotfix;
    private LuaFunction m_HideVoiceRecordTip_hotfix;
    private LuaFunction m_ShowVoiceCancelTip_hotfix;
    private LuaFunction m_ShowVoiceShortTip_hotfix;
    private LuaFunction m_UpdateVoiceRecordTimeSingle_hotfix;
    private LuaFunction m_RegItemButtonClickEventAction`1Action`2Action`2Action`1_hotfix;
    private LuaFunction m_SetRoomIdTipInt32_hotfix;
    private LuaFunction m_EnableRoomIdShowBoolean_hotfix;
    private LuaFunction m_CloseAllTableRedMark_hotfix;
    private LuaFunction m_SetTableRedMarkChatChannel_hotfix;
    private LuaFunction m_ClearChatContent_hotfix;
    private LuaFunction m_OnChatItemFillUIControllerBase_hotfix;
    private LuaFunction m_GetShowChatItemPoolName4InfoChatMessage_hotfix;
    private LuaFunction m_SetScrollViewToBottom_hotfix;
    private LuaFunction m_FindChannelButtonChatChannel_hotfix;
    private LuaFunction m_add_EventOnTableChangeAction`1_hotfix;
    private LuaFunction m_remove_EventOnTableChangeAction`1_hotfix;
    private LuaFunction m_add_EventOnSendButtonClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnSendButtonClickAction`1_hotfix;
    private LuaFunction m_add_EventOnCloseButtonClickAction_hotfix;
    private LuaFunction m_remove_EventOnCloseButtonClickAction_hotfix;
    private LuaFunction m_add_EventOnExpressionFunctionButtonClickAction_hotfix;
    private LuaFunction m_remove_EventOnExpressionFunctionButtonClickAction_hotfix;
    private LuaFunction m_add_EventOnRoomIdInputEndAction`1_hotfix;
    private LuaFunction m_remove_EventOnRoomIdInputEndAction`1_hotfix;
    private LuaFunction m_add_EventOnTalkButtonHoldAction_hotfix;
    private LuaFunction m_remove_EventOnTalkButtonHoldAction_hotfix;
    private LuaFunction m_add_EventOnTalkButtonClickUpAction_hotfix;
    private LuaFunction m_remove_EventOnTalkButtonClickUpAction_hotfix;
    private LuaFunction m_add_EventOnExitTalkButtonClickAction_hotfix;
    private LuaFunction m_remove_EventOnExitTalkButtonClickAction_hotfix;
    private LuaFunction m_add_EventOnEnterTalkButtonClickAction_hotfix;
    private LuaFunction m_remove_EventOnEnterTalkButtonClickAction_hotfix;
    private LuaFunction m_add_EventOnChatTargetChangedAction`3_hotfix;
    private LuaFunction m_remove_EventOnChatTargetChangedAction`3_hotfix;
    private LuaFunction m_get_CurrPlayVoiceMsg_hotfix;
    private LuaFunction m_set_CurrPlayVoiceMsgChatMessageClient_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ChatUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPoolObjectCreated(string poolName, GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRoomIdInputEnd(string content)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnExpressionFunctionButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTalkButtonClickDown(GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator OnTalkButtonTestHoding()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTalkButtonClickUp(GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnExitTalkButton(GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEnterTalkButton(GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSendButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTableSelected(Button currentButton)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChatTargetChanged(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChooseTargetRootClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChangeRoomButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RefreshDropDownList(ChatChannel channel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InputPanel_OnEndEdit(string str)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowOrHidePanel(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowNewChatTip()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InputFieldTextAppend(string appendStr)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateChatList(
      List<ChatComponent.ChatMessageClient> currentChatList,
      ChatChannel channel,
      bool isRefill,
      bool isScroll = false,
      bool hasNew = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateChatCacheInfo(List<ChatComponent.ChatMessageClient> chatInfoList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool GetChannelToggleSelectState(ChatChannel channel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetChannelToggleSelected(ChatChannel channel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsScrollViewInEnd()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateChooseChatPlayerOrGroupListInfo(
      ChatChannel channel,
      List<string> idList,
      List<string> nameList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowOrHideChooseChatTargetPannel(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTalkButtonSize(Vector2 size)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowVoiceRecordTip()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideVoiceRecordTip()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowVoiceCancelTip()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowVoiceShortTip()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateVoiceRecordTime(float time)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RegItemButtonClickEvent(
      Action<ChatComponent.ChatMessageClient> voiceClickAction,
      Action<ChatMessage, GameObject> playerIconClickAction,
      Action<ulong, ChatBagItemMessage> heroEquipmentLinkClickAction,
      Action<ChatPeakArenaBattleReportMessage> battleReportClickAction)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRoomIdTip(int roomId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void EnableRoomIdShow(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CloseAllTableRedMark()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTableRedMark(ChatChannel channel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearChatContent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChatItemFill(UIControllerBase uCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string GetShowChatItemPoolName4Info(ChatMessage chatInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetScrollViewToBottom()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Button FindChannelButton(ChatChannel channel)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<ChatChannel> EventOnTableChange
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string> EventOnSendButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnCloseButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnExpressionFunctionButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string> EventOnRoomIdInputEnd
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnTalkButtonHold
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnTalkButtonClickUp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnExitTalkButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnEnterTalkButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ChatChannel, string, string> EventOnChatTargetChanged
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ChatComponent.ChatMessageClient CurrPlayVoiceMsg
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public ChatUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnTableChange(ChatChannel obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnTableChange(ChatChannel obj)
    {
      this.EventOnTableChange = (Action<ChatChannel>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnSendButtonClick(string obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnSendButtonClick(string obj)
    {
      this.EventOnSendButtonClick = (Action<string>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnCloseButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnCloseButtonClick()
    {
      this.EventOnCloseButtonClick = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnExpressionFunctionButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnExpressionFunctionButtonClick()
    {
      this.EventOnExpressionFunctionButtonClick = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnRoomIdInputEnd(string obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnRoomIdInputEnd(string obj)
    {
      this.EventOnRoomIdInputEnd = (Action<string>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnTalkButtonHold()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnTalkButtonHold()
    {
      this.EventOnTalkButtonHold = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnTalkButtonClickUp()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnTalkButtonClickUp()
    {
      this.EventOnTalkButtonClickUp = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnExitTalkButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnExitTalkButtonClick()
    {
      this.EventOnExitTalkButtonClick = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnEnterTalkButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnEnterTalkButtonClick()
    {
      this.EventOnEnterTalkButtonClick = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnChatTargetChanged(ChatChannel arg1, string arg2, string arg3)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnChatTargetChanged(ChatChannel arg1, string arg2, string arg3)
    {
      this.EventOnChatTargetChanged = (Action<ChatChannel, string, string>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_m_itemVoiceButtonClickEvent(ChatComponent.ChatMessageClient obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_m_itemVoiceButtonClickEvent(ChatComponent.ChatMessageClient obj)
    {
      this.m_itemVoiceButtonClickEvent = (Action<ChatComponent.ChatMessageClient>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_m_itemPlayerIconClickEvent(ChatMessage arg1, GameObject arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_m_itemPlayerIconClickEvent(ChatMessage arg1, GameObject arg2)
    {
      this.m_itemPlayerIconClickEvent = (Action<ChatMessage, GameObject>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_m_heroEquipmentLinkClickAction(ulong arg1, ChatBagItemMessage arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_m_heroEquipmentLinkClickAction(ulong arg1, ChatBagItemMessage arg2)
    {
      this.m_heroEquipmentLinkClickAction = (Action<ulong, ChatBagItemMessage>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_m_battleReportClickAction(ChatPeakArenaBattleReportMessage obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_m_battleReportClickAction(ChatPeakArenaBattleReportMessage obj)
    {
      this.m_battleReportClickAction = (Action<ChatPeakArenaBattleReportMessage>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private ChatUIController m_owner;

      public LuaExportHelper(ChatUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnTableChange(ChatChannel obj)
      {
        this.m_owner.__callDele_EventOnTableChange(obj);
      }

      public void __clearDele_EventOnTableChange(ChatChannel obj)
      {
        this.m_owner.__clearDele_EventOnTableChange(obj);
      }

      public void __callDele_EventOnSendButtonClick(string obj)
      {
        this.m_owner.__callDele_EventOnSendButtonClick(obj);
      }

      public void __clearDele_EventOnSendButtonClick(string obj)
      {
        this.m_owner.__clearDele_EventOnSendButtonClick(obj);
      }

      public void __callDele_EventOnCloseButtonClick()
      {
        this.m_owner.__callDele_EventOnCloseButtonClick();
      }

      public void __clearDele_EventOnCloseButtonClick()
      {
        this.m_owner.__clearDele_EventOnCloseButtonClick();
      }

      public void __callDele_EventOnExpressionFunctionButtonClick()
      {
        this.m_owner.__callDele_EventOnExpressionFunctionButtonClick();
      }

      public void __clearDele_EventOnExpressionFunctionButtonClick()
      {
        this.m_owner.__clearDele_EventOnExpressionFunctionButtonClick();
      }

      public void __callDele_EventOnRoomIdInputEnd(string obj)
      {
        this.m_owner.__callDele_EventOnRoomIdInputEnd(obj);
      }

      public void __clearDele_EventOnRoomIdInputEnd(string obj)
      {
        this.m_owner.__clearDele_EventOnRoomIdInputEnd(obj);
      }

      public void __callDele_EventOnTalkButtonHold()
      {
        this.m_owner.__callDele_EventOnTalkButtonHold();
      }

      public void __clearDele_EventOnTalkButtonHold()
      {
        this.m_owner.__clearDele_EventOnTalkButtonHold();
      }

      public void __callDele_EventOnTalkButtonClickUp()
      {
        this.m_owner.__callDele_EventOnTalkButtonClickUp();
      }

      public void __clearDele_EventOnTalkButtonClickUp()
      {
        this.m_owner.__clearDele_EventOnTalkButtonClickUp();
      }

      public void __callDele_EventOnExitTalkButtonClick()
      {
        this.m_owner.__callDele_EventOnExitTalkButtonClick();
      }

      public void __clearDele_EventOnExitTalkButtonClick()
      {
        this.m_owner.__clearDele_EventOnExitTalkButtonClick();
      }

      public void __callDele_EventOnEnterTalkButtonClick()
      {
        this.m_owner.__callDele_EventOnEnterTalkButtonClick();
      }

      public void __clearDele_EventOnEnterTalkButtonClick()
      {
        this.m_owner.__clearDele_EventOnEnterTalkButtonClick();
      }

      public void __callDele_EventOnChatTargetChanged(ChatChannel arg1, string arg2, string arg3)
      {
        this.m_owner.__callDele_EventOnChatTargetChanged(arg1, arg2, arg3);
      }

      public void __clearDele_EventOnChatTargetChanged(ChatChannel arg1, string arg2, string arg3)
      {
        this.m_owner.__clearDele_EventOnChatTargetChanged(arg1, arg2, arg3);
      }

      public void __callDele_m_itemVoiceButtonClickEvent(ChatComponent.ChatMessageClient obj)
      {
        this.m_owner.__callDele_m_itemVoiceButtonClickEvent(obj);
      }

      public void __clearDele_m_itemVoiceButtonClickEvent(ChatComponent.ChatMessageClient obj)
      {
        this.m_owner.__clearDele_m_itemVoiceButtonClickEvent(obj);
      }

      public void __callDele_m_itemPlayerIconClickEvent(ChatMessage arg1, GameObject arg2)
      {
        this.m_owner.__callDele_m_itemPlayerIconClickEvent(arg1, arg2);
      }

      public void __clearDele_m_itemPlayerIconClickEvent(ChatMessage arg1, GameObject arg2)
      {
        this.m_owner.__clearDele_m_itemPlayerIconClickEvent(arg1, arg2);
      }

      public void __callDele_m_heroEquipmentLinkClickAction(ulong arg1, ChatBagItemMessage arg2)
      {
        this.m_owner.__callDele_m_heroEquipmentLinkClickAction(arg1, arg2);
      }

      public void __clearDele_m_heroEquipmentLinkClickAction(ulong arg1, ChatBagItemMessage arg2)
      {
        this.m_owner.__clearDele_m_heroEquipmentLinkClickAction(arg1, arg2);
      }

      public void __callDele_m_battleReportClickAction(ChatPeakArenaBattleReportMessage obj)
      {
        this.m_owner.__callDele_m_battleReportClickAction(obj);
      }

      public void __clearDele_m_battleReportClickAction(ChatPeakArenaBattleReportMessage obj)
      {
        this.m_owner.__clearDele_m_battleReportClickAction(obj);
      }

      public bool m_InputFocused
      {
        get
        {
          return this.m_owner.m_InputFocused;
        }
        set
        {
          this.m_owner.m_InputFocused = value;
        }
      }

      public Action<ChatComponent.ChatMessageClient> m_itemVoiceButtonClickEvent
      {
        get
        {
          return this.m_owner.m_itemVoiceButtonClickEvent;
        }
        set
        {
          this.m_owner.m_itemVoiceButtonClickEvent = value;
        }
      }

      public Action<ChatMessage, GameObject> m_itemPlayerIconClickEvent
      {
        get
        {
          return this.m_owner.m_itemPlayerIconClickEvent;
        }
        set
        {
          this.m_owner.m_itemPlayerIconClickEvent = value;
        }
      }

      public Action<ulong, ChatBagItemMessage> m_heroEquipmentLinkClickAction
      {
        get
        {
          return this.m_owner.m_heroEquipmentLinkClickAction;
        }
        set
        {
          this.m_owner.m_heroEquipmentLinkClickAction = value;
        }
      }

      public Action<ChatPeakArenaBattleReportMessage> m_battleReportClickAction
      {
        get
        {
          return this.m_owner.m_battleReportClickAction;
        }
        set
        {
          this.m_owner.m_battleReportClickAction = value;
        }
      }

      public ChatChannel m_currentTableType
      {
        get
        {
          return this.m_owner.m_currentTableType;
        }
        set
        {
          this.m_owner.m_currentTableType = value;
        }
      }

      public List<ChatComponent.ChatMessageClient> m_currentChatList
      {
        get
        {
          return this.m_owner.m_currentChatList;
        }
        set
        {
          this.m_owner.m_currentChatList = value;
        }
      }

      public List<GameObject> m_redMarkObjs
      {
        get
        {
          return this.m_owner.m_redMarkObjs;
        }
        set
        {
          this.m_owner.m_redMarkObjs = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public FieldInfo m_inputPanelFieldInfo_AllowInput
      {
        get
        {
          return this.m_owner.m_inputPanelFieldInfo_AllowInput;
        }
        set
        {
          this.m_owner.m_inputPanelFieldInfo_AllowInput = value;
        }
      }

      public Coroutine m_coroutineTalkButtonTestHolding
      {
        get
        {
          return this.m_owner.m_coroutineTalkButtonTestHolding;
        }
        set
        {
          this.m_owner.m_coroutineTalkButtonTestHolding = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public int m_lastUpdateChatInfoListMaxIndex
      {
        get
        {
          return this.m_owner.m_lastUpdateChatInfoListMaxIndex;
        }
        set
        {
          this.m_owner.m_lastUpdateChatInfoListMaxIndex = value;
        }
      }

      public List<string> m_idList
      {
        get
        {
          return this.m_owner.m_idList;
        }
        set
        {
          this.m_owner.m_idList = value;
        }
      }

      public List<string> m_nameList
      {
        get
        {
          return this.m_owner.m_nameList;
        }
        set
        {
          this.m_owner.m_nameList = value;
        }
      }

      public ChatComponent.ChatMessageClient m_currPlayVoiceMsg
      {
        get
        {
          return this.m_owner.m_currPlayVoiceMsg;
        }
        set
        {
          this.m_owner.m_currPlayVoiceMsg = value;
        }
      }

      public ChatChannel m_currentChannel
      {
        get
        {
          return this.m_owner.m_currentChannel;
        }
        set
        {
          this.m_owner.m_currentChannel = value;
        }
      }

      public Button m_lastSelectButton
      {
        get
        {
          return this.m_owner.m_lastSelectButton;
        }
        set
        {
          this.m_owner.m_lastSelectButton = value;
        }
      }

      public RectTransform m_marginTransform
      {
        get
        {
          return this.m_owner.m_marginTransform;
        }
        set
        {
          this.m_owner.m_marginTransform = value;
        }
      }

      public static string emojiInfoTxtName
      {
        get
        {
          return "EmojiUnicodeInfo";
        }
      }

      public static string emojiShowAssetName
      {
        get
        {
          return "EmojiShowImage";
        }
      }

      public static string ChatManagerItem
      {
        get
        {
          return nameof (ChatManagerItem);
        }
      }

      public static string LeftWorldChatItem
      {
        get
        {
          return "WorldChatItemLeft";
        }
      }

      public static string RightWorldChatItem
      {
        get
        {
          return "WorldChatItemRight";
        }
      }

      public static string LeftSystemChatItem
      {
        get
        {
          return "SystemChatItem";
        }
      }

      public static string ChatManagerItemPoolName
      {
        get
        {
          return "ChatManagerPool";
        }
      }

      public static string LeftWorldChatItemPoolName
      {
        get
        {
          return "WorldLeftPool";
        }
      }

      public static string RightWorldChatItemPoolName
      {
        get
        {
          return "WorldRightPool";
        }
      }

      public static string LeftSystemChatItemPoolName
      {
        get
        {
          return "SystemPool";
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void OnPoolObjectCreated(string poolName, GameObject go)
      {
        this.m_owner.OnPoolObjectCreated(poolName, go);
      }

      public void Update()
      {
        this.m_owner.Update();
      }

      public void OnRoomIdInputEnd(string content)
      {
        this.m_owner.OnRoomIdInputEnd(content);
      }

      public void OnExpressionFunctionButtonClick()
      {
        this.m_owner.OnExpressionFunctionButtonClick();
      }

      public void OnTalkButtonClickDown(GameObject go)
      {
        this.m_owner.OnTalkButtonClickDown(go);
      }

      public IEnumerator OnTalkButtonTestHoding()
      {
        return this.m_owner.OnTalkButtonTestHoding();
      }

      public void OnTalkButtonClickUp(GameObject go)
      {
        this.m_owner.OnTalkButtonClickUp(go);
      }

      public void OnExitTalkButton(GameObject go)
      {
        this.m_owner.OnExitTalkButton(go);
      }

      public void OnEnterTalkButton(GameObject go)
      {
        this.m_owner.OnEnterTalkButton(go);
      }

      public void OnSendButtonClick()
      {
        this.m_owner.OnSendButtonClick();
      }

      public void OnCloseButtonClick()
      {
        this.m_owner.OnCloseButtonClick();
      }

      public void OnTableSelected(Button currentButton)
      {
        this.m_owner.OnTableSelected(currentButton);
      }

      public void OnChatTargetChanged(int index)
      {
        this.m_owner.OnChatTargetChanged(index);
      }

      public void OnChooseTargetRootClick()
      {
        this.m_owner.OnChooseTargetRootClick();
      }

      public void OnChangeRoomButtonClick()
      {
        this.m_owner.OnChangeRoomButtonClick();
      }

      public void InputPanel_OnEndEdit(string str)
      {
        this.m_owner.InputPanel_OnEndEdit(str);
      }

      public void OnChatItemFill(UIControllerBase uCtrl)
      {
        this.m_owner.OnChatItemFill(uCtrl);
      }

      public string GetShowChatItemPoolName4Info(ChatMessage chatInfo)
      {
        return this.m_owner.GetShowChatItemPoolName4Info(chatInfo);
      }

      public void SetScrollViewToBottom()
      {
        this.m_owner.SetScrollViewToBottom();
      }
    }
  }
}
