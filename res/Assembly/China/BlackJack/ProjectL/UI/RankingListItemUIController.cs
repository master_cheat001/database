﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.RankingListItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class RankingListItemUIController : UIControllerBase
  {
    public ScrollItemBaseUIController ScrollItemBaseUICtrl;
    [AutoBind("./Rank/RankValueText", AutoBindAttribute.InitState.NotInit, false)]
    public Text RankValueText;
    [AutoBind("./Rank/RankValueImage", AutoBindAttribute.InitState.NotInit, false)]
    public Image RankValueImage;
    [AutoBind("./Rank/PeakArenaImage", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController PeakArenaRankStateCtrl;
    [AutoBind("./HeadIconImage", AutoBindAttribute.InitState.NotInit, false)]
    public Image HeadIconImage;
    [AutoBind("./LevelValue", AutoBindAttribute.InitState.NotInit, false)]
    public Text PlayerLevelText;
    [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
    public Text PlayerNameText;
    [AutoBind("./DetailGroup", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController DetailGroupStateCtrl;
    [AutoBind("./DetailGroup/Top15Hero/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    public Text Top15HeroScoreValueText;
    [AutoBind("./DetailGroup/TopHero/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    public Text TopHeroScoreValueText;
    [AutoBind("./DetailGroup/AllHero/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    public Text AllHeroScoreValueText;
    [AutoBind("./DetailGroup/ChampionHero/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    public Text ChampionHeroScoreTitleText;
    [AutoBind("./DetailGroup/ChampionHero/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    public Text ChampionHeroScoreValueText;
    [AutoBind("./DetailGroup/RiftChapterStar/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    public Text RiftChapterStarScoreValueText;
    [AutoBind("./DetailGroup/RiftAchievement/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    public Text RiftAchievementScoreValueText;
    [AutoBind("./DetailGroup/ClimbTower/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    public Text ClimbTowerScoreValueText;
    [AutoBind("./DetailGroup/PlayerTitle/PlayerTitle/Text", AutoBindAttribute.InitState.NotInit, false)]
    public Text PlayerTitleValueText;
    [AutoBind("./DetailGroup/PlayerTitle/PlayerTitle/Image", AutoBindAttribute.InitState.NotInit, false)]
    public Image PlayerTitleImage;
    [AutoBind("./DetailGroup/PlayerTitle", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController PlayerTitleStateCtrl;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    public void Init()
    {
      this.ScrollItemBaseUICtrl.Init((UIControllerBase) this, true);
    }

    public void RegisterItemClickEvent(Action<UIControllerBase> action)
    {
      this.ScrollItemBaseUICtrl.EventOnUIItemClick = action;
    }

    public void RegisterItemNeedFillEvent(Action<UIControllerBase> action)
    {
      this.ScrollItemBaseUICtrl.EventOnUIItemNeedFill = action;
    }

    public int GetItemIndex()
    {
      return this.ScrollItemBaseUICtrl.ItemIndex;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateItemInfo(
      RankingListType rankType,
      int headIconId,
      int rankLevel,
      int lv,
      string playerName,
      int score,
      string heroName = "")
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateItemInfoInPeakArenaState(
      int headIconId,
      int rank,
      int lv,
      string playerName,
      int seasonId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected Sprite GetRankLevelSprite(int rankLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void UpdateDetailInfo(RankingListType rankType, string heroName, int score)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
