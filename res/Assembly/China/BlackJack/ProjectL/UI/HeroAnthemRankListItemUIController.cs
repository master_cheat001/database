﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroAnthemRankListItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.ProjectL.Common;
using SLua;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class HeroAnthemRankListItemUIController : DynamicGridCellController
  {
    [AutoBind("./RankingIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_rankImage;
    [AutoBind("./RankingText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_rankText;
    [AutoBind("./PlayerNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerNameText;
    [AutoBind("./HeadIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_headIcon;
    [AutoBind("./HeadIcon/HeadFrameDummy", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_headFrameDummy;
    [AutoBind("./HeadIcon/LevelText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_levelText;
    [AutoBind("./ArenaPoints/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_arenaPointsText;
    [DoNotToLua]
    private HeroAnthemRankListItemUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_SetHeroAnthemRankListItemRankingTargetPlayerInfoInt32_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHeroAnthemRankListItem(RankingTargetPlayerInfo playerInfo, int rankLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public HeroAnthemRankListItemUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_OnUpdateCell()
    {
      this.OnUpdateCell();
    }

    private void __callBase_OnRenovate()
    {
      this.OnRenovate();
    }

    private void __callBase_OnRecycle()
    {
      this.OnRecycle();
    }

    private void __callBase_Select()
    {
      this.Select();
    }

    private void __callBase_SendEvent(int eventId)
    {
      this.SendEvent(eventId);
    }

    private void __callBase_SendEvent(int eventId, object obj)
    {
      this.SendEvent(eventId, obj);
    }

    private void __callBase_UpdateCell(object info)
    {
      this.UpdateCell(info);
    }

    private void __callBase_Renovate()
    {
      this.Renovate();
    }

    private void __callBase_Recycle()
    {
      this.Recycle();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private HeroAnthemRankListItemUIController m_owner;

      public LuaExportHelper(HeroAnthemRankListItemUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_OnUpdateCell()
      {
        this.m_owner.__callBase_OnUpdateCell();
      }

      public void __callBase_OnRenovate()
      {
        this.m_owner.__callBase_OnRenovate();
      }

      public void __callBase_OnRecycle()
      {
        this.m_owner.__callBase_OnRecycle();
      }

      public void __callBase_Select()
      {
        this.m_owner.__callBase_Select();
      }

      public void __callBase_SendEvent(int eventId)
      {
        this.m_owner.__callBase_SendEvent(eventId);
      }

      public void __callBase_SendEvent(int eventId, object obj)
      {
        this.m_owner.__callBase_SendEvent(eventId, obj);
      }

      public void __callBase_UpdateCell(object info)
      {
        this.m_owner.__callBase_UpdateCell(info);
      }

      public void __callBase_Renovate()
      {
        this.m_owner.__callBase_Renovate();
      }

      public void __callBase_Recycle()
      {
        this.m_owner.__callBase_Recycle();
      }

      public Image m_rankImage
      {
        get
        {
          return this.m_owner.m_rankImage;
        }
        set
        {
          this.m_owner.m_rankImage = value;
        }
      }

      public Text m_rankText
      {
        get
        {
          return this.m_owner.m_rankText;
        }
        set
        {
          this.m_owner.m_rankText = value;
        }
      }

      public Text m_playerNameText
      {
        get
        {
          return this.m_owner.m_playerNameText;
        }
        set
        {
          this.m_owner.m_playerNameText = value;
        }
      }

      public Image m_headIcon
      {
        get
        {
          return this.m_owner.m_headIcon;
        }
        set
        {
          this.m_owner.m_headIcon = value;
        }
      }

      public Transform m_headFrameDummy
      {
        get
        {
          return this.m_owner.m_headFrameDummy;
        }
        set
        {
          this.m_owner.m_headFrameDummy = value;
        }
      }

      public Text m_levelText
      {
        get
        {
          return this.m_owner.m_levelText;
        }
        set
        {
          this.m_owner.m_levelText = value;
        }
      }

      public Text m_arenaPointsText
      {
        get
        {
          return this.m_owner.m_arenaPointsText;
        }
        set
        {
          this.m_owner.m_arenaPointsText = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }
    }
  }
}
