﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroInteractNetTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Protocol;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class HeroInteractNetTask : UINetTask
  {
    private int m_heroId;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroInteractNetTask(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnHeroInteractAck(
      HeroInteractAck msg,
      List<Goods> rewards,
      int heroPerformanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool StartNetWorking()
    {
      // ISSUE: unable to decompile the method.
    }

    public int HeroPerformanceId { private set; get; }

    public HeroInteractionResultType InteractResult { private set; get; }

    public List<Goods> Rewards { private set; get; }

    public int AddFavoribilityExp { private set; get; }
  }
}
