﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PeakArenaUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Misc;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class PeakArenaUIController : UIControllerBase
  {
    private int m_knockoutGroupId = -1;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./HelpButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_helpButton;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./ScoreText", AutoBindAttribute.InitState.Inactive, false)]
    private Text m_scoreText;
    [AutoBind("./QuitCountText", AutoBindAttribute.InitState.Inactive, false)]
    private Text m_quitCountText;
    [AutoBind("./PlayerResource/Gold", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_goldGameObject;
    [AutoBind("./PlayerResource/ArenaCoin", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_arenaCoinGameObject;
    [AutoBind("./PlayerResource/ArenaCoin/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_arenaCoinText;
    [AutoBind("./PlayerResource/IdolsTicket", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jettonGameObject;
    [AutoBind("./PlayerResource/IdolsTicket/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_jettonText;
    [AutoBind("./PlayerResource/IdolsTicket/AddButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_jettonButton;
    [AutoBind("./TeamCompiledButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_managementTeamButton;
    [AutoBind("./TeamCompiledButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_managementTeamButtonAnimation;
    [AutoBind("./Detail/Margin", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_peakMarginTransform;
    [AutoBind("./Detail/Margin/Tabs/ClashToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_peakClashToggle;
    [AutoBind("./Detail/Margin/Tabs/BattleReportToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_peakBattleReportToggle;
    [AutoBind("./Detail/Margin/Tabs/ArenaLevelToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_peakDanToggle;
    [AutoBind("./Detail/Margin/Tabs/LocalRankingToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_peakLocalRankingToggle;
    [AutoBind("./Detail/Margin/Tabs/MatchToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_peakKnockoutToggle;
    [AutoBind("./Detail/Margin/Tabs/ClashToggle", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_peakClashToggleState;
    [AutoBind("./Detail/Margin/Tabs/BattleReportToggle", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_peakBattleReportToggleState;
    [AutoBind("./Detail/Margin/Tabs/ArenaLevelToggle", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_peakDanToggleState;
    [AutoBind("./Detail/Margin/Tabs/LocalRankingToggle", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_peakLocalRankingToggleState;
    [AutoBind("./Detail/Margin/Tabs/MatchToggle", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_peakKnockoutToggleState;
    [AutoBind("./Detail/Margin/Tabs/ClashToggle/Lock", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_peakClashLockButton;
    [AutoBind("./Detail/Margin/Tabs/BattleReportToggle/Lock", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_peakBattleReportLockButton;
    [AutoBind("./Detail/Margin/Tabs/ArenaLevelToggle/Lock", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_peakDanLockButton;
    [AutoBind("./Detail/Margin/Tabs/LocalRankingToggle/Lock", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_peakLocalRankingLockButton;
    [AutoBind("./Detail/Margin/Tabs/MatchToggle/Lock", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_peakKnockoutLockButton;
    [AutoBind("./Detail/Margin/Tabs/LocalRankingToggle/Click", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_peakLocalRankingClickGameObject;
    [AutoBind("./Detail/Margin/Tabs/MatchToggle/Click", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_peakKnockoutClickGameObject;
    [AutoBind("./Detail/Panels/Clash/LadderMode/ChallengeButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_peakClashLadderChallengeButton;
    [AutoBind("./Detail/Panels/Clash/LadderMode/ChallengeButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_peakClashLadderChallengeButtonUIStateController;
    [AutoBind("./Detail/Panels/Clash/CasualMode/ChallengeButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_peakClashCasualChallengeButton;
    [AutoBind("./Detail/Panels/Clash/LadderMode/AreaGroup/AreaNameGroup/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakClashLadderModeAreaNameText;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_peakClashLadderModeUIStateController;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/RestPanel/BeginTimeGroup/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakClashLadderRestPanelRestModeBeginTimeTitleText;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/RestPanel/BeginTimeGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakClashLadderRestPanelRestModeBeginTimeText;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/KnockOutMatchBeginTimePanel/TopNotceGroup/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakClashLadderBeginTimeModeTopTitleText;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/KnockOutMatchBeginTimePanel/NoticGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakClashLadderBeginTimeModeOpenTimeText;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/KnockOutMatchOverTimePanel/TopNotceGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakClashLadderMatchModeTimeText;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/KnockOutMatchOverTimePanel/NoticGroup/TimesText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakClashLadderMatchModeBattleTimesText;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/KnockOutMatchOverTimePanel/NoticGroup/WinText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakClashLadderMatchModeBattleWinText;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/KnockOutMatchOverTimePanel/NoticGroup/FailedText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakClashLadderMatchModeBattleFailedText;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/KnockOutMatchOverTimePanel/WinRate/WinRateValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakClashLadderMatchModeWinRateValueText;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/KnockOutMatchOverTimePanel/LevelImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_peakClashLadderMatchModeLevelImage;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/GradingMatchCompetitionInfo/TopNotceGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakClashLadderGradingMatchModeTimeText;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/GradingMatchCompetitionInfo/NoticGroup/GradingMatchText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakClashLadderGradingMatchModeBattleTGradingMatchText;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/GradingMatchCompetitionInfo/NoticGroup/WinText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakClashLadderGradingMatchModeBattleWinText;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/GradingMatchCompetitionInfo/NoticGroup/FailedText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakClashLadderGradingMatchModeBattleFailedText;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/GradingMatchCompetitionInfo/CompetitionRoundGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_peakClashLadderGradingMatchModeCompetitionRoundGameObject;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/OpponentPanel/TopNotceGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakClashLadderOpponentModeTimeText;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/OpponentPanel/OpponentInfo/HeadIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_peakClashLadderOpponentModeOpponentHeadIcon;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/OpponentPanel/OpponentInfo/HeadIcon/HeadFrameDummy", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_peakClashLadderOpponentModeOpponentHeadFrameDummy;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/OpponentPanel/OpponentInfo/HeadIcon/LevelText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakClashLadderOpponentModeOpponentLevelText;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/OpponentPanel/OpponentInfo/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakClashLadderOpponentModeOpponentNameText;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/OpponentPanel/OpponentInfo/Toggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_peakClashLadderOpponentModeOpponentInfoToggle;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/OpponentPanel/OpponentInfo/TimeLimit", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_peakClashLadderOpponentModeOpponentInfoTimeLimitGo;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/OpponentPanel/OpponentInfo/TimeLimit/TimeText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakClashLadderOpponentModeOpponentInfoTimeText;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/GradingMatchCloseTimePanelPanel/TopNotceGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakClashLadderGradingMatchCloseTimeModeEndTimeText;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/GradingMatchCloseTimePanelPanel/NoticGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakClashLadderGradingMatchCloseTimeModeTimeText;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/GradingMatchCloseTimePanelPanel/MatchRecord/NoticGroup/TimesText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakClashLadderGradingMatchCloseTimeModeMatchRecordTimesText;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/GradingMatchCloseTimePanelPanel/MatchRecord/NoticGroup/WinText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakClashLadderGradingMatchCloseTimeModeMatchRecordWinText;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/GradingMatchCloseTimePanelPanel/MatchRecord/NoticGroup/FailedText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakClashLadderGradingMatchCloseTimeModeMatchRecordFailedText;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/GradingMatchCloseTimePanelPanel/MatchRecord/WinRate/WinRateValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakClashLadderGradingMatchCloseTimeModeMatchRecordWinRateValueText;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/GradingMatchCloseTimePanelPanel/MatchRecord/LevelImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_peakClashLadderGradingMatchCloseTimeModeMatchRecordLevelImage;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/WaitOpponentPanel/TopNotceGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakClashLadderWaitOpponentModeTimeText;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/OpponentAbstentionPanel/TopNotceGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakClashLadderOpponentAbstentionModeTimeText;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/OpponentAbstentionPanel/NoticGroup/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakClashLadderOpponentAbstentionModeNoticeText;
    [AutoBind("./Detail/Panels/BattleReport/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_peakBattleReportScrollRect;
    [AutoBind("./Detail/Panels/BattleReport/EmptyPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_peakNoBattleReportGameObject;
    [AutoBind("./Detail/Panels/ArenaLevel/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_peakArenaLevelScrollRect;
    [AutoBind("./Prefabs/ArenaLevelListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_peakLevelListItemPrefab;
    [AutoBind("./Detail/Panels/GetLevelRewardNotice/FrameImage/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakArenaLevelGetRewardNoticeText;
    [AutoBind("./Detail/Panels/Ranking/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_peakRankingScrollRect;
    [AutoBind("./Prefabs/ArenaRankingListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_peakRankingListItemPrefab;
    [AutoBind("./Detail/Panels/Ranking/Player/PlayerNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakRankingPlayerNameText;
    [AutoBind("./Detail/Panels/Ranking/Player/Ranking", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_peakRankingPlayerRankingUIStateController;
    [AutoBind("./Detail/Panels/Ranking/Player/Ranking/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakRankingPlayerRankingText;
    [AutoBind("./Detail/Panels/Ranking/Player/Ranking/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_peakRankingPlayerRankingImage;
    [AutoBind("./Detail/Panels/Ranking/Player/ArenaPoints/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakRankingArenaPointsText;
    [AutoBind("./Detail/Panels/Ranking/Player/HeadIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_peakRankingPlayerIconImage;
    [AutoBind("./Detail/Panels/Ranking/Player/HeadIcon/HeadFrameDummy", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_peakRankingPlayerHeadFrameTransform;
    [AutoBind("./Detail/Panels/Ranking/Player/HeadIcon/LevelText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakRankingPlayerLevelText;
    [AutoBind("./Detail/Panels/Match", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_knockoutGameObject;
    [AutoBind("./Detail/Margin/Player/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_peakPlayerIconImage;
    [AutoBind("./Detail/Margin/Player/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakPlayerNameText;
    [AutoBind("./Detail/Margin/Player/ArenaLevel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_peakDanStateCtrl;
    [AutoBind("./Detail/Margin/Player/ArenaLevel/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakDanText;
    [AutoBind("./Detail/Margin/Player/ArenaLevel/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_peakDanIconImage;
    [AutoBind("./Detail/Margin/Player/LvGroup/LvNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakPlayerLevelText;
    [AutoBind("./Detail/Margin/Player/ArenaPoints/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakArenaPointsText;
    [AutoBind("./Detail/Margin/Player/ArenaPointsUp/Text1", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakArenaPointsUpText1;
    [AutoBind("./Detail/Margin/Player/ArenaPointsUp/Text2", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakArenaPointsUpText2;
    [AutoBind("./Detail/Margin/Player/ArenaPointsUp/ProgressBarImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_peakArenaPointsUpBarImage;
    [AutoBind("./Detail/Margin/Player/Player", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_peakArenaKnockOutPlayerTip;
    [AutoBind("./Detail/Margin/Player/ArenaPointsUp", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_peakArenaArenaPointsUpStateCtrl;
    [AutoBind("./Detail/Margin/Player/ArenaGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_peakArenaMatchGroupTextStateCtrl;
    [AutoBind("./Detail/Margin/Player/ArenaGroup/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakArenaKnockOutGroupText;
    [AutoBind("./Detail/Margin/Player/RewardButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_peakArenaRankingRewardButton;
    [AutoBind("./Detail/Margin/Player/RewardButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_peakArenaRankingRewardButtonStateCtrl;
    [AutoBind("./ArenaRankingReward", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_peakArenaRankingRewardUIStateCtrl;
    [AutoBind("./ArenaRankingReward/BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_peakArenaRankingRewardBGButton;
    [AutoBind("./ArenaRankingReward/Detail/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_peakArenaRankingRewardCloseButton;
    [AutoBind("./ArenaRankingReward/Detail/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_peakArenaRankingRewardScrollRect;
    [AutoBind("./ArenaRankingReward/Detail/ScrollView/Viewport/Content/PointRaceGroup/PointRaceTitle/ExplainText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakArenaRankingRewardPointRaceExplainText;
    [AutoBind("./ArenaRankingReward/Detail/ScrollView/Viewport/Content/PointRaceContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_peakArenaRankingRewardPointRaceContent;
    [AutoBind("./ArenaRankingReward/Detail/ScrollView/Viewport/Content/KnockoutGroupContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_peakArenaRankingRewardKnockoutGroupContent;
    [AutoBind("./Prefabs/ArenaSeasonRewardListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_peakRankingRewardListItemPrefab;
    [AutoBind("./PromoteBattleTips", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_promoteBattleUIStateController;
    [AutoBind("./PromoteBattleTips/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_promoteBattleBGButton;
    [AutoBind("./PromoteBattleTips/Panel/TipsText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_promoteBattleText;
    [AutoBind("./PromoteBattleTips/Panel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_promoteBattleConfirmButton;
    [AutoBind("./PromoteSucceedTips", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_promoteSucceedUIStateController;
    [AutoBind("./PromoteSucceedTips/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_promoteSucceedBGButton;
    [AutoBind("./PromoteSucceedTips/Panel/ArenaLevelImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_promoteSucceedDanImage;
    [AutoBind("./PromoteSucceedTips/Panel/Info/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_promoteSucceedDanText;
    [AutoBind("./PromoteSucceedTips/Panel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_promoteSucceedConfirmButton;
    [AutoBind("./PromoteFailedTips", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_promoteFailedUIStateController;
    [AutoBind("./PromoteFailedTips/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_promoteFailedBGButton;
    [AutoBind("./PromoteFailedTips/Panel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_promoteFailedConfirmButton;
    [AutoBind("./PromoteFailedTips/Panel/NowArenaLevelImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_promoteFailedNowDanImage;
    [AutoBind("./PromoteFailedTips/Panel/BeforeArenaLevelImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_promoteFailedOldDanImage;
    [AutoBind("./PromoteFailedTips/Panel/BeforeArenaLevelImage/Grey", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_promoteFailedOldDanGreyImage;
    [AutoBind("./MatchingFailedTips", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_matchingFailedUIStateController;
    [AutoBind("./MatchingFailedTips/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_matchingFailedBGButton;
    [AutoBind("./MatchingFailedTips/Panel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_matchingFailedConfirmButton;
    [AutoBind("./MatchingNow", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_matchingNowUIStateController;
    [AutoBind("./MatchingNow/Panel/Detail/TimeText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_matchingNowTimeText;
    [AutoBind("./MatchingNow/Panel/Detail/PredictTime", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_matchingNowPredictTimeGameObject;
    [AutoBind("./MatchingNow/Panel/Detail/PredictTime/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_matchingNowPredictTimeText;
    [AutoBind("./MatchingNow/Panel/Detail/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_matchingNowCancelButton;
    [AutoBind("./MatchingPlayerInfo", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_matchingPlayerInfoStateController;
    [AutoBind("./MatchingPlayerInfo/Panel/Detail/OpponentInfo/Player/PlayerIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_matchingPlayerInfoOpponentHeadIcon;
    [AutoBind("./MatchingPlayerInfo/Panel/Detail/OpponentInfo/Player/HeadFrameDummy", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_matchingPlayerInfoOpponentHeadFrameDummy;
    [AutoBind("./MatchingPlayerInfo/Panel/Detail/OpponentInfo/Player/LevelText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_matchingPlayerInfoOpponentLevelText;
    [AutoBind("./MatchingPlayerInfo/Panel/Detail/OpponentInfo/PlayerNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_matchingPlayerInfoOpponentNameText;
    [AutoBind("./MatchingPlayerInfo/Panel/Detail/TimeText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_matchingPlayerInfoTimeText;
    [AutoBind("./GradingMatchOver", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_gradingMatchOverStateCtrl;
    [AutoBind("./GradingMatchOver/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_gradingMatchOverBGButton;
    [AutoBind("./GradingMatchOver/Panel/Button", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_gradingMatchOverConfirmButton;
    [AutoBind("./GradingMatchOver/Panel/LevelImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_gradingMatchOverDanImage;
    [AutoBind("./GradingMatchOver/Panel/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_gradingMatchOverDanText;
    [AutoBind("./Detail/Panels/Clash/FirstWinButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_peakFirstWinButton;
    [AutoBind("./Detail/Panels/Clash/FirstWinButton/Chest", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_peakFirstWinChestStateCtrl;
    [AutoBind("./RewardPreview", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_rewardPreviewUIStateController;
    [AutoBind("./RewardPreview/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_rewardPreviewBackgroundButton;
    [AutoBind("./RewardPreview/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_rewardPreviewScrollRect;
    [AutoBind("./ShareBattleReportPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_shareBattleReportPanelUIStateCtrl;
    [AutoBind("./ShareBattleReportPanel/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_shareBattleReportPanelBackgroundButton;
    [AutoBind("./ShareBattleReportPanel/Detail/ButtonGroup/NameCardButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_shareBattleReportPanelNameCardButtonn;
    [AutoBind("./ShareBattleReportPanel/Detail/ButtonGroup/NameCardButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_shareBattleReportPanelNameCardButtonState;
    [AutoBind("./ShareBattleReportPanel/Detail/ButtonGroup/FriendButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_shareBattleReportPanelFriendButton;
    [AutoBind("./ShareBattleReportPanel/Detail/ButtonGroup/TopAreanButton", AutoBindAttribute.InitState.Inactive, false)]
    private Button m_shareBattleReportPanelTopAreanButton;
    [AutoBind("./ShareBattleReportPanel/Detail/ButtonGroup/WorldChannelButon", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_shareBattleReportPanelWorldChannelButon;
    [AutoBind("./ShareBattleReportPanel/Detail/ButtonGroup/GuildButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_shareBattleReportPanelGuildButton;
    private bool m_isMatchingNow;
    private PeakArenaPanelType m_curPeakArenaPanelType;
    private List<ConfigDataPeakArenaDanInfo> m_peakArenaDans;
    private GameObjectPool<ArenaLevelListItemUIController> m_peakArenaLevelPool;
    private GameObjectPool<PeakArenaRankingListItemUIController> m_peakArenaRankingPool;
    private GameObjectPool<PeakArenaReportListItemUIController> m_peakArenaReportPool;
    private ulong m_curBattleReportInstanceId;
    private PeakArenaKnockoutUIController m_peakArenaKnockoutUIController;
    private GameObject m_opponentFrameCloneGameObject;
    private GameObject m_waitOpponentFrameCloneGameObject;
    [DoNotToLua]
    private PeakArenaUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_SetUIListeners_hotfix;
    private LuaFunction m_SetGameObjectPools_hotfix;
    private LuaFunction m_Open_hotfix;
    private LuaFunction m_UpdateToggleState_hotfix;
    private LuaFunction m_UpdatePeakArenaPeakArenaPanelType_hotfix;
    private LuaFunction m_UpdateCurrencyStatePeakArenaPanelType_hotfix;
    private LuaFunction m_UpdateClashStateSeasonTimeState_hotfix;
    private LuaFunction m_ShareBattleReportPanelClose_hotfix;
    private LuaFunction m_SetKnockoutGroupIdInt32_hotfix;
    private LuaFunction m_SetEditorModeUI_hotfix;
    private LuaFunction m_SetLeftPlayerInfo_hotfix;
    private LuaFunction m_SetRewardButtonUIState_hotfix;
    private LuaFunction m_SetClashState_hotfix;
    private LuaFunction m_SetBattleReportState_hotfix;
    private LuaFunction m_IComparorPeakBattleReportBattleReportHeadBattleReportHead_hotfix;
    private LuaFunction m_SetKnockoutState_hotfix;
    private LuaFunction m_RefreshBattleReportName_hotfix;
    private LuaFunction m_SetArenaLevelState_hotfix;
    private LuaFunction m_SetRankingState_hotfix;
    private LuaFunction m_GetLadderModeStateName_hotfix;
    private LuaFunction m_SetPeakClashLadderRestMode_hotfix;
    private LuaFunction m_SetPeakClashLadderBeginTimeMode_hotfix;
    private LuaFunction m_SetPeakClashLadderMatchMode_hotfix;
    private LuaFunction m_SetPeakClashLadderGradingMatchMode_hotfix;
    private LuaFunction m_SetPeakClashLadderGradingMatchCloseTimeMode_hotfix;
    private LuaFunction m_SetPeakClashLadderOpponentMode_hotfix;
    private LuaFunction m_GetKnockOutMatchReadyLastTime_hotfix;
    private LuaFunction m_ShowKnockOutMatchingPlayerInfoPanel_hotfix;
    private LuaFunction m_SetKnockOutMatchingPlayerInfoPanel_hotfix;
    private LuaFunction m_UpdateKnockOutMatchingPlayerInfoPanelTimeText_hotfix;
    private LuaFunction m_CloseKnockOutMatchingPlayerInfoPanel_hotfix;
    private LuaFunction m_SetPeakClashLadderWaitOpponentMode_hotfix;
    private LuaFunction m_SetPeakClashLadderOpponentAbstentionMode_hotfix;
    private LuaFunction m_GetKnockOutMatchTitleTipText_hotfix;
    private LuaFunction m_GetSeasonBeginTime_hotfix;
    private LuaFunction m_GetSeasonBeginTimeTitleText_hotfix;
    private LuaFunction m_GetSeasonPointMatchEndTime_hotfix;
    private LuaFunction m_GetPeakArenaBattleSeason_hotfix;
    private LuaFunction m_ShowArenaTopPromotionTipDialog_hotfix;
    private LuaFunction m_OnReturnButtonClick_hotfix;
    private LuaFunction m_OnHelpButtonClick_hotfix;
    private LuaFunction m_OnManagementTeamClick_hotfix;
    private LuaFunction m_OnPeakClashCasualChallengeButtonClick_hotfix;
    private LuaFunction m_OnPeakClashLadderChallengeButtonClick_hotfix;
    private LuaFunction m_OnReportShareButtonClickUInt64_hotfix;
    private LuaFunction m_OnReportReplyButtonClickUInt64_hotfix;
    private LuaFunction m_OnBattleReportNameChangeButtonClickUInt64_hotfix;
    private LuaFunction m_OnShareBattleReportPanelGuildButtonClick_hotfix;
    private LuaFunction m_OnShareBattleReportPanelWorldChannelButtonClick_hotfix;
    private LuaFunction m_OnShareBattleReportPanelTopAreanButtonClick_hotfix;
    private LuaFunction m_OnShareBattleReportPanelFriendButtonClick_hotfix;
    private LuaFunction m_OnShareBattleReportPanelNameCardButtonClick_hotfix;
    private LuaFunction m_OnShareBattleReportPanelBackgroundButtonClick_hotfix;
    private LuaFunction m_OnPeakClashToggleBoolean_hotfix;
    private LuaFunction m_OnPeakBattleReportToggleBoolean_hotfix;
    private LuaFunction m_OnPeakDanToggleBoolean_hotfix;
    private LuaFunction m_OnPeakLocalRankingToggleBoolean_hotfix;
    private LuaFunction m_OnPeakKnockoutToggleBoolean_hotfix;
    private LuaFunction m_OnToggleLockClick_hotfix;
    private LuaFunction m_OnPeakArenaRankingRewardButtonClick_hotfix;
    private LuaFunction m_ShowPromoteBattleInt32_hotfix;
    private LuaFunction m_ShowPromoteSucceedInt32_hotfix;
    private LuaFunction m_ShowMatchingFailed_hotfix;
    private LuaFunction m_ShowMatchingNow_hotfix;
    private LuaFunction m_HideMatchingNow_hotfix;
    private LuaFunction m_SetMatchingNowTimeTimeSpan_hotfix;
    private LuaFunction m_SetMatchingPredictTimeTimeSpan_hotfix;
    private LuaFunction m_IsMatchingNowPredictTimeActive_hotfix;
    private LuaFunction m_OnMatchingNowCancelButtonClick_hotfix;
    private LuaFunction m_SetLadderWeekWinRewardStatus_hotfix;
    private LuaFunction m_OnPeakFirstWinButtonClick_hotfix;
    private LuaFunction m_ShowGradingMatchOverPanelInt32_hotfix;
    private LuaFunction m_ShowPromoteFailedInt32Int32_hotfix;
    private LuaFunction m_OnPromoteFailedBGButtonClick_hotfix;
    private LuaFunction m_OnGradingMatchOverConfirmButtonClick_hotfix;
    private LuaFunction m_OnRewardPreviewBackgroundButtonClick_hotfix;
    private LuaFunction m_OnPromoteBattleConfirmButtonClick_hotfix;
    private LuaFunction m_OnPromoteSucceedConfirmButtonClick_hotfix;
    private LuaFunction m_OnMatchingFailedConfirmButtonClick_hotfix;
    private LuaFunction m_OnPeakArenaRankingRewardCloseButtonClick_hotfix;
    private LuaFunction m_OnAddJettonTicketClick_hotfix;
    private LuaFunction m_add_EventOnReturnAction_hotfix;
    private LuaFunction m_remove_EventOnReturnAction_hotfix;
    private LuaFunction m_add_EventOnShowHelpAction_hotfix;
    private LuaFunction m_remove_EventOnShowHelpAction_hotfix;
    private LuaFunction m_add_EventOnManagementTeamAction_hotfix;
    private LuaFunction m_remove_EventOnManagementTeamAction_hotfix;
    private LuaFunction m_add_EventOnMatchingCancelAction_hotfix;
    private LuaFunction m_remove_EventOnMatchingCancelAction_hotfix;
    private LuaFunction m_add_EventOnAddJettonTicketAction_hotfix;
    private LuaFunction m_remove_EventOnAddJettonTicketAction_hotfix;
    private LuaFunction m_add_EventOnReportReplyAction`1_hotfix;
    private LuaFunction m_remove_EventOnReportReplyAction`1_hotfix;
    private LuaFunction m_add_EventOnReportNameChangeAction`1_hotfix;
    private LuaFunction m_remove_EventOnReportNameChangeAction`1_hotfix;
    private LuaFunction m_add_EventOnPeakClashCasualChallengeButtonClickAction_hotfix;
    private LuaFunction m_remove_EventOnPeakClashCasualChallengeButtonClickAction_hotfix;
    private LuaFunction m_add_EventOnPeakClashLadderChallengeButtonClickAction_hotfix;
    private LuaFunction m_remove_EventOnPeakClashLadderChallengeButtonClickAction_hotfix;
    private LuaFunction m_add_EventOnShowPeakPanelAction`1_hotfix;
    private LuaFunction m_remove_EventOnShowPeakPanelAction`1_hotfix;
    private LuaFunction m_add_EventOnShareBattleReportAction`2_hotfix;
    private LuaFunction m_remove_EventOnShareBattleReportAction`2_hotfix;

    private PeakArenaUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetUIListeners()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetGameObjectPools()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateToggleState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdatePeakArena(PeakArenaPanelType type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateCurrencyState(PeakArenaPanelType type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateClashStateSeasonTimeState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShareBattleReportPanelClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetKnockoutGroupId(int groupId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetEditorModeUI()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetLeftPlayerInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetRewardButtonUIState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetClashState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetBattleReportState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int IComparorPeakBattleReport(BattleReportHead r1, BattleReportHead r2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetKnockoutState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RefreshBattleReportName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetArenaLevelState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetRankingState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string GetLadderModeStateName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPeakClashLadderRestMode()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPeakClashLadderBeginTimeMode()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPeakClashLadderMatchMode()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPeakClashLadderGradingMatchMode()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPeakClashLadderGradingMatchCloseTimeMode()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPeakClashLadderOpponentMode()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private TimeSpan GetKnockOutMatchReadyLastTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowKnockOutMatchingPlayerInfoPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetKnockOutMatchingPlayerInfoPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateKnockOutMatchingPlayerInfoPanelTimeText()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CloseKnockOutMatchingPlayerInfoPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPeakClashLadderWaitOpponentMode()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPeakClashLadderOpponentAbstentionMode()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string GetKnockOutMatchTitleTipText()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string GetSeasonBeginTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string GetSeasonBeginTimeTitleText()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string GetSeasonPointMatchEndTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string GetPeakArenaBattleSeason()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowArenaTopPromotionTipDialog()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static T GetField<T>(object instance, string fieldname)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHelpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnManagementTeamClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPeakClashCasualChallengeButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPeakClashLadderChallengeButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReportShareButtonClick(ulong reportInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReportReplyButtonClick(ulong reportInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBattleReportNameChangeButtonClick(ulong reportInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnShareBattleReportPanelGuildButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnShareBattleReportPanelWorldChannelButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnShareBattleReportPanelTopAreanButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnShareBattleReportPanelFriendButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnShareBattleReportPanelNameCardButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnShareBattleReportPanelBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPeakClashToggle(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPeakBattleReportToggle(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPeakDanToggle(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPeakLocalRankingToggle(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPeakKnockoutToggle(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnToggleLockClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPeakArenaRankingRewardButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowPromoteBattle(int danId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowPromoteSucceed(int danId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowMatchingFailed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowMatchingNow()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideMatchingNow()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetMatchingNowTime(TimeSpan ts)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetMatchingPredictTime(TimeSpan ts)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsMatchingNowPredictTimeActive()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMatchingNowCancelButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLadderWeekWinRewardStatus()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPeakFirstWinButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowGradingMatchOverPanel(int danId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowPromoteFailed(int oldDanId, int nowDanId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPromoteFailedBGButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGradingMatchOverConfirmButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRewardPreviewBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPromoteBattleConfirmButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPromoteSucceedConfirmButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMatchingFailedConfirmButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPeakArenaRankingRewardCloseButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddJettonTicketClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowHelp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnManagementTeam
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnMatchingCancel
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnAddJettonTicket
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ulong> EventOnReportReply
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ulong> EventOnReportNameChange
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnPeakClashCasualChallengeButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnPeakClashLadderChallengeButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<PeakArenaPanelType> EventOnShowPeakPanel
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<PeakArenaBattleReportType, ulong> EventOnShareBattleReport
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public PeakArenaUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnReturn()
    {
      this.EventOnReturn = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowHelp()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowHelp()
    {
      this.EventOnShowHelp = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnManagementTeam()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnManagementTeam()
    {
      this.EventOnManagementTeam = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnMatchingCancel()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnMatchingCancel()
    {
      this.EventOnMatchingCancel = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnAddJettonTicket()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnAddJettonTicket()
    {
      this.EventOnAddJettonTicket = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnReportReply(ulong obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnReportReply(ulong obj)
    {
      this.EventOnReportReply = (Action<ulong>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnReportNameChange(ulong obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnReportNameChange(ulong obj)
    {
      this.EventOnReportNameChange = (Action<ulong>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnPeakClashCasualChallengeButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnPeakClashCasualChallengeButtonClick()
    {
      this.EventOnPeakClashCasualChallengeButtonClick = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnPeakClashLadderChallengeButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnPeakClashLadderChallengeButtonClick()
    {
      this.EventOnPeakClashLadderChallengeButtonClick = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowPeakPanel(PeakArenaPanelType obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowPeakPanel(PeakArenaPanelType obj)
    {
      this.EventOnShowPeakPanel = (Action<PeakArenaPanelType>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShareBattleReport(PeakArenaBattleReportType arg1, ulong arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShareBattleReport(PeakArenaBattleReportType arg1, ulong arg2)
    {
      this.EventOnShareBattleReport = (Action<PeakArenaBattleReportType, ulong>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private PeakArenaUIController m_owner;

      public LuaExportHelper(PeakArenaUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnReturn()
      {
        this.m_owner.__callDele_EventOnReturn();
      }

      public void __clearDele_EventOnReturn()
      {
        this.m_owner.__clearDele_EventOnReturn();
      }

      public void __callDele_EventOnShowHelp()
      {
        this.m_owner.__callDele_EventOnShowHelp();
      }

      public void __clearDele_EventOnShowHelp()
      {
        this.m_owner.__clearDele_EventOnShowHelp();
      }

      public void __callDele_EventOnManagementTeam()
      {
        this.m_owner.__callDele_EventOnManagementTeam();
      }

      public void __clearDele_EventOnManagementTeam()
      {
        this.m_owner.__clearDele_EventOnManagementTeam();
      }

      public void __callDele_EventOnMatchingCancel()
      {
        this.m_owner.__callDele_EventOnMatchingCancel();
      }

      public void __clearDele_EventOnMatchingCancel()
      {
        this.m_owner.__clearDele_EventOnMatchingCancel();
      }

      public void __callDele_EventOnAddJettonTicket()
      {
        this.m_owner.__callDele_EventOnAddJettonTicket();
      }

      public void __clearDele_EventOnAddJettonTicket()
      {
        this.m_owner.__clearDele_EventOnAddJettonTicket();
      }

      public void __callDele_EventOnReportReply(ulong obj)
      {
        this.m_owner.__callDele_EventOnReportReply(obj);
      }

      public void __clearDele_EventOnReportReply(ulong obj)
      {
        this.m_owner.__clearDele_EventOnReportReply(obj);
      }

      public void __callDele_EventOnReportNameChange(ulong obj)
      {
        this.m_owner.__callDele_EventOnReportNameChange(obj);
      }

      public void __clearDele_EventOnReportNameChange(ulong obj)
      {
        this.m_owner.__clearDele_EventOnReportNameChange(obj);
      }

      public void __callDele_EventOnPeakClashCasualChallengeButtonClick()
      {
        this.m_owner.__callDele_EventOnPeakClashCasualChallengeButtonClick();
      }

      public void __clearDele_EventOnPeakClashCasualChallengeButtonClick()
      {
        this.m_owner.__clearDele_EventOnPeakClashCasualChallengeButtonClick();
      }

      public void __callDele_EventOnPeakClashLadderChallengeButtonClick()
      {
        this.m_owner.__callDele_EventOnPeakClashLadderChallengeButtonClick();
      }

      public void __clearDele_EventOnPeakClashLadderChallengeButtonClick()
      {
        this.m_owner.__clearDele_EventOnPeakClashLadderChallengeButtonClick();
      }

      public void __callDele_EventOnShowPeakPanel(PeakArenaPanelType obj)
      {
        this.m_owner.__callDele_EventOnShowPeakPanel(obj);
      }

      public void __clearDele_EventOnShowPeakPanel(PeakArenaPanelType obj)
      {
        this.m_owner.__clearDele_EventOnShowPeakPanel(obj);
      }

      public void __callDele_EventOnShareBattleReport(PeakArenaBattleReportType arg1, ulong arg2)
      {
        this.m_owner.__callDele_EventOnShareBattleReport(arg1, arg2);
      }

      public void __clearDele_EventOnShareBattleReport(PeakArenaBattleReportType arg1, ulong arg2)
      {
        this.m_owner.__clearDele_EventOnShareBattleReport(arg1, arg2);
      }

      public CommonUIStateController m_uiStateController
      {
        get
        {
          return this.m_owner.m_uiStateController;
        }
        set
        {
          this.m_owner.m_uiStateController = value;
        }
      }

      public Button m_returnButton
      {
        get
        {
          return this.m_owner.m_returnButton;
        }
        set
        {
          this.m_owner.m_returnButton = value;
        }
      }

      public Button m_helpButton
      {
        get
        {
          return this.m_owner.m_helpButton;
        }
        set
        {
          this.m_owner.m_helpButton = value;
        }
      }

      public GameObject m_prefabsGameObject
      {
        get
        {
          return this.m_owner.m_prefabsGameObject;
        }
        set
        {
          this.m_owner.m_prefabsGameObject = value;
        }
      }

      public Text m_scoreText
      {
        get
        {
          return this.m_owner.m_scoreText;
        }
        set
        {
          this.m_owner.m_scoreText = value;
        }
      }

      public Text m_quitCountText
      {
        get
        {
          return this.m_owner.m_quitCountText;
        }
        set
        {
          this.m_owner.m_quitCountText = value;
        }
      }

      public GameObject m_goldGameObject
      {
        get
        {
          return this.m_owner.m_goldGameObject;
        }
        set
        {
          this.m_owner.m_goldGameObject = value;
        }
      }

      public GameObject m_arenaCoinGameObject
      {
        get
        {
          return this.m_owner.m_arenaCoinGameObject;
        }
        set
        {
          this.m_owner.m_arenaCoinGameObject = value;
        }
      }

      public Text m_arenaCoinText
      {
        get
        {
          return this.m_owner.m_arenaCoinText;
        }
        set
        {
          this.m_owner.m_arenaCoinText = value;
        }
      }

      public GameObject m_jettonGameObject
      {
        get
        {
          return this.m_owner.m_jettonGameObject;
        }
        set
        {
          this.m_owner.m_jettonGameObject = value;
        }
      }

      public Text m_jettonText
      {
        get
        {
          return this.m_owner.m_jettonText;
        }
        set
        {
          this.m_owner.m_jettonText = value;
        }
      }

      public Button m_jettonButton
      {
        get
        {
          return this.m_owner.m_jettonButton;
        }
        set
        {
          this.m_owner.m_jettonButton = value;
        }
      }

      public Button m_managementTeamButton
      {
        get
        {
          return this.m_owner.m_managementTeamButton;
        }
        set
        {
          this.m_owner.m_managementTeamButton = value;
        }
      }

      public CommonUIStateController m_managementTeamButtonAnimation
      {
        get
        {
          return this.m_owner.m_managementTeamButtonAnimation;
        }
        set
        {
          this.m_owner.m_managementTeamButtonAnimation = value;
        }
      }

      public RectTransform m_peakMarginTransform
      {
        get
        {
          return this.m_owner.m_peakMarginTransform;
        }
        set
        {
          this.m_owner.m_peakMarginTransform = value;
        }
      }

      public Toggle m_peakClashToggle
      {
        get
        {
          return this.m_owner.m_peakClashToggle;
        }
        set
        {
          this.m_owner.m_peakClashToggle = value;
        }
      }

      public Toggle m_peakBattleReportToggle
      {
        get
        {
          return this.m_owner.m_peakBattleReportToggle;
        }
        set
        {
          this.m_owner.m_peakBattleReportToggle = value;
        }
      }

      public Toggle m_peakDanToggle
      {
        get
        {
          return this.m_owner.m_peakDanToggle;
        }
        set
        {
          this.m_owner.m_peakDanToggle = value;
        }
      }

      public Toggle m_peakLocalRankingToggle
      {
        get
        {
          return this.m_owner.m_peakLocalRankingToggle;
        }
        set
        {
          this.m_owner.m_peakLocalRankingToggle = value;
        }
      }

      public Toggle m_peakKnockoutToggle
      {
        get
        {
          return this.m_owner.m_peakKnockoutToggle;
        }
        set
        {
          this.m_owner.m_peakKnockoutToggle = value;
        }
      }

      public CommonUIStateController m_peakClashToggleState
      {
        get
        {
          return this.m_owner.m_peakClashToggleState;
        }
        set
        {
          this.m_owner.m_peakClashToggleState = value;
        }
      }

      public CommonUIStateController m_peakBattleReportToggleState
      {
        get
        {
          return this.m_owner.m_peakBattleReportToggleState;
        }
        set
        {
          this.m_owner.m_peakBattleReportToggleState = value;
        }
      }

      public CommonUIStateController m_peakDanToggleState
      {
        get
        {
          return this.m_owner.m_peakDanToggleState;
        }
        set
        {
          this.m_owner.m_peakDanToggleState = value;
        }
      }

      public CommonUIStateController m_peakLocalRankingToggleState
      {
        get
        {
          return this.m_owner.m_peakLocalRankingToggleState;
        }
        set
        {
          this.m_owner.m_peakLocalRankingToggleState = value;
        }
      }

      public CommonUIStateController m_peakKnockoutToggleState
      {
        get
        {
          return this.m_owner.m_peakKnockoutToggleState;
        }
        set
        {
          this.m_owner.m_peakKnockoutToggleState = value;
        }
      }

      public Button m_peakClashLockButton
      {
        get
        {
          return this.m_owner.m_peakClashLockButton;
        }
        set
        {
          this.m_owner.m_peakClashLockButton = value;
        }
      }

      public Button m_peakBattleReportLockButton
      {
        get
        {
          return this.m_owner.m_peakBattleReportLockButton;
        }
        set
        {
          this.m_owner.m_peakBattleReportLockButton = value;
        }
      }

      public Button m_peakDanLockButton
      {
        get
        {
          return this.m_owner.m_peakDanLockButton;
        }
        set
        {
          this.m_owner.m_peakDanLockButton = value;
        }
      }

      public Button m_peakLocalRankingLockButton
      {
        get
        {
          return this.m_owner.m_peakLocalRankingLockButton;
        }
        set
        {
          this.m_owner.m_peakLocalRankingLockButton = value;
        }
      }

      public Button m_peakKnockoutLockButton
      {
        get
        {
          return this.m_owner.m_peakKnockoutLockButton;
        }
        set
        {
          this.m_owner.m_peakKnockoutLockButton = value;
        }
      }

      public GameObject m_peakLocalRankingClickGameObject
      {
        get
        {
          return this.m_owner.m_peakLocalRankingClickGameObject;
        }
        set
        {
          this.m_owner.m_peakLocalRankingClickGameObject = value;
        }
      }

      public GameObject m_peakKnockoutClickGameObject
      {
        get
        {
          return this.m_owner.m_peakKnockoutClickGameObject;
        }
        set
        {
          this.m_owner.m_peakKnockoutClickGameObject = value;
        }
      }

      public Button m_peakClashLadderChallengeButton
      {
        get
        {
          return this.m_owner.m_peakClashLadderChallengeButton;
        }
        set
        {
          this.m_owner.m_peakClashLadderChallengeButton = value;
        }
      }

      public CommonUIStateController m_peakClashLadderChallengeButtonUIStateController
      {
        get
        {
          return this.m_owner.m_peakClashLadderChallengeButtonUIStateController;
        }
        set
        {
          this.m_owner.m_peakClashLadderChallengeButtonUIStateController = value;
        }
      }

      public Button m_peakClashCasualChallengeButton
      {
        get
        {
          return this.m_owner.m_peakClashCasualChallengeButton;
        }
        set
        {
          this.m_owner.m_peakClashCasualChallengeButton = value;
        }
      }

      public Text m_peakClashLadderModeAreaNameText
      {
        get
        {
          return this.m_owner.m_peakClashLadderModeAreaNameText;
        }
        set
        {
          this.m_owner.m_peakClashLadderModeAreaNameText = value;
        }
      }

      public CommonUIStateController m_peakClashLadderModeUIStateController
      {
        get
        {
          return this.m_owner.m_peakClashLadderModeUIStateController;
        }
        set
        {
          this.m_owner.m_peakClashLadderModeUIStateController = value;
        }
      }

      public Text m_peakClashLadderRestPanelRestModeBeginTimeTitleText
      {
        get
        {
          return this.m_owner.m_peakClashLadderRestPanelRestModeBeginTimeTitleText;
        }
        set
        {
          this.m_owner.m_peakClashLadderRestPanelRestModeBeginTimeTitleText = value;
        }
      }

      public Text m_peakClashLadderRestPanelRestModeBeginTimeText
      {
        get
        {
          return this.m_owner.m_peakClashLadderRestPanelRestModeBeginTimeText;
        }
        set
        {
          this.m_owner.m_peakClashLadderRestPanelRestModeBeginTimeText = value;
        }
      }

      public Text m_peakClashLadderBeginTimeModeTopTitleText
      {
        get
        {
          return this.m_owner.m_peakClashLadderBeginTimeModeTopTitleText;
        }
        set
        {
          this.m_owner.m_peakClashLadderBeginTimeModeTopTitleText = value;
        }
      }

      public Text m_peakClashLadderBeginTimeModeOpenTimeText
      {
        get
        {
          return this.m_owner.m_peakClashLadderBeginTimeModeOpenTimeText;
        }
        set
        {
          this.m_owner.m_peakClashLadderBeginTimeModeOpenTimeText = value;
        }
      }

      public Text m_peakClashLadderMatchModeTimeText
      {
        get
        {
          return this.m_owner.m_peakClashLadderMatchModeTimeText;
        }
        set
        {
          this.m_owner.m_peakClashLadderMatchModeTimeText = value;
        }
      }

      public Text m_peakClashLadderMatchModeBattleTimesText
      {
        get
        {
          return this.m_owner.m_peakClashLadderMatchModeBattleTimesText;
        }
        set
        {
          this.m_owner.m_peakClashLadderMatchModeBattleTimesText = value;
        }
      }

      public Text m_peakClashLadderMatchModeBattleWinText
      {
        get
        {
          return this.m_owner.m_peakClashLadderMatchModeBattleWinText;
        }
        set
        {
          this.m_owner.m_peakClashLadderMatchModeBattleWinText = value;
        }
      }

      public Text m_peakClashLadderMatchModeBattleFailedText
      {
        get
        {
          return this.m_owner.m_peakClashLadderMatchModeBattleFailedText;
        }
        set
        {
          this.m_owner.m_peakClashLadderMatchModeBattleFailedText = value;
        }
      }

      public Text m_peakClashLadderMatchModeWinRateValueText
      {
        get
        {
          return this.m_owner.m_peakClashLadderMatchModeWinRateValueText;
        }
        set
        {
          this.m_owner.m_peakClashLadderMatchModeWinRateValueText = value;
        }
      }

      public Image m_peakClashLadderMatchModeLevelImage
      {
        get
        {
          return this.m_owner.m_peakClashLadderMatchModeLevelImage;
        }
        set
        {
          this.m_owner.m_peakClashLadderMatchModeLevelImage = value;
        }
      }

      public Text m_peakClashLadderGradingMatchModeTimeText
      {
        get
        {
          return this.m_owner.m_peakClashLadderGradingMatchModeTimeText;
        }
        set
        {
          this.m_owner.m_peakClashLadderGradingMatchModeTimeText = value;
        }
      }

      public Text m_peakClashLadderGradingMatchModeBattleTGradingMatchText
      {
        get
        {
          return this.m_owner.m_peakClashLadderGradingMatchModeBattleTGradingMatchText;
        }
        set
        {
          this.m_owner.m_peakClashLadderGradingMatchModeBattleTGradingMatchText = value;
        }
      }

      public Text m_peakClashLadderGradingMatchModeBattleWinText
      {
        get
        {
          return this.m_owner.m_peakClashLadderGradingMatchModeBattleWinText;
        }
        set
        {
          this.m_owner.m_peakClashLadderGradingMatchModeBattleWinText = value;
        }
      }

      public Text m_peakClashLadderGradingMatchModeBattleFailedText
      {
        get
        {
          return this.m_owner.m_peakClashLadderGradingMatchModeBattleFailedText;
        }
        set
        {
          this.m_owner.m_peakClashLadderGradingMatchModeBattleFailedText = value;
        }
      }

      public GameObject m_peakClashLadderGradingMatchModeCompetitionRoundGameObject
      {
        get
        {
          return this.m_owner.m_peakClashLadderGradingMatchModeCompetitionRoundGameObject;
        }
        set
        {
          this.m_owner.m_peakClashLadderGradingMatchModeCompetitionRoundGameObject = value;
        }
      }

      public Text m_peakClashLadderOpponentModeTimeText
      {
        get
        {
          return this.m_owner.m_peakClashLadderOpponentModeTimeText;
        }
        set
        {
          this.m_owner.m_peakClashLadderOpponentModeTimeText = value;
        }
      }

      public Image m_peakClashLadderOpponentModeOpponentHeadIcon
      {
        get
        {
          return this.m_owner.m_peakClashLadderOpponentModeOpponentHeadIcon;
        }
        set
        {
          this.m_owner.m_peakClashLadderOpponentModeOpponentHeadIcon = value;
        }
      }

      public Transform m_peakClashLadderOpponentModeOpponentHeadFrameDummy
      {
        get
        {
          return this.m_owner.m_peakClashLadderOpponentModeOpponentHeadFrameDummy;
        }
        set
        {
          this.m_owner.m_peakClashLadderOpponentModeOpponentHeadFrameDummy = value;
        }
      }

      public Text m_peakClashLadderOpponentModeOpponentLevelText
      {
        get
        {
          return this.m_owner.m_peakClashLadderOpponentModeOpponentLevelText;
        }
        set
        {
          this.m_owner.m_peakClashLadderOpponentModeOpponentLevelText = value;
        }
      }

      public Text m_peakClashLadderOpponentModeOpponentNameText
      {
        get
        {
          return this.m_owner.m_peakClashLadderOpponentModeOpponentNameText;
        }
        set
        {
          this.m_owner.m_peakClashLadderOpponentModeOpponentNameText = value;
        }
      }

      public Toggle m_peakClashLadderOpponentModeOpponentInfoToggle
      {
        get
        {
          return this.m_owner.m_peakClashLadderOpponentModeOpponentInfoToggle;
        }
        set
        {
          this.m_owner.m_peakClashLadderOpponentModeOpponentInfoToggle = value;
        }
      }

      public GameObject m_peakClashLadderOpponentModeOpponentInfoTimeLimitGo
      {
        get
        {
          return this.m_owner.m_peakClashLadderOpponentModeOpponentInfoTimeLimitGo;
        }
        set
        {
          this.m_owner.m_peakClashLadderOpponentModeOpponentInfoTimeLimitGo = value;
        }
      }

      public Text m_peakClashLadderOpponentModeOpponentInfoTimeText
      {
        get
        {
          return this.m_owner.m_peakClashLadderOpponentModeOpponentInfoTimeText;
        }
        set
        {
          this.m_owner.m_peakClashLadderOpponentModeOpponentInfoTimeText = value;
        }
      }

      public Text m_peakClashLadderGradingMatchCloseTimeModeEndTimeText
      {
        get
        {
          return this.m_owner.m_peakClashLadderGradingMatchCloseTimeModeEndTimeText;
        }
        set
        {
          this.m_owner.m_peakClashLadderGradingMatchCloseTimeModeEndTimeText = value;
        }
      }

      public Text m_peakClashLadderGradingMatchCloseTimeModeTimeText
      {
        get
        {
          return this.m_owner.m_peakClashLadderGradingMatchCloseTimeModeTimeText;
        }
        set
        {
          this.m_owner.m_peakClashLadderGradingMatchCloseTimeModeTimeText = value;
        }
      }

      public Text m_peakClashLadderGradingMatchCloseTimeModeMatchRecordTimesText
      {
        get
        {
          return this.m_owner.m_peakClashLadderGradingMatchCloseTimeModeMatchRecordTimesText;
        }
        set
        {
          this.m_owner.m_peakClashLadderGradingMatchCloseTimeModeMatchRecordTimesText = value;
        }
      }

      public Text m_peakClashLadderGradingMatchCloseTimeModeMatchRecordWinText
      {
        get
        {
          return this.m_owner.m_peakClashLadderGradingMatchCloseTimeModeMatchRecordWinText;
        }
        set
        {
          this.m_owner.m_peakClashLadderGradingMatchCloseTimeModeMatchRecordWinText = value;
        }
      }

      public Text m_peakClashLadderGradingMatchCloseTimeModeMatchRecordFailedText
      {
        get
        {
          return this.m_owner.m_peakClashLadderGradingMatchCloseTimeModeMatchRecordFailedText;
        }
        set
        {
          this.m_owner.m_peakClashLadderGradingMatchCloseTimeModeMatchRecordFailedText = value;
        }
      }

      public Text m_peakClashLadderGradingMatchCloseTimeModeMatchRecordWinRateValueText
      {
        get
        {
          return this.m_owner.m_peakClashLadderGradingMatchCloseTimeModeMatchRecordWinRateValueText;
        }
        set
        {
          this.m_owner.m_peakClashLadderGradingMatchCloseTimeModeMatchRecordWinRateValueText = value;
        }
      }

      public Image m_peakClashLadderGradingMatchCloseTimeModeMatchRecordLevelImage
      {
        get
        {
          return this.m_owner.m_peakClashLadderGradingMatchCloseTimeModeMatchRecordLevelImage;
        }
        set
        {
          this.m_owner.m_peakClashLadderGradingMatchCloseTimeModeMatchRecordLevelImage = value;
        }
      }

      public Text m_peakClashLadderWaitOpponentModeTimeText
      {
        get
        {
          return this.m_owner.m_peakClashLadderWaitOpponentModeTimeText;
        }
        set
        {
          this.m_owner.m_peakClashLadderWaitOpponentModeTimeText = value;
        }
      }

      public Text m_peakClashLadderOpponentAbstentionModeTimeText
      {
        get
        {
          return this.m_owner.m_peakClashLadderOpponentAbstentionModeTimeText;
        }
        set
        {
          this.m_owner.m_peakClashLadderOpponentAbstentionModeTimeText = value;
        }
      }

      public Text m_peakClashLadderOpponentAbstentionModeNoticeText
      {
        get
        {
          return this.m_owner.m_peakClashLadderOpponentAbstentionModeNoticeText;
        }
        set
        {
          this.m_owner.m_peakClashLadderOpponentAbstentionModeNoticeText = value;
        }
      }

      public ScrollRect m_peakBattleReportScrollRect
      {
        get
        {
          return this.m_owner.m_peakBattleReportScrollRect;
        }
        set
        {
          this.m_owner.m_peakBattleReportScrollRect = value;
        }
      }

      public GameObject m_peakNoBattleReportGameObject
      {
        get
        {
          return this.m_owner.m_peakNoBattleReportGameObject;
        }
        set
        {
          this.m_owner.m_peakNoBattleReportGameObject = value;
        }
      }

      public ScrollRect m_peakArenaLevelScrollRect
      {
        get
        {
          return this.m_owner.m_peakArenaLevelScrollRect;
        }
        set
        {
          this.m_owner.m_peakArenaLevelScrollRect = value;
        }
      }

      public GameObject m_peakLevelListItemPrefab
      {
        get
        {
          return this.m_owner.m_peakLevelListItemPrefab;
        }
        set
        {
          this.m_owner.m_peakLevelListItemPrefab = value;
        }
      }

      public Text m_peakArenaLevelGetRewardNoticeText
      {
        get
        {
          return this.m_owner.m_peakArenaLevelGetRewardNoticeText;
        }
        set
        {
          this.m_owner.m_peakArenaLevelGetRewardNoticeText = value;
        }
      }

      public ScrollRect m_peakRankingScrollRect
      {
        get
        {
          return this.m_owner.m_peakRankingScrollRect;
        }
        set
        {
          this.m_owner.m_peakRankingScrollRect = value;
        }
      }

      public GameObject m_peakRankingListItemPrefab
      {
        get
        {
          return this.m_owner.m_peakRankingListItemPrefab;
        }
        set
        {
          this.m_owner.m_peakRankingListItemPrefab = value;
        }
      }

      public Text m_peakRankingPlayerNameText
      {
        get
        {
          return this.m_owner.m_peakRankingPlayerNameText;
        }
        set
        {
          this.m_owner.m_peakRankingPlayerNameText = value;
        }
      }

      public CommonUIStateController m_peakRankingPlayerRankingUIStateController
      {
        get
        {
          return this.m_owner.m_peakRankingPlayerRankingUIStateController;
        }
        set
        {
          this.m_owner.m_peakRankingPlayerRankingUIStateController = value;
        }
      }

      public Text m_peakRankingPlayerRankingText
      {
        get
        {
          return this.m_owner.m_peakRankingPlayerRankingText;
        }
        set
        {
          this.m_owner.m_peakRankingPlayerRankingText = value;
        }
      }

      public Image m_peakRankingPlayerRankingImage
      {
        get
        {
          return this.m_owner.m_peakRankingPlayerRankingImage;
        }
        set
        {
          this.m_owner.m_peakRankingPlayerRankingImage = value;
        }
      }

      public Text m_peakRankingArenaPointsText
      {
        get
        {
          return this.m_owner.m_peakRankingArenaPointsText;
        }
        set
        {
          this.m_owner.m_peakRankingArenaPointsText = value;
        }
      }

      public Image m_peakRankingPlayerIconImage
      {
        get
        {
          return this.m_owner.m_peakRankingPlayerIconImage;
        }
        set
        {
          this.m_owner.m_peakRankingPlayerIconImage = value;
        }
      }

      public Transform m_peakRankingPlayerHeadFrameTransform
      {
        get
        {
          return this.m_owner.m_peakRankingPlayerHeadFrameTransform;
        }
        set
        {
          this.m_owner.m_peakRankingPlayerHeadFrameTransform = value;
        }
      }

      public Text m_peakRankingPlayerLevelText
      {
        get
        {
          return this.m_owner.m_peakRankingPlayerLevelText;
        }
        set
        {
          this.m_owner.m_peakRankingPlayerLevelText = value;
        }
      }

      public GameObject m_knockoutGameObject
      {
        get
        {
          return this.m_owner.m_knockoutGameObject;
        }
        set
        {
          this.m_owner.m_knockoutGameObject = value;
        }
      }

      public Image m_peakPlayerIconImage
      {
        get
        {
          return this.m_owner.m_peakPlayerIconImage;
        }
        set
        {
          this.m_owner.m_peakPlayerIconImage = value;
        }
      }

      public Text m_peakPlayerNameText
      {
        get
        {
          return this.m_owner.m_peakPlayerNameText;
        }
        set
        {
          this.m_owner.m_peakPlayerNameText = value;
        }
      }

      public CommonUIStateController m_peakDanStateCtrl
      {
        get
        {
          return this.m_owner.m_peakDanStateCtrl;
        }
        set
        {
          this.m_owner.m_peakDanStateCtrl = value;
        }
      }

      public Text m_peakDanText
      {
        get
        {
          return this.m_owner.m_peakDanText;
        }
        set
        {
          this.m_owner.m_peakDanText = value;
        }
      }

      public Image m_peakDanIconImage
      {
        get
        {
          return this.m_owner.m_peakDanIconImage;
        }
        set
        {
          this.m_owner.m_peakDanIconImage = value;
        }
      }

      public Text m_peakPlayerLevelText
      {
        get
        {
          return this.m_owner.m_peakPlayerLevelText;
        }
        set
        {
          this.m_owner.m_peakPlayerLevelText = value;
        }
      }

      public Text m_peakArenaPointsText
      {
        get
        {
          return this.m_owner.m_peakArenaPointsText;
        }
        set
        {
          this.m_owner.m_peakArenaPointsText = value;
        }
      }

      public Text m_peakArenaPointsUpText1
      {
        get
        {
          return this.m_owner.m_peakArenaPointsUpText1;
        }
        set
        {
          this.m_owner.m_peakArenaPointsUpText1 = value;
        }
      }

      public Text m_peakArenaPointsUpText2
      {
        get
        {
          return this.m_owner.m_peakArenaPointsUpText2;
        }
        set
        {
          this.m_owner.m_peakArenaPointsUpText2 = value;
        }
      }

      public Image m_peakArenaPointsUpBarImage
      {
        get
        {
          return this.m_owner.m_peakArenaPointsUpBarImage;
        }
        set
        {
          this.m_owner.m_peakArenaPointsUpBarImage = value;
        }
      }

      public GameObject m_peakArenaKnockOutPlayerTip
      {
        get
        {
          return this.m_owner.m_peakArenaKnockOutPlayerTip;
        }
        set
        {
          this.m_owner.m_peakArenaKnockOutPlayerTip = value;
        }
      }

      public CommonUIStateController m_peakArenaArenaPointsUpStateCtrl
      {
        get
        {
          return this.m_owner.m_peakArenaArenaPointsUpStateCtrl;
        }
        set
        {
          this.m_owner.m_peakArenaArenaPointsUpStateCtrl = value;
        }
      }

      public CommonUIStateController m_peakArenaMatchGroupTextStateCtrl
      {
        get
        {
          return this.m_owner.m_peakArenaMatchGroupTextStateCtrl;
        }
        set
        {
          this.m_owner.m_peakArenaMatchGroupTextStateCtrl = value;
        }
      }

      public Text m_peakArenaKnockOutGroupText
      {
        get
        {
          return this.m_owner.m_peakArenaKnockOutGroupText;
        }
        set
        {
          this.m_owner.m_peakArenaKnockOutGroupText = value;
        }
      }

      public Button m_peakArenaRankingRewardButton
      {
        get
        {
          return this.m_owner.m_peakArenaRankingRewardButton;
        }
        set
        {
          this.m_owner.m_peakArenaRankingRewardButton = value;
        }
      }

      public CommonUIStateController m_peakArenaRankingRewardButtonStateCtrl
      {
        get
        {
          return this.m_owner.m_peakArenaRankingRewardButtonStateCtrl;
        }
        set
        {
          this.m_owner.m_peakArenaRankingRewardButtonStateCtrl = value;
        }
      }

      public CommonUIStateController m_peakArenaRankingRewardUIStateCtrl
      {
        get
        {
          return this.m_owner.m_peakArenaRankingRewardUIStateCtrl;
        }
        set
        {
          this.m_owner.m_peakArenaRankingRewardUIStateCtrl = value;
        }
      }

      public Button m_peakArenaRankingRewardBGButton
      {
        get
        {
          return this.m_owner.m_peakArenaRankingRewardBGButton;
        }
        set
        {
          this.m_owner.m_peakArenaRankingRewardBGButton = value;
        }
      }

      public Button m_peakArenaRankingRewardCloseButton
      {
        get
        {
          return this.m_owner.m_peakArenaRankingRewardCloseButton;
        }
        set
        {
          this.m_owner.m_peakArenaRankingRewardCloseButton = value;
        }
      }

      public ScrollRect m_peakArenaRankingRewardScrollRect
      {
        get
        {
          return this.m_owner.m_peakArenaRankingRewardScrollRect;
        }
        set
        {
          this.m_owner.m_peakArenaRankingRewardScrollRect = value;
        }
      }

      public Text m_peakArenaRankingRewardPointRaceExplainText
      {
        get
        {
          return this.m_owner.m_peakArenaRankingRewardPointRaceExplainText;
        }
        set
        {
          this.m_owner.m_peakArenaRankingRewardPointRaceExplainText = value;
        }
      }

      public GameObject m_peakArenaRankingRewardPointRaceContent
      {
        get
        {
          return this.m_owner.m_peakArenaRankingRewardPointRaceContent;
        }
        set
        {
          this.m_owner.m_peakArenaRankingRewardPointRaceContent = value;
        }
      }

      public GameObject m_peakArenaRankingRewardKnockoutGroupContent
      {
        get
        {
          return this.m_owner.m_peakArenaRankingRewardKnockoutGroupContent;
        }
        set
        {
          this.m_owner.m_peakArenaRankingRewardKnockoutGroupContent = value;
        }
      }

      public GameObject m_peakRankingRewardListItemPrefab
      {
        get
        {
          return this.m_owner.m_peakRankingRewardListItemPrefab;
        }
        set
        {
          this.m_owner.m_peakRankingRewardListItemPrefab = value;
        }
      }

      public CommonUIStateController m_promoteBattleUIStateController
      {
        get
        {
          return this.m_owner.m_promoteBattleUIStateController;
        }
        set
        {
          this.m_owner.m_promoteBattleUIStateController = value;
        }
      }

      public Button m_promoteBattleBGButton
      {
        get
        {
          return this.m_owner.m_promoteBattleBGButton;
        }
        set
        {
          this.m_owner.m_promoteBattleBGButton = value;
        }
      }

      public Text m_promoteBattleText
      {
        get
        {
          return this.m_owner.m_promoteBattleText;
        }
        set
        {
          this.m_owner.m_promoteBattleText = value;
        }
      }

      public Button m_promoteBattleConfirmButton
      {
        get
        {
          return this.m_owner.m_promoteBattleConfirmButton;
        }
        set
        {
          this.m_owner.m_promoteBattleConfirmButton = value;
        }
      }

      public CommonUIStateController m_promoteSucceedUIStateController
      {
        get
        {
          return this.m_owner.m_promoteSucceedUIStateController;
        }
        set
        {
          this.m_owner.m_promoteSucceedUIStateController = value;
        }
      }

      public Button m_promoteSucceedBGButton
      {
        get
        {
          return this.m_owner.m_promoteSucceedBGButton;
        }
        set
        {
          this.m_owner.m_promoteSucceedBGButton = value;
        }
      }

      public Image m_promoteSucceedDanImage
      {
        get
        {
          return this.m_owner.m_promoteSucceedDanImage;
        }
        set
        {
          this.m_owner.m_promoteSucceedDanImage = value;
        }
      }

      public Text m_promoteSucceedDanText
      {
        get
        {
          return this.m_owner.m_promoteSucceedDanText;
        }
        set
        {
          this.m_owner.m_promoteSucceedDanText = value;
        }
      }

      public Button m_promoteSucceedConfirmButton
      {
        get
        {
          return this.m_owner.m_promoteSucceedConfirmButton;
        }
        set
        {
          this.m_owner.m_promoteSucceedConfirmButton = value;
        }
      }

      public CommonUIStateController m_promoteFailedUIStateController
      {
        get
        {
          return this.m_owner.m_promoteFailedUIStateController;
        }
        set
        {
          this.m_owner.m_promoteFailedUIStateController = value;
        }
      }

      public Button m_promoteFailedBGButton
      {
        get
        {
          return this.m_owner.m_promoteFailedBGButton;
        }
        set
        {
          this.m_owner.m_promoteFailedBGButton = value;
        }
      }

      public Button m_promoteFailedConfirmButton
      {
        get
        {
          return this.m_owner.m_promoteFailedConfirmButton;
        }
        set
        {
          this.m_owner.m_promoteFailedConfirmButton = value;
        }
      }

      public Image m_promoteFailedNowDanImage
      {
        get
        {
          return this.m_owner.m_promoteFailedNowDanImage;
        }
        set
        {
          this.m_owner.m_promoteFailedNowDanImage = value;
        }
      }

      public Image m_promoteFailedOldDanImage
      {
        get
        {
          return this.m_owner.m_promoteFailedOldDanImage;
        }
        set
        {
          this.m_owner.m_promoteFailedOldDanImage = value;
        }
      }

      public Image m_promoteFailedOldDanGreyImage
      {
        get
        {
          return this.m_owner.m_promoteFailedOldDanGreyImage;
        }
        set
        {
          this.m_owner.m_promoteFailedOldDanGreyImage = value;
        }
      }

      public CommonUIStateController m_matchingFailedUIStateController
      {
        get
        {
          return this.m_owner.m_matchingFailedUIStateController;
        }
        set
        {
          this.m_owner.m_matchingFailedUIStateController = value;
        }
      }

      public Button m_matchingFailedBGButton
      {
        get
        {
          return this.m_owner.m_matchingFailedBGButton;
        }
        set
        {
          this.m_owner.m_matchingFailedBGButton = value;
        }
      }

      public Button m_matchingFailedConfirmButton
      {
        get
        {
          return this.m_owner.m_matchingFailedConfirmButton;
        }
        set
        {
          this.m_owner.m_matchingFailedConfirmButton = value;
        }
      }

      public CommonUIStateController m_matchingNowUIStateController
      {
        get
        {
          return this.m_owner.m_matchingNowUIStateController;
        }
        set
        {
          this.m_owner.m_matchingNowUIStateController = value;
        }
      }

      public Text m_matchingNowTimeText
      {
        get
        {
          return this.m_owner.m_matchingNowTimeText;
        }
        set
        {
          this.m_owner.m_matchingNowTimeText = value;
        }
      }

      public GameObject m_matchingNowPredictTimeGameObject
      {
        get
        {
          return this.m_owner.m_matchingNowPredictTimeGameObject;
        }
        set
        {
          this.m_owner.m_matchingNowPredictTimeGameObject = value;
        }
      }

      public Text m_matchingNowPredictTimeText
      {
        get
        {
          return this.m_owner.m_matchingNowPredictTimeText;
        }
        set
        {
          this.m_owner.m_matchingNowPredictTimeText = value;
        }
      }

      public Button m_matchingNowCancelButton
      {
        get
        {
          return this.m_owner.m_matchingNowCancelButton;
        }
        set
        {
          this.m_owner.m_matchingNowCancelButton = value;
        }
      }

      public CommonUIStateController m_matchingPlayerInfoStateController
      {
        get
        {
          return this.m_owner.m_matchingPlayerInfoStateController;
        }
        set
        {
          this.m_owner.m_matchingPlayerInfoStateController = value;
        }
      }

      public Image m_matchingPlayerInfoOpponentHeadIcon
      {
        get
        {
          return this.m_owner.m_matchingPlayerInfoOpponentHeadIcon;
        }
        set
        {
          this.m_owner.m_matchingPlayerInfoOpponentHeadIcon = value;
        }
      }

      public Transform m_matchingPlayerInfoOpponentHeadFrameDummy
      {
        get
        {
          return this.m_owner.m_matchingPlayerInfoOpponentHeadFrameDummy;
        }
        set
        {
          this.m_owner.m_matchingPlayerInfoOpponentHeadFrameDummy = value;
        }
      }

      public Text m_matchingPlayerInfoOpponentLevelText
      {
        get
        {
          return this.m_owner.m_matchingPlayerInfoOpponentLevelText;
        }
        set
        {
          this.m_owner.m_matchingPlayerInfoOpponentLevelText = value;
        }
      }

      public Text m_matchingPlayerInfoOpponentNameText
      {
        get
        {
          return this.m_owner.m_matchingPlayerInfoOpponentNameText;
        }
        set
        {
          this.m_owner.m_matchingPlayerInfoOpponentNameText = value;
        }
      }

      public Text m_matchingPlayerInfoTimeText
      {
        get
        {
          return this.m_owner.m_matchingPlayerInfoTimeText;
        }
        set
        {
          this.m_owner.m_matchingPlayerInfoTimeText = value;
        }
      }

      public CommonUIStateController m_gradingMatchOverStateCtrl
      {
        get
        {
          return this.m_owner.m_gradingMatchOverStateCtrl;
        }
        set
        {
          this.m_owner.m_gradingMatchOverStateCtrl = value;
        }
      }

      public Button m_gradingMatchOverBGButton
      {
        get
        {
          return this.m_owner.m_gradingMatchOverBGButton;
        }
        set
        {
          this.m_owner.m_gradingMatchOverBGButton = value;
        }
      }

      public Button m_gradingMatchOverConfirmButton
      {
        get
        {
          return this.m_owner.m_gradingMatchOverConfirmButton;
        }
        set
        {
          this.m_owner.m_gradingMatchOverConfirmButton = value;
        }
      }

      public Image m_gradingMatchOverDanImage
      {
        get
        {
          return this.m_owner.m_gradingMatchOverDanImage;
        }
        set
        {
          this.m_owner.m_gradingMatchOverDanImage = value;
        }
      }

      public Text m_gradingMatchOverDanText
      {
        get
        {
          return this.m_owner.m_gradingMatchOverDanText;
        }
        set
        {
          this.m_owner.m_gradingMatchOverDanText = value;
        }
      }

      public Button m_peakFirstWinButton
      {
        get
        {
          return this.m_owner.m_peakFirstWinButton;
        }
        set
        {
          this.m_owner.m_peakFirstWinButton = value;
        }
      }

      public CommonUIStateController m_peakFirstWinChestStateCtrl
      {
        get
        {
          return this.m_owner.m_peakFirstWinChestStateCtrl;
        }
        set
        {
          this.m_owner.m_peakFirstWinChestStateCtrl = value;
        }
      }

      public CommonUIStateController m_rewardPreviewUIStateController
      {
        get
        {
          return this.m_owner.m_rewardPreviewUIStateController;
        }
        set
        {
          this.m_owner.m_rewardPreviewUIStateController = value;
        }
      }

      public Button m_rewardPreviewBackgroundButton
      {
        get
        {
          return this.m_owner.m_rewardPreviewBackgroundButton;
        }
        set
        {
          this.m_owner.m_rewardPreviewBackgroundButton = value;
        }
      }

      public ScrollRect m_rewardPreviewScrollRect
      {
        get
        {
          return this.m_owner.m_rewardPreviewScrollRect;
        }
        set
        {
          this.m_owner.m_rewardPreviewScrollRect = value;
        }
      }

      public CommonUIStateController m_shareBattleReportPanelUIStateCtrl
      {
        get
        {
          return this.m_owner.m_shareBattleReportPanelUIStateCtrl;
        }
        set
        {
          this.m_owner.m_shareBattleReportPanelUIStateCtrl = value;
        }
      }

      public Button m_shareBattleReportPanelBackgroundButton
      {
        get
        {
          return this.m_owner.m_shareBattleReportPanelBackgroundButton;
        }
        set
        {
          this.m_owner.m_shareBattleReportPanelBackgroundButton = value;
        }
      }

      public Button m_shareBattleReportPanelNameCardButtonn
      {
        get
        {
          return this.m_owner.m_shareBattleReportPanelNameCardButtonn;
        }
        set
        {
          this.m_owner.m_shareBattleReportPanelNameCardButtonn = value;
        }
      }

      public CommonUIStateController m_shareBattleReportPanelNameCardButtonState
      {
        get
        {
          return this.m_owner.m_shareBattleReportPanelNameCardButtonState;
        }
        set
        {
          this.m_owner.m_shareBattleReportPanelNameCardButtonState = value;
        }
      }

      public Button m_shareBattleReportPanelFriendButton
      {
        get
        {
          return this.m_owner.m_shareBattleReportPanelFriendButton;
        }
        set
        {
          this.m_owner.m_shareBattleReportPanelFriendButton = value;
        }
      }

      public Button m_shareBattleReportPanelTopAreanButton
      {
        get
        {
          return this.m_owner.m_shareBattleReportPanelTopAreanButton;
        }
        set
        {
          this.m_owner.m_shareBattleReportPanelTopAreanButton = value;
        }
      }

      public Button m_shareBattleReportPanelWorldChannelButon
      {
        get
        {
          return this.m_owner.m_shareBattleReportPanelWorldChannelButon;
        }
        set
        {
          this.m_owner.m_shareBattleReportPanelWorldChannelButon = value;
        }
      }

      public Button m_shareBattleReportPanelGuildButton
      {
        get
        {
          return this.m_owner.m_shareBattleReportPanelGuildButton;
        }
        set
        {
          this.m_owner.m_shareBattleReportPanelGuildButton = value;
        }
      }

      public bool m_isMatchingNow
      {
        get
        {
          return this.m_owner.m_isMatchingNow;
        }
        set
        {
          this.m_owner.m_isMatchingNow = value;
        }
      }

      public PeakArenaPanelType m_curPeakArenaPanelType
      {
        get
        {
          return this.m_owner.m_curPeakArenaPanelType;
        }
        set
        {
          this.m_owner.m_curPeakArenaPanelType = value;
        }
      }

      public List<ConfigDataPeakArenaDanInfo> m_peakArenaDans
      {
        get
        {
          return this.m_owner.m_peakArenaDans;
        }
        set
        {
          this.m_owner.m_peakArenaDans = value;
        }
      }

      public GameObjectPool<ArenaLevelListItemUIController> m_peakArenaLevelPool
      {
        get
        {
          return this.m_owner.m_peakArenaLevelPool;
        }
        set
        {
          this.m_owner.m_peakArenaLevelPool = value;
        }
      }

      public GameObjectPool<PeakArenaRankingListItemUIController> m_peakArenaRankingPool
      {
        get
        {
          return this.m_owner.m_peakArenaRankingPool;
        }
        set
        {
          this.m_owner.m_peakArenaRankingPool = value;
        }
      }

      public GameObjectPool<PeakArenaReportListItemUIController> m_peakArenaReportPool
      {
        get
        {
          return this.m_owner.m_peakArenaReportPool;
        }
        set
        {
          this.m_owner.m_peakArenaReportPool = value;
        }
      }

      public ulong m_curBattleReportInstanceId
      {
        get
        {
          return this.m_owner.m_curBattleReportInstanceId;
        }
        set
        {
          this.m_owner.m_curBattleReportInstanceId = value;
        }
      }

      public PeakArenaKnockoutUIController m_peakArenaKnockoutUIController
      {
        get
        {
          return this.m_owner.m_peakArenaKnockoutUIController;
        }
        set
        {
          this.m_owner.m_peakArenaKnockoutUIController = value;
        }
      }

      public GameObject m_opponentFrameCloneGameObject
      {
        get
        {
          return this.m_owner.m_opponentFrameCloneGameObject;
        }
        set
        {
          this.m_owner.m_opponentFrameCloneGameObject = value;
        }
      }

      public GameObject m_waitOpponentFrameCloneGameObject
      {
        get
        {
          return this.m_owner.m_waitOpponentFrameCloneGameObject;
        }
        set
        {
          this.m_owner.m_waitOpponentFrameCloneGameObject = value;
        }
      }

      public int m_knockoutGroupId
      {
        get
        {
          return this.m_owner.m_knockoutGroupId;
        }
        set
        {
          this.m_owner.m_knockoutGroupId = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void SetUIListeners()
      {
        this.m_owner.SetUIListeners();
      }

      public void SetGameObjectPools()
      {
        this.m_owner.SetGameObjectPools();
      }

      public void SetEditorModeUI()
      {
        this.m_owner.SetEditorModeUI();
      }

      public void SetLeftPlayerInfo()
      {
        this.m_owner.SetLeftPlayerInfo();
      }

      public void SetRewardButtonUIState()
      {
        this.m_owner.SetRewardButtonUIState();
      }

      public void SetClashState()
      {
        this.m_owner.SetClashState();
      }

      public void SetBattleReportState()
      {
        this.m_owner.SetBattleReportState();
      }

      public int IComparorPeakBattleReport(BattleReportHead r1, BattleReportHead r2)
      {
        return this.m_owner.IComparorPeakBattleReport(r1, r2);
      }

      public void SetArenaLevelState()
      {
        this.m_owner.SetArenaLevelState();
      }

      public void SetRankingState()
      {
        this.m_owner.SetRankingState();
      }

      public string GetLadderModeStateName()
      {
        return this.m_owner.GetLadderModeStateName();
      }

      public void SetPeakClashLadderRestMode()
      {
        this.m_owner.SetPeakClashLadderRestMode();
      }

      public void SetPeakClashLadderBeginTimeMode()
      {
        this.m_owner.SetPeakClashLadderBeginTimeMode();
      }

      public void SetPeakClashLadderMatchMode()
      {
        this.m_owner.SetPeakClashLadderMatchMode();
      }

      public void SetPeakClashLadderGradingMatchMode()
      {
        this.m_owner.SetPeakClashLadderGradingMatchMode();
      }

      public void SetPeakClashLadderGradingMatchCloseTimeMode()
      {
        this.m_owner.SetPeakClashLadderGradingMatchCloseTimeMode();
      }

      public void SetPeakClashLadderOpponentMode()
      {
        this.m_owner.SetPeakClashLadderOpponentMode();
      }

      public TimeSpan GetKnockOutMatchReadyLastTime()
      {
        return this.m_owner.GetKnockOutMatchReadyLastTime();
      }

      public void SetKnockOutMatchingPlayerInfoPanel()
      {
        this.m_owner.SetKnockOutMatchingPlayerInfoPanel();
      }

      public void UpdateKnockOutMatchingPlayerInfoPanelTimeText()
      {
        this.m_owner.UpdateKnockOutMatchingPlayerInfoPanelTimeText();
      }

      public void SetPeakClashLadderWaitOpponentMode()
      {
        this.m_owner.SetPeakClashLadderWaitOpponentMode();
      }

      public void SetPeakClashLadderOpponentAbstentionMode()
      {
        this.m_owner.SetPeakClashLadderOpponentAbstentionMode();
      }

      public string GetKnockOutMatchTitleTipText()
      {
        return this.m_owner.GetKnockOutMatchTitleTipText();
      }

      public string GetSeasonBeginTime()
      {
        return this.m_owner.GetSeasonBeginTime();
      }

      public string GetSeasonBeginTimeTitleText()
      {
        return this.m_owner.GetSeasonBeginTimeTitleText();
      }

      public string GetSeasonPointMatchEndTime()
      {
        return this.m_owner.GetSeasonPointMatchEndTime();
      }

      public string GetPeakArenaBattleSeason()
      {
        return this.m_owner.GetPeakArenaBattleSeason();
      }

      public void ShowArenaTopPromotionTipDialog()
      {
        this.m_owner.ShowArenaTopPromotionTipDialog();
      }

      public void OnReturnButtonClick()
      {
        this.m_owner.OnReturnButtonClick();
      }

      public void OnHelpButtonClick()
      {
        this.m_owner.OnHelpButtonClick();
      }

      public void OnManagementTeamClick()
      {
        this.m_owner.OnManagementTeamClick();
      }

      public void OnPeakClashCasualChallengeButtonClick()
      {
        this.m_owner.OnPeakClashCasualChallengeButtonClick();
      }

      public void OnPeakClashLadderChallengeButtonClick()
      {
        this.m_owner.OnPeakClashLadderChallengeButtonClick();
      }

      public void OnReportShareButtonClick(ulong reportInstanceId)
      {
        this.m_owner.OnReportShareButtonClick(reportInstanceId);
      }

      public void OnReportReplyButtonClick(ulong reportInstanceId)
      {
        this.m_owner.OnReportReplyButtonClick(reportInstanceId);
      }

      public void OnBattleReportNameChangeButtonClick(ulong reportInstanceId)
      {
        this.m_owner.OnBattleReportNameChangeButtonClick(reportInstanceId);
      }

      public void OnShareBattleReportPanelGuildButtonClick()
      {
        this.m_owner.OnShareBattleReportPanelGuildButtonClick();
      }

      public void OnShareBattleReportPanelWorldChannelButtonClick()
      {
        this.m_owner.OnShareBattleReportPanelWorldChannelButtonClick();
      }

      public void OnShareBattleReportPanelTopAreanButtonClick()
      {
        this.m_owner.OnShareBattleReportPanelTopAreanButtonClick();
      }

      public void OnShareBattleReportPanelFriendButtonClick()
      {
        this.m_owner.OnShareBattleReportPanelFriendButtonClick();
      }

      public void OnShareBattleReportPanelNameCardButtonClick()
      {
        this.m_owner.OnShareBattleReportPanelNameCardButtonClick();
      }

      public void OnShareBattleReportPanelBackgroundButtonClick()
      {
        this.m_owner.OnShareBattleReportPanelBackgroundButtonClick();
      }

      public void OnPeakClashToggle(bool on)
      {
        this.m_owner.OnPeakClashToggle(on);
      }

      public void OnPeakBattleReportToggle(bool on)
      {
        this.m_owner.OnPeakBattleReportToggle(on);
      }

      public void OnPeakDanToggle(bool on)
      {
        this.m_owner.OnPeakDanToggle(on);
      }

      public void OnPeakLocalRankingToggle(bool on)
      {
        this.m_owner.OnPeakLocalRankingToggle(on);
      }

      public void OnPeakKnockoutToggle(bool on)
      {
        this.m_owner.OnPeakKnockoutToggle(on);
      }

      public void OnToggleLockClick()
      {
        this.m_owner.OnToggleLockClick();
      }

      public void OnPeakArenaRankingRewardButtonClick()
      {
        this.m_owner.OnPeakArenaRankingRewardButtonClick();
      }

      public void OnMatchingNowCancelButtonClick()
      {
        this.m_owner.OnMatchingNowCancelButtonClick();
      }

      public void OnPeakFirstWinButtonClick()
      {
        this.m_owner.OnPeakFirstWinButtonClick();
      }

      public void OnPromoteFailedBGButtonClick()
      {
        this.m_owner.OnPromoteFailedBGButtonClick();
      }

      public void OnGradingMatchOverConfirmButtonClick()
      {
        this.m_owner.OnGradingMatchOverConfirmButtonClick();
      }

      public void OnRewardPreviewBackgroundButtonClick()
      {
        this.m_owner.OnRewardPreviewBackgroundButtonClick();
      }

      public void OnPromoteBattleConfirmButtonClick()
      {
        this.m_owner.OnPromoteBattleConfirmButtonClick();
      }

      public void OnPromoteSucceedConfirmButtonClick()
      {
        this.m_owner.OnPromoteSucceedConfirmButtonClick();
      }

      public void OnMatchingFailedConfirmButtonClick()
      {
        this.m_owner.OnMatchingFailedConfirmButtonClick();
      }

      public void OnPeakArenaRankingRewardCloseButtonClick()
      {
        this.m_owner.OnPeakArenaRankingRewardCloseButtonClick();
      }

      public void OnAddJettonTicketClick()
      {
        this.m_owner.OnAddJettonTicketClick();
      }
    }
  }
}
