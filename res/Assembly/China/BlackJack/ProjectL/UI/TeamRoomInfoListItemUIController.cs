﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.TeamRoomInfoListItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class TeamRoomInfoListItemUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_button;
    [AutoBind("./Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_text;
    [AutoBind("./LockImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_lockGameObject;
    private int m_index;
    private int m_value;
    private int m_value2;
    private bool m_isLocked;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetText(string text)
    {
      this.m_text.text = text;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTextAlpha(float a)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetIndex(int idx)
    {
      this.m_index = idx;
    }

    public int GetIndex()
    {
      return this.m_index;
    }

    public void SetValue(int value)
    {
      this.m_value = value;
    }

    public int GetValue()
    {
      return this.m_value;
    }

    public void SetValue2(int value)
    {
      this.m_value2 = value;
    }

    public int GetValue2()
    {
      return this.m_value2;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLocked(bool isLocked)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsLocked()
    {
      return this.m_isLocked;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<TeamRoomInfoListItemUIController> EventOnButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
