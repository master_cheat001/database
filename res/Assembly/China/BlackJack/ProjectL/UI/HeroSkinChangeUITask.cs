﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroSkinChangeUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class HeroSkinChangeUITask : UITask
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    public const string Mode_SkinBuy = "SkinBuyMode";
    public const string Mode_ShowSkinDetail = "ShowSkinDetailMode";
    public const string Mode_ShowOneSkin = "ShowOneSkin";
    private const string ParamKey_StoreId = "StoreId";
    private const string ParamKey_HeroSkinInfoId = "HeroSkinInfoId";
    private const string ParamKey_HeroSkinMode = "HeroSkinMode";
    private const string ParamKey_HeroObj = "HeroObj";
    private StoreId m_storeId;
    private int m_heroSkinInfoId;
    private string m_skinMode;
    private Hero m_hero;
    private HeroSkinChangeUIController m_heroSkinChangeUIController;
    [DoNotToLua]
    private HeroSkinChangeUITask.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctorString_hotfix;
    private LuaFunction m_IsNeedLoadDynamicRes_hotfix;
    private LuaFunction m_InitAllUIControllers_hotfix;
    private LuaFunction m_ClearAllContextAndRes_hotfix;
    private LuaFunction m_OnStartUIIntent_hotfix;
    private LuaFunction m_OnResumeUIIntent_hotfix;
    private LuaFunction m_OnPause_hotfix;
    private LuaFunction m_OnNewIntentUIIntent_hotfix;
    private LuaFunction m_InitDataFromUIIntentUIIntent_hotfix;
    private LuaFunction m_OnMemoryWarning_hotfix;
    private LuaFunction m_UpdateView_hotfix;
    private LuaFunction m_HeroCharSkinChangeUIController_OnClose_hotfix;
    private LuaFunction m_HeroCharSkinChangeUIController_OnBuySkinInt32_hotfix;
    private LuaFunction m_HeroCharSkinChangeUIController_OnTakeOffCharSkinInt32Action_hotfix;
    private LuaFunction m_HeroCharSkinChangeUIController_OnWearCharSkinInt32Int32Action_hotfix;
    private LuaFunction m_HeroCharSkinChangeUIController_OnWearModelSkinInt32Int32Int32_hotfix;
    private LuaFunction m_HeroCharSkinChangeUIController_OnTakeOffModelSkinInt32Int32_hotfix;
    private LuaFunction m_HeroSkinChangeUIController_OnSkinChangedPreviewStringInt32_hotfix;
    private LuaFunction m_HeroSkinChangeUIController_OnSkinTicketBuy_hotfix;
    private LuaFunction m_ClearUIControllerData_hotfix;
    private LuaFunction m_CloseHeroSkinChangePanelWithAnimAction_hotfix;
    private LuaFunction m_add_EventOnPause2Action_hotfix;
    private LuaFunction m_remove_EventOnPause2Action_hotfix;
    private LuaFunction m_add_EventOnCloseAction_hotfix;
    private LuaFunction m_remove_EventOnCloseAction_hotfix;
    private LuaFunction m_add_EventOnBuySkinSucceedAction`1_hotfix;
    private LuaFunction m_remove_EventOnBuySkinSucceedAction`1_hotfix;
    private LuaFunction m_add_EventOnSkinChangedPreviewAction`2_hotfix;
    private LuaFunction m_remove_EventOnSkinChangedPreviewAction`2_hotfix;
    private LuaFunction m_add_EventOnAddSkinTicketAction_hotfix;
    private LuaFunction m_remove_EventOnAddSkinTicketAction_hotfix;
    private LuaFunction m_get_LayerDescArray_hotfix;
    private LuaFunction m_get_UICtrlDescArray_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroSkinChangeUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static HeroSkinChangeUITask StartUITask(
      string mode,
      StoreId storeId,
      int heroSkinInfoId = 0,
      Hero hero = null,
      UIIntent preUIIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void HeroSkinChangeUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnResume(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnPause()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override bool OnNewIntent(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitDataFromUIIntent(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnMemoryWarning()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroCharSkinChangeUIController_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroCharSkinChangeUIController_OnBuySkin(int skinId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroCharSkinChangeUIController_OnTakeOffCharSkin(int heroId, Action OnSucceed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroCharSkinChangeUIController_OnWearCharSkin(
      int heroId,
      int charSkinId,
      Action OnSucceed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroCharSkinChangeUIController_OnWearModelSkin(
      int heroId,
      int jobRelatedId,
      int modelSkinId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroCharSkinChangeUIController_OnTakeOffModelSkin(int heroId, int jobRelatedId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroSkinChangeUIController_OnSkinChangedPreview(string spinePath, int heroSkinId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroSkinChangeUIController_OnSkinTicketBuy()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearUIControllerData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CloseHeroSkinChangePanelWithAnim(Action onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnPause2
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnBuySkinSucceed
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string, int> EventOnSkinChangedPreview
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnAddSkinTicket
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public HeroSkinChangeUITask.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private bool __callBase_OnStart(UIIntent intent)
    {
      return base.OnStart(intent);
    }

    private void __callBase_OnStop()
    {
      this.OnStop();
    }

    private void __callBase_OnPause()
    {
      base.OnPause();
    }

    private bool __callBase_OnResume(UIIntent intent)
    {
      return base.OnResume(intent);
    }

    private bool __callBase_OnNewIntent(UIIntent intent)
    {
      return base.OnNewIntent(intent);
    }

    private void __callBase_EnableUIInput(bool isEnable, bool? isGlobalEnable)
    {
      this.EnableUIInput(isEnable, isGlobalEnable);
    }

    private bool __callBase_IsNeedLoadDynamicRes()
    {
      return base.IsNeedLoadDynamicRes();
    }

    private List<string> __callBase_CollectAllDynamicResForLoad()
    {
      return this.CollectAllDynamicResForLoad();
    }

    private void __callBase_RegisterPlayerContextEvents()
    {
      this.RegisterPlayerContextEvents();
    }

    private void __callBase_UnregisterPlayerContextEvents()
    {
      this.UnregisterPlayerContextEvents();
    }

    private void __callBase_PostUpdateView()
    {
      this.PostUpdateView();
    }

    private void __callBase_CollectPreloadResourceList()
    {
      this.CollectPreloadResourceList();
    }

    private void __callBase_ClearAssetList()
    {
      this.ClearAssetList();
    }

    private void __callBase_CollectAsset(string assetName)
    {
      this.CollectAsset(assetName);
    }

    private void __callBase_CollectSpriteAsset(string assetName)
    {
      this.CollectSpriteAsset(assetName);
    }

    private void __callBase_CollectFxAsset(string assetName)
    {
      this.CollectFxAsset(assetName);
    }

    private void __callBase_StartLoadCustomAssets(Action onLoadCompleted)
    {
      this.StartLoadCustomAssets(onLoadCompleted);
    }

    private bool __callBase_IsLoadingCustomAssets()
    {
      return this.IsLoadingCustomAssets();
    }

    private bool __callBase_IsPipeLineRunning()
    {
      return this.IsPipeLineRunning();
    }

    private bool __callBase_IsOpeningUI()
    {
      return this.IsOpeningUI();
    }

    private UITaskBase __callBase_ReturnPrevUITask()
    {
      return this.ReturnPrevUITask();
    }

    private void __callBase_ClearUnusedDynamicResourceCache()
    {
      this.ClearUnusedDynamicResourceCache();
    }

    private void __callBase_ClearDynamicResourceCache(List<string> assets)
    {
      this.ClearDynamicResourceCache(assets);
    }

    private void __callBase_OnMemoryWarning()
    {
      base.OnMemoryWarning();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnPause2()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnPause2()
    {
      this.EventOnPause2 = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnClose()
    {
      this.EventOnClose = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnBuySkinSucceed(int obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnBuySkinSucceed(int obj)
    {
      this.EventOnBuySkinSucceed = (Action<int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnSkinChangedPreview(string arg1, int arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnSkinChangedPreview(string arg1, int arg2)
    {
      this.EventOnSkinChangedPreview = (Action<string, int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnAddSkinTicket()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnAddSkinTicket()
    {
      this.EventOnAddSkinTicket = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private HeroSkinChangeUITask m_owner;

      public LuaExportHelper(HeroSkinChangeUITask owner)
      {
        this.m_owner = owner;
      }

      public bool __callBase_OnStart(UIIntent intent)
      {
        return this.m_owner.__callBase_OnStart(intent);
      }

      public void __callBase_OnStop()
      {
        this.m_owner.__callBase_OnStop();
      }

      public void __callBase_OnPause()
      {
        this.m_owner.__callBase_OnPause();
      }

      public bool __callBase_OnResume(UIIntent intent)
      {
        return this.m_owner.__callBase_OnResume(intent);
      }

      public bool __callBase_OnNewIntent(UIIntent intent)
      {
        return this.m_owner.__callBase_OnNewIntent(intent);
      }

      public void __callBase_EnableUIInput(bool isEnable, bool? isGlobalEnable)
      {
        this.m_owner.__callBase_EnableUIInput(isEnable, isGlobalEnable);
      }

      public bool __callBase_IsNeedLoadDynamicRes()
      {
        return this.m_owner.__callBase_IsNeedLoadDynamicRes();
      }

      public List<string> __callBase_CollectAllDynamicResForLoad()
      {
        return this.m_owner.__callBase_CollectAllDynamicResForLoad();
      }

      public void __callBase_RegisterPlayerContextEvents()
      {
        this.m_owner.__callBase_RegisterPlayerContextEvents();
      }

      public void __callBase_UnregisterPlayerContextEvents()
      {
        this.m_owner.__callBase_UnregisterPlayerContextEvents();
      }

      public void __callBase_PostUpdateView()
      {
        this.m_owner.__callBase_PostUpdateView();
      }

      public void __callBase_CollectPreloadResourceList()
      {
        this.m_owner.__callBase_CollectPreloadResourceList();
      }

      public void __callBase_ClearAssetList()
      {
        this.m_owner.__callBase_ClearAssetList();
      }

      public void __callBase_CollectAsset(string assetName)
      {
        this.m_owner.__callBase_CollectAsset(assetName);
      }

      public void __callBase_CollectSpriteAsset(string assetName)
      {
        this.m_owner.__callBase_CollectSpriteAsset(assetName);
      }

      public void __callBase_CollectFxAsset(string assetName)
      {
        this.m_owner.__callBase_CollectFxAsset(assetName);
      }

      public void __callBase_StartLoadCustomAssets(Action onLoadCompleted)
      {
        this.m_owner.__callBase_StartLoadCustomAssets(onLoadCompleted);
      }

      public bool __callBase_IsLoadingCustomAssets()
      {
        return this.m_owner.__callBase_IsLoadingCustomAssets();
      }

      public bool __callBase_IsPipeLineRunning()
      {
        return this.m_owner.__callBase_IsPipeLineRunning();
      }

      public bool __callBase_IsOpeningUI()
      {
        return this.m_owner.__callBase_IsOpeningUI();
      }

      public UITaskBase __callBase_ReturnPrevUITask()
      {
        return this.m_owner.__callBase_ReturnPrevUITask();
      }

      public void __callBase_ClearUnusedDynamicResourceCache()
      {
        this.m_owner.__callBase_ClearUnusedDynamicResourceCache();
      }

      public void __callBase_ClearDynamicResourceCache(List<string> assets)
      {
        this.m_owner.__callBase_ClearDynamicResourceCache(assets);
      }

      public void __callBase_OnMemoryWarning()
      {
        this.m_owner.__callBase_OnMemoryWarning();
      }

      public void __callDele_EventOnPause2()
      {
        this.m_owner.__callDele_EventOnPause2();
      }

      public void __clearDele_EventOnPause2()
      {
        this.m_owner.__clearDele_EventOnPause2();
      }

      public void __callDele_EventOnClose()
      {
        this.m_owner.__callDele_EventOnClose();
      }

      public void __clearDele_EventOnClose()
      {
        this.m_owner.__clearDele_EventOnClose();
      }

      public void __callDele_EventOnBuySkinSucceed(int obj)
      {
        this.m_owner.__callDele_EventOnBuySkinSucceed(obj);
      }

      public void __clearDele_EventOnBuySkinSucceed(int obj)
      {
        this.m_owner.__clearDele_EventOnBuySkinSucceed(obj);
      }

      public void __callDele_EventOnSkinChangedPreview(string arg1, int arg2)
      {
        this.m_owner.__callDele_EventOnSkinChangedPreview(arg1, arg2);
      }

      public void __clearDele_EventOnSkinChangedPreview(string arg1, int arg2)
      {
        this.m_owner.__clearDele_EventOnSkinChangedPreview(arg1, arg2);
      }

      public void __callDele_EventOnAddSkinTicket()
      {
        this.m_owner.__callDele_EventOnAddSkinTicket();
      }

      public void __clearDele_EventOnAddSkinTicket()
      {
        this.m_owner.__clearDele_EventOnAddSkinTicket();
      }

      public UITaskBase.LayerDesc[] m_layerDescArray
      {
        get
        {
          return this.m_owner.m_layerDescArray;
        }
        set
        {
          this.m_owner.m_layerDescArray = value;
        }
      }

      public UITaskBase.UIControllerDesc[] m_uiCtrlDescArray
      {
        get
        {
          return this.m_owner.m_uiCtrlDescArray;
        }
        set
        {
          this.m_owner.m_uiCtrlDescArray = value;
        }
      }

      public static string ParamKey_StoreId
      {
        get
        {
          return "StoreId";
        }
      }

      public static string ParamKey_HeroSkinInfoId
      {
        get
        {
          return "HeroSkinInfoId";
        }
      }

      public static string ParamKey_HeroSkinMode
      {
        get
        {
          return "HeroSkinMode";
        }
      }

      public static string ParamKey_HeroObj
      {
        get
        {
          return "HeroObj";
        }
      }

      public StoreId m_storeId
      {
        get
        {
          return this.m_owner.m_storeId;
        }
        set
        {
          this.m_owner.m_storeId = value;
        }
      }

      public int m_heroSkinInfoId
      {
        get
        {
          return this.m_owner.m_heroSkinInfoId;
        }
        set
        {
          this.m_owner.m_heroSkinInfoId = value;
        }
      }

      public string m_skinMode
      {
        get
        {
          return this.m_owner.m_skinMode;
        }
        set
        {
          this.m_owner.m_skinMode = value;
        }
      }

      public Hero m_hero
      {
        get
        {
          return this.m_owner.m_hero;
        }
        set
        {
          this.m_owner.m_hero = value;
        }
      }

      public HeroSkinChangeUIController m_heroSkinChangeUIController
      {
        get
        {
          return this.m_owner.m_heroSkinChangeUIController;
        }
        set
        {
          this.m_owner.m_heroSkinChangeUIController = value;
        }
      }

      public UITaskBase.LayerDesc[] LayerDescArray
      {
        get
        {
          return this.m_owner.LayerDescArray;
        }
      }

      public UITaskBase.UIControllerDesc[] UICtrlDescArray
      {
        get
        {
          return this.m_owner.UICtrlDescArray;
        }
      }

      public bool IsNeedLoadDynamicRes()
      {
        return this.m_owner.IsNeedLoadDynamicRes();
      }

      public void InitAllUIControllers()
      {
        this.m_owner.InitAllUIControllers();
      }

      public void ClearAllContextAndRes()
      {
        this.m_owner.ClearAllContextAndRes();
      }

      public bool OnStart(UIIntent intent)
      {
        return this.m_owner.OnStart(intent);
      }

      public bool OnResume(UIIntent intent)
      {
        return this.m_owner.OnResume(intent);
      }

      public void OnPause()
      {
        this.m_owner.OnPause();
      }

      public void InitDataFromUIIntent(UIIntent intent)
      {
        this.m_owner.InitDataFromUIIntent(intent);
      }

      public void OnMemoryWarning()
      {
        this.m_owner.OnMemoryWarning();
      }

      public void UpdateView()
      {
        this.m_owner.UpdateView();
      }

      public void HeroCharSkinChangeUIController_OnClose()
      {
        this.m_owner.HeroCharSkinChangeUIController_OnClose();
      }

      public void HeroCharSkinChangeUIController_OnBuySkin(int skinId)
      {
        this.m_owner.HeroCharSkinChangeUIController_OnBuySkin(skinId);
      }

      public void HeroCharSkinChangeUIController_OnTakeOffCharSkin(int heroId, Action OnSucceed)
      {
        this.m_owner.HeroCharSkinChangeUIController_OnTakeOffCharSkin(heroId, OnSucceed);
      }

      public void HeroCharSkinChangeUIController_OnWearCharSkin(
        int heroId,
        int charSkinId,
        Action OnSucceed)
      {
        this.m_owner.HeroCharSkinChangeUIController_OnWearCharSkin(heroId, charSkinId, OnSucceed);
      }

      public void HeroCharSkinChangeUIController_OnWearModelSkin(
        int heroId,
        int jobRelatedId,
        int modelSkinId)
      {
        this.m_owner.HeroCharSkinChangeUIController_OnWearModelSkin(heroId, jobRelatedId, modelSkinId);
      }

      public void HeroCharSkinChangeUIController_OnTakeOffModelSkin(int heroId, int jobRelatedId)
      {
        this.m_owner.HeroCharSkinChangeUIController_OnTakeOffModelSkin(heroId, jobRelatedId);
      }

      public void HeroSkinChangeUIController_OnSkinChangedPreview(string spinePath, int heroSkinId)
      {
        this.m_owner.HeroSkinChangeUIController_OnSkinChangedPreview(spinePath, heroSkinId);
      }

      public void HeroSkinChangeUIController_OnSkinTicketBuy()
      {
        this.m_owner.HeroSkinChangeUIController_OnSkinTicketBuy();
      }
    }
  }
}
