﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroDungeonUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class HeroDungeonUITask : UITask
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private HeroDungeonUIController m_heroDungeonUIController;
    private PlayerResourceUIController m_playerResourceUIController;
    private BattleLevelInfoUITask m_battleLevelInfoUITask;
    private ConfigDataHeroInformationInfo m_heroInformationInfo;
    private ConfigDataHeroDungeonLevelInfo m_heroDungeonLevelInfo;
    private IConfigDataLoader m_configDataLoader;
    private ProjectLPlayerContext m_playerContext;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroDungeonUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    protected override bool OnStart(UIIntent intent)
    {
      this.InitDataFromUIIntent(intent);
      return base.OnStart(intent);
    }

    protected override bool OnResume(UIIntent intent)
    {
      this.InitDataFromUIIntent(intent);
      return base.OnResume(intent);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitDataFromUIIntent(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    protected override void OnMemoryWarning()
    {
      this.ClearUnusedDynamicResourceCache();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AutoGetStarReward()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerResourceUIController_OnAddCrystal()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroDungeonUIController_OnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    private void HeroDungeonUIController_OnShowHelp()
    {
      CommonUIController.Instance.ShowExplanation(ExplanationId.ExplanationId_HeroDungeon);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroDungeonUIController_OnGetStarReward(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroDungeonUIController_OnSelectDungeonLevel(
      ConfigDataHeroDungeonLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartHeroDungeonLevelInfoUITask(
      ConfigDataHeroDungeonLevelInfo heroDungeonLevelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleLevelInfoUITask_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleLevelInfoUITask_OnHeroDungeonRaidComplete(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ChestUITask_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      get
      {
        return this.m_layerDescArray;
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      get
      {
        return this.m_uiCtrlDescArray;
      }
    }
  }
}
