﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PeakArenaHeroDetailUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.Scene;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class PeakArenaHeroDetailUIController : UIControllerBase
  {
    [AutoBind("./HeroDetailSelectSkillUIPrefab", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_selectSkillPanel;
    [AutoBind("./EquipmentDescDummy", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipmentDescDummy;
    [AutoBind("./HeroDetailPanel/Detail/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_closeButton;
    [AutoBind("./HeroDetailPanel/Detail/RightPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_rightPanel;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/HeroInfo/InfoGroup/ArmyImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroInfoArmyImage;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/HeroInfo/InfoGroup/LvValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroInfoLevelText;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/HeroInfo/InfoGroup/HeroNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroInfoNameText;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/HeroInfo/InfoGroup/JobText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroInfoJobText;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/HeroInfo/InfoGroup/PowerValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroInfoPowerText;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/HeroInfo/InfoGroup/PlayerNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerNameText;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/HeroInfo/InfoGroup/ServerNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_serverNameText;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/HeroInfo/InfoGroup/Graphic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroInfoGraphicDummy;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/Property/HP/EvaluateImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_infoHeroPropHPImg;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/Property/DF/EvaluateImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_infoHeroPropDFImg;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/Property/AT/EvaluateImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_infoHeroPropATImg;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/Property/MagicDF/EvaluateImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_infoHeroPropMagicDFImg;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/Property/Magic/EvaluateImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_infoHeroPropMagicImg;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/Property/DEX/EvaluateImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_infoHeroPropDEXImg;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/Property/HP/Text/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoHeroPropHPValueText;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/Property/DF/Text/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoHeroPropDFValueText;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/Property/AT/Text/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoHeroPropATValueText;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/Property/MagicDF/Text/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoHeroPropMagicDFValueText;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/Property/Magic/Text/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoHeroPropMagicValueText;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/Property/DEX/Text/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoHeroPropDEXValueText;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/Property/HP/Text/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoHeroPropHPAddText;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/Property/DF/Text/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoHeroPropDFAddText;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/Property/AT/Text/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoHeroPropATAddText;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/Property/MagicDF/Text/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoHeroPropMagicDFAddText;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/Property/Magic/Text/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoHeroPropMagicAddText;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/Property/DEX/Text/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoHeroPropDEXAddText;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/Skill/Talent", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_talentSkillButton;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/Skill/Talent/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_infoHeroTalentIcon;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/Skill/CostGroup/Costs", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillTotalCost;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/Skill/SkillGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_curSkillContent;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/Skill/ChangeButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_skillChangeButton;
    [AutoBind("./Prefab/SkillItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillItemPrefab;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/Skill/SkillItemDetailPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_SkillItemDetailDummy;
    [AutoBind("./Prefab/EquipItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemPrefab;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/Equip/EquipGroup/EquipItem1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItem1;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/Equip/EquipGroup/EquipItem2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItem2;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/Equip/EquipGroup/EquipItem3", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItem3;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/Equip/EquipGroup/EquipItem4", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItem4;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private PeakArenaHeroDetailUITask m_peakArenaHeroDetailUITask;
    private PeakArenaManagementTeamUIController.HeroWrap m_heroWrap;
    private UISpineGraphic m_playerHeroGraphic;
    private HeroDetailSelectSkillUIController m_heroDetailSelectSkillUIController;
    private SkillDescUIController m_skillDescUIController;
    private PeakArenaSoldierUIController m_heroDetailSoldierUIController;
    private PeakArenaEquipmentDetailUIController m_equipmentDetailUIController;
    [DoNotToLua]
    private PeakArenaHeroDetailUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_SetDataHeroWrap_hotfix;
    private LuaFunction m_UpdateViewHeroWrap_hotfix;
    private LuaFunction m_UpdateViewBaseInfo_hotfix;
    private LuaFunction m_UpdateHeroPropertyHeroWrap_hotfix;
    private LuaFunction m_UpdateHeroSkillHero_hotfix;
    private LuaFunction m_UpdateHeroEquipment_hotfix;
    private LuaFunction m_UpdateHeroSingleEquipmentUInt64GameObject_hotfix;
    private LuaFunction m_UpdateSoldierHero_hotfix;
    private LuaFunction m_CreateSpineGraphic_hotfix;
    private LuaFunction m_OnCloseClick_hotfix;
    private LuaFunction m_OnSkillChangeClick_hotfix;
    private LuaFunction m_OnHeroSkillsSelectInt32List`1Boolean_hotfix;
    private LuaFunction m_OnSkillItemClickPeakArenaSkillItemUIController_hotfix;
    private LuaFunction m_OnTalentSkillItemClick_hotfix;
    private LuaFunction m_OnItemDetailClickPeakArenaEquipItemUIController_hotfix;
    private LuaFunction m_HeroDetailSoldierUIController_OnHeroSoldierSelectInt32Int32Action_hotfix;
    private LuaFunction m_HeroDetailSoldierUIController_OnGotoDrillInt32_hotfix;
    private LuaFunction m_HeroDetailSoldierUIController_OnGotoJobTransferConfigDataJobConnectionInfo_hotfix;
    private LuaFunction m_HeroDetailSoldierUIController_OnSkinInfoButtonClickConfigDataSoldierInfo_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetData(
      PeakArenaManagementTeamUIController.HeroWrap heroWrap)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateView(
      PeakArenaManagementTeamUIController.HeroWrap heroWrap)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateViewBaseInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateHeroProperty(
      PeakArenaManagementTeamUIController.HeroWrap heroWrap)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateHeroSkill(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateHeroEquipment()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateHeroSingleEquipment(ulong id, GameObject parent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateSoldier(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateSpineGraphic()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSkillChangeClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroSkillsSelect(int heroId, List<int> skillIds, bool isSkillChanged)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSkillItemClick(
      PeakArenaSkillItemUIController peakArenaSkillItemUIController)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTalentSkillItemClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnItemDetailClick(
      PeakArenaEquipItemUIController equipItemUIController)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroDetailSoldierUIController_OnHeroSoldierSelect(
      int heroId,
      int soldierId,
      Action OnSucceed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroDetailSoldierUIController_OnGotoDrill(int techId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroDetailSoldierUIController_OnGotoJobTransfer(
      ConfigDataJobConnectionInfo jobConnectionInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroDetailSoldierUIController_OnSkinInfoButtonClick(
      ConfigDataSoldierInfo soldierInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public PeakArenaHeroDetailUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private PeakArenaHeroDetailUIController m_owner;

      public LuaExportHelper(PeakArenaHeroDetailUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public GameObject m_selectSkillPanel
      {
        get
        {
          return this.m_owner.m_selectSkillPanel;
        }
        set
        {
          this.m_owner.m_selectSkillPanel = value;
        }
      }

      public GameObject m_equipmentDescDummy
      {
        get
        {
          return this.m_owner.m_equipmentDescDummy;
        }
        set
        {
          this.m_owner.m_equipmentDescDummy = value;
        }
      }

      public Button m_closeButton
      {
        get
        {
          return this.m_owner.m_closeButton;
        }
        set
        {
          this.m_owner.m_closeButton = value;
        }
      }

      public GameObject m_rightPanel
      {
        get
        {
          return this.m_owner.m_rightPanel;
        }
        set
        {
          this.m_owner.m_rightPanel = value;
        }
      }

      public Image m_heroInfoArmyImage
      {
        get
        {
          return this.m_owner.m_heroInfoArmyImage;
        }
        set
        {
          this.m_owner.m_heroInfoArmyImage = value;
        }
      }

      public Text m_heroInfoLevelText
      {
        get
        {
          return this.m_owner.m_heroInfoLevelText;
        }
        set
        {
          this.m_owner.m_heroInfoLevelText = value;
        }
      }

      public Text m_heroInfoNameText
      {
        get
        {
          return this.m_owner.m_heroInfoNameText;
        }
        set
        {
          this.m_owner.m_heroInfoNameText = value;
        }
      }

      public Text m_heroInfoJobText
      {
        get
        {
          return this.m_owner.m_heroInfoJobText;
        }
        set
        {
          this.m_owner.m_heroInfoJobText = value;
        }
      }

      public Text m_heroInfoPowerText
      {
        get
        {
          return this.m_owner.m_heroInfoPowerText;
        }
        set
        {
          this.m_owner.m_heroInfoPowerText = value;
        }
      }

      public Text m_playerNameText
      {
        get
        {
          return this.m_owner.m_playerNameText;
        }
        set
        {
          this.m_owner.m_playerNameText = value;
        }
      }

      public Text m_serverNameText
      {
        get
        {
          return this.m_owner.m_serverNameText;
        }
        set
        {
          this.m_owner.m_serverNameText = value;
        }
      }

      public GameObject m_heroInfoGraphicDummy
      {
        get
        {
          return this.m_owner.m_heroInfoGraphicDummy;
        }
        set
        {
          this.m_owner.m_heroInfoGraphicDummy = value;
        }
      }

      public Image m_infoHeroPropHPImg
      {
        get
        {
          return this.m_owner.m_infoHeroPropHPImg;
        }
        set
        {
          this.m_owner.m_infoHeroPropHPImg = value;
        }
      }

      public Image m_infoHeroPropDFImg
      {
        get
        {
          return this.m_owner.m_infoHeroPropDFImg;
        }
        set
        {
          this.m_owner.m_infoHeroPropDFImg = value;
        }
      }

      public Image m_infoHeroPropATImg
      {
        get
        {
          return this.m_owner.m_infoHeroPropATImg;
        }
        set
        {
          this.m_owner.m_infoHeroPropATImg = value;
        }
      }

      public Image m_infoHeroPropMagicDFImg
      {
        get
        {
          return this.m_owner.m_infoHeroPropMagicDFImg;
        }
        set
        {
          this.m_owner.m_infoHeroPropMagicDFImg = value;
        }
      }

      public Image m_infoHeroPropMagicImg
      {
        get
        {
          return this.m_owner.m_infoHeroPropMagicImg;
        }
        set
        {
          this.m_owner.m_infoHeroPropMagicImg = value;
        }
      }

      public Image m_infoHeroPropDEXImg
      {
        get
        {
          return this.m_owner.m_infoHeroPropDEXImg;
        }
        set
        {
          this.m_owner.m_infoHeroPropDEXImg = value;
        }
      }

      public Text m_infoHeroPropHPValueText
      {
        get
        {
          return this.m_owner.m_infoHeroPropHPValueText;
        }
        set
        {
          this.m_owner.m_infoHeroPropHPValueText = value;
        }
      }

      public Text m_infoHeroPropDFValueText
      {
        get
        {
          return this.m_owner.m_infoHeroPropDFValueText;
        }
        set
        {
          this.m_owner.m_infoHeroPropDFValueText = value;
        }
      }

      public Text m_infoHeroPropATValueText
      {
        get
        {
          return this.m_owner.m_infoHeroPropATValueText;
        }
        set
        {
          this.m_owner.m_infoHeroPropATValueText = value;
        }
      }

      public Text m_infoHeroPropMagicDFValueText
      {
        get
        {
          return this.m_owner.m_infoHeroPropMagicDFValueText;
        }
        set
        {
          this.m_owner.m_infoHeroPropMagicDFValueText = value;
        }
      }

      public Text m_infoHeroPropMagicValueText
      {
        get
        {
          return this.m_owner.m_infoHeroPropMagicValueText;
        }
        set
        {
          this.m_owner.m_infoHeroPropMagicValueText = value;
        }
      }

      public Text m_infoHeroPropDEXValueText
      {
        get
        {
          return this.m_owner.m_infoHeroPropDEXValueText;
        }
        set
        {
          this.m_owner.m_infoHeroPropDEXValueText = value;
        }
      }

      public Text m_infoHeroPropHPAddText
      {
        get
        {
          return this.m_owner.m_infoHeroPropHPAddText;
        }
        set
        {
          this.m_owner.m_infoHeroPropHPAddText = value;
        }
      }

      public Text m_infoHeroPropDFAddText
      {
        get
        {
          return this.m_owner.m_infoHeroPropDFAddText;
        }
        set
        {
          this.m_owner.m_infoHeroPropDFAddText = value;
        }
      }

      public Text m_infoHeroPropATAddText
      {
        get
        {
          return this.m_owner.m_infoHeroPropATAddText;
        }
        set
        {
          this.m_owner.m_infoHeroPropATAddText = value;
        }
      }

      public Text m_infoHeroPropMagicDFAddText
      {
        get
        {
          return this.m_owner.m_infoHeroPropMagicDFAddText;
        }
        set
        {
          this.m_owner.m_infoHeroPropMagicDFAddText = value;
        }
      }

      public Text m_infoHeroPropMagicAddText
      {
        get
        {
          return this.m_owner.m_infoHeroPropMagicAddText;
        }
        set
        {
          this.m_owner.m_infoHeroPropMagicAddText = value;
        }
      }

      public Text m_infoHeroPropDEXAddText
      {
        get
        {
          return this.m_owner.m_infoHeroPropDEXAddText;
        }
        set
        {
          this.m_owner.m_infoHeroPropDEXAddText = value;
        }
      }

      public Button m_talentSkillButton
      {
        get
        {
          return this.m_owner.m_talentSkillButton;
        }
        set
        {
          this.m_owner.m_talentSkillButton = value;
        }
      }

      public Image m_infoHeroTalentIcon
      {
        get
        {
          return this.m_owner.m_infoHeroTalentIcon;
        }
        set
        {
          this.m_owner.m_infoHeroTalentIcon = value;
        }
      }

      public GameObject m_skillTotalCost
      {
        get
        {
          return this.m_owner.m_skillTotalCost;
        }
        set
        {
          this.m_owner.m_skillTotalCost = value;
        }
      }

      public GameObject m_curSkillContent
      {
        get
        {
          return this.m_owner.m_curSkillContent;
        }
        set
        {
          this.m_owner.m_curSkillContent = value;
        }
      }

      public Button m_skillChangeButton
      {
        get
        {
          return this.m_owner.m_skillChangeButton;
        }
        set
        {
          this.m_owner.m_skillChangeButton = value;
        }
      }

      public GameObject m_skillItemPrefab
      {
        get
        {
          return this.m_owner.m_skillItemPrefab;
        }
        set
        {
          this.m_owner.m_skillItemPrefab = value;
        }
      }

      public GameObject m_SkillItemDetailDummy
      {
        get
        {
          return this.m_owner.m_SkillItemDetailDummy;
        }
        set
        {
          this.m_owner.m_SkillItemDetailDummy = value;
        }
      }

      public GameObject m_equipItemPrefab
      {
        get
        {
          return this.m_owner.m_equipItemPrefab;
        }
        set
        {
          this.m_owner.m_equipItemPrefab = value;
        }
      }

      public GameObject m_equipItem1
      {
        get
        {
          return this.m_owner.m_equipItem1;
        }
        set
        {
          this.m_owner.m_equipItem1 = value;
        }
      }

      public GameObject m_equipItem2
      {
        get
        {
          return this.m_owner.m_equipItem2;
        }
        set
        {
          this.m_owner.m_equipItem2 = value;
        }
      }

      public GameObject m_equipItem3
      {
        get
        {
          return this.m_owner.m_equipItem3;
        }
        set
        {
          this.m_owner.m_equipItem3 = value;
        }
      }

      public GameObject m_equipItem4
      {
        get
        {
          return this.m_owner.m_equipItem4;
        }
        set
        {
          this.m_owner.m_equipItem4 = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public PeakArenaHeroDetailUITask m_peakArenaHeroDetailUITask
      {
        get
        {
          return this.m_owner.m_peakArenaHeroDetailUITask;
        }
        set
        {
          this.m_owner.m_peakArenaHeroDetailUITask = value;
        }
      }

      public PeakArenaManagementTeamUIController.HeroWrap m_heroWrap
      {
        get
        {
          return this.m_owner.m_heroWrap;
        }
        set
        {
          this.m_owner.m_heroWrap = value;
        }
      }

      public UISpineGraphic m_playerHeroGraphic
      {
        get
        {
          return this.m_owner.m_playerHeroGraphic;
        }
        set
        {
          this.m_owner.m_playerHeroGraphic = value;
        }
      }

      public HeroDetailSelectSkillUIController m_heroDetailSelectSkillUIController
      {
        get
        {
          return this.m_owner.m_heroDetailSelectSkillUIController;
        }
        set
        {
          this.m_owner.m_heroDetailSelectSkillUIController = value;
        }
      }

      public SkillDescUIController m_skillDescUIController
      {
        get
        {
          return this.m_owner.m_skillDescUIController;
        }
        set
        {
          this.m_owner.m_skillDescUIController = value;
        }
      }

      public PeakArenaSoldierUIController m_heroDetailSoldierUIController
      {
        get
        {
          return this.m_owner.m_heroDetailSoldierUIController;
        }
        set
        {
          this.m_owner.m_heroDetailSoldierUIController = value;
        }
      }

      public PeakArenaEquipmentDetailUIController m_equipmentDetailUIController
      {
        get
        {
          return this.m_owner.m_equipmentDetailUIController;
        }
        set
        {
          this.m_owner.m_equipmentDetailUIController = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void UpdateView(
        PeakArenaManagementTeamUIController.HeroWrap heroWrap)
      {
        this.m_owner.UpdateView(heroWrap);
      }

      public void UpdateViewBaseInfo()
      {
        this.m_owner.UpdateViewBaseInfo();
      }

      public void UpdateHeroProperty(
        PeakArenaManagementTeamUIController.HeroWrap heroWrap)
      {
        this.m_owner.UpdateHeroProperty(heroWrap);
      }

      public void UpdateHeroSkill(Hero hero)
      {
        this.m_owner.UpdateHeroSkill(hero);
      }

      public void UpdateHeroEquipment()
      {
        this.m_owner.UpdateHeroEquipment();
      }

      public void UpdateHeroSingleEquipment(ulong id, GameObject parent)
      {
        this.m_owner.UpdateHeroSingleEquipment(id, parent);
      }

      public void UpdateSoldier(Hero hero)
      {
        this.m_owner.UpdateSoldier(hero);
      }

      public void CreateSpineGraphic()
      {
        this.m_owner.CreateSpineGraphic();
      }

      public void OnCloseClick()
      {
        this.m_owner.OnCloseClick();
      }

      public void OnSkillChangeClick()
      {
        this.m_owner.OnSkillChangeClick();
      }

      public void OnHeroSkillsSelect(int heroId, List<int> skillIds, bool isSkillChanged)
      {
        this.m_owner.OnHeroSkillsSelect(heroId, skillIds, isSkillChanged);
      }

      public void OnSkillItemClick(
        PeakArenaSkillItemUIController peakArenaSkillItemUIController)
      {
        this.m_owner.OnSkillItemClick(peakArenaSkillItemUIController);
      }

      public void OnTalentSkillItemClick()
      {
        this.m_owner.OnTalentSkillItemClick();
      }

      public void OnItemDetailClick(
        PeakArenaEquipItemUIController equipItemUIController)
      {
        this.m_owner.OnItemDetailClick(equipItemUIController);
      }

      public void HeroDetailSoldierUIController_OnHeroSoldierSelect(
        int heroId,
        int soldierId,
        Action OnSucceed)
      {
        this.m_owner.HeroDetailSoldierUIController_OnHeroSoldierSelect(heroId, soldierId, OnSucceed);
      }

      public void HeroDetailSoldierUIController_OnGotoDrill(int techId)
      {
        this.m_owner.HeroDetailSoldierUIController_OnGotoDrill(techId);
      }

      public void HeroDetailSoldierUIController_OnGotoJobTransfer(
        ConfigDataJobConnectionInfo jobConnectionInfo)
      {
        this.m_owner.HeroDetailSoldierUIController_OnGotoJobTransfer(jobConnectionInfo);
      }

      public void HeroDetailSoldierUIController_OnSkinInfoButtonClick(
        ConfigDataSoldierInfo soldierInfo)
      {
        this.m_owner.HeroDetailSoldierUIController_OnSkinInfoButtonClick(soldierInfo);
      }
    }
  }
}
