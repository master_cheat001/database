﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroOrSoliderSkinUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.Scene;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class HeroOrSoliderSkinUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_stateCtrl;
    [AutoBind("./Select", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_selectImage;
    [AutoBind("./Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameText;
    [AutoBind("./Char", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_charGameObject;
    [AutoBind("./LimitTime", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_timeGo;
    [AutoBind("./LimitTime/TimeValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_timeValueText;
    private UISpineGraphic m_playerHeroGraphic;
    private UISpineGraphic m_soldierInfoGraphic;
    private Hero m_hero;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    [DoNotToLua]
    private HeroOrSoliderSkinUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_InitHeroSkinItemHeroConfigDataJobConnectionInfoInt32_hotfix;
    private LuaFunction m_InitDefaultHeroSkinItemHero_hotfix;
    private LuaFunction m_InitSoldierSkinItemSoldier2SkinResourceConfigDataSoldierSkinInfoHero_hotfix;
    private LuaFunction m_InitDefaultSoldierSkinItemHeroConfigDataSoldierInfo_hotfix;
    private LuaFunction m_ShowSelectImageBoolean_hotfix;
    private LuaFunction m_OnHeroJobCardItemClick_hotfix;
    private LuaFunction m_SetLimitTime_hotfix;
    private LuaFunction m_add_EventOnItemClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnItemClickAction`1_hotfix;
    private LuaFunction m_set_JobConnectionInfoConfigDataJobConnectionInfo_hotfix;
    private LuaFunction m_get_JobConnectionInfo_hotfix;
    private LuaFunction m_set_HeroSkinInfoConfigDataHeroSkinInfo_hotfix;
    private LuaFunction m_get_HeroSkinInfo_hotfix;
    private LuaFunction m_set_ModelSkinResourceInfoConfigDataModelSkinResourceInfo_hotfix;
    private LuaFunction m_get_ModelSkinResourceInfo_hotfix;
    private LuaFunction m_get_SoldierInfo_hotfix;
    private LuaFunction m_set_SoldierInfoConfigDataSoldierInfo_hotfix;
    private LuaFunction m_get_SoldierSkinInfo_hotfix;
    private LuaFunction m_set_SoldierSkinInfoConfigDataSoldierSkinInfo_hotfix;
    private LuaFunction m_set_IsHeroSkinBoolean_hotfix;
    private LuaFunction m_get_IsHeroSkin_hotfix;
    private LuaFunction m_set_IsDefaultSkinBoolean_hotfix;
    private LuaFunction m_get_IsDefaultSkin_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitHeroSkinItem(
      Hero hero,
      ConfigDataJobConnectionInfo jobConnectionInfo,
      int skinId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitDefaultHeroSkinItem(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitSoldierSkinItem(
      Soldier2SkinResource item,
      ConfigDataSoldierSkinInfo soldierSkinInfo,
      Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitDefaultSoldierSkinItem(Hero hero, ConfigDataSoldierInfo soldierInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowSelectImage(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroJobCardItemClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLimitTime()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<HeroOrSoliderSkinUIController> EventOnItemClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataJobConnectionInfo JobConnectionInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataHeroSkinInfo HeroSkinInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataModelSkinResourceInfo ModelSkinResourceInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataSoldierInfo SoldierInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataSoldierSkinInfo SoldierSkinInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsHeroSkin
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsDefaultSkin
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public HeroOrSoliderSkinUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnItemClick(HeroOrSoliderSkinUIController obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnItemClick(HeroOrSoliderSkinUIController obj)
    {
      this.EventOnItemClick = (Action<HeroOrSoliderSkinUIController>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private HeroOrSoliderSkinUIController m_owner;

      public LuaExportHelper(HeroOrSoliderSkinUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnItemClick(HeroOrSoliderSkinUIController obj)
      {
        this.m_owner.__callDele_EventOnItemClick(obj);
      }

      public void __clearDele_EventOnItemClick(HeroOrSoliderSkinUIController obj)
      {
        this.m_owner.__clearDele_EventOnItemClick(obj);
      }

      public CommonUIStateController m_stateCtrl
      {
        get
        {
          return this.m_owner.m_stateCtrl;
        }
        set
        {
          this.m_owner.m_stateCtrl = value;
        }
      }

      public GameObject m_selectImage
      {
        get
        {
          return this.m_owner.m_selectImage;
        }
        set
        {
          this.m_owner.m_selectImage = value;
        }
      }

      public Text m_nameText
      {
        get
        {
          return this.m_owner.m_nameText;
        }
        set
        {
          this.m_owner.m_nameText = value;
        }
      }

      public GameObject m_charGameObject
      {
        get
        {
          return this.m_owner.m_charGameObject;
        }
        set
        {
          this.m_owner.m_charGameObject = value;
        }
      }

      public GameObject m_timeGo
      {
        get
        {
          return this.m_owner.m_timeGo;
        }
        set
        {
          this.m_owner.m_timeGo = value;
        }
      }

      public Text m_timeValueText
      {
        get
        {
          return this.m_owner.m_timeValueText;
        }
        set
        {
          this.m_owner.m_timeValueText = value;
        }
      }

      public UISpineGraphic m_playerHeroGraphic
      {
        get
        {
          return this.m_owner.m_playerHeroGraphic;
        }
        set
        {
          this.m_owner.m_playerHeroGraphic = value;
        }
      }

      public UISpineGraphic m_soldierInfoGraphic
      {
        get
        {
          return this.m_owner.m_soldierInfoGraphic;
        }
        set
        {
          this.m_owner.m_soldierInfoGraphic = value;
        }
      }

      public Hero m_hero
      {
        get
        {
          return this.m_owner.m_hero;
        }
        set
        {
          this.m_owner.m_hero = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public ConfigDataJobConnectionInfo JobConnectionInfo
      {
        set
        {
          this.m_owner.JobConnectionInfo = value;
        }
      }

      public ConfigDataHeroSkinInfo HeroSkinInfo
      {
        set
        {
          this.m_owner.HeroSkinInfo = value;
        }
      }

      public ConfigDataModelSkinResourceInfo ModelSkinResourceInfo
      {
        set
        {
          this.m_owner.ModelSkinResourceInfo = value;
        }
      }

      public ConfigDataSoldierInfo SoldierInfo
      {
        set
        {
          this.m_owner.SoldierInfo = value;
        }
      }

      public ConfigDataSoldierSkinInfo SoldierSkinInfo
      {
        set
        {
          this.m_owner.SoldierSkinInfo = value;
        }
      }

      public bool IsHeroSkin
      {
        set
        {
          this.m_owner.IsHeroSkin = value;
        }
      }

      public bool IsDefaultSkin
      {
        set
        {
          this.m_owner.IsDefaultSkin = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void OnHeroJobCardItemClick()
      {
        this.m_owner.OnHeroJobCardItemClick();
      }
    }
  }
}
