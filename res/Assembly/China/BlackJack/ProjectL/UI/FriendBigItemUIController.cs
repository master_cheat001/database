﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.FriendBigItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class FriendBigItemUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_itemButton;
    [AutoBind("./ChatButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_chatButton;
    [AutoBind("./ButtonGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_buttonGroupUIStateController;
    [AutoBind("./StateGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_friendOnlineUIStateController;
    [AutoBind("./PlayerIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_friendIconImage;
    [AutoBind("./PlayerIconImageGrey", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_friendIconGreyImage;
    [AutoBind("./HeadFrameDummy", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_friendHeadFrameTransform;
    [AutoBind("./PlayerNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_friendNameText;
    [AutoBind("./LevelText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_friendLevelText;
    [AutoBind("./ServerText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_friendServerText;
    [AutoBind("./StateGroup/OnLineText2", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_friendOnlineLongText;
    [AutoBind("./ButtonGroup/AddButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_addButton;
    [AutoBind("./ButtonGroup/UnblockButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_unblockButton;
    [AutoBind("./ButtonGroup/DeleteButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_kickButton;
    [AutoBind("./ButtonGroup/BigChatButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_bigChatButton;
    [AutoBind("./ButtonGroup/SendButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_sendButton;
    [AutoBind("./ButtonGroup/GetButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_getButton;
    [AutoBind("./ButtonGroup/DoneButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_doneButton;
    private string m_userID;
    private UserSummary m_userSummy;
    private IConfigDataLoader m_configDataLoader;
    private ProjectLPlayerContext m_playerContext;
    [DoNotToLua]
    private FriendBigItemUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_SetFriendInfoUserSummaryFriendInfoTypeBoolean_hotfix;
    private LuaFunction m_GetServerNameString_hotfix;
    private LuaFunction m_GetUserID_hotfix;
    private LuaFunction m_GetUserSummy_hotfix;
    private LuaFunction m_GetSimpleInfoPostionType_hotfix;
    private LuaFunction m_GetPlayerSimpleInfoPos_hotfix;
    private LuaFunction m_OnItemButtonClick_hotfix;
    private LuaFunction m_OnAddButtonClick_hotfix;
    private LuaFunction m_OnKickButtonClick_hotfix;
    private LuaFunction m_OnUnblockButtonClick_hotfix;
    private LuaFunction m_OnChatButtonClick_hotfix;
    private LuaFunction m_OnSendButtonClick_hotfix;
    private LuaFunction m_OnGetButtonClick_hotfix;
    private LuaFunction m_OnDoneButtonClick_hotfix;
    private LuaFunction m_add_EventOnShowPlayerSimpleInfoAction`1_hotfix;
    private LuaFunction m_remove_EventOnShowPlayerSimpleInfoAction`1_hotfix;
    private LuaFunction m_add_EventOnAddAction`1_hotfix;
    private LuaFunction m_remove_EventOnAddAction`1_hotfix;
    private LuaFunction m_add_EventOnKickFromGroupAction`1_hotfix;
    private LuaFunction m_remove_EventOnKickFromGroupAction`1_hotfix;
    private LuaFunction m_add_EventOnUnblockAction`1_hotfix;
    private LuaFunction m_remove_EventOnUnblockAction`1_hotfix;
    private LuaFunction m_add_EventOnChatAction`1_hotfix;
    private LuaFunction m_remove_EventOnChatAction`1_hotfix;
    private LuaFunction m_add_EventOnSendFriendShipPointsAction`1_hotfix;
    private LuaFunction m_remove_EventOnSendFriendShipPointsAction`1_hotfix;
    private LuaFunction m_add_EventOnGetFriendShipPointsAction`1_hotfix;
    private LuaFunction m_remove_EventOnGetFriendShipPointsAction`1_hotfix;
    private LuaFunction m_add_EventOnFriendShipPointsDoneAction`1_hotfix;
    private LuaFunction m_remove_EventOnFriendShipPointsDoneAction`1_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public FriendBigItemUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetFriendInfo(
      UserSummary userSummy,
      FriendInfoType friendInfoType,
      bool isChatGroupOwner = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string GetServerName(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetUserID()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public UserSummary GetUserSummy()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PlayerSimpleInfoUITask.PostionType GetSimpleInfoPostionType()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Vector3 GetPlayerSimpleInfoPos()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnItemButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnKickButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnUnblockButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChatButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSendButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGetButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDoneButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<FriendBigItemUIController> EventOnShowPlayerSimpleInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<FriendBigItemUIController> EventOnAdd
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<FriendBigItemUIController> EventOnKickFromGroup
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<FriendBigItemUIController> EventOnUnblock
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<FriendBigItemUIController> EventOnChat
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<FriendBigItemUIController> EventOnSendFriendShipPoints
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<FriendBigItemUIController> EventOnGetFriendShipPoints
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<FriendBigItemUIController> EventOnFriendShipPointsDone
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public FriendBigItemUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowPlayerSimpleInfo(FriendBigItemUIController obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowPlayerSimpleInfo(FriendBigItemUIController obj)
    {
      this.EventOnShowPlayerSimpleInfo = (Action<FriendBigItemUIController>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnAdd(FriendBigItemUIController obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnAdd(FriendBigItemUIController obj)
    {
      this.EventOnAdd = (Action<FriendBigItemUIController>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnKickFromGroup(FriendBigItemUIController obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnKickFromGroup(FriendBigItemUIController obj)
    {
      this.EventOnKickFromGroup = (Action<FriendBigItemUIController>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnUnblock(FriendBigItemUIController obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnUnblock(FriendBigItemUIController obj)
    {
      this.EventOnUnblock = (Action<FriendBigItemUIController>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnChat(FriendBigItemUIController obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnChat(FriendBigItemUIController obj)
    {
      this.EventOnChat = (Action<FriendBigItemUIController>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnSendFriendShipPoints(FriendBigItemUIController obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnSendFriendShipPoints(FriendBigItemUIController obj)
    {
      this.EventOnSendFriendShipPoints = (Action<FriendBigItemUIController>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnGetFriendShipPoints(FriendBigItemUIController obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnGetFriendShipPoints(FriendBigItemUIController obj)
    {
      this.EventOnGetFriendShipPoints = (Action<FriendBigItemUIController>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnFriendShipPointsDone(FriendBigItemUIController obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnFriendShipPointsDone(FriendBigItemUIController obj)
    {
      this.EventOnFriendShipPointsDone = (Action<FriendBigItemUIController>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private FriendBigItemUIController m_owner;

      public LuaExportHelper(FriendBigItemUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnShowPlayerSimpleInfo(FriendBigItemUIController obj)
      {
        this.m_owner.__callDele_EventOnShowPlayerSimpleInfo(obj);
      }

      public void __clearDele_EventOnShowPlayerSimpleInfo(FriendBigItemUIController obj)
      {
        this.m_owner.__clearDele_EventOnShowPlayerSimpleInfo(obj);
      }

      public void __callDele_EventOnAdd(FriendBigItemUIController obj)
      {
        this.m_owner.__callDele_EventOnAdd(obj);
      }

      public void __clearDele_EventOnAdd(FriendBigItemUIController obj)
      {
        this.m_owner.__clearDele_EventOnAdd(obj);
      }

      public void __callDele_EventOnKickFromGroup(FriendBigItemUIController obj)
      {
        this.m_owner.__callDele_EventOnKickFromGroup(obj);
      }

      public void __clearDele_EventOnKickFromGroup(FriendBigItemUIController obj)
      {
        this.m_owner.__clearDele_EventOnKickFromGroup(obj);
      }

      public void __callDele_EventOnUnblock(FriendBigItemUIController obj)
      {
        this.m_owner.__callDele_EventOnUnblock(obj);
      }

      public void __clearDele_EventOnUnblock(FriendBigItemUIController obj)
      {
        this.m_owner.__clearDele_EventOnUnblock(obj);
      }

      public void __callDele_EventOnChat(FriendBigItemUIController obj)
      {
        this.m_owner.__callDele_EventOnChat(obj);
      }

      public void __clearDele_EventOnChat(FriendBigItemUIController obj)
      {
        this.m_owner.__clearDele_EventOnChat(obj);
      }

      public void __callDele_EventOnSendFriendShipPoints(FriendBigItemUIController obj)
      {
        this.m_owner.__callDele_EventOnSendFriendShipPoints(obj);
      }

      public void __clearDele_EventOnSendFriendShipPoints(FriendBigItemUIController obj)
      {
        this.m_owner.__clearDele_EventOnSendFriendShipPoints(obj);
      }

      public void __callDele_EventOnGetFriendShipPoints(FriendBigItemUIController obj)
      {
        this.m_owner.__callDele_EventOnGetFriendShipPoints(obj);
      }

      public void __clearDele_EventOnGetFriendShipPoints(FriendBigItemUIController obj)
      {
        this.m_owner.__clearDele_EventOnGetFriendShipPoints(obj);
      }

      public void __callDele_EventOnFriendShipPointsDone(FriendBigItemUIController obj)
      {
        this.m_owner.__callDele_EventOnFriendShipPointsDone(obj);
      }

      public void __clearDele_EventOnFriendShipPointsDone(FriendBigItemUIController obj)
      {
        this.m_owner.__clearDele_EventOnFriendShipPointsDone(obj);
      }

      public Button m_itemButton
      {
        get
        {
          return this.m_owner.m_itemButton;
        }
        set
        {
          this.m_owner.m_itemButton = value;
        }
      }

      public Button m_chatButton
      {
        get
        {
          return this.m_owner.m_chatButton;
        }
        set
        {
          this.m_owner.m_chatButton = value;
        }
      }

      public CommonUIStateController m_buttonGroupUIStateController
      {
        get
        {
          return this.m_owner.m_buttonGroupUIStateController;
        }
        set
        {
          this.m_owner.m_buttonGroupUIStateController = value;
        }
      }

      public CommonUIStateController m_friendOnlineUIStateController
      {
        get
        {
          return this.m_owner.m_friendOnlineUIStateController;
        }
        set
        {
          this.m_owner.m_friendOnlineUIStateController = value;
        }
      }

      public Image m_friendIconImage
      {
        get
        {
          return this.m_owner.m_friendIconImage;
        }
        set
        {
          this.m_owner.m_friendIconImage = value;
        }
      }

      public Image m_friendIconGreyImage
      {
        get
        {
          return this.m_owner.m_friendIconGreyImage;
        }
        set
        {
          this.m_owner.m_friendIconGreyImage = value;
        }
      }

      public Transform m_friendHeadFrameTransform
      {
        get
        {
          return this.m_owner.m_friendHeadFrameTransform;
        }
        set
        {
          this.m_owner.m_friendHeadFrameTransform = value;
        }
      }

      public Text m_friendNameText
      {
        get
        {
          return this.m_owner.m_friendNameText;
        }
        set
        {
          this.m_owner.m_friendNameText = value;
        }
      }

      public Text m_friendLevelText
      {
        get
        {
          return this.m_owner.m_friendLevelText;
        }
        set
        {
          this.m_owner.m_friendLevelText = value;
        }
      }

      public Text m_friendServerText
      {
        get
        {
          return this.m_owner.m_friendServerText;
        }
        set
        {
          this.m_owner.m_friendServerText = value;
        }
      }

      public Text m_friendOnlineLongText
      {
        get
        {
          return this.m_owner.m_friendOnlineLongText;
        }
        set
        {
          this.m_owner.m_friendOnlineLongText = value;
        }
      }

      public Button m_addButton
      {
        get
        {
          return this.m_owner.m_addButton;
        }
        set
        {
          this.m_owner.m_addButton = value;
        }
      }

      public Button m_unblockButton
      {
        get
        {
          return this.m_owner.m_unblockButton;
        }
        set
        {
          this.m_owner.m_unblockButton = value;
        }
      }

      public Button m_kickButton
      {
        get
        {
          return this.m_owner.m_kickButton;
        }
        set
        {
          this.m_owner.m_kickButton = value;
        }
      }

      public Button m_bigChatButton
      {
        get
        {
          return this.m_owner.m_bigChatButton;
        }
        set
        {
          this.m_owner.m_bigChatButton = value;
        }
      }

      public Button m_sendButton
      {
        get
        {
          return this.m_owner.m_sendButton;
        }
        set
        {
          this.m_owner.m_sendButton = value;
        }
      }

      public Button m_getButton
      {
        get
        {
          return this.m_owner.m_getButton;
        }
        set
        {
          this.m_owner.m_getButton = value;
        }
      }

      public Button m_doneButton
      {
        get
        {
          return this.m_owner.m_doneButton;
        }
        set
        {
          this.m_owner.m_doneButton = value;
        }
      }

      public string m_userID
      {
        get
        {
          return this.m_owner.m_userID;
        }
        set
        {
          this.m_owner.m_userID = value;
        }
      }

      public UserSummary m_userSummy
      {
        get
        {
          return this.m_owner.m_userSummy;
        }
        set
        {
          this.m_owner.m_userSummy = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public string GetServerName(string userId)
      {
        return this.m_owner.GetServerName(userId);
      }

      public void OnItemButtonClick()
      {
        this.m_owner.OnItemButtonClick();
      }

      public void OnAddButtonClick()
      {
        this.m_owner.OnAddButtonClick();
      }

      public void OnKickButtonClick()
      {
        this.m_owner.OnKickButtonClick();
      }

      public void OnUnblockButtonClick()
      {
        this.m_owner.OnUnblockButtonClick();
      }

      public void OnChatButtonClick()
      {
        this.m_owner.OnChatButtonClick();
      }

      public void OnSendButtonClick()
      {
        this.m_owner.OnSendButtonClick();
      }

      public void OnGetButtonClick()
      {
        this.m_owner.OnGetButtonClick();
      }

      public void OnDoneButtonClick()
      {
        this.m_owner.OnDoneButtonClick();
      }
    }
  }
}
