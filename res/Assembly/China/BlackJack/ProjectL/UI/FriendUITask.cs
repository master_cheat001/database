﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.FriendUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.Protocol;
using BlackJack.ProjectLBasic;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class FriendUITask : UITask
  {
    public const string ParamKey_OpenAddFriend = "OpenAddFriend";
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private PlayerSimpleInfoUITask m_playerSimpleInfoUITask;
    private FriendUIController m_friendUIController;
    private FriendPanelType m_panelType;
    private List<string> m_invitedRecommendFriendIDList;
    private List<UserSummary> m_recommendFriendList;
    private List<UserSummary> m_findFriendList;
    private List<string> m_sendFriendshipPointUserIDList;
    private List<string> m_getFriendshipPointUserIDList;
    private string m_unblockPlayerUserID;
    private string m_kickPlayerUserID;
    private string m_kickPlayerUserName;
    private ProChatGroupInfo m_currentGroupInfo;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    [DoNotToLua]
    private FriendUITask.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctorString_hotfix;
    private LuaFunction m_InitAllUIControllers_hotfix;
    private LuaFunction m_ClearAllContextAndRes_hotfix;
    private LuaFunction m_RegisterPlayerContextEvents_hotfix;
    private LuaFunction m_UnregisterPlayerContextEvents_hotfix;
    private LuaFunction m_UpdateView_hotfix;
    private LuaFunction m_PostUpdateView_hotfix;
    private LuaFunction m_UpdatePanel_hotfix;
    private LuaFunction m_FriendUIController_OnShowPanelFriendPanelType_hotfix;
    private LuaFunction m_FriendUIController_OnReturn_hotfix;
    private LuaFunction m_FriendUIController_OnShowHelp_hotfix;
    private LuaFunction m_FriendUIController_OnGetSocialRelationFriendSocialRelationFlag_hotfix;
    private LuaFunction m_FriendUIController_OnShowPlayerInfoUserSummaryVector3PostionType_hotfix;
    private LuaFunction m_OnShowPlayerSimpleInfoVector3PostionType_hotfix;
    private LuaFunction m_PlayerSimpleInfoUITask_GetSocialRelation_hotfix;
    private LuaFunction m_PlayerSimpleInfoUITask_OnClose_hotfix;
    private LuaFunction m_PlayerSimpleInfoUITask_OnPrivateChatButtonClickBusinessCard_hotfix;
    private LuaFunction m_FriendUIController_OnAddFriendList`1_hotfix;
    private LuaFunction m_FriendUIController_OnFriendInviteAceeptList`1_hotfix;
    private LuaFunction m_FriendUIController_OnFriendInviteDeclineList`1_hotfix;
    private LuaFunction m_FriendUIController_OnUnblockPlayerString_hotfix;
    private LuaFunction m_UnblockPlayerCallbackDialogBoxResult_hotfix;
    private LuaFunction m_FriendUIController_OnFindFriendInt32String_hotfix;
    private LuaFunction m_SetCurrentFindFriendList_hotfix;
    private LuaFunction m_FriendUIController_OnGetRecommendFriendList_hotfix;
    private LuaFunction m_SetCurrentRecommedFriendList_hotfix;
    private LuaFunction m_FriendUIController_OnGetAllGroup_hotfix;
    private LuaFunction m_FriendUIController_OnGroupChatString_hotfix;
    private LuaFunction m_FriendUIController_OnWatchGroupStaffStringBoolean_hotfix;
    private LuaFunction m_FriendUIController_OnCreateNewGroupList`1_hotfix;
    private LuaFunction m_FriendUIController_OnInviteFriendToGroupStringList`1_hotfix;
    private LuaFunction m_FriendUIController_OnChangeGroupNameStringString_hotfix;
    private LuaFunction m_FriendUIController_OnLeaveGroupProChatGroupInfo_hotfix;
    private LuaFunction m_OnLeaveGroupDialogBoxCallbackDialogBoxResult_hotfix;
    private LuaFunction m_FriendUIController_OnKickGroupProChatGroupInfoUserSummary_hotfix;
    private LuaFunction m_OnKickFromGroupDialogBoxCallbackDialogBoxResult_hotfix;
    private LuaFunction m_FriendUIController_OnChatUserSummary_hotfix;
    private LuaFunction m_FriendUIController_OnSendFriendshipPointString_hotfix;
    private LuaFunction m_FriendUIController_OnGetFriendshipPointString_hotfix;
    private LuaFunction m_FriendUIController_OnSendAllFriendshipPoint_hotfix;
    private LuaFunction m_FriendUIController_OnGetAllFriendshipPoint_hotfix;
    private LuaFunction m_PlayerContext_OnChatGroupUpdateNtfProChatGroupInfo_hotfix;
    private LuaFunction m_isMeInTheGroupProChatGroupInfo_hotfix;
    private LuaFunction m_ShowErrorMessageInt32_hotfix;
    private LuaFunction m_ShowMessageString_hotfix;
    private LuaFunction m_get_LayerDescArray_hotfix;
    private LuaFunction m_get_UICtrlDescArray_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public FriendUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterPlayerContextEvents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterPlayerContextEvents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void PostUpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdatePanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FriendUIController_OnShowPanel(FriendPanelType panelType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FriendUIController_OnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FriendUIController_OnShowHelp()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FriendUIController_OnGetSocialRelation(
      FriendSocialRelationFlag friendSocialRelationFlag = FriendSocialRelationFlag.All)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FriendUIController_OnShowPlayerInfo(
      UserSummary userSummy,
      Vector3 pos,
      PlayerSimpleInfoUITask.PostionType posType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnShowPlayerSimpleInfo(Vector3 pos, PlayerSimpleInfoUITask.PostionType posType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerSimpleInfoUITask_GetSocialRelation()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerSimpleInfoUITask_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerSimpleInfoUITask_OnPrivateChatButtonClick(BusinessCard playerInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FriendUIController_OnAddFriend(List<string> userIDList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FriendUIController_OnFriendInviteAceept(List<string> userIDList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FriendUIController_OnFriendInviteDecline(List<string> userIDList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FriendUIController_OnUnblockPlayer(string userID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UnblockPlayerCallback(DialogBoxResult r)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FriendUIController_OnFindFriend(int bornChannelID, string partialName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetCurrentFindFriendList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FriendUIController_OnGetRecommendFriendList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetCurrentRecommedFriendList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FriendUIController_OnGetAllGroup()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FriendUIController_OnGroupChat(string groupID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FriendUIController_OnWatchGroupStaff(string groupID, bool needOpenTween)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FriendUIController_OnCreateNewGroup(List<string> userIDList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FriendUIController_OnInviteFriendToGroup(string groupID, List<string> userIDList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FriendUIController_OnChangeGroupName(string groupID, string newName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FriendUIController_OnLeaveGroup(ProChatGroupInfo chatGroupInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnLeaveGroupDialogBoxCallback(DialogBoxResult r)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FriendUIController_OnKickGroup(
      ProChatGroupInfo chatGroupInfo,
      UserSummary userSummy)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnKickFromGroupDialogBoxCallback(DialogBoxResult r)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FriendUIController_OnChat(UserSummary userSummy)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FriendUIController_OnSendFriendshipPoint(string userID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FriendUIController_OnGetFriendshipPoint(string userID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FriendUIController_OnSendAllFriendshipPoint()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FriendUIController_OnGetAllFriendshipPoint()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnChatGroupUpdateNtf(ProChatGroupInfo chatGroupInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool isMeInTheGroup(ProChatGroupInfo chatGroupInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowErrorMessage(int errorCode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowMessage(string text)
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public FriendUITask.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private bool __callBase_OnStart(UIIntent intent)
    {
      return this.OnStart(intent);
    }

    private void __callBase_OnStop()
    {
      this.OnStop();
    }

    private void __callBase_OnPause()
    {
      this.OnPause();
    }

    private bool __callBase_OnResume(UIIntent intent)
    {
      return this.OnResume(intent);
    }

    private bool __callBase_OnNewIntent(UIIntent intent)
    {
      return this.OnNewIntent(intent);
    }

    private void __callBase_EnableUIInput(bool isEnable, bool? isGlobalEnable)
    {
      this.EnableUIInput(isEnable, isGlobalEnable);
    }

    private bool __callBase_IsNeedLoadDynamicRes()
    {
      return this.IsNeedLoadDynamicRes();
    }

    private List<string> __callBase_CollectAllDynamicResForLoad()
    {
      return this.CollectAllDynamicResForLoad();
    }

    private void __callBase_RegisterPlayerContextEvents()
    {
      base.RegisterPlayerContextEvents();
    }

    private void __callBase_UnregisterPlayerContextEvents()
    {
      base.UnregisterPlayerContextEvents();
    }

    private void __callBase_PostUpdateView()
    {
      base.PostUpdateView();
    }

    private void __callBase_CollectPreloadResourceList()
    {
      this.CollectPreloadResourceList();
    }

    private void __callBase_ClearAssetList()
    {
      this.ClearAssetList();
    }

    private void __callBase_CollectAsset(string assetName)
    {
      this.CollectAsset(assetName);
    }

    private void __callBase_CollectSpriteAsset(string assetName)
    {
      this.CollectSpriteAsset(assetName);
    }

    private void __callBase_CollectFxAsset(string assetName)
    {
      this.CollectFxAsset(assetName);
    }

    private void __callBase_StartLoadCustomAssets(Action onLoadCompleted)
    {
      this.StartLoadCustomAssets(onLoadCompleted);
    }

    private bool __callBase_IsLoadingCustomAssets()
    {
      return this.IsLoadingCustomAssets();
    }

    private bool __callBase_IsPipeLineRunning()
    {
      return this.IsPipeLineRunning();
    }

    private bool __callBase_IsOpeningUI()
    {
      return this.IsOpeningUI();
    }

    private UITaskBase __callBase_ReturnPrevUITask()
    {
      return this.ReturnPrevUITask();
    }

    private void __callBase_ClearUnusedDynamicResourceCache()
    {
      this.ClearUnusedDynamicResourceCache();
    }

    private void __callBase_ClearDynamicResourceCache(List<string> assets)
    {
      this.ClearDynamicResourceCache(assets);
    }

    private void __callBase_OnMemoryWarning()
    {
      this.OnMemoryWarning();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private FriendUITask m_owner;

      public LuaExportHelper(FriendUITask owner)
      {
        this.m_owner = owner;
      }

      public bool __callBase_OnStart(UIIntent intent)
      {
        return this.m_owner.__callBase_OnStart(intent);
      }

      public void __callBase_OnStop()
      {
        this.m_owner.__callBase_OnStop();
      }

      public void __callBase_OnPause()
      {
        this.m_owner.__callBase_OnPause();
      }

      public bool __callBase_OnResume(UIIntent intent)
      {
        return this.m_owner.__callBase_OnResume(intent);
      }

      public bool __callBase_OnNewIntent(UIIntent intent)
      {
        return this.m_owner.__callBase_OnNewIntent(intent);
      }

      public void __callBase_EnableUIInput(bool isEnable, bool? isGlobalEnable)
      {
        this.m_owner.__callBase_EnableUIInput(isEnable, isGlobalEnable);
      }

      public bool __callBase_IsNeedLoadDynamicRes()
      {
        return this.m_owner.__callBase_IsNeedLoadDynamicRes();
      }

      public List<string> __callBase_CollectAllDynamicResForLoad()
      {
        return this.m_owner.__callBase_CollectAllDynamicResForLoad();
      }

      public void __callBase_RegisterPlayerContextEvents()
      {
        this.m_owner.__callBase_RegisterPlayerContextEvents();
      }

      public void __callBase_UnregisterPlayerContextEvents()
      {
        this.m_owner.__callBase_UnregisterPlayerContextEvents();
      }

      public void __callBase_PostUpdateView()
      {
        this.m_owner.__callBase_PostUpdateView();
      }

      public void __callBase_CollectPreloadResourceList()
      {
        this.m_owner.__callBase_CollectPreloadResourceList();
      }

      public void __callBase_ClearAssetList()
      {
        this.m_owner.__callBase_ClearAssetList();
      }

      public void __callBase_CollectAsset(string assetName)
      {
        this.m_owner.__callBase_CollectAsset(assetName);
      }

      public void __callBase_CollectSpriteAsset(string assetName)
      {
        this.m_owner.__callBase_CollectSpriteAsset(assetName);
      }

      public void __callBase_CollectFxAsset(string assetName)
      {
        this.m_owner.__callBase_CollectFxAsset(assetName);
      }

      public void __callBase_StartLoadCustomAssets(Action onLoadCompleted)
      {
        this.m_owner.__callBase_StartLoadCustomAssets(onLoadCompleted);
      }

      public bool __callBase_IsLoadingCustomAssets()
      {
        return this.m_owner.__callBase_IsLoadingCustomAssets();
      }

      public bool __callBase_IsPipeLineRunning()
      {
        return this.m_owner.__callBase_IsPipeLineRunning();
      }

      public bool __callBase_IsOpeningUI()
      {
        return this.m_owner.__callBase_IsOpeningUI();
      }

      public UITaskBase __callBase_ReturnPrevUITask()
      {
        return this.m_owner.__callBase_ReturnPrevUITask();
      }

      public void __callBase_ClearUnusedDynamicResourceCache()
      {
        this.m_owner.__callBase_ClearUnusedDynamicResourceCache();
      }

      public void __callBase_ClearDynamicResourceCache(List<string> assets)
      {
        this.m_owner.__callBase_ClearDynamicResourceCache(assets);
      }

      public void __callBase_OnMemoryWarning()
      {
        this.m_owner.__callBase_OnMemoryWarning();
      }

      public UITaskBase.LayerDesc[] m_layerDescArray
      {
        get
        {
          return this.m_owner.m_layerDescArray;
        }
        set
        {
          this.m_owner.m_layerDescArray = value;
        }
      }

      public UITaskBase.UIControllerDesc[] m_uiCtrlDescArray
      {
        get
        {
          return this.m_owner.m_uiCtrlDescArray;
        }
        set
        {
          this.m_owner.m_uiCtrlDescArray = value;
        }
      }

      public PlayerSimpleInfoUITask m_playerSimpleInfoUITask
      {
        get
        {
          return this.m_owner.m_playerSimpleInfoUITask;
        }
        set
        {
          this.m_owner.m_playerSimpleInfoUITask = value;
        }
      }

      public FriendUIController m_friendUIController
      {
        get
        {
          return this.m_owner.m_friendUIController;
        }
        set
        {
          this.m_owner.m_friendUIController = value;
        }
      }

      public FriendPanelType m_panelType
      {
        get
        {
          return this.m_owner.m_panelType;
        }
        set
        {
          this.m_owner.m_panelType = value;
        }
      }

      public List<string> m_invitedRecommendFriendIDList
      {
        get
        {
          return this.m_owner.m_invitedRecommendFriendIDList;
        }
        set
        {
          this.m_owner.m_invitedRecommendFriendIDList = value;
        }
      }

      public List<UserSummary> m_recommendFriendList
      {
        get
        {
          return this.m_owner.m_recommendFriendList;
        }
        set
        {
          this.m_owner.m_recommendFriendList = value;
        }
      }

      public List<UserSummary> m_findFriendList
      {
        get
        {
          return this.m_owner.m_findFriendList;
        }
        set
        {
          this.m_owner.m_findFriendList = value;
        }
      }

      public List<string> m_sendFriendshipPointUserIDList
      {
        get
        {
          return this.m_owner.m_sendFriendshipPointUserIDList;
        }
        set
        {
          this.m_owner.m_sendFriendshipPointUserIDList = value;
        }
      }

      public List<string> m_getFriendshipPointUserIDList
      {
        get
        {
          return this.m_owner.m_getFriendshipPointUserIDList;
        }
        set
        {
          this.m_owner.m_getFriendshipPointUserIDList = value;
        }
      }

      public string m_unblockPlayerUserID
      {
        get
        {
          return this.m_owner.m_unblockPlayerUserID;
        }
        set
        {
          this.m_owner.m_unblockPlayerUserID = value;
        }
      }

      public string m_kickPlayerUserID
      {
        get
        {
          return this.m_owner.m_kickPlayerUserID;
        }
        set
        {
          this.m_owner.m_kickPlayerUserID = value;
        }
      }

      public string m_kickPlayerUserName
      {
        get
        {
          return this.m_owner.m_kickPlayerUserName;
        }
        set
        {
          this.m_owner.m_kickPlayerUserName = value;
        }
      }

      public ProChatGroupInfo m_currentGroupInfo
      {
        get
        {
          return this.m_owner.m_currentGroupInfo;
        }
        set
        {
          this.m_owner.m_currentGroupInfo = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public UITaskBase.LayerDesc[] LayerDescArray
      {
        get
        {
          return this.m_owner.LayerDescArray;
        }
      }

      public UITaskBase.UIControllerDesc[] UICtrlDescArray
      {
        get
        {
          return this.m_owner.UICtrlDescArray;
        }
      }

      public void InitAllUIControllers()
      {
        this.m_owner.InitAllUIControllers();
      }

      public void ClearAllContextAndRes()
      {
        this.m_owner.ClearAllContextAndRes();
      }

      public void RegisterPlayerContextEvents()
      {
        this.m_owner.RegisterPlayerContextEvents();
      }

      public void UnregisterPlayerContextEvents()
      {
        this.m_owner.UnregisterPlayerContextEvents();
      }

      public void UpdateView()
      {
        this.m_owner.UpdateView();
      }

      public void PostUpdateView()
      {
        this.m_owner.PostUpdateView();
      }

      public void UpdatePanel()
      {
        this.m_owner.UpdatePanel();
      }

      public void FriendUIController_OnShowPanel(FriendPanelType panelType)
      {
        this.m_owner.FriendUIController_OnShowPanel(panelType);
      }

      public void FriendUIController_OnReturn()
      {
        this.m_owner.FriendUIController_OnReturn();
      }

      public void FriendUIController_OnShowHelp()
      {
        this.m_owner.FriendUIController_OnShowHelp();
      }

      public void FriendUIController_OnGetSocialRelation(
        FriendSocialRelationFlag friendSocialRelationFlag)
      {
        this.m_owner.FriendUIController_OnGetSocialRelation(friendSocialRelationFlag);
      }

      public void FriendUIController_OnShowPlayerInfo(
        UserSummary userSummy,
        Vector3 pos,
        PlayerSimpleInfoUITask.PostionType posType)
      {
        this.m_owner.FriendUIController_OnShowPlayerInfo(userSummy, pos, posType);
      }

      public void OnShowPlayerSimpleInfo(Vector3 pos, PlayerSimpleInfoUITask.PostionType posType)
      {
        this.m_owner.OnShowPlayerSimpleInfo(pos, posType);
      }

      public void PlayerSimpleInfoUITask_GetSocialRelation()
      {
        this.m_owner.PlayerSimpleInfoUITask_GetSocialRelation();
      }

      public void PlayerSimpleInfoUITask_OnClose()
      {
        this.m_owner.PlayerSimpleInfoUITask_OnClose();
      }

      public void PlayerSimpleInfoUITask_OnPrivateChatButtonClick(BusinessCard playerInfo)
      {
        this.m_owner.PlayerSimpleInfoUITask_OnPrivateChatButtonClick(playerInfo);
      }

      public void FriendUIController_OnAddFriend(List<string> userIDList)
      {
        this.m_owner.FriendUIController_OnAddFriend(userIDList);
      }

      public void FriendUIController_OnFriendInviteAceept(List<string> userIDList)
      {
        this.m_owner.FriendUIController_OnFriendInviteAceept(userIDList);
      }

      public void FriendUIController_OnFriendInviteDecline(List<string> userIDList)
      {
        this.m_owner.FriendUIController_OnFriendInviteDecline(userIDList);
      }

      public void FriendUIController_OnUnblockPlayer(string userID)
      {
        this.m_owner.FriendUIController_OnUnblockPlayer(userID);
      }

      public void UnblockPlayerCallback(DialogBoxResult r)
      {
        this.m_owner.UnblockPlayerCallback(r);
      }

      public void FriendUIController_OnFindFriend(int bornChannelID, string partialName)
      {
        this.m_owner.FriendUIController_OnFindFriend(bornChannelID, partialName);
      }

      public void SetCurrentFindFriendList()
      {
        this.m_owner.SetCurrentFindFriendList();
      }

      public void FriendUIController_OnGetRecommendFriendList()
      {
        this.m_owner.FriendUIController_OnGetRecommendFriendList();
      }

      public void SetCurrentRecommedFriendList()
      {
        this.m_owner.SetCurrentRecommedFriendList();
      }

      public void FriendUIController_OnGetAllGroup()
      {
        this.m_owner.FriendUIController_OnGetAllGroup();
      }

      public void FriendUIController_OnGroupChat(string groupID)
      {
        this.m_owner.FriendUIController_OnGroupChat(groupID);
      }

      public void FriendUIController_OnWatchGroupStaff(string groupID, bool needOpenTween)
      {
        this.m_owner.FriendUIController_OnWatchGroupStaff(groupID, needOpenTween);
      }

      public void FriendUIController_OnCreateNewGroup(List<string> userIDList)
      {
        this.m_owner.FriendUIController_OnCreateNewGroup(userIDList);
      }

      public void FriendUIController_OnInviteFriendToGroup(string groupID, List<string> userIDList)
      {
        this.m_owner.FriendUIController_OnInviteFriendToGroup(groupID, userIDList);
      }

      public void FriendUIController_OnChangeGroupName(string groupID, string newName)
      {
        this.m_owner.FriendUIController_OnChangeGroupName(groupID, newName);
      }

      public void FriendUIController_OnLeaveGroup(ProChatGroupInfo chatGroupInfo)
      {
        this.m_owner.FriendUIController_OnLeaveGroup(chatGroupInfo);
      }

      public void OnLeaveGroupDialogBoxCallback(DialogBoxResult r)
      {
        this.m_owner.OnLeaveGroupDialogBoxCallback(r);
      }

      public void FriendUIController_OnKickGroup(
        ProChatGroupInfo chatGroupInfo,
        UserSummary userSummy)
      {
        this.m_owner.FriendUIController_OnKickGroup(chatGroupInfo, userSummy);
      }

      public void OnKickFromGroupDialogBoxCallback(DialogBoxResult r)
      {
        this.m_owner.OnKickFromGroupDialogBoxCallback(r);
      }

      public void FriendUIController_OnChat(UserSummary userSummy)
      {
        this.m_owner.FriendUIController_OnChat(userSummy);
      }

      public void FriendUIController_OnSendFriendshipPoint(string userID)
      {
        this.m_owner.FriendUIController_OnSendFriendshipPoint(userID);
      }

      public void FriendUIController_OnGetFriendshipPoint(string userID)
      {
        this.m_owner.FriendUIController_OnGetFriendshipPoint(userID);
      }

      public void FriendUIController_OnSendAllFriendshipPoint()
      {
        this.m_owner.FriendUIController_OnSendAllFriendshipPoint();
      }

      public void FriendUIController_OnGetAllFriendshipPoint()
      {
        this.m_owner.FriendUIController_OnGetAllFriendshipPoint();
      }

      public void PlayerContext_OnChatGroupUpdateNtf(ProChatGroupInfo chatGroupInfo)
      {
        this.m_owner.PlayerContext_OnChatGroupUpdateNtf(chatGroupInfo);
      }

      public bool isMeInTheGroup(ProChatGroupInfo chatGroupInfo)
      {
        return this.m_owner.isMeInTheGroup(chatGroupInfo);
      }

      public void ShowErrorMessage(int errorCode)
      {
        this.m_owner.ShowErrorMessage(errorCode);
      }

      public void ShowMessage(string text)
      {
        this.m_owner.ShowMessage(text);
      }
    }
  }
}
