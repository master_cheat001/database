﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.CommonUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class CommonUITask : UITask
  {
    private static CommonUITask s_instance;
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private CommonUIController m_commonUIController;
    private float m_sendHeartBeatTime;
    private DateTime m_nextCheckOnlineTime;
    private StringTableId m_disconnectedByServerMessageId;
    private float m_lastCheckClientVersionTime;
    private int m_netTaskRunningCount;
    public CommonUITask.CommonUIState m_state;
    [DoNotToLua]
    private CommonUITask.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctorString_hotfix;
    private LuaFunction m_Finalize_hotfix;
    private LuaFunction m_InitAllUIControllers_hotfix;
    private LuaFunction m_ClearAllContextAndRes_hotfix;
    private LuaFunction m_OnStartUIIntent_hotfix;
    private LuaFunction m_OnStop_hotfix;
    private LuaFunction m_OnTick_hotfix;
    private LuaFunction m_InitNetReq_hotfix;
    private LuaFunction m_TickCheckClientVersion_hotfix;
    private LuaFunction m_TickChatVoice_hotfix;
    private LuaFunction m_TickClientHeartBeat_hotfix;
    private LuaFunction m__TickCheckOnline_hotfix;
    private LuaFunction m_UpdateView_hotfix;
    private LuaFunction m_IsNeedLoadDynamicRes_hotfix;
    private LuaFunction m_PostUpdateView_hotfix;
    private LuaFunction m_Application_LogMessageReceivedStringStringLogType_hotfix;
    private LuaFunction m_PlayerContext_OnNewMarqueeNtfMarquee_hotfix;
    private LuaFunction m_PlayerContext_OnServerDisconnectNtfInt32_hotfix;
    private LuaFunction m_PlayerContext_OnClientCheatNtf_hotfix;
    private LuaFunction m_PlayerContext_OnOnBattlePracticeFailNtfPracticeMode_hotfix;
    private LuaFunction m_PlayerContext_EventOnGuildUpdateInfoGuildLog_hotfix;
    private LuaFunction m_EventOnSDKPromotingPlaySuccessString_hotfix;
    private LuaFunction m_get_LayerDescArray_hotfix;
    private LuaFunction m_get_UICtrlDescArray_hotfix;
    private LuaFunction m_StartWorldUITask_hotfix;
    private LuaFunction m_WorldUITask_OnLoadAllResCompleted_hotfix;
    private LuaFunction m_StartBattleUITask_hotfix;
    private LuaFunction m_BattleUITask_OnLoadAllResCompleted_hotfix;
    private LuaFunction m_StartArenaBattleUITask_hotfix;
    private LuaFunction m_CheckEnterBattle_hotfix;
    private LuaFunction m_EnterBattleRoomBattle_hotfix;
    private LuaFunction m_EnterBattleLiveRoomBattle_hotfix;
    private LuaFunction m_EnterArenaBattle_hotfix;
    private LuaFunction m_RebuildBattle_hotfix;
    private LuaFunction m_RebuildBattleHappeningBattleTypeInt32UInt64_hotfix;
    private LuaFunction m_RebuildArenaBattle_hotfix;
    private LuaFunction m_RebuildBattleRoom_hotfix;
    private LuaFunction m_RebuildBattleLiveRoom_hotfix;
    private LuaFunction m_RebuildBattleRoomAndStartUITaskInt32_hotfix;
    private LuaFunction m_RebuildBattleLiveRoomAndStartUITask_hotfix;
    private LuaFunction m_StartArenaBattleCancelNetTaskAndStartWorldUITask_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public CommonUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    ~CommonUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnTick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitNetReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TickCheckClientVersion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TickChatVoice()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TickClientHeartBeat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void TickCheckOnline()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void _TickCheckOnline()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void PostUpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Application_LogMessageReceived(string logString, string stackTrace, LogType type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnNewMarqueeNtf(Marquee marquee)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnServerDisconnectNtf(int errorCode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnClientCheatNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnOnBattlePracticeFailNtf(PracticeMode practiceMode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_EventOnGuildUpdateInfo(GuildLog log)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void EventOnSDKPromotingPlaySuccess(string goodsRegisterID)
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartWorldUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartBattleUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool StartArenaBattleUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CheckEnterBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void EnterBattleRoomBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void EnterBattleLiveRoomBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void EnterArenaBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool RebuildBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool RebuildBattleHappening(BattleType battleType, int levelId, ulong instanceId = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool RebuildArenaBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool RebuildBattleRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool RebuildBattleLiveRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool RebuildBattleRoomAndStartUITask(int result)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool RebuildBattleLiveRoomAndStartUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void StartBattleCancelNetTask(Action<int> onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartArenaBattleCancelNetTaskAndStartWorldUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void StartArenaPlayerInfoGetNetTask(Action<int> onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void StartArenaBattleReconnectNetTask(Action<int> onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void StartBattleRoomGetNetTask(Action<int> onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void StartPeakArenaBattleLiveRoomJoinNetTask(
      int matchId,
      ulong roomId,
      Action<int> onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void StartRealTimePVPGetInfoNetTask(Action<int> onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void StartPeakArenaPlayerInfoGetNetTask(Action<int> onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void StartPeakArenaPlayOffCurrentSeasonMatchInfoGetNetTask(
      BattleRoom battleRoom,
      Action<PeakArenaPlayOffMatchupInfo> onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public CommonUITask.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private bool __callBase_OnStart(UIIntent intent)
    {
      return base.OnStart(intent);
    }

    private void __callBase_OnStop()
    {
      base.OnStop();
    }

    private void __callBase_OnPause()
    {
      this.OnPause();
    }

    private bool __callBase_OnResume(UIIntent intent)
    {
      return this.OnResume(intent);
    }

    private bool __callBase_OnNewIntent(UIIntent intent)
    {
      return this.OnNewIntent(intent);
    }

    private void __callBase_EnableUIInput(bool isEnable, bool? isGlobalEnable)
    {
      this.EnableUIInput(isEnable, isGlobalEnable);
    }

    private bool __callBase_IsNeedLoadDynamicRes()
    {
      return base.IsNeedLoadDynamicRes();
    }

    private List<string> __callBase_CollectAllDynamicResForLoad()
    {
      return this.CollectAllDynamicResForLoad();
    }

    private void __callBase_RegisterPlayerContextEvents()
    {
      this.RegisterPlayerContextEvents();
    }

    private void __callBase_UnregisterPlayerContextEvents()
    {
      this.UnregisterPlayerContextEvents();
    }

    private void __callBase_PostUpdateView()
    {
      base.PostUpdateView();
    }

    private void __callBase_CollectPreloadResourceList()
    {
      this.CollectPreloadResourceList();
    }

    private void __callBase_ClearAssetList()
    {
      this.ClearAssetList();
    }

    private void __callBase_CollectAsset(string assetName)
    {
      this.CollectAsset(assetName);
    }

    private void __callBase_CollectSpriteAsset(string assetName)
    {
      this.CollectSpriteAsset(assetName);
    }

    private void __callBase_CollectFxAsset(string assetName)
    {
      this.CollectFxAsset(assetName);
    }

    private void __callBase_StartLoadCustomAssets(Action onLoadCompleted)
    {
      this.StartLoadCustomAssets(onLoadCompleted);
    }

    private bool __callBase_IsLoadingCustomAssets()
    {
      return this.IsLoadingCustomAssets();
    }

    private bool __callBase_IsPipeLineRunning()
    {
      return this.IsPipeLineRunning();
    }

    private bool __callBase_IsOpeningUI()
    {
      return this.IsOpeningUI();
    }

    private UITaskBase __callBase_ReturnPrevUITask()
    {
      return this.ReturnPrevUITask();
    }

    private void __callBase_ClearUnusedDynamicResourceCache()
    {
      this.ClearUnusedDynamicResourceCache();
    }

    private void __callBase_ClearDynamicResourceCache(List<string> assets)
    {
      this.ClearDynamicResourceCache(assets);
    }

    private void __callBase_OnMemoryWarning()
    {
      this.OnMemoryWarning();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public enum CommonUIState
    {
      Init,
      BeginLoadAsset,
      EndLoadAsset,
      WorldOrBattle,
    }

    public class LuaExportHelper
    {
      private CommonUITask m_owner;

      public LuaExportHelper(CommonUITask owner)
      {
        this.m_owner = owner;
      }

      public bool __callBase_OnStart(UIIntent intent)
      {
        return this.m_owner.__callBase_OnStart(intent);
      }

      public void __callBase_OnStop()
      {
        this.m_owner.__callBase_OnStop();
      }

      public void __callBase_OnPause()
      {
        this.m_owner.__callBase_OnPause();
      }

      public bool __callBase_OnResume(UIIntent intent)
      {
        return this.m_owner.__callBase_OnResume(intent);
      }

      public bool __callBase_OnNewIntent(UIIntent intent)
      {
        return this.m_owner.__callBase_OnNewIntent(intent);
      }

      public void __callBase_EnableUIInput(bool isEnable, bool? isGlobalEnable)
      {
        this.m_owner.__callBase_EnableUIInput(isEnable, isGlobalEnable);
      }

      public bool __callBase_IsNeedLoadDynamicRes()
      {
        return this.m_owner.__callBase_IsNeedLoadDynamicRes();
      }

      public List<string> __callBase_CollectAllDynamicResForLoad()
      {
        return this.m_owner.__callBase_CollectAllDynamicResForLoad();
      }

      public void __callBase_RegisterPlayerContextEvents()
      {
        this.m_owner.__callBase_RegisterPlayerContextEvents();
      }

      public void __callBase_UnregisterPlayerContextEvents()
      {
        this.m_owner.__callBase_UnregisterPlayerContextEvents();
      }

      public void __callBase_PostUpdateView()
      {
        this.m_owner.__callBase_PostUpdateView();
      }

      public void __callBase_CollectPreloadResourceList()
      {
        this.m_owner.__callBase_CollectPreloadResourceList();
      }

      public void __callBase_ClearAssetList()
      {
        this.m_owner.__callBase_ClearAssetList();
      }

      public void __callBase_CollectAsset(string assetName)
      {
        this.m_owner.__callBase_CollectAsset(assetName);
      }

      public void __callBase_CollectSpriteAsset(string assetName)
      {
        this.m_owner.__callBase_CollectSpriteAsset(assetName);
      }

      public void __callBase_CollectFxAsset(string assetName)
      {
        this.m_owner.__callBase_CollectFxAsset(assetName);
      }

      public void __callBase_StartLoadCustomAssets(Action onLoadCompleted)
      {
        this.m_owner.__callBase_StartLoadCustomAssets(onLoadCompleted);
      }

      public bool __callBase_IsLoadingCustomAssets()
      {
        return this.m_owner.__callBase_IsLoadingCustomAssets();
      }

      public bool __callBase_IsPipeLineRunning()
      {
        return this.m_owner.__callBase_IsPipeLineRunning();
      }

      public bool __callBase_IsOpeningUI()
      {
        return this.m_owner.__callBase_IsOpeningUI();
      }

      public UITaskBase __callBase_ReturnPrevUITask()
      {
        return this.m_owner.__callBase_ReturnPrevUITask();
      }

      public void __callBase_ClearUnusedDynamicResourceCache()
      {
        this.m_owner.__callBase_ClearUnusedDynamicResourceCache();
      }

      public void __callBase_ClearDynamicResourceCache(List<string> assets)
      {
        this.m_owner.__callBase_ClearDynamicResourceCache(assets);
      }

      public void __callBase_OnMemoryWarning()
      {
        this.m_owner.__callBase_OnMemoryWarning();
      }

      public static CommonUITask s_instance
      {
        get
        {
          return CommonUITask.s_instance;
        }
        set
        {
          CommonUITask.s_instance = value;
        }
      }

      public UITaskBase.LayerDesc[] m_layerDescArray
      {
        get
        {
          return this.m_owner.m_layerDescArray;
        }
        set
        {
          this.m_owner.m_layerDescArray = value;
        }
      }

      public UITaskBase.UIControllerDesc[] m_uiCtrlDescArray
      {
        get
        {
          return this.m_owner.m_uiCtrlDescArray;
        }
        set
        {
          this.m_owner.m_uiCtrlDescArray = value;
        }
      }

      public CommonUIController m_commonUIController
      {
        get
        {
          return this.m_owner.m_commonUIController;
        }
        set
        {
          this.m_owner.m_commonUIController = value;
        }
      }

      public float m_sendHeartBeatTime
      {
        get
        {
          return this.m_owner.m_sendHeartBeatTime;
        }
        set
        {
          this.m_owner.m_sendHeartBeatTime = value;
        }
      }

      public DateTime m_nextCheckOnlineTime
      {
        get
        {
          return this.m_owner.m_nextCheckOnlineTime;
        }
        set
        {
          this.m_owner.m_nextCheckOnlineTime = value;
        }
      }

      public StringTableId m_disconnectedByServerMessageId
      {
        get
        {
          return this.m_owner.m_disconnectedByServerMessageId;
        }
        set
        {
          this.m_owner.m_disconnectedByServerMessageId = value;
        }
      }

      public float m_lastCheckClientVersionTime
      {
        get
        {
          return this.m_owner.m_lastCheckClientVersionTime;
        }
        set
        {
          this.m_owner.m_lastCheckClientVersionTime = value;
        }
      }

      public int m_netTaskRunningCount
      {
        get
        {
          return this.m_owner.m_netTaskRunningCount;
        }
        set
        {
          this.m_owner.m_netTaskRunningCount = value;
        }
      }

      public UITaskBase.LayerDesc[] LayerDescArray
      {
        get
        {
          return this.m_owner.LayerDescArray;
        }
      }

      public UITaskBase.UIControllerDesc[] UICtrlDescArray
      {
        get
        {
          return this.m_owner.UICtrlDescArray;
        }
      }

      public void InitAllUIControllers()
      {
        this.m_owner.InitAllUIControllers();
      }

      public void ClearAllContextAndRes()
      {
        this.m_owner.ClearAllContextAndRes();
      }

      public bool OnStart(UIIntent intent)
      {
        return this.m_owner.OnStart(intent);
      }

      public void OnStop()
      {
        this.m_owner.OnStop();
      }

      public void OnTick()
      {
        this.m_owner.OnTick();
      }

      public void InitNetReq()
      {
        this.m_owner.InitNetReq();
      }

      public void TickCheckClientVersion()
      {
        this.m_owner.TickCheckClientVersion();
      }

      public void TickChatVoice()
      {
        this.m_owner.TickChatVoice();
      }

      public void TickClientHeartBeat()
      {
        this.m_owner.TickClientHeartBeat();
      }

      public void _TickCheckOnline()
      {
        this.m_owner._TickCheckOnline();
      }

      public void UpdateView()
      {
        this.m_owner.UpdateView();
      }

      public bool IsNeedLoadDynamicRes()
      {
        return this.m_owner.IsNeedLoadDynamicRes();
      }

      public void PostUpdateView()
      {
        this.m_owner.PostUpdateView();
      }

      public void Application_LogMessageReceived(string logString, string stackTrace, LogType type)
      {
        this.m_owner.Application_LogMessageReceived(logString, stackTrace, type);
      }

      public void PlayerContext_OnNewMarqueeNtf(Marquee marquee)
      {
        this.m_owner.PlayerContext_OnNewMarqueeNtf(marquee);
      }

      public void PlayerContext_OnServerDisconnectNtf(int errorCode)
      {
        this.m_owner.PlayerContext_OnServerDisconnectNtf(errorCode);
      }

      public void PlayerContext_OnClientCheatNtf()
      {
        this.m_owner.PlayerContext_OnClientCheatNtf();
      }

      public void PlayerContext_OnOnBattlePracticeFailNtf(PracticeMode practiceMode)
      {
        this.m_owner.PlayerContext_OnOnBattlePracticeFailNtf(practiceMode);
      }

      public void PlayerContext_EventOnGuildUpdateInfo(GuildLog log)
      {
        this.m_owner.PlayerContext_EventOnGuildUpdateInfo(log);
      }

      public void EventOnSDKPromotingPlaySuccess(string goodsRegisterID)
      {
        this.m_owner.EventOnSDKPromotingPlaySuccess(goodsRegisterID);
      }

      public void StartWorldUITask()
      {
        this.m_owner.StartWorldUITask();
      }

      public void WorldUITask_OnLoadAllResCompleted()
      {
        this.m_owner.WorldUITask_OnLoadAllResCompleted();
      }

      public void StartBattleUITask()
      {
        this.m_owner.StartBattleUITask();
      }

      public void BattleUITask_OnLoadAllResCompleted()
      {
        this.m_owner.BattleUITask_OnLoadAllResCompleted();
      }

      public bool StartArenaBattleUITask()
      {
        return this.m_owner.StartArenaBattleUITask();
      }

      public bool CheckEnterBattle()
      {
        return this.m_owner.CheckEnterBattle();
      }

      public void EnterBattleRoomBattle()
      {
        this.m_owner.EnterBattleRoomBattle();
      }

      public void EnterBattleLiveRoomBattle()
      {
        this.m_owner.EnterBattleLiveRoomBattle();
      }

      public void EnterArenaBattle()
      {
        this.m_owner.EnterArenaBattle();
      }

      public bool RebuildBattle()
      {
        return this.m_owner.RebuildBattle();
      }

      public bool RebuildBattleHappening(BattleType battleType, int levelId, ulong instanceId)
      {
        return this.m_owner.RebuildBattleHappening(battleType, levelId, instanceId);
      }

      public bool RebuildArenaBattle()
      {
        return this.m_owner.RebuildArenaBattle();
      }

      public bool RebuildBattleRoom()
      {
        return this.m_owner.RebuildBattleRoom();
      }

      public bool RebuildBattleLiveRoom()
      {
        return this.m_owner.RebuildBattleLiveRoom();
      }

      public bool RebuildBattleRoomAndStartUITask(int result)
      {
        return this.m_owner.RebuildBattleRoomAndStartUITask(result);
      }

      public bool RebuildBattleLiveRoomAndStartUITask()
      {
        return this.m_owner.RebuildBattleLiveRoomAndStartUITask();
      }

      public static void StartBattleCancelNetTask(Action<int> onEnd)
      {
        CommonUITask.StartBattleCancelNetTask(onEnd);
      }

      public void StartArenaBattleCancelNetTaskAndStartWorldUITask()
      {
        this.m_owner.StartArenaBattleCancelNetTaskAndStartWorldUITask();
      }

      public static void StartArenaPlayerInfoGetNetTask(Action<int> onEnd)
      {
        CommonUITask.StartArenaPlayerInfoGetNetTask(onEnd);
      }

      public static void StartArenaBattleReconnectNetTask(Action<int> onEnd)
      {
        CommonUITask.StartArenaBattleReconnectNetTask(onEnd);
      }
    }
  }
}
