﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ClimbTowerUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using SLua;
using System;
using System.Collections;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class ClimbTowerUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./HelpButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_helpButton;
    [AutoBind("./TopProgress/RewardButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_showRewardButton;
    [AutoBind("./StartButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_startButton;
    [AutoBind("./StageClearOrNot", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_clearUIStateController;
    [AutoBind("./RaidButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_raidButton;
    [AutoBind("./RaidButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_raidButtonUIStateController;
    [AutoBind("./RaidButton/InfoPanel/TopValue", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_historyMaxFloorText;
    [AutoBind("./RaidButton/InfoPanel/WeekTopValue", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_raidMaxFloorText;
    [AutoBind("./StageName/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_bigFloorNameText;
    [AutoBind("./BlackCover", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_blackCoverUIStateController;
    [AutoBind("./StageGroup/SoldierItem1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_floor0FloorGameObject;
    [AutoBind("./StageGroup/SoldierItem2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_floor1FloorGameObject;
    [AutoBind("./StageGroup/SoldierItem3", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_floor2FloorGameObject;
    [AutoBind("./StageGroup/SoldierItem4", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_floor3FloorGameObject;
    [AutoBind("./StageGroup/BossItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_floor4FloorGameObject;
    [AutoBind("./TopProgress/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
    private Slider m_progressSlider;
    [AutoBind("./TopProgress/ProgressBar/Fill Area/Info/Text/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_progressInfoText;
    [AutoBind("./TopProgress/TimeText/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_flushTimeText;
    [AutoBind("./RewardPreview", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_rewardPreviewUIStateController;
    [AutoBind("./RewardPreview/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_rewardPreviewBackgroundButton;
    [AutoBind("./RewardPreview/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_rewardPreviewScollRect;
    [AutoBind("./RaidPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_raidPanelUIStateController;
    [AutoBind("./RaidPanel/Detail/NowEngryValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_raidPanelNowEnergyText;
    [AutoBind("./RaidPanel/Detail/NeedEngryValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_raidPanelNeedEnergyText;
    [AutoBind("./RaidPanel/Detail/RaidText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_raidPanelText;
    [AutoBind("./RaidPanel/Detail/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_raidPanelConfirmButton;
    [AutoBind("./RaidPanel/Detail/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_raidPanelCancelButton;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefabs/RewardItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_rewardItemPrefab;
    private ClimbTowerFloorItemUIController[] m_climbTowerFloorItemUIControllers;

    [MethodImpl((MethodImplOptions) 32768)]
    private ClimbTowerUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetNextFlushTime(TimeSpan ts)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetClearAllFloors(bool clear)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowClearEffectBegin(bool isRaid, Action onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_ShowClearEffectBegin(bool isRaid, Action onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowClearEffectEnd()
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetProgress(float p)
    {
      this.m_progressSlider.value = p;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCurrentTowerFloor(ConfigDataTowerFloorInfo floorInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTowerFloor(
      int idx,
      ConfigDataTowerFloorInfo floorInfo,
      ConfigDataTowerLevelInfo levelInfo,
      bool isCleared,
      bool isLocked)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowRaidButton(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRaidTowerFloor(
      ConfigDataTowerFloorInfo historyMaxFloorInfo,
      ConfigDataTowerFloorInfo raidMaxFloorInfo,
      bool canRaid)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowRewardPreview()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HideRewardPreview()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowRaidPanel(
      int curEnrgy,
      int costEnergy,
      ConfigDataTowerFloorInfo reachFloorInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HideRaidPanel(Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHelpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnStartButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRaidButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnShowRewardButtonClick()
    {
      this.ShowRewardPreview();
    }

    private void OnRewardPreviewCloseButtonClick()
    {
      this.HideRewardPreview();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRaidPanelConfirmButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnRaidPanelCancelButtonClick()
    {
      this.HideRaidPanel((Action) null);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClimbTowerFloorItemUIController_OnButtonClick(ClimbTowerFloorItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowHelp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowRaid
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowLevelInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnConfirmRaid
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
