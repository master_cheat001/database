﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.CollectionActivityRecommendItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class CollectionActivityRecommendItemUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_stateCtrl;
    [AutoBind("./materialsItem", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_iconButton;
    [AutoBind("./materialsItem", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_iconBgImage;
    [AutoBind("./materialsItem/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_iconImage;
    [AutoBind("./HeroGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroGroupRoot;
    private List<CollectionActivityRecommendHeroUIController> m_heroCtrlList;
    private CollectionActivityCurrency m_currencyInfo;
    [DoNotToLua]
    private CollectionActivityRecommendItemUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_UpdateItemCollectionActivityCurrency_hotfix;
    private LuaFunction m_HandleBeforeReturnToPool_hotfix;
    private LuaFunction m_OnItemClicked_hotfix;
    private LuaFunction m_set_IsMaskedBoolean_hotfix;
    private LuaFunction m_add_EventOnGetFromRecommendHeroPoolFunc`1_hotfix;
    private LuaFunction m_remove_EventOnGetFromRecommendHeroPoolFunc`1_hotfix;
    private LuaFunction m_add_EventOnReturnFromRecommendHeroPoolAction`1_hotfix;
    private LuaFunction m_remove_EventOnReturnFromRecommendHeroPoolAction`1_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivityRecommendItemUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateItem(CollectionActivityCurrency currencyInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HandleBeforeReturnToPool()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnItemClicked()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsMasked
    {
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Func<CollectionActivityRecommendHeroUIController> EventOnGetFromRecommendHeroPool
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<CollectionActivityRecommendHeroUIController> EventOnReturnFromRecommendHeroPool
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public CollectionActivityRecommendItemUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private CollectionActivityRecommendHeroUIController __callDele_EventOnGetFromRecommendHeroPool()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnGetFromRecommendHeroPool()
    {
      this.EventOnGetFromRecommendHeroPool = (Func<CollectionActivityRecommendHeroUIController>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnReturnFromRecommendHeroPool(
      CollectionActivityRecommendHeroUIController obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnReturnFromRecommendHeroPool(
      CollectionActivityRecommendHeroUIController obj)
    {
      this.EventOnReturnFromRecommendHeroPool = (Action<CollectionActivityRecommendHeroUIController>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private CollectionActivityRecommendItemUIController m_owner;

      public LuaExportHelper(CollectionActivityRecommendItemUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public CollectionActivityRecommendHeroUIController __callDele_EventOnGetFromRecommendHeroPool()
      {
        // ISSUE: unable to decompile the method.
      }

      public void __clearDele_EventOnGetFromRecommendHeroPool()
      {
        this.m_owner.__clearDele_EventOnGetFromRecommendHeroPool();
      }

      public void __callDele_EventOnReturnFromRecommendHeroPool(
        CollectionActivityRecommendHeroUIController obj)
      {
        this.m_owner.__callDele_EventOnReturnFromRecommendHeroPool(obj);
      }

      public void __clearDele_EventOnReturnFromRecommendHeroPool(
        CollectionActivityRecommendHeroUIController obj)
      {
        this.m_owner.__clearDele_EventOnReturnFromRecommendHeroPool(obj);
      }

      public CommonUIStateController m_stateCtrl
      {
        get
        {
          return this.m_owner.m_stateCtrl;
        }
        set
        {
          this.m_owner.m_stateCtrl = value;
        }
      }

      public Button m_iconButton
      {
        get
        {
          return this.m_owner.m_iconButton;
        }
        set
        {
          this.m_owner.m_iconButton = value;
        }
      }

      public Image m_iconBgImage
      {
        get
        {
          return this.m_owner.m_iconBgImage;
        }
        set
        {
          this.m_owner.m_iconBgImage = value;
        }
      }

      public Image m_iconImage
      {
        get
        {
          return this.m_owner.m_iconImage;
        }
        set
        {
          this.m_owner.m_iconImage = value;
        }
      }

      public GameObject m_heroGroupRoot
      {
        get
        {
          return this.m_owner.m_heroGroupRoot;
        }
        set
        {
          this.m_owner.m_heroGroupRoot = value;
        }
      }

      public List<CollectionActivityRecommendHeroUIController> m_heroCtrlList
      {
        get
        {
          return this.m_owner.m_heroCtrlList;
        }
        set
        {
          this.m_owner.m_heroCtrlList = value;
        }
      }

      public CollectionActivityCurrency m_currencyInfo
      {
        get
        {
          return this.m_owner.m_currencyInfo;
        }
        set
        {
          this.m_owner.m_currencyInfo = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void OnItemClicked()
      {
        this.m_owner.OnItemClicked();
      }
    }
  }
}
