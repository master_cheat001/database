﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.TeamLocationListItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class TeamLocationListItemUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_toggle;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_energyIconImage;
    [AutoBind("./ChosenText/LevelName", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameTextChosen;
    [AutoBind("./ChosenText/Value", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_energyTextChosen;
    [AutoBind("./ChosenText/X", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_energyXTextChosen;
    [AutoBind("./UnchosenText/LevelName", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameTextUnchosen;
    [AutoBind("./UnchosenText/Value", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_energyTextUnchosen;
    [AutoBind("./UnchosenText/X", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_energyXTextUnchosen;
    [AutoBind("./LockState", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_lockButton;
    private bool m_isLocked;
    private int m_locationId;
    [DoNotToLua]
    private TeamLocationListItemUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_SetNameString_hotfix;
    private LuaFunction m_GetName_hotfix;
    private LuaFunction m_SetLocationIdInt32_hotfix;
    private LuaFunction m_GetLocationId_hotfix;
    private LuaFunction m_SetEnergyInt32_hotfix;
    private LuaFunction m_SetLockedBoolean_hotfix;
    private LuaFunction m_IsLocked_hotfix;
    private LuaFunction m_SetToggleValueBoolean_hotfix;
    private LuaFunction m_GetToggleValue_hotfix;
    private LuaFunction m_OnToggleValueChangedBoolean_hotfix;
    private LuaFunction m_OnLockButtonClick_hotfix;
    private LuaFunction m_add_EventOnToggleValueChangedAction`2_hotfix;
    private LuaFunction m_remove_EventOnToggleValueChangedAction`2_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetName(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLocationId(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetLocationId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetEnergy(int energy)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLocked(bool isLocked)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsLocked()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetToggleValue(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool GetToggleValue()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnToggleValueChanged(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnLockButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<bool, TeamLocationListItemUIController> EventOnToggleValueChanged
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public TeamLocationListItemUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnToggleValueChanged(
      bool arg1,
      TeamLocationListItemUIController arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnToggleValueChanged(
      bool arg1,
      TeamLocationListItemUIController arg2)
    {
      this.EventOnToggleValueChanged = (Action<bool, TeamLocationListItemUIController>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private TeamLocationListItemUIController m_owner;

      public LuaExportHelper(TeamLocationListItemUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnToggleValueChanged(
        bool arg1,
        TeamLocationListItemUIController arg2)
      {
        this.m_owner.__callDele_EventOnToggleValueChanged(arg1, arg2);
      }

      public void __clearDele_EventOnToggleValueChanged(
        bool arg1,
        TeamLocationListItemUIController arg2)
      {
        this.m_owner.__clearDele_EventOnToggleValueChanged(arg1, arg2);
      }

      public Toggle m_toggle
      {
        get
        {
          return this.m_owner.m_toggle;
        }
        set
        {
          this.m_owner.m_toggle = value;
        }
      }

      public CommonUIStateController m_uiStateController
      {
        get
        {
          return this.m_owner.m_uiStateController;
        }
        set
        {
          this.m_owner.m_uiStateController = value;
        }
      }

      public Image m_energyIconImage
      {
        get
        {
          return this.m_owner.m_energyIconImage;
        }
        set
        {
          this.m_owner.m_energyIconImage = value;
        }
      }

      public Text m_nameTextChosen
      {
        get
        {
          return this.m_owner.m_nameTextChosen;
        }
        set
        {
          this.m_owner.m_nameTextChosen = value;
        }
      }

      public Text m_energyTextChosen
      {
        get
        {
          return this.m_owner.m_energyTextChosen;
        }
        set
        {
          this.m_owner.m_energyTextChosen = value;
        }
      }

      public Text m_energyXTextChosen
      {
        get
        {
          return this.m_owner.m_energyXTextChosen;
        }
        set
        {
          this.m_owner.m_energyXTextChosen = value;
        }
      }

      public Text m_nameTextUnchosen
      {
        get
        {
          return this.m_owner.m_nameTextUnchosen;
        }
        set
        {
          this.m_owner.m_nameTextUnchosen = value;
        }
      }

      public Text m_energyTextUnchosen
      {
        get
        {
          return this.m_owner.m_energyTextUnchosen;
        }
        set
        {
          this.m_owner.m_energyTextUnchosen = value;
        }
      }

      public Text m_energyXTextUnchosen
      {
        get
        {
          return this.m_owner.m_energyXTextUnchosen;
        }
        set
        {
          this.m_owner.m_energyXTextUnchosen = value;
        }
      }

      public Button m_lockButton
      {
        get
        {
          return this.m_owner.m_lockButton;
        }
        set
        {
          this.m_owner.m_lockButton = value;
        }
      }

      public bool m_isLocked
      {
        get
        {
          return this.m_owner.m_isLocked;
        }
        set
        {
          this.m_owner.m_isLocked = value;
        }
      }

      public int m_locationId
      {
        get
        {
          return this.m_owner.m_locationId;
        }
        set
        {
          this.m_owner.m_locationId = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void OnToggleValueChanged(bool on)
      {
        this.m_owner.OnToggleValueChanged(on);
      }

      public void OnLockButtonClick()
      {
        this.m_owner.OnLockButtonClick();
      }
    }
  }
}
