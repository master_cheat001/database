﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.FriendInviteToGroupGetNetTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class FriendInviteToGroupGetNetTask : UINetTask
  {
    private List<string> m_userIDList;
    private string m_chatGroupID;

    [MethodImpl((MethodImplOptions) 32768)]
    public FriendInviteToGroupGetNetTask(string chatGroupID, List<string> userIDList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool StartNetWorking()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnInviteToChatGroupAck(
      int result,
      ProChatGroupInfo chatGroupInfo,
      ProChatUserInfo userInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    public ProChatGroupInfo ChatGroupInfo { private set; get; }

    public ProChatUserInfo FailedUser { private set; get; }
  }
}
