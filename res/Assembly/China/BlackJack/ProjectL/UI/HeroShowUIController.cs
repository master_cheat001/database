﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroShowUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Collections;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class HeroShowUIController : UIControllerBase
  {
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private PrefabResourceContainer m_resourceContainer;
    [AutoBind("./HeroShow", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroShowRoot;
    [AutoBind("./HeroShow/Heros/R", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_herosRGameObject;
    [AutoBind("./HeroShow/Heros/SR", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_herosSRGameObject;
    [AutoBind("./HeroShow/Heros/SSR", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_herosSSRGameObject;
    [AutoBind("./ShareButtonDummy", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_shareButtonDummy;
    [AutoBind("./SharePhotpDummy", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_sharePhotoDummy;
    [AutoBind("./BackButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./ToggleGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_toggleGroup;
    [AutoBind("./ToggleGroup/SSRToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_ssrToggle;
    [AutoBind("./ToggleGroup/SRToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_srToggle;
    [AutoBind("./ToggleGroup/RToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_rToggle;
    private Button m_weiBoButton;
    private Button m_weChatButton;
    private Text m_nameText;
    private Text m_lvText;
    private Text m_serverNameText;
    private HeroShowComponent m_heroShowComponent;
    private ArchiveUITask m_task;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    [DoNotToLua]
    private HeroShowUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_SetTaskArchiveUITask_hotfix;
    private LuaFunction m_SwitchShowRankHeroInt32_hotfix;
    private LuaFunction m_LoadHeroTeamInt32_hotfix;
    private LuaFunction m_DestroyHeroShowComponent_hotfix;
    private LuaFunction m_OnWeiBoClick_hotfix;
    private LuaFunction m_OnWeChatClick_hotfix;
    private LuaFunction m_OnReturnClick_hotfix;
    private LuaFunction m_SwitchRankShowClickGameObject_hotfix;
    private LuaFunction m_OnScaleHeroShowSingle_hotfix;
    private LuaFunction m_OnMoveHeroShowVector2_hotfix;
    private LuaFunction m_WeiBoShare_hotfix;
    private LuaFunction m_WeChatShare_hotfix;
    private LuaFunction m_CaptureFrame_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTask(ArchiveUITask task)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SwitchShowRankHero(int rank)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void LoadHeroTeam(int rank)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DestroyHeroShowComponent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnWeiBoClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnWeChatClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SwitchRankShowClick(GameObject obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnScaleHeroShow(float scaleDelta)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMoveHeroShow(Vector2 offset)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator WeiBoShare()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator WeChatShare()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator CaptureFrame()
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public HeroShowUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private HeroShowUIController m_owner;

      public LuaExportHelper(HeroShowUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public PrefabResourceContainer m_resourceContainer
      {
        get
        {
          return this.m_owner.m_resourceContainer;
        }
        set
        {
          this.m_owner.m_resourceContainer = value;
        }
      }

      public GameObject m_heroShowRoot
      {
        get
        {
          return this.m_owner.m_heroShowRoot;
        }
        set
        {
          this.m_owner.m_heroShowRoot = value;
        }
      }

      public GameObject m_herosRGameObject
      {
        get
        {
          return this.m_owner.m_herosRGameObject;
        }
        set
        {
          this.m_owner.m_herosRGameObject = value;
        }
      }

      public GameObject m_herosSRGameObject
      {
        get
        {
          return this.m_owner.m_herosSRGameObject;
        }
        set
        {
          this.m_owner.m_herosSRGameObject = value;
        }
      }

      public GameObject m_herosSSRGameObject
      {
        get
        {
          return this.m_owner.m_herosSSRGameObject;
        }
        set
        {
          this.m_owner.m_herosSSRGameObject = value;
        }
      }

      public GameObject m_shareButtonDummy
      {
        get
        {
          return this.m_owner.m_shareButtonDummy;
        }
        set
        {
          this.m_owner.m_shareButtonDummy = value;
        }
      }

      public GameObject m_sharePhotoDummy
      {
        get
        {
          return this.m_owner.m_sharePhotoDummy;
        }
        set
        {
          this.m_owner.m_sharePhotoDummy = value;
        }
      }

      public Button m_returnButton
      {
        get
        {
          return this.m_owner.m_returnButton;
        }
        set
        {
          this.m_owner.m_returnButton = value;
        }
      }

      public GameObject m_toggleGroup
      {
        get
        {
          return this.m_owner.m_toggleGroup;
        }
        set
        {
          this.m_owner.m_toggleGroup = value;
        }
      }

      public Toggle m_ssrToggle
      {
        get
        {
          return this.m_owner.m_ssrToggle;
        }
        set
        {
          this.m_owner.m_ssrToggle = value;
        }
      }

      public Toggle m_srToggle
      {
        get
        {
          return this.m_owner.m_srToggle;
        }
        set
        {
          this.m_owner.m_srToggle = value;
        }
      }

      public Toggle m_rToggle
      {
        get
        {
          return this.m_owner.m_rToggle;
        }
        set
        {
          this.m_owner.m_rToggle = value;
        }
      }

      public Button m_weiBoButton
      {
        get
        {
          return this.m_owner.m_weiBoButton;
        }
        set
        {
          this.m_owner.m_weiBoButton = value;
        }
      }

      public Button m_weChatButton
      {
        get
        {
          return this.m_owner.m_weChatButton;
        }
        set
        {
          this.m_owner.m_weChatButton = value;
        }
      }

      public Text m_nameText
      {
        get
        {
          return this.m_owner.m_nameText;
        }
        set
        {
          this.m_owner.m_nameText = value;
        }
      }

      public Text m_lvText
      {
        get
        {
          return this.m_owner.m_lvText;
        }
        set
        {
          this.m_owner.m_lvText = value;
        }
      }

      public Text m_serverNameText
      {
        get
        {
          return this.m_owner.m_serverNameText;
        }
        set
        {
          this.m_owner.m_serverNameText = value;
        }
      }

      public HeroShowComponent m_heroShowComponent
      {
        get
        {
          return this.m_owner.m_heroShowComponent;
        }
        set
        {
          this.m_owner.m_heroShowComponent = value;
        }
      }

      public ArchiveUITask m_task
      {
        get
        {
          return this.m_owner.m_task;
        }
        set
        {
          this.m_owner.m_task = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void OnWeiBoClick()
      {
        this.m_owner.OnWeiBoClick();
      }

      public void OnWeChatClick()
      {
        this.m_owner.OnWeChatClick();
      }

      public void OnReturnClick()
      {
        this.m_owner.OnReturnClick();
      }

      public void SwitchRankShowClick(GameObject obj)
      {
        this.m_owner.SwitchRankShowClick(obj);
      }

      public void OnScaleHeroShow(float scaleDelta)
      {
        this.m_owner.OnScaleHeroShow(scaleDelta);
      }

      public void OnMoveHeroShow(Vector2 offset)
      {
        this.m_owner.OnMoveHeroShow(offset);
      }

      public IEnumerator WeiBoShare()
      {
        return this.m_owner.WeiBoShare();
      }

      public IEnumerator WeChatShare()
      {
        return this.m_owner.WeChatShare();
      }

      public IEnumerator CaptureFrame()
      {
        return this.m_owner.CaptureFrame();
      }
    }
  }
}
