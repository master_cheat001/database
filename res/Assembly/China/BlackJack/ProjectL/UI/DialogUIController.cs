﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.DialogUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime;
using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Misc;
using BlackJack.ProjectL.Scene;
using SLua;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class DialogUIController : UIControllerBase
  {
    [AutoBind("./SkipButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_skipButton;
    [AutoBind("./Background/Mask", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_backgroundUIStateController;
    [AutoBind("./Background/Mask/Graphic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_backgroundGraphicGameObject;
    [AutoBind("./Background/Mask/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_backgroundImage;
    [AutoBind("./BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_backgroundButton;
    [AutoBind("./PlaceNamePanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_placeGameObject;
    [AutoBind("./PlaceNamePanel/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_placeText;
    [AutoBind("./WordPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_wordsGameObject;
    [AutoBind("./Char/0", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_char0GameObject;
    [AutoBind("./Char/1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_char1GameObject;
    [AutoBind("./Char/2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_char2GameObject;
    [AutoBind("./AllEffectGroup/EffectGroup1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_effectGroup1;
    [AutoBind("./AllEffectGroup/EffectGroup2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_effectGroup2;
    [AutoBind("./AllEffectGroup/EffectGroup3", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_effectGroup3;
    [AutoBind("./InsertEffect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_insertEffect;
    [AutoBind("./ChangeBGTween", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_changeBGTweenUIStateCtrl;
    private DialogCharUIController[] m_dialogCharUIControllers;
    private DialogBoxUIController m_dialogBoxUIController;
    private UISpineGraphic m_backgroundGraphic;
    private string m_group1PrefabName;
    private string m_group2PrefabName;
    private string m_group3PrefabName;
    private IConfigDataLoader m_configDataLoader;
    private IAudioPlayback m_currentAudio;
    private ConfigDataDialogInfo m_dialogInfo;
    private string m_backgroundAssetName;
    private bool m_needBGChangeTween;

    [MethodImpl((MethodImplOptions) 32768)]
    private DialogUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnDisable()
    {
      this.DestroyBackgroundGraphic();
      this.ClearBackgroundImage();
    }

    private void Update()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowDialog(ConfigDataDialogInfo dialogInfo, bool needBGChangeTween)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_ShowDialog(
      ConfigDataDialogInfo dialogInfo,
      bool needBGChangeTween)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_CharLeavePlay(
      ConfigDataDialogInfo dialogInfo,
      Action onEndAction = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_BGChangeTweenPlay(ConfigDataDialogInfo dialogInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_SetBGChangeLeaveShowUIState(
      ConfigDataDialogInfo dialogInfo,
      string state)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_SetBGChangeEnterShowUIState(string state)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_CreateCharEnter(
      ConfigDataDialogInfo dialogInfo,
      DialogCharUIController c)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayBGMInDialog(ConfigDataDialogInfo dialogInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    public void CloseDialog()
    {
      this.StartCoroutine(this.Co_CloseDialog());
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_CloseDialog()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowDialogBox()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator ShowDialogOption(float endTime, ConfigDataDialogInfo dialogInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateBackgroundGraphic(string assetName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DestroyBackgroundGraphic()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_CreateBeforeEnterEffectGroup()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_CreateBeforeTalkEffectGroup()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_AfterTalkEffectAndLeave(ConfigDataDialogInfo dialogInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_CreateAfterTalkEffectGroup(ConfigDataDialogInfo dialogInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateEffectGroupLeaveTogether(
      ConfigDataDialogInfo dialogInfo,
      List<int> effectGroup,
      GameObject parentGameObject,
      Action onEndAction)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateEffectGroup(
      List<int> effectGroup,
      GameObject parentGameObject,
      Action onEndAction)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayEffectGroup(
      List<int> effectGroup,
      GameObject parentGameObject,
      Action onEndAction)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateEffect(
      ConfigDataPrefabStateInfo prefabStateConfig,
      GameObject parentGameObject,
      Action onEndAction)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateEffectInGroup(
      ref string prefabName,
      ConfigDataPrefabStateInfo prefabStateConfig,
      GameObject parentGameObject,
      Action onEndAction)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateInsertEffect(int prefabStateID, Action onEndAction)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetBackgroundImage(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearBackgroundImage()
    {
      // ISSUE: unable to decompile the method.
    }

    private IAudioPlayback PlayVoice(string name)
    {
      return AudioUtility.PlaySound(name);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StopVoice()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_NextDialog()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowSkipAndNextButton(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSkipButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnLogButtonClick(UIControllerBase uIControllerBase)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnSkip
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<bool> EventOnNext
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ConfigDataDialogInfo> EventOnAutoShowDialogOption
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnLog
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
