﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.GuildCreateReqNetTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class GuildCreateReqNetTask : UINetTask
  {
    private string m_guildName;
    private string m_hiringDeclaration;
    private string m_announcement;
    private bool m_autoJoin;
    private int m_joinLevel;

    [MethodImpl((MethodImplOptions) 32768)]
    public GuildCreateReqNetTask(
      string guildName,
      string hiringDeclaration,
      string announcement,
      bool autoJoin,
      int joinLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool StartNetWorking()
    {
      // ISSUE: unable to decompile the method.
    }

    protected void OnMsgAck(int result)
    {
      this.Result = result;
      this.OnTransactionComplete();
    }
  }
}
