﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BattleReportPlayerPanelUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Battle;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class BattleReportPlayerPanelUIController : UIControllerBase
  {
    [AutoBind("./PlayerInfo", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_playerUIStateController;
    [AutoBind("./PlayerInfo/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerNameText;
    [AutoBind("./PlayerInfo/HeadIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_playerHeadIconImage;
    [AutoBind("./PlayerInfo/HeadIcon/HeadFrameDummy", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_playerHeadFrameTransform;
    [AutoBind("./PlayerInfo/HeadIcon/LevelText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerLevelText;
    [AutoBind("./PlayerInfo/TimeGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_timeGroupGameObject;
    [AutoBind("./PlayerInfo/TimeGroup/SurplusTime", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_timeGameObject;
    [AutoBind("./PlayerInfo/TimeGroup/SurplusTime/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_timeText;
    [AutoBind("./PlayerInfo/TimeGroup/ReserveTime", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_timeGameObject2;
    [AutoBind("./PlayerInfo/TimeGroup/ReserveTime/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_timeText2;
    [AutoBind("./WinGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_winGroupUIStateController;
    [AutoBind("./WinGroup/Win1/Image", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_winGroupWin1GameObject;
    [AutoBind("./WinGroup/Win2/Image", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_winGroupWin2GameObject;
    [AutoBind("./IntoBattleGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroGroupGameObject;
    [AutoBind("./IntoBattleGroup/MyHeroGroup", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_heroGroupTransform;
    [DoNotToLua]
    private BattleReportPlayerPanelUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_SetPlayerStringInt32Int32_hotfix;
    private LuaFunction m_SetPlayerActiveBoolean_hotfix;
    private LuaFunction m_ShowCountdownBoolean_hotfix;
    private LuaFunction m_SetCountdownTimeSpanTimeSpan_hotfix;
    private LuaFunction m_ShowWinCountInt32Boolean_hotfix;
    private LuaFunction m_ShowHerosBoolean_hotfix;
    private LuaFunction m_ClearHeros_hotfix;
    private LuaFunction m_AddHeroGameObjectBattleActor_hotfix;
    private LuaFunction m_SetHeroAliveInt32Boolean_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayer(string name, int level, int headIcon)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayerActive(bool isActive)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowCountdown(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCountdown(TimeSpan ts, TimeSpan ts2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowWinCount(int winCount, bool isBattleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowHeros(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearHeros()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddHero(GameObject prefab, BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHeroAlive(int actorId, bool isAlive)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public BattleReportPlayerPanelUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private BattleReportPlayerPanelUIController m_owner;

      public LuaExportHelper(BattleReportPlayerPanelUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public CommonUIStateController m_playerUIStateController
      {
        get
        {
          return this.m_owner.m_playerUIStateController;
        }
        set
        {
          this.m_owner.m_playerUIStateController = value;
        }
      }

      public Text m_playerNameText
      {
        get
        {
          return this.m_owner.m_playerNameText;
        }
        set
        {
          this.m_owner.m_playerNameText = value;
        }
      }

      public Image m_playerHeadIconImage
      {
        get
        {
          return this.m_owner.m_playerHeadIconImage;
        }
        set
        {
          this.m_owner.m_playerHeadIconImage = value;
        }
      }

      public Transform m_playerHeadFrameTransform
      {
        get
        {
          return this.m_owner.m_playerHeadFrameTransform;
        }
        set
        {
          this.m_owner.m_playerHeadFrameTransform = value;
        }
      }

      public Text m_playerLevelText
      {
        get
        {
          return this.m_owner.m_playerLevelText;
        }
        set
        {
          this.m_owner.m_playerLevelText = value;
        }
      }

      public GameObject m_timeGroupGameObject
      {
        get
        {
          return this.m_owner.m_timeGroupGameObject;
        }
        set
        {
          this.m_owner.m_timeGroupGameObject = value;
        }
      }

      public GameObject m_timeGameObject
      {
        get
        {
          return this.m_owner.m_timeGameObject;
        }
        set
        {
          this.m_owner.m_timeGameObject = value;
        }
      }

      public Text m_timeText
      {
        get
        {
          return this.m_owner.m_timeText;
        }
        set
        {
          this.m_owner.m_timeText = value;
        }
      }

      public GameObject m_timeGameObject2
      {
        get
        {
          return this.m_owner.m_timeGameObject2;
        }
        set
        {
          this.m_owner.m_timeGameObject2 = value;
        }
      }

      public Text m_timeText2
      {
        get
        {
          return this.m_owner.m_timeText2;
        }
        set
        {
          this.m_owner.m_timeText2 = value;
        }
      }

      public CommonUIStateController m_winGroupUIStateController
      {
        get
        {
          return this.m_owner.m_winGroupUIStateController;
        }
        set
        {
          this.m_owner.m_winGroupUIStateController = value;
        }
      }

      public GameObject m_winGroupWin1GameObject
      {
        get
        {
          return this.m_owner.m_winGroupWin1GameObject;
        }
        set
        {
          this.m_owner.m_winGroupWin1GameObject = value;
        }
      }

      public GameObject m_winGroupWin2GameObject
      {
        get
        {
          return this.m_owner.m_winGroupWin2GameObject;
        }
        set
        {
          this.m_owner.m_winGroupWin2GameObject = value;
        }
      }

      public GameObject m_heroGroupGameObject
      {
        get
        {
          return this.m_owner.m_heroGroupGameObject;
        }
        set
        {
          this.m_owner.m_heroGroupGameObject = value;
        }
      }

      public Transform m_heroGroupTransform
      {
        get
        {
          return this.m_owner.m_heroGroupTransform;
        }
        set
        {
          this.m_owner.m_heroGroupTransform = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }
    }
  }
}
