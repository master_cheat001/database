﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ScreenFade
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class ScreenFade
  {
    private float m_curTime;
    private float m_duration;
    private int m_delayFrame;
    private bool m_isFadeIn;
    private bool m_isEnd;
    private Color m_color;
    private Action m_onEnd;
    private Image m_image;

    [MethodImpl((MethodImplOptions) 32768)]
    public ScreenFade()
    {
      // ISSUE: unable to decompile the method.
    }

    public void Setup(Image image)
    {
      this.m_image = image;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FadeIn(float time, Color color, Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FadeOut(float time, Color color, Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsFading()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Update(float dt)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
