﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.UnchartedScoreRewardListItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class UnchartedScoreRewardListItemUIController : DynamicGridCellController
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameText;
    [AutoBind("./DetailTextGroup/ValueGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_scoreText;
    [AutoBind("./RewardGroup", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_rewardGroupTransform;
    [DoNotToLua]
    private UnchartedScoreRewardListItemUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_SetUnchartedScoreRewardGroupInfoConfigDataUnchartedScoreRewardGroupInfo_hotfix;
    private LuaFunction m_SetCollectionActivityScoreRewardGroupInfoConfigDataCollectionActivityScoreRewardGroupInfo_hotfix;
    private LuaFunction m_SetRewardGoodsList`1Int32_hotfix;
    private LuaFunction m_SetGotBoolean_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetUnchartedScoreRewardGroupInfo(ConfigDataUnchartedScoreRewardGroupInfo rewardInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCollectionActivityScoreRewardGroupInfo(
      ConfigDataCollectionActivityScoreRewardGroupInfo rewardInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetRewardGoods(List<Goods> rewardGoods, int displayCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGot(bool got)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public UnchartedScoreRewardListItemUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_OnUpdateCell()
    {
      this.OnUpdateCell();
    }

    private void __callBase_OnRenovate()
    {
      this.OnRenovate();
    }

    private void __callBase_OnRecycle()
    {
      this.OnRecycle();
    }

    private void __callBase_Select()
    {
      this.Select();
    }

    private void __callBase_SendEvent(int eventId)
    {
      this.SendEvent(eventId);
    }

    private void __callBase_SendEvent(int eventId, object obj)
    {
      this.SendEvent(eventId, obj);
    }

    private void __callBase_UpdateCell(object info)
    {
      this.UpdateCell(info);
    }

    private void __callBase_Renovate()
    {
      this.Renovate();
    }

    private void __callBase_Recycle()
    {
      this.Recycle();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private UnchartedScoreRewardListItemUIController m_owner;

      public LuaExportHelper(UnchartedScoreRewardListItemUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_OnUpdateCell()
      {
        this.m_owner.__callBase_OnUpdateCell();
      }

      public void __callBase_OnRenovate()
      {
        this.m_owner.__callBase_OnRenovate();
      }

      public void __callBase_OnRecycle()
      {
        this.m_owner.__callBase_OnRecycle();
      }

      public void __callBase_Select()
      {
        this.m_owner.__callBase_Select();
      }

      public void __callBase_SendEvent(int eventId)
      {
        this.m_owner.__callBase_SendEvent(eventId);
      }

      public void __callBase_SendEvent(int eventId, object obj)
      {
        this.m_owner.__callBase_SendEvent(eventId, obj);
      }

      public void __callBase_UpdateCell(object info)
      {
        this.m_owner.__callBase_UpdateCell(info);
      }

      public void __callBase_Renovate()
      {
        this.m_owner.__callBase_Renovate();
      }

      public void __callBase_Recycle()
      {
        this.m_owner.__callBase_Recycle();
      }

      public CommonUIStateController m_uiStateController
      {
        get
        {
          return this.m_owner.m_uiStateController;
        }
        set
        {
          this.m_owner.m_uiStateController = value;
        }
      }

      public Text m_nameText
      {
        get
        {
          return this.m_owner.m_nameText;
        }
        set
        {
          this.m_owner.m_nameText = value;
        }
      }

      public Text m_scoreText
      {
        get
        {
          return this.m_owner.m_scoreText;
        }
        set
        {
          this.m_owner.m_scoreText = value;
        }
      }

      public Transform m_rewardGroupTransform
      {
        get
        {
          return this.m_owner.m_rewardGroupTransform;
        }
        set
        {
          this.m_owner.m_rewardGroupTransform = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void SetRewardGoods(List<Goods> rewardGoods, int displayCount)
      {
        this.m_owner.SetRewardGoods(rewardGoods, displayCount);
      }
    }
  }
}
