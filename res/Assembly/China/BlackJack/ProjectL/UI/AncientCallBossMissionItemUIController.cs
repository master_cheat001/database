﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.AncientCallBossMissionItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class AncientCallBossMissionItemUIController : UIControllerBase
  {
    [AutoBind("./Detail/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    public Text m_titleText;
    [AutoBind("./Detail/DescText", AutoBindAttribute.InitState.NotInit, false)]
    public Text m_descText;
    [AutoBind("./Detail/CountText", AutoBindAttribute.InitState.NotInit, false)]
    public Text m_countText;
    [AutoBind("./Detail/GetButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button m_getButton;
    [AutoBind("./Detail/ProgressBar/ProgressImg", AutoBindAttribute.InitState.NotInit, false)]
    public Image m_progressImg;
    [AutoBind("./Detail/RewardList", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject m_rewardListGo;
    private Mission m_mission;
    private bool m_isMissionFinished;
    private bool m_isMissionCompleted;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitMissionItem(Mission mission, bool isMissionFinished, bool isMissionCompleted)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetMissionInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGetButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnGetButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
