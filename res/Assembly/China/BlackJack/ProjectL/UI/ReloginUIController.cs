﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ReloginUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using SLua;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class ReloginUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./DialogBox", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_dialogBoxGameObject;
    [AutoBind("./DialogBox/Buttons/RetryButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button RetryLoginButton;
    [AutoBind("./DialogBox/Buttons/LoginButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button ReturnToLoginButton;
    [AutoBind("./Waiting", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_waitingGameObject;

    private ReloginUIController()
    {
    }

    protected override void OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Close()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowWaitForReloginConfirm()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowWaitForReloginProcessing()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowWaitReturnToLoginConfirm()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
