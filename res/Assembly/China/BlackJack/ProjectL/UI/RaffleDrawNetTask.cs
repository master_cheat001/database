﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.RaffleDrawNetTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class RaffleDrawNetTask : UINetTask
  {
    protected int m_poolId;

    [MethodImpl((MethodImplOptions) 32768)]
    public RaffleDrawNetTask(int poolId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool StartNetWorking()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnRaffleDrawAck(int res, int raffleId, Goods goods)
    {
      // ISSUE: unable to decompile the method.
    }

    public Goods DrawnGoods { get; private set; }

    public int DrawnRaffleId { get; private set; }
  }
}
