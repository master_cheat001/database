﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BattleActorUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Scene;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class BattleActorUIController : UIControllerBase
  {
    [AutoBind("./HP", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_hpImage;
    [AutoBind("./Army/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_armyIconImage;
    [AutoBind("./PlayerTag", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_playerTagImage;
    [AutoBind("./Enforce", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_enforceGameObject;
    [AutoBind("./Npc", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_npcGameObject;
    [AutoBind("./Protect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_protectGameObject;
    [AutoBind("./KickOut", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_banGameObject;
    [AutoBind("./ScoreUp", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_scoreBonusGameObject;
    [AutoBind("./PowerUp", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_powerUpGameObject;
    [AutoBind("./DropUp", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_dropUpGameObject;
    private BattleActorBuffUIController[] m_buffControllers;
    [DoNotToLua]
    private BattleActorUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_Init_hotfix;
    private LuaFunction m_SetArmyInfoConfigDataArmyInfo_hotfix;
    private LuaFunction m_SetHpBarTypeInt32_hotfix;
    private LuaFunction m_ShowTagStageActorTagType_hotfix;
    private LuaFunction m_ShowProtectBoolean_hotfix;
    private LuaFunction m_ShowBanBoolean_hotfix;
    private LuaFunction m_ShowPlayerIndexTagBoolean_hotfix;
    private LuaFunction m_SetPlayerIndexInt32_hotfix;
    private LuaFunction m_SetHpInt32Int32_hotfix;
    private LuaFunction m_SetBuffList`1_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    private BattleActorUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetArmyInfo(ConfigDataArmyInfo armyInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHpBarType(int barType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowTag(StageActorTagType tagType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowProtect(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBan(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowPlayerIndexTag(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayerIndex(int playerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHp(int hp, int hpValue)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBuff(List<ClientActorBuff> buffs)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public BattleActorUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private BattleActorUIController m_owner;

      public LuaExportHelper(BattleActorUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public Image m_hpImage
      {
        get
        {
          return this.m_owner.m_hpImage;
        }
        set
        {
          this.m_owner.m_hpImage = value;
        }
      }

      public Image m_armyIconImage
      {
        get
        {
          return this.m_owner.m_armyIconImage;
        }
        set
        {
          this.m_owner.m_armyIconImage = value;
        }
      }

      public Image m_playerTagImage
      {
        get
        {
          return this.m_owner.m_playerTagImage;
        }
        set
        {
          this.m_owner.m_playerTagImage = value;
        }
      }

      public GameObject m_enforceGameObject
      {
        get
        {
          return this.m_owner.m_enforceGameObject;
        }
        set
        {
          this.m_owner.m_enforceGameObject = value;
        }
      }

      public GameObject m_npcGameObject
      {
        get
        {
          return this.m_owner.m_npcGameObject;
        }
        set
        {
          this.m_owner.m_npcGameObject = value;
        }
      }

      public GameObject m_protectGameObject
      {
        get
        {
          return this.m_owner.m_protectGameObject;
        }
        set
        {
          this.m_owner.m_protectGameObject = value;
        }
      }

      public GameObject m_banGameObject
      {
        get
        {
          return this.m_owner.m_banGameObject;
        }
        set
        {
          this.m_owner.m_banGameObject = value;
        }
      }

      public GameObject m_scoreBonusGameObject
      {
        get
        {
          return this.m_owner.m_scoreBonusGameObject;
        }
        set
        {
          this.m_owner.m_scoreBonusGameObject = value;
        }
      }

      public GameObject m_powerUpGameObject
      {
        get
        {
          return this.m_owner.m_powerUpGameObject;
        }
        set
        {
          this.m_owner.m_powerUpGameObject = value;
        }
      }

      public GameObject m_dropUpGameObject
      {
        get
        {
          return this.m_owner.m_dropUpGameObject;
        }
        set
        {
          this.m_owner.m_dropUpGameObject = value;
        }
      }

      public BattleActorBuffUIController[] m_buffControllers
      {
        get
        {
          return this.m_owner.m_buffControllers;
        }
        set
        {
          this.m_owner.m_buffControllers = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }
    }
  }
}
