﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.TestUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using MarchingBytes;
using SLua;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class TestUIController : UIControllerBase
  {
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./SystemInfoButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_systemInfoButton;
    [AutoBind("./GoddessDialogButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_goddessDialogButton;
    [AutoBind("./ClearUserGuideButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_clearUserGuideButton;
    [AutoBind("./CompleteUserGuideButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_completeUserGuideButton;
    [AutoBind("./BecomeStrong/Button", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_becomeStrongButton;
    [AutoBind("./BecomeStrong/PlayerLevelInput", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_becomeStrongPlayerLevelInput;
    [AutoBind("./BecomeStrong/HeroStarLevelInput", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_becomeStrongHeroStarLevelInput;
    [AutoBind("./BecomeStrong/EquipmentLevelInput", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_becomeStrongEquipmentLevelInput;
    [AutoBind("./ClearStageButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_clearStageButton;
    [AutoBind("./ReportBugButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_openReportBugPanelButton;
    [AutoBind("./iPhoneXToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_iPhoneXToggle;
    [AutoBind("./ReloginButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_reloginButton;
    [AutoBind("./BecomeSuperAccountButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_becomeSuperAccountButton;
    [AutoBind("./TestToggles/BattleToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_battleToggle;
    [AutoBind("./TestToggles/DialogToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_dialogToggle;
    [AutoBind("./TestToggles/BattleDialogToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_battleDialogToggle;
    [AutoBind("./TestToggles/UserGuideDialogToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_userGuideDialogToggle;
    [AutoBind("./TestList/LoopVerticalScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private EasyObjectPool m_ListItemPool;
    [AutoBind("./TestList/LoopVerticalScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private LoopVerticalScrollRect m_LoopScroolRect;
    [AutoBind("./MonsterLevel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_monsterLevelGameObject;
    [AutoBind("./MonsterLevel/InputField", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_monsterLevelInputField;
    [AutoBind("./StartTestButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_startTestBattleButton;
    [AutoBind("./TestId/InputField", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_TestIdInputField;
    [AutoBind("./TestId/Next", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_TestIdNextSearch;
    [AutoBind("./TestId/Home", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_TestListHomeBtn;
    [AutoBind("./SystemInfo", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_systemInfoGameObject;
    [AutoBind("./SystemInfo/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_systemInfoBackgroundButton;
    [AutoBind("./SystemInfo/ScrollView/Viewport/Content/Text", AutoBindAttribute.InitState.NotInit, false)]
    private UnityEngine.UI.Text m_systemInfoText;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefabs/TestListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_testListItemPrefab;
    [AutoBind("./ServerDateTime", AutoBindAttribute.InitState.Active, false)]
    private UnityEngine.UI.Text m_serverDateTimeText;
    [AutoBind("./ReportBugPanel/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_closeReportBugPanelButton;
    [AutoBind("./ReportBugPanel", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_reportBugPanelGameObject;
    [AutoBind("./ReportBugPanel/Panel/LogTime/Selection/OneHour", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_oneHourLogTime;
    [AutoBind("./ReportBugPanel/Panel/LogTime/Selection/OneDay", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_oneDayLogTime;
    [AutoBind("./ReportBugPanel/Panel/LogTime/Selection/OneWeek", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_oneWeekLogTime;
    [AutoBind("./ReportBugPanel/Panel/Description/InputField", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_bugDescriptionInputField;
    [AutoBind("./ReportBugPanel/Panel/ReportButtons/Cancel", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_cancelReportBugButton;
    [AutoBind("./ReportBugPanel/Panel/ReportButtons/Submit", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_reportBugButton;
    [AutoBind("./FinishAllTasksButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_finishAllTasksButton;
    [AutoBind("./PeakTest1Button", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_peakArenaTest1Button;
    [AutoBind("./PeakTest2Button", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_peakArenaTest2Button;
    [AutoBind("./ClientGMPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_clientGMBPanel;
    [AutoBind("./ClientGMButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_clientGMButton;
    private List<TestListItemUIController> m_testListItemUIControllers;
    private bool m_isIgnoreToggleEvent;
    private List<KeyValuePair<int, string>> m_CacheTestList;
    private TestListItemUIController m_CurrSelectCtrl;
    private int m_nSearchTestListId;

    [MethodImpl((MethodImplOptions) 32768)]
    private TestUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTestListType(TestListType listType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddTestListItem(int id, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    public void ClearTestList()
    {
      this.m_CacheTestList.Clear();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void ScrollToItem(ScrollRect scrollRect, int itemCount, int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private TestListItemUIController GetSelectedTestListItemUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetMonsterLevel(int level)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMonsterLevel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetServerDataTime(DateTime dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowSystemInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string BuildSystemInfoText(bool textColor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void AppendLine(StringBuilder sb, bool textColor, string name, string value)
    {
      // ISSUE: unable to decompile the method.
    }

    public void HideSystemInfo()
    {
      this.m_systemInfoGameObject.SetActive(false);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitLoopScrollViewRect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPoolObjectCreated(string poolName, GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RefreshLoopVerticalScrollView(TestListType listType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTestItemFill(UIControllerBase ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTestItemClick(UIControllerBase ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DoToggle(bool on, TestListType listType)
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnBattleToggle(bool on)
    {
      this.DoToggle(on, TestListType.Battle);
    }

    private void OnDialogToggle(bool on)
    {
      this.DoToggle(on, TestListType.Dialog);
    }

    private void OnBattleDialogToggle(bool on)
    {
      this.DoToggle(on, TestListType.BattleDialog);
    }

    private void OnUserGuideDialogToggle(bool on)
    {
      this.DoToggle(on, TestListType.UserGuideDialog);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTestListHomeBtnClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTestIdNextBtnClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTestIdInput(string value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnStartTestButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSystemInfoButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGoddessDialogButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnClearUserGuideButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBecomeStrongButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnBecomeSuperAccountButtonClick()
    {
      this.StartCoroutine(this.Co_BecomeSuperAccount());
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_BecomeSuperAccount()
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnFinishAllTasksButtonClick()
    {
      this.StartCoroutine(this.FinishAllTasks());
    }

    private void OnReloginButtonClick()
    {
      LoginUITask.Relogin();
    }

    private void OnClientGMClick()
    {
      this.m_clientGMBPanel.SetActive(true);
    }

    private void OnPeakArenaTest1ButtonClick()
    {
      this.StartCoroutine(this.Co_PeakArenaTest1(1));
    }

    private void OnPeakArenaTest2ButtonClick()
    {
      this.StartCoroutine(this.Co_PeakArenaTest1(2));
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_PeakArenaTest1(int test)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator FinishAllTasks()
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnCancelReportBugButtonClick()
    {
      this.m_reportBugPanelGameObject.SetActive(false);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OniPhoneXToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReportBugButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private double GetReportBugLogHours()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnOpenReportBugPanelButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public static int MaxPlayerLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator UpgradePlayerLevel(int targetPlayerLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator SpeedUpAllHeros(int targetHeroLevel, int targetStarLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public static IEnumerator SetEnchantProperty(
      ulong equipmentInstanceId,
      string values,
      Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator BecomeStrong(
      int targetPlayerLevel,
      int targetStarLevel,
      int targetEquipmentLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public static IEnumerator TenLevelUpEquipment(
      ulong equipmentInstanceId,
      Action OnEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public static IEnumerator SpeedUpHero(
      int heroID,
      int targetHeroLevel,
      int targetStarLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public static IEnumerator ResetHero(int heroID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static List<int> GetHeroJobConnSequence(int heroID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static List<int> GetJobSequence(int rootJobConnectionID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int GetEquipmentLevelLimit(int equipmentStarLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool IsEquipismentMaxLevel(EquipmentBagItem equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator StarLevelUpTopRankEquipments(
      int ssrMaterialEquipmentId,
      int srMaterialEquipmentId)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator AddItem4StarLevelUpEquipment(ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator AddGoods(List<Goods> goods)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator StarLevelUpEquipment(ulong instanceId, ulong materialId)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator EnhanceAllTopRankEquipments(int targetLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator EnhanceEquipment(
      ulong instanceId,
      List<ulong> materialIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator AddAllTopRankEquipments()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator AddAllHeros()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator AddHero(List<int> heros)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator HeroBreak(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator BuyFixedStoreItem(
      int fixedStoreID,
      int fixedStoreItemGoodsID,
      int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator BuyFixedStoreItem(
      int fixedStoreID,
      int fixedStoreItemGoodsID)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator TransferHeroJob(int heroId, int jobConnectionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator AddAllItem(int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator UpgradeHeroJobLevel(Hero hero, int jobConnectionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator SendGMCommand(string cmd)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator UseItem(GoodsType type, int id, int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator AddHeroExp(
      int heroId,
      GoodsType itemType,
      int itemID,
      int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static string ErrorCodeToMessage(int code)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCompleteUserGuideButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnClearStageButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSystemInfoBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<TestListType> EventOnChangeTestList
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnStartTest
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowSystemInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnCloseSystemInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowGoddessDialog
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnClearUserGuide
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnCompleteUserGuide
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnClearStage
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string, double> EventOnReportBug
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
