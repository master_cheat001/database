﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ServerListUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class ServerListUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./BlackBGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_backgroundButton;
    [AutoBind("./Detail/ServerListPanel/ServerScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_liServerListGroup;
    [AutoBind("./Detail/AreaListPanel/ServerScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_areaGroup;
    [AutoBind("./Detail/CharListPanel/ServerScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_headGroup;
    [AutoBind("./Detail/CharListPanel/NoChar", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_noChar;
    [AutoBind("./Prefabs/ServerItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_serverListItemTemplate;
    [AutoBind("./Prefabs/CharItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_headItemTemplate;
    [AutoBind("./Prefabs/AreaItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_areaItemTemplate;
    [AutoBind("./Prefabs/ServerItem/ServerNameGroup/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_liServerNameText;
    [AutoBind("./Prefabs/ServerItem/NewBGImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_liNewServerIcon;
    [AutoBind("./Prefabs/ServerItem/StateImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_liServerStateIcon;
    [AutoBind("./Prefabs/ServerItem/CharGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_liServerCharIcon;
    [AutoBind("./Prefabs/ServerItem/CharGroup/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_liServerCharImage;
    [AutoBind("./Prefabs/ServerItem/CharGroup/LevelNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_liServerCharLevel;
    [AutoBind("./Prefabs/ServerItem/Maintenance", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_liMaitainServerIcon;
    private IConfigDataLoader m_configDataLoader;
    private Dictionary<string, List<LoginUITask.ServerInfo>> m_serverGroup;
    private List<LoginUITask.ServerInfo> m_servers;
    private GameObject m_selectArea;
    private int m_selectServerID;
    private List<LoginUITask.ExistCharInfo> m_existCharsInfo;
    private string m_roleListURL;
    private float m_fDebugReportTime;
    [DoNotToLua]
    private ServerListUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_SetServerListList`1List`1List`1Int32_hotfix;
    private LuaFunction m_ClearServerListItemGameObject_hotfix;
    private LuaFunction m_RefreshServerArea_hotfix;
    private LuaFunction m_RefreshPlayerHead_hotfix;
    private LuaFunction m_AddServerListItemGameObjectList`1_hotfix;
    private LuaFunction m_AddServerListItemGameObjectServerInfo_hotfix;
    private LuaFunction m_Open_hotfix;
    private LuaFunction m_Close_hotfix;
    private LuaFunction m_LastLoginTimeDescriptionInt32_hotfix;
    private LuaFunction m_Update_hotfix;
    private LuaFunction m_OnCloseButtonClicked_hotfix;
    private LuaFunction m_OnAreaClickGameObject_hotfix;
    private LuaFunction m_OnUIClosed_hotfix;
    private LuaFunction m_add_EventOnClosedAction`1_hotfix;
    private LuaFunction m_remove_EventOnClosedAction`1_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    private ServerListUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetServerList(
      List<LoginUITask.ServerInfo> serverList,
      List<LoginUITask.ServerInfo> recentLoginServerList,
      List<LoginUITask.ExistCharInfo> existCharsInfo,
      int selectServerID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearServerListItem(GameObject listGroup)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RefreshServerArea()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RefreshPlayerHead()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddServerListItem(GameObject listGroup, List<LoginUITask.ServerInfo> serverList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddServerListItem(GameObject listGroup, LoginUITask.ServerInfo server)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Close()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string LastLoginTimeDescription(int hour)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseButtonClicked()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAreaClick(GameObject areaItem)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnUIClosed()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnClosed
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public ServerListUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnClosed(int obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnClosed(int obj)
    {
      this.EventOnClosed = (Action<int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private ServerListUIController m_owner;

      public LuaExportHelper(ServerListUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnClosed(int obj)
      {
        this.m_owner.__callDele_EventOnClosed(obj);
      }

      public void __clearDele_EventOnClosed(int obj)
      {
        this.m_owner.__clearDele_EventOnClosed(obj);
      }

      public CommonUIStateController m_uiStateController
      {
        get
        {
          return this.m_owner.m_uiStateController;
        }
        set
        {
          this.m_owner.m_uiStateController = value;
        }
      }

      public Button m_backgroundButton
      {
        get
        {
          return this.m_owner.m_backgroundButton;
        }
        set
        {
          this.m_owner.m_backgroundButton = value;
        }
      }

      public GameObject m_liServerListGroup
      {
        get
        {
          return this.m_owner.m_liServerListGroup;
        }
        set
        {
          this.m_owner.m_liServerListGroup = value;
        }
      }

      public GameObject m_areaGroup
      {
        get
        {
          return this.m_owner.m_areaGroup;
        }
        set
        {
          this.m_owner.m_areaGroup = value;
        }
      }

      public GameObject m_headGroup
      {
        get
        {
          return this.m_owner.m_headGroup;
        }
        set
        {
          this.m_owner.m_headGroup = value;
        }
      }

      public GameObject m_noChar
      {
        get
        {
          return this.m_owner.m_noChar;
        }
        set
        {
          this.m_owner.m_noChar = value;
        }
      }

      public GameObject m_serverListItemTemplate
      {
        get
        {
          return this.m_owner.m_serverListItemTemplate;
        }
        set
        {
          this.m_owner.m_serverListItemTemplate = value;
        }
      }

      public GameObject m_headItemTemplate
      {
        get
        {
          return this.m_owner.m_headItemTemplate;
        }
        set
        {
          this.m_owner.m_headItemTemplate = value;
        }
      }

      public GameObject m_areaItemTemplate
      {
        get
        {
          return this.m_owner.m_areaItemTemplate;
        }
        set
        {
          this.m_owner.m_areaItemTemplate = value;
        }
      }

      public Text m_liServerNameText
      {
        get
        {
          return this.m_owner.m_liServerNameText;
        }
        set
        {
          this.m_owner.m_liServerNameText = value;
        }
      }

      public GameObject m_liNewServerIcon
      {
        get
        {
          return this.m_owner.m_liNewServerIcon;
        }
        set
        {
          this.m_owner.m_liNewServerIcon = value;
        }
      }

      public Image m_liServerStateIcon
      {
        get
        {
          return this.m_owner.m_liServerStateIcon;
        }
        set
        {
          this.m_owner.m_liServerStateIcon = value;
        }
      }

      public GameObject m_liServerCharIcon
      {
        get
        {
          return this.m_owner.m_liServerCharIcon;
        }
        set
        {
          this.m_owner.m_liServerCharIcon = value;
        }
      }

      public Image m_liServerCharImage
      {
        get
        {
          return this.m_owner.m_liServerCharImage;
        }
        set
        {
          this.m_owner.m_liServerCharImage = value;
        }
      }

      public Text m_liServerCharLevel
      {
        get
        {
          return this.m_owner.m_liServerCharLevel;
        }
        set
        {
          this.m_owner.m_liServerCharLevel = value;
        }
      }

      public GameObject m_liMaitainServerIcon
      {
        get
        {
          return this.m_owner.m_liMaitainServerIcon;
        }
        set
        {
          this.m_owner.m_liMaitainServerIcon = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public Dictionary<string, List<LoginUITask.ServerInfo>> m_serverGroup
      {
        get
        {
          return this.m_owner.m_serverGroup;
        }
        set
        {
          this.m_owner.m_serverGroup = value;
        }
      }

      public List<LoginUITask.ServerInfo> m_servers
      {
        get
        {
          return this.m_owner.m_servers;
        }
        set
        {
          this.m_owner.m_servers = value;
        }
      }

      public GameObject m_selectArea
      {
        get
        {
          return this.m_owner.m_selectArea;
        }
        set
        {
          this.m_owner.m_selectArea = value;
        }
      }

      public int m_selectServerID
      {
        get
        {
          return this.m_owner.m_selectServerID;
        }
        set
        {
          this.m_owner.m_selectServerID = value;
        }
      }

      public List<LoginUITask.ExistCharInfo> m_existCharsInfo
      {
        get
        {
          return this.m_owner.m_existCharsInfo;
        }
        set
        {
          this.m_owner.m_existCharsInfo = value;
        }
      }

      public string m_roleListURL
      {
        get
        {
          return this.m_owner.m_roleListURL;
        }
        set
        {
          this.m_owner.m_roleListURL = value;
        }
      }

      public float m_fDebugReportTime
      {
        get
        {
          return this.m_owner.m_fDebugReportTime;
        }
        set
        {
          this.m_owner.m_fDebugReportTime = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void ClearServerListItem(GameObject listGroup)
      {
        this.m_owner.ClearServerListItem(listGroup);
      }

      public void RefreshServerArea()
      {
        this.m_owner.RefreshServerArea();
      }

      public void RefreshPlayerHead()
      {
        this.m_owner.RefreshPlayerHead();
      }

      public void AddServerListItem(GameObject listGroup, List<LoginUITask.ServerInfo> serverList)
      {
        this.m_owner.AddServerListItem(listGroup, serverList);
      }

      public void AddServerListItem(GameObject listGroup, LoginUITask.ServerInfo server)
      {
        this.m_owner.AddServerListItem(listGroup, server);
      }

      public string LastLoginTimeDescription(int hour)
      {
        return this.m_owner.LastLoginTimeDescription(hour);
      }

      public void Update()
      {
        this.m_owner.Update();
      }

      public void OnCloseButtonClicked()
      {
        this.m_owner.OnCloseButtonClicked();
      }

      public void OnAreaClick(GameObject areaItem)
      {
        this.m_owner.OnAreaClick(areaItem);
      }

      public void OnUIClosed()
      {
        this.m_owner.OnUIClosed();
      }
    }
  }
}
