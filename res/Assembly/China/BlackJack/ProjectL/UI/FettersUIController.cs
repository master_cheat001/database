﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.FettersUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class FettersUIController : UIControllerBase
  {
    [AutoBind("./Margin", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_commonUIStateCtrl;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./HelpButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_helpButton;
    [AutoBind("./Margin/Fetters", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_fettersButton;
    [AutoBind("./Margin/Fetters/NewImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_fettersButtonNewImage;
    [AutoBind("./Margin/Fetters/PercentText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_fettersButtonPercentText;
    [AutoBind("./Margin/Information", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_informationButton;
    [AutoBind("./Margin/Information/NewImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_informationButtonNewImage;
    [AutoBind("./Favorability/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_favorabilityTotalValueText;
    [AutoBind("./HeroCharPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_charGameObjectRoot;
    [AutoBind("./Margin/HeroListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_scrollRect;
    [AutoBind("./Margin/HeroListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_scrollViewContent;
    [AutoBind("./Prefab/ListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_listItemPrefab;
    [AutoBind("./ChangeInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_infoPanelCommonUIStateCtrl;
    private List<FettersHeroListItemUIController> m_heroCtrlList;
    private HeroCharUIController m_heroCharUIController;
    private Hero m_hero;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private bool isFirstIn;
    [DoNotToLua]
    private FettersUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_OnDisable_hotfix;
    private LuaFunction m_UpdateViewInFettersHero_hotfix;
    private LuaFunction m_OnListItemClickHero_hotfix;
    private LuaFunction m_HeroListItemCompareHeroHero_hotfix;
    private LuaFunction m_CalcFetterFinishPercentHero_hotfix;
    private LuaFunction m_GoToInformationPanelBoolean_hotfix;
    private LuaFunction m_PlayHeroPerformanceInt32_hotfix;
    private LuaFunction m_OnReturnButtonClick_hotfix;
    private LuaFunction m_OnHelpButtonClick_hotfix;
    private LuaFunction m_OnInformationButtonClick_hotfix;
    private LuaFunction m_OnFettersButtonClick_hotfix;
    private LuaFunction m_ResetScrollViewPosition_hotfix;
    private LuaFunction m_add_EventOnReturnAction_hotfix;
    private LuaFunction m_remove_EventOnReturnAction_hotfix;
    private LuaFunction m_add_EventOnShowHelpAction_hotfix;
    private LuaFunction m_remove_EventOnShowHelpAction_hotfix;
    private LuaFunction m_add_EventOnListItemClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnListItemClickAction`1_hotfix;
    private LuaFunction m_add_EventOnFettersButtonClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnFettersButtonClickAction`1_hotfix;
    private LuaFunction m_add_EventOnInformationButtonClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnInformationButtonClickAction`1_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public FettersUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDisable()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateViewInFetters(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnListItemClick(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int HeroListItemCompare(Hero h1, Hero h2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CalcFetterFinishPercent(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void GoToInformationPanel(bool isGotoOrReturn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayHeroPerformance(int heroPerformanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHelpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInformationButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFettersButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ResetScrollViewPosition()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowHelp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<Hero> EventOnListItemClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<Hero> EventOnFettersButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<Hero> EventOnInformationButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public FettersUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnReturn()
    {
      this.EventOnReturn = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowHelp()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowHelp()
    {
      this.EventOnShowHelp = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnListItemClick(Hero obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnListItemClick(Hero obj)
    {
      this.EventOnListItemClick = (Action<Hero>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnFettersButtonClick(Hero obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnFettersButtonClick(Hero obj)
    {
      this.EventOnFettersButtonClick = (Action<Hero>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnInformationButtonClick(Hero obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnInformationButtonClick(Hero obj)
    {
      this.EventOnInformationButtonClick = (Action<Hero>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private FettersUIController m_owner;

      public LuaExportHelper(FettersUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnReturn()
      {
        this.m_owner.__callDele_EventOnReturn();
      }

      public void __clearDele_EventOnReturn()
      {
        this.m_owner.__clearDele_EventOnReturn();
      }

      public void __callDele_EventOnShowHelp()
      {
        this.m_owner.__callDele_EventOnShowHelp();
      }

      public void __clearDele_EventOnShowHelp()
      {
        this.m_owner.__clearDele_EventOnShowHelp();
      }

      public void __callDele_EventOnListItemClick(Hero obj)
      {
        this.m_owner.__callDele_EventOnListItemClick(obj);
      }

      public void __clearDele_EventOnListItemClick(Hero obj)
      {
        this.m_owner.__clearDele_EventOnListItemClick(obj);
      }

      public void __callDele_EventOnFettersButtonClick(Hero obj)
      {
        this.m_owner.__callDele_EventOnFettersButtonClick(obj);
      }

      public void __clearDele_EventOnFettersButtonClick(Hero obj)
      {
        this.m_owner.__clearDele_EventOnFettersButtonClick(obj);
      }

      public void __callDele_EventOnInformationButtonClick(Hero obj)
      {
        this.m_owner.__callDele_EventOnInformationButtonClick(obj);
      }

      public void __clearDele_EventOnInformationButtonClick(Hero obj)
      {
        this.m_owner.__clearDele_EventOnInformationButtonClick(obj);
      }

      public RectTransform m_marginTransform
      {
        get
        {
          return this.m_owner.m_marginTransform;
        }
        set
        {
          this.m_owner.m_marginTransform = value;
        }
      }

      public CommonUIStateController m_commonUIStateCtrl
      {
        get
        {
          return this.m_owner.m_commonUIStateCtrl;
        }
        set
        {
          this.m_owner.m_commonUIStateCtrl = value;
        }
      }

      public Button m_returnButton
      {
        get
        {
          return this.m_owner.m_returnButton;
        }
        set
        {
          this.m_owner.m_returnButton = value;
        }
      }

      public Button m_helpButton
      {
        get
        {
          return this.m_owner.m_helpButton;
        }
        set
        {
          this.m_owner.m_helpButton = value;
        }
      }

      public Button m_fettersButton
      {
        get
        {
          return this.m_owner.m_fettersButton;
        }
        set
        {
          this.m_owner.m_fettersButton = value;
        }
      }

      public GameObject m_fettersButtonNewImage
      {
        get
        {
          return this.m_owner.m_fettersButtonNewImage;
        }
        set
        {
          this.m_owner.m_fettersButtonNewImage = value;
        }
      }

      public Text m_fettersButtonPercentText
      {
        get
        {
          return this.m_owner.m_fettersButtonPercentText;
        }
        set
        {
          this.m_owner.m_fettersButtonPercentText = value;
        }
      }

      public Button m_informationButton
      {
        get
        {
          return this.m_owner.m_informationButton;
        }
        set
        {
          this.m_owner.m_informationButton = value;
        }
      }

      public GameObject m_informationButtonNewImage
      {
        get
        {
          return this.m_owner.m_informationButtonNewImage;
        }
        set
        {
          this.m_owner.m_informationButtonNewImage = value;
        }
      }

      public Text m_favorabilityTotalValueText
      {
        get
        {
          return this.m_owner.m_favorabilityTotalValueText;
        }
        set
        {
          this.m_owner.m_favorabilityTotalValueText = value;
        }
      }

      public GameObject m_charGameObjectRoot
      {
        get
        {
          return this.m_owner.m_charGameObjectRoot;
        }
        set
        {
          this.m_owner.m_charGameObjectRoot = value;
        }
      }

      public ScrollRect m_scrollRect
      {
        get
        {
          return this.m_owner.m_scrollRect;
        }
        set
        {
          this.m_owner.m_scrollRect = value;
        }
      }

      public GameObject m_scrollViewContent
      {
        get
        {
          return this.m_owner.m_scrollViewContent;
        }
        set
        {
          this.m_owner.m_scrollViewContent = value;
        }
      }

      public GameObject m_listItemPrefab
      {
        get
        {
          return this.m_owner.m_listItemPrefab;
        }
        set
        {
          this.m_owner.m_listItemPrefab = value;
        }
      }

      public CommonUIStateController m_infoPanelCommonUIStateCtrl
      {
        get
        {
          return this.m_owner.m_infoPanelCommonUIStateCtrl;
        }
        set
        {
          this.m_owner.m_infoPanelCommonUIStateCtrl = value;
        }
      }

      public List<FettersHeroListItemUIController> m_heroCtrlList
      {
        get
        {
          return this.m_owner.m_heroCtrlList;
        }
        set
        {
          this.m_owner.m_heroCtrlList = value;
        }
      }

      public HeroCharUIController m_heroCharUIController
      {
        get
        {
          return this.m_owner.m_heroCharUIController;
        }
        set
        {
          this.m_owner.m_heroCharUIController = value;
        }
      }

      public Hero m_hero
      {
        get
        {
          return this.m_owner.m_hero;
        }
        set
        {
          this.m_owner.m_hero = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public bool isFirstIn
      {
        get
        {
          return this.m_owner.isFirstIn;
        }
        set
        {
          this.m_owner.isFirstIn = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void OnDisable()
      {
        this.m_owner.OnDisable();
      }

      public void OnListItemClick(Hero hero)
      {
        this.m_owner.OnListItemClick(hero);
      }

      public int HeroListItemCompare(Hero h1, Hero h2)
      {
        return this.m_owner.HeroListItemCompare(h1, h2);
      }

      public void OnReturnButtonClick()
      {
        this.m_owner.OnReturnButtonClick();
      }

      public void OnHelpButtonClick()
      {
        this.m_owner.OnHelpButtonClick();
      }

      public void OnInformationButtonClick()
      {
        this.m_owner.OnInformationButtonClick();
      }

      public void OnFettersButtonClick()
      {
        this.m_owner.OnFettersButtonClick();
      }

      public void ResetScrollViewPosition()
      {
        this.m_owner.ResetScrollViewPosition();
      }
    }
  }
}
