﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.TeamRoomInviteeInfoGetNetTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Common;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class TeamRoomInviteeInfoGetNetTask : UINetTask
  {
    private List<string> m_inviteeUserIds;
    private TeamRoomInviteeInfoType m_infoType;

    [MethodImpl((MethodImplOptions) 32768)]
    public TeamRoomInviteeInfoGetNetTask(
      List<string> inviteeUserIds,
      TeamRoomInviteeInfoType infoType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool StartNetWorking()
    {
      // ISSUE: unable to decompile the method.
    }

    protected void OnTeamRoomInviteeLevelInfoGetAck(int result)
    {
      this.Result = result;
      this.OnTransactionComplete();
    }
  }
}
