﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.AncientCallBossUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class AncientCallBossUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./TopGroup/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_closeButton;
    [AutoBind("./Margin/BossDetailPanel/EvaluateGroupContent/EvaluateLetterItem", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_bossEvaluateImage;
    [AutoBind("./Margin/BossDetailPanel/HarmValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_bestDamageValueText;
    [AutoBind("./Margin/BossDetailPanel/ButtonGroup/RewardDetailButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_rewardDetailButton;
    [AutoBind("./Margin/BossDetailPanel/ButtonGroup/BossDetailButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_bossDetailButton;
    [AutoBind("./Margin/BossDetailPanel/ButtonGroup/AchievementListButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_achievementListButton;
    [AutoBind("./AttackButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_attackButton;
    [AutoBind("./Margin/BossDetailPanel/AdvantageGroup/Group", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_bossAdvantageGroup;
    [AutoBind("./Margin/BossDetailPanel/BossNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_bossNameText;
    [AutoBind("./Margin/LeftPanel/BGImages/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_rankTitleText;
    [AutoBind("./Margin/LeftPanel/PlayerGroupScollView/ViewPort/PlayerGroup", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_playerGroup;
    [AutoBind("./Margin/LeftPanel/PlayerGroupScollView/ViewPort/PlayerGroup/PlayerInfo", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_playerInfoPrefab;
    [AutoBind("./Margin/LeftPanel/DetailInfoToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_rankDetailButton;
    [AutoBind("./Margin/LeftPanel/RuleButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_scoreRuleButton;
    [AutoBind("./ScoreDescPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_scoreDescPanelUIStateCtrl;
    [AutoBind("./ScoreDescPanel/BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_scoreDescPanelBGButton;
    [AutoBind("./ScoreDescPanel/Detail/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_scoreDescPanelCloseButton;
    [AutoBind("./ScoreDescPanel/Detail/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_scoreDescPanelScrollRect;
    [AutoBind("./RewardInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_rewardInfoPanelUIStateCtrl;
    [AutoBind("./RewardInfoPanel/BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_rewardInfoPanelBGButton;
    [AutoBind("./RewardInfoPanel/Detail/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_rewardInfoPanelCloseButton;
    [AutoBind("./RewardInfoPanel/Detail/RewardListItemScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_rewardInfoPanelScrollRect;
    [AutoBind("./AchievementPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_achievementPanelUIStateCtrl;
    [AutoBind("./AchievementPanel/BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_achievementPanelBGButton;
    [AutoBind("./AchievementPanel/Detail/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_achievementPanelCloseButton;
    [AutoBind("./AchievementPanel/Detail/AchievementScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_achievementPanelScrollRect;
    [AutoBind("./AchievementPanel/Detail/TopDetail/DamageValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_achievementPanelDamageValueText;
    [AutoBind("./AchievementPanel/Detail/TopDetail/EvaluateGroupContent/EvaluateLetterItem", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_achievementPanelEvaluateImage;
    [AutoBind("./AchievementPanel/Detail/TopDetail/RankValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_achievementPanelBestRankingValueText;
    [AutoBind("./AchievementPanel/Detail/TopDetail/RankValueImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_achievementPanelBestRankValueImage;
    [AutoBind("./Prefab/AchievementItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_achievementItemPrefabGo;
    private ConfigDataAncientCallBossInfo m_bossInfo;
    private List<AncientCallPlayerInfoItemUIController> m_rankPlayerItemUIControllerList;

    [MethodImpl((MethodImplOptions) 32768)]
    public AncientCallBossUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowAncientCallBoss(ConfigDataAncientCallBossInfo bossInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetTagInfoPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnScoreRuleButtonClick()
    {
      this.ShowScoreRuleDescPanel();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowScoreRuleDescPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseScoreRulePanel()
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnRewardDetailButtonClick()
    {
      this.ShowAchievementPanel();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowAchievementPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CloseAchievementPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnAchievementListButtonClick()
    {
      this.ShowAchievementListPanel();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowAchievementListPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGetMissionReward(int missionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseAcievementListPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRankDetailButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetBossRankList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OpenTagPanel(ConfigDataHeroTagInfo tagInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBossDetailButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAttackButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnAttack
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowBossDetail
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnGetMissionReward
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
