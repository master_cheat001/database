﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BattleDialogUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime;
using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Misc;
using SLua;
using System;
using System.Collections;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class BattleDialogUIController : UIControllerBase
  {
    [AutoBind("./SkipButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_skipButton;
    [AutoBind("./BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_backgroundButton;
    [AutoBind("./WordPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_wordsGameObject;
    [AutoBind("./Char/0", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_char0GameObject;
    [AutoBind("./Char/1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_char1GameObject;
    private DialogCharUIController[] m_dialogCharUIControllers;
    private BattleDialogBoxUIController m_dialogBoxUIController;
    private IAudioPlayback m_currentAudio;
    private ConfigDataBattleDialogInfo m_dialogInfo;
    private float m_showDialogBoxTime;

    [MethodImpl((MethodImplOptions) 32768)]
    private BattleDialogUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDialog(ConfigDataBattleDialogInfo dialogInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    public void CloseDialog()
    {
      this.StartCoroutine(this.Co_CloseDialog());
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_CloseDialog()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowDialogBox()
    {
      // ISSUE: unable to decompile the method.
    }

    private IAudioPlayback PlayVoice(string name)
    {
      return AudioUtility.PlaySound(name);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StopVoice()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_NextDialog()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDialogBoxLogButton(bool bShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSkipButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnLogButtonClick(UIControllerBase uIControllerBase)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnSkip
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnNext
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnLog
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
