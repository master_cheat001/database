﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.TeamRoomInfoUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class TeamRoomInfoUITask : UITask
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private TeamRoomInfoUIController m_teamRoomInfoUIController;
    private PlayerResourceUIController m_playerResourceUIController;
    private int m_nowSeconds;
    private List<TeamRoomPlayer> m_teamRoomPlayers;
    private bool m_isLeader;
    private PlayerSimpleInfoUITask m_playerSimpleInfoUITask;
    private bool m_roomPlayerChangeNtfsRegistered;
    [DoNotToLua]
    private TeamRoomInfoUITask.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctorString_hotfix;
    private LuaFunction m_OnStartUIIntent_hotfix;
    private LuaFunction m_OnStop_hotfix;
    private LuaFunction m_OnPause_hotfix;
    private LuaFunction m_OnResumeUIIntent_hotfix;
    private LuaFunction m_IsNeedLoadDynamicRes_hotfix;
    private LuaFunction m_InitAllUIControllers_hotfix;
    private LuaFunction m_ClearAllContextAndRes_hotfix;
    private LuaFunction m_RegisterPlayerContextEvents_hotfix;
    private LuaFunction m_UnregisterPlayerContextEvents_hotfix;
    private LuaFunction m_RegisterRoomPlayerChangeNtfs_hotfix;
    private LuaFunction m_UnregisterRoomPlayerChangeNtfs_hotfix;
    private LuaFunction m_InitTeamRoomInfoUIController_hotfix;
    private LuaFunction m_UninitTeamRoomInfoUIController_hotfix;
    private LuaFunction m_OnTick_hotfix;
    private LuaFunction m_UpdateView_hotfix;
    private LuaFunction m_UpdateQuitCountdown_hotfix;
    private LuaFunction m_Close_hotfix;
    private LuaFunction m_StartTeamRoomInviteUITask_hotfix;
    private LuaFunction m_TeamRoomInviteUITask_OnPrepareEndBoolean_hotfix;
    private LuaFunction m_TeamRoomInfoUIController_OnLeave_hotfix;
    private LuaFunction m_TeamRoomInfoUIController_OnStart_hotfix;
    private LuaFunction m_TeamRoomInfoUIController_OnShowChangePlayerPosition_hotfix;
    private LuaFunction m_TeamRoomInfoUIController_OnChangePlayerPositionComplete_hotfix;
    private LuaFunction m_TeamRoomInfoUIController_OnShowChat_hotfix;
    private LuaFunction m_TeamRoomInfoUIController_OnChangeAuthorityTeamRoomAuthority_hotfix;
    private LuaFunction m_TeamRoomInfoUIController_OnShowInvite_hotfix;
    private LuaFunction m_TeamRoomInfoUIController_OnShowPlayerInfoInt32RectTransform_hotfix;
    private LuaFunction m_ShowPlayerSimpleInfoRectTransform_hotfix;
    private LuaFunction m_PlayerSimpleInfoUITask_OnClose_hotfix;
    private LuaFunction m_PlayerSimpleInfoUITask_OnPrivateChatButtonClickBusinessCard_hotfix;
    private LuaFunction m_PlayerContext_OnTeamRoomPlayerJoinNtfTeamRoomPlayer_hotfix;
    private LuaFunction m_PlayerContext_OnTeamRoomPlayerQuitNtfTeamRoomPlayer_hotfix;
    private LuaFunction m_PlayerContext_OnTeamRoomPlayerPositionChangeNtf_hotfix;
    private LuaFunction m_PlayerContext_OnTeamRoomSelfKickOutNtfInt32_hotfix;
    private LuaFunction m_PlayerContext_OnTeamRoomAuthorityChangeNtfTeamRoomAuthority_hotfix;
    private LuaFunction m_PlayerContext_OnTeamRoomInvitationRefusedNtfInt32String_hotfix;
    private LuaFunction m_PlayerContext_OnChatMessageNtfChatMessage_hotfix;
    private LuaFunction m_PlayerContext_OnPlayerInfoInitEnd_hotfix;
    private LuaFunction m_add_EventOnCloseAction_hotfix;
    private LuaFunction m_remove_EventOnCloseAction_hotfix;
    private LuaFunction m_get_LayerDescArray_hotfix;
    private LuaFunction m_get_UICtrlDescArray_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public TeamRoomInfoUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnPause()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnResume(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterPlayerContextEvents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterPlayerContextEvents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RegisterRoomPlayerChangeNtfs()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UnregisterRoomPlayerChangeNtfs()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitTeamRoomInfoUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UninitTeamRoomInfoUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnTick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateQuitCountdown()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Close()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartTeamRoomInviteUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamRoomInviteUITask_OnPrepareEnd(bool ret)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamRoomInfoUIController_OnLeave()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamRoomInfoUIController_OnStart()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamRoomInfoUIController_OnShowChangePlayerPosition()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamRoomInfoUIController_OnChangePlayerPositionComplete()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamRoomInfoUIController_OnShowChat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamRoomInfoUIController_OnChangeAuthority(TeamRoomAuthority authority)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamRoomInfoUIController_OnShowInvite()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamRoomInfoUIController_OnShowPlayerInfo(int index, RectTransform rt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowPlayerSimpleInfo(RectTransform rt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerSimpleInfoUITask_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerSimpleInfoUITask_OnPrivateChatButtonClick(BusinessCard playerInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnTeamRoomPlayerJoinNtf(TeamRoomPlayer player)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnTeamRoomPlayerQuitNtf(TeamRoomPlayer player)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnTeamRoomPlayerPositionChangeNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnTeamRoomSelfKickOutNtf(int reason)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnTeamRoomAuthorityChangeNtf(TeamRoomAuthority authority)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnTeamRoomInvitationRefusedNtf(int result, string inviteeName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnChatMessageNtf(ChatMessage msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnPlayerInfoInitEnd()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public TeamRoomInfoUITask.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private bool __callBase_OnStart(UIIntent intent)
    {
      return base.OnStart(intent);
    }

    private void __callBase_OnStop()
    {
      base.OnStop();
    }

    private void __callBase_OnPause()
    {
      base.OnPause();
    }

    private bool __callBase_OnResume(UIIntent intent)
    {
      return base.OnResume(intent);
    }

    private bool __callBase_OnNewIntent(UIIntent intent)
    {
      return this.OnNewIntent(intent);
    }

    private void __callBase_EnableUIInput(bool isEnable, bool? isGlobalEnable)
    {
      this.EnableUIInput(isEnable, isGlobalEnable);
    }

    private bool __callBase_IsNeedLoadDynamicRes()
    {
      return base.IsNeedLoadDynamicRes();
    }

    private List<string> __callBase_CollectAllDynamicResForLoad()
    {
      return this.CollectAllDynamicResForLoad();
    }

    private void __callBase_RegisterPlayerContextEvents()
    {
      base.RegisterPlayerContextEvents();
    }

    private void __callBase_UnregisterPlayerContextEvents()
    {
      base.UnregisterPlayerContextEvents();
    }

    private void __callBase_PostUpdateView()
    {
      this.PostUpdateView();
    }

    private void __callBase_CollectPreloadResourceList()
    {
      this.CollectPreloadResourceList();
    }

    private void __callBase_ClearAssetList()
    {
      this.ClearAssetList();
    }

    private void __callBase_CollectAsset(string assetName)
    {
      this.CollectAsset(assetName);
    }

    private void __callBase_CollectSpriteAsset(string assetName)
    {
      this.CollectSpriteAsset(assetName);
    }

    private void __callBase_CollectFxAsset(string assetName)
    {
      this.CollectFxAsset(assetName);
    }

    private void __callBase_StartLoadCustomAssets(Action onLoadCompleted)
    {
      this.StartLoadCustomAssets(onLoadCompleted);
    }

    private bool __callBase_IsLoadingCustomAssets()
    {
      return this.IsLoadingCustomAssets();
    }

    private bool __callBase_IsPipeLineRunning()
    {
      return this.IsPipeLineRunning();
    }

    private bool __callBase_IsOpeningUI()
    {
      return this.IsOpeningUI();
    }

    private UITaskBase __callBase_ReturnPrevUITask()
    {
      return this.ReturnPrevUITask();
    }

    private void __callBase_ClearUnusedDynamicResourceCache()
    {
      this.ClearUnusedDynamicResourceCache();
    }

    private void __callBase_ClearDynamicResourceCache(List<string> assets)
    {
      this.ClearDynamicResourceCache(assets);
    }

    private void __callBase_OnMemoryWarning()
    {
      this.OnMemoryWarning();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnClose()
    {
      this.EventOnClose = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private TeamRoomInfoUITask m_owner;

      public LuaExportHelper(TeamRoomInfoUITask owner)
      {
        this.m_owner = owner;
      }

      public bool __callBase_OnStart(UIIntent intent)
      {
        return this.m_owner.__callBase_OnStart(intent);
      }

      public void __callBase_OnStop()
      {
        this.m_owner.__callBase_OnStop();
      }

      public void __callBase_OnPause()
      {
        this.m_owner.__callBase_OnPause();
      }

      public bool __callBase_OnResume(UIIntent intent)
      {
        return this.m_owner.__callBase_OnResume(intent);
      }

      public bool __callBase_OnNewIntent(UIIntent intent)
      {
        return this.m_owner.__callBase_OnNewIntent(intent);
      }

      public void __callBase_EnableUIInput(bool isEnable, bool? isGlobalEnable)
      {
        this.m_owner.__callBase_EnableUIInput(isEnable, isGlobalEnable);
      }

      public bool __callBase_IsNeedLoadDynamicRes()
      {
        return this.m_owner.__callBase_IsNeedLoadDynamicRes();
      }

      public List<string> __callBase_CollectAllDynamicResForLoad()
      {
        return this.m_owner.__callBase_CollectAllDynamicResForLoad();
      }

      public void __callBase_RegisterPlayerContextEvents()
      {
        this.m_owner.__callBase_RegisterPlayerContextEvents();
      }

      public void __callBase_UnregisterPlayerContextEvents()
      {
        this.m_owner.__callBase_UnregisterPlayerContextEvents();
      }

      public void __callBase_PostUpdateView()
      {
        this.m_owner.__callBase_PostUpdateView();
      }

      public void __callBase_CollectPreloadResourceList()
      {
        this.m_owner.__callBase_CollectPreloadResourceList();
      }

      public void __callBase_ClearAssetList()
      {
        this.m_owner.__callBase_ClearAssetList();
      }

      public void __callBase_CollectAsset(string assetName)
      {
        this.m_owner.__callBase_CollectAsset(assetName);
      }

      public void __callBase_CollectSpriteAsset(string assetName)
      {
        this.m_owner.__callBase_CollectSpriteAsset(assetName);
      }

      public void __callBase_CollectFxAsset(string assetName)
      {
        this.m_owner.__callBase_CollectFxAsset(assetName);
      }

      public void __callBase_StartLoadCustomAssets(Action onLoadCompleted)
      {
        this.m_owner.__callBase_StartLoadCustomAssets(onLoadCompleted);
      }

      public bool __callBase_IsLoadingCustomAssets()
      {
        return this.m_owner.__callBase_IsLoadingCustomAssets();
      }

      public bool __callBase_IsPipeLineRunning()
      {
        return this.m_owner.__callBase_IsPipeLineRunning();
      }

      public bool __callBase_IsOpeningUI()
      {
        return this.m_owner.__callBase_IsOpeningUI();
      }

      public UITaskBase __callBase_ReturnPrevUITask()
      {
        return this.m_owner.__callBase_ReturnPrevUITask();
      }

      public void __callBase_ClearUnusedDynamicResourceCache()
      {
        this.m_owner.__callBase_ClearUnusedDynamicResourceCache();
      }

      public void __callBase_ClearDynamicResourceCache(List<string> assets)
      {
        this.m_owner.__callBase_ClearDynamicResourceCache(assets);
      }

      public void __callBase_OnMemoryWarning()
      {
        this.m_owner.__callBase_OnMemoryWarning();
      }

      public void __callDele_EventOnClose()
      {
        this.m_owner.__callDele_EventOnClose();
      }

      public void __clearDele_EventOnClose()
      {
        this.m_owner.__clearDele_EventOnClose();
      }

      public UITaskBase.LayerDesc[] m_layerDescArray
      {
        get
        {
          return this.m_owner.m_layerDescArray;
        }
        set
        {
          this.m_owner.m_layerDescArray = value;
        }
      }

      public UITaskBase.UIControllerDesc[] m_uiCtrlDescArray
      {
        get
        {
          return this.m_owner.m_uiCtrlDescArray;
        }
        set
        {
          this.m_owner.m_uiCtrlDescArray = value;
        }
      }

      public TeamRoomInfoUIController m_teamRoomInfoUIController
      {
        get
        {
          return this.m_owner.m_teamRoomInfoUIController;
        }
        set
        {
          this.m_owner.m_teamRoomInfoUIController = value;
        }
      }

      public PlayerResourceUIController m_playerResourceUIController
      {
        get
        {
          return this.m_owner.m_playerResourceUIController;
        }
        set
        {
          this.m_owner.m_playerResourceUIController = value;
        }
      }

      public int m_nowSeconds
      {
        get
        {
          return this.m_owner.m_nowSeconds;
        }
        set
        {
          this.m_owner.m_nowSeconds = value;
        }
      }

      public List<TeamRoomPlayer> m_teamRoomPlayers
      {
        get
        {
          return this.m_owner.m_teamRoomPlayers;
        }
        set
        {
          this.m_owner.m_teamRoomPlayers = value;
        }
      }

      public bool m_isLeader
      {
        get
        {
          return this.m_owner.m_isLeader;
        }
        set
        {
          this.m_owner.m_isLeader = value;
        }
      }

      public PlayerSimpleInfoUITask m_playerSimpleInfoUITask
      {
        get
        {
          return this.m_owner.m_playerSimpleInfoUITask;
        }
        set
        {
          this.m_owner.m_playerSimpleInfoUITask = value;
        }
      }

      public bool m_roomPlayerChangeNtfsRegistered
      {
        get
        {
          return this.m_owner.m_roomPlayerChangeNtfsRegistered;
        }
        set
        {
          this.m_owner.m_roomPlayerChangeNtfsRegistered = value;
        }
      }

      public UITaskBase.LayerDesc[] LayerDescArray
      {
        get
        {
          return this.m_owner.LayerDescArray;
        }
      }

      public UITaskBase.UIControllerDesc[] UICtrlDescArray
      {
        get
        {
          return this.m_owner.UICtrlDescArray;
        }
      }

      public bool OnStart(UIIntent intent)
      {
        return this.m_owner.OnStart(intent);
      }

      public void OnStop()
      {
        this.m_owner.OnStop();
      }

      public void OnPause()
      {
        this.m_owner.OnPause();
      }

      public bool OnResume(UIIntent intent)
      {
        return this.m_owner.OnResume(intent);
      }

      public bool IsNeedLoadDynamicRes()
      {
        return this.m_owner.IsNeedLoadDynamicRes();
      }

      public void InitAllUIControllers()
      {
        this.m_owner.InitAllUIControllers();
      }

      public void ClearAllContextAndRes()
      {
        this.m_owner.ClearAllContextAndRes();
      }

      public void RegisterPlayerContextEvents()
      {
        this.m_owner.RegisterPlayerContextEvents();
      }

      public void UnregisterPlayerContextEvents()
      {
        this.m_owner.UnregisterPlayerContextEvents();
      }

      public void RegisterRoomPlayerChangeNtfs()
      {
        this.m_owner.RegisterRoomPlayerChangeNtfs();
      }

      public void UnregisterRoomPlayerChangeNtfs()
      {
        this.m_owner.UnregisterRoomPlayerChangeNtfs();
      }

      public void InitTeamRoomInfoUIController()
      {
        this.m_owner.InitTeamRoomInfoUIController();
      }

      public void UninitTeamRoomInfoUIController()
      {
        this.m_owner.UninitTeamRoomInfoUIController();
      }

      public void OnTick()
      {
        this.m_owner.OnTick();
      }

      public void UpdateView()
      {
        this.m_owner.UpdateView();
      }

      public void UpdateQuitCountdown()
      {
        this.m_owner.UpdateQuitCountdown();
      }

      public void Close()
      {
        this.m_owner.Close();
      }

      public void StartTeamRoomInviteUITask()
      {
        this.m_owner.StartTeamRoomInviteUITask();
      }

      public void TeamRoomInviteUITask_OnPrepareEnd(bool ret)
      {
        this.m_owner.TeamRoomInviteUITask_OnPrepareEnd(ret);
      }

      public void TeamRoomInfoUIController_OnLeave()
      {
        this.m_owner.TeamRoomInfoUIController_OnLeave();
      }

      public void TeamRoomInfoUIController_OnStart()
      {
        this.m_owner.TeamRoomInfoUIController_OnStart();
      }

      public void TeamRoomInfoUIController_OnShowChangePlayerPosition()
      {
        this.m_owner.TeamRoomInfoUIController_OnShowChangePlayerPosition();
      }

      public void TeamRoomInfoUIController_OnChangePlayerPositionComplete()
      {
        this.m_owner.TeamRoomInfoUIController_OnChangePlayerPositionComplete();
      }

      public void TeamRoomInfoUIController_OnShowChat()
      {
        this.m_owner.TeamRoomInfoUIController_OnShowChat();
      }

      public void TeamRoomInfoUIController_OnChangeAuthority(TeamRoomAuthority authority)
      {
        this.m_owner.TeamRoomInfoUIController_OnChangeAuthority(authority);
      }

      public void TeamRoomInfoUIController_OnShowInvite()
      {
        this.m_owner.TeamRoomInfoUIController_OnShowInvite();
      }

      public void TeamRoomInfoUIController_OnShowPlayerInfo(int index, RectTransform rt)
      {
        this.m_owner.TeamRoomInfoUIController_OnShowPlayerInfo(index, rt);
      }

      public void ShowPlayerSimpleInfo(RectTransform rt)
      {
        this.m_owner.ShowPlayerSimpleInfo(rt);
      }

      public void PlayerSimpleInfoUITask_OnClose()
      {
        this.m_owner.PlayerSimpleInfoUITask_OnClose();
      }

      public void PlayerSimpleInfoUITask_OnPrivateChatButtonClick(BusinessCard playerInfo)
      {
        this.m_owner.PlayerSimpleInfoUITask_OnPrivateChatButtonClick(playerInfo);
      }

      public void PlayerContext_OnTeamRoomPlayerJoinNtf(TeamRoomPlayer player)
      {
        this.m_owner.PlayerContext_OnTeamRoomPlayerJoinNtf(player);
      }

      public void PlayerContext_OnTeamRoomPlayerQuitNtf(TeamRoomPlayer player)
      {
        this.m_owner.PlayerContext_OnTeamRoomPlayerQuitNtf(player);
      }

      public void PlayerContext_OnTeamRoomPlayerPositionChangeNtf()
      {
        this.m_owner.PlayerContext_OnTeamRoomPlayerPositionChangeNtf();
      }

      public void PlayerContext_OnTeamRoomSelfKickOutNtf(int reason)
      {
        this.m_owner.PlayerContext_OnTeamRoomSelfKickOutNtf(reason);
      }

      public void PlayerContext_OnTeamRoomAuthorityChangeNtf(TeamRoomAuthority authority)
      {
        this.m_owner.PlayerContext_OnTeamRoomAuthorityChangeNtf(authority);
      }

      public void PlayerContext_OnTeamRoomInvitationRefusedNtf(int result, string inviteeName)
      {
        this.m_owner.PlayerContext_OnTeamRoomInvitationRefusedNtf(result, inviteeName);
      }

      public void PlayerContext_OnChatMessageNtf(ChatMessage msg)
      {
        this.m_owner.PlayerContext_OnChatMessageNtf(msg);
      }

      public void PlayerContext_OnPlayerInfoInitEnd()
      {
        this.m_owner.PlayerContext_OnPlayerInfoInitEnd();
      }
    }
  }
}
