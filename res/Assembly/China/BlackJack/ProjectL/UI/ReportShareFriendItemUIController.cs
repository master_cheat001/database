﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ReportShareFriendItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class ReportShareFriendItemUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_itemButton;
    [AutoBind("./StateGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_friendOnlineUIStateController;
    [AutoBind("./StateGroup/OnLineText2", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_friendOnlineLongText;
    [AutoBind("./PlayerIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_friendIconImage;
    [AutoBind("./PlayerIconImageGrey", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_friendIconGreyImage;
    [AutoBind("./HeadFrameDummy", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_friendHeadFrameTransform;
    [AutoBind("./SelectToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_selectToggle;
    [AutoBind("./LevelText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_friendLevelText;
    [AutoBind("./PlayerNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_friendNameText;
    [AutoBind("./ServerText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_serverText;
    private string m_userID;
    private UserSummary m_userSummy;
    private IConfigDataLoader m_configDataLoader;
    private ProjectLPlayerContext m_playerContext;
    [DoNotToLua]
    private ReportShareFriendItemUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_SetFriendInfoUserSummary_hotfix;
    private LuaFunction m_OnSelectToggleValueChangedBoolean_hotfix;
    private LuaFunction m_add_EventOnSelectToggleValueChangedAction`2_hotfix;
    private LuaFunction m_remove_EventOnSelectToggleValueChangedAction`2_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ReportShareFriendItemUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetFriendInfo(UserSummary userSummy)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSelectToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<bool, string> EventOnSelectToggleValueChanged
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public ReportShareFriendItemUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnSelectToggleValueChanged(bool arg1, string arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnSelectToggleValueChanged(bool arg1, string arg2)
    {
      this.EventOnSelectToggleValueChanged = (Action<bool, string>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private ReportShareFriendItemUIController m_owner;

      public LuaExportHelper(ReportShareFriendItemUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnSelectToggleValueChanged(bool arg1, string arg2)
      {
        this.m_owner.__callDele_EventOnSelectToggleValueChanged(arg1, arg2);
      }

      public void __clearDele_EventOnSelectToggleValueChanged(bool arg1, string arg2)
      {
        this.m_owner.__clearDele_EventOnSelectToggleValueChanged(arg1, arg2);
      }

      public Button m_itemButton
      {
        get
        {
          return this.m_owner.m_itemButton;
        }
        set
        {
          this.m_owner.m_itemButton = value;
        }
      }

      public CommonUIStateController m_friendOnlineUIStateController
      {
        get
        {
          return this.m_owner.m_friendOnlineUIStateController;
        }
        set
        {
          this.m_owner.m_friendOnlineUIStateController = value;
        }
      }

      public Text m_friendOnlineLongText
      {
        get
        {
          return this.m_owner.m_friendOnlineLongText;
        }
        set
        {
          this.m_owner.m_friendOnlineLongText = value;
        }
      }

      public Image m_friendIconImage
      {
        get
        {
          return this.m_owner.m_friendIconImage;
        }
        set
        {
          this.m_owner.m_friendIconImage = value;
        }
      }

      public Image m_friendIconGreyImage
      {
        get
        {
          return this.m_owner.m_friendIconGreyImage;
        }
        set
        {
          this.m_owner.m_friendIconGreyImage = value;
        }
      }

      public Transform m_friendHeadFrameTransform
      {
        get
        {
          return this.m_owner.m_friendHeadFrameTransform;
        }
        set
        {
          this.m_owner.m_friendHeadFrameTransform = value;
        }
      }

      public Toggle m_selectToggle
      {
        get
        {
          return this.m_owner.m_selectToggle;
        }
        set
        {
          this.m_owner.m_selectToggle = value;
        }
      }

      public Text m_friendLevelText
      {
        get
        {
          return this.m_owner.m_friendLevelText;
        }
        set
        {
          this.m_owner.m_friendLevelText = value;
        }
      }

      public Text m_friendNameText
      {
        get
        {
          return this.m_owner.m_friendNameText;
        }
        set
        {
          this.m_owner.m_friendNameText = value;
        }
      }

      public Text m_serverText
      {
        get
        {
          return this.m_owner.m_serverText;
        }
        set
        {
          this.m_owner.m_serverText = value;
        }
      }

      public string m_userID
      {
        get
        {
          return this.m_owner.m_userID;
        }
        set
        {
          this.m_owner.m_userID = value;
        }
      }

      public UserSummary m_userSummy
      {
        get
        {
          return this.m_owner.m_userSummy;
        }
        set
        {
          this.m_owner.m_userSummy = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void OnSelectToggleValueChanged(bool isOn)
      {
        this.m_owner.OnSelectToggleValueChanged(isOn);
      }
    }
  }
}
