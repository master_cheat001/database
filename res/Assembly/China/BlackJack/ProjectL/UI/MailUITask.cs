﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.MailUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class MailUITask : UITask
  {
    private ProjectLPlayerContext m_playerCtx;
    private MailListUIController m_mailListUICtrl;
    private List<Mail> m_mailInfoCache;
    private Mail m_currentSelectedMail;
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;

    [MethodImpl((MethodImplOptions) 32768)]
    public MailUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedUpdateDataCache()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateDataCache()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int MailCompare(Mail mailA, Mail mailB)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnResume(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void MailListUIController_OnMailItemButtonClick(Mail mail)
    {
      // ISSUE: unable to decompile the method.
    }

    private void MailListUIController_OnCloseButtonClick()
    {
      this.Pause();
      this.ReturnPrevUITask();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void AppendGoodsToList(Goods goods, List<Goods> goodsList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void MailListUIController_OnGetAllAttachments()
    {
      // ISSUE: unable to decompile the method.
    }

    private void MailDetailUIController_OnGotoButtonClick(Mail obj)
    {
      throw new NotImplementedException();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void MailDetailUIController_OnGetAttachmentButtonClick(Mail mail)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HandleGetMailError(int result)
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnMailsChangedNtf()
    {
      this.StartUpdatePipeLine((UIIntent) null, false, false, true);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMailReadAck(int result)
    {
      // ISSUE: unable to decompile the method.
    }

    protected bool IsPipelineStateMaskNeedUpdate(MailUITask.PipeLineStateMaskType state)
    {
      return this.m_currPipeLineCtx.IsNeedUpdate((int) state);
    }

    protected void EnablePipelineStateMask(MailUITask.PipeLineStateMaskType state)
    {
      this.m_currPipeLineCtx.AddUpdateMask((int) state);
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      get
      {
        return this.m_layerDescArray;
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      get
      {
        return this.m_uiCtrlDescArray;
      }
    }

    public enum PipeLineStateMaskType
    {
      AllRefresh,
      ForUIClick,
      IsReadGetMailAttachment,
    }
  }
}
