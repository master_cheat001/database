﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.CollectionActivityUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.Scene;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class CollectionActivityUITask : UITask, ICollectionActivityWorldListener
  {
    public const string ParamKey_CollectionActivityInstanceId = "CollectionActivityInstanceId";
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private CollectionActivityUIController m_collectionActivityUIController;
    private PlayerResourceUIController m_playerResourceUIController;
    private CollectionActivityWorld m_collectionActivityWorld;
    private List<int> m_movePath;
    private int m_nowSeconds;
    private IConfigDataLoader m_configDataLoader;
    private ConfigDataCollectionActivityInfo m_collectionActivityInfo;
    private int m_needCollectionScenarioHandleLevelId;
    private bool m_isAfterBattle;
    private bool m_isEnterFirstScenarioFail;
    private bool m_needUpdateEventActors;
    [DoNotToLua]
    private CollectionActivityUITask.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctorString_hotfix;
    private LuaFunction m_OnStartUIIntent_hotfix;
    private LuaFunction m_OnResumeUIIntent_hotfix;
    private LuaFunction m_OnStop_hotfix;
    private LuaFunction m_CreateCollectionActivityWorld_hotfix;
    private LuaFunction m_DestroyCollectionActivityWorld_hotfix;
    private LuaFunction m_StartCollectionActivityWorld_hotfix;
    private LuaFunction m_InitDataFromUIIntentUIIntent_hotfix;
    private LuaFunction m_IsNeedLoadDynamicRes_hotfix;
    private LuaFunction m_CollectPlayerAssets_hotfix;
    private LuaFunction m_CollectEventAssets_hotfix;
    private LuaFunction m_InitLayerStateOnLoadAllResCompleted_hotfix;
    private LuaFunction m_InitAllUIControllers_hotfix;
    private LuaFunction m_ClearAllContextAndRes_hotfix;
    private LuaFunction m_RegisterPlayerContextEvents_hotfix;
    private LuaFunction m_UnregisterPlayerContextEvents_hotfix;
    private LuaFunction m_InitCollectionActivityUIController_hotfix;
    private LuaFunction m_UninitCollectionActivityUIController_hotfix;
    private LuaFunction m_UpdateView_hotfix;
    private LuaFunction m_PostUpdateView_hotfix;
    private LuaFunction m_CheckEnterFirstScenarioLevel_hotfix;
    private LuaFunction m_CheckChangeActiveScenarioLevel_hotfix;
    private LuaFunction m_UpdateActiveScenarioAndWaypointState_hotfix;
    private LuaFunction m_UpdateActiveEventState_hotfix;
    private LuaFunction m_OnTick_hotfix;
    private LuaFunction m_OnSlowTick_hotfix;
    private LuaFunction m_UpdateChatUnreadCount_hotfix;
    private LuaFunction m_LoadAndUpdateEventActorsAction_hotfix;
    private LuaFunction m_UpdateExchangeRedMark_hotfix;
    private LuaFunction m_StartCollectionScenarioHandleNetTaskInt32_hotfix;
    private LuaFunction m_StartScenarioLevelConfigDataCollectionActivityScenarioLevelInfo_hotfix;
    private LuaFunction m_CollectionActivityUIController_OnReturn_hotfix;
    private LuaFunction m_CollectionActivityUIController_OnShowHelp_hotfix;
    private LuaFunction m_HasUnlockLootLevel_hotfix;
    private LuaFunction m_CollectionActivityUIController_OnShowTeam_hotfix;
    private LuaFunction m_TeamUITask_OnLoadAllResCompleted_hotfix;
    private LuaFunction m_CollectionActivityUIController_OnShowExchange_hotfix;
    private LuaFunction m_CollectionActivityExchangeUITask_OnLoadAllResCompleted_hotfix;
    private LuaFunction m_CollectionActivityUIController_OnShowChat_hotfix;
    private LuaFunction m_CollectionActivityUIController_OnStartScenarioLevelConfigDataCollectionActivityScenarioLevelInfo_hotfix;
    private LuaFunction m_CollectionActivityUIController_OnStartChallengeLevelConfigDataCollectionActivityChallengeLevelInfo_hotfix;
    private LuaFunction m_CollectionActivityUIController_OnStartLootLevelConfigDataCollectionActivityLootLevelInfo_hotfix;
    private LuaFunction m_PlayerContext_OnCollectionEventAdd_hotfix;
    private LuaFunction m_PlayerContext_OnCollectionEventDelete_hotfix;
    private LuaFunction m_get_LayerDescArray_hotfix;
    private LuaFunction m_get_UICtrlDescArray_hotfix;
    private LuaFunction m_StartWaypointEventConfigDataCollectionEventInfo_hotfix;
    private LuaFunction m_OnHappeningStepEnd_hotfix;
    private LuaFunction m_StartWorldEventMissionUITaskConfigDataEventInfo_hotfix;
    private LuaFunction m_WorldEventMissionUITask_OnClose_hotfix;
    private LuaFunction m_RemoveWorldEventMissionUITaskEvents_hotfix;
    private LuaFunction m_WorldEventMissionUITask_OnCheckEnterMission_hotfix;
    private LuaFunction m_WorldEventMissionUITask_OnEnterMission_hotfix;
    private LuaFunction m_ShowWaypointRewardBattleRewardBoolean_hotfix;
    private LuaFunction m_GetRewardGoodsUITask_OnClose_hotfix;
    private LuaFunction m_ChestUITask_OnClose_hotfix;
    private LuaFunction m_OnWaypointClickConfigDataCollectionActivityWaypointInfo_hotfix;
    private LuaFunction m_PlayerArriveWaypointConfigDataCollectionActivityWaypointInfoConfigDataCollectionActivityWaypointInfo_hotfix;
    private LuaFunction m_ShowLevelListConfigDataCollectionActivityWaypointInfo_hotfix;
    private LuaFunction m_StartCollectionWayPointMoveNetTaskInt32_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivityUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnResume(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateCollectionActivityWorld()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DestroyCollectionActivityWorld()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartCollectionActivityWorld()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitDataFromUIIntent(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectPlayerAssets()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectEventAssets()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitLayerStateOnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterPlayerContextEvents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterPlayerContextEvents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitCollectionActivityUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UninitCollectionActivityUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void PostUpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CheckEnterFirstScenarioLevel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CheckChangeActiveScenarioLevel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateActiveScenarioAndWaypointState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateActiveEventState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnTick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSlowTick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateChatUnreadCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LoadAndUpdateEventActors(Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateExchangeRedMark()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartCollectionScenarioHandleNetTask(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool StartScenarioLevel(
      ConfigDataCollectionActivityScenarioLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectionActivityUIController_OnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectionActivityUIController_OnShowHelp()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool HasUnlockLootLevel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectionActivityUIController_OnShowTeam()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectionActivityUIController_OnShowExchange()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectionActivityExchangeUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectionActivityUIController_OnShowChat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectionActivityUIController_OnStartScenarioLevel(
      ConfigDataCollectionActivityScenarioLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectionActivityUIController_OnStartChallengeLevel(
      ConfigDataCollectionActivityChallengeLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectionActivityUIController_OnStartLootLevel(
      ConfigDataCollectionActivityLootLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnCollectionEventAdd()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnCollectionEventDelete()
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartWaypointEvent(ConfigDataCollectionEventInfo collectinEventInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHappeningStepEnd()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartWorldEventMissionUITask(ConfigDataEventInfo eventInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldEventMissionUITask_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RemoveWorldEventMissionUITaskEvents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool WorldEventMissionUITask_OnCheckEnterMission()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldEventMissionUITask_OnEnterMission()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowWaypointReward(BattleReward reward, bool isChest)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GetRewardGoodsUITask_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ChestUITask_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnWaypointClick(
      ConfigDataCollectionActivityWaypointInfo waypointInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerArriveWaypoint(
      ConfigDataCollectionActivityWaypointInfo waypointInfo,
      ConfigDataCollectionActivityWaypointInfo prevWaypointInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowLevelList(
      ConfigDataCollectionActivityWaypointInfo waypointInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartCollectionWayPointMoveNetTask(int waypointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public CollectionActivityUITask.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private bool __callBase_OnStart(UIIntent intent)
    {
      return base.OnStart(intent);
    }

    private void __callBase_OnStop()
    {
      base.OnStop();
    }

    private void __callBase_OnPause()
    {
      this.OnPause();
    }

    private bool __callBase_OnResume(UIIntent intent)
    {
      return base.OnResume(intent);
    }

    private bool __callBase_OnNewIntent(UIIntent intent)
    {
      return this.OnNewIntent(intent);
    }

    private void __callBase_EnableUIInput(bool isEnable, bool? isGlobalEnable)
    {
      this.EnableUIInput(isEnable, isGlobalEnable);
    }

    private bool __callBase_IsNeedLoadDynamicRes()
    {
      return base.IsNeedLoadDynamicRes();
    }

    private List<string> __callBase_CollectAllDynamicResForLoad()
    {
      return this.CollectAllDynamicResForLoad();
    }

    private void __callBase_RegisterPlayerContextEvents()
    {
      base.RegisterPlayerContextEvents();
    }

    private void __callBase_UnregisterPlayerContextEvents()
    {
      base.UnregisterPlayerContextEvents();
    }

    private void __callBase_PostUpdateView()
    {
      base.PostUpdateView();
    }

    private void __callBase_CollectPreloadResourceList()
    {
      this.CollectPreloadResourceList();
    }

    private void __callBase_ClearAssetList()
    {
      this.ClearAssetList();
    }

    private void __callBase_CollectAsset(string assetName)
    {
      this.CollectAsset(assetName);
    }

    private void __callBase_CollectSpriteAsset(string assetName)
    {
      this.CollectSpriteAsset(assetName);
    }

    private void __callBase_CollectFxAsset(string assetName)
    {
      this.CollectFxAsset(assetName);
    }

    private void __callBase_StartLoadCustomAssets(Action onLoadCompleted)
    {
      this.StartLoadCustomAssets(onLoadCompleted);
    }

    private bool __callBase_IsLoadingCustomAssets()
    {
      return this.IsLoadingCustomAssets();
    }

    private bool __callBase_IsPipeLineRunning()
    {
      return this.IsPipeLineRunning();
    }

    private bool __callBase_IsOpeningUI()
    {
      return this.IsOpeningUI();
    }

    private UITaskBase __callBase_ReturnPrevUITask()
    {
      return this.ReturnPrevUITask();
    }

    private void __callBase_ClearUnusedDynamicResourceCache()
    {
      this.ClearUnusedDynamicResourceCache();
    }

    private void __callBase_ClearDynamicResourceCache(List<string> assets)
    {
      this.ClearDynamicResourceCache(assets);
    }

    private void __callBase_OnMemoryWarning()
    {
      this.OnMemoryWarning();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private CollectionActivityUITask m_owner;

      public LuaExportHelper(CollectionActivityUITask owner)
      {
        this.m_owner = owner;
      }

      public bool __callBase_OnStart(UIIntent intent)
      {
        return this.m_owner.__callBase_OnStart(intent);
      }

      public void __callBase_OnStop()
      {
        this.m_owner.__callBase_OnStop();
      }

      public void __callBase_OnPause()
      {
        this.m_owner.__callBase_OnPause();
      }

      public bool __callBase_OnResume(UIIntent intent)
      {
        return this.m_owner.__callBase_OnResume(intent);
      }

      public bool __callBase_OnNewIntent(UIIntent intent)
      {
        return this.m_owner.__callBase_OnNewIntent(intent);
      }

      public void __callBase_EnableUIInput(bool isEnable, bool? isGlobalEnable)
      {
        this.m_owner.__callBase_EnableUIInput(isEnable, isGlobalEnable);
      }

      public bool __callBase_IsNeedLoadDynamicRes()
      {
        return this.m_owner.__callBase_IsNeedLoadDynamicRes();
      }

      public List<string> __callBase_CollectAllDynamicResForLoad()
      {
        return this.m_owner.__callBase_CollectAllDynamicResForLoad();
      }

      public void __callBase_RegisterPlayerContextEvents()
      {
        this.m_owner.__callBase_RegisterPlayerContextEvents();
      }

      public void __callBase_UnregisterPlayerContextEvents()
      {
        this.m_owner.__callBase_UnregisterPlayerContextEvents();
      }

      public void __callBase_PostUpdateView()
      {
        this.m_owner.__callBase_PostUpdateView();
      }

      public void __callBase_CollectPreloadResourceList()
      {
        this.m_owner.__callBase_CollectPreloadResourceList();
      }

      public void __callBase_ClearAssetList()
      {
        this.m_owner.__callBase_ClearAssetList();
      }

      public void __callBase_CollectAsset(string assetName)
      {
        this.m_owner.__callBase_CollectAsset(assetName);
      }

      public void __callBase_CollectSpriteAsset(string assetName)
      {
        this.m_owner.__callBase_CollectSpriteAsset(assetName);
      }

      public void __callBase_CollectFxAsset(string assetName)
      {
        this.m_owner.__callBase_CollectFxAsset(assetName);
      }

      public void __callBase_StartLoadCustomAssets(Action onLoadCompleted)
      {
        this.m_owner.__callBase_StartLoadCustomAssets(onLoadCompleted);
      }

      public bool __callBase_IsLoadingCustomAssets()
      {
        return this.m_owner.__callBase_IsLoadingCustomAssets();
      }

      public bool __callBase_IsPipeLineRunning()
      {
        return this.m_owner.__callBase_IsPipeLineRunning();
      }

      public bool __callBase_IsOpeningUI()
      {
        return this.m_owner.__callBase_IsOpeningUI();
      }

      public UITaskBase __callBase_ReturnPrevUITask()
      {
        return this.m_owner.__callBase_ReturnPrevUITask();
      }

      public void __callBase_ClearUnusedDynamicResourceCache()
      {
        this.m_owner.__callBase_ClearUnusedDynamicResourceCache();
      }

      public void __callBase_ClearDynamicResourceCache(List<string> assets)
      {
        this.m_owner.__callBase_ClearDynamicResourceCache(assets);
      }

      public void __callBase_OnMemoryWarning()
      {
        this.m_owner.__callBase_OnMemoryWarning();
      }

      public UITaskBase.LayerDesc[] m_layerDescArray
      {
        get
        {
          return this.m_owner.m_layerDescArray;
        }
        set
        {
          this.m_owner.m_layerDescArray = value;
        }
      }

      public UITaskBase.UIControllerDesc[] m_uiCtrlDescArray
      {
        get
        {
          return this.m_owner.m_uiCtrlDescArray;
        }
        set
        {
          this.m_owner.m_uiCtrlDescArray = value;
        }
      }

      public CollectionActivityUIController m_collectionActivityUIController
      {
        get
        {
          return this.m_owner.m_collectionActivityUIController;
        }
        set
        {
          this.m_owner.m_collectionActivityUIController = value;
        }
      }

      public PlayerResourceUIController m_playerResourceUIController
      {
        get
        {
          return this.m_owner.m_playerResourceUIController;
        }
        set
        {
          this.m_owner.m_playerResourceUIController = value;
        }
      }

      public CollectionActivityWorld m_collectionActivityWorld
      {
        get
        {
          return this.m_owner.m_collectionActivityWorld;
        }
        set
        {
          this.m_owner.m_collectionActivityWorld = value;
        }
      }

      public List<int> m_movePath
      {
        get
        {
          return this.m_owner.m_movePath;
        }
        set
        {
          this.m_owner.m_movePath = value;
        }
      }

      public int m_nowSeconds
      {
        get
        {
          return this.m_owner.m_nowSeconds;
        }
        set
        {
          this.m_owner.m_nowSeconds = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public ConfigDataCollectionActivityInfo m_collectionActivityInfo
      {
        get
        {
          return this.m_owner.m_collectionActivityInfo;
        }
        set
        {
          this.m_owner.m_collectionActivityInfo = value;
        }
      }

      public int m_needCollectionScenarioHandleLevelId
      {
        get
        {
          return this.m_owner.m_needCollectionScenarioHandleLevelId;
        }
        set
        {
          this.m_owner.m_needCollectionScenarioHandleLevelId = value;
        }
      }

      public bool m_isAfterBattle
      {
        get
        {
          return this.m_owner.m_isAfterBattle;
        }
        set
        {
          this.m_owner.m_isAfterBattle = value;
        }
      }

      public bool m_isEnterFirstScenarioFail
      {
        get
        {
          return this.m_owner.m_isEnterFirstScenarioFail;
        }
        set
        {
          this.m_owner.m_isEnterFirstScenarioFail = value;
        }
      }

      public bool m_needUpdateEventActors
      {
        get
        {
          return this.m_owner.m_needUpdateEventActors;
        }
        set
        {
          this.m_owner.m_needUpdateEventActors = value;
        }
      }

      public UITaskBase.LayerDesc[] LayerDescArray
      {
        get
        {
          return this.m_owner.LayerDescArray;
        }
      }

      public UITaskBase.UIControllerDesc[] UICtrlDescArray
      {
        get
        {
          return this.m_owner.UICtrlDescArray;
        }
      }

      public bool OnStart(UIIntent intent)
      {
        return this.m_owner.OnStart(intent);
      }

      public bool OnResume(UIIntent intent)
      {
        return this.m_owner.OnResume(intent);
      }

      public void OnStop()
      {
        this.m_owner.OnStop();
      }

      public void CreateCollectionActivityWorld()
      {
        this.m_owner.CreateCollectionActivityWorld();
      }

      public void DestroyCollectionActivityWorld()
      {
        this.m_owner.DestroyCollectionActivityWorld();
      }

      public void StartCollectionActivityWorld()
      {
        this.m_owner.StartCollectionActivityWorld();
      }

      public void InitDataFromUIIntent(UIIntent intent)
      {
        this.m_owner.InitDataFromUIIntent(intent);
      }

      public bool IsNeedLoadDynamicRes()
      {
        return this.m_owner.IsNeedLoadDynamicRes();
      }

      public void CollectPlayerAssets()
      {
        this.m_owner.CollectPlayerAssets();
      }

      public void CollectEventAssets()
      {
        this.m_owner.CollectEventAssets();
      }

      public void InitLayerStateOnLoadAllResCompleted()
      {
        this.m_owner.InitLayerStateOnLoadAllResCompleted();
      }

      public void InitAllUIControllers()
      {
        this.m_owner.InitAllUIControllers();
      }

      public void ClearAllContextAndRes()
      {
        this.m_owner.ClearAllContextAndRes();
      }

      public void RegisterPlayerContextEvents()
      {
        this.m_owner.RegisterPlayerContextEvents();
      }

      public void UnregisterPlayerContextEvents()
      {
        this.m_owner.UnregisterPlayerContextEvents();
      }

      public void InitCollectionActivityUIController()
      {
        this.m_owner.InitCollectionActivityUIController();
      }

      public void UninitCollectionActivityUIController()
      {
        this.m_owner.UninitCollectionActivityUIController();
      }

      public void UpdateView()
      {
        this.m_owner.UpdateView();
      }

      public void PostUpdateView()
      {
        this.m_owner.PostUpdateView();
      }

      public bool CheckEnterFirstScenarioLevel()
      {
        return this.m_owner.CheckEnterFirstScenarioLevel();
      }

      public bool CheckChangeActiveScenarioLevel()
      {
        return this.m_owner.CheckChangeActiveScenarioLevel();
      }

      public void UpdateActiveScenarioAndWaypointState()
      {
        this.m_owner.UpdateActiveScenarioAndWaypointState();
      }

      public void UpdateActiveEventState()
      {
        this.m_owner.UpdateActiveEventState();
      }

      public void OnTick()
      {
        this.m_owner.OnTick();
      }

      public void OnSlowTick()
      {
        this.m_owner.OnSlowTick();
      }

      public void UpdateChatUnreadCount()
      {
        this.m_owner.UpdateChatUnreadCount();
      }

      public void LoadAndUpdateEventActors(Action onEnd)
      {
        this.m_owner.LoadAndUpdateEventActors(onEnd);
      }

      public void UpdateExchangeRedMark()
      {
        this.m_owner.UpdateExchangeRedMark();
      }

      public void StartCollectionScenarioHandleNetTask(int levelId)
      {
        this.m_owner.StartCollectionScenarioHandleNetTask(levelId);
      }

      public bool StartScenarioLevel(
        ConfigDataCollectionActivityScenarioLevelInfo levelInfo)
      {
        return this.m_owner.StartScenarioLevel(levelInfo);
      }

      public void CollectionActivityUIController_OnReturn()
      {
        this.m_owner.CollectionActivityUIController_OnReturn();
      }

      public void CollectionActivityUIController_OnShowHelp()
      {
        this.m_owner.CollectionActivityUIController_OnShowHelp();
      }

      public bool HasUnlockLootLevel()
      {
        return this.m_owner.HasUnlockLootLevel();
      }

      public void CollectionActivityUIController_OnShowTeam()
      {
        this.m_owner.CollectionActivityUIController_OnShowTeam();
      }

      public void TeamUITask_OnLoadAllResCompleted()
      {
        this.m_owner.TeamUITask_OnLoadAllResCompleted();
      }

      public void CollectionActivityUIController_OnShowExchange()
      {
        this.m_owner.CollectionActivityUIController_OnShowExchange();
      }

      public void CollectionActivityExchangeUITask_OnLoadAllResCompleted()
      {
        this.m_owner.CollectionActivityExchangeUITask_OnLoadAllResCompleted();
      }

      public void CollectionActivityUIController_OnShowChat()
      {
        this.m_owner.CollectionActivityUIController_OnShowChat();
      }

      public void CollectionActivityUIController_OnStartScenarioLevel(
        ConfigDataCollectionActivityScenarioLevelInfo levelInfo)
      {
        this.m_owner.CollectionActivityUIController_OnStartScenarioLevel(levelInfo);
      }

      public void CollectionActivityUIController_OnStartChallengeLevel(
        ConfigDataCollectionActivityChallengeLevelInfo levelInfo)
      {
        this.m_owner.CollectionActivityUIController_OnStartChallengeLevel(levelInfo);
      }

      public void CollectionActivityUIController_OnStartLootLevel(
        ConfigDataCollectionActivityLootLevelInfo levelInfo)
      {
        this.m_owner.CollectionActivityUIController_OnStartLootLevel(levelInfo);
      }

      public void PlayerContext_OnCollectionEventAdd()
      {
        this.m_owner.PlayerContext_OnCollectionEventAdd();
      }

      public void PlayerContext_OnCollectionEventDelete()
      {
        this.m_owner.PlayerContext_OnCollectionEventDelete();
      }

      public void StartWaypointEvent(ConfigDataCollectionEventInfo collectinEventInfo)
      {
        this.m_owner.StartWaypointEvent(collectinEventInfo);
      }

      public void OnHappeningStepEnd()
      {
        this.m_owner.OnHappeningStepEnd();
      }

      public void StartWorldEventMissionUITask(ConfigDataEventInfo eventInfo)
      {
        this.m_owner.StartWorldEventMissionUITask(eventInfo);
      }

      public void WorldEventMissionUITask_OnClose()
      {
        this.m_owner.WorldEventMissionUITask_OnClose();
      }

      public void RemoveWorldEventMissionUITaskEvents()
      {
        this.m_owner.RemoveWorldEventMissionUITaskEvents();
      }

      public bool WorldEventMissionUITask_OnCheckEnterMission()
      {
        return this.m_owner.WorldEventMissionUITask_OnCheckEnterMission();
      }

      public void WorldEventMissionUITask_OnEnterMission()
      {
        this.m_owner.WorldEventMissionUITask_OnEnterMission();
      }

      public void ShowWaypointReward(BattleReward reward, bool isChest)
      {
        this.m_owner.ShowWaypointReward(reward, isChest);
      }

      public void GetRewardGoodsUITask_OnClose()
      {
        this.m_owner.GetRewardGoodsUITask_OnClose();
      }

      public void ChestUITask_OnClose()
      {
        this.m_owner.ChestUITask_OnClose();
      }

      public void PlayerArriveWaypoint(
        ConfigDataCollectionActivityWaypointInfo waypointInfo,
        ConfigDataCollectionActivityWaypointInfo prevWaypointInfo)
      {
        this.m_owner.PlayerArriveWaypoint(waypointInfo, prevWaypointInfo);
      }

      public void ShowLevelList(
        ConfigDataCollectionActivityWaypointInfo waypointInfo)
      {
        this.m_owner.ShowLevelList(waypointInfo);
      }

      public void StartCollectionWayPointMoveNetTask(int waypointId)
      {
        this.m_owner.StartCollectionWayPointMoveNetTask(waypointId);
      }
    }
  }
}
