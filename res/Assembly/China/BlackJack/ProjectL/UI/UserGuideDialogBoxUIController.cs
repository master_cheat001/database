﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.UserGuideDialogBoxUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Art;
using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class UserGuideDialogBoxUIController : UIControllerBase
  {
    private const int WordsSpeed = 30;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./Text", AutoBindAttribute.InitState.NotInit, false)]
    private DialogText m_text;
    [AutoBind("./PenGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_waitGameObject;
    [AutoBind("./NameGroup/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameText;
    private float m_time;
    private float m_voicePlayTime;
    private int m_wordsDisplayLength;
    private int m_wordsDisplayLengthMax;
    private string m_openStateName;
    private string m_closeStateName;
    private bool m_isOpened;

    [MethodImpl((MethodImplOptions) 32768)]
    public void Show(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open(Action onFinished = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Close(Action onFinished = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsOpened()
    {
      return this.m_isOpened;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetStyle(int positionType, int frameType)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetName(string name)
    {
      this.m_nameText.text = name;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetWords(string txt)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetVoicePlayTime(float voicePlayTime)
    {
      this.m_voicePlayTime = voicePlayTime;
    }

    public float GetWordsDisplayTime()
    {
      return (float) this.m_wordsDisplayLengthMax / 30f;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
