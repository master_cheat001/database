﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroDetailUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.Scene;
using SLua;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class HeroDetailUIController : UIControllerBase
  {
    [AutoBind("./Margin", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./Margin/LeftButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_leftButton;
    [AutoBind("./Margin/RightButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_rightButton;
    [AutoBind("./Margin/FilterToggles/Info", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_infoToggle;
    [AutoBind("./Margin/FilterToggles/Job", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_jobToggle;
    [AutoBind("./Margin/FilterToggles/Job/Click/RedMark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobToggleClickRedMark;
    [AutoBind("./Margin/FilterToggles/Job/UnClick/RedMark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobToggleUnClickRedMark;
    [AutoBind("./Margin/FilterToggles/Job/JobUpButton", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobUpTip;
    [AutoBind("./Margin/FilterToggles/Soldier", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_soldierToggle;
    [AutoBind("./Margin/FilterToggles/Equip", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_equipToggle;
    [AutoBind("./Margin/FilterToggles/Equip/Click/RedMark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipToggleClickRedMark;
    [AutoBind("./Margin/FilterToggles/Equip/UnClick/RedMark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipToggleUnClickRedMark;
    [AutoBind("./Life", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_lifeToggle;
    [AutoBind("./Life/NewImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_lifeToggleClickRedMark;
    [AutoBind("./Margin/FilterToggles/Equip/EquipMask", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_equipMaskButton;
    [AutoBind("./Margin/JobTransferButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_jobTransferButton;
    [AutoBind("./Margin/JobTransferButton/RedIcon", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobTransferButtonRedIcon;
    [AutoBind("./Margin/JobTransferButton/U_SummonButton_ Press", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobTransferButtonPressEffect;
    [AutoBind("./Margin/JobTransferButton/U_Button_ready", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobTransferButtonReadyEffect;
    private Hero m_hero;
    public int m_curHeroNum;
    public List<Hero> m_curHeroList;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroDetailUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PassHeroInfo(List<Hero> hList, int num)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateViewInHeroDetail()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnLeftButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRightButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnJobTransferButtonClick()
    {
      this.StartCoroutine(this.OnJobTransferButtonClickEffect());
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator OnJobTransferButtonClickEffect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMaskButtonForUserGuideClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool HeroInfoToogleIsOn()
    {
      return this.m_infoToggle.isOn;
    }

    public bool HeroJobToggleIsOn()
    {
      return this.m_jobToggle.isOn;
    }

    public bool HeroEquipToggleIsOn()
    {
      return this.m_equipToggle.isOn;
    }

    public void SetToggleToInfo()
    {
      this.m_infoToggle.isOn = true;
    }

    public void SetToggleToJob()
    {
      this.m_jobToggle.isOn = true;
    }

    public void SetToggleToSoldier()
    {
      this.m_soldierToggle.isOn = true;
    }

    public void SetToggleToEquip()
    {
      this.m_equipToggle.isOn = true;
    }

    public void SetToggleToLife()
    {
      this.m_lifeToggle.isOn = true;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void CreateSpineGraphic(
      ref UISpineGraphic graphic,
      string assetName,
      GameObject parent,
      int direction,
      Vector2 offset,
      float scale,
      List<ReplaceAnim> anims)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void DestroySpineGraphic(ref UISpineGraphic g)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInfoToggleValueChanged(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnJobToggleValueChanged(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSoldierToggleValueChanged(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEquipToggleValueChanged(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnLifeToggleValueChanged(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnJobTransfer
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string> EventOnSetDetailState
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event HeroDetailUIController.Action<int, bool, bool, int, bool> EventOnUpdateViewInListAndDetail
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public delegate void Action<T1, T2, T3, T4, T5>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5);
  }
}
