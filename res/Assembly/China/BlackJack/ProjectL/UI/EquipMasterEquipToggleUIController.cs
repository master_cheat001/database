﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.EquipMasterEquipToggleUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class EquipMasterEquipToggleUIController : UIControllerBase
  {
    private readonly List<CommonUIStateController> m_iconUIStateCtrl;
    private readonly List<Image> m_iconNormalImage;
    private readonly List<Image> m_iconBrightImage;
    [AutoBind("./Group/IconItem", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController m_property1IconUIStateCtrl;
    [AutoBind("./Group/IconItem (1)", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController m_property2IconUIStateCtrl;
    [AutoBind("./Group/IconItem (2)", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController m_property3IconUIStateCtrl;
    [AutoBind("./Group/IconItem (3)", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController m_property4IconUIStateCtrl;
    [AutoBind("./Group/IconItem (4)", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController m_property5IconUIStateCtrl;
    [AutoBind("./Group/IconItem/Bright", AutoBindAttribute.InitState.NotInit, false)]
    public Image m_property1IconImage;
    [AutoBind("./Group/IconItem/Normal", AutoBindAttribute.InitState.NotInit, false)]
    public Image m_property1IconGrayImage;
    [AutoBind("./Group/IconItem (1)/Bright", AutoBindAttribute.InitState.NotInit, false)]
    public Image m_property2IconImage;
    [AutoBind("./Group/IconItem (1)/Normal", AutoBindAttribute.InitState.NotInit, false)]
    public Image m_property2IconGrayImage;
    [AutoBind("./Group/IconItem (2)/Bright", AutoBindAttribute.InitState.NotInit, false)]
    public Image m_property3IconImage;
    [AutoBind("./Group/IconItem (2)/Normal", AutoBindAttribute.InitState.NotInit, false)]
    public Image m_property3IconGrayImage;
    [AutoBind("./Group/IconItem (3)/Bright", AutoBindAttribute.InitState.NotInit, false)]
    public Image m_property4IconImage;
    [AutoBind("./Group/IconItem (3)/Normal", AutoBindAttribute.InitState.NotInit, false)]
    public Image m_property4IconGrayImage;
    [AutoBind("./Group/IconItem (4)/Bright", AutoBindAttribute.InitState.NotInit, false)]
    public Image m_property5IconImage;
    [AutoBind("./Group/IconItem (4)/Normal", AutoBindAttribute.InitState.NotInit, false)]
    public Image m_property5IconGrayImage;

    [MethodImpl((MethodImplOptions) 32768)]
    public EquipMasterEquipToggleUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateToggle(Hero hero, EquipMasterEquipType equipType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateIconImage(
      Image normalImage,
      Image brightImage,
      PropertyModifyType propertyType)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
