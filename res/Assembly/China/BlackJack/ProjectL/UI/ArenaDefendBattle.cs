﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ArenaDefendBattle
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Scene;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class ArenaDefendBattle
  {
    private GameObject m_graphicRoot;
    private GameObject m_mapRoot;
    private GameObject m_mapBackground;
    private GameObject m_mapTerrainFxRoot;
    private Dictionary<GridPosition, GameObject> m_mapTerrainFxs;
    private ConfigDataArenaBattleInfo m_arenaBattleInfo;
    private BattleCamera m_camera;
    private GraphicPool m_graphicPool;
    private GraphicPool m_fxPool;
    private FxPlayer m_fxPlayer;

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaDefendBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Dispose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Initialize(GameObject root)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CreateMap(ConfigDataArenaBattleInfo battleInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DestroyMap()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Update(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateMapBackground(ConfigDataBattlefieldInfo battlefieldInfo, GameObject parent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearMapBackground()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateMapTerrainFx(ConfigDataBattlefieldInfo battlefieldInfo, GameObject parent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearMapTerrainFx()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GenericGraphic CreateGraphic(string assetName, float scale)
    {
      // ISSUE: unable to decompile the method.
    }

    public void DestroyGraphic(GenericGraphic graphic)
    {
      this.m_graphicPool.Destroy(graphic);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Vector2 GridPositionToWorldPosition(GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GridPosition WorldPositionToGridPosition(Vector2 sp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Vector2 ScreenPositionToWorldPosition(Vector2 p)
    {
      // ISSUE: unable to decompile the method.
    }

    public GridPosition ScreenPositionToGridPosition(Vector2 p)
    {
      return this.WorldPositionToGridPosition(this.ScreenPositionToWorldPosition(p));
    }

    public ConfigDataArenaBattleInfo ArenaBattleInfo
    {
      get
      {
        return this.m_arenaBattleInfo;
      }
    }

    public BattleCamera Camera
    {
      get
      {
        return this.m_camera;
      }
    }

    public FxPlayer FxPlayer
    {
      get
      {
        return this.m_fxPlayer;
      }
    }
  }
}
