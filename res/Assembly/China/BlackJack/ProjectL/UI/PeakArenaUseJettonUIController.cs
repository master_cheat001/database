﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PeakArenaUseJettonUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using System;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class PeakArenaUseJettonUIController : UIControllerBase
  {
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiState;
    [AutoBind("./BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_bgButton;
    [AutoBind("./Detail/IdolsButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_jettonButton;
    [AutoBind("./Detail/AddButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_addJettonButton;
    [AutoBind("./Detail/MinusButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_minusJettonButton;
    [AutoBind("./Detail/MaxButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_maxJettonButton;
    [AutoBind("./Detail/MaxIdolsTimes/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_usedJettonText;
    [AutoBind("./Detail/IdolTicketsValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_currentUseJettonText;
    [AutoBind("./Detail/BuyTimesTitleText/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_ownJettonCountText;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private int m_prepareUseJettonCount;
    private PeakArenaPlayOffMatchupInfo m_matchupInfo;
    private string m_userId;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open(PeakArenaPlayOffMatchupInfo matchupInfo, string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int CalculateMaxUseCount(int prepareUseJettonCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnUseClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddJettonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMinusJettonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMaxJettonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnUseJettonSuccess
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
