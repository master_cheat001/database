﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroDungeonLevelListItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class HeroDungeonLevelListItemUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_button;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./Detail/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameText;
    [AutoBind("./Detail/StarImage1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_star1GameObject;
    [AutoBind("./Detail/StarImage2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_star2GameObject;
    [AutoBind("./Detail/StarImage3", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_star3GameObject;
    [AutoBind("./Detail/Trophy", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_trophyGameObject;
    [AutoBind("./Detail/Trophy/TrophyValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_trophyValueText;
    [AutoBind("./Lock/FavorLevelNotEnoughText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_favorLevelNotEnoughText;
    [AutoBind("./Lock/PreLevelNotCompleteText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_preLevelNotCompleteText;
    [AutoBind("./Detail/PieceImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_pieceImage;
    [AutoBind("./Detail/CountDetail/CountValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_lastCountValueText;
    private ConfigDataHeroDungeonLevelInfo m_levelInfo;
    private int m_index;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    [DoNotToLua]
    private HeroDungeonLevelListItemUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_SetDungeonInfoConfigDataHeroDungeonLevelInfo_hotfix;
    private LuaFunction m_GetDungeonInfo_hotfix;
    private LuaFunction m_SetIndexInt32_hotfix;
    private LuaFunction m_GetIndex_hotfix;
    private LuaFunction m_IsFavorLevelLock_hotfix;
    private LuaFunction m_IsPreLevelLock_hotfix;
    private LuaFunction m_OnClick_hotfix;
    private LuaFunction m_add_EventOnClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnClickAction`1_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroDungeonLevelListItemUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDungeonInfo(ConfigDataHeroDungeonLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroDungeonLevelInfo GetDungeonInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetIndex(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetIndex()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsFavorLevelLock()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPreLevelLock()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<HeroDungeonLevelListItemUIController> EventOnClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public HeroDungeonLevelListItemUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnClick(HeroDungeonLevelListItemUIController obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnClick(HeroDungeonLevelListItemUIController obj)
    {
      this.EventOnClick = (Action<HeroDungeonLevelListItemUIController>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private HeroDungeonLevelListItemUIController m_owner;

      public LuaExportHelper(HeroDungeonLevelListItemUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnClick(HeroDungeonLevelListItemUIController obj)
      {
        this.m_owner.__callDele_EventOnClick(obj);
      }

      public void __clearDele_EventOnClick(HeroDungeonLevelListItemUIController obj)
      {
        this.m_owner.__clearDele_EventOnClick(obj);
      }

      public Button m_button
      {
        get
        {
          return this.m_owner.m_button;
        }
        set
        {
          this.m_owner.m_button = value;
        }
      }

      public CommonUIStateController m_uiStateController
      {
        get
        {
          return this.m_owner.m_uiStateController;
        }
        set
        {
          this.m_owner.m_uiStateController = value;
        }
      }

      public Text m_nameText
      {
        get
        {
          return this.m_owner.m_nameText;
        }
        set
        {
          this.m_owner.m_nameText = value;
        }
      }

      public GameObject m_star1GameObject
      {
        get
        {
          return this.m_owner.m_star1GameObject;
        }
        set
        {
          this.m_owner.m_star1GameObject = value;
        }
      }

      public GameObject m_star2GameObject
      {
        get
        {
          return this.m_owner.m_star2GameObject;
        }
        set
        {
          this.m_owner.m_star2GameObject = value;
        }
      }

      public GameObject m_star3GameObject
      {
        get
        {
          return this.m_owner.m_star3GameObject;
        }
        set
        {
          this.m_owner.m_star3GameObject = value;
        }
      }

      public GameObject m_trophyGameObject
      {
        get
        {
          return this.m_owner.m_trophyGameObject;
        }
        set
        {
          this.m_owner.m_trophyGameObject = value;
        }
      }

      public Text m_trophyValueText
      {
        get
        {
          return this.m_owner.m_trophyValueText;
        }
        set
        {
          this.m_owner.m_trophyValueText = value;
        }
      }

      public Text m_favorLevelNotEnoughText
      {
        get
        {
          return this.m_owner.m_favorLevelNotEnoughText;
        }
        set
        {
          this.m_owner.m_favorLevelNotEnoughText = value;
        }
      }

      public Text m_preLevelNotCompleteText
      {
        get
        {
          return this.m_owner.m_preLevelNotCompleteText;
        }
        set
        {
          this.m_owner.m_preLevelNotCompleteText = value;
        }
      }

      public Image m_pieceImage
      {
        get
        {
          return this.m_owner.m_pieceImage;
        }
        set
        {
          this.m_owner.m_pieceImage = value;
        }
      }

      public Text m_lastCountValueText
      {
        get
        {
          return this.m_owner.m_lastCountValueText;
        }
        set
        {
          this.m_owner.m_lastCountValueText = value;
        }
      }

      public ConfigDataHeroDungeonLevelInfo m_levelInfo
      {
        get
        {
          return this.m_owner.m_levelInfo;
        }
        set
        {
          this.m_owner.m_levelInfo = value;
        }
      }

      public int m_index
      {
        get
        {
          return this.m_owner.m_index;
        }
        set
        {
          this.m_owner.m_index = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void OnClick()
      {
        this.m_owner.OnClick();
      }
    }
  }
}
