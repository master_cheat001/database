﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.CreateGuildUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class CreateGuildUIController : UIControllerBase
  {
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_createGuildPanelAnimation;
    [AutoBind("./Detail/SociatyNameInputField", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_guildNameInputField;
    [AutoBind("./Detail/SociatyDeclarationInputField", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_guildDeclarationInputField;
    [AutoBind("./Detail/LevelInputField", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_levelInputField;
    [AutoBind("./Detail/LevelInputField/PreButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_forwardLevelButton;
    [AutoBind("./Detail/LevelInputField/AftButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_backwardLevelButton;
    [AutoBind("./Detail/ValueGroup/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_requireItemIamge;
    [AutoBind("./Detail/ApproveGroup/AutoButton", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_approveAutoToggle;
    [AutoBind("./Detail/ApproveGroup/ChairmanButton", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_approveChairmanToggle;
    [AutoBind("./Detail/GreatButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_createButton;
    [AutoBind("./Detail/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_colseButton;
    [AutoBind("./BackBGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_bgButton;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private int m_requireLevel;
    private GuildUITask m_guildUITask;
    [DoNotToLua]
    private CreateGuildUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_CreateGuild_hotfix;
    private LuaFunction m_RefreshLevelDisplay_hotfix;
    private LuaFunction m_OnCreateGuildClick_hotfix;
    private LuaFunction m_Show_hotfix;
    private LuaFunction m_OnCloseClick_hotfix;
    private LuaFunction m_OnForwardLevelClick_hotfix;
    private LuaFunction m_OnBackwardLevelClick_hotfix;
    private LuaFunction m_OnBGClick_hotfix;
    private LuaFunction m_OnLevelInputFiledEndEditString_hotfix;
    private LuaFunction m_OnGuildDeclaratioInputFiledEndEditString_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateGuild()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RefreshLevelDisplay()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCreateGuildClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Show()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnForwardLevelClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBackwardLevelClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBGClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnLevelInputFiledEndEdit(string content)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGuildDeclaratioInputFiledEndEdit(string content)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public CreateGuildUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private CreateGuildUIController m_owner;

      public LuaExportHelper(CreateGuildUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public CommonUIStateController m_createGuildPanelAnimation
      {
        get
        {
          return this.m_owner.m_createGuildPanelAnimation;
        }
        set
        {
          this.m_owner.m_createGuildPanelAnimation = value;
        }
      }

      public InputField m_guildNameInputField
      {
        get
        {
          return this.m_owner.m_guildNameInputField;
        }
        set
        {
          this.m_owner.m_guildNameInputField = value;
        }
      }

      public InputField m_guildDeclarationInputField
      {
        get
        {
          return this.m_owner.m_guildDeclarationInputField;
        }
        set
        {
          this.m_owner.m_guildDeclarationInputField = value;
        }
      }

      public InputField m_levelInputField
      {
        get
        {
          return this.m_owner.m_levelInputField;
        }
        set
        {
          this.m_owner.m_levelInputField = value;
        }
      }

      public Button m_forwardLevelButton
      {
        get
        {
          return this.m_owner.m_forwardLevelButton;
        }
        set
        {
          this.m_owner.m_forwardLevelButton = value;
        }
      }

      public Button m_backwardLevelButton
      {
        get
        {
          return this.m_owner.m_backwardLevelButton;
        }
        set
        {
          this.m_owner.m_backwardLevelButton = value;
        }
      }

      public Image m_requireItemIamge
      {
        get
        {
          return this.m_owner.m_requireItemIamge;
        }
        set
        {
          this.m_owner.m_requireItemIamge = value;
        }
      }

      public Toggle m_approveAutoToggle
      {
        get
        {
          return this.m_owner.m_approveAutoToggle;
        }
        set
        {
          this.m_owner.m_approveAutoToggle = value;
        }
      }

      public Toggle m_approveChairmanToggle
      {
        get
        {
          return this.m_owner.m_approveChairmanToggle;
        }
        set
        {
          this.m_owner.m_approveChairmanToggle = value;
        }
      }

      public Button m_createButton
      {
        get
        {
          return this.m_owner.m_createButton;
        }
        set
        {
          this.m_owner.m_createButton = value;
        }
      }

      public Button m_colseButton
      {
        get
        {
          return this.m_owner.m_colseButton;
        }
        set
        {
          this.m_owner.m_colseButton = value;
        }
      }

      public Button m_bgButton
      {
        get
        {
          return this.m_owner.m_bgButton;
        }
        set
        {
          this.m_owner.m_bgButton = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public int m_requireLevel
      {
        get
        {
          return this.m_owner.m_requireLevel;
        }
        set
        {
          this.m_owner.m_requireLevel = value;
        }
      }

      public GuildUITask m_guildUITask
      {
        get
        {
          return this.m_owner.m_guildUITask;
        }
        set
        {
          this.m_owner.m_guildUITask = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void CreateGuild()
      {
        this.m_owner.CreateGuild();
      }

      public void RefreshLevelDisplay()
      {
        this.m_owner.RefreshLevelDisplay();
      }

      public void OnCreateGuildClick()
      {
        this.m_owner.OnCreateGuildClick();
      }

      public void OnCloseClick()
      {
        this.m_owner.OnCloseClick();
      }

      public void OnForwardLevelClick()
      {
        this.m_owner.OnForwardLevelClick();
      }

      public void OnBackwardLevelClick()
      {
        this.m_owner.OnBackwardLevelClick();
      }

      public void OnBGClick()
      {
        this.m_owner.OnBGClick();
      }

      public void OnLevelInputFiledEndEdit(string content)
      {
        this.m_owner.OnLevelInputFiledEndEdit(content);
      }

      public void OnGuildDeclaratioInputFiledEndEdit(string content)
      {
        this.m_owner.OnGuildDeclaratioInputFiledEndEdit(content);
      }
    }
  }
}
