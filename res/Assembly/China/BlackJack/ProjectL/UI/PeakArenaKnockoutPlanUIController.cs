﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PeakArenaKnockoutPlanUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class PeakArenaKnockoutPlanUIController : UIControllerBase
  {
    [AutoBind("./PlayerGroup", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_playerGroup;
    [AutoBind("./ButtonGroup/ReplayButtonGroup", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_replayButtonGroup;
    [AutoBind("./ButtonGroup/WatchButtonGroup", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_watchButtonGroup;
    [AutoBind("./16Players", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_16PlayerLineGroup;
    [AutoBind("./8Players", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_8PlayerLineGroup;
    [AutoBind("./4Players", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_4PlayerLineGroup;
    [AutoBind("./2Players", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_2PlayerLineGroup;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private PeakArenaUITask m_peakArenaUITask;
    private List<CommonUIStateController> m_16LineState;
    private List<CommonUIStateController> m_8LineState;
    private List<CommonUIStateController> m_4LineState;
    private List<CommonUIStateController> m_2LineState;
    private List<PeakArenaKnockoutPlayerInfoUIController> m_playerInfoList;
    private List<Button> replayButtonList;
    private List<Button> watchButtonList;
    private List<PeakArenaPlayOffMatchupInfo> m_roundOneMatchInfoList;
    private List<PeakArenaPlayOffMatchupInfo> m_roundTwoMatchInfoList;
    private List<PeakArenaPlayOffMatchupInfo> m_roundThreeMatchInfoList;
    private List<PeakArenaPlayOffMatchupInfo> m_roundFourMatchInfoList;
    private List<PeakArenaPlayOffMatchupInfo> m_matchInfoList;
    private int m_groupIndex;
    [DoNotToLua]
    private PeakArenaKnockoutPlanUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_Bind_hotfix;
    private LuaFunction m_UpdateData_hotfix;
    private LuaFunction m_UpdateView_hotfix;
    private LuaFunction m_DebugLog_hotfix;
    private LuaFunction m_UpdateViewMatchupTreeList`1_hotfix;
    private LuaFunction m_UpdateViewLinePeakArenaPlayOffMatchupInfoInt32_hotfix;
    private LuaFunction m_SetMatchResultLineStateCommonUIStateControllerPeakArenaPlayOffMatchupInfoBoolean_hotfix;
    private LuaFunction m_UpdateViewPreviousLineStateInt32StringString_hotfix;
    private LuaFunction m_CalculateLineIndexInt32Int32_hotfix;
    private LuaFunction m_GetLineListInt32_hotfix;
    private LuaFunction m_UpdateViewButtonPeakArenaPlayOffMatchupInfoInt32_hotfix;
    private LuaFunction m_SetGroupIdInt32_hotfix;
    private LuaFunction m_Refresh_hotfix;
    private LuaFunction m_OnReplayClickInt32_hotfix;
    private LuaFunction m_OnWatchClickInt32_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaKnockoutPlanUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Bind()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DebugLog()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateViewMatchupTree(List<PeakArenaPlayOffMatchupInfo> matchupInfoList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateViewLine(PeakArenaPlayOffMatchupInfo matchupInfo, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetMatchResultLineState(
      CommonUIStateController lineState,
      PeakArenaPlayOffMatchupInfo matchupInfo,
      bool isLeftPlayer)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateViewPreviousLineState(int prevMatchId, string userId, string stateName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int CalculateLineIndex(int matchRound, int matchIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<CommonUIStateController> GetLineList(int matchRound)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateViewButton(PeakArenaPlayOffMatchupInfo matchupInfo, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGroupId(int groupIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Refresh()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReplayClick(int replayIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnWatchClick(int watchIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public PeakArenaKnockoutPlanUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private PeakArenaKnockoutPlanUIController m_owner;

      public LuaExportHelper(PeakArenaKnockoutPlanUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public Transform m_playerGroup
      {
        get
        {
          return this.m_owner.m_playerGroup;
        }
        set
        {
          this.m_owner.m_playerGroup = value;
        }
      }

      public Transform m_replayButtonGroup
      {
        get
        {
          return this.m_owner.m_replayButtonGroup;
        }
        set
        {
          this.m_owner.m_replayButtonGroup = value;
        }
      }

      public Transform m_watchButtonGroup
      {
        get
        {
          return this.m_owner.m_watchButtonGroup;
        }
        set
        {
          this.m_owner.m_watchButtonGroup = value;
        }
      }

      public Transform m_16PlayerLineGroup
      {
        get
        {
          return this.m_owner.m_16PlayerLineGroup;
        }
        set
        {
          this.m_owner.m_16PlayerLineGroup = value;
        }
      }

      public Transform m_8PlayerLineGroup
      {
        get
        {
          return this.m_owner.m_8PlayerLineGroup;
        }
        set
        {
          this.m_owner.m_8PlayerLineGroup = value;
        }
      }

      public Transform m_4PlayerLineGroup
      {
        get
        {
          return this.m_owner.m_4PlayerLineGroup;
        }
        set
        {
          this.m_owner.m_4PlayerLineGroup = value;
        }
      }

      public Transform m_2PlayerLineGroup
      {
        get
        {
          return this.m_owner.m_2PlayerLineGroup;
        }
        set
        {
          this.m_owner.m_2PlayerLineGroup = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public PeakArenaUITask m_peakArenaUITask
      {
        get
        {
          return this.m_owner.m_peakArenaUITask;
        }
        set
        {
          this.m_owner.m_peakArenaUITask = value;
        }
      }

      public List<CommonUIStateController> m_16LineState
      {
        get
        {
          return this.m_owner.m_16LineState;
        }
        set
        {
          this.m_owner.m_16LineState = value;
        }
      }

      public List<CommonUIStateController> m_8LineState
      {
        get
        {
          return this.m_owner.m_8LineState;
        }
        set
        {
          this.m_owner.m_8LineState = value;
        }
      }

      public List<CommonUIStateController> m_4LineState
      {
        get
        {
          return this.m_owner.m_4LineState;
        }
        set
        {
          this.m_owner.m_4LineState = value;
        }
      }

      public List<CommonUIStateController> m_2LineState
      {
        get
        {
          return this.m_owner.m_2LineState;
        }
        set
        {
          this.m_owner.m_2LineState = value;
        }
      }

      public List<PeakArenaKnockoutPlayerInfoUIController> m_playerInfoList
      {
        get
        {
          return this.m_owner.m_playerInfoList;
        }
        set
        {
          this.m_owner.m_playerInfoList = value;
        }
      }

      public List<Button> replayButtonList
      {
        get
        {
          return this.m_owner.replayButtonList;
        }
        set
        {
          this.m_owner.replayButtonList = value;
        }
      }

      public List<Button> watchButtonList
      {
        get
        {
          return this.m_owner.watchButtonList;
        }
        set
        {
          this.m_owner.watchButtonList = value;
        }
      }

      public List<PeakArenaPlayOffMatchupInfo> m_roundOneMatchInfoList
      {
        get
        {
          return this.m_owner.m_roundOneMatchInfoList;
        }
        set
        {
          this.m_owner.m_roundOneMatchInfoList = value;
        }
      }

      public List<PeakArenaPlayOffMatchupInfo> m_roundTwoMatchInfoList
      {
        get
        {
          return this.m_owner.m_roundTwoMatchInfoList;
        }
        set
        {
          this.m_owner.m_roundTwoMatchInfoList = value;
        }
      }

      public List<PeakArenaPlayOffMatchupInfo> m_roundThreeMatchInfoList
      {
        get
        {
          return this.m_owner.m_roundThreeMatchInfoList;
        }
        set
        {
          this.m_owner.m_roundThreeMatchInfoList = value;
        }
      }

      public List<PeakArenaPlayOffMatchupInfo> m_roundFourMatchInfoList
      {
        get
        {
          return this.m_owner.m_roundFourMatchInfoList;
        }
        set
        {
          this.m_owner.m_roundFourMatchInfoList = value;
        }
      }

      public List<PeakArenaPlayOffMatchupInfo> m_matchInfoList
      {
        get
        {
          return this.m_owner.m_matchInfoList;
        }
        set
        {
          this.m_owner.m_matchInfoList = value;
        }
      }

      public int m_groupIndex
      {
        get
        {
          return this.m_owner.m_groupIndex;
        }
        set
        {
          this.m_owner.m_groupIndex = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void Bind()
      {
        this.m_owner.Bind();
      }

      public void UpdateData()
      {
        this.m_owner.UpdateData();
      }

      public void UpdateView()
      {
        this.m_owner.UpdateView();
      }

      public void DebugLog()
      {
        this.m_owner.DebugLog();
      }

      public void UpdateViewMatchupTree(List<PeakArenaPlayOffMatchupInfo> matchupInfoList)
      {
        this.m_owner.UpdateViewMatchupTree(matchupInfoList);
      }

      public void UpdateViewLine(PeakArenaPlayOffMatchupInfo matchupInfo, int index)
      {
        this.m_owner.UpdateViewLine(matchupInfo, index);
      }

      public void SetMatchResultLineState(
        CommonUIStateController lineState,
        PeakArenaPlayOffMatchupInfo matchupInfo,
        bool isLeftPlayer)
      {
        this.m_owner.SetMatchResultLineState(lineState, matchupInfo, isLeftPlayer);
      }

      public void UpdateViewPreviousLineState(int prevMatchId, string userId, string stateName)
      {
        this.m_owner.UpdateViewPreviousLineState(prevMatchId, userId, stateName);
      }

      public int CalculateLineIndex(int matchRound, int matchIndex)
      {
        return this.m_owner.CalculateLineIndex(matchRound, matchIndex);
      }

      public List<CommonUIStateController> GetLineList(int matchRound)
      {
        return this.m_owner.GetLineList(matchRound);
      }

      public void UpdateViewButton(PeakArenaPlayOffMatchupInfo matchupInfo, int index)
      {
        this.m_owner.UpdateViewButton(matchupInfo, index);
      }

      public void OnReplayClick(int replayIndex)
      {
        this.m_owner.OnReplayClick(replayIndex);
      }

      public void OnWatchClick(int watchIndex)
      {
        this.m_owner.OnWatchClick(watchIndex);
      }
    }
  }
}
