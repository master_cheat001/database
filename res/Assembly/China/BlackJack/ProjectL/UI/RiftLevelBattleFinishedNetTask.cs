﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.RiftLevelBattleFinishedNetTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.Protocol;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class RiftLevelBattleFinishedNetTask : UINetTask
  {
    private int m_riftLevelId;
    private ProBattleReport m_battleReport;

    [MethodImpl((MethodImplOptions) 32768)]
    public RiftLevelBattleFinishedNetTask(int riftLevelId, ProBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool StartNetWorking()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnRiftLevelBattleFinishedAck(
      int result,
      int stars,
      BattleReward reward,
      bool isFirstWin,
      List<int> achievements)
    {
      // ISSUE: unable to decompile the method.
    }

    public int Stars { private set; get; }

    public bool IsFirstWin { private set; get; }

    public BattleReward Reward { private set; get; }

    public List<int> Achievements { private set; get; }
  }
}
