﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.UnchartedUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class UnchartedUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./Margin", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./OrganizeTeamButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_teamButton;
    [AutoBind("./StoreButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_storeButton;
    [AutoBind("./Margin/Toggle/DailyToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_dailyToggle;
    [AutoBind("./Margin/Toggle/LimitTimeToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_limitTimeToggle;
    [AutoBind("./Margin/Toggle/ChallengeToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_challengeToggle;
    [AutoBind("./UnchartedList/DailyUnchartedList", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_dailyScrollRect;
    [AutoBind("./UnchartedList/LimitTimeUnchartedList", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_limitTimeScrollRect;
    [AutoBind("./UnchartedList/ChallengeUnchartedList", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_challengeScrollRect;
    [AutoBind("./UnchartedList/DailyUnchartedList/Viewport/Content/Aniki", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_anikiButton;
    [AutoBind("./UnchartedList/DailyUnchartedList/Viewport/Content/Aniki", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_anikiUIStateController;
    [AutoBind("./UnchartedList/DailyUnchartedList/Viewport/Content/Aniki/DailyReward/NumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_anikiDailyRewardText;
    [AutoBind("./UnchartedList/DailyUnchartedList/Viewport/Content/Thearchy", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_thearchyButton;
    [AutoBind("./UnchartedList/DailyUnchartedList/Viewport/Content/Thearchy", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_thearchyUIStateController;
    [AutoBind("./UnchartedList/DailyUnchartedList/Viewport/Content/Thearchy/DailyReward/NumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_thearchyDailyRewardText;
    [AutoBind("./UnchartedList/DailyUnchartedList/Viewport/Content/TreasureMap", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_treasureMapButton;
    [AutoBind("./UnchartedList/DailyUnchartedList/Viewport/Content/TreasureMap", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_treasureMapUIStateController;
    [AutoBind("./UnchartedList/DailyUnchartedList/Viewport/Content/TreasureMap/DailyReward/NumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_treasureMapChallengeCountText;
    [AutoBind("./UnchartedList/DailyUnchartedList/Viewport/Content/MemoryCorridor", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_memoryCorridorButton;
    [AutoBind("./UnchartedList/DailyUnchartedList/Viewport/Content/MemoryCorridor", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_memoryCorridorUIStateController;
    [AutoBind("./UnchartedList/DailyUnchartedList/Viewport/Content/MemoryCorridor/DailyReward/NumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_memoryCorridorDailyRewardText;
    [AutoBind("./UnchartedList/DailyUnchartedList/Viewport/Content/Training", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_heroTrainingButton;
    [AutoBind("./UnchartedList/DailyUnchartedList/Viewport/Content/Training", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_heroTrainingUIStateController;
    [AutoBind("./UnchartedList/DailyUnchartedList/Viewport/Content/Training/DailyReward/NumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroTrainingDailyRewardText;
    [AutoBind("./UnchartedList/DailyUnchartedList/Viewport/Content/ClimbTower", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_climbTowerButton;
    [AutoBind("./UnchartedList/DailyUnchartedList/Viewport/Content/ClimbTower", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_climbTowerUIStateController;
    [AutoBind("./UnchartedList/DailyUnchartedList/Viewport/Content/ClimbTower/DailyReward/NumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_climbTowerFlushtText;
    [AutoBind("./UnchartedList/DailyUnchartedList/Viewport/Content/EternalShrine", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_eternalShrineButton;
    [AutoBind("./UnchartedList/DailyUnchartedList/Viewport/Content/EternalShrine", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_eternalShrineUIStateController;
    [AutoBind("./UnchartedList/DailyUnchartedList/Viewport/Content/EternalShrine/DailyReward/NumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_eternalShrineChallengeCountText;
    [AutoBind("./UnchartedList/ChallengeUnchartedList/Viewport/Content/HeroAnthem", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_heroAnthemButton;
    [AutoBind("./UnchartedList/ChallengeUnchartedList/Viewport/Content/HeroAnthem", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_heroAnthemUIStateController;
    [AutoBind("./UnchartedList/ChallengeUnchartedList/Viewport/Content/AncientCall", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_ancientCallGameObject;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefabs/HeroPhantom", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroPhantomButtonPrefab;
    [AutoBind("./Prefabs/CooperateBattle", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_cooperateBattleButtonPrefab;
    [AutoBind("./Prefabs/UnchartedScore", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_unchartedScoreButtonPrefab;
    [AutoBind("./Prefabs/CollectionActivity", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_collectionActivityButtonPrefab;
    private bool m_isIgnoreToggleEvent;
    private AncientCallActivityButton m_ancientCallActivityButton;
    [DoNotToLua]
    private UnchartedUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_Open_hotfix;
    private LuaFunction m_SetTabTypeInt32_hotfix;
    private LuaFunction m_SetDailyItems_hotfix;
    private LuaFunction m_SetLimitTimeItemsList`1List`1List`1List`1_hotfix;
    private LuaFunction m_SetChallengeItems_hotfix;
    private LuaFunction m_SetClimbTowerFlushTimeTimeSpan_hotfix;
    private LuaFunction m_SetButtonOpenedCommonUIStateControllerGameFunctionType_hotfix;
    private LuaFunction m_CheckButtonOpenedGameFunctionType_hotfix;
    private LuaFunction m_FireEventOnShowUnchartedBattleTypeInt32_hotfix;
    private LuaFunction m_OnReturnButtonClick_hotfix;
    private LuaFunction m_OnTeamButtonClick_hotfix;
    private LuaFunction m_OnStoreButtonClick_hotfix;
    private LuaFunction m_OnDailyToggleValueChangedBoolean_hotfix;
    private LuaFunction m_OnLimitTimeToggleValueChangedBoolean_hotfix;
    private LuaFunction m_OnChallengeToggleValueChangedBoolean_hotfix;
    private LuaFunction m_OnAnikiButtonClick_hotfix;
    private LuaFunction m_OnThearchyButtonClick_hotfix;
    private LuaFunction m_OnTreasureMapButtonClick_hotfix;
    private LuaFunction m_OnMemoryCorridorButtonClick_hotfix;
    private LuaFunction m_OnHeroTrainingButtonClick_hotfix;
    private LuaFunction m_OnClimbTowerButtonClick_hotfix;
    private LuaFunction m_OnEternalShrineButtonClick_hotfix;
    private LuaFunction m_OnHeroAnthemButtonClick_hotfix;
    private LuaFunction m_OnAncientCallButtonClick_hotfix;
    private LuaFunction m_OnHeroPhantomButtonClickHeroPhantomButton_hotfix;
    private LuaFunction m_OnCooperateBattleButtonClickCooperateBattleButton_hotfix;
    private LuaFunction m_OnUnchartedScoreButtonClickUnchartedScoreButton_hotfix;
    private LuaFunction m_OnCollectionButtonClickCollectionActivityButton_hotfix;
    private LuaFunction m_add_EventOnShowUnchartedAction`2_hotfix;
    private LuaFunction m_remove_EventOnShowUnchartedAction`2_hotfix;
    private LuaFunction m_add_EventOnReturnAction_hotfix;
    private LuaFunction m_remove_EventOnReturnAction_hotfix;
    private LuaFunction m_add_EventOnShowTeamAction_hotfix;
    private LuaFunction m_remove_EventOnShowTeamAction_hotfix;
    private LuaFunction m_add_EventOnStoreClickAction_hotfix;
    private LuaFunction m_remove_EventOnStoreClickAction_hotfix;
    private LuaFunction m_add_EventOnChangeTabTypeAction`1_hotfix;
    private LuaFunction m_remove_EventOnChangeTabTypeAction`1_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTabType(int tabIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDailyItems()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLimitTimeItems(
      List<ConfigDataCooperateBattleInfo> cooperateBattleInfos,
      List<ConfigDataHeroPhantomInfo> heroPhantomInfos,
      List<ConfigDataUnchartedScoreInfo> unchartedScoreInfos,
      List<ConfigDataCollectionActivityInfo> collectionActivityInfos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetChallengeItems()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetClimbTowerFlushTime(TimeSpan ts)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetButtonOpened(CommonUIStateController ctrl, GameFunctionType gameFunctionType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CheckButtonOpened(GameFunctionType gameFuncType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FireEventOnShowUncharted(BattleType battleType, int chapterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTeamButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnStoreButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDailyToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnLimitTimeToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChallengeToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAnikiButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnThearchyButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTreasureMapButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMemoryCorridorButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroTrainingButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnClimbTowerButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEternalShrineButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroAnthemButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAncientCallButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroPhantomButtonClick(HeroPhantomButton ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCooperateBattleButtonClick(CooperateBattleButton ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnUnchartedScoreButtonClick(UnchartedScoreButton ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCollectionButtonClick(CollectionActivityButton ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<BattleType, int> EventOnShowUncharted
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowTeam
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnStoreClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnChangeTabType
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public UnchartedUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowUncharted(BattleType arg1, int arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowUncharted(BattleType arg1, int arg2)
    {
      this.EventOnShowUncharted = (Action<BattleType, int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnReturn()
    {
      this.EventOnReturn = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowTeam()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowTeam()
    {
      this.EventOnShowTeam = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnStoreClick()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnStoreClick()
    {
      this.EventOnStoreClick = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnChangeTabType(int obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnChangeTabType(int obj)
    {
      this.EventOnChangeTabType = (Action<int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private UnchartedUIController m_owner;

      public LuaExportHelper(UnchartedUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnShowUncharted(BattleType arg1, int arg2)
      {
        this.m_owner.__callDele_EventOnShowUncharted(arg1, arg2);
      }

      public void __clearDele_EventOnShowUncharted(BattleType arg1, int arg2)
      {
        this.m_owner.__clearDele_EventOnShowUncharted(arg1, arg2);
      }

      public void __callDele_EventOnReturn()
      {
        this.m_owner.__callDele_EventOnReturn();
      }

      public void __clearDele_EventOnReturn()
      {
        this.m_owner.__clearDele_EventOnReturn();
      }

      public void __callDele_EventOnShowTeam()
      {
        this.m_owner.__callDele_EventOnShowTeam();
      }

      public void __clearDele_EventOnShowTeam()
      {
        this.m_owner.__clearDele_EventOnShowTeam();
      }

      public void __callDele_EventOnStoreClick()
      {
        this.m_owner.__callDele_EventOnStoreClick();
      }

      public void __clearDele_EventOnStoreClick()
      {
        this.m_owner.__clearDele_EventOnStoreClick();
      }

      public void __callDele_EventOnChangeTabType(int obj)
      {
        this.m_owner.__callDele_EventOnChangeTabType(obj);
      }

      public void __clearDele_EventOnChangeTabType(int obj)
      {
        this.m_owner.__clearDele_EventOnChangeTabType(obj);
      }

      public CommonUIStateController m_uiStateController
      {
        get
        {
          return this.m_owner.m_uiStateController;
        }
        set
        {
          this.m_owner.m_uiStateController = value;
        }
      }

      public RectTransform m_marginTransform
      {
        get
        {
          return this.m_owner.m_marginTransform;
        }
        set
        {
          this.m_owner.m_marginTransform = value;
        }
      }

      public Button m_returnButton
      {
        get
        {
          return this.m_owner.m_returnButton;
        }
        set
        {
          this.m_owner.m_returnButton = value;
        }
      }

      public Button m_teamButton
      {
        get
        {
          return this.m_owner.m_teamButton;
        }
        set
        {
          this.m_owner.m_teamButton = value;
        }
      }

      public Button m_storeButton
      {
        get
        {
          return this.m_owner.m_storeButton;
        }
        set
        {
          this.m_owner.m_storeButton = value;
        }
      }

      public Toggle m_dailyToggle
      {
        get
        {
          return this.m_owner.m_dailyToggle;
        }
        set
        {
          this.m_owner.m_dailyToggle = value;
        }
      }

      public Toggle m_limitTimeToggle
      {
        get
        {
          return this.m_owner.m_limitTimeToggle;
        }
        set
        {
          this.m_owner.m_limitTimeToggle = value;
        }
      }

      public Toggle m_challengeToggle
      {
        get
        {
          return this.m_owner.m_challengeToggle;
        }
        set
        {
          this.m_owner.m_challengeToggle = value;
        }
      }

      public ScrollRect m_dailyScrollRect
      {
        get
        {
          return this.m_owner.m_dailyScrollRect;
        }
        set
        {
          this.m_owner.m_dailyScrollRect = value;
        }
      }

      public ScrollRect m_limitTimeScrollRect
      {
        get
        {
          return this.m_owner.m_limitTimeScrollRect;
        }
        set
        {
          this.m_owner.m_limitTimeScrollRect = value;
        }
      }

      public ScrollRect m_challengeScrollRect
      {
        get
        {
          return this.m_owner.m_challengeScrollRect;
        }
        set
        {
          this.m_owner.m_challengeScrollRect = value;
        }
      }

      public Button m_anikiButton
      {
        get
        {
          return this.m_owner.m_anikiButton;
        }
        set
        {
          this.m_owner.m_anikiButton = value;
        }
      }

      public CommonUIStateController m_anikiUIStateController
      {
        get
        {
          return this.m_owner.m_anikiUIStateController;
        }
        set
        {
          this.m_owner.m_anikiUIStateController = value;
        }
      }

      public Text m_anikiDailyRewardText
      {
        get
        {
          return this.m_owner.m_anikiDailyRewardText;
        }
        set
        {
          this.m_owner.m_anikiDailyRewardText = value;
        }
      }

      public Button m_thearchyButton
      {
        get
        {
          return this.m_owner.m_thearchyButton;
        }
        set
        {
          this.m_owner.m_thearchyButton = value;
        }
      }

      public CommonUIStateController m_thearchyUIStateController
      {
        get
        {
          return this.m_owner.m_thearchyUIStateController;
        }
        set
        {
          this.m_owner.m_thearchyUIStateController = value;
        }
      }

      public Text m_thearchyDailyRewardText
      {
        get
        {
          return this.m_owner.m_thearchyDailyRewardText;
        }
        set
        {
          this.m_owner.m_thearchyDailyRewardText = value;
        }
      }

      public Button m_treasureMapButton
      {
        get
        {
          return this.m_owner.m_treasureMapButton;
        }
        set
        {
          this.m_owner.m_treasureMapButton = value;
        }
      }

      public CommonUIStateController m_treasureMapUIStateController
      {
        get
        {
          return this.m_owner.m_treasureMapUIStateController;
        }
        set
        {
          this.m_owner.m_treasureMapUIStateController = value;
        }
      }

      public Text m_treasureMapChallengeCountText
      {
        get
        {
          return this.m_owner.m_treasureMapChallengeCountText;
        }
        set
        {
          this.m_owner.m_treasureMapChallengeCountText = value;
        }
      }

      public Button m_memoryCorridorButton
      {
        get
        {
          return this.m_owner.m_memoryCorridorButton;
        }
        set
        {
          this.m_owner.m_memoryCorridorButton = value;
        }
      }

      public CommonUIStateController m_memoryCorridorUIStateController
      {
        get
        {
          return this.m_owner.m_memoryCorridorUIStateController;
        }
        set
        {
          this.m_owner.m_memoryCorridorUIStateController = value;
        }
      }

      public Text m_memoryCorridorDailyRewardText
      {
        get
        {
          return this.m_owner.m_memoryCorridorDailyRewardText;
        }
        set
        {
          this.m_owner.m_memoryCorridorDailyRewardText = value;
        }
      }

      public Button m_heroTrainingButton
      {
        get
        {
          return this.m_owner.m_heroTrainingButton;
        }
        set
        {
          this.m_owner.m_heroTrainingButton = value;
        }
      }

      public CommonUIStateController m_heroTrainingUIStateController
      {
        get
        {
          return this.m_owner.m_heroTrainingUIStateController;
        }
        set
        {
          this.m_owner.m_heroTrainingUIStateController = value;
        }
      }

      public Text m_heroTrainingDailyRewardText
      {
        get
        {
          return this.m_owner.m_heroTrainingDailyRewardText;
        }
        set
        {
          this.m_owner.m_heroTrainingDailyRewardText = value;
        }
      }

      public Button m_climbTowerButton
      {
        get
        {
          return this.m_owner.m_climbTowerButton;
        }
        set
        {
          this.m_owner.m_climbTowerButton = value;
        }
      }

      public CommonUIStateController m_climbTowerUIStateController
      {
        get
        {
          return this.m_owner.m_climbTowerUIStateController;
        }
        set
        {
          this.m_owner.m_climbTowerUIStateController = value;
        }
      }

      public Text m_climbTowerFlushtText
      {
        get
        {
          return this.m_owner.m_climbTowerFlushtText;
        }
        set
        {
          this.m_owner.m_climbTowerFlushtText = value;
        }
      }

      public Button m_eternalShrineButton
      {
        get
        {
          return this.m_owner.m_eternalShrineButton;
        }
        set
        {
          this.m_owner.m_eternalShrineButton = value;
        }
      }

      public CommonUIStateController m_eternalShrineUIStateController
      {
        get
        {
          return this.m_owner.m_eternalShrineUIStateController;
        }
        set
        {
          this.m_owner.m_eternalShrineUIStateController = value;
        }
      }

      public Text m_eternalShrineChallengeCountText
      {
        get
        {
          return this.m_owner.m_eternalShrineChallengeCountText;
        }
        set
        {
          this.m_owner.m_eternalShrineChallengeCountText = value;
        }
      }

      public Button m_heroAnthemButton
      {
        get
        {
          return this.m_owner.m_heroAnthemButton;
        }
        set
        {
          this.m_owner.m_heroAnthemButton = value;
        }
      }

      public CommonUIStateController m_heroAnthemUIStateController
      {
        get
        {
          return this.m_owner.m_heroAnthemUIStateController;
        }
        set
        {
          this.m_owner.m_heroAnthemUIStateController = value;
        }
      }

      public GameObject m_ancientCallGameObject
      {
        get
        {
          return this.m_owner.m_ancientCallGameObject;
        }
        set
        {
          this.m_owner.m_ancientCallGameObject = value;
        }
      }

      public GameObject m_prefabsGameObject
      {
        get
        {
          return this.m_owner.m_prefabsGameObject;
        }
        set
        {
          this.m_owner.m_prefabsGameObject = value;
        }
      }

      public GameObject m_heroPhantomButtonPrefab
      {
        get
        {
          return this.m_owner.m_heroPhantomButtonPrefab;
        }
        set
        {
          this.m_owner.m_heroPhantomButtonPrefab = value;
        }
      }

      public GameObject m_cooperateBattleButtonPrefab
      {
        get
        {
          return this.m_owner.m_cooperateBattleButtonPrefab;
        }
        set
        {
          this.m_owner.m_cooperateBattleButtonPrefab = value;
        }
      }

      public GameObject m_unchartedScoreButtonPrefab
      {
        get
        {
          return this.m_owner.m_unchartedScoreButtonPrefab;
        }
        set
        {
          this.m_owner.m_unchartedScoreButtonPrefab = value;
        }
      }

      public GameObject m_collectionActivityButtonPrefab
      {
        get
        {
          return this.m_owner.m_collectionActivityButtonPrefab;
        }
        set
        {
          this.m_owner.m_collectionActivityButtonPrefab = value;
        }
      }

      public bool m_isIgnoreToggleEvent
      {
        get
        {
          return this.m_owner.m_isIgnoreToggleEvent;
        }
        set
        {
          this.m_owner.m_isIgnoreToggleEvent = value;
        }
      }

      public AncientCallActivityButton m_ancientCallActivityButton
      {
        get
        {
          return this.m_owner.m_ancientCallActivityButton;
        }
        set
        {
          this.m_owner.m_ancientCallActivityButton = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void SetButtonOpened(CommonUIStateController ctrl, GameFunctionType gameFunctionType)
      {
        this.m_owner.SetButtonOpened(ctrl, gameFunctionType);
      }

      public bool CheckButtonOpened(GameFunctionType gameFuncType)
      {
        return this.m_owner.CheckButtonOpened(gameFuncType);
      }

      public void FireEventOnShowUncharted(BattleType battleType, int chapterId)
      {
        this.m_owner.FireEventOnShowUncharted(battleType, chapterId);
      }

      public void OnReturnButtonClick()
      {
        this.m_owner.OnReturnButtonClick();
      }

      public void OnTeamButtonClick()
      {
        this.m_owner.OnTeamButtonClick();
      }

      public void OnStoreButtonClick()
      {
        this.m_owner.OnStoreButtonClick();
      }

      public void OnDailyToggleValueChanged(bool isOn)
      {
        this.m_owner.OnDailyToggleValueChanged(isOn);
      }

      public void OnLimitTimeToggleValueChanged(bool isOn)
      {
        this.m_owner.OnLimitTimeToggleValueChanged(isOn);
      }

      public void OnChallengeToggleValueChanged(bool isOn)
      {
        this.m_owner.OnChallengeToggleValueChanged(isOn);
      }

      public void OnAnikiButtonClick()
      {
        this.m_owner.OnAnikiButtonClick();
      }

      public void OnThearchyButtonClick()
      {
        this.m_owner.OnThearchyButtonClick();
      }

      public void OnTreasureMapButtonClick()
      {
        this.m_owner.OnTreasureMapButtonClick();
      }

      public void OnMemoryCorridorButtonClick()
      {
        this.m_owner.OnMemoryCorridorButtonClick();
      }

      public void OnHeroTrainingButtonClick()
      {
        this.m_owner.OnHeroTrainingButtonClick();
      }

      public void OnClimbTowerButtonClick()
      {
        this.m_owner.OnClimbTowerButtonClick();
      }

      public void OnEternalShrineButtonClick()
      {
        this.m_owner.OnEternalShrineButtonClick();
      }

      public void OnHeroAnthemButtonClick()
      {
        this.m_owner.OnHeroAnthemButtonClick();
      }

      public void OnAncientCallButtonClick()
      {
        this.m_owner.OnAncientCallButtonClick();
      }

      public void OnHeroPhantomButtonClick(HeroPhantomButton ctrl)
      {
        this.m_owner.OnHeroPhantomButtonClick(ctrl);
      }

      public void OnCooperateBattleButtonClick(CooperateBattleButton ctrl)
      {
        this.m_owner.OnCooperateBattleButtonClick(ctrl);
      }

      public void OnUnchartedScoreButtonClick(UnchartedScoreButton ctrl)
      {
        this.m_owner.OnUnchartedScoreButtonClick(ctrl);
      }

      public void OnCollectionButtonClick(CollectionActivityButton ctrl)
      {
        this.m_owner.OnCollectionButtonClick(ctrl);
      }
    }
  }
}
