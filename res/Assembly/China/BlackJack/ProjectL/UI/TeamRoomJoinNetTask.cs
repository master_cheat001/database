﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.TeamRoomJoinNetTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class TeamRoomJoinNetTask : UINetTask
  {
    private int m_roomId;
    private GameFunctionType m_gameFunctionTypeId;
    private int m_locationId;
    private ulong m_inviterSessionId;

    [MethodImpl((MethodImplOptions) 32768)]
    public TeamRoomJoinNetTask(
      int roomId,
      GameFunctionType gameFunctionTypeId,
      int locationId,
      ulong inviterSessionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool StartNetWorking()
    {
      // ISSUE: unable to decompile the method.
    }

    protected void OnTeamRoomJoinAck(int result)
    {
      this.Result = result;
      this.OnTransactionComplete();
    }
  }
}
