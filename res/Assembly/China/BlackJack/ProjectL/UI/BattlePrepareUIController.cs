﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BattlePrepareUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Scene;
using SLua;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class BattlePrepareUIController : UIControllerBase
  {
    private static int s_testSoldierCount0 = 10;
    private static int s_testSoldierCount1 = 10;
    [AutoBind("./Margin", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("./Margin/Title/PauseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_pauseButton;
    [AutoBind("./Margin/Title/ActorCount/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_stageActorCountText;
    [AutoBind("./Margin/Title/BattlePower", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_battlePowerUIStateController;
    [AutoBind("./Margin/Title/BattlePower/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_battlePowerText;
    [AutoBind("./Margin/Title/BattlePower/Arrow", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_battlePowerArrowGo;
    [AutoBind("./Margin/RulePanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_battleRulePanelUIStateController;
    [AutoBind("./Margin/RulePanel/Desc/Title/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_battleRuleTitleText;
    [AutoBind("./Margin/RulePanel/Desc/DescText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_battleRuleDescText;
    [AutoBind("./Margin/ChatButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_chatButton;
    [AutoBind("./Margin/ChatButton/CountPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_chatRedPoint;
    [AutoBind("./Margin/ActionOrderButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_actionOrderButton;
    [AutoBind("./StartButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_startButton;
    [AutoBind("./RecommendButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_recommendHeroButton;
    [AutoBind("./TeamReady", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_teamReadyGameObject;
    [AutoBind("./TeamReady/Time", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_teamReadyTimeUIStateController;
    [AutoBind("./TeamReady/Time/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_teamReadyTimeText;
    [AutoBind("./TeamReady/Wait", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_teamReadyWaitGameObject;
    [AutoBind("./TeamReady/Wait/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_teamReadyWaitTeammateText;
    [AutoBind("./TeamReady/Wait/Text2", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_teamReadyWaitOpponentText;
    [AutoBind("./Test", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_testGameObject;
    [AutoBind("./Test/TestOnStageToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_testOnStageToggle;
    [AutoBind("./Test/SoldierInputField0", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_testSoldierInputField0;
    [AutoBind("./Test/SoldierInputField1", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_testSoldierInputField1;
    [AutoBind("./Test/SkillInputField0", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_testSkillInputField0;
    [AutoBind("./Test/SkillInputField1", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_testSkillInputField1;
    [AutoBind("./Test/TalentInputField0", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_testTalentInputField0;
    [AutoBind("./Test/TalentInputField1", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_testTalentInputField1;
    [AutoBind("./ActorList", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_actorListGameObject;
    [AutoBind("./ActorList/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_actorListScrollRect;
    [AutoBind("./ActorList/Disable", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_actorListDisableGameObject;
    [AutoBind("./TerrainInfo", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_terrainInfoGameObject;
    [AutoBind("./TerrainInfo/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_terrainInfoText;
    [AutoBind("./TerrainInfo/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_terrainInfoImage;
    [AutoBind("./TerrainInfo/Def/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_terrainInfoDefText;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefabs/HeroButton", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroButtonPrefab;
    private GameObject m_armyRelationGameObject;
    private CommonUIStateController m_armyRelationButtonUIStateController;
    private CommonUIStateController m_armyRelationDescUIStateController;
    private ClientBattle m_clientBattle;
    private Camera m_camera;
    private float m_chatRedPointLastTime;
    private bool m_isIgnorePointerClick;
    private BattlePrepareStageActor m_pointerDownStageActor;
    private bool m_isIgnoreToggleEvent;
    private bool m_isSkipStageActorGraphic;
    private int m_stageActorCountMax;
    private int m_turnStageActorCountMax;
    private bool m_isStageActorChanged;
    private List<HeroDragButton> m_heroButtons;
    private List<BattlePrepareStageActor> m_stageActors;
    private List<BattlePrepareTreasure> m_treasures;
    private List<GridPosition>[] m_stagePositions;
    private List<int>[] m_stageDirs;
    private HeroDragButton m_draggingHeroButton;
    private static int s_testSkillId0;
    private static int s_testSkillId1;
    private static int s_testTalentId0;
    private static int s_testTalentId1;
    private static bool s_isTestOnStage;
    private int m_battlePowerValue;
    private Coroutine m_setBattlePowerValueCoroutine;
    private bool m_hasBattleRule;
    [DoNotToLua]
    private BattlePrepareUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_InitializeClientBattle_hotfix;
    private LuaFunction m_OnDisable_hotfix;
    private LuaFunction m_OnApplicationPauseBoolean_hotfix;
    private LuaFunction m_OnApplicationFocusBoolean_hotfix;
    private LuaFunction m_CancelDragging_hotfix;
    private LuaFunction m_Update_hotfix;
    private LuaFunction m_CheckStageActorChange_hotfix;
    private LuaFunction m_PrepareBattle_hotfix;
    private LuaFunction m_PauseBoolean_hotfix;
    private LuaFunction m_SetStageActorCountMaxInt32_hotfix;
    private LuaFunction m_SetTurnStageActorCountMaxInt32_hotfix;
    private LuaFunction m_ClearHeros_hotfix;
    private LuaFunction m_AddHeroBattleHeroStageActorTagType_hotfix;
    private LuaFunction m_ClearStagePositions_hotfix;
    private LuaFunction m_AddStagePositionGridPositionInt32StagePositionType_hotfix;
    private LuaFunction m_GetStagePositionsStagePositionType_hotfix;
    private LuaFunction m_GetStageDirectionGridPositionStagePositionType_hotfix;
    private LuaFunction m_CreateHeroButtonBattleHeroTransform_hotfix;
    private LuaFunction m_SkipStageActorGraphicBoolean_hotfix;
    private LuaFunction m_EnableHeroOnStageBoolean_hotfix;
    private LuaFunction m_HeroOnStageBattleHeroGridPositionInt32Int32StagePositionTypeStageActorTagTypeInt32Int32Int32_hotfix;
    private LuaFunction m_HeroOnStageBattleHeroGridPositionInt32StagePositionTypeInt32StageActorTagType_hotfix;
    private LuaFunction m_ActorOffStageBattlePrepareStageActor_hotfix;
    private LuaFunction m_ActorOnStageMoveBattlePrepareStageActorGridPosition_hotfix;
    private LuaFunction m_ActorOnStageExchangeBattlePrepareStageActorBattlePrepareStageActor_hotfix;
    private LuaFunction m_SortChildByYTransform_hotfix;
    private LuaFunction m_ClearStageActors_hotfix;
    private LuaFunction m_GetStageActorGridPosition_hotfix;
    private LuaFunction m_GetStageActorByHeroIdInt32_hotfix;
    private LuaFunction m_GetStageActors_hotfix;
    private LuaFunction m_UpdateStageActorBattleHero_hotfix;
    private LuaFunction m_AddBattleTreasureConfigDataBattleTreasureInfoBoolean_hotfix;
    private LuaFunction m_ClearBattleTreasures_hotfix;
    private LuaFunction m_ShowTerrainInfoConfigDataTerrainInfo_hotfix;
    private LuaFunction m_HideTerrainInfo_hotfix;
    private LuaFunction m_ShowArmyRelationDesc_hotfix;
    private LuaFunction m_HideArmyRelationDesc_hotfix;
    private LuaFunction m_HideArmyRelation_hotfix;
    private LuaFunction m_IsArmyRelationDescVisible_hotfix;
    private LuaFunction m_ShowClimbTowerBattleRuleConfigDataTowerBattleRuleInfo_hotfix;
    private LuaFunction m_ShowEternalShrineBattleRuleConfigDataEternalShrineInfo_hotfix;
    private LuaFunction m_ShowBattleRulePanel_hotfix;
    private LuaFunction m_HideBattleRulePanel_hotfix;
    private LuaFunction m_ShowRecommendHeroButtonBoolean_hotfix;
    private LuaFunction m_ShowActionOrderButtonBoolean_hotfix;
    private LuaFunction m_ShowChatButtonBoolean_hotfix;
    private LuaFunction m_SetBattlePowerInt32_hotfix;
    private LuaFunction m_Co_SetBattlePowerValueInt32Int32_hotfix;
    private LuaFunction m_UpdateTestUI_hotfix;
    private LuaFunction m_UpdateTestValuesToInputField_hotfix;
    private LuaFunction m_UpdateTestValuesFromInputField_hotfix;
    private LuaFunction m_ShowStartButtonBoolean_hotfix;
    private LuaFunction m_ShowHeroListBoolean_hotfix;
    private LuaFunction m_ShowTeamReadyCountdownTimeSpan_hotfix;
    private LuaFunction m_HideTeamReadyCountdown_hotfix;
    private LuaFunction m_ShowTeamReadyWaitBattleRoomType_hotfix;
    private LuaFunction m_HideTeamReadyWait_hotfix;
    private LuaFunction m_OnPauseButtonClick_hotfix;
    private LuaFunction m_OnArmyRelationButtonClick_hotfix;
    private LuaFunction m_OnStartButtonClick_hotfix;
    private LuaFunction m_OnRecommendHeroButtonClick_hotfix;
    private LuaFunction m_OnActionOrderButtonClick_hotfix;
    private LuaFunction m_OnChatButtonClick_hotfix;
    private LuaFunction m_OnHelpButtonClick_hotfix;
    private LuaFunction m_IsTestBattle_hotfix;
    private LuaFunction m_IsDeveloperSingleBattle_hotfix;
    private LuaFunction m_OnTestOnStageToggleBoolean_hotfix;
    private LuaFunction m_IsTestOnStage_hotfix;
    private LuaFunction m_GetTestSoldierCountInt32_hotfix;
    private LuaFunction m_GetTestSkillIdInt32_hotfix;
    private LuaFunction m_GetTestTalentIdInt32_hotfix;
    private LuaFunction m_ClearPointerDownStageActor_hotfix;
    private LuaFunction m_OnScenePointerDownPointerEventData_hotfix;
    private LuaFunction m_OnScenePointerUpPointerEventData_hotfix;
    private LuaFunction m_OnScenePointerClickPointerEventData_hotfix;
    private LuaFunction m_OnSceneBeginDragPointerEventData_hotfix;
    private LuaFunction m_OnSceneEndDragPointerEventData_hotfix;
    private LuaFunction m_OnSceneDragPointerEventData_hotfix;
    private LuaFunction m_OnScene3DTouchVector2_hotfix;
    private LuaFunction m_CreateDraggingHeroButtonBattleHero_hotfix;
    private LuaFunction m_DestroyDragginHeroButton_hotfix;
    private LuaFunction m_MoveDraggingHeroButtonVector2_hotfix;
    private LuaFunction m_DropHeroButtonVector2_hotfix;
    private LuaFunction m_ComputeStageActorCount_hotfix;
    private LuaFunction m_ComputeSelfStageActorCount_hotfix;
    private LuaFunction m_UpdateStageActorCount_hotfix;
    private LuaFunction m_HeroDragButton_OnBeginDragHeroDragButtonPointerEventData_hotfix;
    private LuaFunction m_HeroDragButton_OnEndDragHeroDragButtonPointerEventData_hotfix;
    private LuaFunction m_HeroDragButton_OnDragPointerEventData_hotfix;
    private LuaFunction m_HeroDragButton_OnDropPointerEventData_hotfix;
    private LuaFunction m_HeroDragButton_OnClickHeroDragButton_hotfix;
    private LuaFunction m_RefreshChatRedState_hotfix;
    private LuaFunction m_HideActorInfo_hotfix;
    private LuaFunction m_add_EventOnPauseBattleAction_hotfix;
    private LuaFunction m_remove_EventOnPauseBattleAction_hotfix;
    private LuaFunction m_add_EventOnShowArmyRelationAction_hotfix;
    private LuaFunction m_remove_EventOnShowArmyRelationAction_hotfix;
    private LuaFunction m_add_EventOnStartAction_hotfix;
    private LuaFunction m_remove_EventOnStartAction_hotfix;
    private LuaFunction m_add_EventOnShowRecommendHeroAction_hotfix;
    private LuaFunction m_remove_EventOnShowRecommendHeroAction_hotfix;
    private LuaFunction m_add_EventOnShowActionOrderAction_hotfix;
    private LuaFunction m_remove_EventOnShowActionOrderAction_hotfix;
    private LuaFunction m_add_EventOnTestOnStageAction_hotfix;
    private LuaFunction m_remove_EventOnTestOnStageAction_hotfix;
    private LuaFunction m_add_EventOnShowMyActorInfoAction`1_hotfix;
    private LuaFunction m_remove_EventOnShowMyActorInfoAction`1_hotfix;
    private LuaFunction m_add_EventOnHideActorInfoAction_hotfix;
    private LuaFunction m_remove_EventOnHideActorInfoAction_hotfix;
    private LuaFunction m_add_EventOnStageActorChangeAction_hotfix;
    private LuaFunction m_remove_EventOnStageActorChangeAction_hotfix;
    private LuaFunction m_add_EventOnShowChatAction_hotfix;
    private LuaFunction m_remove_EventOnShowChatAction_hotfix;
    private LuaFunction m_add_EventOnShowHelpAction_hotfix;
    private LuaFunction m_remove_EventOnShowHelpAction_hotfix;
    private LuaFunction m_add_EventOnHeroOnStageAction`3_hotfix;
    private LuaFunction m_remove_EventOnHeroOnStageAction`3_hotfix;
    private LuaFunction m_add_EventOnStageActorOffStageAction`1_hotfix;
    private LuaFunction m_remove_EventOnStageActorOffStageAction`1_hotfix;
    private LuaFunction m_add_EventOnStageActorMoveAction`2_hotfix;
    private LuaFunction m_remove_EventOnStageActorMoveAction`2_hotfix;
    private LuaFunction m_add_EventOnStageActorSwapAction`2_hotfix;
    private LuaFunction m_remove_EventOnStageActorSwapAction`2_hotfix;
    private LuaFunction m_add_EventOnPointerDownAction`2_hotfix;
    private LuaFunction m_remove_EventOnPointerDownAction`2_hotfix;
    private LuaFunction m_add_EventOnPointerUpAction`2_hotfix;
    private LuaFunction m_remove_EventOnPointerUpAction`2_hotfix;
    private LuaFunction m_add_EventOnPointerClickAction`2_hotfix;
    private LuaFunction m_remove_EventOnPointerClickAction`2_hotfix;
    private LuaFunction m_add_EventOnBeginDragHeroAction_hotfix;
    private LuaFunction m_remove_EventOnBeginDragHeroAction_hotfix;
    private LuaFunction m_add_EventOnEndDragHeroAction_hotfix;
    private LuaFunction m_remove_EventOnEndDragHeroAction_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    private BattlePrepareUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Initialize(ClientBattle clientBattle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDisable()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnApplicationPause(bool isPause)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnApplicationFocus(bool focus)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CancelDragging()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CheckStageActorChange()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PrepareBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Pause(bool pause)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetStageActorCountMax(int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTurnStageActorCountMax(int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearHeros()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddHero(BattleHero hero, StageActorTagType tagType = StageActorTagType.None)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearStagePositions()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddStagePosition(GridPosition p, int dir, StagePositionType posType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<GridPosition> GetStagePositions(StagePositionType posType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetStageDirection(GridPosition p, StagePositionType posType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private HeroDragButton CreateHeroButton(BattleHero hero, Transform parent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SkipStageActorGraphic(bool skip)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void EnableHeroOnStage(bool enable)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattlePrepareStageActor HeroOnStage(
      BattleHero hero,
      GridPosition pos,
      int dir,
      int team,
      StagePositionType posType,
      StageActorTagType tagType = StageActorTagType.None,
      int behaviorId = -1,
      int groupId = 0,
      int playerIndex = -1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool IsSkillListEqual(List<int> skillIds1, List<int> skillIds2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattlePrepareStageActor HeroOnStage(
      BattleHero hero,
      GridPosition pos,
      int team,
      StagePositionType posType,
      int playerIndex,
      StageActorTagType tagType = StageActorTagType.None)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ActorOffStage(BattlePrepareStageActor sa)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ActorOnStageMove(BattlePrepareStageActor sa, GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ActorOnStageExchange(BattlePrepareStageActor sa1, BattlePrepareStageActor sa2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SortChildByY(Transform parent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int CompareTransformY(Transform t0, Transform t1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearStageActors()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattlePrepareStageActor GetStageActor(GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattlePrepareStageActor GetStageActorByHeroId(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BattlePrepareStageActor> GetStageActors()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateStageActor(BattleHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddBattleTreasure(ConfigDataBattleTreasureInfo treasureInfo, bool isOpened)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearBattleTreasures()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowTerrainInfo(ConfigDataTerrainInfo terrain)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideTerrainInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowArmyRelationDesc()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideArmyRelationDesc()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideArmyRelation()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsArmyRelationDescVisible()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowClimbTowerBattleRule(ConfigDataTowerBattleRuleInfo ruleInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowEternalShrineBattleRule(ConfigDataEternalShrineInfo eternalShrineInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowBattleRulePanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HideBattleRulePanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowRecommendHeroButton(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowActionOrderButton(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowChatButton(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBattlePower(int battlePower)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_SetBattlePowerValue(int newValue, int oldValue)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateTestUI()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateTestValuesToInputField()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void SetInputFieldValue(InputField inputField, int value, int defaultValue)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateTestValuesFromInputField()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int GetInputFieldValue(InputField inputField, int defaultValue)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowStartButton(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowHeroList(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowTeamReadyCountdown(TimeSpan ts)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HideTeamReadyCountdown()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowTeamReadyWait(BattleRoomType battleRoomType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HideTeamReadyWait()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPauseButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnArmyRelationButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnStartButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRecommendHeroButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnActionOrderButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChatButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHelpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsTestBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsDeveloperSingleBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTestOnStageToggle(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsTestOnStage()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetTestSoldierCount(int team)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetTestSkillId(int team)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetTestTalentId(int team)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearPointerDownStageActor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnScenePointerDown(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnScenePointerUp(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnScenePointerClick(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnSceneBeginDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnSceneEndDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnSceneDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnScene3DTouch(Vector2 p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateDraggingHeroButton(BattleHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DestroyDragginHeroButton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void MoveDraggingHeroButton(Vector2 pos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DropHeroButton(Vector2 pos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ComputeStageActorCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ComputeSelfStageActorCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateStageActorCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HeroDragButton_OnBeginDrag(HeroDragButton b, PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HeroDragButton_OnEndDrag(HeroDragButton b, PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HeroDragButton_OnDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HeroDragButton_OnDrop(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HeroDragButton_OnClick(HeroDragButton b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RefreshChatRedState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HideActorInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnPauseBattle
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowArmyRelation
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnStart
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowRecommendHero
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowActionOrder
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnTestOnStage
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<BattleHero> EventOnShowMyActorInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnHideActorInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnStageActorChange
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowChat
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowHelp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<BattleHero, GridPosition, int> EventOnHeroOnStage
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<BattlePrepareStageActor> EventOnStageActorOffStage
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<BattlePrepareStageActor, GridPosition> EventOnStageActorMove
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<BattlePrepareStageActor, BattlePrepareStageActor> EventOnStageActorSwap
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<PointerEventData.InputButton, Vector2> EventOnPointerDown
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<PointerEventData.InputButton, Vector2> EventOnPointerUp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<PointerEventData.InputButton, Vector2> EventOnPointerClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBeginDragHero
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnEndDragHero
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public BattlePrepareUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnPauseBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnPauseBattle()
    {
      this.EventOnPauseBattle = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowArmyRelation()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowArmyRelation()
    {
      this.EventOnShowArmyRelation = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnStart()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnStart()
    {
      this.EventOnStart = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowRecommendHero()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowRecommendHero()
    {
      this.EventOnShowRecommendHero = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowActionOrder()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowActionOrder()
    {
      this.EventOnShowActionOrder = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnTestOnStage()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnTestOnStage()
    {
      this.EventOnTestOnStage = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowMyActorInfo(BattleHero obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowMyActorInfo(BattleHero obj)
    {
      this.EventOnShowMyActorInfo = (Action<BattleHero>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnHideActorInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnHideActorInfo()
    {
      this.EventOnHideActorInfo = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnStageActorChange()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnStageActorChange()
    {
      this.EventOnStageActorChange = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowChat()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowChat()
    {
      this.EventOnShowChat = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowHelp()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowHelp()
    {
      this.EventOnShowHelp = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnHeroOnStage(BattleHero arg1, GridPosition arg2, int arg3)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnHeroOnStage(BattleHero arg1, GridPosition arg2, int arg3)
    {
      this.EventOnHeroOnStage = (Action<BattleHero, GridPosition, int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnStageActorOffStage(BattlePrepareStageActor obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnStageActorOffStage(BattlePrepareStageActor obj)
    {
      this.EventOnStageActorOffStage = (Action<BattlePrepareStageActor>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnStageActorMove(BattlePrepareStageActor arg1, GridPosition arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnStageActorMove(BattlePrepareStageActor arg1, GridPosition arg2)
    {
      this.EventOnStageActorMove = (Action<BattlePrepareStageActor, GridPosition>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnStageActorSwap(
      BattlePrepareStageActor arg1,
      BattlePrepareStageActor arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnStageActorSwap(
      BattlePrepareStageActor arg1,
      BattlePrepareStageActor arg2)
    {
      this.EventOnStageActorSwap = (Action<BattlePrepareStageActor, BattlePrepareStageActor>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnPointerDown(PointerEventData.InputButton arg1, Vector2 arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnPointerDown(PointerEventData.InputButton arg1, Vector2 arg2)
    {
      this.EventOnPointerDown = (Action<PointerEventData.InputButton, Vector2>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnPointerUp(PointerEventData.InputButton arg1, Vector2 arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnPointerUp(PointerEventData.InputButton arg1, Vector2 arg2)
    {
      this.EventOnPointerUp = (Action<PointerEventData.InputButton, Vector2>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnPointerClick(PointerEventData.InputButton arg1, Vector2 arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnPointerClick(PointerEventData.InputButton arg1, Vector2 arg2)
    {
      this.EventOnPointerClick = (Action<PointerEventData.InputButton, Vector2>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnBeginDragHero()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnBeginDragHero()
    {
      this.EventOnBeginDragHero = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnEndDragHero()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnEndDragHero()
    {
      this.EventOnEndDragHero = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private BattlePrepareUIController m_owner;

      public LuaExportHelper(BattlePrepareUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnPauseBattle()
      {
        this.m_owner.__callDele_EventOnPauseBattle();
      }

      public void __clearDele_EventOnPauseBattle()
      {
        this.m_owner.__clearDele_EventOnPauseBattle();
      }

      public void __callDele_EventOnShowArmyRelation()
      {
        this.m_owner.__callDele_EventOnShowArmyRelation();
      }

      public void __clearDele_EventOnShowArmyRelation()
      {
        this.m_owner.__clearDele_EventOnShowArmyRelation();
      }

      public void __callDele_EventOnStart()
      {
        this.m_owner.__callDele_EventOnStart();
      }

      public void __clearDele_EventOnStart()
      {
        this.m_owner.__clearDele_EventOnStart();
      }

      public void __callDele_EventOnShowRecommendHero()
      {
        this.m_owner.__callDele_EventOnShowRecommendHero();
      }

      public void __clearDele_EventOnShowRecommendHero()
      {
        this.m_owner.__clearDele_EventOnShowRecommendHero();
      }

      public void __callDele_EventOnShowActionOrder()
      {
        this.m_owner.__callDele_EventOnShowActionOrder();
      }

      public void __clearDele_EventOnShowActionOrder()
      {
        this.m_owner.__clearDele_EventOnShowActionOrder();
      }

      public void __callDele_EventOnTestOnStage()
      {
        this.m_owner.__callDele_EventOnTestOnStage();
      }

      public void __clearDele_EventOnTestOnStage()
      {
        this.m_owner.__clearDele_EventOnTestOnStage();
      }

      public void __callDele_EventOnShowMyActorInfo(BattleHero obj)
      {
        this.m_owner.__callDele_EventOnShowMyActorInfo(obj);
      }

      public void __clearDele_EventOnShowMyActorInfo(BattleHero obj)
      {
        this.m_owner.__clearDele_EventOnShowMyActorInfo(obj);
      }

      public void __callDele_EventOnHideActorInfo()
      {
        this.m_owner.__callDele_EventOnHideActorInfo();
      }

      public void __clearDele_EventOnHideActorInfo()
      {
        this.m_owner.__clearDele_EventOnHideActorInfo();
      }

      public void __callDele_EventOnStageActorChange()
      {
        this.m_owner.__callDele_EventOnStageActorChange();
      }

      public void __clearDele_EventOnStageActorChange()
      {
        this.m_owner.__clearDele_EventOnStageActorChange();
      }

      public void __callDele_EventOnShowChat()
      {
        this.m_owner.__callDele_EventOnShowChat();
      }

      public void __clearDele_EventOnShowChat()
      {
        this.m_owner.__clearDele_EventOnShowChat();
      }

      public void __callDele_EventOnShowHelp()
      {
        this.m_owner.__callDele_EventOnShowHelp();
      }

      public void __clearDele_EventOnShowHelp()
      {
        this.m_owner.__clearDele_EventOnShowHelp();
      }

      public void __callDele_EventOnHeroOnStage(BattleHero arg1, GridPosition arg2, int arg3)
      {
        this.m_owner.__callDele_EventOnHeroOnStage(arg1, arg2, arg3);
      }

      public void __clearDele_EventOnHeroOnStage(BattleHero arg1, GridPosition arg2, int arg3)
      {
        this.m_owner.__clearDele_EventOnHeroOnStage(arg1, arg2, arg3);
      }

      public void __callDele_EventOnStageActorOffStage(BattlePrepareStageActor obj)
      {
        this.m_owner.__callDele_EventOnStageActorOffStage(obj);
      }

      public void __clearDele_EventOnStageActorOffStage(BattlePrepareStageActor obj)
      {
        this.m_owner.__clearDele_EventOnStageActorOffStage(obj);
      }

      public void __callDele_EventOnStageActorMove(BattlePrepareStageActor arg1, GridPosition arg2)
      {
        this.m_owner.__callDele_EventOnStageActorMove(arg1, arg2);
      }

      public void __clearDele_EventOnStageActorMove(BattlePrepareStageActor arg1, GridPosition arg2)
      {
        this.m_owner.__clearDele_EventOnStageActorMove(arg1, arg2);
      }

      public void __callDele_EventOnStageActorSwap(
        BattlePrepareStageActor arg1,
        BattlePrepareStageActor arg2)
      {
        this.m_owner.__callDele_EventOnStageActorSwap(arg1, arg2);
      }

      public void __clearDele_EventOnStageActorSwap(
        BattlePrepareStageActor arg1,
        BattlePrepareStageActor arg2)
      {
        this.m_owner.__clearDele_EventOnStageActorSwap(arg1, arg2);
      }

      public void __callDele_EventOnPointerDown(PointerEventData.InputButton arg1, Vector2 arg2)
      {
        this.m_owner.__callDele_EventOnPointerDown(arg1, arg2);
      }

      public void __clearDele_EventOnPointerDown(PointerEventData.InputButton arg1, Vector2 arg2)
      {
        this.m_owner.__clearDele_EventOnPointerDown(arg1, arg2);
      }

      public void __callDele_EventOnPointerUp(PointerEventData.InputButton arg1, Vector2 arg2)
      {
        this.m_owner.__callDele_EventOnPointerUp(arg1, arg2);
      }

      public void __clearDele_EventOnPointerUp(PointerEventData.InputButton arg1, Vector2 arg2)
      {
        this.m_owner.__clearDele_EventOnPointerUp(arg1, arg2);
      }

      public void __callDele_EventOnPointerClick(PointerEventData.InputButton arg1, Vector2 arg2)
      {
        this.m_owner.__callDele_EventOnPointerClick(arg1, arg2);
      }

      public void __clearDele_EventOnPointerClick(PointerEventData.InputButton arg1, Vector2 arg2)
      {
        this.m_owner.__clearDele_EventOnPointerClick(arg1, arg2);
      }

      public void __callDele_EventOnBeginDragHero()
      {
        this.m_owner.__callDele_EventOnBeginDragHero();
      }

      public void __clearDele_EventOnBeginDragHero()
      {
        this.m_owner.__clearDele_EventOnBeginDragHero();
      }

      public void __callDele_EventOnEndDragHero()
      {
        this.m_owner.__callDele_EventOnEndDragHero();
      }

      public void __clearDele_EventOnEndDragHero()
      {
        this.m_owner.__clearDele_EventOnEndDragHero();
      }

      public RectTransform m_marginTransform
      {
        get
        {
          return this.m_owner.m_marginTransform;
        }
        set
        {
          this.m_owner.m_marginTransform = value;
        }
      }

      public Button m_pauseButton
      {
        get
        {
          return this.m_owner.m_pauseButton;
        }
        set
        {
          this.m_owner.m_pauseButton = value;
        }
      }

      public Text m_stageActorCountText
      {
        get
        {
          return this.m_owner.m_stageActorCountText;
        }
        set
        {
          this.m_owner.m_stageActorCountText = value;
        }
      }

      public CommonUIStateController m_battlePowerUIStateController
      {
        get
        {
          return this.m_owner.m_battlePowerUIStateController;
        }
        set
        {
          this.m_owner.m_battlePowerUIStateController = value;
        }
      }

      public Text m_battlePowerText
      {
        get
        {
          return this.m_owner.m_battlePowerText;
        }
        set
        {
          this.m_owner.m_battlePowerText = value;
        }
      }

      public GameObject m_battlePowerArrowGo
      {
        get
        {
          return this.m_owner.m_battlePowerArrowGo;
        }
        set
        {
          this.m_owner.m_battlePowerArrowGo = value;
        }
      }

      public CommonUIStateController m_battleRulePanelUIStateController
      {
        get
        {
          return this.m_owner.m_battleRulePanelUIStateController;
        }
        set
        {
          this.m_owner.m_battleRulePanelUIStateController = value;
        }
      }

      public Text m_battleRuleTitleText
      {
        get
        {
          return this.m_owner.m_battleRuleTitleText;
        }
        set
        {
          this.m_owner.m_battleRuleTitleText = value;
        }
      }

      public Text m_battleRuleDescText
      {
        get
        {
          return this.m_owner.m_battleRuleDescText;
        }
        set
        {
          this.m_owner.m_battleRuleDescText = value;
        }
      }

      public Button m_chatButton
      {
        get
        {
          return this.m_owner.m_chatButton;
        }
        set
        {
          this.m_owner.m_chatButton = value;
        }
      }

      public GameObject m_chatRedPoint
      {
        get
        {
          return this.m_owner.m_chatRedPoint;
        }
        set
        {
          this.m_owner.m_chatRedPoint = value;
        }
      }

      public Button m_actionOrderButton
      {
        get
        {
          return this.m_owner.m_actionOrderButton;
        }
        set
        {
          this.m_owner.m_actionOrderButton = value;
        }
      }

      public Button m_startButton
      {
        get
        {
          return this.m_owner.m_startButton;
        }
        set
        {
          this.m_owner.m_startButton = value;
        }
      }

      public Button m_recommendHeroButton
      {
        get
        {
          return this.m_owner.m_recommendHeroButton;
        }
        set
        {
          this.m_owner.m_recommendHeroButton = value;
        }
      }

      public GameObject m_teamReadyGameObject
      {
        get
        {
          return this.m_owner.m_teamReadyGameObject;
        }
        set
        {
          this.m_owner.m_teamReadyGameObject = value;
        }
      }

      public CommonUIStateController m_teamReadyTimeUIStateController
      {
        get
        {
          return this.m_owner.m_teamReadyTimeUIStateController;
        }
        set
        {
          this.m_owner.m_teamReadyTimeUIStateController = value;
        }
      }

      public Text m_teamReadyTimeText
      {
        get
        {
          return this.m_owner.m_teamReadyTimeText;
        }
        set
        {
          this.m_owner.m_teamReadyTimeText = value;
        }
      }

      public GameObject m_teamReadyWaitGameObject
      {
        get
        {
          return this.m_owner.m_teamReadyWaitGameObject;
        }
        set
        {
          this.m_owner.m_teamReadyWaitGameObject = value;
        }
      }

      public Text m_teamReadyWaitTeammateText
      {
        get
        {
          return this.m_owner.m_teamReadyWaitTeammateText;
        }
        set
        {
          this.m_owner.m_teamReadyWaitTeammateText = value;
        }
      }

      public Text m_teamReadyWaitOpponentText
      {
        get
        {
          return this.m_owner.m_teamReadyWaitOpponentText;
        }
        set
        {
          this.m_owner.m_teamReadyWaitOpponentText = value;
        }
      }

      public GameObject m_testGameObject
      {
        get
        {
          return this.m_owner.m_testGameObject;
        }
        set
        {
          this.m_owner.m_testGameObject = value;
        }
      }

      public Toggle m_testOnStageToggle
      {
        get
        {
          return this.m_owner.m_testOnStageToggle;
        }
        set
        {
          this.m_owner.m_testOnStageToggle = value;
        }
      }

      public InputField m_testSoldierInputField0
      {
        get
        {
          return this.m_owner.m_testSoldierInputField0;
        }
        set
        {
          this.m_owner.m_testSoldierInputField0 = value;
        }
      }

      public InputField m_testSoldierInputField1
      {
        get
        {
          return this.m_owner.m_testSoldierInputField1;
        }
        set
        {
          this.m_owner.m_testSoldierInputField1 = value;
        }
      }

      public InputField m_testSkillInputField0
      {
        get
        {
          return this.m_owner.m_testSkillInputField0;
        }
        set
        {
          this.m_owner.m_testSkillInputField0 = value;
        }
      }

      public InputField m_testSkillInputField1
      {
        get
        {
          return this.m_owner.m_testSkillInputField1;
        }
        set
        {
          this.m_owner.m_testSkillInputField1 = value;
        }
      }

      public InputField m_testTalentInputField0
      {
        get
        {
          return this.m_owner.m_testTalentInputField0;
        }
        set
        {
          this.m_owner.m_testTalentInputField0 = value;
        }
      }

      public InputField m_testTalentInputField1
      {
        get
        {
          return this.m_owner.m_testTalentInputField1;
        }
        set
        {
          this.m_owner.m_testTalentInputField1 = value;
        }
      }

      public GameObject m_actorListGameObject
      {
        get
        {
          return this.m_owner.m_actorListGameObject;
        }
        set
        {
          this.m_owner.m_actorListGameObject = value;
        }
      }

      public ScrollRect m_actorListScrollRect
      {
        get
        {
          return this.m_owner.m_actorListScrollRect;
        }
        set
        {
          this.m_owner.m_actorListScrollRect = value;
        }
      }

      public GameObject m_actorListDisableGameObject
      {
        get
        {
          return this.m_owner.m_actorListDisableGameObject;
        }
        set
        {
          this.m_owner.m_actorListDisableGameObject = value;
        }
      }

      public GameObject m_terrainInfoGameObject
      {
        get
        {
          return this.m_owner.m_terrainInfoGameObject;
        }
        set
        {
          this.m_owner.m_terrainInfoGameObject = value;
        }
      }

      public Text m_terrainInfoText
      {
        get
        {
          return this.m_owner.m_terrainInfoText;
        }
        set
        {
          this.m_owner.m_terrainInfoText = value;
        }
      }

      public Image m_terrainInfoImage
      {
        get
        {
          return this.m_owner.m_terrainInfoImage;
        }
        set
        {
          this.m_owner.m_terrainInfoImage = value;
        }
      }

      public Text m_terrainInfoDefText
      {
        get
        {
          return this.m_owner.m_terrainInfoDefText;
        }
        set
        {
          this.m_owner.m_terrainInfoDefText = value;
        }
      }

      public GameObject m_prefabsGameObject
      {
        get
        {
          return this.m_owner.m_prefabsGameObject;
        }
        set
        {
          this.m_owner.m_prefabsGameObject = value;
        }
      }

      public GameObject m_heroButtonPrefab
      {
        get
        {
          return this.m_owner.m_heroButtonPrefab;
        }
        set
        {
          this.m_owner.m_heroButtonPrefab = value;
        }
      }

      public GameObject m_armyRelationGameObject
      {
        get
        {
          return this.m_owner.m_armyRelationGameObject;
        }
        set
        {
          this.m_owner.m_armyRelationGameObject = value;
        }
      }

      public CommonUIStateController m_armyRelationButtonUIStateController
      {
        get
        {
          return this.m_owner.m_armyRelationButtonUIStateController;
        }
        set
        {
          this.m_owner.m_armyRelationButtonUIStateController = value;
        }
      }

      public CommonUIStateController m_armyRelationDescUIStateController
      {
        get
        {
          return this.m_owner.m_armyRelationDescUIStateController;
        }
        set
        {
          this.m_owner.m_armyRelationDescUIStateController = value;
        }
      }

      public ClientBattle m_clientBattle
      {
        get
        {
          return this.m_owner.m_clientBattle;
        }
        set
        {
          this.m_owner.m_clientBattle = value;
        }
      }

      public Camera m_camera
      {
        get
        {
          return this.m_owner.m_camera;
        }
        set
        {
          this.m_owner.m_camera = value;
        }
      }

      public float m_chatRedPointLastTime
      {
        get
        {
          return this.m_owner.m_chatRedPointLastTime;
        }
        set
        {
          this.m_owner.m_chatRedPointLastTime = value;
        }
      }

      public bool m_isIgnorePointerClick
      {
        get
        {
          return this.m_owner.m_isIgnorePointerClick;
        }
        set
        {
          this.m_owner.m_isIgnorePointerClick = value;
        }
      }

      public BattlePrepareStageActor m_pointerDownStageActor
      {
        get
        {
          return this.m_owner.m_pointerDownStageActor;
        }
        set
        {
          this.m_owner.m_pointerDownStageActor = value;
        }
      }

      public bool m_isIgnoreToggleEvent
      {
        get
        {
          return this.m_owner.m_isIgnoreToggleEvent;
        }
        set
        {
          this.m_owner.m_isIgnoreToggleEvent = value;
        }
      }

      public bool m_isSkipStageActorGraphic
      {
        get
        {
          return this.m_owner.m_isSkipStageActorGraphic;
        }
        set
        {
          this.m_owner.m_isSkipStageActorGraphic = value;
        }
      }

      public int m_stageActorCountMax
      {
        get
        {
          return this.m_owner.m_stageActorCountMax;
        }
        set
        {
          this.m_owner.m_stageActorCountMax = value;
        }
      }

      public int m_turnStageActorCountMax
      {
        get
        {
          return this.m_owner.m_turnStageActorCountMax;
        }
        set
        {
          this.m_owner.m_turnStageActorCountMax = value;
        }
      }

      public bool m_isStageActorChanged
      {
        get
        {
          return this.m_owner.m_isStageActorChanged;
        }
        set
        {
          this.m_owner.m_isStageActorChanged = value;
        }
      }

      public List<HeroDragButton> m_heroButtons
      {
        get
        {
          return this.m_owner.m_heroButtons;
        }
        set
        {
          this.m_owner.m_heroButtons = value;
        }
      }

      public List<BattlePrepareStageActor> m_stageActors
      {
        get
        {
          return this.m_owner.m_stageActors;
        }
        set
        {
          this.m_owner.m_stageActors = value;
        }
      }

      public List<BattlePrepareTreasure> m_treasures
      {
        get
        {
          return this.m_owner.m_treasures;
        }
        set
        {
          this.m_owner.m_treasures = value;
        }
      }

      public List<GridPosition>[] m_stagePositions
      {
        get
        {
          return this.m_owner.m_stagePositions;
        }
        set
        {
          this.m_owner.m_stagePositions = value;
        }
      }

      public List<int>[] m_stageDirs
      {
        get
        {
          return this.m_owner.m_stageDirs;
        }
        set
        {
          this.m_owner.m_stageDirs = value;
        }
      }

      public HeroDragButton m_draggingHeroButton
      {
        get
        {
          return this.m_owner.m_draggingHeroButton;
        }
        set
        {
          this.m_owner.m_draggingHeroButton = value;
        }
      }

      public static int s_testSoldierCount0
      {
        get
        {
          return BattlePrepareUIController.s_testSoldierCount0;
        }
        set
        {
          BattlePrepareUIController.s_testSoldierCount0 = value;
        }
      }

      public static int s_testSoldierCount1
      {
        get
        {
          return BattlePrepareUIController.s_testSoldierCount1;
        }
        set
        {
          BattlePrepareUIController.s_testSoldierCount1 = value;
        }
      }

      public static int s_testSkillId0
      {
        get
        {
          return BattlePrepareUIController.s_testSkillId0;
        }
        set
        {
          BattlePrepareUIController.s_testSkillId0 = value;
        }
      }

      public static int s_testSkillId1
      {
        get
        {
          return BattlePrepareUIController.s_testSkillId1;
        }
        set
        {
          BattlePrepareUIController.s_testSkillId1 = value;
        }
      }

      public static int s_testTalentId0
      {
        get
        {
          return BattlePrepareUIController.s_testTalentId0;
        }
        set
        {
          BattlePrepareUIController.s_testTalentId0 = value;
        }
      }

      public static int s_testTalentId1
      {
        get
        {
          return BattlePrepareUIController.s_testTalentId1;
        }
        set
        {
          BattlePrepareUIController.s_testTalentId1 = value;
        }
      }

      public static bool s_isTestOnStage
      {
        get
        {
          return BattlePrepareUIController.s_isTestOnStage;
        }
        set
        {
          BattlePrepareUIController.s_isTestOnStage = value;
        }
      }

      public int m_battlePowerValue
      {
        get
        {
          return this.m_owner.m_battlePowerValue;
        }
        set
        {
          this.m_owner.m_battlePowerValue = value;
        }
      }

      public Coroutine m_setBattlePowerValueCoroutine
      {
        get
        {
          return this.m_owner.m_setBattlePowerValueCoroutine;
        }
        set
        {
          this.m_owner.m_setBattlePowerValueCoroutine = value;
        }
      }

      public bool m_hasBattleRule
      {
        get
        {
          return this.m_owner.m_hasBattleRule;
        }
        set
        {
          this.m_owner.m_hasBattleRule = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void OnDisable()
      {
        this.m_owner.OnDisable();
      }

      public void OnApplicationPause(bool isPause)
      {
        this.m_owner.OnApplicationPause(isPause);
      }

      public void OnApplicationFocus(bool focus)
      {
        this.m_owner.OnApplicationFocus(focus);
      }

      public void CancelDragging()
      {
        this.m_owner.CancelDragging();
      }

      public void Update()
      {
        this.m_owner.Update();
      }

      public int GetStageDirection(GridPosition p, StagePositionType posType)
      {
        return this.m_owner.GetStageDirection(p, posType);
      }

      public HeroDragButton CreateHeroButton(BattleHero hero, Transform parent)
      {
        return this.m_owner.CreateHeroButton(hero, parent);
      }

      public static bool IsSkillListEqual(List<int> skillIds1, List<int> skillIds2)
      {
        return BattlePrepareUIController.IsSkillListEqual(skillIds1, skillIds2);
      }

      public void SortChildByY(Transform parent)
      {
        this.m_owner.SortChildByY(parent);
      }

      public static int CompareTransformY(Transform t0, Transform t1)
      {
        return BattlePrepareUIController.CompareTransformY(t0, t1);
      }

      public void ShowBattleRulePanel()
      {
        this.m_owner.ShowBattleRulePanel();
      }

      public void HideBattleRulePanel()
      {
        this.m_owner.HideBattleRulePanel();
      }

      public IEnumerator Co_SetBattlePowerValue(int newValue, int oldValue)
      {
        return this.m_owner.Co_SetBattlePowerValue(newValue, oldValue);
      }

      public void UpdateTestUI()
      {
        this.m_owner.UpdateTestUI();
      }

      public void UpdateTestValuesToInputField()
      {
        this.m_owner.UpdateTestValuesToInputField();
      }

      public static void SetInputFieldValue(InputField inputField, int value, int defaultValue)
      {
        BattlePrepareUIController.SetInputFieldValue(inputField, value, defaultValue);
      }

      public void UpdateTestValuesFromInputField()
      {
        this.m_owner.UpdateTestValuesFromInputField();
      }

      public static int GetInputFieldValue(InputField inputField, int defaultValue)
      {
        return BattlePrepareUIController.GetInputFieldValue(inputField, defaultValue);
      }

      public void HideTeamReadyCountdown()
      {
        this.m_owner.HideTeamReadyCountdown();
      }

      public void HideTeamReadyWait()
      {
        this.m_owner.HideTeamReadyWait();
      }

      public void OnPauseButtonClick()
      {
        this.m_owner.OnPauseButtonClick();
      }

      public void OnArmyRelationButtonClick()
      {
        this.m_owner.OnArmyRelationButtonClick();
      }

      public void OnStartButtonClick()
      {
        this.m_owner.OnStartButtonClick();
      }

      public void OnRecommendHeroButtonClick()
      {
        this.m_owner.OnRecommendHeroButtonClick();
      }

      public void OnActionOrderButtonClick()
      {
        this.m_owner.OnActionOrderButtonClick();
      }

      public void OnChatButtonClick()
      {
        this.m_owner.OnChatButtonClick();
      }

      public void OnHelpButtonClick()
      {
        this.m_owner.OnHelpButtonClick();
      }

      public void OnTestOnStageToggle(bool on)
      {
        this.m_owner.OnTestOnStageToggle(on);
      }

      public void ClearPointerDownStageActor()
      {
        this.m_owner.ClearPointerDownStageActor();
      }

      public void CreateDraggingHeroButton(BattleHero hero)
      {
        this.m_owner.CreateDraggingHeroButton(hero);
      }

      public void DestroyDragginHeroButton()
      {
        this.m_owner.DestroyDragginHeroButton();
      }

      public void MoveDraggingHeroButton(Vector2 pos)
      {
        this.m_owner.MoveDraggingHeroButton(pos);
      }

      public void UpdateStageActorCount()
      {
        this.m_owner.UpdateStageActorCount();
      }

      public void HideActorInfo()
      {
        this.m_owner.HideActorInfo();
      }
    }
  }
}
