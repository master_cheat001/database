﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.SignRewardItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using SLua;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class SignRewardItemUIController : UIControllerBase, IPointerClickHandler, IEventSystemHandler
  {
    [AutoBind("./ItemGroup/RewardGoodsDummy", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_signItemGoodsDummy;
    [AutoBind("./FrameImage", AutoBindAttribute.InitState.NotInit, false)]
    public Image m_itemFrameImage;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_signItemUIStateCtrl;
    [AutoBind("./DayText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_dayText;
    private SignRewardItemUIController.SignState m_signState;
    private GoodsType m_goodsType;
    private int m_goodsId;
    private int m_goodsCount;
    private ConfigDataItemInfo m_itemInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPointerClick(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetReward(Goods goods)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDay(int day)
    {
      // ISSUE: unable to decompile the method.
    }

    public void PlaySignAnimation(Action onEnd)
    {
      this.StartCoroutine(this.Co_ChangeStateToSigning(onEnd));
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_ChangeStateToSigning(Action onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void TodayAutoSign()
    {
      // ISSUE: unable to decompile the method.
    }

    public GoodsType GetGoodsType()
    {
      return this.m_goodsType;
    }

    public int GetGoodsId()
    {
      return this.m_goodsId;
    }

    public int GetGoodsCount()
    {
      return this.m_goodsCount;
    }

    public event Action EventOnSignTodayItemClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<GoodsType, int, int> EventOnSignTodayBoxOpenClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<List<Goods>, SignRewardItemUIController> EventOnShowBoxRewards
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public enum SignState
    {
      Signed,
      NeedSign,
      NotSign,
    }
  }
}
