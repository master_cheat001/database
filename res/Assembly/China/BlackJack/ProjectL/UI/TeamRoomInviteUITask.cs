﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.TeamRoomInviteUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class TeamRoomInviteUITask : UITask
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private TeamRoomInviteUIController m_teamRoomInviteUIController;
    private TeamRoomInvitePlayerType m_playerType;
    private List<UserSummary> m_playerList;
    private List<string> m_updateStatusUserIds;
    private List<TeamRoomInviteUITask.OnlinePlayerStatus> m_onlinePlayersStatus;
    private GameFunctionType m_onlinePlayersStatusGameFunctionType;
    private int m_onlinePlayersStatusLocationId;
    [DoNotToLua]
    private TeamRoomInviteUITask.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctorString_hotfix;
    private LuaFunction m_PrepareForStartOrResumeUIIntentAction`1_hotfix;
    private LuaFunction m_GetGuildInfoAction`1_hotfix;
    private LuaFunction m_InitAllUIControllers_hotfix;
    private LuaFunction m_ClearAllContextAndRes_hotfix;
    private LuaFunction m_InitTeamRoomInviteUIController_hotfix;
    private LuaFunction m_UninitTeamRoomInviteUIController_hotfix;
    private LuaFunction m_UpdateView_hotfix;
    private LuaFunction m_StartTeamRoomInviteeInfoGetNetTaskList`1TeamRoomInviteeInfoType_hotfix;
    private LuaFunction m_IsUserInTeamRoomString_hotfix;
    private LuaFunction m_CloseAndReturnPrevUITask_hotfix;
    private LuaFunction m_UpdateOnlinePlayerStatusStringTeamRoomPlayerInviteState_hotfix;
    private LuaFunction m_GetOnlinePlayerStatusString_hotfix;
    private LuaFunction m_TeamRoomInviteUIController_OnConfirmList`1_hotfix;
    private LuaFunction m_TeamRoomInviteUIController_OnCancel_hotfix;
    private LuaFunction m_TeamRoomInviteUIController_OnChangePlayerTypeTeamRoomInvitePlayerType_hotfix;
    private LuaFunction m_PlayerContext_OnTeamRoomInviteeInfoNtfStringInt32Int32_hotfix;
    private LuaFunction m_get_LayerDescArray_hotfix;
    private LuaFunction m_get_UICtrlDescArray_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public TeamRoomInviteUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void PrepareForStartOrResume(UIIntent intent, Action<bool> onPrepareEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GetGuildInfo(Action<bool> onPrepareEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitTeamRoomInviteUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UninitTeamRoomInviteUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartTeamRoomInviteeInfoGetNetTask(
      List<string> userIds,
      TeamRoomInviteeInfoType infoType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsUserInTeamRoom(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int ComparePlayerLevel(UserSummary u1, UserSummary u2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseAndReturnPrevUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateOnlinePlayerStatus(string userId, TeamRoomPlayerInviteState inviteState)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private TeamRoomInviteUITask.OnlinePlayerStatus GetOnlinePlayerStatus(
      string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamRoomInviteUIController_OnConfirm(List<string> playerUserIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamRoomInviteUIController_OnCancel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamRoomInviteUIController_OnChangePlayerType(TeamRoomInvitePlayerType playerType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnTeamRoomInviteeInfoNtf(
      string inviteeUserId,
      int levelUnlocked,
      int guildMassiveCombatAvailable)
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public TeamRoomInviteUITask.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private bool __callBase_OnStart(UIIntent intent)
    {
      return this.OnStart(intent);
    }

    private void __callBase_OnStop()
    {
      this.OnStop();
    }

    private void __callBase_OnPause()
    {
      this.OnPause();
    }

    private bool __callBase_OnResume(UIIntent intent)
    {
      return this.OnResume(intent);
    }

    private bool __callBase_OnNewIntent(UIIntent intent)
    {
      return this.OnNewIntent(intent);
    }

    private void __callBase_EnableUIInput(bool isEnable, bool? isGlobalEnable)
    {
      this.EnableUIInput(isEnable, isGlobalEnable);
    }

    private bool __callBase_IsNeedLoadDynamicRes()
    {
      return this.IsNeedLoadDynamicRes();
    }

    private List<string> __callBase_CollectAllDynamicResForLoad()
    {
      return this.CollectAllDynamicResForLoad();
    }

    private void __callBase_RegisterPlayerContextEvents()
    {
      this.RegisterPlayerContextEvents();
    }

    private void __callBase_UnregisterPlayerContextEvents()
    {
      this.UnregisterPlayerContextEvents();
    }

    private void __callBase_PostUpdateView()
    {
      this.PostUpdateView();
    }

    private void __callBase_CollectPreloadResourceList()
    {
      this.CollectPreloadResourceList();
    }

    private void __callBase_ClearAssetList()
    {
      this.ClearAssetList();
    }

    private void __callBase_CollectAsset(string assetName)
    {
      this.CollectAsset(assetName);
    }

    private void __callBase_CollectSpriteAsset(string assetName)
    {
      this.CollectSpriteAsset(assetName);
    }

    private void __callBase_CollectFxAsset(string assetName)
    {
      this.CollectFxAsset(assetName);
    }

    private void __callBase_StartLoadCustomAssets(Action onLoadCompleted)
    {
      this.StartLoadCustomAssets(onLoadCompleted);
    }

    private bool __callBase_IsLoadingCustomAssets()
    {
      return this.IsLoadingCustomAssets();
    }

    private bool __callBase_IsPipeLineRunning()
    {
      return this.IsPipeLineRunning();
    }

    private bool __callBase_IsOpeningUI()
    {
      return this.IsOpeningUI();
    }

    private UITaskBase __callBase_ReturnPrevUITask()
    {
      return this.ReturnPrevUITask();
    }

    private void __callBase_ClearUnusedDynamicResourceCache()
    {
      this.ClearUnusedDynamicResourceCache();
    }

    private void __callBase_ClearDynamicResourceCache(List<string> assets)
    {
      this.ClearDynamicResourceCache(assets);
    }

    private void __callBase_OnMemoryWarning()
    {
      this.OnMemoryWarning();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [HotFix]
    public class OnlinePlayerStatus
    {
      public DateTime UpdateTime;
      public string UserId;
      public TeamRoomPlayerInviteState InviteState;
    }

    public class LuaExportHelper
    {
      private TeamRoomInviteUITask m_owner;

      public LuaExportHelper(TeamRoomInviteUITask owner)
      {
        this.m_owner = owner;
      }

      public bool __callBase_OnStart(UIIntent intent)
      {
        return this.m_owner.__callBase_OnStart(intent);
      }

      public void __callBase_OnStop()
      {
        this.m_owner.__callBase_OnStop();
      }

      public void __callBase_OnPause()
      {
        this.m_owner.__callBase_OnPause();
      }

      public bool __callBase_OnResume(UIIntent intent)
      {
        return this.m_owner.__callBase_OnResume(intent);
      }

      public bool __callBase_OnNewIntent(UIIntent intent)
      {
        return this.m_owner.__callBase_OnNewIntent(intent);
      }

      public void __callBase_EnableUIInput(bool isEnable, bool? isGlobalEnable)
      {
        this.m_owner.__callBase_EnableUIInput(isEnable, isGlobalEnable);
      }

      public bool __callBase_IsNeedLoadDynamicRes()
      {
        return this.m_owner.__callBase_IsNeedLoadDynamicRes();
      }

      public List<string> __callBase_CollectAllDynamicResForLoad()
      {
        return this.m_owner.__callBase_CollectAllDynamicResForLoad();
      }

      public void __callBase_RegisterPlayerContextEvents()
      {
        this.m_owner.__callBase_RegisterPlayerContextEvents();
      }

      public void __callBase_UnregisterPlayerContextEvents()
      {
        this.m_owner.__callBase_UnregisterPlayerContextEvents();
      }

      public void __callBase_PostUpdateView()
      {
        this.m_owner.__callBase_PostUpdateView();
      }

      public void __callBase_CollectPreloadResourceList()
      {
        this.m_owner.__callBase_CollectPreloadResourceList();
      }

      public void __callBase_ClearAssetList()
      {
        this.m_owner.__callBase_ClearAssetList();
      }

      public void __callBase_CollectAsset(string assetName)
      {
        this.m_owner.__callBase_CollectAsset(assetName);
      }

      public void __callBase_CollectSpriteAsset(string assetName)
      {
        this.m_owner.__callBase_CollectSpriteAsset(assetName);
      }

      public void __callBase_CollectFxAsset(string assetName)
      {
        this.m_owner.__callBase_CollectFxAsset(assetName);
      }

      public void __callBase_StartLoadCustomAssets(Action onLoadCompleted)
      {
        this.m_owner.__callBase_StartLoadCustomAssets(onLoadCompleted);
      }

      public bool __callBase_IsLoadingCustomAssets()
      {
        return this.m_owner.__callBase_IsLoadingCustomAssets();
      }

      public bool __callBase_IsPipeLineRunning()
      {
        return this.m_owner.__callBase_IsPipeLineRunning();
      }

      public bool __callBase_IsOpeningUI()
      {
        return this.m_owner.__callBase_IsOpeningUI();
      }

      public UITaskBase __callBase_ReturnPrevUITask()
      {
        return this.m_owner.__callBase_ReturnPrevUITask();
      }

      public void __callBase_ClearUnusedDynamicResourceCache()
      {
        this.m_owner.__callBase_ClearUnusedDynamicResourceCache();
      }

      public void __callBase_ClearDynamicResourceCache(List<string> assets)
      {
        this.m_owner.__callBase_ClearDynamicResourceCache(assets);
      }

      public void __callBase_OnMemoryWarning()
      {
        this.m_owner.__callBase_OnMemoryWarning();
      }

      public UITaskBase.LayerDesc[] m_layerDescArray
      {
        get
        {
          return this.m_owner.m_layerDescArray;
        }
        set
        {
          this.m_owner.m_layerDescArray = value;
        }
      }

      public UITaskBase.UIControllerDesc[] m_uiCtrlDescArray
      {
        get
        {
          return this.m_owner.m_uiCtrlDescArray;
        }
        set
        {
          this.m_owner.m_uiCtrlDescArray = value;
        }
      }

      public TeamRoomInviteUIController m_teamRoomInviteUIController
      {
        get
        {
          return this.m_owner.m_teamRoomInviteUIController;
        }
        set
        {
          this.m_owner.m_teamRoomInviteUIController = value;
        }
      }

      public TeamRoomInvitePlayerType m_playerType
      {
        get
        {
          return this.m_owner.m_playerType;
        }
        set
        {
          this.m_owner.m_playerType = value;
        }
      }

      public List<UserSummary> m_playerList
      {
        get
        {
          return this.m_owner.m_playerList;
        }
        set
        {
          this.m_owner.m_playerList = value;
        }
      }

      public List<string> m_updateStatusUserIds
      {
        get
        {
          return this.m_owner.m_updateStatusUserIds;
        }
        set
        {
          this.m_owner.m_updateStatusUserIds = value;
        }
      }

      public List<TeamRoomInviteUITask.OnlinePlayerStatus> m_onlinePlayersStatus
      {
        get
        {
          return this.m_owner.m_onlinePlayersStatus;
        }
        set
        {
          this.m_owner.m_onlinePlayersStatus = value;
        }
      }

      public GameFunctionType m_onlinePlayersStatusGameFunctionType
      {
        get
        {
          return this.m_owner.m_onlinePlayersStatusGameFunctionType;
        }
        set
        {
          this.m_owner.m_onlinePlayersStatusGameFunctionType = value;
        }
      }

      public int m_onlinePlayersStatusLocationId
      {
        get
        {
          return this.m_owner.m_onlinePlayersStatusLocationId;
        }
        set
        {
          this.m_owner.m_onlinePlayersStatusLocationId = value;
        }
      }

      public UITaskBase.LayerDesc[] LayerDescArray
      {
        get
        {
          return this.m_owner.LayerDescArray;
        }
      }

      public UITaskBase.UIControllerDesc[] UICtrlDescArray
      {
        get
        {
          return this.m_owner.UICtrlDescArray;
        }
      }

      public void GetGuildInfo(Action<bool> onPrepareEnd)
      {
        this.m_owner.GetGuildInfo(onPrepareEnd);
      }

      public void InitAllUIControllers()
      {
        this.m_owner.InitAllUIControllers();
      }

      public void ClearAllContextAndRes()
      {
        this.m_owner.ClearAllContextAndRes();
      }

      public void InitTeamRoomInviteUIController()
      {
        this.m_owner.InitTeamRoomInviteUIController();
      }

      public void UninitTeamRoomInviteUIController()
      {
        this.m_owner.UninitTeamRoomInviteUIController();
      }

      public void UpdateView()
      {
        this.m_owner.UpdateView();
      }

      public void StartTeamRoomInviteeInfoGetNetTask(
        List<string> userIds,
        TeamRoomInviteeInfoType infoType)
      {
        this.m_owner.StartTeamRoomInviteeInfoGetNetTask(userIds, infoType);
      }

      public bool IsUserInTeamRoom(string userId)
      {
        return this.m_owner.IsUserInTeamRoom(userId);
      }

      public static int ComparePlayerLevel(UserSummary u1, UserSummary u2)
      {
        return TeamRoomInviteUITask.ComparePlayerLevel(u1, u2);
      }

      public void CloseAndReturnPrevUITask()
      {
        this.m_owner.CloseAndReturnPrevUITask();
      }

      public void UpdateOnlinePlayerStatus(string userId, TeamRoomPlayerInviteState inviteState)
      {
        this.m_owner.UpdateOnlinePlayerStatus(userId, inviteState);
      }

      public TeamRoomInviteUITask.OnlinePlayerStatus GetOnlinePlayerStatus(
        string userId)
      {
        return this.m_owner.GetOnlinePlayerStatus(userId);
      }

      public void TeamRoomInviteUIController_OnConfirm(List<string> playerUserIds)
      {
        this.m_owner.TeamRoomInviteUIController_OnConfirm(playerUserIds);
      }

      public void TeamRoomInviteUIController_OnCancel()
      {
        this.m_owner.TeamRoomInviteUIController_OnCancel();
      }

      public void TeamRoomInviteUIController_OnChangePlayerType(TeamRoomInvitePlayerType playerType)
      {
        this.m_owner.TeamRoomInviteUIController_OnChangePlayerType(playerType);
      }

      public void PlayerContext_OnTeamRoomInviteeInfoNtf(
        string inviteeUserId,
        int levelUnlocked,
        int guildMassiveCombatAvailable)
      {
        this.m_owner.PlayerContext_OnTeamRoomInviteeInfoNtf(inviteeUserId, levelUnlocked, guildMassiveCombatAvailable);
      }
    }
  }
}
