﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.WorldEventListItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class WorldEventListItemUIController : UIControllerBase
  {
    [AutoBind("./Panel", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_button;
    [AutoBind("./Panel/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_iconImage;
    [AutoBind("./Panel/Title/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_titleText;
    [AutoBind("./Panel/Energy/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_energyText;
    [AutoBind("./Panel/Level/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_levelText;
    [AutoBind("./Panel/Time", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_timeGameObject;
    [AutoBind("./Panel/Time/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_timeText;
    private ConfigDataWaypointInfo m_wayPointInfo;
    private ConfigDataEventInfo m_eventInfo;
    private DateTime m_expireTime;
    private float m_updateCountdown;

    [MethodImpl((MethodImplOptions) 32768)]
    public WorldEventListItemUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetEventInfo(ConfigDataWaypointInfo waypointInfo, ConfigDataEventInfo eventInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetExpireTime(long expireTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateRemainTime()
    {
      // ISSUE: unable to decompile the method.
    }

    public ConfigDataWaypointInfo GetWaypointInfo()
    {
      return this.m_wayPointInfo;
    }

    public ConfigDataEventInfo GetEventInfo()
    {
      return this.m_eventInfo;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<WorldEventListItemUIController> EventOnClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
