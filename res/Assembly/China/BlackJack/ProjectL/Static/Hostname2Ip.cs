﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Static.Hostname2Ip
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using System;
using System.Net;
using System.Runtime.CompilerServices;
using System.Threading;

namespace BlackJack.ProjectL.Static
{
  internal class Hostname2Ip
  {
    private Thread m_thread;
    private bool m_isThreadExit;
    private string m_ip;
    private string m_hostname;

    [MethodImpl((MethodImplOptions) 32768)]
    public Hostname2Ip(string hostname)
    {
      this.m_hostname = hostname;
      this.m_thread = new Thread(new ThreadStart(this.ThreadProc));
      this.m_thread.Start();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Destroy()
    {
      if (this.m_thread != null)
        this.m_thread.Abort();
      this.m_thread = (Thread) null;
      this.m_isThreadExit = true;
    }

    public bool isDone
    {
      get
      {
        return this.m_isThreadExit;
      }
    }

    public string ip
    {
      get
      {
        return this.m_ip;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ThreadProc()
    {
      this.m_ip = this.Hostname2IP(this.m_hostname);
      this.m_thread = (Thread) null;
      this.m_isThreadExit = true;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string Hostname2IP(string hostname)
    {
      try
      {
        IPAddress address;
        if (IPAddress.TryParse(hostname, out address))
          return address.ToString();
        return Dns.GetHostEntry(hostname).AddressList[0].ToString();
      }
      catch (Exception ex)
      {
        return string.Empty;
      }
    }
  }
}
