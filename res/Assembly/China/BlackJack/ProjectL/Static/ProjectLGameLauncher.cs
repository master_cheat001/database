﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Static.ProjectLGameLauncher
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime;
using BlackJack.BJFramework.Runtime.Log;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.UI;
using PD.SDK;
using System;
using System.Collections;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using UnityEngine;

namespace BlackJack.ProjectL.Static
{
  public class ProjectLGameLauncher : MonoBehaviour
  {
    private ProjectLGameManager m_gameManager;
    private DateTime m_pauseTime;
    private IGooglePlayObbDownloader m_obbDownloader;

    [MethodImpl((MethodImplOptions) 32768)]
    private void Awake()
    {
      ProjectLGameLauncher.InitAutoRotateSetting();
      Debug.m_mainThread = Thread.CurrentThread;
      LogManager.CreateLogManager();
      LogManager.Instance.Initlize(Application.platform == RuntimePlatform.WindowsEditor, true, Application.persistentDataPath + "/Log/", "Log");
      Application.targetFrameRate = 60;
      Util.LogMemorySize("ProjectLGameLauncher.Awake");
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Start()
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new ProjectLGameLauncher.\u003CStart\u003Ec__Iterator0()
      {
        \u0024this = this
      };
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void InitAutoRotateSetting()
    {
      Screen.autorotateToLandscapeLeft = true;
      Screen.autorotateToLandscapeRight = true;
      Screen.autorotateToPortrait = false;
      Screen.autorotateToPortraitUpsideDown = false;
      Screen.orientation = ScreenOrientation.AutoRotation;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      if (this.m_gameManager == null)
        return;
      this.m_gameManager.Tick();
    }

    private void LateUpdate()
    {
      CommonUIController.StaticLateUpdate();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnApplicationQuit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnApplicationPause(bool isPause)
    {
      Debug.Log(string.Format("ProjectLGameLauncher.OnApplicationPause {0}", (object) isPause));
      if (GameManager.Instance == null)
        return;
      ProjectLPlayerContext playerContext = GameManager.Instance.PlayerContext as ProjectLPlayerContext;
      bool flag = (UnityEngine.Object) PDSDK.Instance != (UnityEngine.Object) null && PDSDK.Instance.m_isCallWebView;
      if (isPause)
      {
        this.m_pauseTime = DateTime.Now;
        if (flag || playerContext == null)
          return;
        playerContext.SendClientAppPauseNtf();
      }
      else
      {
        if (!(this.m_pauseTime != DateTime.MinValue))
          return;
        if (!flag && playerContext != null)
          playerContext.SendClientAppResumeNtf();
        this.m_pauseTime = DateTime.MinValue;
      }
    }
  }
}
