﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.CommonReflection.IComponentOwner
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

namespace BlackJack.ProjectL.CommonReflection
{
  public interface IComponentOwner
  {
    T AddOwnerComponent<T>() where T : class, IComponentBase, new();

    void RemoveOwnerComponent<T>() where T : class, IComponentBase;

    T GetOwnerComponent<T>() where T : class, IComponentBase;

    IComponentBase GetOwnerComponent(string componentName);
  }
}
