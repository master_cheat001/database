﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.LibClient.IClient
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

namespace BlackJack.ProjectL.LibClient
{
  public interface IClient
  {
    bool LoginByAuthToken(
      string serverAddress,
      int serverPort,
      string authToken,
      string clientVersion,
      string clientDeviceId,
      string localization,
      int loginChannelId,
      int bornChannelId,
      string serverDomain);

    bool LoginBySessionToken(
      string sessionToken,
      string clientVersion,
      string localization,
      int loginChannelId,
      int bornChannelId);

    bool Disconnect();

    bool SendMessage(object msg);

    void Tick();

    void Close();

    void BlockProcessMsg(bool isBlock);
  }
}
