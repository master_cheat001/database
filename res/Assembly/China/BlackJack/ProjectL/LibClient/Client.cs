﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.LibClient.Client
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.LibClient;
using SLua;
using System;
using System.IO;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.LibClient
{
  [CustomLuaClass]
  public class Client : IClient
  {
    private LibClientStateMachine m_csm;
    private IClientEventHandler m_clientEventHandler;
    private string m_authToken;
    private string m_clientVersion;
    private string m_clientDeviceId;
    private string m_localization;
    private int m_loginChannelId;
    private int m_bornChannelId;
    private string m_sessionToken;
    private Connection m_connect;
    private LibClientProtoProvider m_protoProvider;
    private bool m_isBlockProcessMsg;
    private Func<Stream, Type, int, object> m_messageDeserializeAction;
    private string m_connectedAddress;

    [MethodImpl((MethodImplOptions) 32768)]
    public Client(
      IClientEventHandler handler,
      Func<Stream, Type, int, object> deserializeMessageAction = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetClientToInit()
    {
      // ISSUE: unable to decompile the method.
    }

    public void BlockProcessMsg(bool isBlock)
    {
      this.m_isBlockProcessMsg = isBlock;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool LoginByAuthToken(
      string serverAddress,
      int serverPort,
      string authToken,
      string clientVersion,
      string clientDeviceId,
      string localization = "",
      int loginChannelId = 0,
      int bornChannelId = 0,
      string serverDomain = "")
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool LoginBySessionToken(
      string sessionToken,
      string clientVersion,
      string localization = "",
      int loginChannelId = 0,
      int bornChannelId = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Disconnect()
    {
      // ISSUE: unable to decompile the method.
    }

    public void Close()
    {
      this.m_connect.Close();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendMessage(object msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Tick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void TickAuthLogin()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void TickSessionLogin()
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual void TickLoginOk()
    {
      this.ProcessMessages((MessageProc) null);
    }

    protected virtual void TickDisconnecting()
    {
      this.ProcessMessages((MessageProc) null);
    }

    protected virtual void TickDisconnected()
    {
      this.ProcessMessages((MessageProc) null);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DebugWarning(string fun, string msg1, string msg2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ProcessMessages(MessageProc proc = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetEndPoint()
    {
      // ISSUE: unable to decompile the method.
    }

    public void RegConnectionLogEvent(Action<string> logEvent)
    {
      this.m_connect.EventOnLogPrint += logEvent;
    }

    public string GetIp()
    {
      return this.m_connectedAddress;
    }
  }
}
