﻿// Decompiled with JetBrains decompiler
// Type: DebugConsoleCtrl
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using System;
using System.Runtime.CompilerServices;

public class DebugConsoleCtrl
{
  private DebugConsoleView _consoleView;
  private DebugConsoleMode _consoleMode;

  private DebugConsoleCtrl()
  {
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static DebugConsoleCtrl Create(
    DebugConsoleView consoleView,
    DebugConsoleMode consoleMode)
  {
    DebugConsoleCtrl debugConsoleCtrl = new DebugConsoleCtrl();
    debugConsoleCtrl._Init(consoleView, consoleMode);
    return debugConsoleCtrl;
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void _Init(DebugConsoleView consoleView, DebugConsoleMode consoleMode)
  {
    this._consoleView = consoleView;
    this._consoleMode = consoleMode;
    this._consoleView.OnKeyDown += new EventHandler<KeyDownEventArgs>(this._ConsoleView_OnKeyDown);
    this._consoleView.OnKeyUp += new EventHandler<KeyUpEventArgs>(this._ConsoleView_OnKeyUp);
    this._consoleView.OnOKButtonClick = new Action(this._ConsoleView_OnBtnOKClick);
    this._consoleView.OnCloseButtonClick = new Action(this._ConsoleView_OnBtnCloseClick);
    this._consoleMode.refreshEvent += new DebugConsoleMode.refreshDelegate(this._ConsoleModeLogRefresh);
  }

  private void _ConsoleView_OnKeyUp(object sender, KeyUpEventArgs e)
  {
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void _ConsoleView_OnKeyDown(object sender, KeyDownEventArgs e)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void _ConsoleView_OnBtnCloseClick()
  {
    // ISSUE: unable to decompile the method.
  }

  private void _ConsoleView_OnBtnOKClick()
  {
    this._ProcessCmd();
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void _ProcessCmd()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void _ConsoleModeLogRefresh()
  {
    this._consoleView.LogText = this._consoleMode.GetLogText();
  }
}
